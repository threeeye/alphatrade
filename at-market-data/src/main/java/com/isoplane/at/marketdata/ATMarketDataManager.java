package com.isoplane.at.marketdata;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATMarketData.OptionRight;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATMarketDataSubscriber;
import com.isoplane.at.commons.service.IATMarketStatusService;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.IATIdable;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.marketdata.util.ATMarketDataMongodbTools;
import com.isoplane.at.mongodb.ATMongoStatusService;

public class ATMarketDataManager
		implements IATMarketDataSubscriber, IATProtoMarketSubscriber, IATMarketDataService, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATMarketDataManager.class);

	// private Configuration _config;
	private boolean _isStreaming = false;
	// private boolean _isInitialized;
	private boolean _processNotifications;
	private Set<IATMarketDataSubscriber> _subscribers;
	private ATMarketDataMongodbTools _mongodb;
	private Map<String, IATMarketData> _persistenceBufferMap;
	private EATMarketStatus _marketStatus = null;
	private ATMarketDataNotifier _notifier;
	private Future<?> _notifierFuture;
	private long _notifierTS = 0;

	private Map<String, Long> _throttleMap;
	private BlockingQueue<String> _queue;
	private Integer _queueSize = 0;
	private Long _queueInterval;
	private Long _queueTs = System.currentTimeMillis();
	private Long _queueCountIn = 0L;
	private Long _queueCountOut = 0L;

	private int _notifierCrashCount = 0;

	private ATMoverMap _moverMap;

	private Long _threshold;
	private Double _thresholdFactor = 1.0;

	public static void main(String[] args_) {
		try {
			String propertiesPath = args_[0];
			log.info(String.format("Reading properties: %s", propertiesPath));
			ATConfigUtil.init(propertiesPath, false);
			// PropertiesConfiguration config = new Configurations().properties(new File(propertiesPath));

			ATExecutors.init();
			IATMarketStatusService statusSvc = new ATMongoStatusService();
			ATServiceRegistry.setStatusService(statusSvc, false);

			ATMarketDataManager instance = new ATMarketDataManager(true);
			instance.initService();

			// instance._mongodb.migrateOptions();

			ATSysUtils.waitForEnter();
			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATMarketDataManager.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATMarketDataManager(boolean isStreaming_) {
		Configuration config = ATConfigUtil.config();
		// _config = config_;
		_isStreaming = isStreaming_;
		_persistenceBufferMap = new HashMap<>();
		_moverMap = new ATMoverMap();
		// _moverMap = new HashMap<>();
		_throttleMap = new HashMap<>();

		if (_isStreaming) {
			_queueSize = config.getInt("market.queueSize");
			_queueInterval = config.getLong("market.queueInterval");
			log.info(String.format("Initializing queue [%d]", _queueSize));
			_queue = new ArrayBlockingQueue<>(_queueSize);

			scheduleNotifier();
		}

		ATSystemStatusManager.register(this);
	}

	private void scheduleNotifier() {
		String threadId = ATMarketDataNotifier.class.getSimpleName();
		_notifier = new ATMarketDataNotifier(threadId);
		_notifierFuture = ATExecutors.schedule(_notifier.getId(), _notifier, 0);
	}

	public void initService() {
		log.info("Initializing...");

		_threshold = ATConfigUtil.config().getLong("market.throttle");

		initMarketData();
		_processNotifications = true;
		// _isInitialized = true;
		// if (false) { // Deprecated with KafkaProducer and MarketSink
		// bulkPersist(); // NOTE: Requires isInitialized
		// }
	}

	public void shutdown() {
		// _isInitialized = false;
		_processNotifications = false;
		log.debug(String.format("Shutting down"));
		if (_subscribers != null)
			_subscribers.clear();
		if (_notifier != null) {
			_notifier.shutdown();
		}
		if (_mongodb != null)
			_mongodb.shutdown();
	}

	@Override
	public void subscribe(IATMarketDataSubscriber subscriber_) {
		if (_subscribers == null) {
			_subscribers = new HashSet<>();
		}
		if (!_subscribers.contains(subscriber_)) {
			_subscribers.add(subscriber_);
		}
	}

	@Override
	public void unsubscribe(IATMarketDataSubscriber subscriber_) {
		if (_subscribers == null || subscriber_ == null)
			return;
		_subscribers.remove(subscriber_);
	}

	@Override
	public List<ATMarketData> getMovers() {
		List<ATMarketData> movers = new ArrayList<>();
		for (String occId : _moverMap.keySet()) {
			ATMarketData mkt = _mongodb.getMarketData(occId);
			if (mkt != null && mkt.getPxChange() != null) {
				movers.add(mkt);
			}
		}
		Collections.sort(movers, new Comparator<ATMarketData>() {
			@Override
			public int compare(ATMarketData m1, ATMarketData m2) {
				Double c1 = m1.getPxChange();
				Double c2 = m2.getPxChange();
				return Double.compare(c1, c2);
			}
		});
		return movers;
	}

	// @Override
	// public Map<String, ATMarketData> getMarketData(boolean symbolsOnly_) {
	// if (symbolsOnly_) {
	// Map<String, ATMarketData> newMap = new HashMap<>();
	// Map<String, ATMarketData> oldMap = _mongodb.getMarketData();
	// Set<String> keys = new HashSet<>(oldMap.keySet());
	// for (String key : keys) {
	// if (key.length() > 6)
	// continue;
	// ATMarketData mkt = oldMap.get(key);
	// if (mkt != null) {
	// newMap.put(key, mkt);
	// }
	// }
	// // Map<String, ATMarketData> map = new HashMap<>(_mongodb.getMarketData());
	// // Iterator<String> ikey = map.keySet().iterator();
	// // while (ikey.hasNext()) {
	// // String occId = ikey.next();
	// // if (occId.length() > 6)
	// // ikey.remove();
	// // }
	// return newMap;
	// }
	//
	// throw new ATException("Obsolete function");
	// // return marketData;
	// }

	@Override
	public Map<String, ATMarketData> getMarketData(IATWhereQuery query_) {
		Map<String, ATMarketData> data = _mongodb.getMarketData(query_);
		return data;
	}

	@Override
	public ATMarketData getMarketData(String occId_) {
		if (occId_ == null)
			return null;
		ATMarketData mkt = _mongodb.getMarketData(occId_);
		// log.info(String.format("MD keys: %s", market.keySet()));
		// IATMarketData data = market.get(occId_);
		return mkt;
	}

	@Override
	public Map<String, List<ATHistoryData>> getHistory(IATWhereQuery query_, List<String> flags_, String avgMode_, String hvMode_) {
		Map<String, List<ATHistoryData>> historyMap = _mongodb.getHistory(query_, flags_, avgMode_, hvMode_);
		if (EATMarketStatus.CLOSED != getMarketStatus()) {
			String dStr = ATFormats.DATE_yyyy_MM_dd.get().format(new Date());
			for (String occId : historyMap.keySet()) {
				IATMarketData mkt = getMarketData(occId);
				if (mkt != null) {
					List<ATHistoryData> hist = historyMap.get(occId);
					ATHistoryData last = hist.get(hist.size() - 1);
					ATHistoryData now = new ATHistoryData();
					now.putAll(last);
					now.setDateStr(dStr);
					now.setOpen(mkt.getPxOpen());
					now.setClose(mkt.getPxLast());
					now.setHigh(mkt.getPxHigh());
					now.setLow(mkt.getPxLow());
					hist.add(now);
				}
			}
		}
		return historyMap;
	}

	@Deprecated
	@Override
	public Map<String, Map<String, Set<ATMarketData>>> getOptionChainOld(String occId_, String right_) {
		long start = System.currentTimeMillis();
		Map<String, Map<String, Set<ATMarketData>>> r2 = _mongodb.getOptionChain(occId_, right_);
		Duration time = Duration.ofMillis(System.currentTimeMillis() - start);
		log.debug(String.format("getOptionChain Duration [%s]", time));
		return r2;
		// IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, occId_);
		// Map<String, Map<String, List<ATOption>>> result = _mongodb.getOptionChain(query, right_);
		// return result;
	}

	@Override
	public ATSecurityPack getOptionChain(String symbol_, OptionRight right_) {
		Map<String, ATMarketData> chainMap = _mongodb.getChain(symbol_);
		// IATMarketData.CALL;
		TreeMap<String, TreeSet<String>> callMap = new TreeMap<>();
		TreeMap<String, TreeSet<String>> putMap = new TreeMap<>();
		if (chainMap == null)
			return null;

		// String bla = String.format("list: %s", chainMap.keySet());

		SimpleDateFormat fmtDS10 = ATFormats.DATE_yyyy_MM_dd.get();
		TreeMap<String, ATMarketData> mktMap = new TreeMap<>();

		Iterator<ATMarketData> i = chainMap.values().iterator();// _dataMap.iterator();
		while (i.hasNext()) {
			ATMarketData mkt = i.next();
			String occId = mkt.getOccId();
			if (occId.length() < 10)
				continue;
			String rightStr = mkt.getOptionRight();
			OptionRight right = OptionRight.toOptionRight(rightStr);
			if (right_ != null && right_ != right)
				continue;
			TreeMap<String, TreeSet<String>> expMap = OptionRight.CALL == right ? callMap : putMap;
			Date dExp = mkt.getDateExp();
			String expDS10 = fmtDS10.format(dExp);
			TreeSet<String> expSet = expMap.get(expDS10);
			if (expSet == null) {
				expSet = new TreeSet<>();
				expMap.put(expDS10, expSet);
			}
			expSet.add(occId);
			mktMap.put(occId, mkt);
		}
		ATSecurityPack pack = new ATSecurityPack(null, null, mktMap);
		if (callMap != null && !callMap.isEmpty()) {
			TreeMap<String, TreeMap<String, TreeSet<String>>> map = new TreeMap<>();
			map.put(symbol_, callMap);
			pack.setCallChain(map);
		}
		if (putMap != null && !putMap.isEmpty()) {
			TreeMap<String, TreeMap<String, TreeSet<String>>> map = new TreeMap<>();
			map.put(symbol_, putMap);
			pack.setPutChain(map);
		}
		return pack;
	}

	@Override
	public Map<String, ATMarketData> getOptionUnderlying(String occId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, occId_);
		Map<String, ATMarketData> result = _mongodb.getOptionUnderlying(query);
		return result;
	}

	private void initMarketData() {
		if (_isStreaming) {
			readMarketStatus();
		}
		if (_isStreaming) {
			// ATKafkaRegistry.register(_config, ATKafkaMarketDataMonitor.class);
			ATKafkaMarketDataConsumer marketDataConsumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
			marketDataConsumer.subscribe(this);
			ATKafkaMarketDataPackConsumer marketDataPackConsumer = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
			marketDataPackConsumer.subscribe(this);
			// ATKafkaRegistry.start();
		}
		_mongodb = new ATMarketDataMongodbTools();
		_mongodb.init(true);
	}

	// private Future<?> bulkPersist() {
	// long interval = _config.getLong("tradier.stream.save.interval");
	// String threadId = String.format("%s.bulkPersist", getClass().getSimpleName());
	// Future<?> future = ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {
	//
	// @Override
	// public void run() {
	// Thread.currentThread().setName(threadId);
	// try {
	// if (_persistenceBufferMap.isEmpty()) {
	// log.trace(String.format("bulkPersist [%d]", 0));
	// return;
	// }
	// Map<String, IATMarketData> newBufferMap = new HashMap<>();
	// Map<String, IATMarketData> oldBufferMap = _persistenceBufferMap;
	// _persistenceBufferMap = newBufferMap;
	// if (log.isDebugEnabled()) {
	// log.debug(String.format("bulkPersist [%d]", oldBufferMap.size()));
	// }
	// Collection<IATMarketData> storageCollection = oldBufferMap.values();
	// // if (log.isDebugEnabled()) {
	// // Optional<IATMarketData> any = storageCollection.stream().filter(m -> "LB".equals(m.getAtId())).findAny();
	// // if (any.isPresent()) {
	// // IATMarketData data = any.get();
	// // log.debug(String.format("Found: %s", data));
	// // }
	// // }
	// _mongodb.save(storageCollection);
	// oldBufferMap.clear();
	// } catch (Exception ex) {
	// log.error(String.format("Error in bulk persistence thread"), ex);
	// }
	// // } while (_isInitialized);
	// }
	// }, 0, interval);
	// return future;
	// }

	public void update(IATMarketData data_) {
		_persistenceBufferMap.put(data_.getAtId(), data_);
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper wrapper_) {
		for (MarketData protoMkt : wrapper_.proto().getMarketDataList()) {
			processProtoMarket(protoMkt);
		}
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
		for (ATProtoMarketDataWrapper wrapper : wrappers_) {
			processProtoMarket(wrapper.proto());
		}
	}

	private ATMarketData processProtoMarket(MarketData proto_) {
		if (proto_ == null) {
			return null;
		}
		String occId = proto_.getId();
		// if ("SPY".equals(occId)) {
		// log.debug(String.format("debug [%s]", occId));
		// }
		ATMarketData oldMkt = getMarketData(occId);
		ATMarketData mkt = ATProtoMarketDataWrapper.mergeMarketData(oldMkt, proto_);
		_mongodb.updateCache(mkt);
		_moverMap.eval(mkt);

		if (_subscribers != null) {
			for (IATMarketDataSubscriber sub : _subscribers) {
				sub.notify(mkt);
			}
		}
		return mkt;
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper wrapper_) {
		if (_subscribers == null || _subscribers.isEmpty())
			return;
		EATMarketStatus status = wrapper_.value();
		for (IATMarketDataSubscriber sub : _subscribers) {
			sub.notify(status);
		}
	}

	@Override
	public void notify(IATMarketData data_) {
		log.debug(String.format("Notify: %s", data_));
		String occId = data_.getAtId();
		if (!_processNotifications || !_isStreaming || _subscribers == null || _queue.contains(occId))
			return;
		Long lastTimestamp = _throttleMap.get(data_.getAtId());
		if (lastTimestamp != null) {
			if (System.currentTimeMillis() - lastTimestamp < _threshold)
				return;
		}
		_throttleMap.put(data_.getAtId(), System.currentTimeMillis());
		try {
			_queueCountIn++;
			// ATMarketData mkt = data_ instanceof ATMarketData ? (ATMarketData) data_ : new ATMarketData(data_);
			int queueSize = _queue.size();
			int queueRemaining = _queue.remainingCapacity();
			if (!_queue.offer(occId, _queueInterval, TimeUnit.MILLISECONDS)) {
				_queueInterval = ATConfigUtil.config().getLong("market.queueInterval");
				log.error(String.format("Queue[mkt] full [%d/%d]", queueSize, queueRemaining));
				// _queue.clear();
				// int queueSize = _config.getInt("market.queueSize");
			}
			if (queueSize > _queueSize / 2) {
				_thresholdFactor = 1.01;
				_threshold = (long) (_threshold * _thresholdFactor);
			} else {
				_thresholdFactor = 0.99;
				_threshold = (long) (_threshold * _thresholdFactor);
			}
			if (_threshold > 300000) {
				_threshold = 300000L;
			} else if (_threshold < 30000) {
				_threshold = 30000L;
			}
		} catch (Exception ex) {
			log.error(String.format("Queue[mkt] processing error"), ex);
		}
	}

	@Override
	public void notify(EATMarketStatus status_) {
		log.debug(String.format("Notify market status: %s", status_));
		if (!_processNotifications || !_isStreaming || _subscribers == null || status_ == _marketStatus)
			return;
		_marketStatus = status_;
		try {
			for (IATMarketDataSubscriber listener : _subscribers) {
				listener.notify(status_);
			}
		} catch (Exception ex) {
			log.error(String.format("Error notifying market status [%s]", status_), ex);
		}
	}

	public EATMarketStatus getMarketStatus() {
		return _marketStatus;
	}

	private void readMarketStatus() {
		try {
			Map<String, Object> status = ATServiceRegistry.getStatusService().getStatus();
			String mktStatusStr = status != null ? (String) status.get("marketStatus") : null;
			if (status != null) {
				EATMarketStatus marketStatus = EATMarketStatus.valueOf(mktStatusStr);
				log.info(String.format("Loaded market status [%s]", marketStatus));
				_marketStatus = marketStatus;
			}
		} catch (Exception ex) {
			log.error(String.format("Error loading market status"), ex);
		}
	}

	public void update(EATMarketStatus status_) {
		if (status_ == null || _marketStatus == status_)
			return;
		log.info(String.format("Market status changed [%s -> %s]", _marketStatus, status_));
		_marketStatus = status_;
		Map<String, Object> status = Collections.singletonMap("marketStatus", status_.name());
		ATServiceRegistry.getStatusService().update(status);
	}

	// @Override
	private Map<String, String> getMonitoringStatus() {
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("persistenceBuffer.size", _persistenceBufferMap.size() + "");
		statusMap.put("isRunning", _notifier._isRunning + "");
		if (_notifier._finalError != null) {
			statusMap.put("error", _notifier._finalError);
		}
		// statusMap.put("persistenceBufferMapMemory", getPersistenceBufferMapMemory());
		statusMap.put("throttleMapSize", _throttleMap.size() + "");
		if (_queue != null) {
			statusMap.put("queue.size", _queue.size() + "");
			statusMap.put("queue.remaining", _queue.remainingCapacity() + "");
		} else {
			statusMap.put("queue", "NULL");
		}
		long tpPeriod = (System.currentTimeMillis() - _queueTs) / 1000;
		double queueTpIn = 1.0 * _queueCountIn / tpPeriod;
		double queueTpOut = 1.0 * _queueCountOut / tpPeriod;
		statusMap.put("queue.tp.in", Precision.round(queueTpIn, 2) + "");
		statusMap.put("queue.tp.out", Precision.round(queueTpOut, 2) + "");
		statusMap.put("threshold", _threshold + "");
		statusMap.put("threshold.factor", Precision.round(_thresholdFactor, 2) + "");

		String notifierStatus = "null";
		if (_notifierFuture != null) {
			if (_notifierFuture.isCancelled()) {
				notifierStatus = "Cancelled";
			} else if (_notifierFuture.isDone()) {
				notifierStatus = "Done";
			} else {
				notifierStatus = "OK";
			}
		}
		statusMap.put("notifier.status", notifierStatus);
		statusMap.put("notifier.ts", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date(_notifierTS)));

		statusMap.put("notifier.crash", _notifierCrashCount + "");
		// statusMap.put("pthrottleMapSizeMemory", getThrottleMapMemory());
		return statusMap;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		return getMonitoringStatus();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	public class ATMarketDataNotifier implements Runnable, IATIdable {

		private String _id;
		private boolean _isRunning;
		private String _finalError;

		public ATMarketDataNotifier(String id_) {
			_id = id_;
		}

		@Override
		public String getId() {
			return _id;
		}

		public void shutdown() {
			_isRunning = false;
		}

		@Override
		public void run() {
			_isRunning = true;
			Thread.currentThread().setName(_id);
			try {
				do {
					// ATMarketData data = _queue.poll(_queueInterval, TimeUnit.MILLISECONDS);
					String occId = _queue.poll(_queueInterval, TimeUnit.MILLISECONDS);
					ATMarketData data = occId != null ? _mongodb.getMarketData(occId) : null;
					_notifierTS = System.currentTimeMillis();
					if (data == null) {
						_queueInterval = ATConfigUtil.config().getLong("market.queueInterval");
						log.debug(String.format("Polling empty queue [%d]", _queueInterval));
					} else {
						_queueCountOut++;
						_moverMap.eval(data);
						for (IATMarketDataSubscriber listener : _subscribers) {
							try {
								listener.notify(data);
							} catch (Exception ex) {
								log.error(String.format("Notification error: %s -> %s", data, listener), ex);
							}
						}
					}
				} while (_isRunning);
			} catch (Throwable ex) {
				_notifierCrashCount++;
				_isRunning = false;
				_finalError = ex.getMessage();
				log.error(String.format("%s terminated.", ATMarketDataNotifier.class.getSimpleName()), ex);
				scheduleNotifier();
			}
			log.debug(String.format("Thread [%s] done", getId()));
		}

	}

	public class ATMoverMap extends ConcurrentHashMap<String, Double> {

		private static final long serialVersionUID = 1L;

		private String _lowestId;
		private Double _lowestChange = 0.0;
		private String threadId = String.format("%s.eval", ATMoverMap.class.getSimpleName());

		public void eval(ATMarketData mkt_) {
			String occId = mkt_.getOccId();
			if (occId.length() > 10)
				return;

			ATExecutors.submit(threadId, new Runnable() {
				@Override
				public void run() {
					Double change = Math.abs(mkt_.getPxChange());
					try {
						if (ATMoverMap.this.size() < 10 || ATMoverMap.this.containsKey(occId)) {
							ATMoverMap.this.put(occId, change);
						} else if (change > _lowestChange) {
							ATMoverMap.this.remove(_lowestId);
							ATMoverMap.this.put(occId, change);
							if (log.isDebugEnabled())
								log.debug(String.format("Mover [%s]", occId));
						} else {
							return;
						}
						Double lowestChange = Double.MAX_VALUE;
						String lowestId = null;
						for (Entry<String, Double> entry : ATMoverMap.this.entrySet()) {
							Double chng = entry.getValue();
							if (chng < lowestChange) {
								lowestChange = chng;
								lowestId = entry.getKey();
							}
						}
						_lowestChange = lowestChange;
						_lowestId = lowestId;
					} catch (Exception ex) {
						log.error(String.format("Error", ex));
					}
				}
			});

			// String occId = mkt_.getOccId();
			// if (occId.length() > 6)
			// return;
			// Double change = Math.abs(mkt_.getPxChange());
			// try {
			// if (this.size() < 10 || this.containsKey(occId)) {
			// this.put(occId, change);
			// } else if (change > _lowestChange) {
			// this.remove(_lowestId);
			// this.put(occId, change);
			// if (log.isDebugEnabled())
			// log.debug(String.format("Mover [%s]", occId));
			// } else {
			// return;
			// }
			// Double lowestChange = Double.MAX_VALUE;
			// String lowestId = null;
			// for (Entry<String, Double> entry : this.entrySet()) {
			// Double chng = entry.getValue();
			// if (chng < lowestChange) {
			// lowestChange = chng;
			// lowestId = entry.getKey();
			// }
			// }
			// _lowestChange = lowestChange;
			// _lowestId = lowestId;
			// } catch (Exception ex) {
			// log.error(String.format("Error", ex));
			// }
		}

	}

}
