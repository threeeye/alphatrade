package com.isoplane.at.marketdata.util;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOption;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATAmeritradeUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.marketdata.model.ATMongoMarketData;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

public class ATMarketDataMongodbTools implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATMarketDataMongodbTools.class);

	// private Configuration _config;
	// private boolean _isStreaming;
	// private IATMarketDataSubscriber _listener;
	private ATMongoDao _dao;
	// private Map<String, ATMarketData> _dataMap;
	private String _mktTable;
	private String _mktTableAmeritrade;
	private String _historyTable;
	private Set<String> _blackList;

	private MarketCache _dataMap;
	// private Cache<String, ATMarketData> _dataMap;

	public ATMarketDataMongodbTools() {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;
		// _isStreaming = isStreaming_;
		// _listener = listener_;
		// _dataMap = new TreeMap<>();
		_blackList = new HashSet<>();

		ATMongoTableRegistry.init();

		_historyTable = ATMongoTableRegistry.getHistoryTable();
		_mktTable = ATMongoTableRegistry.getMarketDataTable();
		_mktTableAmeritrade = ATMongoTableRegistry.getMarketDataTableAmeritrade();

		ATSystemStatusManager.register(this);
	}

	public ATMarketDataMongodbTools init(boolean asService_) {
		_dataMap = new MarketCache();// ATEhCache.instance().getMktCache();
		_dao = new ATMongoDao();
		_dao.setSkipUpdateTS(true);

		if (asService_) {
			cleanup(false);
			readMarketDataSync();
			String threadId1 = String.format("%s.blacklist", this.getClass().getSimpleName());
			ATExecutors.scheduleAtFixedRate(threadId1, new Runnable() {

				@Override
				public void run() {
					Thread.currentThread().setName(threadId1);
					log.info(String.format("Cleaning blacklist [%d]", _blackList.size()));
					_blackList = new HashSet<>();
				}
			}, 1000 * 60 * 5, 1000 * 60 * 5); // 5min

			String threadId2 = String.format("%s.cleanup", this.getClass().getSimpleName());
			ATExecutors.scheduleAtFixedRate(threadId2, new Runnable() {

				@Override
				public void run() {
					Thread.currentThread().setName(threadId2);
					cleanup(true);
				}
			}, 1000 * 60 * 60 * 24, 1000 * 60 * 60 * 24); // 24h
		}
		return this;
	}

	// public Map<String, ATMarketData> getMarketData() {
	// return _dataMap;
	// }

	public ATMarketData getMarketData(String occId_) {
		ATMarketData mkt = _dataMap.get(occId_);
		if (mkt != null || _blackList.contains(occId_))
			return mkt;
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, occId_);
		Map<String, ATMarketData> map = getMarketData(query);
		mkt = map == null || map.isEmpty() ? null : map.get(occId_);
		if (mkt == null) {
			_blackList.add(occId_);
			log.debug(String.format("getMarketData blacklist[%s]", occId_));
			return null;
		}
		_dataMap.put(occId_, mkt);
		return mkt;
	}

	public Map<String, ATMarketData> getMarketData(IATWhereQuery query_) {
		List<ATMongoMarketData> list = _dao.query(_mktTable, query_, ATMongoMarketData.class);
		if (list == null || list.isEmpty())
			return null;
		Map<String, ATMarketData> map = new HashMap<>();
		for (ATMongoMarketData momkt : list) {
			String occId = momkt.getAtId();
			momkt.put(IATAssetConstants.OCCID, occId);
			ATMarketData mkt = toMarketData(momkt);// new ATMarketData((IATMarketData) momkt);
			_dataMap.put(mkt.getOccId(), mkt);
			map.put(mkt.getOccId(), mkt);
		}
		return map;
	}

	public Map<String, ATMarketData> getOptionUnderlying(IATWhereQuery query_) {
		Function<Document, ATMarketData> f = new Function<Document, ATMarketData>() {
			@Override
			public ATMarketData apply(Document doc_) {
				Document doc = (Document) doc_.get("ul");
				ATMarketData ul = ATAmeritradeUtil.toMarketData(doc);
				return ul;
			}
		};

		Map<String, ATMarketData> result = new HashMap<>();
		List<ATMarketData> list = _dao.query(_mktTableAmeritrade, query_, f);
		if (list != null) {
			for (ATMarketData ul : list) {
				String occId = ul.getOccId();
				result.put(occId, ul);
			}
		}
		return result;
	}

	// public Map<String, ATMarketData> getOptionChain(String symbol_) {
	// String pre = StringUtils.rightPad(symbol_, 6);
	// // _dataMap.g
	// return null;
	// }

	public Map<String, ATMarketData> getChain(String symbol_) {
		return _dataMap.getMap(symbol_, true);
	}

	@Deprecated
	public Map<String, Map<String, Set<ATMarketData>>> getOptionChain(String symbol_, String type_x) {
		Map<String, Map<String, Set<ATMarketData>>> rightMap = new HashMap<>();
		// HashMap<String, List<ATMarketData>> callMap = new HashMap<>();
		// HashMap<String, List<ATMarketData>> putMap = new HashMap<>();

		Map<String, ATMarketData> chainMap = _dataMap.getMap(symbol_, true);
		if (chainMap == null)
			return null;

		Iterator<ATMarketData> i = chainMap.values().iterator();// _dataMap.iterator();
		while (i.hasNext()) {
			ATMarketData mkt = i.next();
			String occId = mkt.getOccId();
			if (occId.length() < 10)
				continue;
			// if (!occId.startsWith(symbol_) || occId.length() < 10)
			// continue;
			// ATMarketData mkt = entry.getValue();
			String right = mkt.getOptionRight();
			Map<String, Set<ATMarketData>> expMap = rightMap.get(right);
			if (expMap == null) {
				expMap = new TreeMap<>();
				rightMap.put(right, expMap);
			}
			String expDs6 = mkt.getExpDS6();
			if (expDs6.length() > 6) {
				log.debug(String.format("expDS6.length > 6 [%s / %s]", symbol_, expDs6));
			}
			// if ("190405C".equals(expDs6)) {
			// log.info("DEBUG");
			// }
			Set<ATMarketData> expList = expMap.get(expDs6);
			if (expList == null) {
				expList = new TreeSet<>();
				expMap.put(expDs6, expList);
			}
			expList.add(mkt);
		}
		return rightMap;
	}

	public Map<String, Map<String, List<ATOption>>> getOptionChain(IATWhereQuery query_, String type_) {
		Function<Document, Map<String, List<ATOption>>> f = new Function<Document, Map<String, List<ATOption>>>() {
			@Override
			public Map<String, List<ATOption>> apply(Document doc_) {
				Map<String, List<ATOption>> result = new HashMap<>();
				if (StringUtils.isBlank(type_) || IATAssetConstants.PUT.equals(type_)) {
					Document puts = (Document) doc_.get("puts");
					if (puts != null) {
						List<ATOption> putList = new ArrayList<>();
						for (String occId : puts.keySet()) {
							Document put = (Document) puts.get(occId);
							ATOption option = ATAmeritradeUtil.toOption(put);
							putList.add(option);
						}
						if (!putList.isEmpty()) {
							result.put(IATAssetConstants.PUT, putList);
						}
					}
				}
				if (StringUtils.isBlank(type_) || IATAssetConstants.CALL.equals(type_)) {
					Document calls = (Document) doc_.get("calls");
					if (calls != null) {
						List<ATOption> callList = new ArrayList<>();
						for (String occId : calls.keySet()) {
							Document put = (Document) calls.get(occId);
							ATOption option = ATAmeritradeUtil.toOption(put);
							callList.add(option);
						}
						if (!callList.isEmpty()) {
							result.put(IATAssetConstants.CALL, callList);
						}
					}
				}
				return result;
			}
		};

		Map<String, Map<String, List<ATOption>>> result = new HashMap<>();
		List<Map<String, List<ATOption>>> list = _dao.query(_mktTableAmeritrade, query_, f);
		// List<Map<String, List<ATOption>>> list = _dao.query(_mktTable, query_, f);
		if (list != null) {
			for (Map<String, List<ATOption>> options : list) {
				for (String right : options.keySet()) {
					Map<String, List<ATOption>> rightMap = result.get(right);
					if (rightMap == null) {
						rightMap = new HashMap<String, List<ATOption>>();
						result.put(right, rightMap);
					}
					for (ATOption option : options.get(right)) {
						String expDS6 = option != null ? option.getExpDS6() : null;
						if (StringUtils.isBlank(expDS6))
							continue;
						List<ATOption> exps = rightMap.get(expDS6);
						if (exps == null) {
							exps = new ArrayList<>();
							rightMap.put(expDS6, exps);
						}
						exps.add(option);
					}
				}
			}
		}
		// List<ATOption> aalTestList = calls.get("210219");// ("AAL 210219C00011000");
		// ATOption aalTest = aalTestList == null ? null
		// : aalTestList.stream().filter(o -> o.getOccId().equals("AAL 210219C00011000")).findAny().get();

		return result;
	}

	private void readMarketDataSync() {
		long start = System.currentTimeMillis();

		// Set<String> testList = new TreeSet<>();
		IATWhereQuery query = ATPersistenceRegistry.query().isNull(IATAssetConstants.OCCID_UNDERLYING);
		List<ATMongoMarketData> market = _dao.query(_mktTable, query, ATMongoMarketData.class);
		for (ATMongoMarketData data : market) {
			String occid = data.getAtId();
			// if(occid.startsWith("JPM")) {
			// testList.add(occid);
			// }
			data.put(IATAssetConstants.OCCID, occid);
			ATMarketData mkt = toMarketData(data);
			_dataMap.put(data.getAtId(), mkt);
		}
		// String s = String.format("testList: %s", testList);
		Duration time = Duration.ofMillis(System.currentTimeMillis() - start);
		log.info(String.format("readMarketDataSync Loaded [%d] market data records in [%s]", market.size(), time));
	}

	// NOTE: Better to handle through usual interface.
	private ATMarketData toMarketData(ATMongoMarketData mongo_) {
		ATMarketData mkt = new ATMarketData((IATMarketData) mongo_);
		mkt.setIV(mongo_.getIV());
		mkt.setOptionDelta(mongo_.getOptionDelta());
		mkt.setOptionGamma(mongo_.getOptionGamma());
		mkt.setOptionRho(mongo_.getOptionRho());
		mkt.setOptionTheta(mongo_.getOptionTheta());
		mkt.setOptionVega(mongo_.getOptionVega());
		mkt.setPxTheo(mongo_.getPxTheo());
		return mkt;
	}

	// @Deprecated // NOTE: We use Kafka now
	// private void monitorMarketData() {
	// ATMongoReactiveDao daoReact = new ATMongoReactiveDao(_config);
	// daoReact.subscribe(new IATMongoListener() {
	//
	// @Override
	// public void notify(Object data_, String operation_, UpdateDescription update_) {
	// if (log.isTraceEnabled()) {
	// log.trace(String.format("Notify Market Data [%s]: %s", operation_, data_));
	// }
	// switch (operation_) {
	// case "ADD":
	// case "INSERT":
	// case "UPDATE":
	// case "REPLACE":
	// Document doc = (Document) data_;
	// ATMarketData mkt = new ATMarketData(doc);
	// String occId = mkt.getOccId();
	// if (occId.length() > 6 || _dataMap.containsKey(occId)) {
	// _dataMap.put(occId, mkt);
	// }
	// notifyListener(mkt);
	// break;
	// default:
	// // NOTE: Market data only supports add, update, replace
	// return;
	// }
	// }
	//
	// @Override
	// public String getCollection() {
	// return _mktTable;
	// }
	// });
	// }

	public void updateCache(ATMarketData mkt_) {
		String occId = mkt_.getOccId();
		// if (occId.length() > 10) {
		// log.debug("DEBUG");
		// }
		_dataMap.put(occId, mkt_);
	}

	// public long save(Collection<IATMarketData> data_) {
	// if (data_ == null || data_.isEmpty()) {
	// log.debug(String.format("No market data to save"));
	// return 0;
	// }
	// try {
	// ArrayList<Document> dataList = new ArrayList<>();
	// for (IATMarketData inData : data_) {
	// ATMongoMarketData outData;
	// if (inData instanceof ATMongoMarketData) {
	// outData = (ATMongoMarketData) inData;
	// } else if (inData instanceof Map<?, ?>) {
	// outData = new ATMongoMarketData();
	// @SuppressWarnings("unchecked")
	// Map<String, Object> inMap = (Map<String, Object>) inData;
	// outData.putAll(inMap);
	// } else {
	// outData = new ATMongoMarketData(inData);
	// }
	// String id = outData.getAtId();
	// if (id == null) {
	// log.error(String.format("Null ID [%s]", outData));
	// continue;
	// }
	// if (id.length() >= 12) {
	// String symbol = id.substring(0, 6).trim();
	// outData.put(IATAssetConstants.OCCID_UNDERLYING, symbol);
	// String dateStr = id.substring(6, 12);
	// outData.put(IATAssetConstants.EXPIRATION_DATE_STR, dateStr);
	// }
	// dataList.add(outData);
	// }
	// long count = _dao.fastBulkInsert(_mktTable, dataList);
	// if (log.isDebugEnabled())
	// log.debug(String.format("Bulk result [%4d/%4d]", count, data_.size()));
	// // long count = _dao.insert(table, dataList.toArray(new ATMongoMarketData[0]));
	// return count;
	// } catch (Exception ex) {
	// log.error(String.format("Error saving market data"), ex);
	// return -1;
	// }
	// }

	public Map<String, List<ATHistoryData>> getHistory(IATWhereQuery query_, List<String> flags_, String avgMode_,
			String hvMode_) {
		List<String> flags = flags_ != null ? flags_ : new ArrayList<>();
		boolean isOHLC = flags.contains("ohlc");
		boolean isAvg = flags.contains("avg");
		boolean isHV = flags.contains("hv");
		boolean isCloseOnly = flags.contains("close") && !flags.contains("ohlc");
		boolean isVolume = flags.contains("vol");
		String period = StringUtils.isBlank(avgMode_) ? null : avgMode_.replaceAll("[^\\d.]", "");
		boolean isMinMax = flags.contains("minmax") && (period != null);
		String modStr = flags.stream().filter(f -> f.startsWith("mod")).map(f -> f.split(":")[1]).findAny().orElse("");

		int mod = NumberUtils.isDigits(modStr) ? Integer.parseInt(modStr) : 1;
		Map<String, Integer> modMap = new HashMap<>();

		Map<String, List<ATHistoryData>> historyMap = new TreeMap<>();
		List<ATPriceHistory> dbHistory = _dao.query(_historyTable, query_, ATPriceHistory.class);
		for (ATPriceHistory history : dbHistory) {
			String occId = history.getOccId();
			List<ATHistoryData> symHistory = historyMap.get(occId);
			if (symHistory == null) {
				symHistory = new ArrayList<>();
				historyMap.put(occId, symHistory);
				modMap.put(occId, 0);
			}
			int modIdx = modMap.put(occId, modMap.get(occId) + 1); // NOTE: Returns prev value
			if (mod != 1 && modIdx % mod != 0)
				continue;

			String dStr = new StringBuilder(history.getDateStr()).insert(6, '-').insert(4, '-').toString();
			ATHistoryData data = new ATHistoryData();
			data.setDateStr(dStr);
			if (isAvg) {
				data.setAvg(history.getAvg(avgMode_));
			}
			if (isOHLC) {
				data.setOpen(history.getOpen());
				data.setHigh(history.getHigh());
				data.setLow(history.getLow());
				data.setClose(history.getClose());
			} else if (isCloseOnly) {
				data.setClose(history.getClose());
			}
			if (isHV) {
				data.setHV(history.getHV(hvMode_));
			}
			if (isMinMax) {
				data.setMax(history.getMax(period));
				data.setMin(history.getMin(period));
			}
			if (isVolume) {
				data.setVolume(history.getVolume());
			}
			symHistory.add(data);
		}
		return historyMap;
	}

	// private void notifyListener(ATMarketData data_) {
	// if (!_isStreaming || _listener == null)
	// return;
	// try {
	// _listener.notify(data_);
	// } catch (Exception ex) {
	// log.error("Error in notify", ex);
	// }
	// }

	private void cleanup(boolean reload_) {
		try {
			String todayStr = ATFormats.DATE_yyMMdd.get().format(new Date());
			long count = _dao.delete(_mktTable,
					ATPersistenceRegistry.query().lt(IATAssetConstants.EXPIRATION_DATE_STR_OLD, todayStr));
			log.info(String.format("Cleaned [%d] market data records", count));
			if (reload_) {
				readMarketDataSync();
			}
		} catch (Exception ex) {
			log.error(String.format("Cleanup error"), ex);
		}
	}

	public void shutdown() {
		log.debug(String.format("Shutting down"));
	}

	// @Override
	public Map<String, String> getMonitoringStatus() {
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("dataMapSize", String.format("%d", _dataMap.getSize()));
		// statusMap.put("dataMapMemory", getDataMapMemory());
		return statusMap;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		return getMonitoringStatus();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	public long store(Collection<ATProtoMarketDataWrapper> wrappers_, String table_) {
		if (wrappers_ == null || wrappers_.isEmpty()) {
			log.debug(String.format("store: No data to save"));
			return 0;
		}
		var table = table_ == null ? _mktTable : table_;
		ATProtoMarketDataWrapper tmp = null;
		try {
			ArrayList<Document> dataList = new ArrayList<>();
			for (ATProtoMarketDataWrapper wrapper : wrappers_) {
				tmp = wrapper;
				if (wrapper.proto().getIsTest())
					continue;
				ATMongoMarketData outData = new ATMongoMarketData(wrapper);
				outData.put(IATLookupMap.INSERT_TIME, System.currentTimeMillis());
				dataList.add(outData);
			}
			long count = _dao.fastBulkUpdate(table, dataList);
			if (log.isDebugEnabled())
				log.debug(String.format("Bulk result [%4d/%4d]", count, wrappers_.size()));
			return count;
		} catch (Exception ex) {
			String id = tmp != null ? tmp.proto().getId() : "n/a";
			log.error(String.format("store Error saving market data [%s]", id), ex);
			return -1;
		}
	}

	public long storeDailyHistory(Collection<ATProtoMarketDataWrapper> wrappers_) {
		if (wrappers_ == null || wrappers_.isEmpty()) {
			log.debug(String.format("store: No data to save"));
			return 0;
		}
		var table = ATMongoTableRegistry.getPositionHistoryTable();
		var dateStr = ATFormats.DATE_yyyy_MM_dd.get().format(new Date());
		ATProtoMarketDataWrapper tmp = null;
		try {
			ArrayList<Document> dataList = new ArrayList<>();
			for (ATProtoMarketDataWrapper wrapper : wrappers_) {
				tmp = wrapper;
				if (wrapper.proto().getIsTest())
					continue;
				ATMongoMarketData outData = new ATMongoMarketData(wrapper);
				var historyId = String.format("%s:%s", wrapper.proto().getId(), dateStr);
				outData.setMongoId(historyId);
				outData.put(IATAssetConstants.DATE_STR, dateStr);
				outData.put(IATLookupMap.INSERT_TIME, System.currentTimeMillis());
				dataList.add(outData);
			}
			long count = _dao.fastBulkInsert(table, dataList);
			if (log.isDebugEnabled())
				log.debug(String.format("Bulk result [%4d/%4d]", count, wrappers_.size()));
			return count;
		} catch (Exception ex) {
			String id = tmp != null ? tmp.proto().getId() : "n/a";
			log.error(String.format("store Error saving market data [%s]", id), ex);
			return -1;
		}
	}

	public static class MarketCache {

		private ConcurrentHashMap<String, Map<String, ATMarketData>> _cache;

		public MarketCache() {
			_cache = new ConcurrentHashMap<>();
		}

		public boolean containsKey(String key_) {
			String key = ATModelUtil.getSymbol(key_);
			Map<String, ATMarketData> keyMap = _cache.get(key);
			boolean r = keyMap != null && keyMap.containsKey(key_);
			return r;
		}

		public Map<String, ATMarketData> getMap(String key_, boolean getPartition_) {
			if (!getPartition_) {
				ATMarketData mkt = this.get(key_);
				Map<String, ATMarketData> result = mkt != null ? Collections.singletonMap(key_, mkt) : null;
				return result;
			} else {
				String key = ATModelUtil.getSymbol(key_);
				Map<String, ATMarketData> keyMap = _cache.get(key);
				Map<String, ATMarketData> result = keyMap != null ? Collections.unmodifiableMap(keyMap) : null;
				return result;
			}
		}

		public ATMarketData get(String key_) {
			String key = ATModelUtil.getSymbol(key_);
			Map<String, ATMarketData> keyMap = _cache.get(key);
			ATMarketData result = keyMap != null ? keyMap.get(key_) : null;
			return result;
		}

		public void put(String key_, ATMarketData value_) {
			if (key_.length() > 10) {
				long startOfToday = LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000;
				Date dExp = ATModelUtil.getExpiration(key_);
				if (dExp.getTime() < startOfToday) {
					if (log.isDebugEnabled()) {
						log.debug(String.format("put: Discarding outdated market data [%s]", key_));
					}
					if (!MarketCacheCleaner.isBusy) {
						ATExecutors.submit(MarketCacheCleaner.class.getSimpleName(), new MarketCacheCleaner(_cache));
					}
					return;
				}
			}

			String key = ATModelUtil.getSymbol(key_);
			Map<String, ATMarketData> keyMap = _cache.get(key);
			if (keyMap == null) {
				keyMap = new ConcurrentSkipListMap<>();// new TreeMap<>();
				_cache.put(key, keyMap);
			}
			keyMap.put(key_, value_);
		}

		public long getSize() {
			long s = _cache.values().stream().mapToInt(c -> c.size()).sum();
			return s;
		}

		public void updateCache(ATMarketData mkt_) {
			String occId = mkt_.getOccId();
			this.put(occId, mkt_);
		}

		public static class MarketCacheCleaner implements Runnable {

			static boolean isBusy = false;

			private Map<String, Map<String, ATMarketData>> _cache;

			public MarketCacheCleaner(Map<String, Map<String, ATMarketData>> cache_) {
				this._cache = cache_;
			}

			@Override
			public void run() {
				// isBusy = false;
				if (isBusy)
					return;
				isBusy = true;
				try {
					log.info(String.format("Running cache cleaner..."));
					Instant start = Instant.now();
					int count = 0;
					String nowDS6 = ATFormats.DATE_yyMMdd.get().format(new Date());
					Iterator<Entry<String, Map<String, ATMarketData>>> symIterator = this._cache.entrySet().iterator();
					while (symIterator.hasNext()) {
						Entry<String, Map<String, ATMarketData>> symEntry = symIterator.next();
						Map<String, ATMarketData> symMap = symEntry.getValue();
						if (symMap != null) {
							Iterator<Entry<String, ATMarketData>> optIterator = symMap.entrySet().iterator();
							while (optIterator.hasNext()) {
								Entry<String, ATMarketData> optEntry = optIterator.next();
								String occId = optEntry.getKey();
								String expDS6 = ATModelUtil.getExpirationDS6(occId);
								if (expDS6 != null && nowDS6.compareTo(expDS6) > 0) {
									optIterator.remove();
									count++;
								}
							}
						}
						if (symMap == null || symMap.isEmpty()) {
							symIterator.remove();
							continue;
						}
					}
					Duration dur = Duration.between(start, Instant.now());
					log.info(String.format("Cleaned [%d] outdated cache entries in [%s]", count, dur));
				} catch (Exception ex) {
					log.error(String.format("Error"), ex);
				} finally {
					isBusy = false;
				}
			}
		}

	}

}
