package com.isoplane.at.marketdata.model;

import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoMarketData extends Document implements IATMarketData, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	public ATMongoMarketData() {
	}

	public ATMongoMarketData(IATMarketData data_) {
		String id = data_.getAtId();
		this.setMongoId(id);
		this.setAtId(id);
		Double pxAsk = data_.getPxAsk();
		if (pxAsk != null && !pxAsk.isNaN())
			this.setPxAsk(pxAsk);
		Integer szAsk = data_.getSzAsk();
		if (szAsk != null)
			this.setSzAsk(szAsk);
		Long tsAsk = data_.getTsAsk();
		if (tsAsk != null)
			this.setTsAsk(tsAsk);
		Double pxBid = data_.getPxBid();
		if (pxBid != null && !pxBid.isNaN())
			this.setPxBid(pxBid);
		Integer szBid = data_.getSzBid();
		if (szBid != null)
			this.setSzBid(szBid);
		Long tsBid = data_.getTsBid();
		if (tsBid != null)
			this.setTsBid(tsBid);
		Double pxLast = data_.getPxLast();
		if (pxLast != null && !pxLast.isNaN())
			this.setPxLast(pxLast);
		Integer szLast = data_.getSzLast();
		if (szLast != null)
			this.setSzLast(szLast);
		Long tsLast = data_.getTsLast();
		if (tsLast != null)
			this.setTsLast(tsLast);
		Long dailyVol = data_.getDailyVol();
		if (dailyVol != null)
			this.setDailyVol(dailyVol);
		Double pxOpen = data_.getPxOpen();
		if (pxOpen != null && !pxOpen.isNaN())
			this.setPxOpen(pxOpen);
		Double pxHigh = data_.getPxHigh();
		if (pxHigh != null && !pxHigh.isNaN())
			this.setPxHigh(pxHigh);
		Double pxLow = data_.getPxLow();
		if (pxLow != null && !pxLow.isNaN())
			this.setPxLow(pxLow);
		Double pxClosePrev = data_.getPxClosePrev();
		if (pxClosePrev != null && !pxClosePrev.isNaN())
			this.setPxClosePrev(pxClosePrev);
		Double pxClose = data_.getPxClose();
		if (pxClose != null && !pxClose.isNaN())
			this.setPxClose(pxClose);
		Long openInt = data_.getOpenInterest();
		if (openInt != null)
			this.setOpenInterest(openInt);
		this.setPxChange(data_.getPxChange());

		if (id.length() > 10) {
			// String symbol = id.substring(0, 6).trim();
			String right = id.substring(12, 13);
			String s = id.substring(13);
			Integer is = Integer.parseInt(s);
			Double strike = is / 1000.0;
			// super.put(IATAssetConstants.OCCID_UNDERLYING, symbol);
			super.put(IATAssetConstants.OPTION_RIGHT, right);
			super.put(IATAssetConstants.PX_STRIKE, strike);
		}
		// Double pxStrike = data_.getPxStrike();
		// if (pxStrike != null && !pxStrike.isNaN())
		// this.setPxStrike(pxStrike);
		// String optionRight = data_.getOptionRight();
		// if (!StringUtils.isBlank(optionRight))
		// this.setOptionRight(optionRight);
	}

	public ATMongoMarketData(ATProtoMarketDataWrapper wrapper_) {
		MarketData mkt = wrapper_.proto();

		String id = mkt.getId();
		this.setMongoId(id);
		this.setAtId(id);

		// >> PX
		if (mkt.hasPxAsk()) {
			this.setPxAsk(mkt.getPxAsk());
		}
		if (mkt.hasPxBid()) {
			this.setPxBid(mkt.getPxBid());
		}
		if (mkt.hasPxClose()) {
			this.setPxClose(mkt.getPxClose());
		}
		if (mkt.hasPxClosePre()) {
			this.setPxClosePrev(mkt.getPxClosePre());
		}
		if (mkt.hasPxDayCh()) {
			this.setPxChange(mkt.getPxDayCh());
		}
		if (mkt.hasPxDayHi()) {
			this.setPxHigh(mkt.getPxDayHi());
		}
		if (mkt.hasPxDayLo()) {
			this.setPxLow(mkt.getPxDayLo());
		}
		if (mkt.hasPxLast()) {
			this.setPxLast(mkt.getPxLast());
		}
		if (mkt.hasPxOpen()) {
			this.setPxOpen(mkt.getPxOpen());
		}
		// << PX
		// >> SZ
		if (mkt.hasSzAsk()) {
			this.setSzAsk(mkt.getSzAsk());
		}
		if (mkt.hasSzBid()) {
			this.setSzBid(mkt.getSzBid());
		}
		if (mkt.hasSzLast()) {
			this.setSzLast(mkt.getSzLast());
		}
		// << SZ
		// >> TS
		if (mkt.hasTsAsk()) {
			this.setTsAsk(mkt.getTsAsk());
		}
		if (mkt.hasTsBid()) {
			this.setTsBid(mkt.getTsBid());
		}
		if (mkt.hasTsLast()) {
			this.setTsLast(mkt.getTsLast());
		}
		// << TS
		// >> OTHER
		if (mkt.hasOpenInt()) {
			this.setOpenInterest(mkt.getOpenInt());
		}
		if (mkt.hasVolDay()) {
			this.setDailyVol(mkt.getVolDay());
		}
		if (mkt.hasIv()) {
			this.setIV(mkt.getIv());
		}
		if (mkt.hasSrc()) {
			this.put(IATAssetConstants.SOURCE, mkt.getSrc());
		}
		// << OTHER

		if (id.length() > 10) {
			String symbol = ATModelUtil.getSymbol(id);
			String right = id.substring(12, 13);
			String s = id.substring(13);
			Integer is = Integer.parseInt(s);
			Double strike = is / 1000.0;
			String dateStr = id.substring(6, 12);
			super.put(IATAssetConstants.OCCID_UNDERLYING, symbol);
			super.put(IATAssetConstants.OPTION_RIGHT, right);
			super.put(IATAssetConstants.PX_STRIKE, strike);
			super.put(IATAssetConstants.EXPIRATION_DATE_STR, dateStr);

			if (mkt.hasPxTheo()) {
				this.setPxTheo(mkt.getPxTheo());
			}
			// >> OPTION
			if (mkt.hasDelta()) {
				this.setOptionDelta(mkt.getDelta());
			}
			if (mkt.hasGamma()) {
				this.setOptionGamma(mkt.getGamma());
			}
			if (mkt.hasRho()) {
				this.setOptionRho(mkt.getRho());
			}
			if (mkt.hasTheta()) {
				this.setOptionTheta(mkt.getTheta());
			}
			if (mkt.hasVega()) {
				this.setOptionVega(mkt.getVega());
			}
			// << OPTION
		}

	}

//	@Deprecated
//	public void prepare() {
//		String atid = getAtId();
//		if (atid == null)
//			return;
//		if (atid.length() <= 10)
//			return;
//		String symbol = atid.substring(0, 6).trim();
//		String right = atid.substring(12, 13);
//		String s = atid.substring(13);
//		Integer is = Integer.parseInt(s);
//		Double strike = is / 1000.0;
//		super.put(IATAssetConstants.OCCID_UNDERLYING, symbol);
//		super.put(IATAssetConstants.OPTION_RIGHT, right);
//		super.put(IATAssetConstants.PX_STRIKE, strike);
//	}

	public void setMongoId(String value_) {
		super.put(MONGO_ID, value_);
	}

	public String getMongoId() {
		return super.getString(MONGO_ID);
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	@Override
	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getUnderlying() {
		return IATMarketData.getUnderlying(this);
	}

	@Override
	public Double getPxLast() {
		return IATMarketData.getPxLast(this);
	}

	public void setPxLast(Double value_) {
		IATMarketData.setPxLast(this, value_);
	}

	@Override
	public Integer getSzLast() {
		return IATAsset.getSzLast(this);
	}

	public void setSzLast(Integer value_) {
		IATAsset.setSzLast(this, value_);
	}

	@Override
	public Long getTsLast() {
		return IATMarketData.getTsLast(this);
	}

	public void setTsLast(Long value_) {
		IATMarketData.setTsLast(this, value_);
	}

	@Override
	public Double getPxAsk() {
		return IATMarketData.getPxAsk(this);
	}

	public void setPxAsk(Double value_) {
		IATMarketData.setPxAsk(this, value_);
	}

	@Override
	public Long getTsAsk() {
		return IATMarketData.getTsAsk(this);
	}

	public void setTsAsk(Long value_) {
		IATMarketData.setTsAsk(this, value_);
	}

	@Override
	public Integer getSzAsk() {
		return IATAsset.getSzAsk(this);
	}

	public void setSzAsk(Integer value_) {
		IATAsset.setSzAsk(this, value_);
	}

	@Override
	public Double getPxBid() {
		return IATMarketData.getPxBid(this);
	}

	public void setPxBid(Double value_) {
		IATMarketData.setPxBid(this, value_);
	}

	@Override
	public Long getTsBid() {
		return IATMarketData.getTsBid(this);
	}

	public void setTsBid(Long value_) {
		IATMarketData.setTsBid(this, value_);
	}

	@Override
	public Integer getSzBid() {
		return IATAsset.getSzBid(this);
	}

	public void setSzBid(Integer value_) {
		IATAsset.setSzBid(this, value_);
	}

	@Override
	public Long getDailyVol() {
		return IATMarketData.getDailyVol(this);
	}

	public void setDailyVol(Long value_) {
		IATMarketData.setDailyVol(this, value_);
	}

	@Override
	public Double getPxOpen() {
		return IATMarketData.getPxOpen(this);
	}

	public void setPxOpen(Double value_) {
		IATMarketData.setPxOpen(this, value_);
	}

	@Override
	public Double getPxClose() {
		return IATMarketData.getPxClose(this);
	}

	public void setPxClose(Double value_) {
		IATMarketData.setPxClose(this, value_);
	}

	@Override
	public Double getPxClosePrev() {
		return IATMarketData.getPxClosePrev(this);
	}

	public void setPxClosePrev(Double value_) {
		IATMarketData.setPxClosePrev(this, value_);
	}

	@Override
	public Double getPxHigh() {
		return IATMarketData.getPxHigh(this);
	}

	public void setPxHigh(Double value_) {
		IATMarketData.setPxHigh(this, value_);
	}

	@Override
	public Double getPxLow() {
		return IATMarketData.getPxLow(this);
	}

	public void setPxLow(Double value_) {
		IATMarketData.setPxLow(this, value_);
	}

	@Override
	public Long getOpenInterest() {
		return IATMarketData.getOpenInterest(this);
	}

	public void setOpenInterest(Long value_) {
		IATMarketData.setOpenInterest(this, value_);
	}

	@Override
	public Double getPxStrike() {
		return IATMarketData.getPxStrike(this);
	}

	public void setPxStrike(Double value_) {
		IATAsset.setPxStrike(this, value_);
	}

	@Override
	public Double getPxChange() {
		return IATMarketData.getPxChange(this);
	}

	public void setPxChange(Double value_) {
		IATMarketData.setPxChange(this, value_);
	}

	public Double getPxTheo() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_THEORETICAL);
	}

	public void setPxTheo(Double value_) {
		super.put(IATAssetConstants.PX_THEORETICAL, value_);
	}

	// @Override
	// public Double getPxYield() {
	// return IATMarketData.getPxYield(this);
	// }

	@Override
	public String getOptionRight() {
		return IATMarketData.getOptionRight(this);
	}

	public void setOptionRight(String value_) {
		IATMarketData.setOptionRight(this, value_);
	}

	public Double getOptionDelta() {
		return IATMarketData.getOptionDelta(this);
	}

	public void setOptionDelta(Double value_) {
		IATMarketData.setOptionDelta(this, value_);
	}

	public Double getOptionGamma() {
		return IATMarketData.getOptionGamma(this);
	}

	public void setOptionGamma(Double value_) {
		IATMarketData.setOptionGamma(this, value_);
	}

	public Double getOptionRho() {
		return IATMarketData.getOptionRho(this);
	}

	public void setOptionRho(Double value_) {
		IATMarketData.setOptionRho(this, value_);
	}

	public Double getOptionTheta() {
		return IATMarketData.getOptionTheta(this);
	}

	public void setOptionTheta(Double value_) {
		IATMarketData.setOptionTheta(this, value_);
	}

	public Double getOptionVega() {
		return IATMarketData.getOptionVega(this);
	}

	public void setOptionVega(Double value_) {
		IATMarketData.setOptionVega(this, value_);
	}

	public Double getIV() {
		return IATMarketData.getIV(this);
	}

	public void setIV(Double value_) {
		IATMarketData.setIV(this, value_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public <T> T getMeta(String key_) {
		return IATMarketData.getMeta(this, key_);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

}
