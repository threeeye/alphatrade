
"use strict";

// node google-sheets.js -p 8081
// npm install -g typescript

var ip = require('ip');
var express = require('express');        // call express
var bodyParser = require('body-parser');
const commandLineArgs = require('command-line-args');

var mbc = require('./dist/components/mc-google-sheets-connector');
//import gsc from './dist/components/mc-google-sheets-connector';

const optionDefinitions = [
    { name: 'port', alias: 'p', type: Number }
]

const options = commandLineArgs(optionDefinitions);

var gsc = new mbc.MobuddyCash.MCGoogleSheetsConnector();

var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var port = options.port || 8080;        // set our port
var router = express.Router();          // get an instance of the express Router

router.get('/', function (req, res) {
    res.json({ message: 'hooray! welcome to our api!' });
});

router.get('/pf', function (req, res) {
    var pf = gsc.getPf();
    res.json(pf);
});

app.use('/api/v1', router);

app.listen(port);
gsc.init();
//var gsc = new mbc();
//gsc.init();

process.on('uncaughtException', function (err) {
    console.log("Uncaught Exception: " + err);
});
process.on('SIGINT', function () {
    gsc.shutdown();
    process.exit();
});

console.log('###');
console.log('### Server running [%s:%s]', ip.address(), port);
console.log('###');
