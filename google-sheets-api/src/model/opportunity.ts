var dateFormat = require('dateformat');
var util = require('util');
//var printf = require('printf');

export class Opportunity {

    date: Date;
    days: number;
    flags: string;
    industry: string;
    isActive: Boolean;
    label: string;
    meta: string;
    occId: string;
    periodicity: string;
    priceAsk: number;
    priceAvg: number;
    priceBid: number;
    priceClose: number;
    priceMid: number;
    probability: number;
    safetyMgn: number;
    scoreExternal: number;
    scoreInternal: number;
    strategy: string;
    strikeA: number;
    strikeB: number;
    symbol: string;
    timestamp: Date;
    yieldAbs: number;
    yieldPctA: number;

    //   _spreadDateStr: string;

    constructor(obj_: any) {
        if (obj_ != null) {
            for (var i in obj_) {
                this[i] = obj_[i];
            }
        }
    }

    // ⚪🔵🔴☝ 💡 💰👍⭐✨⚡⚓☀☑☠💎⏱🔎🚩🏴🏳🔮🏁💢🛑🌀🔔⛔🚫✍☕
    static getAsArray = (type_: string, opportunity_: Opportunity) => { //: any[]
        var row = [];
        var flags = opportunity_.flags && opportunity_.flags != null && opportunity_.flags.length > 0 ? opportunity_.flags : ' ';
        var meta = opportunity_.meta && opportunity_.meta != null && opportunity_.meta.length > 0 ? opportunity_.meta : null;
        var metaJson = meta != null ? JSON.parse(meta) : null;
        switch (type_) {
            case 'naked':
                switch (opportunity_.strategy) {
                    case 'H':
                        var rangeStr = metaJson != null && metaJson.range != null ? metaJson.range : ' ';
                        var pxChange = 0;
                        if (opportunity_.priceClose && opportunity_.priceClose != 0) {
                            var pxChange = 100 * (opportunity_.priceMid - opportunity_.priceClose) / opportunity_.priceClose;
                            // pxChangeStr = printf('%+.2f%%', pxChange);
                        }
                        row.push(opportunity_.isActive ? '🌀' : ' '); // ⚪
                        row.push(opportunity_.label);
                        row.push(' ');
                        row.push(opportunity_.priceMid);
                        row.push(pxChange != 0 ? pxChange : ' ');
                        row.push(' ');
                        row.push(opportunity_.priceAvg);
                        row.push(' ');
                        row.push(' ');
                        row.push(' ');
                        row.push(' ');
                        row.push(' ');
                        row.push(opportunity_.periodicity);
                        //   row.push(meta);
                        row.push(rangeStr);
                        row.push('');
                        row.push(flags);
                        row.push(opportunity_.industry);
                        row.push('https://snapshot.fidelity.com/fidresearch/snapshot/landing.jhtml#/research?symbol=' + opportunity_.symbol);
                        break;
                    case 'CC':
                    case 'C':
                    case 'P':
                        var earnArray: string[] = metaJson != null && metaJson.earn != null ? metaJson.earn : null;
                        var earnStr: string = earnArray != null ? earnArray.join(', ') : ' ';
                        var divArray: string[] = metaJson != null && metaJson.div != null ? metaJson.div : null;
                        var divStr = divArray != null ? divArray.join(', ') : ' ';
                        row.push(opportunity_.strategy);
                        row.push(opportunity_.label);
                        row.push(opportunity_.priceBid);
                        row.push(opportunity_.priceMid);
                        row.push(opportunity_.priceAsk);
                        row.push(opportunity_.yieldPctA);
                        row.push(opportunity_.yieldAbs);
                        row.push(opportunity_.safetyMgn);
                        row.push(' ');
                        row.push(opportunity_.days);
                        row.push(opportunity_.scoreInternal);
                        row.push(opportunity_.scoreExternal);
                        row.push(' ');
                        row.push(earnStr);
                        row.push(divStr);
                        row.push(flags);
                        row.push(' ');//JSON.stringify(metaJson));
                        row.push(' ');
                        break;
                }
                break;
            case 'spread':
                switch (opportunity_.strategy) {
                    case 'H':
                    var rangeStr = metaJson != null && metaJson.range != null ? metaJson.range : ' ';
                    var noteStr = metaJson != null && metaJson.note != null ? metaJson.note : ' ';
                    var pxChange = 0;
                        if (opportunity_.priceClose && opportunity_.priceClose != 0) {
                            var pxChange = 100 * (opportunity_.priceMid - opportunity_.priceClose) / opportunity_.priceClose;
                        }
                        var updStr = dateFormat(opportunity_.timestamp, "HH:MM:ss");
                        row.push(opportunity_.isActive ? '✅' : ' '); // ⚪🌀
                        row.push(opportunity_.label);
                        row.push(opportunity_.priceMid);
                        row.push(pxChange != 0 ? pxChange : ' ');
                        row.push(' ');
                        row.push(opportunity_.priceAvg);
                        row.push(' ');
                        row.push(' ');
                        row.push(' ');
                        row.push(flags);
                        row.push(updStr);
                        row.push('https://snapshot.fidelity.com/fidresearch/snapshot/landing.jhtml#/research?symbol=' + opportunity_.symbol);
                        row.push(noteStr);
                        row.push('');//   row.push(opportunity_.periodicity);
                        //   row.push(meta);
                    //    row.push(' '); //row.push(rangeStr);
                    //    row.push('');
                    //    row.push(flags);
                    //    row.push(' '); // row.push(opportunity_.industry);
                    //    break;
                    case 'S':
                   // console.log("meta: " + metaJson);
                    var rangeStr = metaJson != null && metaJson.range != null ? metaJson.range : ' ';
                    var theoStr = metaJson != null && metaJson.theo != null ? metaJson.theo : ' ';
                    var earnStr: string = metaJson != null && metaJson.earn != null ? metaJson.earn : ' ';
                        var prbStr = opportunity_.probability != null ? opportunity_.probability * 100.0 : ' ';
                        //    var str = opportunity_.occIdSell.substr(0, 12).trim();
                        var dateStr = dateFormat(opportunity_.date, "yymmdd");
                        row.push(opportunity_.strategy);
                        row.push(dateStr);
                        row.push(opportunity_.strikeB);
                        row.push(opportunity_.strikeA);
                        row.push(opportunity_.strikeB - opportunity_.strikeA);
                        //          row.push(' ');
                        row.push(opportunity_.safetyMgn * 100.0);
                        row.push(prbStr);
                        row.push(opportunity_.yieldPctA * 100.0);
                        row.push(opportunity_.yieldAbs);
                        row.push(flags);
                        row.push(rangeStr);
                        row.push(earnStr);
                        row.push(theoStr);
                    //    row.push(' ');
                    //    row.push(' ');
                    //    row.push(' ');
                        //  row.push(meta);
                        break;
                }
                break;
        }
        return row;
    }

    static getArrayDescription = (type_: string) => {
        var row = [];
        switch (type_) {
            case 'naked':
                row.push(' ');
                row.push('Action...');
                row.push('Bid');
                row.push('Mid');
                row.push('Ask');
                row.push('Yd %');
                row.push('Yd $');
                row.push('Margin');
                row.push(' ');
                row.push('Days');
                row.push('Score');
                row.push('Rank');
                row.push(' ');
                //        row.push('Meta');
                row.push('Earnings');
                row.push('Dividend');
                row.push(' ');
                row.push('Industry');
                break;
            case 'spread':
                row.push(' ');
                row.push('Action...');
                row.push('Sell');
                row.push('Buy');
                row.push('Spread');
                row.push('Margin');
                row.push('Prb %');
                row.push('Yd %');
                row.push('Px $');
                row.push(' ');
                row.push(' ');
                row.push(' ');
                row.push(' ');
                row.push(' ');
                row.push(' ');
                row.push(' ');
                break;
        }
        return row;
    }

}
