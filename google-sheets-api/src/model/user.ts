
export class User {

    email: string;
    sheetId: string;
    permissions: string[];
  //  ideasTime: number;
  //  authClient: any;

    constructor(email_: string, sheetId_: string, permissions_: string[]) {
        if (email_ == null || email_.length < 1 || sheetId_ == null || sheetId_.length < 1 || permissions_ == null || permissions_.length < 1) {
            throw new TypeError('Invalid user');
        } else {
            this.email = email_;
            this.sheetId = sheetId_;
            this.permissions = permissions_;
        }
    }
}