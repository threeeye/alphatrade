
export class Note {

    _atid: string;
    note: string;

    constructor(note_: any) {
        //   if('string' === (typeof note_))
        var note: string = note_ != null ? note_.toString() : null;
        //   console.info('note1: ' + typeof note_);
        //   console.info('note2: ' + typeof note_.toString());
        var text: string[] = note != null && note.length > 2 ? note.split('¶') : null;
        if (text == null) {
            throw new TypeError('Invalid note: ' + note + ' - ' + note.length);
        }
        this._atid = text[0];
        this.note = text[1];
    }

}