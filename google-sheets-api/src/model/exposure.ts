
export class Exposure {

    occId: string;
    callAmount: number;
    putAmount: number;
    equityAmount: number;
    totalAmount: number;
    meta: string;

    static getHeader = () => {
        var row = ['Symbol', 'Calls', 'Puts', 'Equity', 'Total'];
        return row;
    }

    static getAsArray = (exp_: Exposure) => {
        var row = [exp_.occId];
        row.push(exp_.callAmount != null && exp_.callAmount != 0 ? exp_.callAmount + '' : ' ');
        row.push(exp_.putAmount != null && exp_.putAmount != 0 ? exp_.putAmount + '' : ' ');
        row.push(exp_.equityAmount != null && exp_.equityAmount != 0 ? exp_.equityAmount + '' : ' ');
        row.push(exp_.totalAmount != null && exp_.totalAmount != 0 ? exp_.totalAmount + '' : ' ');
        row.push(exp_.meta != null ? exp_.meta : ' ');
        return row;
    }

}