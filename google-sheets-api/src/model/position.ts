import { Security } from './security';


var dateFormat = require('dateformat');
var oneDay = 24 * 60 * 60 * 1000;

export class Position {

    index: number;
    tradeDate: Date;
    tradeTotal: number;
    count: number;
    price: number;
    tradeType: number;
    broker: string;
    ticker: string;
    lastUpdate: Date;
    dateEarnings: Date;
    dateDividend: Date;
    strike: number;
    margin: number;
    risk: number;
    value: number;
    changePct: number;
    security: Security;

    constructor(label_: string, index_: number) {
        if (label_ == null || label_.length < 1) {
            throw new TypeError('Invalid label');
        } else {
            var tokens = label_.split(',');
            this.index = index_;
            this.ticker = tokens[0].toUpperCase();
            this.count = +tokens[2];
            this.price = +tokens[3];
            this.tradeType = +tokens[4];
            this.broker = tokens[5];
            this.tradeTotal = +tokens[6];
            this.security = new Security(this.ticker);
            var dateTokens = tokens[1].split("-");
            if (dateTokens.length > 3) {
                var timeTokens = dateTokens[3].split(":");
                this.tradeDate = new Date(+dateTokens[0], +dateTokens[1] - 1, +dateTokens[2], +timeTokens[0], +timeTokens[1]);
            } else {
                this.tradeDate = new Date(+dateTokens[0], +dateTokens[1] - 1, +dateTokens[2]);
            }
        }
    }

    static getArrayDescription = () => {
        var row = ['Px', 'Px UL', 'Value', 'Expiry', 'Earn', 'Div', 'Mrgn', 'Risk', 'Exp(d)', 'Ch%']; //, 'Strike'
        return row;
    }

    static getAsArray = (pos_: Position, now_: Date) => {
        var security = pos_.security;
        var priceUnderlying = security.priceUnderlying == null ? '' : security.priceUnderlying;
        var earnDateStr = pos_.dateEarnings && pos_.dateEarnings != null ? dateFormat(pos_.dateEarnings, 'yyyy-mm-dd') : '';
        var divDateStr = pos_.dateDividend && pos_.dateDividend != null ? dateFormat(pos_.dateDividend, 'yyyy-mm-dd') : '';
   //     var strike = security.strike == null ? '' : security.strike;
        var margin = pos_.margin == null ? '' : pos_.margin;
        var risk = pos_.risk == null ? '' : pos_.risk;
        var value = pos_.value == null ? '' : pos_.value;
        var expiryDateStr = security.expirationDate == null ? '' : dateFormat(security.expirationDate, 'yyyy-mm-dd');
        var expiryDays = security.expirationDate == null ? '' : 1 + Math.round((security.expirationDate.getTime() - now_.getTime()) / oneDay);
        var changePct = pos_.changePct == null ? '' : pos_.changePct;

        var row = [security.price, priceUnderlying, value, expiryDateStr, earnDateStr, divDateStr, margin, risk, expiryDays, changePct]; // ,strike
        return row;
    }

}