
import { Position } from './position';

export class UserData {

    ideasTime: number;
    rollTime: number;
    resetTime: number;
    spreadTime: number;
    lastOpportunitySize: number;
    lastSpreadSize: number;
    portfolio: Position[];
    authClient: any;
    exposureHash: string;
    
    constructor() {
        this.ideasTime = (new Date()).getTime();
        this.rollTime = this.ideasTime;
        this.resetTime = this.ideasTime;
        this.spreadTime = this.ideasTime;
        this.lastOpportunitySize = 10000;
        this.lastSpreadSize = 10000;
        this.exposureHash = '';
    };
}