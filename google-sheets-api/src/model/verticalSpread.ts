import { spawn } from "child_process";

var dateFormat = require('dateformat');
var util = require('util');
//var printf = require('printf');

export class VerticalSpread {

    timestamp: Date;
    occIdStock: string;
    occIdBuy: string;
    occIdSell: string;
    expirationDate: Date;
    right: string;
    strikeBuy: number;
    strikeSell: number;
    priceSpread: number;
    priceStock: number;
    apy: number;
    safetyMargin: number;
    flags: string;
    meta: string;

    constructor(obj_: any) {
        if (obj_ != null) {
            for (var i in obj_) {
                this[i] = obj_[i];
            }
        }
    }

    // ⚪🔵🔴☝ 💡 💰👍⭐✨⚡⚓☀☑☠💎⏱🔎🚩🏴🏳🔮🏁💢🛑🌀🔔⛔🚫✍☕
    static getAsArray = (spread_: VerticalSpread) => { //: any[]
        var row = [];
        switch (spread_.right) {
            default:
                var str = spread_.occIdSell.substr(0, 12).trim();
                var dateStr = dateFormat(spread_.expirationDate, "yyyy-mm-dd");
                row.push(spread_.right);
                row.push(spread_.occIdStock);
                row.push(dateStr);
                row.push(spread_.strikeBuy);
                row.push(spread_.strikeSell);
                row.push(spread_.priceStock);
                row.push(spread_.safetyMargin * 100.0);
                row.push(spread_.apy * 100.0);
                row.push(spread_.priceSpread);
                row.push(spread_.flags);
                row.push(spread_.meta);
                break;
        }
        return row;
    }

    static getArrayDescription = () => {
        var row = [];
        row.push('');
        row.push('Action...');
        row.push('Date');
        row.push('Buy');
        row.push('Sell');
        row.push('Px (u)');
        row.push('Margin');
        row.push('Yd %');
        row.push('Px $');
        row.push(' ');
        row.push(' ');
        return row;
    }

}
