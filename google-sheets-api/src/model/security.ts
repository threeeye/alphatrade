var dateFormat = require('dateformat');

export class Security {
    tickerId: string;
    label: string;
    symbol: string;
    secType: string;
    price: number;
    strike: number;
 //   optExpDate: Date;
    optRight: string;
    priceUnderlying: number;
    lastUpdate: Date;
    meta: string;
    expirationDate: Date;

    constructor(label_: any) {
        if (label_ == null) {
            throw new TypeError('NULL input');
        } else if ('string' === (typeof label_) && label_.length > 0) {
            var label = label_.trim();
            if (label.length < 7) {
                //  this.tickerId = null;
                this.label = label;
                this.symbol = label;
                this.secType = 'S';
                this.price = null;
                this.lastUpdate = new Date();
            } else {
                var idx = 5;// label.indexOf(' ');
                //                var symbol = label.substr(0, idx).trim();
                var symbol = label.substr(0, 5).trim();
                //var remaining = label.substr(symbol.length, label.length).trim();
                //idx = remaining.indexOf(' ');
                // var dateStr = remaining.substr(0, idx).trim();
                var dateStr = label.substr(5, 6).trim();
                var date = this.convertToDate(dateStr);
                //    remaining = remaining.substr(dateStr.length, remaining.length).trim();
                //    idx = remaining.indexOf(' ');
                //    var right = remaining.substr(0, idx).trim();
                var right = label.charAt(12);
                //    var valueStr = remaining.substr(right.length, remaining.length).trim();
                var valueStr = label.substr(13).trim();
                var value = Number(valueStr) * 1.0;
                //   var result: Security = {
                //  this.tickerId = null;
                this.label = label;
                this.symbol = symbol;
                this.secType = 'O';
                this.expirationDate = date;
                this.optRight = right;
                this.strike = value;
                this.price = null;
                this.lastUpdate = new Date();
                //     }
            }
            this.tickerId = this.generateTickerId(this);
        } else {
            this.label = label_.label;
            this.priceUnderlying = label_.priceUnderlying;
            this.strike = label_.strike;
            //            console.log('strike: ' + label_.strike);
        }
    }


    private convertToDate = function (dateStr_: string) {
        var d1 = 2000 + Number(dateStr_.substr(0, 2));
        var d2 = Number(dateStr_.substr(2, 2)) - 1;
        var d3 = Number(dateStr_.substr(4, 2));
        var date = new Date(d1, d2, d3, 0, 0, 0, 0);
        return date;
    };

    private generateTickerId = function (sec_: Security) {
        if (sec_ == null)
            return null;

        if (sec_.secType === "O") {
            var sym = sec_.symbol;
            for (var i = sym.length; i < 6; i++) {
                sym = sym + " ";
            }
            var strike = sec_.strike * 1000 + '';
            for (var i = strike.length; i < 8; i++) {
                strike = "0" + strike;
            }
            var dateStr = dateFormat(sec_.expirationDate, "yymmdd");
            var result = sym + dateStr + sec_.optRight + strike;
            return result;
        } else {
            return sec_.symbol;
        }
    }
}
