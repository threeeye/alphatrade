

export class Configuration {

    type: string;
    alertTriggers: string[];
    baseSafetyMargin: number;
    blacklistSymbols: string[];
    boundaryMargin: number;
    boundaryRange: number;
    extraSymbols: string[];
    ideasTime: number;
    ideaTriggers: string[];
    isNotifications: boolean;
    maxOptionDays: number;
    minOptionDays: number;
    minApy: number;
    minPx: number;
    optionFlags: string[];
    ownerId: string;
    processPeriod: number;
    pricingPeriod: number;
    rollTime: number;
    resetTime: number;
    resetCode: string;
    riskFreeInterest: number;
    saveBulkSize: number;
    sheetId: string;
    spreadSymbols: string[];
    spreadTime: number;
    timeDecay: number;

    constructor(type_: string, settings_: string[]) {
        if (settings_ == null || settings_.length < 1) {
            throw new TypeError('Invalid settings');
        } else if ('system' == type_) {
            var i = 0;
            this.riskFreeInterest = +settings_[i++];
            // var oflgs = settings_[i++] + "";
            // if (oflgs != null && oflgs.length > 0) {
            //     oflgs = oflgs.toUpperCase();
            //     this.optionFlags = oflgs.split(',');
            // }
            this.minOptionDays = +settings_[i++];
            this.maxOptionDays = +settings_[i++];
        //    this.baseSafetyMargin = +settings_[i++];
        //    this.minApy = +settings_[i++];
            this.timeDecay = +settings_[i++];
            this.boundaryMargin = +settings_[i++];
            this.boundaryRange = +settings_[i++];
            this.saveBulkSize = +settings_[i++];
            this.processPeriod = +settings_[i++];
            this.pricingPeriod = +settings_[i++];
            var nStr = settings_[i++];
            this.isNotifications = nStr != 'FALSE';
            //            i++; // Visual break
        //    this.rollTime = +settings_[i++];
        //    this.ideasTime = +settings_[i++];
            var resetStr = settings_[i++] + '';
            if (resetStr.length > 0) {
                var tokens = resetStr.split(":");
                this.resetTime = +tokens[0];
                this.resetCode = tokens[1];
                //     console.log('reset str [%s] - [%s]', this.resetTime, this.resetCode);
            }
            var xs = settings_[i++] + "";
            if (xs != null && xs.length > 0) {
                xs = xs.toUpperCase();
                this.extraSymbols = xs.split(',');
            }
            var bs = settings_[i++] + "";
            if (bs != null && bs.length > 0) {
                bs = bs.toUpperCase();
                this.blacklistSymbols = bs.split(',');
            }
            var ss = settings_[i++] + "";
            if (ss != null && ss.length > 0) {
                ss = ss.toUpperCase();
                this.spreadSymbols = ss.split(',');
            }
            // var it = settings_[i++] + "";
            // if (it != null && it.length > 0) {
            //     this.ideaTriggers = it.split(',');
            // }
            // var at = settings_[i++] + "";
            // if (at != null && at.length > 0) {
            //     this.alertTriggers = at.split(',');
            // }
//            this.ownerId = settings_[i++];
     //       this.sheetId = settings_[i++];
            //  console.log('roll: ' + this.rollTime);
        } else if ('user' == type_) {
            var i = 0;
       //     this.riskFreeInterest = +settings_[i++];
            var oflgs = settings_[i++] + "";
            if (oflgs != null && oflgs.length > 0) {
                oflgs = oflgs.toUpperCase();
                this.optionFlags = oflgs.split(',');
            }
            this.minOptionDays = +settings_[i++];
            this.maxOptionDays = +settings_[i++];
            this.baseSafetyMargin = +settings_[i++];
            this.minApy = +settings_[i++];
            this.minPx = +settings_[i++];
            //   this.timeDecay = +settings_[i++];
         //   this.boundaryMargin = +settings_[i++];
         //   this.boundaryRange = +settings_[i++];
         //   this.saveBulkSize = +settings_[i++];
         //   this.processPeriod = +settings_[i++];
         //   this.pricingPeriod = +settings_[i++];
            var nStr = settings_[i++];
            this.isNotifications = nStr != 'FALSE';
            //            i++; // Visual break
            this.rollTime = +settings_[i++];
            this.ideasTime = +settings_[i++];
            this.spreadTime = +settings_[i++];
          //  var resetStr = settings_[i++] + '';
            // if (resetStr.length > 0) {
            //     var tokens = resetStr.split(":");
            //     this.resetTime = +tokens[0];
            //     this.resetCode = tokens[1];
            //     //     console.log('reset str [%s] - [%s]', this.resetTime, this.resetCode);
            // }
            var xs = settings_[i++] + "";
            if (xs != null && xs.length > 0) {
                xs = xs.toUpperCase();
                this.extraSymbols = xs.split(',');
            }
            var bs = settings_[i++] + "";
            if (bs != null && bs.length > 0) {
                bs = bs.toUpperCase();
                this.blacklistSymbols = bs.split(',');
            }
            var it = settings_[i++] + "";
            if (it != null && it.length > 0) {
                this.ideaTriggers = it.split(',');
            }
            var at = settings_[i++] + "";
            if (at != null && at.length > 0) {
                this.alertTriggers = at.split(',');
            }
            var ss = settings_[i++] + "";
            if (ss != null && ss.length > 0) {
                ss = ss.toUpperCase();
                this.spreadSymbols = ss.split(',');
            }
            this.ownerId = settings_[i++];
            this.sheetId = settings_[i++];
            //  console.log('roll: ' + this.rollTime);
        }
        this.type = type_;
    }
}