
import { MCUtil } from '../components/mc-util';

var google = require('googleapis');
var dateFormat = require('dateformat');

var _sheets = google.sheets('v4');

export module MCGoogleSpreadsheet {

    export function readSpreadsheet(authClient_: any, sheetId_: string, range_: string) {
        // console.log('%s - readSpreadsheet: %s, %s', MCUtil.logDate(), sheetId_, range_);
        var promise = new Promise(function (resolve_, reject_) {
            _sheets.spreadsheets.values.get({
                auth: authClient_,
                spreadsheetId: sheetId_,
                range: range_,
            }, function (err_: any, response_: any) {
                if (err_) {
                    reject_('Google API error: ' + err_);
                } else if (response_ == null || response_.values == null) {
                    reject_('No records found');
                } else {
                    var rows = response_.values;
                    resolve_(rows);
                }
            });
        });
        return promise;
    }

    export function writeSpreadsheet(authClient_: any, sheetId_: string, range_: string, data_: any, isClear_: boolean, isCols_: boolean) {
        var dimension = isCols_ ? "COLUMNS" : "ROWS";
        var promise = new Promise((resolve_, reject_) => {
            clearRange(authClient_, sheetId_, range_, isClear_)
                .then((val_) => {
                    _sheets.spreadsheets.values.update({
                        auth: authClient_,
                        spreadsheetId: sheetId_,
                        range: range_,
                        valueInputOption: 'USER_ENTERED',
                        resource: {
                            majorDimension: dimension,
                            values: data_
                        }
                    }, (err_: any, response_: any) => {
                        if (err_) {
                            reject_('Google API error: ' + err_);
                        } else {
                           //      console.log('response: ' + response_);
                            resolve_(response_);
                        }
                    });
                })
                .catch((err_) => {
                    console.log('Error from range clear: ' + err_);
                    reject_(err_);
                });
        });
        return promise;
    }

    function clearRange(authClient_: any, sheetId_: string, range_: string, isClear_: boolean): Promise<boolean> {
        if (!isClear_) {
       //     console.log('No range clear');
            return Promise.resolve(true);
        }
     //   console.log('clearing range');
        var promise = new Promise<boolean>((resolve_, reject_) => {
            _sheets.spreadsheets.values.clear({
                auth: authClient_,
                spreadsheetId: sheetId_,
                range: range_
            }, (err_: any, response_: any) => {
                if (err_) {
             //       console.log('range clear error: ' + err_);
                    //   console.log('%s - The API returned an error: %s', logDate(), err);
                    reject_(err_);
                } else {
              //      console.log('range cleared');
                    resolve_(true);
                }
            });
        });
        return promise;
    }

}