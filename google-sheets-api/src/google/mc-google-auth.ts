
import * as fs from "fs";
import { MCUtil } from './../components/mc-util';
import { User } from '../model/user';

var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

export module MCGoogleAuth {

    export class MCGoogleAuth {

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
        private SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
        private TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
        private TOKEN_PATH = this.TOKEN_DIR;
        private TOKEN_POSTFIX = 'sheets.googleapis.token.json';
        //        private TOKEN_PATH = this.TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json';
        private CLIENT_FILE = 'client_secret.json';

        private _authClient: any;
        private _sheets = google.sheets('v4');
        private _period = 60000;
        private _counter = 0;


        public getAuthClient(user_: User): Promise<{}> {
            //          if (user_ == null)
            //              return Promise.resolve(null);
            var tokenPath = user_ == null ? this.TOKEN_PATH + this.TOKEN_POSTFIX : this.TOKEN_PATH + user_.email + "_" + this.TOKEN_POSTFIX;
            var promise = this.loadCredentials(tokenPath)
            return promise;
        }

        private loadCredentials(tokenPath_: string): Promise<{}> {
            var promise = new Promise<any>((resolve_, reject_) => {
                var clientSecret = fs.readFileSync(this.CLIENT_FILE, 'UTF8');
                var credentials = JSON.parse(clientSecret);
                console.log('%s - Loaded client secret file', MCUtil.logTimestamp());
                this.authorize(credentials, tokenPath_)
                    .then((msg_: string) => {
                        console.log("%s - %s", MCUtil.logTimestamp(), msg_);
                        resolve_(this._authClient);
                    })
                    .catch((err_: any) => {
                        console.error('%s - Error authorizing access: %s', MCUtil.logTimestamp(), err_);
                        reject_('Error authenticating');
                    });
            });
            return promise;
        }

        /**
         * Create an OAuth2 client with the given credentials, and then execute the
         * given callback function.
         *
         * @param {Object} credentials_ The authorization client credentials.
         */
        private authorize(credentials_: any, tokenPath_: string) {
            var clientSecret = credentials_.installed.client_secret;
            var clientId = credentials_.installed.client_id;
            var redirectUrl = credentials_.installed.redirect_uris[0];
            var auth = new googleAuth();
            this._authClient = new auth.OAuth2(clientId, clientSecret, redirectUrl);
            var promise = new Promise((resolve_, reject_) => {
                // Check if we have previously stored a token.
                fs.readFile(tokenPath_, (err_: any, token_: any) => {
                    if (err_) {
                        this.getNewToken(this._authClient, tokenPath_).then(function () {
                            resolve_('New client credentials generated');
                        });
                    } else {
                        this._authClient.credentials = JSON.parse(token_);
                        resolve_('Loaded client credentials [' + tokenPath_ + ']');
                    }
                });
            });
            return promise;
        }

        /**
         * Get and store new token after prompting for user authorization, and then
         * execute the given callback with the authorized OAuth2 client.
         *
         * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
         */
        private getNewToken(authClient_: any, tokenPath_: string): Promise<string> {
            var authUrl = authClient_.generateAuthUrl({
                access_type: 'offline',
                scope: this.SCOPES
            });
            console.log('Authorize this app by visiting this url: ', authUrl);
            var rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout
            });
            var promise = new Promise<string>((resolve_, reject_) => {
                rl.question('Enter the code from that page here: ', (code_: string) => {
                    rl.close();
                    authClient_.getToken(code_, (err_: any, token_: string) => {
                        if (err_) {
                            reject_('Error while trying to retrieve access token: ' + err_);
                        } else {
                            authClient_.credentials = token_;
                            this.storeToken(token_, tokenPath_);
                            resolve_('');
                        }
                    });
                });
            });
            return promise;
        }

        /**
         * Store token to disk be used in later program executions.
         *
         * @param {Object} token The token to store to disk.
         */
        private storeToken(token_: string, tokenPath_: string): void {
            try {
                fs.mkdirSync(this.TOKEN_DIR);
            } catch (err) {
                if (err.code != 'EEXIST') {
                    throw err;
                }
            }
            fs.writeFile(tokenPath_, JSON.stringify(token_));
            console.log('%s - Token stored to: %s', MCUtil.logTimestamp(), tokenPath_);
        }

    }
}