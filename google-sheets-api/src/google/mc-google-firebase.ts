import { MCUtil } from './../components/mc-util';

var firebase = require("firebase-admin");

export module MCGoogleFirebase {

    export class MCGoogleFirebase {

        private SERVICE_ACCOUNT = "saltnpepa-6c410-firebase-adminsdk-4hmst-8da43ba797.json";
        //  private _firebase: any;

        public init() {
            firebase.initializeApp({
                credential: firebase.credential.cert(this.SERVICE_ACCOUNT),
                databaseURL: "https://saltnpepa-6c410.firebaseio.com"
            });
            console.log('%s - Firebase initialized', MCUtil.logTimestamp());
            // console.log('fb init: ' + firebase);
            // var db = firebase.database();
            // var ref = db.ref('users');
            // ref.once("value", function (snapshot_: any) {
            //     console.log(snapshot_.val());
            // });

            this.message();
        }


        private message() {
            var dbkey = this.emailToFirebaseKey('mironroth@gmail.com');

            var db = firebase.database();
            var ref = db.ref('users/' + dbkey);
            ref.once("value", function (snapshot_: any) {
                var user = snapshot_ != null ? snapshot_.val() : null;
                var tokens = user != null ? user.notification_tokens : null;
                if (tokens == null || tokens.length == 0)
                    return;
                console.log(snapshot_.val());

                var payload = {
                    data: {
                        score: "850",
                        time: "2:45"
                    }
                };

                // Send a message to the device corresponding to the provided registration token.
                firebase.messaging().sendToDevice(tokens, payload)
                    .then(function (response_: any) {
                        // See the MessagingDevicesResponse reference documentation for
                        // the contents of response.
                        console.log("Successfully sent message: ", response_);
                    })
                    .catch(function (error_: any) {
                        console.log("Error sending message: ", error_);
                    });
            });



        }

        private emailToFirebaseKey(emailAddress_: string): string {
            return emailAddress_.replace(/[.]/g, '%20');
        }


    }
}