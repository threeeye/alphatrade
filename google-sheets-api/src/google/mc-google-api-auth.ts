
import { MCUtil } from './../components/mc-util';

//var Promise = require('promise');
var fs = require('fs');
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');

//var mcUtil = require('./mc-util');


// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json
var SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
var TOKEN_DIR = (process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE) + '/.credentials/';
var TOKEN_PATH = TOKEN_DIR + 'sheets.googleapis.com-nodejs-quickstart.json';

var _authClient: any;
var _sheets = google.sheets('v4');
var _period = 60000;
var _counter = 0;


exports.getAuthClient = function () {
    var promise = loadCredentials()
    return promise;
}

function loadCredentials() {
    var promise = new Promise(function (resolve_, reject_) {
        // Load client secrets from a local file.
        fs.readFile('client_secret.json', function processClientSecrets(err_: any, content_: string) {
            if (err_) {
                reject_('Error loading client secret file: ' + err_);
            } else {
                // Authorize a client with the loaded credentials
                var credentials = JSON.parse(content_);
                console.log('%s - Loaded client secret file', MCUtil.logTimestamp());
                authorize(credentials).then(function (msg_) {
                    console.log("%s - %s", MCUtil.logTimestamp(), msg_);
                    resolve_(_authClient);
                });
            }
        });
    });
    return promise;
}

/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 *
 * @param {Object} credentials_ The authorization client credentials.
 */
function authorize(credentials_: any) {
    var clientSecret = credentials_.installed.client_secret;
    var clientId = credentials_.installed.client_id;
    var redirectUrl = credentials_.installed.redirect_uris[0];
    var auth = new googleAuth();
    _authClient = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    var promise = new Promise(function (resolve_, reject_) {
        // Check if we have previously stored a token.
        fs.readFile(TOKEN_PATH, function (err_: any, token_: any) {
            if (err_) {
                getNewToken(_authClient).then(function () {
                    resolve_('New client credentials generated');
                });
            } else {
                _authClient.credentials = JSON.parse(token_);
                resolve_('Loaded client credentials');
            }
        });
    });
    return promise;
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 *
 * @param {google.auth.OAuth2} oauth2Client The OAuth2 client to get token for.
 */
function getNewToken(authClient_: any): Promise<any> {
    var authUrl = authClient_.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    var promise = new Promise((resolve_, reject_) => {
        rl.question('Enter the code from that page here: ', function (code_: string) {
            rl.close();
            authClient_.getToken(code_, function (err_: any, token_: string) {
                if (err_) {
                    reject_('Error while trying to retrieve access token: ' + err_);
                } else {
                    authClient_.credentials = token_;
                    storeToken(token_);
                    resolve_();
                }
            });
        });
    });
    return promise;
}

/**
 * Store token to disk be used in later program executions.
 *
 * @param {Object} token The token to store to disk.
 */
function storeToken(token_: string) {
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    fs.writeFile(TOKEN_PATH, JSON.stringify(token_));
    console.log('%s - Token stored to: %s', MCUtil.logTimestamp(), TOKEN_PATH);
}

