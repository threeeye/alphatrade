import { Security } from '../model/security';

var dateFormat = require('dateformat');

export module MCTickerUtil {
/*
    export function tickerToObject(label_: string, index_: number): Security {
        if (label_ == null || label_.length < 1) {
            return null;
        } else {
            var label = label_.trim();
            var idx = label.indexOf(' ');
            if (idx == -1) {
                var result: Security = {
                    tickerId: label,
                    label: label,
                    symbol: label,
                    secType: 'S',
                    price: null,
                    index: index_,
                    lastUpdate: new Date(),
                    optStrike: null,
                    optExpDate: null,
                    optRight: null,
                }
                return result;
            } else {
                var symbol = label.substr(0, idx).trim();
                var remaining = label.substr(symbol.length, label.length).trim();
                idx = remaining.indexOf(' ');
                var dateStr = remaining.substr(0, idx).trim();
                var date = convertToDate(dateStr);
                remaining = remaining.substr(dateStr.length, remaining.length).trim();
                idx = remaining.indexOf(' ');
                var right = remaining.substr(0, idx).trim();
                var valueStr = remaining.substr(right.length, remaining.length).trim();
                var value = Number(valueStr) * 1.0;
                var result: Security = {
                    tickerId: null,
                    label: label,
                    symbol: symbol,
                    secType: 'O',
                    optExpDate: date,
                    optRight: right,
                    optStrike: value,
                    price: null,
                    index: index_,
                    lastUpdate: new Date()
                }
                result.tickerId = genetateTickerId(result);
                return result;
            }
        }
    }
*/

    function convertToDate(dateStr_: string) {
        var d1 = 2000 + Number(dateStr_.substr(0, 2));
        var d2 = Number(dateStr_.substr(2, 2)) - 1;
        var d3 = Number(dateStr_.substr(4, 2));
        var date = new Date(d1, d2, d3, 0, 0, 0, 0);
        return date;
    };

    // function generateTickerId(symbol_: Security) {
    //     if (symbol_ == null)
    //         return null;

    //     if (symbol_.secType === "O") {
    //         var strike = symbol_.strike * 1000 + '';
    //         for (var i = strike.length; i < 8; i++) {
    //             strike = "0" + strike;
    //         }
    //         var dateStr = dateFormat(symbol_.optExpDate, "yymmdd");
    //         var result = symbol_.symbol + dateStr + symbol_.optRight + strike;
    //         return result;
    //     } else {
    //         return symbol_.symbol;
    //     }
    // }

}