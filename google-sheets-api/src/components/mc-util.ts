var dateFormat = require('dateformat');

export module MCUtil {
    export function logTimestamp() {
        return createLogTimestamp();
    }

    export function getObject(json_: any) {
        if (json_ == null || json_.length < 1)
            return null;
        try {
            var obj = JSON.parse(json_);
            return obj;
        } catch (ex_) {
            console.log('%s - getObject Error: %s - %s', MCUtil.logTimestamp(), json_, ex_);
            return null;
        }
    }

    // exports.log = function (msg_: string) {
    //     var logdate = createLogDate();
    //     var str = `${logdate} - ${msg_}`;
    //     console.log(str);
    // }

    function createLogTimestamp() {
        var now = new Date();
        var str = dateFormat(now, "yyyy-mm-dd HH:MM:ss")
        return str;
    }

    function formatDateShort(date_: Date) {
        if (!date_ || date_ == null) return null;
        var str = dateFormat(date_, "yyyy-mm-dd")
        return str;
    }



}