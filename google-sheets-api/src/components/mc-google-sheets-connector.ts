
"use strict";

var dateFormat = require('dateformat');
var util = require('util');

import { Configuration } from '../model/configuration';
import { MCGoogleAuth } from '../google/mc-google-auth';
import { MCGoogleFirebase } from '../google/mc-google-firebase';
import { MCGoogleSpreadsheet } from '../google/mc-google-sheets';
import { MCSourceAdapter } from '../adapter/mc-source-adapter';
import { MCUtil } from './mc-util';
import { Note } from '../model/note';
import { Opportunity } from '../model/opportunity';
import { Position } from '../model/position';
import { Security } from '../model/security';
import { User } from '../model/user';
import { UserData } from '../model/userData';
import { VerticalSpread } from '../model/verticalSpread';
import { Exposure } from '../model/exposure';

// SAVE (Ctrl+S) to build!
export module MobuddyCash {
    export class MCGoogleSheetsConnector {

        private _portfolioReadRange: string = '2018!ApiTicker';
        private _portfolioWriteRange: string = 'PositionOutputRange';
        //https://docs.google.com/spreadsheets/d/1j4xV-HmJ-_laGsigs_EbWraKFIesNezi-SJNKfzah1w/edit?usp=sharing
        //   private _watchlistReadRange: string = "InputRange";// "API_input!A:A";// 'WatchlistRead';
        private _watchlistWriteRange: string = "IdeaOutputRange";
        private _systemSettingsRange: string = "System!SystemConfig";
        private _userSettingsRange: string = "UserConfig";
        private _rollReadRange: string = "RollInputRange";
        private _rollWriteRange: string = "RollOutputRange";
        private _spreadWriteRange: string = "SpreadOutputRange";
        private _exposureRange: string = "ExposureRange";
        private _portfolioTimestampOutputCell: string = 'PortfolioTimestampOutputCell';

        //    private _authClient: any;
        private _readSettingsPeriod: number = 10000;
        private _readPortfolioPeriod = 60000;
        private _writePortfolioPeriod: number = this._readPortfolioPeriod + 5000;
        private _ideasPeriod: number = 5000;
        private _exposurePeriod: number = 60000;
        //   private _configuration: Configuration = null; // TODO: Can be localized

        private _userDataMap: Map<string, UserData> = new Map<string, UserData>();

        private _users: User[];
        private _auth = new MCGoogleAuth.MCGoogleAuth();
        private _firebase = new MCGoogleFirebase.MCGoogleFirebase();

        constructor() { }

        public init(): void {
            this._firebase.init();
            this.readUsers();
            this._auth.getAuthClient(null)
                .then((authClient_: any) => {
                    this.readSettings();
                    this.readPortfolio();
                    this.writePortfolio();
                //    this.updateExposure();
                    //       this.updateIdeas();
                })
                .catch((err_: any) => {
                    console.log('%s - Error loading client secret file: %s', MCUtil.logTimestamp(), err_);
                });
        }

        public shutdown() {
            console.log('%s - Shutting down', MCUtil.logTimestamp());
        }

        private readSettings() {
            console.log("%s - Read settings period [%s] ms", MCUtil.logTimestamp(), this._readSettingsPeriod);
            setInterval(() => {
                this.readUsers();
                if (this._users == null || this._users.length == 0)
                    return;
                this._users.forEach(u => {
                    try {
                 //       console.log('Settings: ' + u.permissions);
                        var userData = this._userDataMap.get(u.email);
                        if (userData == null || userData.authClient == null)
                            return;
                        ['user', 'system'].forEach(t => {
                            if ('system' == t && u.permissions.indexOf('system') < 0)
                                return;
                            var range = 'system' == t ? this._systemSettingsRange : this._userSettingsRange;
                            MCGoogleSpreadsheet.readSpreadsheet(userData.authClient, u.sheetId, range)
                                .then((settings_: string[]) => {
                                    if (settings_ == null || settings_.length < 3) {
                                        console.log('%s - [%s] Configuration is empty [%s]', MCUtil.logTimestamp(), t, u.email);
                                        return;
                                    } else {
                                        //  console.log('settings: ' + settings_);
                                        var config = new Configuration(t, settings_);
                                        if (config.ideasTime > userData.ideasTime) {
                                            userData.ideasTime = config.ideasTime;
                                            this.handleIdeas(u);
                                        }
                                        if (config.rollTime > userData.rollTime) {
                                            userData.rollTime = config.rollTime;
                                            this.handleRoll(u);
                                        }
                                        if (config.spreadTime > userData.spreadTime) {
                                            userData.spreadTime = config.spreadTime;
                                            this.handleSpreads(u);
                                        }
                                        MCSourceAdapter.updateConfiguration(config, u)
                                            .then((res_: boolean) => {
                                                if (res_) {
                                                    console.log('%s - [%s] Configuration changed [%s]', MCUtil.logTimestamp(), t, u.email);
                                                }
                                            })
                                            .catch((error_) => {
                                                console.error("Error updating [%s] configuration [%s]: %s", t, u.email, error_);
                                            });
                                        // if (u.permissions.indexOf('configuration') < 0)
                                        //     return;
                                        if (config.resetTime > userData.resetTime) {
                                            userData.resetTime = config.resetTime;
                                            MCSourceAdapter.reset(config.resetCode, u);
                                        }
                                    }
                                })
                                .catch((err_: any) => {
                                    console.log("%s - Error reading [%s] settings [%s]: %s", MCUtil.logTimestamp(), t, u.email, err_);
                                });
                        })
                    } catch (ex_) {
                        console.log("%s - Error handling settings [%s]: %s", MCUtil.logTimestamp(), u.email, ex_);
                    }
                });
            }, this._readSettingsPeriod);
        }

        private readUsers() {
            try {
        //        console.log('Reading users...');
                MCSourceAdapter.getUsers()
                    .then((users_) => {
                        if (users_ == null || users_.length == 0) {
                            console.log('%s - No users', MCUtil.logTimestamp());
                            return;
                        }
                        console.log('%s - User count [%s]', MCUtil.logTimestamp(), users_.length);
                        users_.forEach(newU => {
                            var user = this._users == null ? null : this._users.find(oldU => oldU.email == newU.email);
                            if (user == null) {
                                this._userDataMap.set(newU.email, new UserData());
                                this._auth.getAuthClient(newU)
                                    .then((authClient_: any) => {
                                        this._userDataMap.get(newU.email).authClient = authClient_;
                                        //    newU.authClient = authClient_;
                                    });
                                //       this._ideasTimeMap.set(newU.email, (new Date()).getTime());
                                //        this._rollTimeMap.set(newU.email, (new Date()).getTime());
                                //        this._resetTimeMap.set(newU.email, (new Date()).getTime());
                                //   } else {
                                //       newU.authClient = user.authClient;
                            }
                            //      console.log('u: %s - %s - %s', newU.email, newU.ideasTime, newU.authClient);
                        });
                        for (let email of Array.from(this._userDataMap.keys())) {
                            if (users_.find(u => u.email == email) == null) {
                                console.log("%s - Removing user data [%s]", MCUtil.logTimestamp(), email);
                                this._userDataMap.delete(email);
                            }
                        }
                        this._users = users_;
                    })
                    .catch((error_) => {
                        console.error("Error updating users: %s", error_);
                    });
            } catch (ex_) {
                console.log("%s - Error reading users: %s", MCUtil.logTimestamp(), ex_);
            }
        }

        private readPortfolio() {
            console.log("%s - Read portfolio period [%s] ms", MCUtil.logTimestamp(), this._readPortfolioPeriod);
            setInterval(() => {
                if (this._users == null || this._users.length == 0)
                    return;
                this._users.forEach(u => {
                    var userData = this._userDataMap.get(u.email);
                    if (userData == null)
                        return;
                    if (u.permissions.indexOf('portfolio') < 0) {
                        //     console.log("%s - Skipping 'readPortfolio' [%s]", MCUtil.logDate(), u.email);
                        return;
                    }
                    try {
                        MCGoogleSpreadsheet.readSpreadsheet(userData.authClient, u.sheetId, this._portfolioReadRange)
                            .then((tickers_: string[]) => {
                                // First entries are header
                                if (tickers_ == null || tickers_.length < 3) {
                                    console.log('%s - Portfolio is empty [%s]', MCUtil.logTimestamp(), u.email);
                                    return;
                                } else {
                                    console.log('%s - Portfolio size [%s - %s]', MCUtil.logTimestamp(), u.email, tickers_.length);
                                    var newPortfolio: Position[] = [];
                                    // Convert to objects
                                    for (var i = 2; i < tickers_.length; i++) {
                                        var label = tickers_[i][0];
                                        if (label == null || label.length < 1)
                                            continue;
                                        var pos = new Position(label, i);
                                        newPortfolio.push(pos);
                                    }
                                    MCSourceAdapter.updatePortfolio(newPortfolio, u)
                                        .then((positions_) => {
                                            console.log('%s - Processed [%s - %s] positions', MCUtil.logTimestamp(), u.email, positions_.length);
                                            userData.portfolio = positions_;
                                        })
                                        .catch((error_) => {
                                            console.error("%s - Error updating portfolio [%s]: %s", MCUtil.logTimestamp(), u.email, error_);
                                        });
                                }
                            })
                            .catch((err_: any) => {
                                console.log("%s - Error reading portfolio [%s]: %s", MCUtil.logTimestamp(), u.email, err_);
                            });
                    } catch (ex_) {
                        console.log("%s - Error reading portfolio [%s]: %s", MCUtil.logTimestamp(), u.email, ex_);
                    }
                });
            }, this._readPortfolioPeriod);
        }

        private writePortfolio() {
            console.log("%s - Write portfolio period [%s] ms", MCUtil.logTimestamp(), this._writePortfolioPeriod);
            setInterval(() => {
                if (this._users == null || this._users.length == 0)
                    return;
                this._users.forEach(u => {
                    var userData = this._userDataMap.get(u.email);
                    if (userData == null)
                        return;
                    if (u.permissions.indexOf('portfolio') < 0) {
                        //      console.log("%s - Skipping 'writePortfolio' [%s]", MCUtil.logDate(), u.email);
                        return;
                    }
                    try {
                        var portfolio = userData.portfolio;// this._portfolioMap.get(u.email);
                        if (portfolio == null)
                            return;
                        var writeData = this.generatePortfolioOutput(portfolio, u);
                        if (writeData != null) {
                            MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, u.sheetId, this._portfolioWriteRange , writeData, false, false) 
                                .then((msg_: string) => {
                                    MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, u.sheetId, this._portfolioTimestampOutputCell , [[MCUtil.logTimestamp()]], false, false) 
                                    .then((msg_: string) => {
                                 //       console.log('%s - Portfolio timestamp write complete [%s]', MCUtil.logTimestamp(), u.email);
                                    })
                                    .catch((err_: any) => {
                                        console.error("%s - Error writing portfolio timestamp [%s]: %s", MCUtil.logTimestamp(), u.email, err_);
                                    });    
                                    console.log('%s - Portfolio write complete [%s]', MCUtil.logTimestamp(), u.email);
                                })
                                .catch((err_: any) => {
                                    console.error("%s - Error writing portfolio [%s]: %s", MCUtil.logTimestamp(), u.email, err_);
                                });
                        }
                    } catch (ex_) {
                        console.log("%s - Error writing portfolio [%s]: %s", MCUtil.logTimestamp(), u.email, ex_);
                    }
                });
            }, this._writePortfolioPeriod);
        }

        private generatePortfolioOutput(portfolio_: Position[], user_: User) {
            if (portfolio_ == null || portfolio_.length == 0) {
                console.log('%s - No positions to write [%s]', MCUtil.logTimestamp(), user_.email);
                return;
            } else {
                console.log('%s - [%s] position updates [%s]', MCUtil.logTimestamp(), portfolio_.length, user_.email);
            }
            // Check for updates since last write
            var length = 0;
            for (var i = 0; i < portfolio_.length; i++) {
                var position = portfolio_[i];
                if (position.index > length) {
                    length = position.index;
                }
            }
            //    this._lastWriteTime = new Date();
            var writeData: any[][] = [[]];
            writeData[0] = [];// Position.getArrayDescription();// ['Px', 'Px UL', 'Earn', 'Div', 'Strike', 'Mrgn', 'Risk', 'Value'];
            writeData[1] =[];// [MCUtil.logTimestamp(), '', '', '', '', ''];
            for (var i = 2; i <= length; i++) {
                writeData.push(['', '', '', '', '', '', '']);
            }
            var now = new Date();
       //     now.setHours(0, 0, 0);
            for (var i = 0; i < portfolio_.length; i++) {
                var position = portfolio_[i];
                var security = position.security;
                if (security != null) {
                    writeData[position.index] = Position.getAsArray(position, now);
                }
                //        console.log('%s - sym: %s - %s', mcUtil.logDate(), sym.ticker, sym.price);
            }
            return writeData;
        }

        private updateExposure() {
            console.log("%s - Exposure update period [%s] ms", MCUtil.logTimestamp(), this._exposurePeriod);
            setInterval(() => {
                if (this._users == null || this._users.length == 0)
                    return;
                this._users.forEach(user_ => {
                    var userData = this._userDataMap.get(user_.email);
                    if (userData == null)
                        return;
                    if (user_.permissions.indexOf('portfolio') < 0) {
                        //     console.log("%s - Skipping 'readPortfolio' [%s]", MCUtil.logDate(), u.email);
                        return;
                    }
                    try {
                        MCSourceAdapter.getExposure(user_)
                            .then((exposures_) => {
                                console.log('%s - Retrieved [%s - %s] exposures', MCUtil.logTimestamp(), user_.email, exposures_.length);
                                //    userData.portfolio = positions_;
                                var grid: any[] = [];
                                var header = Exposure.getHeader();
                                header.push(dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'));
                                grid.push(header);
                                var hash = '';
                                exposures_.forEach(exp_ => {
                                    grid.push(Exposure.getAsArray(exp_));
                                    hash += exp_.occId + exp_.callAmount + exp_.putAmount + exp_.equityAmount;
                                });
                                var userData = this._userDataMap.get(user_.email);
                                if (userData.exposureHash === hash) {
                                    //      console.log("No exposure change");
                                    return;
                                }
                                userData.exposureHash = hash;
                                for (var i = 0; i < 100; i++) {
                                    var row: any[] = [];
                                    row.push(' ');
                                    row.push(' ');
                                    row.push(' ');
                                    row.push(' ');
                                    grid.push(row);
                                }
                                MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, user_.sheetId, this._exposureRange, grid, true, false);
                                console.log('%s - Exposures done [%s]', MCUtil.logTimestamp(), user_.email);
                            })
                            .catch((error_) => {
                                console.error("%s - Error updating exposure [%s]: %s", MCUtil.logTimestamp(), user_.email, error_);
                            });
                    } catch (ex_) {
                        console.log("%s - Error updating exposure [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
                    }
                });
            }, this._exposurePeriod);
        }

        private handleRoll(user_: User) {
            var userData = this._userDataMap.get(user_.email);
            if (userData == null)
                return;
            if (user_.permissions.indexOf('portfolio') < 0) {
                console.log("%s - Skipping 'handleRoll' [%s]", MCUtil.logTimestamp(), user_.email);
                return;
            }
            console.log('%s - Handling Rolls [%s]', MCUtil.logTimestamp(), user_.email);
            try {
                MCGoogleSpreadsheet.readSpreadsheet(userData.authClient, user_.sheetId, this._rollReadRange)
                    .then((tickers_: string[]) => {
                        // First entries are header
                        if (tickers_ == null || tickers_.length < 0) {
                            console.log('%s - Rolls are empty', MCUtil.logTimestamp());
                            return;
                        } else {
                            tickers_.sort();
                            var rolls: Security[] = [];
                            // Convert to objects
                            for (var i = 0; i < tickers_.length; i++) {
                                var label = tickers_[i][0];
                                if (label == null || label.length < 1)
                                    continue;
                                //            console.log('Roll [%s]', label);
                                var sec = new Security(label);
                                rolls.push(sec);
                            }
                            MCSourceAdapter.getRolls(rolls, user_)
                                .then((b_: string[][]) => {
                                    this.writeRolls(b_, user_);
                                })
                                .catch((error_) => {
                                    console.error("%s - Error getting rolls [%s]: %s", MCUtil.logTimestamp(), user_.email, error_);
                                });
                        }
                    })
                    .catch((err_: any) => {
                        console.log("%s - Error reading rolls [%s]: %s", MCUtil.logTimestamp(), user_.email, err_);
                    });
            } catch (ex_) {
                console.log("%s - Error handling rolls [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
            }
        }

        private writeRolls(rolls_: string[][], user_: User) {
            var userData = this._userDataMap.get(user_.email);
            if (userData == null)
                return;
            if (user_.permissions.indexOf('portfolio') < 0) {
                console.log("%s - Skipping 'writeRolls' [%s]", MCUtil.logTimestamp(), user_.email);
                return;
            }
            MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, user_.sheetId, this._rollWriteRange, rolls_, true, false);
            console.log('%s - Rolls done [%s]', MCUtil.logTimestamp(), user_.email);
        }

        private handleIdeas(user_: User) {
            //     console.log("%s - Handling ideas [%s]", MCUtil.logDate(), user_.email);
            // setInterval(() => {
            //     if (this._users == null || this._users.length == 0)
            //         return;
            //     this._users.forEach(u => {
            try {
                var userData = this._userDataMap.get(user_.email);
                if (userData == null)
                    return;
                if (user_.permissions.indexOf('ideas') < 0) {
                    console.log("%s - Not authorized 'handleIdeas' [%s]", MCUtil.logTimestamp(), user_.email);
                    return;
                }
                console.log("%s - Requesting ideas [%s]", MCUtil.logTimestamp(), user_.email);
                //     this._ideasTimeMap.set(user_.email, (new Date()).getTime());
                MCSourceAdapter.getOpportunities(user_)
                    .then((opportunities_: Opportunity[]) => {
                        //        console.log('got opportunities: ' + opportunities_.length);
                        var writeData = this.generateOpportunityOutput('naked', opportunities_);
                        if (writeData != null && writeData.length != 0) {
                            var userData = this._userDataMap.get(user_.email);
                            var isClear = userData.lastOpportunitySize > writeData.length;
                            userData.lastOpportunitySize = writeData.length;
                            // var isClear = this._lastOpportunitySize > writeData.length;
                            // this._lastOpportunitySize = writeData.length;
                            console.log("%s - [%d] new ideas [%s]", MCUtil.logTimestamp(), writeData.length, user_.email);
                            MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, user_.sheetId, this._watchlistWriteRange, writeData, isClear, false)
                                .then((msg_: string) => {
                                    console.log('%s - Idea write complete [%s]', MCUtil.logTimestamp(), user_.email);
                                })
                                .catch((err_: any) => {
                                    console.error("%s - Error writing ideas [%s]: %s", MCUtil.logTimestamp(), user_.email, err_);
                                });
                        } else {
                            console.log("%s - No new ideas [%s]", MCUtil.logTimestamp(), user_.email);
                        }
                    })
                    .catch((ex_: any) => {
                        console.log("%s - Error writing ideas [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
                    });
            } catch (ex_) {
                console.log("%s - Error uploading ideas [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
            }
            //     });
            // }, this._ideasPeriod);
        }

        private generateOpportunityOutput(type_: string, opos_: Opportunity[]) {
            if (opos_ == null || opos_.length == 0) {
                console.log('%s - No opportunities to write', MCUtil.logTimestamp());
                return;
            }
            var grid: any[] = [];
            var desc = Opportunity.getArrayDescription(type_);
            desc.push(dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'));
            grid.push(desc);
            opos_.some((opo_: Opportunity) => {
                var row = Opportunity.getAsArray(type_, opo_);
                if (row != null && row.length > 0) {
                    grid.push(row);
                }
                if (grid.length >= 999) {
                    grid.push(['Throttled...']);
                    console.log('%s - Throttling grid: %d', MCUtil.logTimestamp(), grid.length);
                    return true;
                }
            });
            return grid;
        }

        private handleSpreads(user_: User) {
            try {
                var userData = this._userDataMap.get(user_.email);
                if (userData == null)
                    return;
                if (user_.permissions.indexOf('ideas') < 0) {
                    console.log("%s - Not authorized 'handleSpreads' [%s]", MCUtil.logTimestamp(), user_.email);
                    return;
                }
                console.log("%s - Requesting spreads [%s]", MCUtil.logTimestamp(), user_.email);
                MCSourceAdapter.getSpreads(user_)
                    .then((spreads_: Opportunity[]) => {
                        //        console.log('got opportunities: ' + opportunities_.length);
                        var writeData = this.generateOpportunityOutput('spread', spreads_);
                        if (writeData != null && writeData.length != 0) {
                            var userData = this._userDataMap.get(user_.email);
                            var isClear = userData.lastSpreadSize > writeData.length;
                            userData.lastSpreadSize = writeData.length;
                            // var isClear = this._lastOpportunitySize > writeData.length;
                            // this._lastOpportunitySize = writeData.length;
                            console.log("%s - [%d] new spreads [%s]", MCUtil.logTimestamp(), writeData.length - 1, user_.email);
                            MCGoogleSpreadsheet.writeSpreadsheet(userData.authClient, user_.sheetId, this._spreadWriteRange, writeData, isClear, false)
                                .then((msg_: string) => {
                                    console.log('%s - Spread write complete [%s]', MCUtil.logTimestamp(), user_.email);
                                })
                                .catch((err_: any) => {
                                    console.error("%s - Error writing spreads [%s]: %s", MCUtil.logTimestamp(), user_.email, err_);
                                });
                        } else {
                            console.log("%s - No new spreads [%s]", MCUtil.logTimestamp(), user_.email);
                        }
                    })
                    .catch((ex_: any) => {
                        console.log("%s - Error writing spreads [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
                    });

            } catch (ex_) {
                console.log("%s - Error uploading spreads [%s]: %s", MCUtil.logTimestamp(), user_.email, ex_);
            }
        }

        private generateSpreadOutput(spreads_: VerticalSpread[]) {
            if (spreads_ == null || spreads_.length == 0) {
                console.log('%s - No spreads to write', MCUtil.logTimestamp());
                return;
            }
            var grid: any[] = [];
            var desc = VerticalSpread.getArrayDescription();
            desc.push(dateFormat(new Date(), 'yyyy-mm-dd HH:MM:ss'));
            grid.push(desc);
            spreads_.some((spread_: VerticalSpread) => {
                var row = VerticalSpread.getAsArray(spread_);
                if (row != null && row.length > 0) {
                    grid.push(row);
                }
                if (grid.length >= 999) {
                    grid.push(['Throttled...']);
                    console.log('%s - Throttling grid: %d', MCUtil.logTimestamp(), grid.length);
                    return true;
                }
            });
            return grid;
        }

    }

}

module.exports.MobuddyCash = MobuddyCash;
