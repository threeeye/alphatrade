
//import * as  WebRequest from 'web-request';
import { Security } from '../model/security';
import { MCUtil } from '../components/mc-util';

//var Promise = require('promise');
var https = require('https');
//var http = require('http');
//var dateFormat = require('dateformat');

//var mcUtil = require('./mc-util');
export module MCTradierBroker {

    export function updatePrices(symbols_: Security[]): Promise<number> {
        if (symbols_ == null || symbols_.length == 0)
            return Promise.resolve(null);

        var query = symbols_.map((el) => {
            if (el != null && el.secType != null)
                return el.tickerId;
        }).join(',');
        var path = '/v1/markets/quotes?symbols=' + query;
        //     console.log('%s - getTradier path: %s', MCUtil.logDate(), path);
        var options = {
            hostname: 'sandbox.tradier.com',
            port: 443,
            path: path,
            //  method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Authorization': 'Bearer LvsUw1I2flrLb2eTO5S4PeOhAkey'
            },
            timeout: 30000
        };
        var promise = new Promise<number>((resolve_, reject_) => {
            https.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    //      console.log('%s - getTradier end: %s', MCUtil.logDate(), body);
                    var data = getObject(body);
                    if (data == null || data.quotes == null || data.quotes.quote == null) {
                        resolve_(0);
                    } else if (data.error != null) {
                        console.log('%s - getTradier Error: %s', MCUtil.logTimestamp(), data.error);
                        resolve_(-1);
                    } else {
                        var count = 0;
                        var quotes = Array.isArray(data.quotes.quote) ? data.quotes.quote : [data.quotes.quote];
                        quotes.some((quote_: any) => {
                            symbols_.some((sym_: Security) => {
                                if (quote_.symbol === sym_.tickerId) {
                                    //            console.log('%s - found: %s', MCUtil.logDate(), quote_.symbol);
                                    var price = quote_.bid == 0 && quote_.volume == 0 ? quote_.last : (quote_.ask + quote_.bid) / 2.0;
                                    sym_.price = price;
                                    count++;
                                    return true;
                                }
                            });
                        });
                        resolve_(count);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getTradier Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
            //   req.end();
        });
        return promise;
    }

    function getObject(json_: any) {
        if (json_ == null || json_.length < 1)
            return null;
        try {
            var obj = JSON.parse(json_);
            return obj;
        } catch (ex_) {
            console.log('%s - getObject Error: %s', MCUtil.logTimestamp(), ex_);
            return null;
        }
    }

}