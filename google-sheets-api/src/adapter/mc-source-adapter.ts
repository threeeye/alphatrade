"use strict";

var http = require('http');
var dateFormat = require('dateformat');
var sourceHost = '127.0.0.1';
var sourcePort = 8082;

import { Configuration } from '../model/configuration';
import { MCUtil } from '../components/mc-util';
import { Note } from '../model/note';
import { Opportunity } from '../model/opportunity';
import { Position } from '../model/position';
import { Security } from '../model/security';
import { User } from '../model/user';
import { VerticalSpread } from '../model/verticalSpread';
import { Exposure } from '../model/exposure';

export module MCSourceAdapter {

    export function updatePortfolio(positions_: Position[], user_: User): Promise<Position[]> {
        if (positions_ == null || positions_.length == 0)
            return;// Promise.resolve(null);
        var promise = new Promise<Position[]>((resolve_, reject_) => {
            try {
                var post_data = JSON.stringify(positions_);
                var options = {
                    hostname: sourceHost,
                    port: sourcePort,
                    path: '/api/v1/portfolio',
                    headers: {
                        'Accept': 'application/json',
                        'User': user_.email
                    },
                    method: 'PUT',
                    timeout: 30000
                };
                var req = http.request(options, function (res_: any) {
                    //     console.log('STATUS: ' + res_.statusCode);
                    //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                    // Buffer the body entirely for processing as a whole.
                    var bodyChunks = new Array();
                    res_.on('data', function (chunk: string) {
                        //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                        //Process streamed parts
                        bodyChunks.push(chunk);
                    }).on('end', function () {
                        var body = Buffer.concat(bodyChunks);
                        //                  console.log('%s - updatePortfolio end: %s', MCUtil.logDate(), body);
                        var data = MCUtil.getObject(body);
                        if (data == null || data.data == null || data.data.length == 0) {
                            console.log('No positions data.');
                            Promise.resolve(null);
                        } else if (data.error != null) {
                            console.log('%s - updatePortfolio Error: %s', MCUtil.logTimestamp(), data.error);
                            Promise.resolve(null);
                        } else {
                            var count = 0;
                            var positions = data.data;
                            positions.forEach((p: Position) => {
                                p.tradeDate = new Date(p.tradeDate);
                                var s = p.security;
                                //                                      console.log('lastup: %s - %f', s.symbol, s.price);
                                s.lastUpdate = new Date(s.lastUpdate);
                                if (s.expirationDate != null) {
                                    s.expirationDate = new Date(s.expirationDate);
                                }
                                //    s.expirationDate = new Date(s.expirationDate);
                                //   s.optExpDate = new Date(s.optExpDate);
                                //  if(((any)s).strike!=null) {

                                //  }
                            });
                            //           console.log("positions length: "+ data.data);
                            resolve_(positions);
                        }
                    }).on('error', (err_: any) => {
                        console.log('%s - updatePortfolio http Error: %s', MCUtil.logTimestamp(), err_);
                    });
                });
                req.write(post_data);
                req.end();
            } catch (ex_) {
                console.log('%s - updatePortfolio Error: %s', MCUtil.logTimestamp(), ex_);
                reject_(ex_);
            }
        });
        return promise;
    }

    export function sendNotes(notes_: Note[], user_: User): Promise<number> {
        if (notes_ == null || notes_.length == 0)
            return;
        var promise = new Promise<number>((resolve_, reject_) => {
            try {
                var post_data = JSON.stringify(notes_);
                var options = {
                    hostname: sourceHost,
                    port: sourcePort,
                    path: '/api/v1/notes',
                    headers: {
                        'Accept': 'application/json',
                        'User': user_.email
                    },
                    method: 'PUT',
                    timeout: 30000
                };
                var req = http.request(options, function (res_: any) {
                    //     console.log('STATUS: ' + res_.statusCode);
                    //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                    // Buffer the body entirely for processing as a whole.
                    var bodyChunks = new Array();
                    res_.on('data', function (chunk: string) {
                        //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                        //Process streamed parts
                        bodyChunks.push(chunk);
                    }).on('end', function () {
                        var body = Buffer.concat(bodyChunks);
                        //                  console.log('%s - updatePortfolio end: %s', MCUtil.logDate(), body);
                        var data = MCUtil.getObject(body);
                        if (data == null || data.data == null || data.data.length == 0) {
                            console.log('No notes response.');
                            Promise.resolve(null);
                        } else if (data.error != null) {
                            console.log('%s - sendNotes Error: %s', MCUtil.logTimestamp(), data.error);
                            Promise.resolve(null);
                        } else {
                            var count = data.data;
                            resolve_(count);
                        }
                    }).on('error', (err_: any) => {
                        console.log('%s - sendNotes http Error: %s', MCUtil.logTimestamp(), err_);
                    });
                });
                req.write(post_data);
                req.end();
            } catch (ex_) {
                console.log('%s - sendNotes Error: %s', MCUtil.logTimestamp(), ex_);
                reject_(ex_);
            }
        });
        return promise;
    }

    export function updateSecurities(symbols_: Security[]): void {
        if (symbols_ == null || symbols_.length == 0)
            return;// Promise.resolve(null);
        try {
            var symStr = symbols_.map((s_) => {
                return s_.tickerId;
            }).join(",");
            console.log('%s - Sourcing [%s]', MCUtil.logTimestamp(), symStr);
            var options = {
                hostname: sourceHost,
                port: sourcePort,
                path: '/api/v1/add/' + symStr,
                headers: {
                    'Accept': 'application/json',
                },
                method: 'PUT',
                timeout: 30000
            };
            var req = http.request(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    //   var body = Buffer.concat(bodyChunks);
                    //   console.log('%s - Body: %s', MCUtil.logDate(), body);
                }).on('error', (err_: any) => {
                    console.log('%s - updateSecurities http Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
            //  req.write();
            req.end();
        } catch (ex_) {
            console.log('%s - updateSecurities Error: %s', MCUtil.logTimestamp(), ex_);
        }
    }

    export function getOpportunities(user_: User): Promise<Opportunity[]> {
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/opportunities',
            headers: {
                'Accept': 'application/json',
                'User': user_.email
            },
            timeout: 30000
        };
        var promise = new Promise<Opportunity[]>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        Promise.resolve(null);
                    } else if (data.error != null) {
                        console.log('%s - getOpportunities Error: %s', MCUtil.logTimestamp(), data.error);
                        Promise.resolve(null);
                    } else {
                        var opportunities = data.data;
                        var result: Opportunity[] = [];
                        opportunities.forEach((opo_: Opportunity) => {
                            //       var opo = new Opportunity(opo_);
                            //     console.log('%s - Opo: %s', MCUtil.logDate(), JSON.stringify(opo_));
                            result.push(opo_);
                        });
                        resolve_(result);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getOpportunities Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }

    export function getSpreads(user_: User): Promise<Opportunity[]> {
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/spreads',
            headers: {
                'Accept': 'application/json',
                'User': user_.email
            },
            timeout: 30000
        };
        var promise = new Promise<Opportunity[]>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        Promise.resolve(null);
                    } else if (data.error != null) {
                        console.log('%s - getOpportunities Error: %s', MCUtil.logTimestamp(), data.error);
                        Promise.resolve(null);
                    } else {
                        var opportunities = data.data;
                        var result: Opportunity[] = [];
                        opportunities.forEach((opo_: Opportunity) => {
                            //       var opo = new Opportunity(opo_);
                            //     console.log('%s - Opo: %s', MCUtil.logDate(), JSON.stringify(opo_));
                            result.push(opo_);
                        });
                        resolve_(result);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getOpportunities Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }


    export function getUsers(): Promise<User[]> {
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/users',
            headers: {
                'Accept': 'application/json'
            },
            timeout: 30000
        };
        var promise = new Promise<User[]>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        Promise.resolve(null);
                    } else if (data.error != null) {
                        console.log('%s - getUsers Error: %s', MCUtil.logTimestamp(), data.error);
                        Promise.resolve(null);
                    } else {
                        var users = data.data;
                        var result: User[] = [];
                        users.forEach((user_: User) => {
                            //       var opo = new Opportunity(opo_);
                            //     console.log('%s - Opo: %s', MCUtil.logDate(), JSON.stringify(opo_));
                            result.push(user_);
                        });
                        resolve_(result);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getUsers Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }

    export function getExposure(user_: User): Promise<Exposure[]> {
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/exposure',
            headers: {
                'Accept': 'application/json',
                'User': user_.email
            },
            timeout: 30000
        };
        var promise = new Promise<Exposure[]>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        Promise.resolve(null);
                    } else if (data.error != null) {
                        console.log('%s - getExposure Error: %s', MCUtil.logTimestamp(), data.error);
                        Promise.resolve(null);
                    } else {
                        var exposures = data.data;
                        var result: Exposure[] = [];
                        exposures.forEach((exp_: Exposure) => {
                            //       var opo = new Opportunity(opo_);
                            //     console.log('%s - Opo: %s', MCUtil.logDate(), JSON.stringify(opo_));
                            result.push(exp_);
                        });
                        resolve_(result);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getExposure Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }

    export function reset(resetCode_: string, user_: User): Promise<boolean> {
        if (resetCode_ == null || resetCode_.length == 0)
            return Promise.resolve(false);
        var promise = new Promise<boolean>((resolve_, reject_) => {
            try {
                //    var post_data = JSON.stringify(config_);
                var options = {
                    hostname: sourceHost,
                    port: sourcePort,
                    path: '/api/v1/reset/' + resetCode_,
                    headers: {
                        'Accept': 'application/json',
                        'User': user_.email
                    },
                    method: 'POST',
                    timeout: 30000
                };
                var req = http.request(options, function (res_: any) {
                    //     console.log('STATUS: ' + res_.statusCode);
                    //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                    // Buffer the body entirely for processing as a whole.
                    var bodyChunks = new Array();
                    res_.on('data', function (chunk: string) {
                        //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                        //Process streamed parts
                        bodyChunks.push(chunk);
                    }).on('end', function () {
                        var body = Buffer.concat(bodyChunks);
                        console.log('%s - reset end: %s', MCUtil.logTimestamp(), body);
                        var data = MCUtil.getObject(body);
                        if (data == null) {
                            console.log('Reset failed.');
                            resolve_(false);
                        } else if (data.error != null) {
                            console.log('%s - reset Error: %s', MCUtil.logTimestamp(), data.error);
                            resolve_(false);
                        } else {
                            resolve_(data.data);
                        }
                    }).on('error', (err_: any) => {
                        console.log('%s - reset http Error: %s', MCUtil.logTimestamp(), err_);
                    });
                });
                //    req.write(post_data);
                req.end();
            } catch (ex_) {
                console.log('%s - reset Error: %s', MCUtil.logTimestamp(), ex_);
                reject_(ex_);
            }
        });
        return promise;
    }


    export function updateConfiguration(config_: Configuration, user_: User): Promise<boolean> {
        if (config_ == null)
            return Promise.resolve(false);
        var promise = new Promise<boolean>((resolve_, reject_) => {
            try {
                var post_data = JSON.stringify(config_);
                var options = {
                    hostname: sourceHost,
                    port: sourcePort,
                    path: '/api/v1/configuration',
                    headers: {
                        'Accept': 'application/json',
                        'User': user_.email
                    },
                    method: 'POST',
                    timeout: 30000
                };
                var req = http.request(options, function (res_: any) {
                    //     console.log('STATUS: ' + res_.statusCode);
                    //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                    // Buffer the body entirely for processing as a whole.
                    var bodyChunks = new Array();
                    res_.on('data', function (chunk: string) {
                        //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                        //Process streamed parts
                        bodyChunks.push(chunk);
                    }).on('end', function () {
                        var body = Buffer.concat(bodyChunks);
                        //           console.log('%s - updatePortfolio end: %s', MCUtil.logDate(), body);
                        var data = MCUtil.getObject(body);
                        if (data == null) {
                            console.log('Configuration failed.');
                            resolve_(false);
                        } else if (data.error != null) {
                            console.log('%s - updateConfiguration Error: %s', MCUtil.logTimestamp(), data.error);
                            resolve_(false);
                        } else {
                            resolve_(data.data);
                        }
                    }).on('error', (err_: any) => {
                        console.log('%s - updateConfiguration http Error: %s', MCUtil.logTimestamp(), err_);
                    });
                });
                req.write(post_data);
                req.end();
            } catch (ex_) {
                console.log('%s - updateConfiguration Error: %s', MCUtil.logTimestamp(), ex_);
                reject_(ex_);
            }
        });
        return promise;
    }

    export function updatePrices(symbols_: Security[]): Promise<number> {
        if (symbols_ == null || symbols_.length == 0)
            return Promise.resolve(null);

        var tickerStr = symbols_.map((el) => {
            if (el != null && el.secType != null)
                return el.tickerId;
        }).join(',');
        //     console.log('%s - getTradier path: %s', MCUtil.logDate(), path);
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/pricing/' + tickerStr,
            //  method: 'GET',
            headers: {
                'Accept': 'application/json',
            },
            timeout: 30000
        };
        var promise = new Promise<number>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    //      console.log('%s - getTradier end: %s', MCUtil.logDate(), body);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        resolve_(0);
                    } else if (data.error != null) {
                        console.log('%s - updatePrices Error: %s', MCUtil.logTimestamp(), data.error);
                        resolve_(-1);
                    } else {
                        var count = 0;
                        // var quotes = Array.isArray(data.quotes.quote) ? data.quotes.quote : [data.quotes.quote];
                        var quotes = data.data;
                        symbols_.forEach((sym_: Security) => {
                            quotes.some((quote_: any) => {
                                if (quote_.occId === sym_.tickerId) {
                                    //            console.log('%s - found: %s', MCUtil.logDate(), quote_.symbol);
                                    var price = quote_.priceBid == 0 && quote_.volume == 0 ? quote_.priceLast : (quote_.priceAsk + quote_.priceBid) / 2.0;
                                    sym_.price = price;
                                    count++;
                                    return true;
                                }
                            });
                        });
                        resolve_(count);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - updatePrices Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }

    export function getRolls(symbols_: Security[], user_: User): Promise<string[][]> {
        if (symbols_ == null || symbols_.length == 0)
            return Promise.resolve(null);

        var tickerStr = symbols_.map((el) => {
            if (el != null && el.secType != null)
                return el.tickerId;
        }).join(',');
        var options = {
            hostname: sourceHost,
            port: sourcePort,
            path: '/api/v1/roll/' + tickerStr,
            //  method: 'GET',
            headers: {
                'Accept': 'application/json',
                'User': user_.email
            },
            timeout: 30000
        };
        var promise = new Promise<string[][]>((resolve_, reject_) => {
            http.get(options, function (res_: any) {
                //     console.log('STATUS: ' + res_.statusCode);
                //     console.log('HEADERS: ' + JSON.stringify(res_.headers));

                // Buffer the body entirely for processing as a whole.
                var bodyChunks = new Array();
                res_.on('data', function (chunk: string) {
                    //       console.log('%s - getTradier data: %s', MCUtil.logDate(), chunk);
                    //Process streamed parts
                    bodyChunks.push(chunk);
                }).on('end', function () {
                    var body = Buffer.concat(bodyChunks);
                    //     console.log('%s - getRolls end: %s', MCUtil.logDate(), body);
                    var data = MCUtil.getObject(body);
                    if (data == null || data.data == null || data.data.length == 0) {
                        resolve_(null);
                    } else if (data.error != null) {
                        console.log('%s - getRolls Error: %s', MCUtil.logTimestamp(), data.error);
                        resolve_(null);
                    } else {
                        var dateSet: Set<string> = new Set();
                        //     var dateStrs: string[] = [];
                        dateSet.add('');
                        //     dateStrs.push('');
                        for (var i = 0; i < data.data.length; i++) {
                            var element = data.data[i];
                            //     var base = <Security>element.base;
                            var rolls = element.rolls;
                            //  var rolls = <Map<String, any>>data.rolls;
                            //    console.log('roll: ' + JSON.stringify(base));
                            for (var dateStr in rolls) {
                                dateSet.add(dateStr);
                                //                                dateStrs.push(dateStr);
                                // var d = new Date(dateStr);
                                // dateStrs.push(dateFormat(d, 'yyyy-mm-dd'));
                                // console.log('date: ' + dateStr);
                            }
                        }
                        var dateStrs = Array.from(dateSet);
                        dateStrs.sort();
                        console.log('dates: ' + dateStrs);
                        //        console.log('done rolls step 1');
                        var result: string[][] = [];
                        var dates: string[] = [];
                        result.push(dates);
                        dates.push('Action...');
                        dates.push('$ (ul)');
                        dates.push('');
                        for (var i = 1; i < dateStrs.length; i++) {
                            var dateStr = dateStrs[i];
                            var d = new Date(dateStr);
                            dates.push(dateFormat(d, 'yyyy-mm-dd'));
                            dates.push('$');
                            dates.push('%');
                        }
                        for (var i = 0; i < data.data.length; i++) {
                            var element = data.data[i];
                            var base = new Security(element.base);
                            //     var base = <Security>element.base;
                            var rolls = element.rolls;
                            var rows: any[][] = [];
                            rows.push([base.label, base.priceUnderlying, base.strike]);
                            rows.push(['', '', '']);
                            rows.push(['', '', '']);
                            for (var j = 1; j < dateStrs.length; j++) {
                                var roll = rolls[dateStrs[j]];
                                if (roll != null && roll.length > 0) {
                                    for (var k = 0; k < roll.length; k++) {
                                        //     console.log('dstr: ' + roll[k].option.label);
                                        rows[k].push(roll[k].option.label);
                                        rows[k].push(roll[k].yield.abs);
                                        rows[k].push(roll[k].yield.apy);
                                    }
                                } else {
                                    for (var k = 0; k < rows.length; k++) {
                                        rows[k].push('');
                                        rows[k].push('');
                                        rows[k].push('');
                                    }
                                }
                            }
                            for (var j = 0; j < rows.length; j++) {
                                result.push(rows[j]);
                            }
                            result.push(['-']);
                        }
                        resolve_(result);
                    }
                }).on('error', (err_: any) => {
                    console.log('%s - getRolls Error: %s', MCUtil.logTimestamp(), err_);
                });
            });
        });
        return promise;
    }

}