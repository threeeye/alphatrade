package com.isoplane.at.collector;

import java.text.ParseException;

import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.fundamentals.ATFakeFundamentalsManager;
import com.isoplane.at.symbols.ATFakeSymbolManager;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATCollectorControllerTest {

	static final Logger log = LoggerFactory.getLogger(ATCollectorControllerTest.class);
	static private ATCollectorController _collCtrl;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String[] configKeys = {
				"mongo.t.foundation",
				"mongo.t.statement.blnc.alva",
				"mongo.t.statement.cash.alva",
				"mongo.t.statement.comp.alva",
				"mongo.t.statement.cons.alva",
				"mongo.t.statement.earn.alva" };

		String propertiesPath = System.getProperty("test_args");
		Configuration config = ATConfigUtil.init(propertiesPath, false);
		ATConfigUtil.overrideConfiguration("alphavantage.key", "demo");
		ATConfigUtil.overrideConfiguration("alphavantage.throttletime", 1250);
		for (String key : configKeys) {
			String testTable = String.format("%s_test", config.getString(key));
			ATConfigUtil.overrideConfiguration(key, testTable);
		}

		ATExecutors.init();
		ATServiceRegistry.init();
		IATSymbolProvider symSvc = new ATFakeSymbolManager("IBM");
		ATServiceRegistry.setSymbolProvider(symSvc, false);
		IATFundamentalsProvider fndSvc = new ATFakeFundamentalsManager();
		ATServiceRegistry.setFundamentalsProvider(fndSvc, false);

		_collCtrl = new ATCollectorController();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	@Disabled
	public void testPopulate() throws Exception {
		_collCtrl.run();
		Thread.sleep(5000L);
		_collCtrl.shutdown();
	}

	@Test
	// @Disabled
	public void testRun() throws ParseException, InterruptedException {
		_collCtrl.run();
	//	Thread.sleep(5000L);
		ATSysUtils.waitForEnter();
		_collCtrl.shutdown();
	}

}
