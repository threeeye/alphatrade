package com.isoplane.at.collector;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.protobuf.ATProtoMapMessageWrapper;
import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.fundamentals.ATFakeFundamentalsManager;
import com.isoplane.at.fundamentals.ATFundamentalsManager;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.fundamentals.ATKafkaFundamentalsProducer;
import com.isoplane.at.kafka.map.IATMapMessageTypes;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer.DefaultStatusRunner;
import com.isoplane.at.symbols.ATFakeSymbolManager;
import com.isoplane.at.symbols.ATSymbolManager;

public class ATKafkaCollectorService implements IATFundamentalListener, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaCollectorService.class);
	static private String ID;

	private ATCollectorController _ctrl;
	private final CountDownLatch _serviceLatch;
	private ATKafkaSystemStatusProducer _statusProducer;
	private ATKafkaFundamentalsProducer _fndProducer;
	private boolean _isRunning;

	private static final boolean isTestMode = false;

	public static void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATKafkaRegistry.register("collector_svc",
					ATKafkaFundamentalsProducer.class,
					ATKafkaSystemStatusProducer.class);

			String ipStr = ATSysUtils.getIpAddress("n/a");
			String portStr = config.getString("web.port");
			ID = StringUtils.isBlank(portStr) ? String.format("%s:%s", ATKafkaCollectorService.class.getSimpleName(), ipStr)
					: String.format("%s:%s:%s", ATKafkaCollectorService.class.getSimpleName(), ipStr, portStr);

			ATServiceRegistry.init();
			if (!ATKafkaCollectorService.isTestMode) {
				IATFundamentalsProvider fndSvc = new ATFundamentalsManager();
				ATServiceRegistry.setFundamentalsProvider(fndSvc, false);
				IATSymbolProvider symSvc = new ATSymbolManager(false);
				ATServiceRegistry.setSymbolProvider(symSvc, false);
			} else {
				config.setProperty("alphavantage.key", "demo");
				IATFundamentalsProvider fndSvc = new ATFakeFundamentalsManager();
				ATServiceRegistry.setFundamentalsProvider(fndSvc, false);
				IATSymbolProvider symSvc = new ATFakeSymbolManager("IBM");
				ATServiceRegistry.setSymbolProvider(symSvc, false);
			}

			// String symbolStr = ATConfigUtil.getResourceFileAsString("ark_symbols.csv");
			// String[] tokens = symbolStr.split(",");
			// Set<String> symbols = new TreeSet<>(Arrays.asList(tokens));
			// Iterator<String> iSym = symbols.iterator();
			// Set<String> out = new TreeSet<>();
			// while (iSym.hasNext()) {
			// String sym = iSym.next();
			// if (StringUtils.isBlank(sym) || NumberUtils.isCreatable(sym) || sym.indexOf(' ') >= 0) {
			// iSym.remove();
			// out.add(sym);
			// continue;
			// }
			//
			// }
			// symbolStr = String.join(",", symbols);
			// log.info(String.format("out: %s", out));
			// log.info(String.format("in : %s", symbolStr));

			ATKafkaCollectorService instance = new ATKafkaCollectorService();
			instance.init();
			instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaCollectorService.class.getSimpleName()), ex);
			System.exit(1);
		}

	}

	public ATKafkaCollectorService() {
		_serviceLatch = new CountDownLatch(1);
	}

	private void init() {
		_fndProducer = ATKafkaRegistry.get(ATKafkaFundamentalsProducer.class);
		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);
		ATSystemStatusManager.register(this);

		_ctrl = new ATCollectorController();
		_ctrl.subscribe(this);
	}

	public void start() {
		this._isRunning = true;
		ATKafkaRegistry.start();
		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, DefaultStatusRunner.class.getSimpleName()),
				new DefaultStatusRunner(ATKafkaCollectorService.ID, this, this._statusProducer));

		_ctrl.run();

		// ATExecutors.submit(String.format("%s.%s", pre, DelayedPushRunner.class.getSimpleName()), new DelayedPushRunner());
	}

	private void shutdown() {
		this._isRunning = false;
		this._ctrl.shutdown();
		ATKafkaRegistry.stop();
	}

	public void notify(String src_, ATFundamental fnd_) {
		Map<String, Object> map = fnd_.getAsMap();
		String src = src_ != null ? src_ : "n/a";
		MapMessage msg = ATProtoMapMessageWrapper.toMapMessage(IATMapMessageTypes.FUNDAMENTAL_TYPE, fnd_.getAtId(), src, "fnd",
				System.currentTimeMillis(), map);
		_fndProducer.send(msg);
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("isRunning", String.format("%b", this._isRunning));
		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

}
