package com.isoplane.at.collector;

import com.isoplane.at.commons.model.ATFundamental;

public interface IATFundamentalListener {

	void notify(String source, ATFundamental fundamental);
}
