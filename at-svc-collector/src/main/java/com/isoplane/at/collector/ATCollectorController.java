package com.isoplane.at.collector;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.alphavantage.ATAlphaVantageCollector;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATFundamentalsSource;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;

public class ATCollectorController implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATCollectorController.class);

	private Set<IATFundamentalListener> subscribers;
	private Set<SingleSymbolRunner> _runners;

	public ATCollectorController() {
		this.init();
		this.subscribers = new HashSet<>();
		ATSystemStatusManager.register(this);
	}

	private void init() {
		this._runners = new HashSet<>();
		IATFundamentalsSource alphaVantage = new ATAlphaVantageCollector();
		SingleSymbolRunner avRunner = new SingleSymbolRunner("alphaVantage", alphaVantage);
		this._runners.add(avRunner);
	}

	public void subscribe(IATFundamentalListener listener_) {
		this.subscribers.add(listener_);
	}

	public void run() {
		if (this._runners == null) {
			throw new ATException(String.format("Runners not initialized"));
		}
		for (SingleSymbolRunner runner : _runners) {
			ATExecutors.submit(runner.getId(), runner);
		}
	}

	public void shutdown() {
		if (this._runners != null) {
			for (SingleSymbolRunner runner : _runners) {
				runner.shutdown();
			}
		}
	}

	private void notifyFundamental(ATFundamental fnd_) {
		ATFundamental fnd = this.populateFundamental(fnd_);
		if (fnd != null && !this.subscribers.isEmpty()) {
			String src = IATAsset.getString(fnd_, IATAssetConstants.SOURCE);
			for (IATFundamentalListener subscriber : this.subscribers) {
				subscriber.notify(src, fnd);
			}
		}
	}

	public ATFundamental populateFundamental(ATFundamental newFnd_) {
		String symbol = newFnd_ != null ? newFnd_.getAtId() : null;
		if (StringUtils.isBlank(symbol))
			return null;
		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		Map<String, ATFundamental> oldFndMap = fndSvc.getFundamentals(symbol);
		ATFundamental oldFnd = oldFndMap != null ? oldFndMap.get(symbol) : null;
		if (oldFnd != null) {
			String oldDStr = oldFnd.getLatestDS8();
			String newDStr = newFnd_.getLatestDS8();
			if (StringUtils.isBlank(oldDStr) || oldDStr.compareTo(newDStr) < 0) {
				oldFnd.setLatestDS8(newDStr);
				Double peTrail = newFnd_.getTrailingPE();
				if (peTrail != null) {
					oldFnd.setTrailingPE(peTrail);
				}
				Double revPEGrowthR = newFnd_.getPEGrowthRatio();
				if (revPEGrowthR != null) {
					oldFnd.setPEGrowthRatio(revPEGrowthR);
				}
				Double revMlt = newFnd_.getRevenueMultiple();
				if (revMlt != null) {
					oldFnd.setRevenueMultiple(revMlt);
				}
				Double revGrowthYoY = newFnd_.getRevenueGrowthYoY();
				if (revGrowthYoY != null) {
					oldFnd.setRevenueGrowthYoY(revGrowthYoY);
				}
				Double revGrowthAvg3Y = newFnd_.getRevenueGrowthAvg3Y();
				if (revGrowthAvg3Y != null) {
					oldFnd.setRevenueGrowthAvg3Y(revGrowthAvg3Y);
				}
				Double revGrowth1Y = newFnd_.getRevenueGrowth1Y();
				if (revGrowth1Y != null) {
					oldFnd.setRevenueGrowth1Y(revGrowth1Y);
				}
				Double revGrowth1Q = newFnd_.getRevenueGrowth1Q();
				if (revGrowth1Q != null) {
					oldFnd.setRevenueGrowth1Q(revGrowth1Q);
				}
				Double rev1Y = newFnd_.getRevenue1Y();
				if (rev1Y != null) {
					oldFnd.setRevenue1Y(rev1Y);
				}
				Double rev1Q = newFnd_.getRevenue1Q();
				if (rev1Q != null) {
					oldFnd.setRevenue1Q(rev1Q);
				}
				fndSvc.updateFundamental(oldFnd);
				log.info(String.format("Updated [%s]", symbol));
				return oldFnd;
			}
		} else {
			fndSvc.updateFundamental(newFnd_);
			log.info(String.format("Created [%s]", symbol));
			newFnd_.put("_temp_tag", "new");
			return newFnd_;
		}
		log.info(String.format("Skipped [%s]", symbol));
		return null;
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		for (SingleSymbolRunner runner : _runners) {
			status.put(String.format("%s.cycles", runner.getId()), String.format("%d", runner.getCycleCount()));
			status.put(String.format("%s.symbols", runner.getId()), String.format("%d", runner.getSymbolCount()));
			status.put(String.format("%s.current", runner.getId()), String.format("%s", runner.getCurrent()));
		}
		return status;
	}

	protected final class SingleSymbolRunner implements Runnable {

		private String id;
		private boolean isRunning = false;
		private IATFundamentalsSource source;
		private long cycleCount = 0;
		private Collection<ATSymbol> symbols;
		private String _current;

		public SingleSymbolRunner(String id_, IATFundamentalsSource source_) {
			this.id = id_;
			this.source = source_;
		}

		public void run() {
			this.isRunning = true;
			while (this.isRunning) {
				try {
					cycleCount++;
					int days = ATConfigUtil.config().getInt("collector.fundamentals.period");
					Date cutoffDate = DateUtils.addDays(new Date(), -days);
					String cutoffDStr = ATFormats.DATE_yyyyMMdd.get().format(cutoffDate);
					IATSymbolProvider symPro = ATServiceRegistry.getSymbolProvider();
					Map<String, ATSymbol> symMap = symPro.getSymbols();
					IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
					Map<String, ATFundamental> oldFndMap = fndSvc.getFundamentals();
					if (symMap == null || symMap.isEmpty()) {
						Thread.sleep(300000L);
						continue;
					} else if (oldFndMap != null && !oldFndMap.isEmpty()) {
						Set<ATSymbol> testSet = new TreeSet<>();
						Set<String> rejected = new TreeSet<>();
						for (Entry<String, ATSymbol> testSym : symMap.entrySet()) {
							ATFundamental oldFnd = oldFndMap.get(testSym.getKey());
							if (oldFnd != null) {
								String dStr = oldFnd.getLatestDS8();
								if (dStr != null && dStr.compareTo(cutoffDStr) > 0) {
									rejected.add(oldFnd.getOccId());
									continue;
								}
							}
							testSet.add(testSym.getValue());
						}
						log.info(String.format("%s skipping fundamentals before [%s - %d/%d]: %s", SingleSymbolRunner.class.getSimpleName(),
								cutoffDStr,
								rejected.size(), oldFndMap.size(), rejected));
						symbols = testSet;
					} else {
						symbols = symMap.values();
					}
					int count = 1;
					for (IATSymbol symbol : symbols) {
						log.info(String.format("%s processing [%s:%4d/%d - %s]", SingleSymbolRunner.class.getSimpleName(), this.id, count++,
								symbols.size(), symbol.getId()));
						this._current = symbol.getId();
						Map<String, ATFundamental> fndMap = this.source.processFundamentals(symbol);
						if (fndMap != null && !fndMap.isEmpty()) {
							for (ATFundamental fnd : fndMap.values()) {
								ATCollectorController.this.notifyFundamental(fnd);
							}
						}
						if (!this.isRunning)
							break;
					}
				} catch (Exception ex) {
					log.error(String.format("%s Error", SingleSymbolRunner.class.getSimpleName()), ex);
				}
			}
			log.info(String.format("%s Terminating", SingleSymbolRunner.class.getSimpleName()));
		}

		public String getId() {
			return this.id;
		}

		public String getCurrent() {
			return this._current;
		}

		public long getCycleCount() {
			return cycleCount;
		}

		public int getSymbolCount() {
			int symCount = symbols != null ? symbols.size() : 0;
			return symCount;
		}

		public void shutdown() {
			this.isRunning = false;
		}
	}

}
