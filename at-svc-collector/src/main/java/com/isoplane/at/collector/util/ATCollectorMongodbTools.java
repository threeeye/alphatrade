package com.isoplane.at.collector.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentBridgeMap;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;

public class ATCollectorMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATCollectorMongodbTools.class);

	private ATMongoDao _dao;

	private String _balanceTable;
	private String _cashflowTable;
	private String _companyTable;
	private String _consolidatedTable;
	private String _earningsTable;

	public ATCollectorMongodbTools() {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
	}

	public ATCollectorMongodbTools init() {
		Configuration config = ATConfigUtil.config();

		this._balanceTable = config.getString("mongo.t.statement.blnc.alva");
		this._cashflowTable = config.getString("mongo.t.statement.cash.alva");
		this._companyTable = config.getString("mongo.t.statement.comp.alva");
		this._consolidatedTable = config.getString("mongo.t.statement.cons.alva");
		this._earningsTable = config.getString("mongo.t.statement.earn.alva");
		this._dao = new ATMongoDao();
		return this;
	}

	public boolean saveStatement(String type_, Map<String, Object> statement_) {
		if (statement_ == null || statement_.isEmpty())
			return false;
		String table = getTable(type_);
		if (table == null)
			return false;
		String id = IATLookupMap.getAtId(statement_);
		IATWhereQuery delQ = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, id);
		_dao.delete(table, delQ);
		StatementBridgeMap bridgeMap = new StatementBridgeMap();
		bridgeMap.putAll(statement_);
		@SuppressWarnings("unchecked")
		boolean success = _dao.update(table, IATLookupMap.AT_ID, bridgeMap);
		return success;
	}

	public List<Map<String, Object>> loadStatements(String type_) {
		String table = getTable(type_);
		if (table == null)
			return null;
		IATWhereQuery query = ATPersistenceRegistry.query().empty();
		List<StatementBridgeMap> statements = this._dao.query(table, query, StatementBridgeMap.class);
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> result = (List<Map<String, Object>>) (Object) statements;
		return result;
	}

	private String getTable(String type_) {
		String table = null;
		switch (type_) {
		case StatementBridgeMap.BALANCE_SHEET:
			table = this._balanceTable;
			break;
		case StatementBridgeMap.CASH_FLOW:
			table = this._cashflowTable;
			break;
		case StatementBridgeMap.COMPANY:
			table = this._companyTable;
			break;
		case StatementBridgeMap.CONSOLIDATED:
			table = this._consolidatedTable;
			break;
		case StatementBridgeMap.EARNINGS:
			table = this._earningsTable;
			break;
		default:
			log.warn(String.format("Unsupported type [%s]", type_));
			return null;
		}
		return table;
	}

	public static class StatementBridgeMap extends ATPersistentBridgeMap {

		public static final String BALANCE_SHEET = "balance";
		public static final String CASH_FLOW = "cashflow";
		public static final String COMPANY = "company";
		public static final String CONSOLIDATED = "consolidated";
		public static final String EARNINGS = "earnings";

		private static final long serialVersionUID = 1L;

		public StatementBridgeMap() {
			super(IATLookupMap.AT_ID);
		}
	}

}
