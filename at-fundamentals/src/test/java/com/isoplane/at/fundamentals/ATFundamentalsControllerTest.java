package com.isoplane.at.fundamentals;

import java.text.ParseException;

import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATFundamentalsControllerTest {

	static final Logger log = LoggerFactory.getLogger(ATFundamentalsControllerTest.class);
	static private ATFundamentalsManager _fndSvc;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(propertiesPath, true);
		ATConfigUtil.overrideConfiguration("mongo.t.foundation", "foundation_test");

		ATExecutors.init();
		_fndSvc = new ATFundamentalsManager();
		_fndSvc.initService();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	// @Disabled
	public void testUpdate() throws ParseException {
		// _fndSvc.updateFundamentals();
		// List<Map<String, Object>> result = _avAdp.getIncomeStatement(symbol);
		// assertTrue(result != null);
	}


}
