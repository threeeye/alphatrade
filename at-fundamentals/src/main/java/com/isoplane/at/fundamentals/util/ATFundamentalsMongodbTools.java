package com.isoplane.at.fundamentals.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.fundamentals.model.ATMongoDividend;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

public class ATFundamentalsMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATFundamentalsMongodbTools.class);

	// private Configuration _config;
	private ATMongoDao _dao;
	private String _divTable;
	// private String _optionexpTable;
	private String _foundationTable;

	public ATFundamentalsMongodbTools() {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;

		_divTable = ATMongoTableRegistry.getDividendTable();
		_foundationTable = ATMongoTableRegistry.getFoundationTable();
	}

	public void init() {
		_dao = new ATMongoDao();
	}

	// public void updateFundamentals() {
	// // String table = _foundationTable + "_test";
	//
	// List<ATPersistentEquityFoundation> fnds = _dao.query(_foundationTable, ATPersistenceRegistry.query().empty(),
	// ATPersistentEquityFoundation.class);
	// int i = 0;
	// for (ATPersistentEquityFoundation fnd : fnds) {
	// Map<String, Double> hvMap = new TreeMap<>();
	// for (String hvKey : IATStatistics.HVS) {
	// Double hv = fnd.getHV(hvKey);
	// if (hv != null) {
	// hvMap.put(hvKey, hv);
	// }
	// }
	// if (!hvMap.isEmpty()) {
	// fnd.remove("_id");
	// Map<String, Object> upMap = new HashMap<>();
	// upMap.put(IATLookupMap.AT_ID, fnd.getAtId());
	// upMap.put("hv_map", hvMap);
	// @SuppressWarnings("unchecked")
	// boolean success = _dao.update(_foundationTable, IATLookupMap.AT_ID, upMap);
	// log.info(String.format("Updated [%s / %b]", fnd.getOccId(), success));
	// }
	// if (i++ >= 1000)
	// break;
	// }
	//
	// }

	public Map<String, ATFundamental> getFundamentals(IATWhereQuery query_) {
		List<ATPersistentEquityFoundation> foundation = _dao.query(_foundationTable, query_, ATPersistentEquityFoundation.class);
		Map<String, ATFundamental> newMap = new HashMap<>();
		if (foundation == null || foundation.isEmpty())
			return newMap;
		// final String modeAvg = IATStatistics.SMA60;
		// final String modeHV = IATStatistics.HV60;
		for (ATPersistentEquityFoundation f : foundation) {
			ATFundamental fnd = new ATFundamental(f, null, null);
			// fnd.setDividend(f.getDividend());
			// fnd.setDividendEstDS8(f.getDividendEstimatedDS8());
			// fnd.setDividendExDS8(f.getDividendExDS8());
			// fnd.setDividendFrequency(f.getDividendFrequency());
			// fnd.setEarningsDS8(fnd.getEarningsDS8());
			// fnd.setIV(f.getIV());
			// fnd.setMarketCap(f.getMarketCap());
			// fnd.setModeAvg(modeAvg);
			// fnd.setModeHV(modeHV);
			// fnd.setOccId(f.getOccId());
			// fnd.setPxAvg(f.getAvg(modeAvg));
			// fnd.setPxHV(f.getHV(modeHV));
			newMap.put(fnd.getOccId(), fnd);
		}
		return newMap;
	}

	public boolean updateFundamental(ATFundamental fnd_) {
		if (fnd_ == null)
			return false;
		ATPersistentEquityFoundation fnd = new ATPersistentEquityFoundation(fnd_);
		String id = fnd.getAtId();
		if (StringUtils.isBlank(id)) {
			id = fnd.getOccId();
			if (StringUtils.isBlank(id)) {
				log.error("Missing id: %s", fnd);
				return false;
			}
			fnd.setAtId(id);
		}
		Iterator<Entry<String, Object>> i = fnd.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> e = i.next();
			if (e.getValue() == null) {
				i.remove();
			}
		}
		boolean success = _dao.update(_foundationTable, fnd);
		return success;
	}

	public Map<String, IATDividend> getDividends(IATWhereQuery query_) {
		IATWhereQuery query = query_ != null ? query_ : ATPersistenceRegistry.query().empty();
		List<String> keys = Arrays.asList("div", "div_frq", "divex_dstr", "divpy_dstr", "divrc_dstr", "div_est_ds8");
		Map<String, ATMongoDividend> divMap = _dao.queryEdge(_divTable, query, IATAssetConstants.OCCID, IATLookupMap.AT_ID, -1, keys,
				ATMongoDividend.class);
		Map<String, IATDividend> resultMap = new TreeMap<>();
		if (divMap != null) {
			for (ATMongoDividend mDiv : divMap.values()) {
				String occId = (String) mDiv.get(IATMongo.MONGO_ID);
				// mDiv.setOccId(occId);
				ATDividend div = new ATDividend(mDiv);
				// String occId = (String) mDiv.get(IATMongo.MONGO_ID);
				div.setOccId(occId);
				resultMap.put(occId, div);
			}
		}
		return resultMap;
	}

	public boolean updateDividends(Collection<IATDividend> divs_) {
		if (divs_ == null || divs_.isEmpty())
			return false;
		List<IATLookupMap> data = divs_.stream().map(d -> new ATMongoDividend(d)).collect(Collectors.toList());
		boolean success = _dao.update(_divTable, data.toArray(new IATLookupMap[0]));
		return success;
	}

	public long deleteDividends(IATWhereQuery query_) {
		if (query_ == null)
			return -1;
		long count = _dao.delete(_divTable, query_);
		return count;
	}

}
