package com.isoplane.at.fundamentals;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.service.IATFundamentalsProvider;

public class ATFakeFundamentalsManager implements IATFundamentalsProvider {

	static final Logger log = LoggerFactory.getLogger(ATFakeFundamentalsManager.class);

	@Override
	public void initService() {
		log.info(String.format("initService"));
	}

	@Override
	public Map<String, IATDividend> getDividends(String... symbols) {
		log.info(String.format("getDividends: %s", (Object) symbols));
		if (symbols == null)
			return null;
		Map<String, IATDividend> divMap = Arrays.asList(symbols).stream().collect(Collectors.toMap(s -> s, s -> {
			ATDividend div = new ATDividend();
			div.setOccId(s);
			return div;
		}));
		return divMap;
	}

	@Override
	public Map<String, ATFundamental> getFundamentals(String... symbols) {
		log.info(String.format("getFundamentals: %s", (Object) symbols));
		if (symbols == null)
			return null;
		Map<String, ATFundamental> fndMap = Arrays.asList(symbols).stream().collect(Collectors.toMap(s -> s, s -> {
			ATFundamental fnd = new ATFundamental();
			fnd.setOccId(s);
			return fnd;
		}));
		return fndMap;
	}

	@Override
	public boolean updateFundamental(ATFundamental fnd) {
		log.info(String.format("updateFundamental: %s", fnd));
		return fnd != null;
	}

}
