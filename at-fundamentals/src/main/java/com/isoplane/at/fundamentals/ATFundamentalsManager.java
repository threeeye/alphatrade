package com.isoplane.at.fundamentals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.protobuf.ATProtoMapMessageWrapper;
import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATProtoMapMessageSubscriber;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.fundamentals.util.ATFundamentalsMongodbTools;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.fundamentals.ATKafkaFundamentalsConsumer;
import com.isoplane.at.kafka.map.IATMapMessageTypes;

public class ATFundamentalsManager implements IATFundamentalsProvider, IATProtoMapMessageSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATFundamentalsManager.class);

	protected ATFundamentalsMongodbTools _mongodb;

	private Map<String, ATFundamental> _fndCache;
	private boolean _isStreaming = false;

	public ATFundamentalsManager() {
		this._fndCache = new ConcurrentHashMap<>();

		_isStreaming = ATConfigUtil.config().getBoolean("kafka.fundamentals.streaming", false);
	}

	@Override
	public void initService() {
		_mongodb = new ATFundamentalsMongodbTools();
		_mongodb.init();

		initFundamentals();
	}

	private void initFundamentals() {
		if (this._isStreaming) {
			// NOTE: Configured as streaming receiver
			Map<String, ATFundamental> fnds = this.loadFundamentals();
			this._fndCache.putAll(fnds);
			log.info(String.format("Initialized [%d] fundamentals", this._fndCache.size()));

			ATKafkaFundamentalsConsumer fndConsumer = ATKafkaRegistry.get(ATKafkaFundamentalsConsumer.class);
			fndConsumer.subscribe(this);
		}
	}

	@Override
	public Map<String, IATDividend> getDividends(String... symbols_) {
		Collection<String> symbols = symbols_ != null ? Arrays.asList(symbols_) : new ArrayList<>();
		String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		IATWhereQuery query = ATPersistenceRegistry.query().gte(IATAssetConstants.DIV_EX_DATE_STR_OLD, today).or()
				.gte(IATAssetConstants.DIV_EST_DATE_STR, today);
		query = symbols.isEmpty() ? query : query.and().in(IATAssetConstants.OCCID, symbols);
		Map<String, IATDividend> resultMap = _mongodb.getDividends(query);
		return resultMap;
	}

	@Override
	public Map<String, ATFundamental> getFundamentals(String... symbols_) {
		if (this._isStreaming) {
			if (symbols_ == null || symbols_.length == 0) {
				return Collections.unmodifiableMap(this._fndCache);
			} else {
				Map<String, ATFundamental> resultMap = new TreeMap<>();
				ArrayList<String> missing = new ArrayList<>();
				for (String symbol : symbols_) {
					ATFundamental fnd = this._fndCache.get(symbol);
					if (fnd != null) {
						resultMap.put(symbol, fnd);
					} else {
						missing.add(symbol);
					}
				}
				if (!missing.isEmpty()) {
					Map<String, ATFundamental> loadedMap = this.loadFundamentals(missing.toArray(new String[0]));
					for (Entry<String, ATFundamental> entry : loadedMap.entrySet()) {
						this._fndCache.put(entry.getKey(), entry.getValue());
						resultMap.put(entry.getKey(), entry.getValue());
					}
				}
				return resultMap;
			}
		} else {
			return loadFundamentals(symbols_);
		}
	}

	private Map<String, ATFundamental> loadFundamentals(String... symbols_) {
		Collection<String> symbols = symbols_ != null ? Arrays.asList(symbols_) : new ArrayList<>();
		IATWhereQuery query = ATPersistenceRegistry.query();
		if (symbols.isEmpty()) {
			query = query.empty();
		} else if (symbols.size() == 1) {
			query = query.eq(IATLookupMap.AT_ID, symbols.iterator().next());
		} else {
			query = query.in(IATLookupMap.AT_ID, symbols);
		}
		Map<String, ATFundamental> fndMap = _mongodb.getFundamentals(query);
		return fndMap;
	}

	@Override
	public boolean updateFundamental(ATFundamental fnd_) {
		return _mongodb.updateFundamental(fnd_);
	}

	@Override
	public void notifyMapMessage(MapMessage msg_) {
		if (msg_ == null || !IATMapMessageTypes.FUNDAMENTAL_TYPE.equals(msg_.getType()))
			return;
		ATFundamental fnd = new ATFundamental();
		ATProtoMapMessageWrapper.toMap(msg_, fnd);
		String symbol = fnd.getAtId();
		this._fndCache.put(symbol, fnd);
		log.debug(String.format("Updated [%s]", symbol));
	}

	// public void updateFundamentals() {
	// this._mongodb.updateFundamentals();
	// }

}
