package com.isoplane.at.fundamentals.model;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.util.ATFormats;

public class ATMongoDividend extends ATPersistentLookupBase implements IATDividend, Comparable<IATDividend> {

	private static final long serialVersionUID = 1L;

	public ATMongoDividend() {
		super();
	}

	public ATMongoDividend(IATDividend template_) {
		super();
		if (template_ != null) {
			setOccId(template_.getOccId());
			setDividend(template_.getDividend());
			setDividendFrequency(template_.getDividendFrequency());
			String estDS8 = template_.getDividendEstimatedDS8();
			if (estDS8 != null)
				setDividendEstimatedDS8(estDS8);
			String estDstr = template_.getDividendExDateStr();
			if (estDstr != null)
				setDividendExDateStr(estDstr);
			String payDstr = template_.getDividendPayDateStr();
			if (payDstr != null)
				setDividendPayDateStr(payDstr);
			String recDstr = template_.getDividendRecordDateStr();
			if (recDstr != null)
				setDividendRecordDateStr(recDstr);
			generateAtId();
		}
	}

	public ATMongoDividend(Map<String, Object> data_) {
		super(data_);
	}

	@Override
	public int compareTo(IATDividend other_) {
		return IATDividend.compareTo(this, other_);
	}

	public void generateAtId() {
		String atid = IATDividend.getATId(this);
		super.setAtId(atid);
	}

	// @Override
	// public int compareTo(ATMongoDividend other_) {
	// return getAtId().compareTo(other_.getAtId());
	// }

	@Override
	public String toString() {
		String dStr = getDividendExDateStr();
		if (dStr == null)
			dStr = getDividendEstimatedDS8();
		String str = String.format("%-5s%s %.2f", getOccId(), dStr, getDividend());
		return str;
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> map = new HashMap<>();
		IATDividend.setOccId(map, getOccId());
		IATDividend.setDividend(map, getDividend());
		IATDividend.setDividendFrequency(map, getDividendFrequency());
		IATDividend.setDividendEstimatedDS8(map, getDividendEstimatedDS8());
		IATDividend.setDividendExDateStr(map, getDividendExDateStr());
		IATDividend.setDividendPayDateStr(map, getDividendPayDateStr());
		IATDividend.setDividendRecordDateStr(map, getDividendRecordDateStr());
		return map;
	}

	@Override
	public String getOccId() {
		String occId = (String) get(IATAssetConstants.OCCID);
		// if (StringUtils.isBlank(occId)) {
		// occId = getAtId();
		// }
		return occId;
	}

	public void setOccId(String value_) {
		put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getDividendExDateStr() {
		return (String) get(IATAssetConstants.DIV_EX_DATE_STR_OLD);
	}

	public void setDividendExDateStr(String value_) {
		put(IATAssetConstants.DIV_EX_DATE_STR_OLD, value_);
	}

	@Override
	public Date getDividendExDate() {
		String dstr = getDividendExDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			Date date = ATFormats.DATE_yyyyMMdd.get().parse(dstr);
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}

	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public String getSource() {
		return (String) super.get(IATAssetConstants.SOURCE);
	}

	@Override
	public Double getDividend() {
		return (Double) super.get(IATAssetConstants.DIVIDEND);
	}

	public void setDividend(Double value_) {
		super.put(IATAssetConstants.DIVIDEND, value_);
	}

	@Override
	public Integer getDividendFrequency() {
		return (Integer) super.get(IATAssetConstants.DIV_FREQUENCY);
	}

	public void setDividendFrequency(Integer value_) {
		super.put(IATAssetConstants.DIV_FREQUENCY, value_);
	}

	@Override
	public Date getDividendRecordDate() {
		String dstr = getDividendRecordDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			Date date = ATFormats.DATE_yyyyMMdd.get().parse(dstr);
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}

	@Override
	public Date getDividendPayDate() {
		String dstr = getDividendPayDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			Date date = ATFormats.DATE_yyyyMMdd.get().parse(dstr);
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}

	@Override
	public String getDividendRecordDateStr() {
		return (String) get(IATAssetConstants.DIV_REC_DATE_STR);
	}

	public void setDividendRecordDateStr(String value_) {
		super.put(IATAssetConstants.DIV_REC_DATE_STR, value_);
	}

	@Override
	public String getDividendPayDateStr() {
		return (String) get(IATAssetConstants.DIV_PAY_DATE_STR);
	}

	public void setDividendPayDateStr(String value_) {
		super.put(IATAssetConstants.DIV_PAY_DATE_STR, value_);
	}

	@Override
	public String getDividendEstimatedDS8() {
		return IATDividend.getDividendEstimatedDS8(this);
	}

	public void setDividendEstimatedDS8(String value_) {
		IATDividend.setDividendEstimatedDS8(this, value_);
	}

	@Override
	public Date getDividendEstimatedDate() {
		return IATDividend.getDividendEstimatedDate(this);
	}

}
