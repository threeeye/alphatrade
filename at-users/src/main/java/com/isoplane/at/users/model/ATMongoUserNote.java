package com.isoplane.at.users.model;

import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.model.IATUserNote;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoUserNote extends Document implements IATMongo, IATLookupMap, IATUserNote {

	private static final long serialVersionUID = 1L;

	public ATMongoUserNote() {
	}

	public ATMongoUserNote(IATUserNote tmpl_) {
		this.setUserId(tmpl_.getUserId());
		this.setOccId(tmpl_.getOccId());
		this.setNote(tmpl_.getNote());
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> tmpl_) {
		super.putAll(tmpl_);
	}

	@Override
	public String getAtId() {
		return IATMongo.getMongoId(this);
	}

	@Override
	public void setAtId(String value_) {
		IATMongo.setMongoId(this, value_);
	}

	@Override
	public String getOccId() {
		return IATUserAlert.getOccId(this);
	}

	public void setOccId(String value_) {
		IATUserAlert.setOccId(this, value_);
	}

	@Override
	public String getUserId() {
		return IATUserAlert.getUserId(this);
	}

	public void setUserId(String value_) {
		IATUserAlert.setUserId(this, value_);
	}

	@Override
	public String getNote() {
		return IATUserNote.getNote(this);
	}

	public void setNote(String value_) {
		IATUserNote.setNote(this, value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

}
