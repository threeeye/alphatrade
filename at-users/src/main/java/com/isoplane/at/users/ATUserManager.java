package com.isoplane.at.users;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATOpportunityClip;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserNote;
import com.isoplane.at.commons.model.ATUserNote.ATUserNotesPack;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.ATUserSetting;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.IATUserNote;
import com.isoplane.at.commons.model.alertcriteria.ATBaseCriteria;
import com.isoplane.at.commons.model.alertcriteria.ATCriteriaUtil;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.service.IATSessionListener;
import com.isoplane.at.commons.service.IATSessionTokenProcessor;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATUserAlertListener;
import com.isoplane.at.commons.service.IATUserListener;
import com.isoplane.at.commons.service.IATUserNotesListener;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.users.util.ATUserMongodbTools;

public class ATUserManager
		implements IATUserProvider, IATSessionListener, IATUserListener, IATUserAlertListener, IATUserNotesListener,
		IATSystemStatusProvider, IATProtoUserSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATUserManager.class);

	private boolean _isStreaming;
	private ATUserMongodbTools _mongodb;
	private IATSessionTokenProcessor _auth;
	private Map<String, ATSession> _sessionMap;
	private Map<String, ATUser> _userMap;
	private Set<String> _activeUserIds;
	private Set<String> _readonlyActiveUserIds;
	private boolean _isMasterSessionList;
	private Set<IATSessionListener> _sessionListeners;
	private Set<IATUserAlertListener> _alertListeners;
	private Set<IATUserNotesListener> _noteListeners;
	// private ATKafkaEventbusConsumer _eventbusListener;

	public static void main(String[] args_) {
		try {
			// String[] propertiesPaths = args_[0].split(",");
			// CompositeConfiguration cconfig = new CompositeConfiguration();
			// for (String path : propertiesPaths) {
			// log.info(String.format("Reading properties [%s]", path));
			// PropertiesConfiguration config = new Configurations().properties(new File(path));
			// cconfig.addConfiguration(config);
			// }

			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);

			ATUserManager instance = new ATUserManager(null, true);
			instance.initService();

			ATSysUtils.waitForEnter();
			instance.shutdown();
			ATExecutors.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATUserManager.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATUserManager(IATSessionTokenProcessor authSvc_, boolean isStreaming_) {
		Configuration config = ATConfigUtil.config();
		_auth = authSvc_;
		_isStreaming = isStreaming_;
		_sessionMap = new ConcurrentHashMap<>();
		_userMap = new ConcurrentHashMap<>();
		_activeUserIds = new ConcurrentSkipListSet<>();
		_readonlyActiveUserIds = Collections.unmodifiableSet(_activeUserIds);
		_isMasterSessionList = config.getBoolean("users.isMasterList", false);
		ATSystemStatusManager.register(this);
	}

	public void initService() {
		Configuration config = ATConfigUtil.config();
		log.info(String.format("Masterlist [%b]", _isMasterSessionList));
		_mongodb = new ATUserMongodbTools();
		_mongodb.init();
		initStreaming();

		if (_isMasterSessionList) {
			if (_auth == null) {
				String msg = String.format("initService Error: %s not initialized", IATSessionTokenProcessor.class.getSimpleName());
				throw new ATException(msg);
			}
			// Sync fb users to mongo db
			Map<String, String> users = _auth.listUsers();
			_mongodb.updateUsers(users);
		}

		loadUsersSync();
		loadSessionsSync();

		if (_isMasterSessionList) {
			long cleanerInterval = config.getLong("users.cleanerInterval");
			String threadId = String.format("%s.cleaner", getClass().getSimpleName());
			ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {

				@Override
				public void run() {
					cleanupSessions();
				}
			}, cleanerInterval, cleanerInterval);
		}
	}

	private void initStreaming() {
		if (!_isStreaming)
			return;

		ATKafkaUserSessionConsumer sessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		sessionConsumer.subscribe(this);

		// eventbusConsumer.subscribe(this);
	}

	private void cleanupSessions() {
		Configuration config = ATConfigUtil.config();
		long sessionTimeout = config.getLong("users.sessionTimeout");
		long cutoff = System.currentTimeMillis() - sessionTimeout;
		IATWhereQuery query = ATPersistenceRegistry.query().lt(IATLookupMap.UPDATE_TIME, cutoff);
		long deleteCount = _mongodb.deleteSession(query);
		if (deleteCount > 0) {
			log.info(String.format("Deleted [%d] timeout sessions", deleteCount));
		}
	}

	private void loadSessionsSync() {
		cleanupSessions();
		IATWhereQuery query = ATPersistenceRegistry.query().empty();
		List<ATSession> sessions = _mongodb.querySession(query);
		Map<String, ATSession> smap = new ConcurrentHashMap<>();
		for (ATSession session : sessions) {
			smap.put(session.getToken(), session);
		}
		_sessionMap = smap;
		updateUserIds();
	}

	private void loadUsersSync() {
		IATWhereQuery query = ATPersistenceRegistry.query().empty();
		List<IATUser> users = _mongodb.queryUsers(query);
		Map<String, ATUser> smap = new ConcurrentHashMap<>();
		for (IATUser user : users) {
			smap.put(user.getAtId(), new ATUser(user));
		}
		_userMap = smap;
	}

	@Override
	public ATUser getUser(String userId_) {
		ATUser user = _userMap.get(userId_);
		return user;
	}

	@Override
	public boolean updateUser(ATUser user_) {
		return _mongodb.updateUser(user_);
	}

	@Override
	public void logUserActivity(String ip_, String userId_, String activity_, String data_) {
		ATUser user = userId_ != null ? getUser(userId_) : null;
		String activity = activity_;
		if (user == null && !StringUtils.isBlank(userId_)) {
			log.error(String.format("User not found [%s]", userId_));
			user = new ATUser();
			user.setAtId(userId_);
			activity = String.format("Unknown user: %s", activity);
		}
		_mongodb.saveUserActivity(new Date(), ip_, user, activity, data_);
	}

	@Override
	public List<ATUserAlertRulePack> getUserAlerts(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().empty();
		if (!StringUtils.isBlank(userId_))
			query = query.eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(symbol_))
			query = query.eq(IATAssetConstants.OCCID, symbol_);
		List<ATUserAlertRulePack> packs = _mongodb.queryUserAlerts(query);
		return packs;
	}

	@Override
	public boolean saveUserAlerts(String userId_, ATUserAlertRulePack pack_) {
		List<ATAlertRule> alerts = new ArrayList<>();
		if (pack_ == null || pack_.alerts != null && pack_.alerts.length > 0) {
			for (ATAlertRule alert : pack_.alerts) {
				alert.cleanup();
				Set<ATBaseCriteria> criteria = ATCriteriaUtil.createCriteria(userId_, alert);
				if (criteria == null || criteria.isEmpty()) {
					log.warn(String.format("Skipping invalid alert: %s", alert));
					continue;
				}
				alerts.add(alert);
			}
			if (alerts.isEmpty())
				return false;
		}
		pack_.alerts = alerts.toArray(new ATAlertRule[0]);
		boolean result = _mongodb.saveUserAlerts(userId_, pack_) == alerts.size();
		return result;
	}

	@Override
	public boolean deleteUserAlerts(String userId_, String symbol_) {
		if (StringUtils.isBlank(userId_))
			return false;
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATAssetConstants.OCCID, symbol_);
		}
		long count = _mongodb.deleteUserAlerts(query);
		return count != 0;
	}

	@Override
	public List<ATUserNote> getUserNotes(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().empty();
		if (!StringUtils.isBlank(userId_))
			query = query.eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(symbol_))
			query = query.eq(IATAssetConstants.OCCID, symbol_);
		List<IATUserNote> dbNotes = _mongodb.queryUserNotes(query);
		List<ATUserNote> userNotes = dbNotes != null ? dbNotes.stream().map(a -> new ATUserNote(a)).collect(Collectors.toList()) : null;
		return userNotes;
	}

	@Override
	public boolean saveUserNote(String userId_, IATUserNote note_) {
		boolean result = _mongodb.saveUserNote(userId_, note_);
		return result;
	}

	@Override
	public List<ATBroker> getUserBrokers(String userId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		List<ATBroker> brokers = _mongodb.queryUserBrokers(query);
		return brokers;
	}

	@Override
	public boolean saveUserBroker(String userId_, ATBroker broker_) {
		return _mongodb.saveUserBroker(userId_, broker_);
	}

	// @Deprecated
	//// @Override
	// public List<ATSecurityResearchConfig> saveSecurityResearchConfigs(String userId_, String type_, IATSecurityResearchConfig... configs_) {
	// if (configs_ != null) {
	// for (IATSecurityResearchConfig config : configs_) {
	// config.setAtId(null);
	// config.setUserId(userId_);
	// }
	// }
	// boolean updateResult = _mongodb.saveSecurityResearchConfigs(userId_, type_, configs_);
	// if (updateResult) {
	// return getSecurityResearchConfigs(userId_, type_);
	// }
	// return null;
	// }

	// @Deprecated
	//// @Override
	// public List<ATSecurityResearchConfig> getSecurityResearchConfigs(String userId_, String type_) {
	// IATWhereQuery query = ATPersistenceRegistry.query().empty();
	// if (!StringUtils.isBlank(userId_))
	// query = query.eq(IATAssetConstants.USER_ID, userId_);
	// if (!StringUtils.isBlank(type_) && !IATSecurityResearchConfig.TYPE_OPTION.equals(type_))
	// query = query.eq(IATAssetConstants.TYPE, type_);
	// List<IATSecurityResearchConfig> dbConfigs = _mongodb.querySecurityResearchConfigs(query);
	// List<ATSecurityResearchConfig> userConfigs = dbConfigs != null
	// ? dbConfigs.stream().map(a -> new ATSecurityResearchConfig(a)).collect(Collectors.toList())
	// : null;
	// return userConfigs;
	// }

	private void updateUserIds() {
		Set<String> userIds = _sessionMap.values().stream().map(s_ -> s_.getUserId()).collect(Collectors.toSet());
		_activeUserIds.clear();
		_activeUserIds.addAll(userIds);
	}

	@Override
	public String getUserId(String sessionId_) {
		Configuration config = ATConfigUtil.config();
		ATSession session = _sessionMap.get(sessionId_);
		if (session == null)
			return null;
		long sessionTimeout = config.getLong("users.sessionTimeout");
		if (session.getUpdateTime() + sessionTimeout < System.currentTimeMillis())
			return null;
		String userId = session.getUserId();
		return userId;
	}

	@Override
	public ATUserSetting getUserSetting(String userId_, String settingId_) {
		ATUserSetting setting = _mongodb.getUserSetting(userId_, settingId_);
		return setting;
	}

	public boolean saveUserSetting(ATUserSetting setting_) {
		boolean result = _mongodb.saveUserSetting(setting_);
		return result;
	}

	@Override
	public Set<String> getActiveUserIds() {
		return _readonlyActiveUserIds;
	}

	@Override
	public void removeMessageTokens(Set<String> failedTokens_) {
		if (failedTokens_ == null || failedTokens_.isEmpty())
			return;
		for (ATUser user : _userMap.values()) {
			List<String> userTokens = user.getMessageTokens();
			if (userTokens == null || userTokens.isEmpty())
				continue;
			userTokens = new ArrayList<>(userTokens);
			boolean isFlagged = false;
			Iterator<String> i = userTokens.iterator();
			while (i.hasNext()) {
				String token = i.next();
				token = token.substring(token.indexOf(':'));
				if (failedTokens_.contains(token)) {
					log.info(String.format("Removing message token from user [%s]: %s", user.getAtId(), token));
					isFlagged = true;
					i.remove();
				}
			}
			if (!isFlagged)
				continue;
			user.setMessageTokens(userTokens);
			_mongodb.updateUser(user);
		}
	}

	@Override
	public boolean removeSession(String sessionId_) {
		if (_auth == null) {
			String msg = String.format("removeSession Error: %s not initialized", IATSessionTokenProcessor.class.getSimpleName());
			throw new ATException(msg);
		}
		if (StringUtils.isBlank(sessionId_)) {
			log.warn(String.format("Missing logout session id [%s]", sessionId_));
			return false;
		}
		boolean result = false;
		ATSession delSession = _sessionMap.remove(sessionId_);
		if (delSession != null) {
			result = true;
			_auth.revokeSessionToken(delSession.getRefreshToken());
			String userId = delSession.getUserId();
			boolean isUserActive = _sessionMap.values().stream().anyMatch(s -> s.getUserId().equals(userId));
			if (!isUserActive) {
				_activeUserIds.remove(userId);
				for (IATSessionListener sub : _sessionListeners) {
					sub.notify(EATChangeOperation.DELETE, delSession);
				}
			}
		}
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, sessionId_);
		long deleteCount = _mongodb.deleteSession(query);
		log.info(String.format("Terminated [%d] sessions [%s]", deleteCount, sessionId_));
		return result;
	}

	@Override
	public boolean logoutUser(String userId_) {
		if (StringUtils.isBlank(userId_)) {
			log.warn(String.format("Missing logout user id [%s]", userId_));
			return false;
		}
		Set<String> deleteIds = _sessionMap.entrySet().stream().filter(e -> e.getValue().getUserId().equals(userId_))
				.map(s -> s.getKey()).collect(Collectors.toSet());
		boolean result = true;
		for (String sessionId : deleteIds) {
			result |= this.removeSession(sessionId);
		}
		return result;
	}

	@Override
	public IATSession registerSession(IATSession session_) {
		if (_auth == null) {
			String msg = String.format("registerSession Error: %s not initialized", IATSessionTokenProcessor.class.getSimpleName());
			throw new ATException(msg);
		}
		ATSession createdSession = _auth.verifySessionToken(session_);
		if (createdSession != null) {
			ATUser user = _userMap.get(createdSession.getUserId());
			createdSession.setRoles(user.getRoles());
			_sessionMap.put(createdSession.getToken(), createdSession); // We know this is ATSession
			_mongodb.save(createdSession);
			// boolean isNewUser = _activeUserIds.add(createdSession.getUserId());
			// if (true || isNewUser) {
			for (IATSessionListener subs : _sessionListeners) {
				subs.notify(EATChangeOperation.CREATE, createdSession);
			}
			// }
		} else {
			log.error(String.format("Failed to register session: %s", session_));
		}
		return createdSession;
	}

	@Override
	public IATSession verifySession(String sessionId_, String clientIp_, Long activityTs_) {
		ATSession session = _sessionMap.get(sessionId_);
		if (session == null || clientIp_ == null || !clientIp_.equals(session.getClientIp())) {
			throw new ATException(String.format("Invalid session [%s/%s]", sessionId_, clientIp_));
		}
		Configuration config = ATConfigUtil.config();
		long now = System.currentTimeMillis();
		long sessionTimeout = config.getLong("users.sessionTimeout");
		// long tokenAge = System.currentTimeMillis() - session.getUpdated();
		if (session.getUpdateTime() + sessionTimeout < now) {
			IATWhereQuery delQuery = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, session.getToken());
			_mongodb.deleteSession(delQuery);
			log.error(String.format("Authentication timeout [%s / %s]", session.getUserId(), clientIp_));
			return null;
		}
		// NOTE: Only update timestamp when user is active
		boolean activityTimedOut = activityTs_ > 0 && activityTs_ + sessionTimeout < now;
		if (!activityTimedOut) {
			session.setUpdateTime(now);
			_mongodb.updateSession(sessionId_);
		} else {
			log.info(String.format("Detected inactive user [%s/%s]", clientIp_, session.getUserId()));
		}
		return session;
	}

	@Override
	public long getSessionTimeout() {
		Configuration config = ATConfigUtil.config();
		long sessionTimeout = config.getLong("users.sessionTimeout");
		return sessionTimeout;
	}

	private void shutdown() {
		_mongodb.shutdown();
	}

	@Override
	public void notify(EATChangeOperation operation_, ATSession session_) {
		log.debug(String.format("%s - %s", operation_, session_));
		ATSession session = (session_ instanceof ATSession) ? (ATSession) session_ : new ATSession(session_);
		switch (operation_) {
		case ADD:
		case UPDATE:
			_sessionMap.put(session.getToken(), session); // We know this is ATSession
			break;
		case DELETE:
			_sessionMap.remove(session.getToken());
		default:
			if (log.isDebugEnabled())
				log.debug(String.format("Ignoring [%s]: %s", operation_, session_));
			return;
		}
		updateUserIds();
	}

	@Override
	public void notifyUserSession(EATChangeOperation operation_, ATSession session_) {
		notify(operation_, session_);
	}

	@Override
	public void notify(EATChangeOperation operation_, IATUser user_) {
		log.debug(String.format("%s - %s", operation_, user_));
		ATUser user = (user_ instanceof ATUser) ? (ATUser) user_ : new ATUser(user_);
		switch (operation_) {
		case ADD:
		case UPDATE:
			_userMap.put(user.getAtId(), user);
			break;
		case DELETE:
			_userMap.remove(user.getAtId());
		default:
			if (log.isDebugEnabled())
				log.debug(String.format("Ignoring [%s]: %s", operation_, user));
			return;
		}
	}

	@Override
	public void notify(ATUserAlertRulePack alertPack_) {
		if (_alertListeners == null || alertPack_ == null)
			return;
		for (IATUserAlertListener listener : _alertListeners) {
			try {
				listener.notify(alertPack_);
			} catch (Exception ex) {
				log.error(String.format("Error notifying: %s", listener), ex);
			}
		}
	}

	@Override
	public void notify(ATUserNotesPack notesPack_) {
		if (_noteListeners == null || notesPack_ == null)
			return;
		for (IATUserNotesListener listener : _noteListeners) {
			try {
				listener.notify(notesPack_);
			} catch (Exception ex) {
				log.error(String.format("Error notifying: %s", listener), ex);
			}
		}
	}

	// @Override
	// public void notify(ATSecurityResearchConfigPack configPack_) {
	// if (_secConfigListeners == null || configPack_ == null)
	// return;
	// for (IATSecurityResearchConfigListener listener : _secConfigListeners) {
	// try {
	// listener.notify(configPack_);
	// } catch (Exception ex) {
	// log.error(String.format("Error notifying: %s", listener), ex);
	// }
	// }
	// }

	// Monitoring

	@Override
	public void register(IATSessionListener listener_) {
		if (_sessionListeners == null) {
			_sessionListeners = new HashSet<>();
		}
		_sessionListeners.add(listener_);
	}

	@Override
	public void register(IATUserAlertListener listener_) {
		if (_alertListeners == null) {
			_alertListeners = new HashSet<>();
		}
		_alertListeners.add(listener_);
	}

	@Override
	public void register(IATUserNotesListener listener_) {
		if (_noteListeners == null) {
			_noteListeners = new HashSet<>();
		}
		_noteListeners.add(listener_);
	}

	// @Override
	// public void register(IATSecurityResearchConfigListener listener_) {
	// if (_secConfigListeners == null) {
	// _secConfigListeners = new HashSet<>();
	// }
	// _secConfigListeners.add(listener_);
	// }

	// @Override
	// public String clearSessions() {
	// IATWhereQuery query = ATPersistenceRegistry.query().empty();
	// long deleteCount = _mongodb.deleteSession(query);
	// _sessionMap.clear();
	// _activeUserIds.clear();
	// String msg = String.format("Deleted [%d] sessions", deleteCount);
	// log.info(msg);
	// return msg;
	// }

	// @Override
	// public Long getSessionMapMemory() {
	// return ATMBeanUtil.getMemoryUsage(_sessionMap);
	// }

	private Map<String, String> getMonitoringStatus() {
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("count.sessions", _sessionMap.size() + "");
		statusMap.put("count.users", _activeUserIds.size() + "");
		statusMap.put("userIds", String.join(", ", getActiveUserIds()));
		return statusMap;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		return getMonitoringStatus();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public ATUserEquityModel saveUserEquityModel(ATUserEquityModel model_) {
		if (StringUtils.isBlank(model_.getId())) {
			model_.setId(String.format("%s:%s", model_.getUserId(), model_.getLabel()));
		}
		return _mongodb.saveUserEquityModel(model_);
	}

	@Override
	public List<ATUserEquityModel> getUserEquityModels(String userId_) {
		return _mongodb.loadUserEquityModels(userId_);
	}

	@Override
	public Boolean deleteUserEquityModel(String userId_, String atid_) {
		return _mongodb.deleteUserEquityModel(userId_, atid_) == 1;
	}

	@Override
	public ATUserOpportunityConfig saveUserOpportunityConfig(ATUserOpportunityConfig config_) {
		if (StringUtils.isBlank(config_.getId())) {
			config_.setId(String.format("%s:%s", config_.getUserId(), config_.getLabel()));
		}
		return _mongodb.saveUserOpportunityConfig(config_);
	}

	@Override
	public List<ATUserOpportunityConfig> getUserOpportunityConfigs(String userId_) {
		return _mongodb.loadUserOpportunityConfigs(userId_);
	}

	@Override
	public Boolean deleteUserOpportunityConfig(String userId_, String atid_) {
		return _mongodb.deleteUserOpportunityConfig(userId_, atid_) == 1;
	}

	@Override
	public List<ATOpportunityClip> updateOpportunityClips(String userId_, Set<ATOpportunityClip> clips_) {
		if (StringUtils.isBlank(userId_) || clips_ == null || clips_.isEmpty())
			return null;
		List<String> symbols = clips_.stream().map(c_ -> c_.getSymbol()).distinct().collect(Collectors.toList());
		if (symbols == null || symbols.size() != 1) {
			log.error(String.format("Only supporting single symbols %s", symbols));
		}
		String symbol = symbols.get(0);
		_mongodb.deleteOpportunityClips(userId_, symbol);
		if (clips_.size() > 1 || clips_.iterator().next().getComponents() != null) {
			_mongodb.saveOpportunityClips(userId_, clips_);
		}
		List<ATOpportunityClip> outClips = getOpportunityClips(userId_, symbol);
		return outClips;
	}

	@Override
	public List<ATOpportunityClip> getOpportunityClips(String userId_, String symbol_) {
		List<ATOpportunityClip> outClips = _mongodb.loadOpportunityClips(userId_, symbol_);
		return outClips;
	}

	@Override
	public boolean deleteWatchlist(String userId_, String atid_) {
		String prohibitedId = String.format("%s:1", userId_);
		if (prohibitedId.equals(atid_))
			return false;
		return _mongodb.deleteWatchlist(userId_, atid_) == 1;
	}

	@Override
	public ATWatchlist saveWatchlist(ATWatchlist data_) {
		if (StringUtils.isBlank(data_.getId())) {
			data_.setId(String.format("%s:%s", data_.getUserId(), data_.getLabel()));
		}
		return _mongodb.saveWatchlist(data_);
	}

	@Override
	public List<ATWatchlist> getWatchlists(String userId_) {
		return _mongodb.loadWatchlists(userId_);
	}

	@Override
	public List<ATUser> getUsers() {
		return new ArrayList<>(_userMap.values());
	}

	@Override
	public Map<String, Object> importUserData(String userId_, ATBroker[] brokers_, ATUserNote[] notes_, ATUserAlertRulePack[] alertPacks_,
			ATUserEquityModel[] models_, boolean isPurgeExisting_) {
		Map<String, Object> resultMap = new HashMap<>();
		if (isPurgeExisting_) {
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
			long deleteBrokerCount = _mongodb.deleteUserBrokers(deleteQuery);
			resultMap.put("brokers.deleted", deleteBrokerCount);
			long deleteAlertCount = _mongodb.deleteUserAlerts(deleteQuery);
			resultMap.put("alerts.deleted", deleteAlertCount);
			long deleteNoteCount = _mongodb.deleteUserNotes(deleteQuery);
			resultMap.put("notes.deleted", deleteNoteCount);
			long deleteModelEqtCount = _mongodb.deleteUserEquityModel(userId_, null);
			resultMap.put("modelsEqt.deleted", deleteModelEqtCount);
		}
		int brokerSaveCount = 0;
		if (brokers_ != null && brokers_.length > 0) {
			for (ATBroker broker : brokers_) {
				broker.setUser(userId_);
				if (saveUserBroker(userId_, broker)) {
					brokerSaveCount++;
				}
			}
		}
		resultMap.put("brokers.inserted", brokerSaveCount);

		int alertCount = alertPacks_ != null ? alertPacks_.length : 0;
		int alertSaveCount = 0;
		if (alertCount != 0) {
			for (ATUserAlertRulePack pack : alertPacks_) {
				pack.usrId = userId_;
				// if (pack.alerts != null) {
				// for (ATAlertRule alert : pack.alerts) {
				// alert.setUserId(userId_);
				// }
				// }
				alertSaveCount += _mongodb.saveUserAlerts(userId_, pack);
			}
		}
		resultMap.put("alerts.inserted", alertSaveCount == alertCount ? alertSaveCount : String.format("%d/%d", alertSaveCount, alertCount));

		int noteCount = notes_ != null ? notes_.length : 0;
		int noteSaveCount = 0;
		if (notes_ != null && notes_.length > 0) {
			for (ATUserNote note : notes_) {
				note.setUserId(userId_);
				if (_mongodb.saveUserNote(userId_, note)) {
					noteSaveCount++;
				}
			}
		}
		resultMap.put("notes.inserted", noteSaveCount == noteCount ? noteSaveCount : String.format("%d/%d", noteSaveCount, noteCount));

		int modelEqtCount = models_ != null ? models_.length : 0;
		int modelEqtSaveCount = 0;
		if (models_ != null && models_.length > 0) {
			for (ATUserEquityModel model : models_) {
				model.setUserId(userId_);
				model.setId(null);
				if (_mongodb.saveUserEquityModel(model) != null) {
					modelEqtSaveCount++;
				}
			}
		}
		resultMap.put("modelsEqt.inserted",
				modelEqtSaveCount == modelEqtCount ? modelEqtSaveCount : String.format("%d/%d", modelEqtSaveCount, modelEqtCount));

		return resultMap;
	}

}
