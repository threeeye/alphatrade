package com.isoplane.at.users.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATAlertRuleComponent;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATOpportunityClip;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.ATUserSetting;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.model.IATTable;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.model.IATUserNote;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentBridgeMap;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.users.model.ATMongoSession;
import com.isoplane.at.users.model.ATMongoUser;
import com.isoplane.at.users.model.ATMongoUserNote;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATUserMongodbTools { // implements ATUserMongodbToolsMBean {

	static final Logger log = LoggerFactory.getLogger(ATUserMongodbTools.class);

//	private static final Gson _gson = new Gson();

	// private Configuration _config;
//	private boolean _isStreaming;
//	private IATSessionListener _sessionListener;
//	private IATUserListener _userListener;
//	private IATUserAlertListener _alertsListener;
//	private IATUserNotesListener _notesListener;
	// private IATSecurityResearchConfigListener _configListener;
	// private Map<String, ATMongoSession> _sessionMap;
	private String _sessionTable;
	private String _userActivityTable;
	private String _usersTable;
	@Deprecated
	private String _alertsTableOld;
	private String _alertTable;
	private String _brokerTable;
	private String _notesTable;
	private String _eqtModelTable;
	private String _watchlistTable;
	// private String _secResearchConfigTable;

	private ATMongoDao _dao;

	public ATUserMongodbTools() { // IATSecurityResearchConfigListener configListener_
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;
//		_isStreaming = isStreaming_;
//		_sessionListener = sessionListener_;
//		_userListener = userListener_;
//		_alertsListener = alertsListener_;
//		_notesListener = notesListener_;
		// _configListener = configListener_;
		// _sessionMap = new ConcurrentHashMap<>();

		// _alertsTableOld = ATMongoTableRegistry.getUserAlertsTable();
		_alertTable = ATMongoTableRegistry.getUserAlertTable();
		_brokerTable = ATMongoTableRegistry.getUserBrokerTable();
		// _secResearchConfigTable = ATMongoTableRegistry.getUserResearchConfigTable();
		_notesTable = ATMongoTableRegistry.getUserNotesTable();
		_sessionTable = ATMongoTableRegistry.getUserSessionTable();
		_userActivityTable = ATMongoTableRegistry.getUserActivityTable();
		_usersTable = ATMongoTableRegistry.getUserTable();
		_eqtModelTable = ATMongoTableRegistry.getUserEquityModelTable();
		_watchlistTable = ATMongoTableRegistry.getUserWatchlistTable();

		// ATMBeanUtil.register(this);
	}

	public ATUserMongodbTools init() {
		_dao = new ATMongoDao();

		// readSessionsSync();

// 		if (_isStreaming) {
// //			monitorUsers();
// //			monitorSessions();
// //			monitorUserAlerts();
// //			monitorUserNotes();
// 			log.error(String.format("#################################"));
// 			log.error(String.format("#                               #"));
// 			log.error(String.format("# REMOVE MONGO REACTIVE DRIVER! #"));
// 			log.error(String.format("#                               #"));
// 			log.error(String.format("#################################"));
// 			// monitorSecurityResearchConfigs();
// 		}
		return this;
	}

	// private void readSessionsSync() {
	// List<ATMongoSession> sessions = _dao.query(_sessionTable, ATPersistenceRegistry.query().empty(), ATMongoSession.class);
	// Map<String, ATMongoSession> sessionMap = new ConcurrentHashMap<>();
	// for (ATMongoSession session : sessions) {
	// _sessionMap.put(session.getAtId(), session);
	// }
	// _sessionMap = sessionMap;
	// log.info(String.format("Loaded [%d] sessions", sessionMap.size()));
	// }

	// public IATSession getSession(String sessionId_) {
	// return _sessionMap.get(sessionId_);
	// }

	public boolean updateUser(ATUser user_) {
		if (user_ == null || StringUtils.isBlank(user_.getAtId()))
			return false;
		Iterator<Entry<String, Object>> i = user_.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> entry = i.next();
			if (entry.getValue() == null) {
				i.remove();
			}
		}
		user_.remove(IATMongo.MONGO_ID);
		// user_.remove(IATLookupMap.AT_ID);

		List<IATUser> oldUser = queryUsers(new ATMongoWhereQuery().eq(IATLookupMap.AT_ID, user_.getAtId()));
		String id = null;
		if (!oldUser.isEmpty()) {
			id = ((ATMongoUser) oldUser.get(0)).get(IATMongo.MONGO_ID).toString();
			// id = bla.toString();
		}

		boolean result = _dao.updateValues(_usersTable, id, user_, true);
		log.debug(String.format("Update user [%s/%b]", user_.getAtId(), result));
		return result;
	}

	public void updateUsers(Map<String, String> extUserMap_) {
		IATWhereQuery loadQuery = ATPersistenceRegistry.query().empty();
		List<ATMongoUser> dbUsers = _dao.query(_usersTable, loadQuery, ATMongoUser.class);
		Map<String, ATMongoUser> dbUserMap = dbUsers.stream()
				.collect(Collectors.toMap(ATMongoUser::getAtId, Function.identity()));
		int updateCount = 0;
		for (String extUserId : extUserMap_.keySet()) {
			String extEmail = extUserMap_.get(extUserId);
			if (StringUtils.isBlank(extEmail)) {
				log.error(String.format("User [%s] email blank", extUserId));
				continue;
			}
			ATMongoUser dbUser = dbUserMap.remove(extUserId);
			if (dbUser == null) {
				dbUser = new ATMongoUser();
				dbUser.setAtId(extUserId);
			}
			if (extEmail.equals(dbUser.getEmail()))
				continue;
			dbUser.setEmail(extEmail);
			if (_dao.updateValues(_usersTable, dbUser.getAtId(), dbUser, true))
				// if (updateUser(dbUser).update(_usersTable, dbUser))
				updateCount++;
		}
		int deleteCount = 0;
		for (String dbUserId : dbUserMap.keySet()) {
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, dbUserId);
			deleteCount += _dao.delete(_usersTable, deleteQuery);
		}
		log.info(String.format("Users updated [%d], deleted [%d]", updateCount, deleteCount));
	}

	// private void monitorSessions() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify session [%s]: %s", operation_, data_));
	// 			}
	// 			switch (operation_) {
	// 			case "DELETE":
	// 				BsonDocument bdoc = (BsonDocument) data_;
	// 				String delMongoId = bdoc.getString(IATMongo.MONGO_ID).getValue();
	// 				// String delId = ATMongoSession.fromMongoId(delMongoId);
	// 				// ATMongoSession delSession = _sessionMap.remove(delMongoId);
	// 				ATSession delSession = new ATSession();
	// 				delSession.setToken(delMongoId);
	// 				notifySessionListener(EATChangeOperation.DELETE, delSession);
	// 				break;
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 				Document doc = (Document) data_;
	// 				ATSession newSession = new ATSession(doc);
	// 				newSession.setToken(doc.getString(IATMongo.MONGO_ID));
	// 				EATChangeOperation op = doc.getBoolean(IATMongo.DISABLED, false)
	// 						? EATChangeOperation.DELETE
	// 						: ATMongoTools.toOperation(operation_);
	// 				notifySessionListener(op, newSession);
	// 				break;
	// 			default:
	// 				log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 				break;
	// 			}
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _sessionTable;// _config.getString(SESSION_TABLE_ID);
	// 		}
	// 	});
	// }

	public List<IATUser> queryUsers(IATWhereQuery query_) {
		List<ATMongoUser> mongoUsers = _dao.query(_usersTable, query_, ATMongoUser.class);
		// List<IATUser> users = mongoUsers.stream().collect(Collectors.toList());
		@SuppressWarnings("unchecked")
		List<IATUser> users = (List<IATUser>) (Object) mongoUsers;
		return users;
	}

	// private void monitorUsers() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify user [%s]: %s", operation_, data_));
	// 			}
	// 			switch (operation_) {
	// 			case "DELETE":
	// 				BsonDocument bdoc = (BsonDocument) data_;
	// 				String delMongoId = bdoc.getString(IATMongo.MONGO_ID).getValue();
	// 				log.warn(String.format("Deleted user [%s]", delMongoId));
	// 				break;
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 				Document doc = (Document) data_;
	// 				ATUser newUser = new ATUser(doc);
	// 				EATChangeOperation op = doc.getBoolean(IATMongo.DISABLED, false)
	// 						? EATChangeOperation.DELETE
	// 						: ATMongoTools.toOperation(operation_);
	// 				notifyUserListener(op, newUser);
	// 				break;
	// 			default:
	// 				log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 				break;
	// 			}
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _usersTable;// _config.getString(SESSION_TABLE_ID);
	// 		}
	// 	});
	// }

	public List<IATUserNote> queryUserNotes(IATWhereQuery query) {
		List<ATMongoUserNote> mongoUserNotes = _dao.query(_notesTable, query, ATMongoUserNote.class);
		@SuppressWarnings("unchecked")
		List<IATUserNote> notes = (List<IATUserNote>) (Object) mongoUserNotes;
		return notes;
	}

	public synchronized boolean saveUserNote(String userId_, IATUserNote note_) {
		if (StringUtils.isBlank(userId_) || note_ == null || StringUtils.isBlank(note_.getOccId()) || !userId_.equals(note_.getUserId()))
			return false;
		String occId = note_.getOccId().trim().toUpperCase();
		IATWhereQuery delQ = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_).eq(IATAssetConstants.OCCID, occId);
		long deleteCount = _dao.delete(_notesTable, delQ);
		log.debug(String.format("saveUserNote deleted [%d]", deleteCount));
		if (note_ == null || StringUtils.isBlank(note_.getNote()))
			return true;

		ATMongoUserNote outData = new ATMongoUserNote(note_);
		outData.setOccId(occId);
		String id = String.format("%s:%s:%d", userId_, occId, System.currentTimeMillis());
		IATMongo.setMongoId(outData, id);
		long result = _dao.fastBulkInsert(_notesTable, Arrays.asList(outData));
		return result == 1;
	}

	public long deleteUserNotes(IATWhereQuery query_) {
		long deleteCount = _dao.delete(_notesTable, query_);
		return deleteCount;
	}

	// private void monitorUserNotes() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify user note [%s]: %s", operation_, data_));
	// 			}
	// 			String id = null;
	// 			switch (operation_) {
	// 			case "DELETE":
	// 				id = ((BsonDocument) data_).getString(IATMongo.MONGO_ID).getValue();
	// 				break;
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 				id = ((Document) data_).getString(IATMongo.MONGO_ID);
	// 				break;
	// 			default:
	// 				log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 				break;
	// 			}
	// 			String[] tokens = StringUtils.isBlank(id) ? null : id.split(":");
	// 			if (tokens == null || tokens.length < 2)
	// 				return;
	// 			String userId = tokens[0];
	// 			String symbol = tokens[1];
	// 			notifyNotesListener(userId, symbol);
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _notesTable;
	// 		}
	// 	});
	// }

	public long saveUserAlerts(String userId_, ATUserAlertRulePack pack_) {
		if (StringUtils.isBlank(userId_) || pack_ == null || StringUtils.isBlank(pack_.occId))
			return -1;
		IATWhereQuery delQ = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_).eq(IATAssetConstants.OCCID, pack_.occId);
		_dao.delete(_alertTable, delQ);
		if (pack_.alerts == null || pack_.alerts.length == 0)
			return 0;
		AlertBridgeMap bridgeMap = new AlertBridgeMap(pack_);
		@SuppressWarnings("unchecked")
		boolean success = _dao.update(_alertTable, IATLookupMap.AT_ID, bridgeMap);

		// this.migrateUserAlerts();

		return success ? pack_.alerts.length : 0;
	}

	public List<ATUserAlertRulePack> queryUserAlerts(IATWhereQuery query_) {
		List<AlertBridgeMap> bridgeMaps = _dao.query(_alertTable, query_, AlertBridgeMap.class);
		List<ATUserAlertRulePack> packs = new ArrayList<>();
		if (bridgeMaps == null || bridgeMaps.isEmpty())
			return packs;
		for (AlertBridgeMap bridge : bridgeMaps) {
			ATUserAlertRulePack pack = bridge.getPack();
			if (pack != null && pack.alerts != null && pack.alerts.length > 0) {
				packs.add(pack);
			}
		}
		return packs;
	}

	public long deleteUserAlerts(IATWhereQuery query_) {
		long deleteCount = _dao.delete(_alertsTableOld, query_);
		return deleteCount;
	}

	// private void migrateUserAlerts() {
	// List<ATUserAlertRulePack> packs = this.queryUserAlerts(null);
	// for (ATUserAlertRulePack pack : packs) {
	// for (ATAlertRule alert : pack.alerts) {
	// alert.setType("eqt");
	// }
	// saveUserAlerts(pack.usrId, pack);
	// }
	// }

	// private void monitorUserAlerts() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify user alert [%s]: %s", operation_, data_));
	// 			}
	// 			String id = null;
	// 			switch (operation_) {
	// 			case "DELETE":
	// 				id = ((BsonDocument) data_).getString(IATMongo.MONGO_ID).getValue();
	// 				break;
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 				id = ((Document) data_).getString(IATMongo.MONGO_ID);
	// 				break;
	// 			default:
	// 				log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 				break;
	// 			}
	// 			String[] tokens = StringUtils.isBlank(id) ? null : id.split(":");
	// 			if (tokens == null || tokens.length < 2)
	// 				return;
	// 			String userId = tokens[0];
	// 			String symbol = tokens[1];
	// 			notifyAlertsListener(userId, symbol);
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _alertTable;
	// 		}
	// 	});
	// }

	public List<ATBroker> queryUserBrokers(IATWhereQuery query_) {
		List<BrokerBridgeMap> bridgeList = _dao.query(_brokerTable, query_, BrokerBridgeMap.class);
		List<ATBroker> brokers = new ArrayList<>();
		if (bridgeList != null) {
			for (BrokerBridgeMap bridge : bridgeList) {
				ATBroker broker = new ATBroker(bridge);
				brokers.add(broker);
			}
		}
		return brokers;
	}

	public boolean saveUserBroker(String userId_, ATBroker broker_) {
		if (StringUtils.isBlank(userId_) || broker_ == null || StringUtils.isBlank(broker_.getId()) || !userId_.equals(broker_.getUser()))
			return false;
		String mongoId = String.format("%s:%s", broker_.getUser(), broker_.getId());
		BrokerBridgeMap map = new BrokerBridgeMap();
		map.putAll(broker_);
		map.setAtId(mongoId);
		boolean result = _dao.update(_brokerTable, true, map);
		return result;
	}

	public long deleteUserBrokers(IATWhereQuery query_) {
		long deleteCount = _dao.delete(_brokerTable, query_);
		return deleteCount;
	}

	public ATUserEquityModel saveUserEquityModel(ATUserEquityModel model_) {
		if (model_ == null || StringUtils.isBlank(model_.getLabel()) || model_.getFilters() == null || model_.getFilters().size() == 0) {
			throw new ATException(String.format("Invalid Equity Model"));
		}

		if (StringUtils.isBlank(model_.getId())) {
			model_.setId(String.format("%s:%s", model_.getUserId(), model_.getLabel()));
			// ObjectId m_id = new ObjectId();
			// model_.setId(m_id.toString());
		}
		ATPersistentBridgeMap bridge = new ATPersistentBridgeMap(IATLookupMap.AT_ID) {
			private static final long serialVersionUID = 1L;
		};
		bridge.putAll(model_);
		// bridge.put(IATMongo.MONGO_ID, model_.getId());
		// bridge.remove("id");
		boolean result = _dao.update(_eqtModelTable, true, bridge);
		return result ? model_ : null;
	}

	public List<ATUserEquityModel> loadUserEquityModels(String userId_) {
		List<ATUserEquityModel> result = new ArrayList<>();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		List<ATPersistentLookupBase> tmpls = _dao.query(_eqtModelTable, query, ATPersistentLookupBase.class);
		// List<ATPersistentBridgeMap> tmpls = _dao.query(_eqtModelTable, query, HashMap.class);
		if (tmpls == null)
			return result;
		for (ATPersistentLookupBase tmpl : tmpls) {
			ATUserEquityModel uem = new ATUserEquityModel(tmpl);
			result.add(uem);
		}
		return result;
	}

	public long deleteUserEquityModel(String userId_, String atid_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(atid_)) {
			query = query.eq(IATLookupMap.AT_ID, atid_);
		}
		long count = _dao.delete(_eqtModelTable, query);
		return count;
	}

	public ATUserOpportunityConfig saveUserOpportunityConfig(ATUserOpportunityConfig config_) {
		if (config_ == null || StringUtils.isBlank(config_.getLabel())) {
			throw new ATException(String.format("Invalid Opportunity Config"));
		}

		if (StringUtils.isBlank(config_.getId())) {
			config_.setId(String.format("%s:%s", config_.getUserId(), config_.getLabel()));
		}
		ATPersistentBridgeMap bridge = new ATPersistentBridgeMap(IATLookupMap.AT_ID) {
			private static final long serialVersionUID = 1L;
		};
		bridge.putAll(config_);
		String table = ATMongoTableRegistry.getUserOpportunityConfigTable();
		boolean result = _dao.update(table, true, bridge);
		return result ? config_ : null;
	}

	public List<ATUserOpportunityConfig> loadUserOpportunityConfigs(String userId_) {
		List<ATUserOpportunityConfig> result = new ArrayList<>();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		String table = ATMongoTableRegistry.getUserOpportunityConfigTable();
		List<ATPersistentLookupBase> tmpls = _dao.query(table, query, ATPersistentLookupBase.class);
		if (tmpls == null)
			return result;
		for (ATPersistentLookupBase tmpl : tmpls) {
			ATUserOpportunityConfig uem = new ATUserOpportunityConfig(tmpl);
			result.add(uem);
		}
		return result;
	}

	public int deleteUserOpportunityConfig(String userId_, String atid_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(atid_)) {
			query = query.eq(IATLookupMap.AT_ID, atid_);
		}
		String table = ATMongoTableRegistry.getUserOpportunityConfigTable();
		int count = (int) _dao.delete(table, query);
		return count;
	}

	public long deleteWatchlist(String userId_, String atid_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		if (!StringUtils.isBlank(atid_)) {
			query = query.eq(IATLookupMap.AT_ID, atid_);
		}
		long count = _dao.delete(_watchlistTable, query);
		return count;
	}

	public ATWatchlist saveWatchlist(ATWatchlist data_) {
		if (data_ == null || StringUtils.isBlank(data_.getLabel()) || data_.getSymbols() == null || data_.getSymbols().size() == 0) {
			throw new ATException(String.format("Invalid Watchlist"));
		}

		if (StringUtils.isBlank(data_.getId())) {
			data_.setId(String.format("%s:%s", data_.getUserId(), data_.getLabel()));
		}
		ATPersistentBridgeMap bridge = new ATPersistentBridgeMap(IATLookupMap.AT_ID) {
			private static final long serialVersionUID = 1L;
		};
		bridge.putAll(data_);
		boolean result = _dao.update(_watchlistTable, true, bridge);
		return result ? data_ : null;
	}

	public List<ATWatchlist> loadWatchlists(String userId_) {
		List<ATWatchlist> result = new ArrayList<>();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		List<ATPersistentLookupBase> tmpls = _dao.query(_watchlistTable, query, ATPersistentLookupBase.class);
		if (tmpls == null)
			return result;
		for (ATPersistentLookupBase tmpl : tmpls) {
			ATWatchlist wl = new ATWatchlist(tmpl);
			result.add(wl);
		}
		return result;
	}

	public long deleteSession(IATWhereQuery query_) {
		long count = _dao.delete(_sessionTable, query_);
		return count;
	}

	public List<ATSession> querySession(IATWhereQuery query_) {
		List<ATMongoSession> mongoSessions = _dao.query(_sessionTable, query_, ATMongoSession.class);
		List<ATSession> sessions = mongoSessions.stream().map(s_ -> new ATSession((IATSession) s_)).collect(Collectors.toList());
		return sessions;
	}

	public boolean save(ATSession session_) {
		if (session_ == null) {
			log.debug(String.format("No session to save"));
			return false;
		}
		try {
			ATMongoSession outSession = new ATMongoSession(session_);
			boolean result = _dao.update(_sessionTable, true, outSession);
			if (log.isDebugEnabled())
				log.debug(String.format("Session saved [%b]", result));
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error saving session: %s", session_), ex);
			return false;
		}
	}

	public boolean updateSession(String sessionId_) {
		boolean result = _dao.touch(_sessionTable, sessionId_);
		return result;
	}

	// private void notifySessionListener(EATChangeOperation operation_, ATSession session_) {
	// 	if (!_isStreaming || _sessionListener == null)
	// 		return;
	// 	try {
	// 		_sessionListener.notify(operation_, session_);
	// 	} catch (Exception ex) {
	// 		log.error("Error in notify", ex);
	// 	}
	// }

	// private void notifyUserListener(EATChangeOperation operation_, IATUser user_) {
	// 	if (!_isStreaming || _userListener == null)
	// 		return;
	// 	try {
	// 		_userListener.notify(operation_, user_);
	// 	} catch (Exception ex) {
	// 		log.error("Error in notify", ex);
	// 	}
	// }

//	private ScheduledFuture<?> alertListenerFuture;

	// private void notifyAlertsListener(String userId_, String symbol_) {
	// 	if (!_isStreaming || _alertsListener == null)
	// 		return;
	// 	try {
	// 		if (alertListenerFuture != null && !alertListenerFuture.isDone()) {
	// 			alertListenerFuture.cancel(false);
	// 		}
	// 		String taskId = String.format("%s.%s", this.getClass().getSimpleName(), "notifyAlertsListener");
	// 		alertListenerFuture = ATExecutors.schedule(taskId, () -> {
	// 			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
	// 			if (!StringUtils.isBlank(symbol_))
	// 				query = query.eq(IATAssetConstants.OCCID, symbol_);
	// 			List<ATUserAlertRulePack> packs = queryUserAlerts(query);
	// 			if (packs == null)
	// 				return;
	// 			for (ATUserAlertRulePack pack : packs) {
	// 				_alertsListener.notify(pack);
	// 			}
	// 		}, 1000L);
	// 	} catch (Exception ex) {
	// 		log.error("Error in notify", ex);
	// 	}
	// }

	//private ScheduledFuture<?> notesListenerFuture;

	// private void notifyNotesListener(String userId_, String symbol_) {
	// 	if (!_isStreaming || _alertsListener == null)
	// 		return;
	// 	try {
	// 		if (notesListenerFuture != null && !notesListenerFuture.isDone()) {
	// 			notesListenerFuture.cancel(false);
	// 		}
	// 		String taskId = String.format("%s.%s", this.getClass().getSimpleName(), "notifyNotesListener");
	// 		notesListenerFuture = ATExecutors.schedule(taskId, () -> {
	// 			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
	// 			if (!StringUtils.isBlank(symbol_))
	// 				query = query.eq(IATAssetConstants.OCCID, symbol_);
	// 			List<IATUserNote> dbNotes = queryUserNotes(query);
	// 			List<ATUserNote> userNotes = dbNotes != null ? dbNotes.stream().map(a -> new ATUserNote(a)).collect(Collectors.toList()) : null;
	// 			ATUserNotesPack pack = new ATUserNotesPack();
	// 			pack.usrId = userId_;
	// 			pack.occId = symbol_;
	// 			pack.notes = userNotes != null ? userNotes.toArray(new ATUserNote[0]) : null;
	// 			_notesListener.notify(pack);

	// 		}, 1000L);
	// 	} catch (Exception ex) {
	// 		log.error("Error in notify", ex);
	// 	}
	// }

	public void shutdown() {
		// TODO Auto-generated method stub

	}

	// MONITORING

	public boolean saveUserActivity(Date date_, String ip_, ATUser user_, String activity_, String data_) {
		Document doc = new Document();
		ObjectId id = new ObjectId();
		doc.put(IATMongo.MONGO_ID, id);
		// String userId = user_ != null ? user_.getAtId() : _rand.nextInt() + "";
		doc.put("ip", ip_);
		if (user_ != null) {
			doc.put(IATAssetConstants.USER_ID, user_.getAtId());
			String name = user_.getEmail();
			if (name != null) {
				doc.put(IATAssetConstants.NAME, user_.getEmail());
			}
		}
		String dStr = ATFormats.DATE_json.get().format(date_);
		doc.put(IATLookupMap.INSERT_TIME, dStr);
		doc.put("activity", activity_);
		if (!StringUtils.isBlank(data_)) {
			doc.put("data", data_);
		}
		long count = _dao.fastBulkInsert(_userActivityTable, Arrays.asList(doc));
		return count == 1;
	}

	// Helper Classes

	public static class BrokerBridgeMap extends ATPersistentBridgeMap {
		private static final long serialVersionUID = 1L;

		public BrokerBridgeMap() {
			super(IATMongo.MONGO_ID);
		}
	}

	public static class UserSettingBridgeMap extends ATPersistentBridgeMap {
		private static final long serialVersionUID = 1L;

		public UserSettingBridgeMap() {
			super(IATMongo.MONGO_ID);
		}

		public UserSettingBridgeMap(ATUserSetting setting_) {
			this();
			this.setAtId(String.format("%s:%s", setting_.getUserId(), setting_.getSettingId()));
			this.put(IATTable.DATA, setting_.getData());
			this.put(IATTable.SETTING_ID, setting_.getSettingId());
			this.put(IATAssetConstants.USER_ID, setting_.getUserId());
		}

		public ATUserSetting value() {
			ATUserSetting setting = new ATUserSetting();
			setting.setData(IATAsset.getString(this, IATTable.DATA));
			setting.setSettingId(IATAsset.getString(this, IATTable.SETTING_ID));
			setting.setUserId(IATAsset.getString(this, IATAssetConstants.USER_ID));
			return setting;
		}
	}

	public static class AlertBridgeMap extends ATPersistentBridgeMap {
		private static final long serialVersionUID = 1L;

		public AlertBridgeMap() {
			// super(IATLookupMap.AT_ID);
			super(IATMongo.MONGO_ID);
		}

		public AlertBridgeMap(ATUserAlertRulePack pack_) {
			this();
			// String id = String.format("%s:%s:%d", pack_.usrId, pack_.occId, System.currentTimeMillis());
			String id = String.format("%s:%s", pack_.usrId, pack_.occId);
			this.put(IATMongo.MONGO_ID, id);
			this.put(IATAssetConstants.USER_ID, pack_.usrId);
			this.put(IATAssetConstants.OCCID, pack_.occId);
			this.put(IATUserAlert.ALERT_GROUP_LOGIC, pack_.groupLogic);

			if (pack_.alerts != null && pack_.alerts.length > 0) {
				List<Serializable> alerts = new ArrayList<>();
				for (ATAlertRule alert : pack_.alerts) {
					HashMap<String, Object> map = new HashMap<>(alert);
					alerts.add(map);
				}
				this.put("alerts", alerts);
			}
		}

		public ATUserAlertRulePack getPack() {
			ATUserAlertRulePack pack = new ATUserAlertRulePack();
			pack.usrId = (String) this.get(IATAssetConstants.USER_ID);
			pack.occId = (String) this.get(IATAssetConstants.OCCID);
			pack.groupLogic = (String) this.get(IATUserAlert.ALERT_GROUP_LOGIC);
			Object ao = this.get("alerts");
			if (ao instanceof ArrayList<?> && !((List<?>) ao).isEmpty()) {
				ArrayList<ATAlertRule> alerts = new ArrayList<>();
				for (Object element : (List<?>) ao) {
					Document doc = (Document) element;
					ATAlertRule alert = new ATAlertRule(doc);
					alerts.add(alert);
					Object co = alert.getComponents();
					if (co instanceof ArrayList<?> && !((List<?>) co).isEmpty()) {
						ArrayList<ATAlertRuleComponent> cmps = new ArrayList<>();
						for (Object celement : (List<?>) co) {
							Document cdoc = (Document) celement;
							ATAlertRuleComponent cmp = ATAlertRuleComponent.cleanup(cdoc);
							cmps.add(cmp);
						}
						alert.setComponents(cmps);
					}
				}
				pack.alerts = alerts.toArray(new ATAlertRule[0]);
			}
			return pack;
		}
	}

	public ATUserSetting getUserSetting(String userId_, String settingId_) {
		if (StringUtils.isBlank(userId_) || StringUtils.isBlank(settingId_))
			return null;
		String table = ATMongoTableRegistry.getUserSettingTable();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_).and().eq(IATTable.SETTING_ID, settingId_);
		List<UserSettingBridgeMap> docs = _dao.query(table, query, UserSettingBridgeMap.class);
		if (docs == null || docs.size() != 1)
			return null;
		return docs.get(0).value();
	}

	public boolean saveUserSetting(ATUserSetting setting_) {
		if (setting_ == null || StringUtils.isBlank(setting_.getUserId()) || StringUtils.isBlank(setting_.getSettingId()))
			return false;
		UserSettingBridgeMap bridge = new UserSettingBridgeMap(setting_);
		String table = ATMongoTableRegistry.getUserSettingTable();
		boolean result = _dao.update(table, bridge);
		return result;
	}

	public int deleteOpportunityClips(String userId_, String symbol_) {
		if (StringUtils.isBlank(userId_) || StringUtils.isBlank(symbol_))
			return -1;
		String table = ATMongoTableRegistry.getUserOpportunityClipTable();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_).and().eq(IATAssetConstants.SYMBOL, symbol_);
		int count = (int) _dao.delete(table, query);
		return count;
	}

	public List<ATOpportunityClip> saveOpportunityClips(String userId_, Set<ATOpportunityClip> clips_) {
		if (StringUtils.isBlank(userId_) || clips_ == null || clips_.isEmpty()) {
			throw new ATException(String.format("Invalid Opportunity Clips"));
		}

		List<Document> toInsert = new ArrayList<>();
		for (ATOpportunityClip clip : clips_) {
			Document doc = new Document();
			doc.putAll(clip);
			Object id = doc.remove(IATLookupMap.AT_ID);
			doc.put(IATMongo.MONGO_ID, id);
			toInsert.add(doc);
		}
		String table = ATMongoTableRegistry.getUserOpportunityClipTable();
		long count = _dao.fastBulkInsert(table, toInsert);
		return new ArrayList<>(clips_);
	}

	public List<ATOpportunityClip> loadOpportunityClips(String userId_, String symbol_) {
		if (StringUtils.isBlank(userId_) || StringUtils.isBlank(symbol_))
			return null;
		List<ATOpportunityClip> result = new ArrayList<>();
		String table = ATMongoTableRegistry.getUserOpportunityClipTable();
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_).and().eq(IATAssetConstants.SYMBOL, symbol_);
		List<ATPersistentLookupBase> tmpls = _dao.query(table, query, ATPersistentLookupBase.class);
		if (tmpls == null)
			return result;
		for (ATPersistentLookupBase tmpl : tmpls) {
			ATOpportunityClip clip = new ATOpportunityClip();
			clip.setAtId(IATMongo.getMongoId(tmpl));
			clip.setUserId(userId_);
			clip.setLabel(ATOpportunityClip.getLabel(tmpl));
			clip.setSymbol(symbol_);
			Document cmpDoc = (Document) tmpl.get(ATOpportunityClip.COMPONENTS);
			if (cmpDoc == null)
				continue;
			Map<String, Double> cmpMap = new TreeMap<>();
			for (Entry<String, Object> cmpEntry : cmpDoc.entrySet()) {
				String occId = cmpEntry.getKey();
				Double count = (Double) cmpEntry.getValue();
				cmpMap.put(occId, count);
			}
			clip.setComponents(cmpMap);
			result.add(clip);
		}
		return result;
	}

}
