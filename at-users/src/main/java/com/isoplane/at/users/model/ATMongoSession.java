package com.isoplane.at.users.model;

import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoSession extends Document implements IATMongo, IATLookupMap, IATSession {

	private static final long serialVersionUID = 1L;

	// private String name;
	// private String token;
	// private Long created;
	// private Long updated;
	// private transient String userId;

	private transient String email;
	// private transient Long expire;
	// private transient String clientIp;
	private transient String refreshToken;

	public ATMongoSession() {
	}

	public ATMongoSession(IATSession session_) {
		this.setAtId(session_.getToken());
		this.setClientIp(session_.getClientIp());
		this.setEmail(session_.getEmail());
		this.setUpdateTime(session_.getUpdateTime());
		// this.setInsertTime(session_.geti);
		this.setName(session_.getName());
		// this.setRefreshToken(session_.getr);
		// this.setToken(session_.getToken());
		// this.setUpdateTime(session_.getu);
		this.setUserId(session_.getUserId());
		this.setRoles(session_.getRoles());
	}

	@Override
	public void setAtId(String value_) {
		IATAsset.setMapValue(this, MONGO_ID, value_);
	}

	@Override
	public String getAtId() {
		return IATAsset.getString(this, MONGO_ID);
	}

	@Override
	public String getEmail() {
		return email;
	}

	public void setEmail(String value_) {
		email = value_;
	}

	@Override
	public String getToken() {
		return this.getAtId();
		// return IATAsset.getString(this, TOKEN);
	}

	public void setToken(String value_) {
		this.setAtId(value_);
		// IATAsset.setMapValue(this, MONGO_ID, value_);
	}

	@Override
	public String getClientIp() {
		return IATAsset.getString(this, CLIENT_IP);
	}

	public void setClientIp(String value_) {
		IATAsset.setMapValue(this, CLIENT_IP, value_);
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Override
	public String getUserId() {
		return IATAsset.getString(this, USER_ID);
	}

	public void setUserId(String value_) {
		IATAsset.setMapValue(this, USER_ID, value_);
		;
	}

	@Override
	public String getName() {
		return IATAsset.getString(this, NAME);
	}

	public void setName(String value_) {
		IATAsset.setMapValue(this, NAME, value_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		super.putAll(m);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public List<String> getRoles() {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) super.get(IATUser.ROLE, List.class);
		return list;
	}
	
	public void setRoles(List<String> value_) {
		if (value_ == null) {
			this.remove(IATUser.ROLE);
		} else {
			IATAsset.setMapValue(this, IATUser.ROLE, value_);
		}
	}

}
