package com.isoplane.at.users.model;

import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoUser extends Document implements IATMongo, IATLookupMap, IATUser {

	private static final long serialVersionUID = 1L;

	@Override
	public void putAll(Map<? extends String, ? extends Object> tmpl_) {
		super.putAll(tmpl_);
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	// @Override
	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getEmail() {
		return IATAsset.getString(this, EMAIL);
	}

	public void setEmail(String value_) {
		IATAsset.setMapValue(this, EMAIL, value_);
	}

	@Override
	public List<String> getMessageTokens() {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) super.get(MESSAGE_TOKENS, List.class);
		return list;
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public List<String> getRoles() {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) super.get(ROLE, List.class);
		return list;
	}

}
