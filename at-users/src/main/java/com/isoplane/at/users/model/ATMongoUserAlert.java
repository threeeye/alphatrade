package com.isoplane.at.users.model;

import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.ATAlertRule.ATAlertRuleComponent;
import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoUserAlert extends Document implements IATMongo, IATLookupMap, IATUserAlert {

	private static final long serialVersionUID = 1L;

	public ATMongoUserAlert() {
	}

	public ATMongoUserAlert(IATUserAlert tmpl_) {
		// this.setUserId(tmpl_.getUserId());
		this.setOccId(tmpl_.getOccId());
		this.setProperty(tmpl_.getProperty());
		this.setOperator(tmpl_.getOperator());
		this.setValue(tmpl_.getValue());
		this.setGroup(tmpl_.getGroup());
		this.setMessage(tmpl_.getMessage());
		this.setTitle(tmpl_.getTitle());
		this.setType(tmpl_.getType());
		this.setComponents(tmpl_.getComponents());
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> tmpl_) {
		super.putAll(tmpl_);
	}

	@Override
	public String getAtId() {
		return IATMongo.getMongoId(this);
	}

	@Override
	public void setAtId(String value_) {
		IATMongo.setMongoId(this, value_);
	}

	@Override
	public String getOccId() {
		return IATUserAlert.getOccId(this);
	}

	public void setOccId(String value_) {
		IATUserAlert.setOccId(this, value_);
	}

	// @Override
	// public String getUserId() {
	// return IATUserAlert.getUserId(this);
	// }

	// public void setUserId(String value_) {
	// IATUserAlert.setUserId(this, value_);
	// }

	@Override
	public String getProperty() {
		return IATUserAlert.getProperty(this);
	}

	public void setProperty(String value_) {
		IATUserAlert.setProperty(this, value_);
	}

	@Override
	public String getOperator() {
		return IATUserAlert.getOperator(this);
	}

	public void setOperator(String value_) {
		IATUserAlert.setOperator(this, value_);
	}

	@Override
	public String getValue() {
		return IATUserAlert.getValue(this);
	}

	public void setValue(String value_) {
		IATUserAlert.setValue(this, value_);
	}

	@Override
	public String getGroup() {
		return IATUserAlert.getGroup(this);
	}

	public void setGroup(String value_) {
		IATUserAlert.setGroup(this, value_);
	}

	@Override
	public String getTitle() {
		return IATUserAlert.getTitle(this);
	}

	public void setTitle(String value_) {
		IATUserAlert.setTitle(this, value_);
	}

	@Override
	public String getType() {
		return IATUserAlert.getType(this);
	}

	public void setType(String value_) {
		IATUserAlert.setType(this, value_);
	}

	@Override
	public String getMessage() {
		return IATUserAlert.getMessage(this);
	}

	public void setMessage(String value_) {
		IATUserAlert.setMessage(this, value_);
	}

	@Override
	public List<ATAlertRuleComponent> getComponents() {
		return IATUserAlert.getComponents(this);
	}

	public void setComponents(List<ATAlertRuleComponent> value_) {
		IATUserAlert.setComponents(this, value_);
	}

	@Override
	public Boolean isActive() {
		return IATUserAlert.isActive(this);
	}

	public void setActive(Boolean value_) {
		IATUserAlert.setActive(this, value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

}
