package com.isoplane.at.mongodb.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.util.EATChangeOperation;

public class ATMongoTools {

	static final Logger log = LoggerFactory.getLogger(ATMongoTools.class);

	static public EATChangeOperation toOperation(String value_) {
		switch (value_) {
		case "ADD":
		case "INSERT":
			return EATChangeOperation.ADD;
		case "DELETE":
			return EATChangeOperation.DELETE;
		case "UPDATE":
			return EATChangeOperation.UPDATE;
		default:
			log.error(String.format("Unsupported operation [%s]", value_));
			return null;
		}
	}
}
