package com.isoplane.at.mongodb.util;

import org.bson.Document;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.mongodb.IATMongoListener;
import com.mongodb.client.model.changestream.ChangeStreamDocument;

@Deprecated
// NOTE: Only worhs in replica set: https://docs.mongodb.com/manual/tutorial/convert-standalone-to-replica-set/
public class ATMongoReactiveSubscriber implements Subscriber<ChangeStreamDocument<Document>> {

	static final Logger log = LoggerFactory.getLogger(ATMongoReactiveSubscriber.class);
	static final Gson _gson = new Gson();

	private IATMongoListener _listener;

	private volatile long counter;
	private volatile Subscription subscription;

	public ATMongoReactiveSubscriber(IATMongoListener listener_) {
		_listener = listener_;
	}

	// abstract public void notify(Document data, String operation);

	// abstract public String getCollection();

	@Override
	public void onSubscribe(final Subscription s_) {
		subscription = s_;
		subscription.request(Integer.MAX_VALUE);
	}

	@Override
	public void onNext(final ChangeStreamDocument<Document> change_) {
		if (log.isDebugEnabled()) {
			log.debug(String.format("onNext: %s", change_));
		}
		String operation = change_.getOperationType().name();
		if ("DELETE".equals(operation)) {
			_listener.notify(change_.getDocumentKey(), operation, change_.getUpdateDescription());
		} else {
			_listener.notify(change_.getFullDocument(), operation, change_.getUpdateDescription());
		}
		counter++;
	}

	@Override
	public void onError(final Throwable t_) {
		log.error(String.format("onError"), t_);
		// onComplete();
	}

	@Override
	public void onComplete() {
		log.info(String.format("onComplete[%d]", counter));
	}

	public void shotdown() throws Throwable {
		subscription.cancel();
	}
}
