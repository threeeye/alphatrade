package com.isoplane.at.mongodb.util;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.util.ATConfigUtil;

public class ATMongoTableRegistry {

	static final Logger log = LoggerFactory.getLogger(ATMongoTableRegistry.class);

	static private boolean IS_TEST = false;

	static public void setTestMode(boolean isTest_) {
		IS_TEST = isTest_;
	}

	static public void init() {
	}

	static public String getAssetTable() {
		return getTable("mongo.t.assets");
	}

	static public String getUserBrokerTable() {
		return getTable("mongo.t.user.brokers");
	}

	static public String getDividendTable() {
		return getTable("mongo.t.dividend");
	}

	static public String getFoundationTable() {
		return getTable("mongo.t.foundation");
	}

	static public String getFundamentalAmeritradeTable() {
		return getTable("mongo.t.fundamentals.ameri");
	}

	static public String getHistoryTable() {
		return getTable("mongo.t.history");
	}

	static public String getMarketDataTable() {
		return getTable("mongo.t.market");
	}

	static public String getMarketDataTableAmeritrade() {
		return getTable("mongo.t.market.ameri");
	}

	static public String getOptionExpiryTable() {
		return getTable("mongo.t.option_exp");
	}

	static public String getPortfolioTable() {
		return getTable("mongo.t.portfolio");
	}

	static public String getPositionTable() {
		return getTable("mongo.t.position");
	}

	static public String getPositionHistoryTable() {
		return getTable("mongo.t.position_history");
	}

	static public String getStatusTable() {
		return getTable("mongo.t.status");
	}

	static public String getTaskTable() {
		return getTable("mongo.t.task");
	}

	static public String getTradeTable() {
		return getTable("mongo.t.trades");
	}

	static public void overrideTradeTable(String value_) {
		String tableId = "mongo.t.trades";
		log.info(String.format("Overriding [%s]: %s", tableId, value_));
		ATConfigUtil.overrideConfiguration(tableId, value_);
	}

	static public String getTradeCumulatorTable() {
		return getTable("mongo.t.trade.acu");
	}

	static public String getTradeGroupTable() {
		return getTable("mongo.t.trade.group");
	}

	static public String getTradeDigestTable() {
		return getTable("mongo.t.trade.digest");
	}

	static public String getTradeLedgerTable() {
		return getTable("mongo.t.trade.ledger");
	}

	static public void overrideTradeLedgerTable(String value_) {
		String tableId = "mongo.t.trade.ledger";
		log.info(String.format("Overriding [%s]: %s", tableId, value_));
		ATConfigUtil.overrideConfiguration(tableId, value_);
	}

	static public String getTradeTraceTable() {
		return getTable("mongo.t.trade.trace");
	}

	static public String getUserTable() {
		return getTable("mongo.t.users");
	}

	static public String getUserActivityTable() {
		return getTable("mongo.t.user.activity");
	}

	@Deprecated
	static public String getUserAlertsTable() {
		return getTable("mongo.t.users.alerts");
	}

	static public String getUserAlertTable() {
		return getTable("mongo.t.user.alert");
	}

	public static String getUserEquityModelTable() {
		return getTable("mongo.t.users.model_eqt");
	}

	static public String getUserNotesTable() {
		return getTable("mongo.t.users.notes");
	}

	public static String getUserOpportunityConfigTable() {
		return getTable("monto.t.user.conf.oppt");
	}

	public static String getUserOpportunityClipTable() {
		return getTable("mongo.t.user.oppt.clip");
	}

	static public String getUserResearchConfigTable() {
		return getTable("mongo.t.res_cfg.sec");
	}

	static public String getUserSessionTable() {
		return getTable("mongo.t.session");
	}

	static public String getUserSettingTable() {
		return getTable("mongo.t.user.setting");
	}

	static public String getDynamicSymbolTable() {
		return getTable("mongo.t.symbol.dyn");
	}

	static public String getSymbolTable() {
		return getTable("mongo.t.symbols");
	}

	static public String getSymbolConverterTable() {
		return getTable("mongo.t.symbol.conv");
	}

	public static String getUserWatchlistTable() {
		return getTable("mongo.t.user.watchlist");
	}

	static public String getTable(String id_) {
		Configuration config = ATConfigUtil.config();
		String table = config.getString(id_);
		if (IS_TEST && !StringUtils.isBlank(table)) {
			table = String.format("TEST_%s", table);
		}
		return table;
	}

}
