package com.isoplane.at.mongodb;

import java.util.List;
import java.util.Map;

import com.isoplane.at.commons.service.IATMarketStatusService;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATMongoStatusService implements IATMarketStatusService {

	static final Logger log = LoggerFactory.getLogger(ATMongoStatusService.class);

	//	private Configuration _config;
	//	private Set<IATMarketStatusListener> _listeners;
	private Map<String, Object> _status;
	private ATMongoDao _dao;
	private String _statusTable;

	public ATMongoStatusService() {
		//	_config = config_;
		//		_listeners = new HashSet<>();
		_statusTable = ATMongoTableRegistry.getStatusTable();
	}

	@Override
	public Map<String, Object> getStatus() {
		return _status;
	}

	// private void monitorStatus() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isTraceEnabled()) {
	// 				log.trace(String.format("Notify status [%s]: %s", operation_, data_));
	// 			}
	// 			switch (operation_) {
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 			case "REPLACE":
	// 				Document doc = (Document) data_;
	// 				_status = doc;
	// 				notifyListeners(doc);
	// 				break;
	// 			default:
	// 				// NOTE: Status only supports add, update, replace
	// 				return;
	// 			}
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _statusTable;
	// 		}
	// 	});

	// }

	// private void notifyListeners(Map<String, Object> data_) {
	// 	if (_listeners == null || _listeners.isEmpty())
	// 		return;
	// 	try {
	// 		for (IATMarketStatusListener listener : _listeners) {
	// 			listener.notify(data_);
	// 		}
	// 	} catch (Exception ex) {
	// 		log.error(String.format("Error notifying status: %s", data_), ex);
	// 	}
	// }

	@Override
	public void initService() {
		_dao = new ATMongoDao();

		String table = ATMongoTableRegistry.getStatusTable();
		List<ATPersistentLookupBase> status = _dao.query(table, new ATMongoWhereQuery().eq(IATMongo.MONGO_ID, "status"),
				ATPersistentLookupBase.class);
		if (status != null && !status.isEmpty()) {
			_status = status.get(0);
			log.info(String.format("Status initialized: %s", status));
		}

		boolean isStatusService = ATConfigUtil.config().getBoolean("mongo.isStatusListener", false);
		if (!isStatusService) {
			log.info(String.format("Status service [%b]", isStatusService));
			return;
		}
		//		monitorStatus();
	}

	// @Override
	// public void register(IATMarketStatusListener listener_) {
	// 	log.info(String.format("Registering status listener [%s]", listener_.getClass().getSimpleName()));
	// 	_listeners.add(listener_);
	// }

	@Override
	public boolean update(Map<String, Object> status_) {
		if (status_ == null || status_.isEmpty())
			return false;
		boolean result = _dao.updateValues(_statusTable, "status", status_, true);
		return result;
	}

}
