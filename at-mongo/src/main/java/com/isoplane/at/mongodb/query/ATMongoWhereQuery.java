package com.isoplane.at.mongodb.query;

import java.util.Collection;
import java.util.regex.Pattern;

import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.mongodb.client.model.Filters;

public class ATMongoWhereQuery implements IATWhereQuery {

	private boolean _orNextSegment;
	private Bson _query;

	public ATMongoWhereQuery() {
	}

	@Override
	public IATWhereQuery contains(String field_, String value_) {
		Pattern pattern = Pattern.compile(".*" + Pattern.quote(value_) + ".*", Pattern.CASE_INSENSITIVE);
		Bson query = Filters.regex(field_, pattern);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery startsWith(String field_, String value_) {
		Pattern pattern = Pattern.compile("^" + Pattern.quote(value_), Pattern.CASE_INSENSITIVE);
		Bson query = Filters.regex(field_, pattern);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery endsWith(String field_, String value_) {
		Pattern pattern = Pattern.compile(Pattern.quote(value_) + "$", Pattern.CASE_INSENSITIVE);
		Bson query = Filters.regex(field_, pattern);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery and() {
		_orNextSegment = false;
		return this;
	}

	@Override
	public IATWhereQuery eq(String field_, Object value_) {
		Bson query = Filters.eq(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery gt(String field_, Object value_) {
		Bson query = Filters.gt(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery gte(String field_, Object value_) {
		Bson query = Filters.gte(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery isNull(String field_) {
		Bson query = Filters.eq(field_, null);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery lt(String field_, Object value_) {
		Bson query = Filters.lt(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery lte(String field_, Object value_) {
		Bson query = Filters.lte(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery ne(String field_, Object value_) {
		Bson query = Filters.ne(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery notNull(String field_) {
		Bson query = Filters.ne(field_, null);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery in(String field_, Collection<? extends Object> value_) {
		Bson query = Filters.in(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery nin(String field_, Collection<? extends Object> value_) {
		Bson query = Filters.nin(field_, value_);
		constructQuery(query);
		return this;
	}

	@Override
	/** Warning! Very slow! */
	public IATWhereQuery where(String expr_) {
		Bson query = Filters.where(expr_);
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery expr(String expr_) {
		Bson query = Filters.expr(Document.parse(expr_));
		// Bson query = Document.parse("{$expr: {$eq: [\"_ul\",\"occId\"]}}");
		// Bson query = Filters.expr(Filters.eq(Document.parse("[\"$_ul\",\"$occId\"]")));
		constructQuery(query);
		return this;
	}

	@Override
	public IATWhereQuery or() {
		_orNextSegment = true;
		return this;
	}

	private void constructQuery(Bson newQuery_) {
		_query = _query == null ? newQuery_ : _orNextSegment ? Filters.or(_query, newQuery_) : Filters.and(_query, newQuery_);
	}

	public Bson getQuery() {
		return _query;
	}

	@Override
	public IATWhereQuery and(IATWhereQuery other_) {
		if (!(other_ instanceof ATMongoWhereQuery))
			throw new ATException("Incompatible query types");
		ATMongoWhereQuery other = (ATMongoWhereQuery) other_;
		if (other._query != null) {
			_query = _query == null ? other._query : Filters.and(_query, other._query);
		}
		return this;
	}

	@Override
	public IATWhereQuery or(IATWhereQuery other_) {
		if (!(other_ instanceof ATMongoWhereQuery))
			throw new ATException("Incompatible query types");
		ATMongoWhereQuery other = (ATMongoWhereQuery) other_;
		if (other._query != null) {
			_query = _query == null ? other._query : Filters.or(_query, other._query);
		}
		return this;
	}

	@Override
	public boolean isEmpty() {
		return _query == null || "{}".equals(_query.toString());
	}

	@Override
	public String toString() {
		// String result = _query != null ? _query.toString() : "{}";
		// result = getQuery().toString();
		String result = _query != null ? _query.toString() : "{}";
		return result;
	}

	@Override
	public IATWhereQuery empty() {
		Bson query = new BsonDocument();
		constructQuery(query);
		return this;
	}

}
