package com.isoplane.at.mongodb;

import java.util.Map;

public interface IATMongo {

	final String MONGO_ID = "_id";
	final String DISABLED = "_off";
	
	static public String getMongoId(Map<String, Object> map_) {
		return (String) map_.get(MONGO_ID);
	}

	static public void setMongoId(Map<String, Object> map_, String value_) {
		map_.put(MONGO_ID, value_);
	}

}
