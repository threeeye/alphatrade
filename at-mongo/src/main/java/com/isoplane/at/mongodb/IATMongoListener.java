package com.isoplane.at.mongodb;

import com.mongodb.client.model.changestream.UpdateDescription;

public interface IATMongoListener {

	/**
	 * Called on data change.
	 * @param data - Typically a bson Document
	 * @param operation - INSERT, UPDATE, DELETE (no data)
	 */
	void notify(Object data, String operation, UpdateDescription update_);
	
	String getCollection();
}
