package com.isoplane.at.mongodb;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.mongodb.util.ATMongoReactiveSubscriber;
import com.mongodb.ConnectionString;
import com.mongodb.client.model.changestream.FullDocument;
import com.mongodb.reactivestreams.client.ChangeStreamPublisher;
//import com.mongodb.client.MongoDatabase;
import com.mongodb.reactivestreams.client.MongoClient;
import com.mongodb.reactivestreams.client.MongoClients;
import com.mongodb.reactivestreams.client.MongoCollection;
import com.mongodb.reactivestreams.client.MongoDatabase;

import org.apache.commons.configuration2.Configuration;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class ATMongoReactiveDao {

	static final Logger log = LoggerFactory.getLogger(ATMongoReactiveDao.class);

	private MongoClient _mongoClient;
	private MongoDatabase _db;

	private String _dbName;
	private String _user;
	private String _pass;
	// private String _server;
	private String _connectionStr;

	public ATMongoReactiveDao() {
		try {
			Configuration config = ATConfigUtil.config();
			// _server = config_.getString("mongo.server");
			_dbName = config.getString("mongo.db");
			_user = config.getString("mongo.user");
			_pass = config.getString("mongo.pass");
			_connectionStr = config.getString("mongo.cstr");
			init();
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	private void init() throws UnsupportedEncodingException {
		String user = URLEncoder.encode(_user, "UTF-8");
		String pass = URLEncoder.encode(_pass, "UTF-8");
		String mongoStr = _connectionStr;
		if (_connectionStr.contains("%s")) {
			mongoStr = String.format(_connectionStr, user, pass);
		}
		// MongoClientURI uri = new MongoClientURI(mongoStr);
		// _mongoClient = MongoClients.create(uri);
		_mongoClient = MongoClients.create(new ConnectionString(mongoStr));
		_db = _mongoClient.getDatabase(_dbName);
	}

	public void subscribe(IATMongoListener listener_) {
		log.error(String.format("Deprecated usage of Reactive Mongo [%s]", listener_));
		MongoCollection<Document> collection = _db.getCollection(listener_.getCollection());
		ChangeStreamPublisher<Document> publisher = collection.watch().fullDocument(FullDocument.UPDATE_LOOKUP);
		ATMongoReactiveSubscriber subscriber = new ATMongoReactiveSubscriber(listener_);
		// ChangeStreamPublisher<ATTempTest> publisher = collection.watch(ATTempTest.class);
		// ATMongoReactiveSubscriber<ChangeStreamDocument<ATTempTest>> subscriber = new ATMongoReactiveSubscriber<ChangeStreamDocument<ATTempTest>>();
		publisher.subscribe(subscriber);
	}
}
