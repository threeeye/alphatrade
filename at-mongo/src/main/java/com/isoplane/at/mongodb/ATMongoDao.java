package com.isoplane.at.mongodb;

import static com.mongodb.client.model.Accumulators.first;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static com.mongodb.client.model.Updates.setOnInsert;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.bulk.BulkWriteUpsert;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.BsonField;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOneModel;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.UpdateOneModel;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.model.WriteModel;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

// https://docs.atlas.mongodb.com/reference/free-shared-limitations/
public class ATMongoDao implements IATMongo, IATPersistentLookupStore {

	static final Logger log = LoggerFactory.getLogger(ATMongoDao.class);

	private MongoClient _mongoClient;
	protected MongoDatabase _db;

	private String _dbName;
	private String _user;
	private String _pass;
	// private String _server;
	private String _connectionStr;
	private boolean _skipUpdateTS = false;

	public ATMongoDao() {
		this(ATConfigUtil.config());
	}

	@Deprecated
	public ATMongoDao(Configuration config_) {
		try {
			// _server = config_.getString("mongo.server");
			_dbName = config_.getString("mongo.db");
			_user = config_.getString("mongo.user");
			_pass = config_.getString("mongo.pass");
			_connectionStr = config_.getString("mongo.cstr");
			init();
		} catch (Exception ex) {
			throw new ATException(ex);
			// log.error("ATMongoDao", ex);
		}
	}

	private void init() throws UnsupportedEncodingException {
		// try {
		String user = URLEncoder.encode(_user, "UTF-8");
		String pass = URLEncoder.encode(_pass, "UTF-8");
		String mongoStr = _connectionStr;
		if (_connectionStr.contains("%s")) {
			mongoStr = String.format(_connectionStr, user, pass);
		}
		MongoClientURI uri = new MongoClientURI(mongoStr);
		// MongoClientURI uri = new MongoClientURI(_connectionStr);
		_mongoClient = new MongoClient(uri);
		_db = _mongoClient.getDatabase(_dbName);
		// } catch (Exception ex) {
		// log.error("Error", ex);
		// }
	}

	public void setSkipUpdateTS(boolean skipUpdateTS_) {
		_skipUpdateTS = skipUpdateTS_;
	}

	@Override
	public void close() {
		try {
			_mongoClient.close();
		} catch (Exception ex) {
			log.error(String.format("Error closing [%s]", _dbName), ex);
		}
	}

	@Override
	public String getDbId() {
		return _dbName;
	}

	@Override
	public boolean insert(String table_, IATLookupMap data_) {
		if (StringUtils.isBlank(table_)) {
			log.error(String.format("Missing table"));
			return false;
		}
		MongoCollection<Document> coll = _db.getCollection(table_);
		long time = System.currentTimeMillis();
		// data_.setInsertTime(time);
		// data_.setUpdateTime(time);
		Document doc = new Document(data_);
		doc.append(IATLookupMap.INSERT_TIME, time);
		doc.append(IATLookupMap.UPDATE_TIME, time);
		coll.insertOne(doc);
		return true;
	}

	/**
	 * Bulk insert may fail partially.
	 * 
	 * @param table_
	 * @param data_
	 */
	public long insert(String table_, IATLookupMap... data_) {
		if (StringUtils.isBlank(table_)) {
			log.error(String.format("Missing table"));
			return 0;
		}
		if (data_ == null || data_.length < 1) {
			log.debug(String.format("Nothing to insert [%s]", table_));
			return 0;
		}
		if (data_.length == 1) {
			int count = insert(table_, data_[0]) ? 1 : 0;
			log.debug(String.format("Inserted [%s/%d]", table_, count));
			return count;
		}
		long time = System.currentTimeMillis();
		MongoCollection<Document> coll = _db.getCollection(table_);
		List<Document> inserts = new ArrayList<>();
		for (IATLookupMap i : data_) {
			Document doc = new Document(i);
			doc.append(IATLookupMap.INSERT_TIME, time);
			if (!_skipUpdateTS) {
				doc.append(IATLookupMap.UPDATE_TIME, time);
			}
			inserts.add(doc);
		}
		coll.insertMany(inserts);
		return inserts.size();
	}

	/**
	 * Fast bulk insert overwrites existing data and inserts new
	 * 
	 * @param table_
	 * @param data_
	 * @return
	 */
	public long fastBulkInsert(String table_, Collection<Document> data_) {
		if (StringUtils.isBlank(table_)) {
			log.error(String.format("Missing table"));
			return 0;
		}
		if (data_ == null || data_.isEmpty()) {
			log.debug(String.format("Nothing to insert [%s]", table_));
			return 0;
		}
		List<WriteModel<Document>> bulks = new ArrayList<>();
		for (Document doc : data_) {
			bulks.add(new ReplaceOneModel<>(Filters.eq(IATMongo.MONGO_ID, doc.get(IATMongo.MONGO_ID)), doc,
					new ReplaceOptions().upsert(true)));
		}
		BulkWriteResult bulkResult = _db.getCollection(table_).bulkWrite(bulks, new BulkWriteOptions().ordered(false));
		List<BulkWriteUpsert> upserts = bulkResult.getUpserts();
		long count = bulkResult.getInsertedCount() + bulkResult.getModifiedCount()
				+ (upserts != null ? upserts.size() : 0);
		if (log.isDebugEnabled())
			log.debug(String.format("Bulk [%d]: %s", data_.size(), bulkResult));
		// log.info(String.format("Bulk [%d] {deleted: %d, inserted: %d, matched: %d, modified: %d}", data_.size(), bulkResult.getDeletedCount(),
		// bulkResult.getInsertedCount(), bulkResult.getMatchedCount(), bulkResult.getModifiedCount()));

		// MongoCollection<Document> coll = _db.getCollection(table_);
		// InsertManyOptions options = new InsertManyOptions().ordered(false);
		// coll.insertMany(data_, options);
		return count;
	}

	/**
	 * Fast bulk update overwrites existing data and inserts new
	 * 
	 * @param table_
	 * @param data_
	 * @return
	 */
	public long fastBulkUpdate(String table_, Collection<Document> data_) {
		if (StringUtils.isBlank(table_)) {
			log.error(String.format("Missing table"));
			return 0;
		}
		if (data_ == null || data_.isEmpty()) {
			log.debug(String.format("Nothing to insert [%s]", table_));
			return 0;
		}
		List<WriteModel<Document>> bulks = new ArrayList<>();
		for (Document toUpdate : data_) {
			String id = toUpdate.getString(IATLookupMap.AT_ID);
			Document toInsert = new Document().append(IATLookupMap.AT_ID, id);
			toUpdate.remove(IATMongo.MONGO_ID);
			toUpdate.remove(IATLookupMap.AT_ID);
			Document doc = new Document().append("$setOnInsert", toInsert).append("$set", toUpdate);
			bulks.add(new UpdateOneModel<>(Filters.eq(IATMongo.MONGO_ID, id), doc, new UpdateOptions().upsert(true)));
		}
		BulkWriteResult bulkResult = _db.getCollection(table_).bulkWrite(bulks, new BulkWriteOptions().ordered(false));
		List<BulkWriteUpsert> upserts = bulkResult.getUpserts();
		long count = bulkResult.getInsertedCount() + bulkResult.getModifiedCount()
				+ (upserts != null ? upserts.size() : 0);
		if (log.isDebugEnabled())
			log.debug(String.format("Bulk [%d]: %s", data_.size(), bulkResult));
		return count;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public IATLookupMap get(String table_, String id_) {
		Bson query = eq(IATLookupMap.AT_ID, id_);
		MongoCollection<Document> coll = _db.getCollection(table_);
		FindIterable<HashMap> results = coll.find(query, HashMap.class);
		HashMap item = results.first();
		ATPersistentLookupBase result = item != null ? new ATPersistentLookupBase(item) : null;
		return result;
	}

	// public IATLookupMap get(String table_, IATWhereQuery query_) {
	// 	Bson query = ((ATMongoWhereQuery) query_).getQuery();
	// 	MongoCollection<Document> coll = _db.getCollection(table_);
	// 	var results = coll.find(query, HashMap.class);
	// 	var item = results.first();
	// 	@SuppressWarnings("unchecked")
	// 	ATPersistentLookupBase result = item != null ? new ATPersistentLookupBase(item) : null;
	// 	return result;
	// }

	public Document min(String table_, String field_) {
		MongoCollection<Document> coll = _db.getCollection(table_);
		Document item = coll.find().sort(new Document(field_, 1)).first();
		return item;
	}

	// private void changes() {
	//// _db.getCollection("test").watch(asList(Aggregates.match(and(asList(
	//// in("operationType", asList("insert")),
	//// eq("fullDocument.even", 1L)))))).for;
	// ChangeStreamIterable<Document> publisher = _db.getCollection("test").watch();
	// // publisher.su
	//
	// }

	public boolean update0(String table_, IATLookupMap data_) {
		try {
			Bson query = eq(IATLookupMap.AT_ID, data_.getAtId());
			data_.setUpdateTime(System.currentTimeMillis());
			List<Bson> updates = new ArrayList<>();
			for (String key : data_.keySet()) {
				Object value = data_.get(key);
				Bson upd = IATLookupMap.INSERT_TIME.equals(key) ? setOnInsert(key, value) : set(key, value);
				updates.add(upd);
			}
			Bson update = combine(updates);
			MongoCollection<Document> coll = _db.getCollection(table_);
			UpdateResult result = coll.updateOne(query, update, new UpdateOptions().upsert(true));
			return result.wasAcknowledged();
		} catch (Exception ex) {
			throw new ATException("Persistence Error", ex);
		}
	}

	@Override
	public boolean update(String table_, IATLookupMap... data_) {
		if (data_ == null || data_.length == 0)
			return false;
		try {
			UpdateOptions options = new UpdateOptions().upsert(true);
			List<WriteModel<Document>> updates = new ArrayList<>();
			long currentTime = System.currentTimeMillis();
			for (IATLookupMap d : data_) {
				Object mongoId = d.get(IATMongo.MONGO_ID);
				Bson filter = eq(IATLookupMap.AT_ID, d.getAtId());
				List<Bson> updateList = new ArrayList<>();
				for (String key : d.keySet()) {
					if (IATLookupMap.INSERT_TIME.equals(key) || IATMongo.MONGO_ID.equals(key))
						continue;
					// if ("hv_map".equals(key)) {
					// log.debug(key);
					// }
					Object value = d.get(key);
					Bson upd = set(key, value);
					updateList.add(upd);
				}
				if (mongoId != null) {
					updateList.add(setOnInsert(IATMongo.MONGO_ID, mongoId));
				}
				updateList.add(setOnInsert(IATLookupMap.INSERT_TIME, currentTime));
				updateList.add(set(IATLookupMap.UPDATE_TIME, currentTime));
				Bson update = combine(updateList);
				UpdateOneModel<Document> model = new UpdateOneModel<>(filter, update, options);
				updates.add(model);
			}
			MongoCollection<Document> coll = _db.getCollection(table_);
			if (updates.size() > 1) {
				log.debug("Upsert incompatible with bulkWrite");
			}
			BulkWriteResult result = coll.bulkWrite(updates);
			return result.wasAcknowledged();
		} catch (Exception ex) {
			throw new ATException("Persistence Error", ex);
		}
	}

	@Override
	public boolean update(String table_, boolean isUpsert_, IATLookupMap... data_) {
		if (data_ == null || data_.length == 0)
			return false;
		if (!isUpsert_)
			return update(table_, data_);
		try {
			MongoCollection<Document> coll = _db.getCollection(table_);
			UpdateOptions options = new UpdateOptions().upsert(true);
			boolean result = true;
			long time = System.currentTimeMillis();
			for (IATLookupMap d : data_) {
				Bson filter = eq(IATLookupMap.AT_ID, d.getAtId());
				List<Bson> updateList = new ArrayList<>();
				for (String key : d.keySet()) {
					if (IATLookupMap.INSERT_TIME.equals(key))
						continue;
					Object value = d.get(key);
					Bson upd = set(key, value);
					updateList.add(upd);
				}
				updateList.add(set(IATLookupMap.UPDATE_TIME, time));
				updateList.add(setOnInsert(IATLookupMap.INSERT_TIME, time));
				Bson update = combine(updateList);
				UpdateResult rez = coll.updateOne(filter, update, options);
				result = result && rez.wasAcknowledged();
			}
			return result;
		} catch (Exception ex) {
			throw new ATException("Persistence Error", ex);
		}
	}

	public boolean update(String table_, String key_, @SuppressWarnings("unchecked") Map<String, Object>... data_) {
		if (data_ == null || data_.length == 0)
			return false;
		try {
			MongoCollection<Document> coll = _db.getCollection(table_);
			UpdateOptions options = new UpdateOptions().upsert(true);
			boolean result = true;
			long time = System.currentTimeMillis();
			for (Map<String, Object> d : data_) {
				String id = (String) d.get(key_);
				if (id == null) {
					d.put(IATLookupMap.UPDATE_TIME, time);
					d.put(IATLookupMap.INSERT_TIME, time);
					Document doc = new Document(d);
					coll.insertOne(doc);
					// ObjectId oid = doc.getObjectId(IATMongo.MONGO_ID);
					// d.put(key_, oid.toString());
				} else {
					Bson filter = eq(key_, id);
					List<Bson> updateList = new ArrayList<>();
					for (String key : d.keySet()) {
						if (IATLookupMap.INSERT_TIME.equals(key) || IATLookupMap.UPDATE_TIME.equals(key))
							continue;
						Object value = d.get(key);
						Bson upd = set(key, value);
						updateList.add(upd);
					}
					updateList.add(set(IATLookupMap.UPDATE_TIME, time));
					updateList.add(setOnInsert(IATLookupMap.INSERT_TIME, time));
					Bson update = combine(updateList);
					UpdateResult rez = coll.updateOne(filter, update, options);
					result = result && rez.wasAcknowledged();
				}
			}
			return result;
		} catch (Exception ex) {
			throw new ATException("Persistence Error", ex);
		}
	}

	/** Updates timestamp */
	public boolean touch(String table_, String id_) {
		MongoCollection<Document> coll = _db.getCollection(table_);
		// UpdateOptions options = new UpdateOptions().upsert(false);
		Bson filter = eq(IATMongo.MONGO_ID, id_);
		Bson update = set(IATLookupMap.UPDATE_TIME, System.currentTimeMillis());
		UpdateResult result = coll.updateOne(filter, update);
		return result.wasAcknowledged();
	}

	@Override
	public boolean updateValues(String table_, String id_, Map<String, Object> data_, boolean isUpsert_) {
		MongoCollection<Document> coll = _db.getCollection(table_);
		UpdateOptions options = new UpdateOptions().upsert(isUpsert_);
		Bson filter = eq(IATMongo.MONGO_ID, id_);
		List<Bson> updates = new ArrayList<>();
		for (Entry<String, Object> entry : data_.entrySet()) {
			if (IATLookupMap.INSERT_TIME.equals(entry.getKey()))
				continue;
			updates.add(set(entry.getKey(), entry.getValue()));
		}
		long time = System.currentTimeMillis();
		updates.add(set(IATLookupMap.UPDATE_TIME, time));
		if (isUpsert_) {
			updates.add(setOnInsert(IATLookupMap.INSERT_TIME, time));
		}
		Bson update = combine(updates);
		UpdateResult result = coll.updateOne(filter, update, options);
		return result.wasAcknowledged();
	}

	@Override
	public boolean updateValues(String table_, IATWhereQuery query_, Map<String, Object> data_) {
		MongoCollection<Document> coll = _db.getCollection(table_);
		UpdateOptions options = new UpdateOptions().upsert(false);
		Bson filter = ((ATMongoWhereQuery) query_).getQuery();
		List<Bson> updates = new ArrayList<>();
		for (Entry<String, Object> entry : data_.entrySet()) {
			updates.add(set(entry.getKey(), entry.getValue()));
		}
		updates.add(setOnInsert(IATLookupMap.INSERT_TIME, System.currentTimeMillis()));
		Bson update = combine(updates);
		UpdateResult result = coll.updateOne(filter, update, options);
		return result.wasAcknowledged();
	}

	@Override
	public <T extends IATLookupMap> List<T> query(String table_, IATWhereQuery query_, Class<T> type_) {
		ATMongoWhereQuery query = (ATMongoWhereQuery) query_;
		MongoCollection<Document> coll = _db.getCollection(table_);
		List<T> result = new ArrayList<>();
		Bson filter = query != null ? query.getQuery() : null;
		try (MongoCursor<Document> cursor = (filter == null ? coll.find() : coll.find(filter)).iterator()) {
			while (cursor.hasNext()) {
				// T next = cursor.next();
				// result.add(next);
				Document map = cursor.next();
				if (map.containsKey("mcap")) {
					log.debug("mcap");
				}
				T x = type_.newInstance();
				x.putAll(map);
				result.add(x);
			}
		} catch (Exception ex) {
			log.error(String.format("Error on query [%s]", query_), ex);
		}
		return result;
	}

	public ArrayList<Document> queryProjection(String table_, IATWhereQuery query_, String... fields) {
		try {
			ATMongoWhereQuery query = (ATMongoWhereQuery) query_;
			MongoCollection<Document> coll = _db.getCollection(table_);
			Bson filter = query != null ? query.getQuery() : null;
			Document projection = new Document();
			if (fields != null) {
				for (String field : fields) {
					projection.put(field, 1);
				}
			}
			ArrayList<Document> result = (filter == null ? coll.find() : coll.find(filter)).projection(projection)
					.into(new ArrayList<>());
			return result;
		} catch (Exception ex) {
			log.error(String.format("queryProjection Error [%s]", query_), ex);
			return null;
		}
	}

	public <T> List<T> query(String table_, IATWhereQuery query_, Function<Document, T> call_) {
		ATMongoWhereQuery query = (ATMongoWhereQuery) query_;
		MongoCollection<Document> coll = _db.getCollection(table_);
		List<T> result = new ArrayList<>();
		Bson filter = query != null ? query.getQuery() : null;
		try (MongoCursor<Document> cursor = (filter == null ? coll.find() : coll.find(filter)).iterator()) {
			while (cursor.hasNext()) {
				T bla = call_.apply(cursor.next());
				result.add(bla);
			}
		} catch (Exception ex) {
			log.error(String.format("Error on query [%s]", query_), ex);
		}
		return result;
	}

	// dir: max: -1
	public <T extends IATLookupMap> TreeMap<String, T> queryEdge(String table_, IATWhereQuery query_, String groupId_,
			String sortField_,
			int sortDir_,
			Collection<String> projection_, Class<T> type_) {
		TreeMap<String, T> resultMap = new TreeMap<>();
		try {
			Bson query = query_ != null ? ((ATMongoWhereQuery) query_).getQuery() : null;
			List<BsonField> fields = projection_.stream().map(s -> first(s, "$" + s)).collect(Collectors.toList());
			Bson group = group("$" + groupId_, fields);

			List<Bson> pipeline = new ArrayList<>();
			if (query != null)
				pipeline.add(match(query));
			pipeline.add(sort(new Document(sortField_, sortDir_)));
			pipeline.add(group);

			AggregateIterable<Document> aggregate = _db.getCollection(table_).aggregate(pipeline);
			for (Document document : aggregate) {
				String id = document.getString("_id");
				T x;
				x = type_.newInstance();
				x.putAll(document);
				resultMap.put(id, x);
			}
		} catch (Exception ex) {
			log.error(String.format("queryEdge Error"), ex);
		}
		return resultMap;
	}

	public Map<String, Double[]> aggregateSum(String table_, IATWhereQuery query_, String groupId_, String... fields_) {
		Bson filter = ((ATMongoWhereQuery) query_).getQuery();
		ArrayList<BsonField> accumulators = new ArrayList<>();
		for (String field : fields_) {
			accumulators.add(Accumulators.sum(field, "$" + field));
		}
		List<Bson> pipeline = Arrays.asList(
				Aggregates.match(filter),
				Aggregates.group("$" + groupId_, accumulators));
		AggregateIterable<Document> aggregate = _db.getCollection(table_).aggregate(pipeline);
		Map<String, Double[]> resultMap = new TreeMap<>();
		for (Document document : aggregate) {
			String id = document.getString("_id");
			Double[] data = new Double[fields_.length];
			for (int i = 0; i < fields_.length; i++) {
				String field = fields_[i];
				Double count = document.getDouble(field);
				data[i] = count;
			}
			resultMap.put(id, data);
		}
		return resultMap;

	}

	@Override
	public long delete(String table_, IATWhereQuery query_) {
		try {
			ATMongoWhereQuery query = (ATMongoWhereQuery) query_;
			MongoCollection<Document> coll = _db.getCollection(table_);
			Bson mq = query.getQuery();
			DeleteResult result = coll.deleteMany(mq);
			return result.getDeletedCount();
		} catch (Exception ex) {
			log.error(String.format("Failed to delete [%s, %s]", table_, query_), ex);
			return -1;
		}
	}

	@Override
	public <T> Set<T> distinct(String table_, IATWhereQuery query_, String field_, Class<T> class_) {
		try {
			ATMongoWhereQuery query = (ATMongoWhereQuery) query_;
			MongoCollection<Document> coll = _db.getCollection(table_);
			MongoCursor<T> c = coll.distinct(field_, query.getQuery(), class_).iterator();
			Set<T> result = new HashSet<>();
			while (c.hasNext()) {
				result.add(c.next());
			}
			return result;
		} catch (Exception ex) {
			throw new ATException(String.format("%s [%s, %s]", "distinct", table_, field_), ex);
		}
	}

	/**
	 * Expects table sorted by AT_ID and T with explicit constructor(Map<String,Object>)
	 * 
	 * @param table_
	 * @param groupField_
	 * @param maxField_
	 * @param class_
	 * @return
	 */
	@Override
	public <T extends IATLookupMap> TreeMap<String, T> groupMax(String table_, String groupField_, String maxField_,
			Class<T> class_) {
		Map<String, Object> e = null;
		Map<String, Object> map = null;
		String groupKey = null;
		try {
			Constructor<T> ctr = class_.getConstructor(Map.class);
			MongoCollection<T> coll = _db.getCollection(table_, class_);
			Bson s = sort(Sorts.descending(IATLookupMap.AT_ID));
			Bson b = group("$" + groupField_, Accumulators.first("items", "$$ROOT"));
			MongoCursor<T> find = coll.aggregate(Arrays.asList(s, b)).allowDiskUse(true).maxTime(1, TimeUnit.MINUTES)
					.iterator();
			TreeMap<String, T> result = new TreeMap<>();
			while (find.hasNext()) {
				e = (Map<String, Object>) find.next();
				@SuppressWarnings("unchecked")
				Map<String, Object> tmpMap = (Map<String, Object>) e.get("items");
				map = tmpMap;
				T t = ctr.newInstance(map);
				groupKey = (String) t.get(groupField_);
				if (!StringUtils.isBlank(groupKey)) {
					result.put(groupKey, t);
				} else {
					log.error(String.format("Group key is blank [%s]: %s", groupField_, e));
				}
			}
			return result;
		} catch (Exception ex) {
			throw new ATException(
					String.format("%s [%s, {%s: %s}, %s]: %s | %s", "distinctMax", table_, groupField_, groupKey,
							maxField_, e, map),
					ex);
		}
	}

	public Map<String, Integer> count(String table_, String field_) {
		MongoCollection<Document> coll = _db.getCollection(table_);

		String field = String.format("$%s", field_);
		AggregateIterable<Document> aggregate = coll
				.aggregate(Arrays.asList(Aggregates.group(field, Accumulators.sum("count", 1))));
		Map<String, Integer> resultMap = new TreeMap<>();
		for (Document document : aggregate) {
			String id = document.getString(IATMongo.MONGO_ID);
			Integer count = document.getInteger("count");
			resultMap.put(id, count);
		}
		return resultMap;
	}

}
