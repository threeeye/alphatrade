package com.isoplane.at.mongodb;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATLookupStore;
import com.isoplane.at.commons.store.IATWhereQuery;

public interface IATPersistentLookupStore extends IATLookupStore {

	/**
	 * Fast bulk update. Upsert may not work.
	 * 
	 * @param table
	 * @param data
	 * @return
	 */
	boolean update(String table, IATLookupMap... data);

	/**
	 * Slow bulk update. Upsert should work
	 * 
	 * @param table
	 * @param isUpsert
	 * @param data
	 * @return
	 */
	boolean update(String table, boolean isUpsert, IATLookupMap... data);

	boolean insert(String table, IATLookupMap data);

	<T extends IATLookupMap> List<T> query(String table, IATWhereQuery query, Class<T> type);

	/**
	 * @return Deleted record count
	 */
	long delete(String table, IATWhereQuery query);

	<T> Set<T> distinct(String table, IATWhereQuery query, String field, Class<T> clazz);

	void close();

	/** Update values in single record */
	boolean updateValues(String table, String id, Map<String, Object> data, boolean isUpsert);

	boolean updateValues(String table, IATWhereQuery query, Map<String, Object> data);

}
