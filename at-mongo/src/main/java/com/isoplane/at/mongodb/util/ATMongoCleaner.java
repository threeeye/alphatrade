package com.isoplane.at.mongodb.util;

import static com.mongodb.client.model.Filters.lt;

import java.text.Format;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;

public class ATMongoCleaner {

	static final Logger log = LoggerFactory.getLogger(ATMongoCleaner.class);

	private Timer _cleaner;
	private TimerTask _cleanerTask;
	private MongoDatabase _db;
	private String _table;

	public ATMongoCleaner getDailyCleaner(String table_, String column_, Format format_) {

		_cleanerTask = new TimerTask() {

			@Override
			public void run() {
				try {
					String dateStr = format_.format(new Date());
					Bson query = lt(column_, dateStr);
					MongoCollection<Document> coll = _db.getCollection(table_);
					DeleteResult result = coll.deleteMany(query);
					log.info(String.format("Cleaned [%b, %d]", result.wasAcknowledged(), result.getDeletedCount()));
				} catch (Exception ex) {
					log.error("Error", ex);
				}
			}
		};
		
		return this;
	}

	public void run(MongoDatabase db_) {
		_db = db_;
		_cleaner = new Timer(String.format("MongoDailyCleaner[%s]", _table));
		_cleaner.scheduleAtFixedRate(_cleanerTask, 0, 24 * 60 * 60 * 1000);
	}

	public void stop() {
		_cleaner.cancel();
	}
}
