package com.isoplane.at.mongo;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.ATMongoDao;

//@RunWith(SpringJUnit4ClassRunner.class)
public class ATMongoDbTest {

	static final Logger log = LoggerFactory.getLogger(ATMongoDbTest.class);

	static ATMongoDao _dao;
	static int counter;
	static Configuration _config;

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		_config = new CompositeConfiguration();
		_config.addProperty("mongo.server", ATMongoNoCheckin.MONGO_SERVER);
		_config.addProperty("mongo.db", ATMongoNoCheckin.MONGO_DATABASE);
		_config.addProperty("mongo.user", ATMongoNoCheckin.MONGO_USER);
		_config.addProperty("mongo.pass", ATMongoNoCheckin.MONGO_PW);

		_dao = new ATMongoDao();
		// _dao.init();

		Thread.sleep(1000);
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {

		Scanner scanner = new Scanner(System.in);
		log.info("in: " + scanner.nextLine());
		scanner.close();

		_dao.close();

		log.info("End");
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Disabled
	@Test
	public void testMongo() throws Exception {
		String table = "ib";
		String id = "id_" + System.currentTimeMillis();
		// final int counter = 0;
		IATLookupMap data = new ATPersistentLookupBase();
		data.setAtId(id);
		data.put("testkey", "testvalue_" + counter++);
		_dao.insert(table, data);

		(new Timer("ChainReader")).scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					IATLookupMap data = new ATPersistentLookupBase();
					data.setAtId(id);
					data.put("testkey", "testvalue_" + counter++);
					boolean result = _dao.update(table, data);

					IATLookupMap update = _dao.get("ib", id);
					log.info(String.format("TEST: %b - %s", result, update));
				} catch (Exception ex) {
					log.error("Error", ex);
				}
			}
		}, 5000, 5000);

	}

}
