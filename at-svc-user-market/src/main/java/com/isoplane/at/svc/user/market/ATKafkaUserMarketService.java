package com.isoplane.at.svc.user.market;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.Any;
import com.google.protobuf.GeneratedMessageV3;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataStatus;
import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage.Builder;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSessionDelayQueue;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer.DefaultStatusRunner;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;

public class ATKafkaUserMarketService implements IATProtoUserSubscriber, IATProtoMarketSubscriber, IATSystemStatusProvider {
	static final Logger log = LoggerFactory.getLogger(ATKafkaUserMarketService.class);

	private static ATKafkaUserMarketService _instance;
	static private String ID;

	private boolean _isRunning;
	private final CountDownLatch _serviceLatch;

	private DelayQueue<DelayedMarketData> _delayedMarketQueue;
	private ConcurrentHashMap<String, MarketData.Builder> _marketMap;

	private Set<String> _userIds;
	private Map<String, Map<String, Set<String>>> _symbolOptionUserMap;
	private Map<String, Set<String>> _portOccId2UserMap;
	private Map<String, Set<String>> _tempOccId2UserMap;

	private ATKafkaSseMessageProducer _sseProducer;
	private long _msgCount = 0;
	private long _nextTimeStamp = 0;
	private long _mktDelay;

	private ATKafkaSystemStatusProducer _statusProducer;

	private ATSessionDelayQueue _sessionQueue;

	public static void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					_instance.shutdown();
					if (_instance != null) {
						ATExecutors.shutdown();
					}
				}
			}));

			ATExecutors.init();
			ATKafkaRegistry.register("user_mkt_svc",
					ATKafkaUserSessionConsumer.class,
					ATKafkaMarketDataConsumer.class,
					ATKafkaMarketDataPackConsumer.class,
					ATKafkaSseMessageProducer.class,
					ATKafkaSystemStatusProducer.class);

			// IATPortfolioService portfolio = new ATPortfolioManager(true);
			// ATServiceRegistry.setPortfolioService(portfolio, false);

			String ipStr = ATSysUtils.getIpAddress("n/a");
			String portStr = config.getString("web.port");
			ID = StringUtils.isBlank(portStr) ? String.format("%s:%s", ATKafkaUserMarketService.class.getSimpleName(), ipStr)
					: String.format("%s:%s:%s", ATKafkaUserMarketService.class.getSimpleName(), ipStr, portStr);

			_instance = new ATKafkaUserMarketService();
			_instance.init();
			_instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

			_instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaUserMarketService.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaUserMarketService() {
		_serviceLatch = new CountDownLatch(1);
		_userIds = new TreeSet<>();
		_portOccId2UserMap = new TreeMap<>();
		_tempOccId2UserMap = new TreeMap<>();
		_symbolOptionUserMap = new TreeMap<>();
		// _delayedSessionQueue = new DelayQueue<>();
		_delayedMarketQueue = new DelayQueue<>();
		this._sessionQueue = new ATSessionDelayQueue();
		_marketMap = new ConcurrentHashMap<>();
	}

	public void init() {
		Configuration config = ATConfigUtil.config();
		_mktDelay = config.getLong("usr_mkt.delay", 1000);

		// _pfSvc = ATServiceRegistry.getPortfolioService();

		ATKafkaUserSessionConsumer userSessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		userSessionConsumer.subscribe(this._sessionQueue);
		this._sessionQueue.subscribe(this);
		ATKafkaMarketDataConsumer marketDataConsumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
		marketDataConsumer.subscribe(this);
		ATKafkaMarketDataPackConsumer marketDataPackConsumer = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
		marketDataPackConsumer.subscribe(this);
		_sseProducer = ATKafkaRegistry.get(ATKafkaSseMessageProducer.class);
		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

		ATSystemStatusManager.register(this);
	}

	public void start() {
		this._isRunning = true;
		this._sessionQueue.start();
		ATKafkaRegistry.start();
		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, DefaultStatusRunner.class.getSimpleName()),
				new DefaultStatusRunner(ATKafkaUserMarketService.ID, this, this._statusProducer));
		ATExecutors.submit(String.format("%s.%s", pre, DelayedMarketRunner.class.getSimpleName()), new DelayedMarketRunner());
		// ATExecutors.submit(String.format("%s.%s", pre, SessionRunner.class.getSimpleName()), new SessionRunner());
	}

	public void shutdown() {
		this._isRunning = false;
		ATKafkaRegistry.stop();
	}

	@Override
	public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
		switch (action_) {
		case ADD:
		case CREATE:
		case UPDATE:
			updateSession(session_);
			break;
		case DELETE:
			removeUser(session_.getUserId(), true, true);
			break;
		default:
			log.warn(String.format("%s: Unsupported action [%s]", this.getClass().getSimpleName(), action_));
			break;
		}

		// // log.debug(String.format("notifyUserSession[%s] %s", action_, session_));
		// String userId = session_.getUserId();
		// if (StringUtils.isBlank(userId) || IATUser.STATIC_USERS.contains(userId))
		// return;
		// Iterator<DelayedSessionNotification> dsi = _delayedSessionQueue.iterator();
		//
		// // Update action on unprocessed sessions
		// while (dsi.hasNext()) {
		// DelayedSessionNotification ds = dsi.next();
		// if (ds.userId.equals(userId)) {
		// ds.action = action_;
		// ds.session = session_;
		// return;
		// }
		// }
		// this._delayedSessionQueue.offer(new DelayedSessionNotification(action_, session_, 1000));
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> data_) {
		if (data_ != null && !data_.isEmpty()) {
			Configuration config = ATConfigUtil.config();
			_mktDelay = config.getLong("usr_mkt.delay", 1000);
			for (ATProtoMarketDataWrapper wrapper : data_) {
				notifyMarketData(wrapper.proto());
			}
		}
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper data_) {
		if (data_ == null || data_.proto().getSymbolsCount() < 1)
			return;
		MarketDataPack pack = data_.proto();
		String symbol = pack.getSymbols(0);
//		if ("SPY".equals(symbol)) {
//			log.debug(String.format("debug [%s]", symbol));
//		}
		String subtype = pack.hasSubType() ? pack.getSubType() : null;
		if (ATProtoMarketDataWrapper.TYPE_MARKET_DATA.equals(subtype)) {
			Set<String> userIds = this._portOccId2UserMap.get(symbol);
			if (userIds == null) {
				userIds = new HashSet<>();
			}
			Set<String> tempUserIds = this._tempOccId2UserMap.get(symbol);
			if (tempUserIds != null) {
				userIds.addAll(tempUserIds);
			}
			if (userIds == null || userIds.isEmpty())
				return;
			QueuedMarketDataPack queuedPack = new QueuedMarketDataPack(pack, userIds);
			this.sendMarketDataPack(queuedPack);
			this.checkPackOptions(symbol, pack);
		} else if (ATProtoMarketDataWrapper.SUB_TYPE_OPTION_CHAIN.equals(subtype)) {
			Set<String> tempUserIds = this._tempOccId2UserMap.get(symbol);
			if (tempUserIds != null && !tempUserIds.isEmpty()) {
				this.sendMarketDataAvailable(symbol, tempUserIds);
			}
			this.checkPackOptions(symbol, pack);
		} else {
			for (MarketData proto : pack.getMarketDataList()) {
				notifyMarketData(proto);
			}
		}
	}

	private void checkPackOptions(String symbol_, MarketDataPack pack_) {
		Map<String, Set<String>> optionUserMap = this._symbolOptionUserMap.get(symbol_);
		if (optionUserMap != null && !optionUserMap.isEmpty()) {
			for (MarketData mkt : pack_.getMarketDataList()) {
				String occId = mkt.getId();
				Set<String> userIds = optionUserMap.get(occId);
				if (userIds != null && !userIds.isEmpty()) {
					updateMarketData(mkt);
					DelayedMarketData dm = new DelayedMarketData(occId, userIds, _mktDelay);
					if (!_delayedMarketQueue.contains(dm)) {
						_delayedMarketQueue.offer(dm);
					}
				}
			}
		}
	}

	private void notifyMarketData(MarketData proto_) {
		String occId = proto_.getId();
		Set<String> userIds = this._portOccId2UserMap.get(occId);
		if (userIds == null) {
			userIds = new HashSet<>();
		}
		Set<String> tempUserIds = this._tempOccId2UserMap.get(occId);
		if (tempUserIds != null) {
			userIds.addAll(tempUserIds);
		}
		if (userIds == null || userIds.isEmpty())
			return;
		updateMarketData(proto_);
		DelayedMarketData dm = new DelayedMarketData(proto_.getId(), userIds, _mktDelay);
		if (!_delayedMarketQueue.contains(dm)) {
			_delayedMarketQueue.offer(dm);
		}
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper wrapper_) {
		String status = wrapper_ != null && wrapper_.proto() != null ? wrapper_.proto().getStatus() : null;
		log.info(String.format("notifyMarketStatus[%s]", status));
	}

	private void updateMarketData(MarketData mkt_) {
		String occId = mkt_.getId();
		MarketData.Builder builder = _marketMap.get(occId);
		if (builder == null) {
			builder = mkt_.toBuilder();
			_marketMap.put(occId, builder);
		} else {
			builder.mergeFrom(mkt_);
		}
	}

	private boolean updateSession(ATSession session_) {
		String userId = session_.getUserId();
		Map<String, String> sessionData = session_ != null ? session_.getData() : null;
		if (StringUtils.isBlank(userId) || sessionData == null || sessionData.isEmpty()) {
			log.info(String.format("updateSession Missing data", userId));
			return false;
		}
		String portOccIdStr = (String) sessionData.get(ATSession.DATA_SYMBOLS_PORT);
		String tempSymbolStr = (String) sessionData.get(ATSession.DATA_SYMBOLS_TEMP);
		String[] portOccIds = StringUtils.isBlank(portOccIdStr) ? null : portOccIdStr.split(",");
		String[] tempSymbols = StringUtils.isBlank(tempSymbolStr) ? null : tempSymbolStr.split(",");

		int portCount = 0;
		boolean result = false;
		if (portOccIds != null && portOccIds.length > 0) {
			removeUser(userId, true, false);
			for (String occId : portOccIds) {
				Set<String> userIds = _portOccId2UserMap.get(occId);
				if (userIds == null) {
					userIds = new HashSet<>();
					userIds.add(userId);
					_portOccId2UserMap.put(occId, userIds);
					updateSymbolOptionUserMap(occId, userId);
				}
				userIds.add(userId);
			}
			portCount += portOccIds.length;
			result = true;
		}
		int tempCount = 0;
		if (tempSymbols != null && tempSymbols.length > 0) {
			removeUser(userId, false, true);
			for (String occId : tempSymbols) {
				String symbol = ATModelUtil.getSymbol(occId);
				Set<String> userIds = _tempOccId2UserMap.get(symbol);
				if (userIds == null) {
					userIds = new HashSet<>();
					userIds.add(userId);
					_tempOccId2UserMap.put(symbol, userIds);
				}
				userIds.add(userId);
			}
			tempCount += tempSymbols.length;
			result = true;
		}
		_userIds.add(userId);
		log.info(String.format("updateSession {user: %s, port: %d, temp: %d}", userId, portCount, tempCount));
		return result;
	}

	private void updateSymbolOptionUserMap(String occId_, String userId_) {
		if (occId_ == null || occId_.length() < 10)
			return;
		String symbol = ATModelUtil.getSymbol(occId_);
		Map<String, Set<String>> optionUserMap = this._symbolOptionUserMap.get(symbol);
		if (optionUserMap == null) {
			optionUserMap = new HashMap<>();
			this._symbolOptionUserMap.put(symbol, optionUserMap);
		}
		Set<String> userSet = optionUserMap.get(occId_);
		if (userSet == null) {
			userSet = new HashSet<>();
			optionUserMap.put(occId_, userSet);
		}
		userSet.add(userId_);
	}

	private void removeSymbolOptionUserMapUser(String userId_) {
		Iterator<Entry<String, Map<String, Set<String>>>> i1 = this._symbolOptionUserMap.entrySet().iterator();
		while (i1.hasNext()) {
			Entry<String, Map<String, Set<String>>> symbolOptionUserEntry = i1.next();
			Map<String, Set<String>> optionUserMap = symbolOptionUserEntry.getValue();
			Iterator<Entry<String, Set<String>>> i2 = optionUserMap.entrySet().iterator();
			while (i2.hasNext()) {
				Entry<String, Set<String>> optionUserEntry = i2.next();
				Set<String> userSet = optionUserEntry.getValue();
				if (userSet.remove(userId_) && userSet.isEmpty()) {
					i2.remove();
				}
			}
			if (optionUserMap.isEmpty()) {
				i1.remove();
			}
		}
	}

	private boolean removeUser(String userId_, boolean isRemovePort_, boolean isRemoveTemp_) {
		if (userId_ == null || !_userIds.contains(userId_)) {
			log.info(String.format("removeUser Missing user [%s]", userId_));
			return false;
		}
		if (isRemovePort_ && isRemoveTemp_) {
			_userIds.remove(userId_);
		}

		int portCount = 0;
		if (isRemovePort_) {
			Iterator<Entry<String, Set<String>>> iPort = _portOccId2UserMap.entrySet().iterator();
			while (iPort.hasNext()) {
				Entry<String, Set<String>> entry = iPort.next();
				Set<String> userIds = entry.getValue();
				if (userIds.remove(userId_)) {
					if (userIds.isEmpty()) {
						iPort.remove();
						portCount++;
					}
				}
			}
			removeSymbolOptionUserMapUser(userId_);
		}
		int tempCount = 0;
		if (isRemoveTemp_) {
			Iterator<Entry<String, Set<String>>> iTemp = _tempOccId2UserMap.entrySet().iterator();
			while (iTemp.hasNext()) {
				Entry<String, Set<String>> entry = iTemp.next();
				Set<String> userIds = entry.getValue();
				if (userIds.remove(userId_)) {
					if (userIds.isEmpty()) {
						iTemp.remove();
						tempCount++;
					}
				}
			}
		}
		log.info(String.format("removeUser {user: %s, port: %d, temp: %d}", userId_, portCount, tempCount));
		return true;
	}

	private void sendMarketDataAvailable(String symbol_, Set<String> userIds_) {
		MarketDataStatus.Builder builder = MarketDataStatus.newBuilder();
		builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_AVAILABLE);
		builder.setSubType(ATProtoMarketDataWrapper.SUB_TYPE_OPTION_CHAIN);
		builder.setData(symbol_);
		builder.setTimestamp(System.currentTimeMillis());
		MarketDataStatus status = builder.build();

		sendSseMessage(ATProtoMarketDataWrapper.TYPE_MARKET_AVAILABLE, status, userIds_);
	}

	private void sendMarketData(DelayedMarketData dm_) {
		if (dm_ == null)
			return;

		MarketData.Builder mktBuilder = _marketMap.remove(dm_.occId);
		if (mktBuilder == null)
			return;
		MarketData mkt = mktBuilder.build();
		sendSseMessage(ATProtoMarketDataWrapper.TYPE_MARKET_DATA, mkt, dm_.userIds);
	}

	private void sendMarketDataPack(QueuedMarketDataPack pack_) {
		if (pack_ == null)
			return;
		sendSseMessage(ATProtoMarketDataWrapper.TYPE_MARKET_DATA_PACK, pack_.pack, pack_.userIds);
	}

	private void sendSseMessage(String type_, GeneratedMessageV3 data_, Set<String> userIds_) {
		Builder sseBuilder = SseMessage.newBuilder();
		Any anyMkt = Any.pack(data_);
		sseBuilder.setData(anyMkt);
		sseBuilder.setType(type_);
		sseBuilder.addAllTargets(userIds_);
		SseMessage msg = sseBuilder.build();

		ATKafkaUserMarketService.this._sseProducer.send(msg);
		ATKafkaUserMarketService.this._msgCount++;

		if (log.isTraceEnabled()) {
			log.trace(String.format("sse[%s]", msg.getType()));
		}
		long ts = System.currentTimeMillis();
		if (ts > _nextTimeStamp) {
			long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
			ATKafkaUserMarketService.this._nextTimeStamp = ts + statusPeriod;
			log.info(String.format("sendSseMessage Count [%d]", _msgCount));
		}
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		// status.put("delay.mkt.queue.size", String.format("%d", _delayedMarketQueue.size()));
		status.put("isRunning", String.format("%b", _isRunning));
		status.put("size.marketMap", String.format("%d", _marketMap.size()));
		status.put("size.marketQueue", String.format("%d", _delayedMarketQueue.size()));
		status.put("size.sessionQueue", String.format("%d", this._sessionQueue.size()));// _delayedSessionQueue.size()));

		status.put("size.occid", String.format("%d", _portOccId2UserMap.size() + _tempOccId2UserMap.size()));
		status.put("size.occid.portfolio", String.format("%d", _portOccId2UserMap.size()));
		status.put("size.occid.portfolio.ids", String.format("%s", _portOccId2UserMap.keySet()));
		status.put("size.occid.temporary", String.format("%d", _tempOccId2UserMap.size()));
		status.put("size.occid.temporary.ids", String.format("%s", _tempOccId2UserMap.keySet()));

		status.put("size.symbols", String.format("%d", _symbolOptionUserMap.size()));
		status.put("size.symbols.ids", String.format("%s", _symbolOptionUserMap.keySet()));

		status.put("size.users", String.format("%d", _userIds.size()));
		status.put("size.users.ids", String.format("%s", _userIds));

		status.put("count.messages", String.format("%d", _msgCount));
		return status;
	}

	private class DelayedMarketRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (ATKafkaUserMarketService.this._isRunning) {
					Configuration config = ATConfigUtil.config();
					long queueInterval = config.getLong("usr_mkt.queue.sse.interval");
					DelayedMarketData dm = _delayedMarketQueue.poll(queueInterval, TimeUnit.MILLISECONDS);
					ATKafkaUserMarketService.this.sendMarketData(dm);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (ATKafkaUserMarketService.this._isRunning) {
					run();
				}
			}
			ATKafkaUserMarketService.this._serviceLatch.countDown();
		}
	}

	private static class DelayedMarketData implements Delayed {

		public String occId;
		public Set<String> userIds;
		public long timeout;

		public DelayedMarketData(String occId_, Set<String> userIds_, long delay_) {
			occId = occId_;
			userIds = userIds_;
			timeout = System.currentTimeMillis() + delay_;
		}

		@Override
		public int compareTo(Delayed other_) {
			long otherTimeout = other_.getDelay(TimeUnit.MILLISECONDS);
			int result = Long.compare(timeout, otherTimeout);
			return result;
		}

		@Override
		public long getDelay(TimeUnit unit_) {
			long remainingMs = timeout - System.currentTimeMillis();
			return unit_.convert(remainingMs, TimeUnit.MILLISECONDS);
		}

		@Override
		public boolean equals(Object other_) {
			boolean result = other_ instanceof DelayedMarketData && occId.equals(((DelayedMarketData) other_).occId);
			return result;
		}

		@Override
		public int hashCode() {
			return occId.hashCode();
		}

	}

	private static class QueuedMarketDataPack {

		public QueuedMarketDataPack(MarketDataPack pack_, Set<String> userIds_) {
			this.pack = pack_;
			this.userIds = userIds_;
		}

		Set<String> userIds;
		MarketDataPack pack;
	}

}
