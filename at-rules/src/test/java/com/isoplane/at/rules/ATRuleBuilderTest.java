package com.isoplane.at.rules;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.rules.model.ATSecurityRuleGroup;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATRuleBuilderTest {

	static final Logger log = LoggerFactory.getLogger(ATRuleBuilderTest.class);

	private Gson gson;
	private ATRuleBuilder rulesMgr;

	@BeforeAll
	static public void beforeAll() {
	}

	@AfterAll
	static public void afterAll() {
	}

	@BeforeEach
	public void beforeEach() {
		this.gson = new Gson();
		this.rulesMgr = new ATRuleBuilder();
	}

	@AfterEach
	public void afterEach() {
	}

	@Test
	public void testSimpleStock() {
		String json = ATSysUtils.getResourceFileAsString("test_stock.json");
		this.test(json);
	}

	@Test
	public void testCC() {
		String json = ATSysUtils.getResourceFileAsString("test_cc.json");
		this.test(json);
	}

	@Test
	public void testSpread() {
		String json = ATSysUtils.getResourceFileAsString("test_spread.json");
		this.test(json);
	}

	@Test
	public void testDate() {
		String json = ATSysUtils.getResourceFileAsString("test_date.json");

		String tomorrow = LocalDate.now().plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		this.test(json.replace("XXXX-XX-XX", tomorrow).replace("RESULT", "false"));

		String yesterday = LocalDate.now().minusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		this.test(json.replace("XXXX-XX-XX", yesterday).replace("RESULT", "true"));

	}

	@Test
	public void testDateAndPx() {
		String json = ATSysUtils.getResourceFileAsString("test_date_and_px.json");
		this.test(json);
	}

	public void test(String json_) {
		// String json = getResourceFileAsString(testFile_);
		JsonObject jo = this.gson.fromJson(json_, JsonObject.class);
		TestData[] testData = this.gson.fromJson(jo.get("data"), TestData[].class);
		String ruleJson = jo.get("rule").toString();
		// Set<String> userPositions = new HashSet<>(Arrays.asList(testData[0].id));

		ATSecurityRuleGroup rule = this.rulesMgr.create(ruleJson);
		for (TestData test : testData) {
			Map<String, MarketData> mktMap = new HashMap<>();
			for (TestMarketData tmkt : test.mkt) {
				if (tmkt == null)
					continue;
				MarketData mkt = buildMarketData(tmkt.occId, tmkt.pxLst);
				mktMap.put(mkt.getId(), mkt);
			}
			boolean result = rule.trigger(mktMap);
			log.info(String.format("Running test '%s' -> %b", test.id, result));
			if (test.result) {
				assert (result);
			} else {
				assert (!result);
			}
		}
	}

	static MarketData buildMarketData(String occId_, Double pxLst_) {
		MarketData.Builder builder = MarketData.newBuilder();
		builder.setId(occId_);
		builder.setIsTest(true);
		if (pxLst_ != null) {
			builder.setPxLast(pxLst_);
		}
		MarketData mkt = builder.build();
		return mkt;
	}

	public static class TestData {
		public String id;
		public Boolean result;
		public TestMarketData[] mkt;
	}

	public static class TestMarketData {
		public String occId;
		public Double pxLst;
	}
}
