package com.isoplane.at.rules;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.rules.eval.ATAlertEvaluatorFactory;
import com.isoplane.at.rules.eval.IATAlertEvaluator;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleComponent;
import com.isoplane.at.rules.notification.ATPxNotificationBuilder;

public class ATNotificationBuilderTest {
	static final Logger log = LoggerFactory.getLogger(ATNotificationBuilderTest.class);

	private Gson gson;
	// private ATRuleBuilder rulesMgr;

	@BeforeAll
	static public void beforeAll() {
	}

	@AfterAll
	static public void afterAll() {
	}

	@BeforeEach
	public void beforeEach() {
		this.gson = new Gson();
		// this.rulesMgr = new ATRuleBuilder();
	}

	@AfterEach
	public void afterEach() {
	}

	@Test
	public void testPriceNotificationBuilder() {
		String userId = "test_user";
		Set<String> occIds = new TreeSet<>(Arrays.asList("ID_1"));
		// EATAlertFilter filter = EATAlertFilter.GTE;
		String operator = IATUserAlert.OPERATOR_GTE;
		Double pxTarget = 50.0;
		Double pxActual = 55.0;
		String note = "Test Note";
		boolean isActive = true;
		ATRuleComponent cmp = new ATRuleComponent("TEST", 1.0);
		MarketData mkt = MarketData.newBuilder().setPxLast(pxActual).build();
		Map<String, MarketData> mktMap = Collections.singletonMap("TEST", mkt);

		ATPxNotificationBuilder builder = new ATPxNotificationBuilder(userId, occIds, operator, pxTarget, isActive, note);
		IATAlertEvaluator eval = ATAlertEvaluatorFactory.getBuilder(IATUserAlert.PROP_PX, pxTarget, operator).build();
		eval.ingest(cmp, mktMap);

		assert (eval.evaluate());

		builder.trigger(eval);
		PushNotification alert = builder.build();
		String msg = this.gson.toJson(alert);

		log.info(String.format("Alert: %s", msg));

	}
}
