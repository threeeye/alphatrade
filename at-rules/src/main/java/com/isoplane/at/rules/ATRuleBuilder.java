package com.isoplane.at.rules;

import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATAlertRuleComponent;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.rules.eval.ATAlertEvaluatorFactory;
import com.isoplane.at.rules.model.ATSecurityRuleGroup;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleAlert;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleComponent;
import com.isoplane.at.rules.notification.ATDateNotificationBuilder;
import com.isoplane.at.rules.notification.ATPxNotificationBuilder;
import com.isoplane.at.rules.util.ATGroupLogicCumulator;

public class ATRuleBuilder {

	static final Logger log = LoggerFactory.getLogger(ATRuleBuilder.class);

	public ATSecurityRuleGroup create(String json_) {
		ATUserAlertRulePack pack = parse(json_);
		ATSecurityRuleGroup rule = build(pack, false);
		return rule;
	}

	public ATUserAlertRulePack parse(String json_) {
		if (log.isDebugEnabled()) {
			log.info(String.format("Parsing: %s", json_));
		}
		Gson gson = new Gson();
		ATUserAlertRulePack pack = gson.fromJson(json_, ATUserAlertRulePack.class);
		pack.cleanup();
		return pack;
	}

	public ATSecurityRuleGroup build(ATUserAlertRulePack pack_, boolean isActive_) {
		ATSecurityRuleGroup ruleGroup = new ATSecurityRuleGroup();
		ruleGroup.symbol = pack_.occId;
		ruleGroup.userId = pack_.usrId;
		String groupLogic = StringUtils.isBlank(pack_.groupLogic) ? IATUserAlert.GROUP_LOGIC_A_OR : pack_.groupLogic;
		ruleGroup.resultCumulator = new ATGroupLogicCumulator(groupLogic);

		for (ATAlertRule pAlert : pack_.alerts) {
			try {
				ATRuleAlert rule = new ATRuleAlert();
				String operator = pAlert.getOperator();
				String alertTypeStr = pAlert.getProperty();
				Object alertTargetValue = pAlert.getValue();

				List<ATAlertRuleComponent> pCmps = pAlert.getComponents();
				if (pCmps == null || pCmps.isEmpty()) {
					ATRuleComponent cmp = new ATRuleComponent(pack_.occId, 1.0);
					rule.components.add(cmp);
				} else {
					Double ulCount = pCmps.stream()
							.filter(cmp_ -> cmp_.getOccId().length() < 10)
							.map(cmp_ -> cmp_.getCount()).findFirst().orElse(1.0);
					for (ATAlertRuleComponent pCmp : pCmps) {
						Double count = pCmp.getCount() / (ulCount);// * (pCmp.getOccId().length() < 10 ? 1 : 0.01));
						ATRuleComponent cmp = new ATRuleComponent(pCmp.getOccId(), count);
						rule.components.add(cmp);
					}
				}
				ruleGroup.activeOccIds = rule.components.stream()
						.map(c -> c.occId).collect(Collectors.toCollection(TreeSet::new));

				switch (alertTypeStr) {
				case IATUserAlert.PROP_DATE:
					rule.evalBuilder = ATAlertEvaluatorFactory.getBuilder(alertTypeStr, alertTargetValue, operator);
					rule.notificationBuilder = new ATDateNotificationBuilder(ruleGroup.userId, ruleGroup.activeOccIds, operator, alertTargetValue,
							isActive_, pAlert.getMessage());
					break;
				case IATUserAlert.PROP_PX:
					rule.evalBuilder = ATAlertEvaluatorFactory.getBuilder(alertTypeStr, alertTargetValue, operator);
					rule.notificationBuilder = new ATPxNotificationBuilder(ruleGroup.userId, ruleGroup.activeOccIds, operator, alertTargetValue,
							isActive_, pAlert.getMessage());
					break;
				default:
					log.error(String.format("build: Unsupported alert type [%s]", alertTypeStr));
					continue;
				}

				String group = pAlert.getGroup();
				if (StringUtils.isBlank(group) || IATUserAlert.GROUP_A.equals(group)) {
					ruleGroup.addAlertGroupA(rule);
				} else {
					ruleGroup.addAlertGroupB(rule);
				}
			} catch (Exception ex) {
				log.error(String.format("Error initializing rule: ", pack_), ex);
			}
		}
		return ruleGroup;
	}

}
