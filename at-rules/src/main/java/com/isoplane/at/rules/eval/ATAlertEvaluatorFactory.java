package com.isoplane.at.rules.eval;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.IATUserAlert;

public class ATAlertEvaluatorFactory {

	static final Logger log = LoggerFactory.getLogger(ATAlertEvaluatorFactory.class);

	static public IATAlertEvaluatorBuilder getBuilder(String type_, Object value_, String operator_) {
		IATAlertEvaluatorBuilder builder = null;
		switch (type_) {
		case IATUserAlert.PROP_DATE:
			builder = ATDateEvaluator.newBuilder(value_, operator_);
			break;
		case IATUserAlert.PROP_PX:
			builder = ATPxEvaluator.newBuilder(value_, operator_);
			break;
		default:
			log.error(String.format("Unsupported alert type [%s]", type_));
			break;
		}
		return builder;
	}

}
