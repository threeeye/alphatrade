package com.isoplane.at.rules.eval;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleComponent;
import com.isoplane.at.rules.util.ATFilterFunctionBuilder;
import com.isoplane.at.rules.util.IATFilterFuntcion;

public class ATDateEvaluator implements IATAlertEvaluator {

	private IATFilterFuntcion<Long> filter;
	private Date dateTarget;

	private ATDateEvaluator(Date dateTarget_, IATFilterFuntcion<Long> filter_) {
		this.filter = filter_;
		this.dateTarget = dateTarget_;
	}

	@Override
	public boolean ingest(ATRuleComponent cmp_, Map<String, MarketData> mktMap_) {
		return true;
	}

	@Override
	public boolean evaluate() {
		boolean result = this.filter.filter(this.dateTarget.getTime(), System.currentTimeMillis());
		return result;
	}

	public LocalDate getActualDate() {
		return LocalDate.now();
	}

	static public IATAlertEvaluatorBuilder newBuilder(Object value_, String operator_) {
		return new ATDateEvaluator.Builder(value_, operator_);
	}

	static private class Builder implements IATAlertEvaluatorBuilder {

		private IATFilterFuntcion<Long> filter;
		private Date dateTarget;

		public Builder(Object value_, String operator_) {
			try {
				this.filter = ATFilterFunctionBuilder.buildLongFilter(operator_);
				this.dateTarget = value_ instanceof String ? ATFormats.DATE_yyyy_MM_dd.get().parse((String) value_) : (Date) value_;
			} catch (ParseException ex) {
				throw new ATException(String.format("Invalid date [%s]", value_));
			}
		}

		@Override
		public IATAlertEvaluator build() {
			return new ATDateEvaluator(this.dateTarget, this.filter);
		}

	}
}