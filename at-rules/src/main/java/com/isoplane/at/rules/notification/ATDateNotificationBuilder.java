package com.isoplane.at.rules.notification;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification.Builder;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.rules.eval.ATDateEvaluator;
import com.isoplane.at.rules.eval.IATAlertEvaluator;

public class ATDateNotificationBuilder extends ATBaseNotificationBuilder {

	private DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private String bodyFormatStr;
	private LocalDate dateTarget;
	private LocalDate dateActual;
	private String titleFormatStr;

	public ATDateNotificationBuilder(String userId_, Set<String> occIds_, String operator_, Object dateTarget_, boolean isActive_,
			String note_) {
		super(userId_, occIds_, operator_, dateTarget_, isActive_, note_);
		this.dateTarget = dateTarget_ instanceof String ? LocalDate.parse((String) dateTarget_) : (LocalDate) dateTarget_;

		TreeSet<String> occIds = new TreeSet<>(occIds_);
		String occId = occIds.first();
		String symbol = super.getSymbol();// ATModelUtil.getSymbol(occId);
		String symbolLabel = String.format(occIds_.size() == 1 ? "%s" : "%s...", ATFormats.toLabel(occId));
		String operator = getOperatorLabel(operator_);
		String targetDateStr = this.dateTarget.format(df);
		
		this.bodyFormatStr = StringUtils.isBlank(note_)
				? String.format("💥  %s %s '%s' (%%s%%ddays)", symbolLabel, operator, targetDateStr)
				: String.format("💥  '%s' (%%s%%ddays)", note_);
		this.titleFormatStr = String.format("%s '%%s' %s '%s'", symbol, operator, targetDateStr);

		String iconStar = isActive_ ? "_star" : "";
		String icon = String.format("/globe_price%s.ico", iconStar);
		String mode = isActive_ ? IATUser.MESSAGE_ALL : IATUser.MESSAGE_MIN;
		String tag = String.format("alert_%s_%s_%s", symbol, operator, targetDateStr);

		Builder builder = super.getBuilder();
		builder.setIcon(icon);
		builder.setMode(mode);
		builder.setTag(tag);
	}

	@Override
	public void trigger(IATAlertEvaluator evaluator_) {
		ATDateEvaluator evaluator = (ATDateEvaluator) evaluator_;
		this.dateActual = evaluator.getActualDate();
	}

	@Override
	public PushNotification build() {
		long days = ChronoUnit.DAYS.between(this.dateTarget, this.dateActual);
		String actualDateStr = this.dateActual.format(df);
		String sign = days <= 0 ? "" : "+";
		String body = String.format(this.bodyFormatStr, sign, days);
		String title = String.format(this.titleFormatStr, actualDateStr);

		Builder builder = super.getBuilder();
		builder.setBody(body);
		builder.setTimestamp(System.currentTimeMillis());
		builder.setTitle(title);
		PushNotification alert = builder.build();
		return alert;
	}

}
