package com.isoplane.at.rules.eval;

public interface IATAlertEvaluatorBuilder {

	IATAlertEvaluator build();
}
