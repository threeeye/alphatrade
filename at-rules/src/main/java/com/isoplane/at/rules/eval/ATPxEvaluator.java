package com.isoplane.at.rules.eval;

import java.util.Map;

import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleComponent;
import com.isoplane.at.rules.util.ATFilterFunctionBuilder;
import com.isoplane.at.rules.util.IATFilterFuntcion;

public class ATPxEvaluator implements IATAlertEvaluator {

	private double pxTotal;
	private IATFilterFuntcion<Double> filter;
	private double pxTarget;

	private ATPxEvaluator(Double pxTarget_, IATFilterFuntcion<Double> filter_) {
		this.pxTotal = 0;
		this.filter = filter_;
		this.pxTarget = pxTarget_;
	}

	@Override
	public boolean ingest(ATRuleComponent cmp_, Map<String, MarketData> mktMap_) {
		MarketData mkt = mktMap_.get(cmp_.occId);
		Double px = mkt != null ? mkt.getPxLast() : 0;
		if (px == 0)
			return false;
		double pxEffective = cmp_.count * px; // cmp.factor *
		pxTotal += pxEffective;
		return true;
	}

	@Override
	public boolean evaluate() {
		boolean result = this.filter.filter(this.pxTarget, this.pxTotal);
		return result;
	}

	public Double getTotal() {
		return this.pxTotal;
	}

	static public IATAlertEvaluatorBuilder newBuilder(Object value_, String operator_) {
		return new ATPxEvaluator.Builder(value_, operator_);
	}

	static private class Builder implements IATAlertEvaluatorBuilder {

		private IATFilterFuntcion<Double> filter;
		private double pxTarget;

		public Builder(Object value_, String operator_) {
			this.filter = ATFilterFunctionBuilder.buildDobleFilter(operator_);
			this.pxTarget = value_ instanceof String ? Double.parseDouble((String) value_) : (Double) value_;
		}

		@Override
		public IATAlertEvaluator build() {
			return new ATPxEvaluator(this.pxTarget, this.filter);
		}

	}
}