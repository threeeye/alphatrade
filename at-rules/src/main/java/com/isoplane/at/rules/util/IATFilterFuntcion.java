package com.isoplane.at.rules.util;

public interface IATFilterFuntcion<T> {

	boolean filter(T target, T actual);
}
