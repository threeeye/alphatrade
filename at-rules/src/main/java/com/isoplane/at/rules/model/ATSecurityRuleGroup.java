package com.isoplane.at.rules.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.rules.eval.IATAlertEvaluator;
import com.isoplane.at.rules.eval.IATAlertEvaluatorBuilder;
import com.isoplane.at.rules.notification.IATNotificationBuilder;
import com.isoplane.at.rules.util.ATGroupLogicCumulator;

public class ATSecurityRuleGroup {

	public String symbol;
	public Set<String> activeOccIds;
	public String userId;
	public ATGroupLogicCumulator resultCumulator;
	private List<ATRuleAlert> alertsA;
	private List<ATRuleAlert> alertsB;

	public IATNotificationBuilder getAlertBuilder() {
		IATNotificationBuilder alertBuilder = this.alertsA.get(0).notificationBuilder;
		return alertBuilder;
	}

	public boolean trigger(Map<String, MarketData> mktMap_) {
		boolean resultInitA = this.resultCumulator.init();
		boolean resultA = triggerAlerts(this.alertsA, mktMap_);
		if (resultInitA && !resultA)
			return false;
		if (this.alertsB == null || this.alertsB.isEmpty())
			return resultA;

		boolean resultB = triggerAlerts(this.alertsB, mktMap_);
		boolean totalResult = this.resultCumulator.finalize(resultA, resultB);
		return totalResult;
	}

	private boolean triggerAlerts(List<ATRuleAlert> alerts_, Map<String, MarketData> mktMap_) {
		boolean resultInit = this.resultCumulator.init();
		boolean resultTotal = resultInit;
		for (ATRuleAlert alert : alerts_) {
			IATAlertEvaluator evaluator = alert.evalBuilder.build();
			for (ATRuleComponent cmp : alert.components) {
				if (!evaluator.ingest(cmp, mktMap_)) {
					return false;
				}
			}
			boolean result = evaluator.evaluate();
			if (resultInit && !result)
				return false;
			if (result) {
				alert.notificationBuilder.trigger(evaluator);
			}
			resultTotal = this.resultCumulator.cumulate(resultTotal, result);
		}
		return resultTotal;
	}

	public String getSymbol() {
		return this.symbol;
	}

	public Set<String> getActiveOccIds() {
		return Collections.unmodifiableSet(this.activeOccIds);
	}

	public void addAlertGroupA(ATRuleAlert alert_) {
		if (alert_ == null)
			return;
		if (this.alertsA == null) {
			this.alertsA = new ArrayList<>();
		}
		this.alertsA.add(alert_);
	}

	public void addAlertGroupB(ATRuleAlert alert_) {
		if (alert_ == null)
			return;
		if (this.alertsB == null) {
			this.alertsB = new ArrayList<>();
		}
		this.alertsB.add(alert_);
	}

	public static class ATRuleAlert {
		public String operator;
		public IATAlertEvaluatorBuilder evalBuilder;
		public List<ATRuleComponent> components = new ArrayList<>();
		public IATNotificationBuilder notificationBuilder;
	}

	public static class ATRuleComponent {
		public String occId;
		public Double count;
		public Double factor;

		public ATRuleComponent(String occId_, Double count_) {
			this.occId = occId_;
			this.count = count_;
			this.factor = 1.0;// occId_.length() < 10 ? 1.0 : 100.0;
		}
	}
}
