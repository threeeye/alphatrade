package com.isoplane.at.rules.notification;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.util.ATModelUtil;

public abstract class ATBaseNotificationBuilder implements IATNotificationBuilder {

	private String id;
	private String symbol;
	private PushNotification.Builder builder;

	public ATBaseNotificationBuilder(String userId_, Set<String> occIds_, String operator_, Object dateTarget_, boolean isActive_, String note_) {
		String userId = userId_;

		TreeSet<String> occIds = new TreeSet<>(occIds_);
		String occId = occIds.first();

		this.id = String.format("px %s %s", occId, userId_);
		this.symbol = ATModelUtil.getSymbol(occId);
		String clickAction = String.format("https://tradonado.com/security/%s", this.symbol);
		String meta = Collections.singletonMap("symbol", symbol).toString();

		this.builder = PushNotification.newBuilder();
		this.builder.addUserIds(userId);
		this.builder.setClickAction(clickAction);
		this.builder.setIsActive(isActive_);
		this.builder.setIsSilent(false);
		this.builder.setMeta(meta);
		this.builder.setSymbol(symbol);
		this.builder.setType("alert");
	}

	protected PushNotification.Builder getBuilder() {
		return this.builder;
	}

	protected String getSymbol() {
		return this.symbol;
	}

	@Override
	public String getId() {
		return this.id;
	}

	static protected String getOperatorLabel(String op_) {
		switch (op_) {
		case IATUserAlert.OPERATOR_EQ:
			return "=";
		case IATUserAlert.OPERATOR_GT:
			return ">";
		case IATUserAlert.OPERATOR_GTE:
			return "≥";
		case IATUserAlert.OPERATOR_LT:
			return "<";
		case IATUserAlert.OPERATOR_LTE:
			return "≤";
		default:
			return op_;
		}
	}
}
