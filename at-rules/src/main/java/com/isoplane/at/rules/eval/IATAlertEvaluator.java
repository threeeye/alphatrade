package com.isoplane.at.rules.eval;

import java.util.Map;

import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.rules.model.ATSecurityRuleGroup.ATRuleComponent;

public interface IATAlertEvaluator {

	boolean ingest(ATRuleComponent component, Map<String, MarketData> mktMap);

	boolean evaluate();
}
