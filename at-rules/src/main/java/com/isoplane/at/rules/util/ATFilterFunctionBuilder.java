package com.isoplane.at.rules.util;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATUserAlert;

public class ATFilterFunctionBuilder {

	static public IATFilterFuntcion<Double> buildDobleFilter(String operator_) {
		switch (operator_) {
		case IATUserAlert.OPERATOR_EQ:
			return (targetValue_, testValue_) -> testValue_ == targetValue_;
		case IATUserAlert.OPERATOR_GT:
			return (targetValue_, testValue_) -> testValue_ > targetValue_;
		case IATUserAlert.OPERATOR_GTE:
			return (targetValue_, testValue_) -> testValue_ >= targetValue_;
		case IATUserAlert.OPERATOR_LT:
			return (targetValue_, testValue_) -> testValue_ < targetValue_;
		case IATUserAlert.OPERATOR_LTE:
			return (targetValue_, testValue_) -> testValue_ <= targetValue_;
		default:
			throw new ATException(String.format("Unsupported operator [%s]", operator_));
		}
	}

	static public IATFilterFuntcion<Long> buildLongFilter(String operator_) {
		switch (operator_) {
		case IATUserAlert.OPERATOR_EQ:
			return (targetValue_, testValue_) -> testValue_ == targetValue_;
		case IATUserAlert.OPERATOR_GT:
			return (targetValue_, testValue_) -> testValue_ > targetValue_;
		case IATUserAlert.OPERATOR_GTE:
			return (targetValue_, testValue_) -> testValue_ >= targetValue_;
		case IATUserAlert.OPERATOR_LT:
			return (targetValue_, testValue_) -> testValue_ < targetValue_;
		case IATUserAlert.OPERATOR_LTE:
			return (targetValue_, testValue_) -> testValue_ <= targetValue_;
		default:
			throw new ATException(String.format("Unsupported operator [%s]", operator_));
		}
	}
	
}
