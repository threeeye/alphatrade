package com.isoplane.at.rules.util;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.Message;
import com.isoplane.at.commons.model.IATUserAlert;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATFormats;

@Deprecated
public class ATProtobufExtractor {

	static final Logger log = LoggerFactory.getLogger(ATProtobufExtractor.class);

	private CastFunction<?> castFnc;
	private ExtractFunction extFnc;

	public ATProtobufExtractor(String property_) {
		switch (property_) {
		case IATUserAlert.PROP_DATE:
			this.castFnc = (Object obj) -> {
				try {
					Long d = obj instanceof String ? ATFormats.DATE_yyyy_MM_dd.get().parse((String) obj).getTime() : (Long) obj;
					return d;
				} catch (ParseException ex) {
					log.error(String.format("Error parsing [%s]", obj), ex);
					return null;
				}
			};
			this.extFnc = (Message msg) -> {
				return System.currentTimeMillis();
			};
			break;
		case IATUserAlert.PROP_PX:
			this.castFnc = (Object obj) -> {
				Double d = obj instanceof String ? Double.parseDouble((String) obj) : (Double) obj;
				return d;
			};
			this.extFnc = (Message msg) -> {
				MarketData mkt = (MarketData) msg;
				Double px = mkt.hasPxLast() ? mkt.getPxLast() : null;
				return px;
			};
			break;
		}

	}

	@SuppressWarnings("unchecked")
	public <S> S extract(Message msg_) {
		return (S) this.extFnc.extract(msg_);
	}

	@SuppressWarnings("unchecked")
	public <T> T cast(Object obj_) {
		return (T) this.castFnc.cast(obj_);
	}

	private interface ExtractFunction {
		Object extract(Message msg);
	}

	private interface CastFunction<T> {
		T cast(Object obj);
	}

}
