package com.isoplane.at.rules.util;

import com.isoplane.at.commons.model.IATUserAlert;

public class ATGroupLogicCumulator {

	private String groupLogic;
	private boolean initValue;
	private LogicFunction logicFnc;
	private LogicFunction finalFnc;

	public ATGroupLogicCumulator(String groupLogic_) {
		this.groupLogic = groupLogic_;
		switch (groupLogic_) {
		case IATUserAlert.GROUP_LOGIC_A_AND:
		case IATUserAlert.GROUP_LOGIC_A_AND_OR_B_AND:
			this.initValue = true;
			this.logicFnc = (boolean oldState_, boolean newState_) -> oldState_ && newState_;
			this.finalFnc = (boolean a_, boolean b_) -> a_ || b_;
			break;
		default:
			this.initValue = false;
			this.logicFnc = (boolean oldState_, boolean newState_) -> oldState_ || newState_;
			this.finalFnc = (boolean a_, boolean b_) -> a_ && b_;
			break;
		}
	}

	public String getGroupLogic() {
		return this.groupLogic;
	}

	public boolean init() {
		return this.initValue;
	}

	public boolean cumulate(Boolean oldValue_, boolean newValue_) {
		boolean oldValue = oldValue_ != null ? oldValue_ : this.initValue;
		boolean result = this.logicFnc.cumulate(oldValue, newValue_);
		return result;
	}

	public boolean finalize(boolean a_, boolean b_) {
		boolean result = this.finalFnc.cumulate(a_, b_);
		return result;
	}

	private static interface LogicFunction {
		boolean cumulate(boolean oldState, boolean newState);
	}
}
