package com.isoplane.at.rules.notification;

import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification.Builder;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.rules.eval.ATPxEvaluator;
import com.isoplane.at.rules.eval.IATAlertEvaluator;

public class ATPxNotificationBuilder extends ATBaseNotificationBuilder {

	private String bodyFormatStr;
	private double pxTarget;
	private double pxActual;
	private String titleFormatStr;

	public ATPxNotificationBuilder(String userId_, Set<String> occIds_, String operator_, Object pxTarget_, boolean isActive_,
			String note_) {
		super(userId_, occIds_, operator_, pxTarget_, isActive_, note_);
		this.pxTarget = pxTarget_ instanceof String ? Double.parseDouble((String) pxTarget_) : (Double) pxTarget_;
		TreeSet<String> occIds = new TreeSet<>(occIds_);
		String occId = occIds.first();
		String symbol = ATModelUtil.getSymbol(occId);
		String label = String.format(occIds.size() == 1 ? "%s" : "%s...", ATFormats.toLabel(occId));
		String operator = ATBaseNotificationBuilder.getOperatorLabel(operator_);

		this.bodyFormatStr = StringUtils.isBlank(note_)
				? String.format("💥  %s %s %.2f (%%s%%.2f%%%%)", label, operator, this.pxTarget)
				: String.format("💥  '%s' (%%s%%.2f%%%%)", note_);
		this.titleFormatStr = String.format("%s %%.2f %s %.2f", symbol, operator, this.pxTarget);

		String iconStar = isActive_ ? "_star" : "";
		String icon = String.format("/globe_price%s.ico", iconStar);
		String mode = IATUser.MESSAGE_ALL;
		String tag = String.format("alert_%s_%s_%.2f", symbol, operator, this.pxTarget);

		Builder builder = super.getBuilder();
		builder.setIcon(icon);
		builder.setMode(mode);
		builder.setTag(tag);
	}

	@Override
	public void trigger(IATAlertEvaluator evaluator_) {
		ATPxEvaluator evaluator = (ATPxEvaluator) evaluator_;
		this.pxActual = evaluator.getTotal();
	}

	@Override
	public PushNotification build() {
		double pct = 100 * (this.pxActual - this.pxTarget) / this.pxTarget;
		String sign = pct <= 0 ? "" : "+";
		String body = String.format(this.bodyFormatStr, sign, pct);
		String title = String.format(this.titleFormatStr, this.pxActual);

		Builder builder = super.getBuilder();
		builder.setBody(body);
		builder.setTimestamp(System.currentTimeMillis());
		builder.setTitle(title);
		PushNotification alert = builder.build();
		return alert;
	}

}
