package com.isoplane.at.rules.util;

@Deprecated
public enum EATAlertFilter {

	EQ((Double referenceValue_, Double testValue_) -> testValue_ == referenceValue_),
	GT((Double referenceValue_, Double testValue_) -> testValue_ > referenceValue_),
	GTE((Double referenceValue_, Double testValue_) -> testValue_ >= referenceValue_),
	LT((Double referenceValue_, Double testValue_) -> testValue_ < referenceValue_),
	LTE((Double referenceValue_, Double testValue_) -> testValue_ <= referenceValue_);

	protected FilterFunction filterFnc;

	private EATAlertFilter(FilterFunction fnc_) {
		this.filterFnc = fnc_;
	}

	public boolean filter(Double referenceValue_, Double testValue_) {
		return this.filterFnc.filter(referenceValue_, testValue_);
	}
	
	public interface FilterFunction {
		boolean filter(Double reference, Double test);
	}

}
