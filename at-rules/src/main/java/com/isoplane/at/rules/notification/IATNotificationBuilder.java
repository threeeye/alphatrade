package com.isoplane.at.rules.notification;

import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.rules.eval.IATAlertEvaluator;

public interface IATNotificationBuilder {

//	void init(Object... params_);
	
	String getId();

	void trigger(IATAlertEvaluator evaluator);

	PushNotification build();

}
