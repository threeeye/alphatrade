package com.isoplane.at.svc.inspector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.svc.inspector.tools.ATInspectorMarketDataProducer;
import com.isoplane.at.svc.inspector.tools.ATInspectorMarketDataReceiver;
import com.isoplane.at.svc.inspector.tools.ATInspectorWebServer;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATInspector implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATInspector.class);

	private static ATInspector _instance;
	private static String ID;

	private final CountDownLatch _serviceLatch;
	private boolean _isRunning;
	private long _configHash;

	private ATKafkaSystemStatusProducer _statusProducer;

	private ATInspectorMarketDataProducer _mktProducer;
	private ATInspectorMarketDataReceiver _mktReceiver;
	private ATInspectorWebServer _webServer;

	static public void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);

			final String ipStr = ATSysUtils.getIpAddress("n/a");

			ID = String.format("%s:%s", ATInspector.class.getSimpleName(), ipStr);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					_instance.shutdown();
					if (_instance != null) {
						ATExecutors.shutdown();
					}
				}
			}));

			ATExecutors.init();
			ArrayList<Class<?>> classes = new ArrayList<>();
			classes.add(ATKafkaSystemStatusProducer.class);
			if (isActive(ATInspectorMarketDataProducer.KEY)) {
				classes.add(ATKafkaMarketDataPackProducer.class);
			}
			if (isActive(ATInspectorMarketDataReceiver.KEY)) {
				classes.add(ATKafkaMarketDataPackConsumer.class);
			}
			ATKafkaRegistry.register("insp_src", classes.toArray(new Class<?>[0]));

			_instance = new ATInspector();
			_instance.init();

			String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
			log.info(msg);

			_instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATInspector.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private ATInspector() {
		Configuration config = ATConfigUtil.config();
		int count = 0;
		if (config.getBoolean(ATInspectorMarketDataProducer.KEY, false)) {
			count += 1;
		}
		_serviceLatch = new CountDownLatch(count);
		ATSystemStatusManager.register(this);
	}

	private void init() {
		try {
			ATExecutors.scheduleAtFixedRate("config_monitor", new Runnable() {

				@Override
				public void run() {
					updateConfig();
				}
			}, 5000, 5000);

			if (isActive(ATInspectorMarketDataProducer.KEY)) {
				_mktProducer = new ATInspectorMarketDataProducer();
				_mktProducer.init();
			}
			if (isActive(ATInspectorMarketDataReceiver.KEY)) {
				_mktReceiver = new ATInspectorMarketDataReceiver();
				_mktReceiver.init();
			}
			if (isActive(ATInspectorWebServer.KEY)) {
				_webServer = new ATInspectorWebServer();
				_webServer.init();
			}

			ATKafkaRegistry.start();
			_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	private void updateConfig() {
		Configuration config = ATConfigUtil.config();
		long newHash = 0;
		Iterator<String> keys = config.getKeys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = config.getProperty(key);
			newHash += (key.hashCode() + value.hashCode());
		}
		if (_configHash == newHash)
			return;

		_configHash = newHash;
		log.info(String.format("Config changed!"));
		_mktReceiver.updateConfig();
	}

	private void start() {
		_isRunning = true;
		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

		if (isActive(ATInspectorMarketDataProducer.KEY)) {
			_mktProducer.start();
		}
		// ATKafkaRegistry.start();
	}

	public void shutdown() {
		_isRunning = false;
		log.debug(String.format("Shutting down"));
		if (isActive(ATInspectorMarketDataProducer.KEY)) {
			this._mktProducer.shutdown();
			this._serviceLatch.countDown();
		}
		ATKafkaRegistry.stop();
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	static private boolean isActive(String key_) {
		boolean isActive = ATConfigUtil.config().getBoolean(key_, false);
		return isActive;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("timestamp", ATFormats.DATE_TIME_mid.get().format(new Date()));
		statusMap.put("mkt.producer", String.format("%b", isActive(ATInspectorMarketDataProducer.KEY)));
		return statusMap;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (_isRunning) {
					long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
					Map<String, String> status = ATSystemStatusManager.getStatus();
					_statusProducer.send(ID, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (_isRunning) {
					run();
				}
			}
		}
	}

}
