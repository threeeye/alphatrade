package com.isoplane.at.svc.inspector.tools;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATInspectorMarketDataProducer implements IATSystemStatusProvider {

    static final Logger log = LoggerFactory.getLogger(ATInspectorMarketDataProducer.class);

    public static final String KEY = "inspector.mkt.producer";

    static public final String SOURCE_KEY = "_INSP_";

    private boolean _isRunning = false;
    private ATKafkaMarketDataPackProducer _kafkaMarketDataPackProducer;
    private MarketRunner _marketWorker;
    private Random _random;

    public ATInspectorMarketDataProducer() {
        ATSystemStatusManager.register(this);
    }

    public void init() {
        Configuration config = ATConfigUtil.config();
        long period = config.getLong(String.format("%s.period", KEY), 5000);
        String symStr = config.getString(String.format("%s.symbols", KEY));
        log.info(String.format("Symbols: %s", symStr));
        log.info(String.format("Period : %d", period));

        _random = new Random(System.currentTimeMillis());
        _kafkaMarketDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);

        String requestThreadId = String.format("%s", this.getClass().getSimpleName());
        this._marketWorker = new MarketRunner(requestThreadId);
    }

    public void start() {
        this._isRunning = true;
        ATExecutors.submit(this._marketWorker._threadId, this._marketWorker);
    }

    public void shutdown() {
        this._isRunning = false;
    }

    @Override
    public Map<String, String> getSystemStatus() {
        // Configuration config = ATConfigUtil.config();
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("timestamp", ATFormats.DATE_TIME_mid.get().format(new Date()));
        statusMap.put("symbols", String.format("%s", this._marketWorker._symbols));
        statusMap.put("cycle.count", String.format("%d", this._marketWorker._cycleCount));
        statusMap.put("cycle.duration", String.format("%s", this._marketWorker._cycleDuration));
        statusMap.put("cycle.period", String.format("%d", this._marketWorker._period));
        return statusMap;
    }

    @Override
    public boolean isRunning() {
        return _isRunning;
    }

    private Map<String, MarketData> generateMarketData(Collection<String> syms_) {
        if (syms_ == null)
            return null;
        long ts_lst = System.currentTimeMillis();
        Map<String, MarketData> map = new HashMap<>();
        for (String sym : syms_) {
            double px_lst = this._random.nextDouble(0.1, 10.0);
            double spread = this._random.nextDouble(px_lst / 100.0);
            MarketData.Builder builder = MarketData.newBuilder();
            builder.setId(sym);
            builder.setIdUl(sym);
            builder.setActionCode(EATChangeOperation.UPDATE.name());
            builder.setSrc(SOURCE_KEY);
            builder.setPxLast(px_lst);
            builder.setTsLast(ts_lst);
            builder.setPxAsk(px_lst + spread);
            builder.setTsAsk(ts_lst);
            builder.setPxBid(px_lst - spread);
            builder.setTsBid(ts_lst);
            MarketData mkt = builder.build();
            map.put(sym, mkt);
        }
        return map;
    }

    private void sendMarketData(Map<String, MarketData> data_) {
        if (data_ == null || data_.isEmpty())
            return;
        MarketDataPack.Builder builder = MarketDataPack.newBuilder();
        builder.addAllMarketData(data_.values());
        builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
        builder.setSubType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA_PACK);
        builder.setSource(ATInspectorMarketDataProducer.SOURCE_KEY);
        builder.addAllSymbols(data_.keySet());
        builder.setSeqLength(data_.size());
        builder.setSeq((int) this._marketWorker._cycleCount);
        MarketDataPack pack = builder.build();
        ATProtoMarketDataPackWrapper wrapper = new ATProtoMarketDataPackWrapper(pack);
        if (log.isDebugEnabled()) {
            log.debug(String.format("sendMarketData [%02d/%d]", this._marketWorker._cycleCount + 1, data_.size()));
        }
        this._kafkaMarketDataPackProducer.send(wrapper);
    }

    private class MarketRunner implements Runnable {

        private String _threadId;
        private long _cycleCount;
        private Duration _cycleDuration = Duration.ZERO;
        private long _period;
        private Set<String> _symbols;

        public MarketRunner(String threadId_) {
            _threadId = threadId_;
            _cycleCount = 0;
            _period = 5000;
        }

        @Override
        public void run() {
            Thread.currentThread().setName(_threadId);
            do {
                try {
                    long start = System.currentTimeMillis();
                    this._cycleCount++;
                    Configuration config = ATConfigUtil.config();
                    this._period = config.getLong(String.format("%s.period", KEY), 5000);

                    String symStr = config.getString(String.format("%s.symbols", KEY));
                    if (!StringUtils.isBlank(symStr)) {
                        this._symbols = Arrays.stream(symStr.split(",")).map(String::trim)
                                .collect(Collectors.toSet());

                        if (log.isDebugEnabled()) {
                            log.debug(String.format("Processing: %s", this._symbols));
                        }
                        Map<String, MarketData> data = ATInspectorMarketDataProducer.this.generateMarketData(this._symbols);
                        if (data != null && !data.isEmpty()) {
                            ATInspectorMarketDataProducer.this.sendMarketData(data);
                        }
                    } else {
                        log.warn(String.format("%s Error: No symbols defined", this.getClass().getSimpleName()));
                    }
                    long durationMs = System.currentTimeMillis() - start;
                    _cycleDuration = Duration.ofMillis(durationMs);

                    _cycleCount++;
                    long delta = this._period - durationMs;
                    if (delta > 0) {
                        Thread.sleep(delta);
                    }
                } catch (Exception ex) {
                    log.error(String.format("% Error", this.getClass().getSimpleName()), ex);
                }
            } while (ATInspectorMarketDataProducer.this._isRunning);
        }
    }

}
