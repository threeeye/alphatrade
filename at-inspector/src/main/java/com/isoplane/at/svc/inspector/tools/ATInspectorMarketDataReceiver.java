package com.isoplane.at.svc.inspector.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATInspectorMarketDataReceiver implements IATProtoMarketSubscriber, IATSystemStatusProvider {

    static final Logger log = LoggerFactory.getLogger(ATInspectorMarketDataReceiver.class);

    public static final String KEY = "inspector.mkt.receiver";

    private boolean _isLogging;
    private long _count;
    private Set<String> _symbolsRec;
    private Set<String> _symbolsFilter;

    public ATInspectorMarketDataReceiver() {
        _symbolsRec = new TreeSet<>();
        _symbolsFilter = new TreeSet<>();
        ATSystemStatusManager.register(this);
    }

    public void init() {
        ATKafkaMarketDataPackConsumer mktDataPackRC = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
        mktDataPackRC.subscribe(this);
        updateConfig();
    }

    public void updateConfig() {
        Configuration config = ATConfigUtil.config();
        _isLogging = config.getBoolean(String.format("%s.log", KEY));

        String symStr = config.getString(String.format("%s.symbols", KEY));
        _symbolsFilter = StringUtils.isBlank(symStr) ? new HashSet<>()
                : Arrays.stream(symStr.split(",")).map(String::trim)
                        .collect(Collectors.toSet());
    }

    @Override
    public void notifyMarketData(Collection<ATProtoMarketDataWrapper> data_) {
        if (data_ == null || data_.isEmpty())
            return;
        for (ATProtoMarketDataWrapper wrapper : data_) {
            MarketData mkt = wrapper.proto();
            String id = mkt.getId();
            if (this._symbolsFilter != null && !this._symbolsFilter.contains(id))
                continue;
            this._count++;
            this._symbolsRec.add(id);
            if (this._isLogging) {
                log.info(String.format("MD: %s", id));
            }
        }
    }

    @Override
    public void notifyMarketDataPack(ATProtoMarketDataPackWrapper data_) {
        if (data_ == null || !ATProtoMarketDataWrapper.TYPE_MARKET_DATA.equals(data_.getType()))
            return;
        List<MarketData> list = data_.proto().getMarketDataList();
        for (MarketData mkt : list) {
            String id = mkt.getId();
            if (this._symbolsFilter != null && !this._symbolsFilter.contains(id))
                continue;
            this._count++;
            this._symbolsRec.add(id);
            if (this._isLogging) {
                log.info(String.format("MDP: %s", id));
            }
        }
    }

    @Override
    public void notifyMarketStatus(ATProtoMarketStatusWrapper data_) {
        // Ingored
    }

    @Override
    public Map<String, String> getSystemStatus() {
        Map<String, String> statusMap = new HashMap<>();
        statusMap.put("timestamp", ATFormats.DATE_TIME_mid.get().format(new Date()));
        List<String> symbols = new ArrayList<>(this._symbolsRec);
        statusMap.put("symbols", String.format("%s", symbols));
        statusMap.put("count", String.format("%d", this._count));
        return statusMap;
    }

    @Override
    public boolean isRunning() {
        return true;
    }
}
