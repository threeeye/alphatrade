package com.isoplane.at.svc.inspector.tools;

import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.Javalin;
import io.javalin.http.staticfiles.Location;

public class ATInspectorWebServer {

    static final Logger log = LoggerFactory.getLogger(ATInspectorWebServer.class);

    public static final String KEY = "inspector.webserver";

    private Javalin _server;

    public void init() {
        Configuration config = ATConfigUtil.config();
        int port = config.getInt(String.format("%s.port", KEY));
        String webroot = config.getString(String.format("%s.webroot", KEY));
        _server = Javalin.create(webConfig -> {
            webConfig.addStaticFiles(staticFiles -> {
                staticFiles.hostedPath = "/";
                staticFiles.directory = webroot;
                staticFiles.location = Location.EXTERNAL;
            });
        });
        _server.sse("/sse", client -> {
            client.sendEvent("connected", "Hello, SSE");
            client.onClose(() -> log.info("Client disconnected"));
        });

        // _server.get("/", ctx -> ctx.result(String.format("%s says 'Hello!'",
        // this.getClass().getSimpleName())));
        _server.start(port);
        log.info(String.format("%s started on port [%d]", this.getClass().getSimpleName(), port));

        ATExecutors.scheduleAtFixedRate("sse", new Runnable() {

            @Override
            public void run() {
                

            }
        }, 5000, 5000);
    }

}
