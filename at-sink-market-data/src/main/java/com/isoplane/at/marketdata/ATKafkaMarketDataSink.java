package com.isoplane.at.marketdata;

import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusConsumer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.marketdata.util.ATMarketDataMongodbTools;

public class ATKafkaMarketDataSink implements Runnable, IATProtoMarketSubscriber, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataSink.class);

	static private String ID;

	private boolean _isRunning;
	// private Configuration _config;
	private ArrayBlockingQueue<Collection<ATProtoMarketDataWrapper>> _storageQueue;
	private final CountDownLatch _serviceLatch;// = new CountDownLatch(2);
	private long _queueInterval;
	private int _queueSize;
	private int _storeCounter = 0; // Note: Used as throttle
	private int _maxStoreCounter = 10;
	private long _persistenceCounter = 0; // Note: Count of elements stored
	private ATMarketDataMongodbTools _mongodb;
	private ATPositionHistorySink _poshistSink;

	private ATKafkaSystemStatusProducer _statusProducer;

	public static void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATKafkaRegistry.register("system_status",
					ATKafkaMarketDataConsumer.class,
					ATKafkaMarketDataPackConsumer.class,
					ATKafkaMarketStatusConsumer.class);

			ATKafkaMarketDataSink instance = new ATKafkaMarketDataSink(config);
			instance.init();
			instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATMarketDataManager.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaMarketDataSink(Configuration config_) {
		_serviceLatch = new CountDownLatch(1);
	}

	public void init() throws UnknownHostException {

		Configuration config = ATConfigUtil.config();

		final String ipStr = ATSysUtils.getIpAddress("n/a");
		ID = String.format("%s:%s", this.getClass().getSimpleName(), ipStr);

		this._queueSize = config.getInt("market.sinkSize");
		this._queueInterval = config.getLong("market.sinkInterval");
		log.info(String.format("Initializing sink [%d/%s]", this._queueSize, this._queueInterval));
		this._storageQueue = new ArrayBlockingQueue<>(this._queueSize);

		this._mongodb = new ATMarketDataMongodbTools();
		this._mongodb.init(true);

		ATSystemStatusManager.register(this);

		ATKafkaMarketDataConsumer mktDataCs = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
		ATKafkaMarketDataPackConsumer mktDataPackCs = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
		mktDataCs.subscribe(this);
		mktDataPackCs.subscribe(this);
		ATKafkaMarketStatusConsumer mktStatusCs = ATKafkaRegistry.get(ATKafkaMarketStatusConsumer.class);
		mktStatusCs.subscribe(this);

		ATKafkaRegistry.register("mkt_data_sink", ATKafkaSystemStatusProducer.class);
		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

		this._poshistSink = new ATPositionHistorySink(this._mongodb);

		boolean isPersisting = config.getBoolean("market.sink.persisting", false);
		if(!isPersisting) {
			log.warn(String.format("############### NOT PERSISTING! ###############"));
		}
	}

	public boolean store(Collection<ATProtoMarketDataWrapper> data_) {
		if (!this._isRunning || data_ == null || data_.isEmpty())
			return false;
		return this._storageQueue.offer(data_);
	}

	private void runPersister() {
		while (this._isRunning) {
			try {
				Collection<ATProtoMarketDataWrapper> wrappers = _storageQueue.poll(_queueInterval, TimeUnit.MILLISECONDS);
				if (wrappers == null || wrappers.isEmpty())
					continue;
				if (log.isDebugEnabled()) {
					log.debug(String.format("runPersister [%d]", wrappers.size()));
				}
				// ATProtoMarketDataWrapper hit = wrappers.stream().filter(w -> w.proto().getId().length() > 10).findFirst()
				// .orElse(null);
				// if (hit != null) {
				// log.info(String.format("hit: %s", hit.proto().getId()));
				// }
				Configuration config = ATConfigUtil.config();
				boolean isPersisting = config.getBoolean("market.sink.persisting", false);
				long count = isPersisting ? _mongodb.store(wrappers, null) : wrappers.size();
				_persistenceCounter += count;
				if (_storeCounter++ == _maxStoreCounter) {
					_storeCounter = 0;
					log.info(String.format("Persistence count [%d]", _persistenceCounter));
					_maxStoreCounter = config.getInt("market.sink.logThrottle");
				}
			} catch (Exception ex) {
				log.error(String.format("runPersister Error"), ex);
			}
		}
		this._serviceLatch.countDown();
	}

	@Override
	public void run() {
		this.runPersister();
	}

	public void shutdown() {
		ATKafkaRegistry.stop();
		this.stop();
	}

	public void start() {
		this._isRunning = true;
		ATKafkaRegistry.start();
		ATExecutors.submit(this.getClass().getSimpleName(), this);
		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());
		this._poshistSink.start();
	}

	public void stop() {
		this._isRunning = false;
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
		if (wrappers_ == null || wrappers_.isEmpty())
			return;		
		boolean result = this.store(wrappers_);
		if (!result) {
			log.warn(String.format("notifyMarketData Error: Failed to store. Remaining capacity [%d]", _storageQueue.remainingCapacity()));
		}
		if (log.isDebugEnabled() && result) {
			log.debug(String.format("Queue added [%d]", wrappers_.size()));
		}
		this._poshistSink.notifyMarketData(wrappers_);
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper wrapper_) {
		if (wrapper_ == null || !ATProtoMarketDataWrapper.TYPE_MARKET_DATA.equals(wrapper_.getType()))
			return;
		List<ATProtoMarketDataWrapper> wrappers = wrapper_.proto().getMarketDataList().stream().map(m -> new ATProtoMarketDataWrapper(m))
				.collect(Collectors.toList());
		notifyMarketData(wrappers);
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper wrapper_) {
		// NOTE: Ignored for sink
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("size.storageQueue", String.format("%d", _storageQueue.size()));
		status.put("remaining.storageQueue", String.format("%d", _storageQueue.remainingCapacity()));
		status.put("count.persistence", String.format("%d", _persistenceCounter));
		boolean isPersisting = ATConfigUtil.config().getBoolean("market.sink.persisting", false);
		status.put("persisting", String.format("%b", isPersisting));
		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (_isRunning) {
					long statusPeriod = ATConfigUtil.config().getLong("status.period", 5000);
					Map<String, String> status = ATSystemStatusManager.getStatus();
					_statusProducer.send(ID, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (_isRunning) {
					run();
				}
			}
		}
	}

}
