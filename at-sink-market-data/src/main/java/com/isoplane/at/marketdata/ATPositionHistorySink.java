package com.isoplane.at.marketdata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.marketdata.util.ATMarketDataMongodbTools;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPositionHistorySink implements IATSystemStatusProvider {

    static final Logger log = LoggerFactory.getLogger(ATPositionHistorySink.class);

    private boolean _isRunning = false;
    private ArrayBlockingQueue<Collection<ATProtoMarketDataWrapper>> _bufferQueue;
    private Map<String, ATProtoMarketDataWrapper> _protoMap;
    private ATPortfolioMogodbTools _positionMongoDb;
    private ATMarketDataMongodbTools _mktMongoDb;

    public ATPositionHistorySink(ATMarketDataMongodbTools mongodb_) {
        this._mktMongoDb = mongodb_;
    }

    public void start() {
        var config = ATConfigUtil.config();

        this.init();

        this._isRunning = true;
        var positionRefreshPeriod = config.getLong("pos_hist.position.interval");
        var flushPeriod = config.getLong("pos_hist.flush.interval");
        ATExecutors.submit(String.format("%s.buffer", this.getClass().getSimpleName()), () -> this.runBuffer());
        ATExecutors.scheduleAtFixedRate(String.format("%s.position", this.getClass().getSimpleName()),
                new PositionLoader(), positionRefreshPeriod, positionRefreshPeriod);
        ATExecutors.scheduleAtFixedRate(String.format("%s.flush", this.getClass().getSimpleName()),
                new HistorySaver(), flushPeriod, flushPeriod);
    }

    private void init() {
        var config = ATConfigUtil.config();

        this._protoMap = new HashMap<>();
        var sinkSize = config.getInt("pos_hist.sinkSize");
        this._bufferQueue = new ArrayBlockingQueue<>(sinkSize);

        this._positionMongoDb = new ATPortfolioMogodbTools(false, null);
        this._positionMongoDb.init();

        ATSystemStatusManager.register(this);
    }

    public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
        if (wrappers_ != null) {
            this._bufferQueue.offer(wrappers_);
        }
    }

    private void runBuffer() {
        var config = ATConfigUtil.config();
        var bufferInterval = 10000L;
        while (this._isRunning) {
            try {
                Collection<ATProtoMarketDataWrapper> wrappers = _bufferQueue.poll(bufferInterval,
                        TimeUnit.MILLISECONDS);
                if (wrappers == null || wrappers.isEmpty() || this._protoMap.isEmpty())
                    continue;
                for (ATProtoMarketDataWrapper wrapper : wrappers) {
                    var occId = wrapper.proto().getId();
                    if (this._protoMap.containsKey(occId)) {
                        this._protoMap.put(occId, wrapper);
                    }
                }
                bufferInterval = config.getLong("pos_hist.buffer.interval");
            } catch (Exception ex) {
                log.error(String.format("runBuffer Error"), ex);
            }
        }
    }

    @Override
    public Map<String, String> getSystemStatus() {
        Map<String, String> status = new TreeMap<>();
        status.put("size.positions", String.format("%d", this._protoMap.size()));
        // status.put("remaining.storageQueue", String.format("%d", _storageQueue.remainingCapacity()));
        // status.put("count.persistence", String.format("%d", _persistenceCounter));
        // boolean isPersisting = ATConfigUtil.config().getBoolean("market.sink.persisting", false);
        // status.put("persisting", String.format("%b", isPersisting));
        return status;
    }

    @Override
    public boolean isRunning() {
        return _isRunning;
    }

    public class PositionLoader implements Runnable {

        public PositionLoader() {
            ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
        }

        @Override
        public void run() {
            IATWhereQuery query = ATPersistenceRegistry.query().empty();
            var digests = ATPositionHistorySink.this._positionMongoDb.getTradeDigest(query);
            if (digests == null || digests.isEmpty())
                return;
            var occIds = digests.stream().map(ATTradeDigest::getOccId).collect(Collectors.toSet());
            var symIds = occIds.stream().map(ATModelUtil::getSymbol).collect(Collectors.toSet());
            occIds.addAll(symIds);
            var newMap = new HashMap<>(ATPositionHistorySink.this._protoMap);
            newMap.keySet().retainAll(occIds);
            occIds.removeAll(newMap.keySet());
            for (String occId : occIds) {
                newMap.put(occId, null);
            }
            ATPositionHistorySink.this._protoMap = newMap;
            log.info(String.format("Monitoring positions [%d]", newMap.size()));
        }

    }

    public class HistorySaver implements Runnable {

        public HistorySaver() {

        }

        @Override
        public void run() {
            var table = ATMongoTableRegistry.getPositionHistoryTable();
            if (ATPositionHistorySink.this._protoMap == null
                    || ATPositionHistorySink.this._protoMap.isEmpty() && table != null)
                return;
            var wrappers = new ArrayList<>(ATPositionHistorySink.this._protoMap.values());
            while (wrappers.remove(null))
                ;
            if (!wrappers.isEmpty()) {
                ATPositionHistorySink.this._mktMongoDb.storeDailyHistory(wrappers);
            }
            log.info(String.format("Saved position history [%d]", wrappers.size()));
        }

    }

}
