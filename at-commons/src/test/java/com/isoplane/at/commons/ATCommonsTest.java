package com.isoplane.at.commons;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.util.ATSysLogger;

//@RunWith(SpringJUnit4ClassRunner.class)
public class ATCommonsTest {

	static final Logger log = LoggerFactory.getLogger(ATCommonsTest.class);

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		// ATSysLogger syslog = new ATSysLogger();
		ATSysLogger.info("hello");
		Thread.sleep(2000);
	}

}
