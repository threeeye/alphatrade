package com.isoplane.at.commons;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Map;
import java.util.TreeMap;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMapMessageWrapper;
import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;

public class ATProtoMapMessageWrapperTest {

	static final Logger log = LoggerFactory.getLogger(ATProtoMapMessageWrapperTest.class);

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		Map<String, Object> testMap = new TreeMap<>();
		testMap.put("int1", 1);
		testMap.put("int2", 2);
		testMap.put("double", 1.1);
		testMap.put("long", 10L);
		testMap.put("String", "string");
		Map<String, Object> testSubMap1 = new TreeMap<>();
		testSubMap1.put("intSub", 11);
		testSubMap1.put("doubleSub", 11.11);
		testSubMap1.put("longSub", 101L);
		testSubMap1.put("StringSub", "string sub");
		testMap.put("subMap1", testSubMap1);
		Map<String, Object> testSubMap2 = new TreeMap<>();
		testSubMap2.put("intSub", 22);
		testMap.put("subMap2", testSubMap2);

		MapMessage mapMessage = ATProtoMapMessageWrapper.toMapMessage("test", "test_id", "test_src", "test_tgt", System.currentTimeMillis(), testMap);
		assertTrue(mapMessage != null);
		Map<String, Object> resultMap = ATProtoMapMessageWrapper.toMap(mapMessage, new TreeMap<>());
		assertTrue(resultMap != null);
	}
}
