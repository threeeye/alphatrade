compile with:

Single file:
C:\Dev\util\protoc-3.19.4-win64\bin\protoc.exe -I="C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf" --java_out="C:\Dev\workspace\alphatrade\at-commons\src\main\generated-java" C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf\sse_message.proto

All files:
C:\Dev\util\protoc-3.14.0-win64\bin\protoc --proto_path=C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf\  --java_out="C:\Dev\workspace\alphatrade\at-commons\target\generated-sources\" --experimental_allow_proto3_optional C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf\*.proto