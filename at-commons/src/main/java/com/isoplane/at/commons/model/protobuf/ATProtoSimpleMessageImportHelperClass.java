package com.isoplane.at.commons.model.protobuf;

import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage;

public class ATProtoSimpleMessageImportHelperClass {

	private SimpleMessage _protoMsg;
 
	public ATProtoSimpleMessageImportHelperClass(SimpleMessage msg_) {
		this._protoMsg = msg_;
	}

	public SimpleMessage proto() {
		return this._protoMsg;
	}


}
