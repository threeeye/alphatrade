package com.isoplane.at.commons.store;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATEarning;
import com.isoplane.at.commons.model.IATEquityDescription;
import com.isoplane.at.commons.model.IATEquityFoundation;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATSymbol;

public class ATPersistentEquityFoundation extends ATPersistentLookupBase
		implements IATEquityFoundation, IATEquityDescription, IATDividend, IATEarning, IATStatistics, Comparable<ATPersistentEquityFoundation>,
		IATAssetConstants {

	static final Logger log = LoggerFactory.getLogger(ATPersistentEquityFoundation.class);

	private static final long serialVersionUID = 1L;

	// private SimpleDateFormat _yyyyMMddFormat = ATFormats.DATE_yyyyMMdd.get();

	// static private DateTimeFormatter _yyyyMMddFormater = DateTimeFormatter.ofPattern("yyyyMMdd");

	// transient private String _note;

	public ATPersistentEquityFoundation() {
	}

	public ATPersistentEquityFoundation(String occId_) {
		super.setAtId(occId_);
		super.put(IATAssetConstants.OCCID, occId_);
	}

	public ATPersistentEquityFoundation(Map<String, Object> data_) {
		super(data_ == null ? new HashMap<>() : data_);
		Iterator<Entry<String, Object>> i = this.entrySet().iterator();
		while (i.hasNext()) {
			Object value = i.next().getValue();
			if (value == null) {
				i.remove();
			}
		}
	}

	public ATPersistentEquityFoundation(IATSymbol symbol_) {
		String id = symbol_.getId();
		if (id.length() > 8)
			throw new ATException(String.format("Cannot create foundation with Option ID", id));
		super.setAtId(id);
		super.put(IATAssetConstants.OCCID, id);
		super.put(IATAssetConstants.MODEL, symbol_.getModel());
		super.put(IATAssetConstants.NAME, symbol_.getName());
		super.put(IATAssetConstants.DESCRIPTION, symbol_.getDescription());
		super.put(IATAssetConstants.SECTOR, symbol_.getSector());
		super.put(IATAssetConstants.INDUSTRY, symbol_.getIndustry());
	}

	@Override
	public int compareTo(ATPersistentEquityFoundation other_) {
		return this.getOccId().compareTo(other_.getOccId());
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	public Double getMarketCap() {
		return (Double) get(MARKET_CAP);
	}

	public String getModel() {
		return (String) get(MODEL);
	}

	public TreeMap<Integer, Double> getVolatilityMap() {
		final Integer[] hvs = new Integer[] { 10, 30, 60, 90, 125, 254 };
		TreeMap<Integer, Double> result = new TreeMap<>();
		for (Integer hvi : hvs) {
			String hvCode = "hv" + hvi;
			Double hv = this.getHV(hvCode);
			if (hv != null) {
				result.put(hvi, hv);
			}
		}
		return result.isEmpty() ? null : result;
	}

	@Override
	public String getOccId() {
		return (String) get(OCCID);
	}

	@Override
	public String getLabel() {
		return getOccId();
	}

	@Override
	public String getSymbol() {
		return getOccId();
	}

	@Override
	public String getDescription() {
		return (String) get(DESCRIPTION);
	}

	@Override
	public String getIndustry() {
		return (String) get(INDUSTRY);
	}

	@Override
	public String getSector() {
		return (String) get(SECTOR);
	}

	@Override
	public String getEarningsDateStr() {
		return (String) get(EARNINGS_DATE_STR_OLD);
	}

	@Override
	public Date getEarningsDate() {
		String dstr = null;
		try {
			dstr = getEarningsDateStr();
			if (StringUtils.isBlank(dstr))
				return null;
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			log.error(String.format("getEarningsDate Error [%s]", dstr), ex);
			return null;
		}
	}

	@Override
	public String getDividendExDateStr() {
		return (String) get(DIV_EX_DATE_STR_OLD);
	}

	public Date getDividendExDate(boolean includeEstimated_) {
		Date d = getDividendExDate();
		if (!includeEstimated_ || d != null) {
			return d;
		}
		d = getDividendEstimatedDate();
		return d;
	}

	@Override
	public Date getDividendExDate() {
		String dstr = getDividendExDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			log.error(String.format("getDividendExDate Error [%s]: %s", dstr, ex.getMessage()));
			return null;
		}
	}

	@Override
	public String getDividendRecordDateStr() {
		return (String) get(DIV_REC_DATE_STR);
	}

	@Override
	public Date getDividendRecordDate() {
		String dstr = getDividendRecordDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			log.error(String.format("getDividendRecordDate Error [%s]: %s", dstr, ex.getMessage()));
			return null;
		}
	}

	@Override
	public String getDividendPayDateStr() {
		return (String) get(DIV_PAY_DATE_STR);
	}

	@Override
	public Date getDividendPayDate() {
		String dstr = getDividendPayDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			log.error(String.format("getDividendPayDate Error [%s]: %s", dstr, ex.getMessage()));
			return null;
		}
	}

	@Override
	public Double getDividend() {
		return (Double) get(DIVIDEND);
	}

	@Override
	public Integer getDividendFrequency() {
		return (Integer) get(DIV_FREQUENCY);
	}

	@Override
	public Double getHV10() {
		return (Double) get(HV10);
	}

	@Override
	public Double getHV20() {
		return (Double) get(HV20);
	}

	@Override
	public Double getHV30() {
		return (Double) get(HV30);
	}

	@Override
	public Double getHV60() {
		return (Double) get(HV60);
	}

	@Override
	public Double getHV90() {
		return (Double) get(HV90);
	}

	@Override
	public Double getHV125() {
		return (Double) get(HV125);
	}

	@Override
	public Double getHV254() {
		return (Double) get(HV254);
	}

	@Override
	public Double getMin60() {
		return (Double) get(MIN60);
	}

	@Override
	public Double getMin125() {
		return (Double) get(MIN125);
	}

	@Override
	public Double getMin254() {
		return (Double) get(MIN254);
	}

	@Override
	public Double getMax60() {
		return (Double) get(MAX60);
	}

	@Override
	public Double getMax125() {
		return (Double) get(MAX125);
	}

	@Override
	public Double getMax254() {
		return (Double) get(MAX254);
	}

	// public void setNote(String value_) {
	// _note = value_;
	// }

	// @Override
	// public String getNote() {
	// return _note;
	// }

	@Override
	public Double getHV(String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case "hv10":
			return getHV10();
		case "hv20":
			return getHV20();
		case "hv30":
			return getHV30();
		case "hv60":
			return getHV60();
		case "hv90":
			return getHV90();
		case "hv125":
			return getHV125();
		case "hv254":
			return getHV254();
		}
		return null;
	}

	@Override
	public Double getAvg(String mode_) {
		if (mode_ == null)
			return null;
		Object obj = get(mode_);
		return (obj != null && obj instanceof Double) ? (Double) obj : null;
	}

	@Override
	public Double getSma60() {
		return (Double) get(SMA60);
	}

	@Override
	public Double getSma125() {
		return (Double) get(SMA125);
	}

	@Override
	public Double getSma254() {
		return (Double) get(SMA254);
	}

	@Override
	public Double getIV() {
		return (Double) get(IV);
	}

	@Override
	public String getEarningsDS8() {
		return getEarningsDateStr();
	}

	@Override
	public String getDividendExDS8() {
		return getDividendExDateStr();
	}

	@Override
	public String getDividendRecordDS8() {
		return getDividendRecordDateStr();
	}

	@Override
	public String getDividendPayDS8() {
		return getDividendPayDateStr();
	}

	@Override
	public String getDividendEstimatedDS8() {
		return IATDividend.getDividendEstimatedDS8(this);
	}

	@Override
	public Date getDividendEstimatedDate() {
		return IATDividend.getDividendEstimatedDate(this);
	}

	@Override
	public Double getMin(String mode_) {
		Double value = IATEquityFoundation.getMin(this, mode_);
		return value;
	}

	@Override
	public Double getMax(String mode_) {
		Double value = IATEquityFoundation.getMax(this, mode_);
		return value;
	}

	@Override
	public Map<String, Double> getHvMap() {
		Object oMap = this.get(IATAssetConstants.HV_MAP);
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (oMap instanceof Map) ? (Map<String, Double>) oMap : null;
		return map;
	}

	public void setHvMap(Map<String, Double> value_) {
		if (value_ == null || value_.isEmpty()) {
			this.remove(IATAssetConstants.HV_MAP);
		} else {
			this.put(IATAssetConstants.HV_MAP, value_);
		}
	}

	@Override
	public Map<String, Double> getAvgMap() {
		Object oMap = this.get(IATAssetConstants.AVG_MAP);
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (oMap instanceof Map) ? (Map<String, Double>) oMap : null;
		return map;
	}

	public void setAvgMap(Map<String, Double> value_) {
		if (value_ == null || value_.isEmpty()) {
			this.remove(IATAssetConstants.AVG_MAP);
		} else {
			this.put(IATAssetConstants.AVG_MAP, value_);
		}
	}

	@Override
	public Map<String, Double> getMaxMap() {
		Object oMap = this.get(IATAssetConstants.MAX_MAP);
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (oMap instanceof Map) ? (Map<String, Double>) oMap : null;
		return map;
	}

	public void setMaxMap(Map<String, Double> value_) {
		if (value_ == null || value_.isEmpty()) {
			this.remove(IATAssetConstants.MAX_MAP);
		} else {
			this.put(IATAssetConstants.MAX_MAP, value_);
		}
	}

	@Override
	public Map<String, Double> getMinMap() {
		Object oMap = this.get(IATAssetConstants.MIN_MAP);
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (oMap instanceof Map) ? (Map<String, Double>) oMap : null;
		return map;
	}

	public void setMinMap(Map<String, Double> value_) {
		if (value_ == null || value_.isEmpty()) {
			this.remove(IATAssetConstants.MIN_MAP);
		} else {
			this.put(IATAssetConstants.MIN_MAP, value_);
		}
	}
}
