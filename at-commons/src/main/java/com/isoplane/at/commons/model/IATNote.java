package com.isoplane.at.commons.model;

public interface IATNote {

	String getAtId();

	String getNote();
	
	String getOccId();
	
	String getOwnerId();
}
