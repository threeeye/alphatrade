package com.isoplane.at.commons.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATOpportunityClip;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserNote;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.ATUserSetting;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.model.IATUserNote;

public interface IATUserProvider extends IATService {

	IATSession registerSession(IATSession session);

	IATSession verifySession(String sessionId, String clientIp, Long activityTs);

	boolean removeSession(String sessionId);

	boolean logoutUser(String userId);

	String getUserId(String sessionId);

	Set<String> getActiveUserIds();

	long getSessionTimeout();

	ATUser getUser(String userId);

	boolean updateUser(ATUser user);

	void removeMessageTokens(Set<String> failedTokens);

	List<ATUserAlertRulePack> getUserAlerts(String userId, String symbol);

	boolean saveUserAlerts(String userId, ATUserAlertRulePack pack);

	boolean deleteUserAlerts(String userId, String symbol);

	List<ATUserNote> getUserNotes(String userId, String symbol);

	boolean saveUserNote(String userId, IATUserNote note);

	// List<ATSecurityResearchConfig> getSecurityResearchConfigs(String userId, String type);

	// List<ATSecurityResearchConfig> saveSecurityResearchConfigs(String userId, String type, IATSecurityResearchConfig... configs);

	void register(IATSessionListener listener);

	void register(IATUserAlertListener listener);

	void register(IATUserNotesListener listener);

	// void register(IATSecurityResearchConfigListener listener);

	ATUserEquityModel saveUserEquityModel(ATUserEquityModel model);

	List<ATUserEquityModel> getUserEquityModels(String userId);

	Boolean deleteUserEquityModel(String userId, String atid);

	List<ATBroker> getUserBrokers(String userId);

	boolean saveUserBroker(String userId, ATBroker broker);

	List<ATUser> getUsers();

	Map<String, Object> importUserData(String userId, ATBroker[] brokers, ATUserNote[] notes, ATUserAlertRulePack[] alertPacks,
			ATUserEquityModel[] models,
			boolean isPurgeExisting);

	void logUserActivity(String ip, String userId, String activity, String data);

	ATUserSetting getUserSetting(String userId, String settingId);

	boolean saveUserSetting(ATUserSetting setting);

	boolean deleteWatchlist(String uid, String atid);

	ATWatchlist saveWatchlist(ATWatchlist data);

	List<ATWatchlist> getWatchlists(String uid);

	ATUserOpportunityConfig saveUserOpportunityConfig(ATUserOpportunityConfig config);

	List<ATUserOpportunityConfig> getUserOpportunityConfigs(String uid);

	Boolean deleteUserOpportunityConfig(String uid, String atid);

	List<ATOpportunityClip> updateOpportunityClips(String uid, Set<ATOpportunityClip> clips);

	List<ATOpportunityClip> getOpportunityClips(String uid, String symbol);

	// ATOpportunityConfig saveUserOpportunityConfig(ATOpportunityConfig config_);

}
