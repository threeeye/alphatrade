package com.isoplane.at.commons.util;

public class ATGenericDelayedRequest extends ATDelayedRequest {

	private IATIdentifyableRunnable _run;

	public ATGenericDelayedRequest(IATIdentifyableRunnable run_) {
		_run = run_;
	}

	@Override
	public void run() {
		_run.run();
	}

	@Override
	public Object getIdentifyableId() {
		return _run.getIdentification();
	}

}
