package com.isoplane.at.commons.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;

public class ATPriorityQueue<T> {

	private Map<Integer, LinkedTransferQueue<T>> _queues;
	private int _lasti = -1;
	private boolean _isQueueable;
	private long _totalCount = 0;
	// private long _cycleCount = 0;
	private long _pollCount = 0;

	public ATPriorityQueue() {
		_queues = new TreeMap<>();
	}

	public boolean add(T object_, int key_) {
		if (key_ <= 0) {
			throw new IllegalArgumentException(String.format("Key must be greater zero [%d]", key_));
		}
		LinkedTransferQueue<T> queue = _queues.get(key_);
		if (queue == null) {
			queue = new LinkedTransferQueue<>();
			_queues.put(key_, queue);
			_isQueueable = object_ instanceof IATQueueable;
		}
		if (queue.contains(object_))
			return false;
		boolean result = queue.add(object_);
		// _totalCount = _queues.values().stream().mapToInt(q_ -> q_.size()).sum();
		return result;
	}

	public boolean addAll(Collection<T> all_, int key_) {
		if (key_ <= 0) {
			throw new IllegalArgumentException(String.format("Key must be greater zero [%d]", key_));
		}
		LinkedTransferQueue<T> queue = _queues.get(key_);
		if (queue == null) {
			queue = new LinkedTransferQueue<>();
			_queues.put(key_, queue);
			if (all_.size() > 0) {
				_isQueueable = all_.iterator().next() instanceof IATQueueable;
			}
		}
		boolean result = queue.addAll(all_);
		// _totalCount = _queues.values().stream().mapToInt(q_ -> q_.size()).sum();
		return result;
	}

	public Collection<T> poll(int number_) {
		Set<T> result = new HashSet<>();
		_pollCount++;
		if (_queues.isEmpty())
			return result;
		int remaining = number_;
		Set<Integer> keys = new TreeSet<>(_queues.keySet());
		if (_lasti >= 0) {
			keys.removeIf(i -> i <= _lasti);
		}
		while (remaining > 0) {
			int count = pollCycle(result, keys, remaining);
			remaining -= count;
			if (remaining <= 0 || count == 0) {
				break;
			}
			keys = _queues.keySet();
		}
		_totalCount += result.size();
		return result;
	}

	private int pollCycle(Set<T> set_, Set<Integer> keys_, int remaining_) {
		int pollCount = 0;
		int remaining = remaining_;
		for (Integer i : keys_) {
			int pollSize = Math.min(remaining, i);
			LinkedTransferQueue<T> queue = _queues.get(i);
			int count = queue.drainTo(set_, pollSize);
			remaining -= count;
			pollCount += count;
			if (remaining <= 0)
				break;
			_lasti = i;
		}
		_lasti = -1;
		return pollCount;
	}

	public Map<String, Object> getStatus() {
		int totalSize = 0;
		Map<String, Object> statusMap = new TreeMap<>();
		for (Integer i : _queues.keySet()) {
			LinkedTransferQueue<T> queue = _queues.get(i);
			int size = queue.size();
			if (size == 0)
				continue;
			totalSize += size;
			statusMap.put(String.format("size.%02d", i), size);
			if (_isQueueable) {
				OptionalDouble avgD = queue.stream().mapToLong(t_ -> ((IATQueueable) t_).getLastProcessedTime()).filter(t_ -> t_ > 0).average();
				if (avgD.isPresent()) {
					long delta = System.currentTimeMillis() - (long) avgD.getAsDouble();
					long age = TimeUnit.SECONDS.convert(delta, TimeUnit.MILLISECONDS);
					statusMap.put(String.format("age.%02d", i), age);
				}
			}
		}
		statusMap.put("size.total", totalSize);
		statusMap.put("count.cycle", totalSize == 0 ? 0 : (long) (_totalCount / totalSize));
		statusMap.put("count.poll", _pollCount);
		@SuppressWarnings("unchecked")
		Map<String, Object> result = (Map<String, Object>) (Object) statusMap;
		return result;
	}

	public int size() {
		int totalSize = 0;
		for (Integer i : _queues.keySet()) {
			LinkedTransferQueue<T> queue = _queues.get(i);
			int size = queue.size();
			if (size == 0)
				continue;
			totalSize += size;
		}
		return totalSize;
	}

	public void clear() {
		if (_queues == null || _queues.isEmpty())
			return;
		for (LinkedTransferQueue<T> queue : _queues.values()) {
			queue.clear();
		}
	}

	public interface IATQueueable {
		Long getLastProcessedTime();
	}
}
