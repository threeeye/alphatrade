package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;

public class ATEquityDescription extends HashMap<String, Object> implements IATAsset, IATEquityDescription {

	private static final long serialVersionUID = 1L;

	public ATEquityDescription() {
	}
	
	public ATEquityDescription(String occId_, String name_, String dscr_, String sct_, String ind_) {
		setOccId(occId_);
		setName(name_);
		setDescription(dscr_);
		setSector(sct_);
		setIndustry(ind_);
	}

	public ATEquityDescription(IATEquityDescription tmpl_) {
		if (tmpl_ instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) tmpl_;
			this.setOccId(IATAsset.getString(map, IATAssetConstants.OCCID));
			this.setDescription(IATAsset.getString(map, IATAssetConstants.DESCRIPTION));
			this.setIndustry(IATAsset.getString(map, IATAssetConstants.INDUSTRY));
			this.setSector(IATAsset.getString(map, IATAssetConstants.SECTOR));
			this.setName(IATAsset.getString(map, IATAssetConstants.NAME));
		} else {
			this.setOccId(tmpl_.getOccId());
			this.setDescription(tmpl_.getDescription());
			this.setIndustry(tmpl_.getIndustry());
			this.setSector(tmpl_.getSector());
		}
	}

	@Override
	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	public String getName() {
		return IATAsset.getString(this, IATAssetConstants.NAME);
	}

	public void setName(String value_) {
		super.put(IATAssetConstants.NAME, value_);
	}

	@Override
	public String getDescription() {
		return IATAsset.getString(this, IATAssetConstants.DESCRIPTION);
	}

	public void setDescription(String value_) {
		super.put(IATAssetConstants.DESCRIPTION, value_);
	}

	@Override
	public String getIndustry() {
		return IATAsset.getString(this, IATAssetConstants.INDUSTRY);
	}

	public void setIndustry(String value_) {
		super.put(IATAssetConstants.INDUSTRY, value_);
	}

	@Override
	public String getSector() {
		return IATAsset.getString(this, IATAssetConstants.SECTOR);
	}

	public void setSector(String value_) {
		super.put(IATAssetConstants.SECTOR, value_);
	}

	@Override
	public String getAtId() {
		return this.getOccId();
	}

	@Override
	public String getLabel() {
		return this.getOccId();
	}

	@Override
	public String getSymbol() {
		return this.getOccId();
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	// super.put(IATAssetConstants.OCCID, id);
	// super.put(IATAssetConstants.MODEL, symbol_.getModel());
	// super.put(IATAssetConstants.NAME, symbol_.getName());
	// super.put(IATAssetConstants.DESCRIPTION, symbol_.getDescription());
	// super.put(IATAssetConstants.SECTOR, symbol_.getSector());
	// super.put(IATAssetConstants.INDUSTRY, symbol_.getIndustry());
}
