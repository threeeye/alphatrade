package com.isoplane.at.commons.model;

public enum EATMarketStatus {
	PRE, OPEN, POST, CLOSED;
}
