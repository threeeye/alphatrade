package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATTradeGroup extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public final String TRADES = "trades";

	public ATTradeGroup() {
	}

	public ATTradeGroup(String userId_, String account_, String symbol_) {
		this();
		this.setAccount(account_);
		this.setTradeUserId(userId_);
		this.setUnderlying(symbol_);
	}

	public ATTradeGroup(Map<String, Object> map_) {
		this();
		String id = IATAsset.getString(map_, IATLookupMap.AT_ID);
		this.setAtId(id);
		String account = IATAsset.getString(map_, IATTrade.TRADE_ACCOUNT);
		this.setAccount(account);
		String userId = IATAsset.getString(map_, IATTrade.TRADE_USER);
		this.setTradeUserId(userId);
		Double pxNet = IATAsset.getDouble(map_, IATTrade.PX_NET);
		this.setPxNet(pxNet);
		Double pxCum = IATAsset.getDouble(map_, IATTrade.PX_ACU);
		this.setPxCumulative(pxCum);
		Double riskAbs = IATAsset.getDouble(map_, IATTrade.RISK_ABS);
		this.setRiskAbs(riskAbs);
		Double riskLoss = IATAsset.getDouble(map_, IATTrade.RISK_LOSS);
		this.setRiskLoss(riskLoss);
		String strategy = IATAsset.getString(map_, IATTrade.STRATEGY);
		this.setStrategy(strategy);
		Double valMid = IATAsset.getDouble(map_, IATTrade.VAL_MID);
		this.setValueMid(valMid);
		Double yieldApy = IATAsset.getDouble(map_, IATTrade.YIELD_APY);
		this.setYieldApy(yieldApy);
		Double yieldPct = IATAsset.getDouble(map_, IATTrade.YIELD_PCT);
		this.setYieldPct(yieldPct);
		String ul = IATAsset.getString(map_, IATTrade.UNDERLYING);
		this.setUnderlying(ul);
		Long tsTrd = IATAsset.getLong(map_, IATTrade.TS_TRADE);
		this.setTsTrade(tsTrd);
		Long tsExp = IATAsset.getLong(map_, IATTrade.TS_EXP);
		this.setTsExp(tsExp);
		Object ratioObj = map_.get(IATTrade.RATIO);
		if (ratioObj != null && ratioObj instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Double> ratioTmpl = (Map<String, Double>) ratioObj;
			Map<String, Double> ratioMap = new TreeMap<>(ratioTmpl);
			this.setRatios(ratioMap);
		}
	}

	public static void removeNonPersistable(Map<String, Object> map_) {
		map_.remove(IATLookupMap.AT_ID);
		map_.remove(IATTrade.RISK_LOSS);
		map_.remove(IATTrade.VAL_MID);
		map_.remove(IATTrade.YIELD_APY);
		map_.remove(IATTrade.YIELD_PCT);
	}

	public String getAtId() {
		return IATAsset.getString(this, IATLookupMap.AT_ID);
	}

	public void setAtId(String value_) {
		IATAsset.setRemovableValue(this, IATLookupMap.AT_ID, value_);
	}

	public String getAccount() {
		return IATAsset.getString(this, IATTrade.TRADE_ACCOUNT);
	}

	public void setAccount(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.TRADE_ACCOUNT, value_);
	}

	public String getTradeUserId() {
		return IATAsset.getString(this, IATTrade.TRADE_USER);
	}

	public void setTradeUserId(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.TRADE_USER, value_);
	}

	/** Invested amount of current holding. Recognizing Dividends, Fees, Covered Calls, etc. Resets on position close. */
	public Double getPxNet() {
		return IATAsset.getDouble(this, IATTrade.PX_NET);
	}

	public void setPxNet(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.PX_NET, value_);
	}

	/**
	 * Invested amount over life of account - Mostly relevant to equity. In addition to 'actual' recognizes Spread yield on equity. Does not
	 * reset on position close.
	 */
	public Double getPxCumulative() {
		return IATAsset.getDouble(this, IATTrade.PX_ACU);
	}

	public void setPxCumulative(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.PX_ACU, value_);
	}

	public Double getRiskAbs() {
		return IATAsset.getDouble(this, IATTrade.RISK_ABS);
	}

	public void setRiskAbs(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.RISK_ABS, value_);
	}

	public Double getRiskLoss() {
		return IATAsset.getDouble(this, IATTrade.RISK_LOSS);
	}

	public void setRiskLoss(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.RISK_LOSS, value_);
	}

	public String getStrategy() {
		return IATAsset.getString(this, IATTrade.STRATEGY);
	}

	public void setStrategy(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.STRATEGY, value_);
	}

	public Double getValueMid() {
		return IATAsset.getDouble(this, IATTrade.VAL_MID);
	}

	public void setValueMid(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.VAL_MID, value_);
	}

	public Double getYieldApy() {
		return IATAsset.getDouble(this, IATTrade.YIELD_APY);
	}

	public void setYieldApy(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.YIELD_APY, value_);
	}

	public Double getYieldPct() {
		return IATAsset.getDouble(this, IATTrade.YIELD_PCT);
	}

	public void setYieldPct(Double value_) {
		IATAsset.setRemovableValue(this, IATTrade.YIELD_PCT, value_);
	}

	public String getUnderlying() {
		return IATAsset.getString(this, IATTrade.UNDERLYING);
	}

	public void setUnderlying(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.UNDERLYING, value_);
	}

			/** Averaged traded timestamp */
	public Long getTsTrade() {
		return IATAsset.getLong(this, IATTrade.TS_TRADE);
	}

	public void setTsTrade(Long value_) {
		IATAsset.setRemovableValue(this, IATTrade.TS_TRADE, value_);
	}

	public Long getTsExp() {
		return IATAsset.getLong(this, IATTrade.TS_EXP);
	}

	public void setTsExp(Long value_) {
		IATAsset.setRemovableValue(this, IATTrade.TS_EXP, value_);
	}

	public Map<String, Double> getRatios() {
		Object obj = this.get(IATTrade.RATIO);
		if (obj == null || !(obj instanceof Map))
			return null;
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) obj;
		return map;
	}

	public void setRatios(Map<String, Double> value_) {
		IATAsset.setRemovableValue(this, IATTrade.RATIO, value_);
	}

	// public List<ATTradeDigest> getTrades() {
	// Object trdTmpl = this.get(TRADES);
	// if (trdTmpl == null || !(trdTmpl instanceof List))
	// return null;
	// @SuppressWarnings("unchecked")
	// List<ATTradeDigest> trades = (List<ATTradeDigest>) trdTmpl;
	// return trades;
	// }
	//
	// public void setTrades(List<ATTradeDigest> value_) {
	// IATAsset.setRemovableValue(this, TRADES, value_);
	// }

}