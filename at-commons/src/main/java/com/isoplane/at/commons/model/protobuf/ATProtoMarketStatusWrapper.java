package com.isoplane.at.commons.model.protobuf;

import java.util.Date;

import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.protobuf.MarketStatusProtos.MarketStatus;
import com.isoplane.at.commons.model.protobuf.MarketStatusProtos.MarketStatus.Builder;
import com.isoplane.at.commons.util.EATChangeOperation;;

public class ATProtoMarketStatusWrapper {

	private MarketStatus _protoStatus;
	private EATMarketStatus _atStatus;

	public ATProtoMarketStatusWrapper(MarketStatus status_) {
		this._protoStatus = status_;
	}

	public MarketStatus proto() {
		return this._protoStatus;
	}

	public EATMarketStatus value() {
		if (this._atStatus == null) {
			this._atStatus = toMarketStatus(this._protoStatus);
		}
		return this._atStatus;
	}

	static public EATMarketStatus toMarketStatus(MarketStatus status_) {
		EATMarketStatus status = EATMarketStatus.valueOf(status_.getStatus());
		return status;
	}

	static public MarketStatus toMarketStatus(EATMarketStatus status_, Date date_) {
		return toMarketStatus(status_, date_, false);
	}

	static public MarketStatus toMarketStatus(EATMarketStatus status_, Date date_, boolean isTest_) {
		Builder builder = MarketStatus.newBuilder();
		builder.setActionCode(EATChangeOperation.UPDATE.name());
		builder.setStatus(status_.name());
		builder.setTimestamp(date_.getTime());
		if (isTest_) {
			builder.setIsTest(isTest_);
		}
		MarketStatus mkt = builder.build();
		return mkt;
	}

}
