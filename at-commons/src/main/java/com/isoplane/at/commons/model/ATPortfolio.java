package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ATPortfolio extends HashMap<String, Object> implements IATPortfolio {

	private static final long serialVersionUID = 1L;

	public static final String DETAILS = "details";
	public static final String ORIGINAL_ID = "_origid";

	static public ATPortfolio create(IATPortfolio tmpl_) {
		ATPortfolio trd = new ATPortfolio(tmpl_);
		return trd;
	}

	public ATPortfolio() {
	}

	public ATPortfolio(String userId_, String account_, String occId_) {
		String atid = String.format("%s:%s:%s", userId_, account_, occId_);
		setAtId(atid);
		setOccId(occId_);
		setTradeAccount(account_);
		setUserId(userId_);
		setTs(new Date());
		setPxAccumulated(0.0);
		setPxTradeTotal(0.0);
		setSz(0.0);
		if (occId_.length() < 10) {
			setUnderlying(occId_);
		} else {
			String ul = occId_.substring(0, 6).trim();
			setUnderlying(ul);
			String exDs6 = occId_.substring(6, 12);
			setExpDS6(exDs6);
		}
	}

	public ATPortfolio(IATPortfolio template_) {
		// Map<String, Object> map = template_.getAsMap();
		this.setAtId(template_.getAtId());
		this.setOccId(template_.getOccId());
		this.setUnderlying(template_.getUnderlying());
		this.setPxTradeTotal(template_.getPxTradeTotal());
		this.setPxAccumulated(template_.getPxAccumulated());
		this.setSz(template_.getSz());
		this.setTradeAccount(template_.getTradeAccount());
		this.setUserId(template_.getUserId());
		this.setTs(template_.getTs());
		this.setTsLong(template_.getTsLong());
		this.setExpDS6(template_.getExpDS6());
		// super.putAll(map);
	}

	public ATPortfolio prep4Save() {
		ATPortfolio trade = new ATPortfolio(this);
		String occId = trade.getOccId();
		if (occId.length() > 6) {
			String dateStr = occId.substring(6, 12);
			trade.put(IATAssetConstants.EXPIRATION_DATE_STR, dateStr);
		}
		return trade;
	}

	@Override
	public int compareTo(Object other_) {
		String otherId = other_ != null && (other_ instanceof ATPortfolio) ? ((ATPortfolio) other_).getAtId() : null;
		if (otherId == null)
			return -1;
		return this.getAtId().compareTo(otherId);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getOccId() {
		return IATPortfolio.getOccId(this);
	}

	public void setOccId(String value_) {
		IATPortfolio.setOccId(this, value_);
	}

	@Override
	public String getUnderlying() {
		return IATPortfolio.getUnderlying(this);
	}

	public void setUnderlying(String value_) {
		IATPortfolio.setUnderlying(this, value_);
	}

	@Override
	public String getUserId() {
		return IATPortfolio.getUserId(this);
	}

	public void setUserId(String value_) {
		IATPortfolio.setUserId(this, value_);
	}

	@Override
	public Date getTs() {
		return IATPortfolio.getTs(this);
	}

	public void setTs(Date value_) {
		IATPortfolio.setTs(this, value_);
	}

	@Override
	public Long getTsLong() {
		return IATPortfolio.getTsLong(this);
	}

	public void setTsLong(Long value_) {
		IATPortfolio.setTsLong(this, value_);
	}

	@Override
	public String getExpDS6() {
		return IATPortfolio.getExpDS6(this);
	}

	public void setExpDS6(String value_) {
		IATPortfolio.setExpDS6(this, value_);
	}

	@Override
	public Double getSz() {
		return IATPortfolio.getSz(this);
	}

	public void setSz(Double value_) {
		IATPortfolio.setSz(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATPortfolio.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATPortfolio.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxAccumulated() {
		return IATPortfolio.getPxAccumulated(this);
	}

	public void setPxAccumulated(Double value_) {
		IATPortfolio.setPxAccumulated(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATPortfolio.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		IATPortfolio.setTradeAccount(this, value_);
	}

	@Override
	public String toString() {
		return IATPortfolio.toString(this);
	}

}
