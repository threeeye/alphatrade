package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ATDividend extends HashMap<String, Object> implements IATDividend, Comparable<IATDividend> {

	private static final long serialVersionUID = 1L;

	public ATDividend() {
	}

	public ATDividend(IATDividend template_) {
		if (template_ != null) {
			setOccId(template_.getOccId());
			setDividend(template_.getDividend());
			setDividendFrequency(template_.getDividendFrequency());
			setDividendEstimatedDS8(template_.getDividendEstimatedDS8());
			setDividendExDateStr(template_.getDividendExDateStr());
			setDividendPayDateStr(template_.getDividendPayDateStr());
			setDividendRecordDateStr(template_.getDividendRecordDateStr());
		}
	}

	@Override
	public int compareTo(IATDividend other_) {
		return IATDividend.compareTo(this, other_);
	}

	@Override
	public String toString() {
		return IATDividend.toString(this);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return new HashMap<>(this);
	}

	@Override
	public String getOccId() {
		return IATDividend.getOccId(this);
	}

	public void setOccId(String value_) {
		IATDividend.setOccId(this, value_);
	}

	@Override
	public String getDividendExDateStr() {
		return IATDividend.getDividendExDateStr(this);
	}

	public void setDividendExDateStr(String value_) {
		IATDividend.setDividendExDateStr(this, value_);
	}

	@Override
	public Date getDividendExDate() {
		return IATDividend.getDividendExDate(this);
	}

	@Override
	public String getDividendRecordDateStr() {
		return IATDividend.getDividendRecordDateStr(this);
	}

	public void setDividendRecordDateStr(String value_) {
		IATDividend.setDividendRecordDateStr(this, value_);
	}

	@Override
	public Date getDividendRecordDate() {
		return IATDividend.getDividendRecordDate(this);
	}

	@Override
	public String getDividendPayDateStr() {
		return IATDividend.getDividendPayDateStr(this);
	}

	public void setDividendPayDateStr(String value_) {
		IATDividend.setDividendPayDateStr(this, value_);
	}

	@Override
	public Date getDividendPayDate() {
		return IATDividend.getDividendPayDate(this);
	}

	@Override
	public Double getDividend() {
		return IATDividend.getDividend(this);
	}

	public void setDividend(Double value_) {
		IATDividend.setDividend(this, value_);
	}

	@Override
	public Integer getDividendFrequency() {
		return IATDividend.getDividendFrequency(this);
	}

	public void setDividendFrequency(Integer value_) {
		IATDividend.setDividendFrequency(this, value_);
	}

	@Override
	public String getDividendEstimatedDS8() {
		return IATDividend.getDividendEstimatedDS8(this);
	}

	public void setDividendEstimatedDS8(String value_) {
		IATDividend.setDividendEstimatedDS8(this, value_);
	}

	@Override
	public Date getDividendEstimatedDate() {
		return IATDividend.getDividendEstimatedDate(this);
	}

}
