package com.isoplane.at.commons.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATDelayedRequestRunner2 {

	static final Logger log = LoggerFactory.getLogger(ATDelayedRequestRunner2.class);

	private boolean _isRejectingDuplicates;
	private ScheduledThreadPoolExecutor _executor;
	private ScheduledThreadPoolExecutor _manager;
	private Map<Object, ATCallable> _workerMap;
	// private Set<Object> _workerIds;
	private int _maxQueueSize = 1000;
	private boolean _isQueueSizeExceeded;
	private long _reqDelay;
	private long _runningId;
	private long _timeoutTimestamp = -1;
	private int _timeoutCount = 0;
	private int _timeoutMaxCount = 10;
	private long _timeoutMaxDuration = 60000;

	public ATDelayedRequestRunner2(int poolSize_, long reqDelay_, boolean isRejectingDuplicates_) {
		_isRejectingDuplicates = isRejectingDuplicates_;
		_reqDelay = reqDelay_;
		_isQueueSizeExceeded = false;

		// _workerIds = Collections.synchronizedSet(new HashSet<>());
		_workerMap = Collections.synchronizedMap(new HashMap<>());

		_executor = new ScheduledThreadPoolExecutor(poolSize_);
		_executor.setRemoveOnCancelPolicy(true);
		_executor.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
		_executor.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
		_executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

			@Override
			public void rejectedExecution(Runnable r_, ThreadPoolExecutor executor_) {
				log.info(String.format("Executor rejected [%s]", r_));
			}
		});

		_manager = new ScheduledThreadPoolExecutor(poolSize_);
		_manager.setRemoveOnCancelPolicy(true);
		_manager.setContinueExistingPeriodicTasksAfterShutdownPolicy(false);
		_manager.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
		_manager.setRejectedExecutionHandler(new RejectedExecutionHandler() {

			@Override
			public void rejectedExecution(Runnable r_, ThreadPoolExecutor executor_) {
				log.info(String.format("Manager rejected [%s]", r_));
			}
		});
	}

	public void shutdown() {
		_executor.shutdownNow();
		_manager.shutdownNow();
	}

	public long size() {
		long count = _executor.getTaskCount() - _executor.getCompletedTaskCount();
		return count;
	}

	public void setRequestDelay(long reqDelay_) {
		if (_reqDelay == reqDelay_)
			return;
		log.info(String.format("Request delay changed [%d -> %d]", _reqDelay, reqDelay_));
		_reqDelay = reqDelay_;
	}

	public long getRequestDelay() {
		return _reqDelay;
	}

	public void setTimeoutMaxCount(int count_) {
		if (_timeoutMaxCount == count_)
			return;
		log.info(String.format("Timeout max count changed [%d -> %d]", _timeoutCount, count_));
		_timeoutMaxCount = count_;
	}

	public int getTimeoutMaxCount() {
		return _timeoutMaxCount;
	}

	public void setTimeoutMaxDuration(long duration_) {
		if (_timeoutMaxDuration == duration_)
			return;
		log.info(String.format("Timeout max duration changed [%d -> %d]", _timeoutMaxDuration, duration_));
		_timeoutMaxDuration = duration_;
	}

	public long getTimeoutMaxDuration() {
		return _timeoutMaxDuration;
	}

	synchronized public boolean add(ATCallable req_) {
		try {
			String id = _isRejectingDuplicates ? req_.getId() : String.format("%s_%d", req_.getId(), _runningId++);
			// TODO: Should we update ID in callable?
			if (_isRejectingDuplicates && _workerMap.containsKey(id) /* _workerIds.contains(id) */) {
				log.debug(String.format("[%-10s] Rejecting duplicate", id));
				return false;
			}
			if (_isQueueSizeExceeded && req_.isCancellable()) {
				int queueSize = _executor.getQueue().size();
				if (queueSize > _maxQueueSize / 2) {
					Thread.sleep(_reqDelay);
					return false;
				}
				log.info(String.format("Queue size reset [%d]", queueSize));
				_isQueueSizeExceeded = false;
			}
			// _workerIds.add(id);
			_workerMap.put(id, req_); // TODO: What if accepting duplicates?
			req_.setScheduleTime(System.currentTimeMillis());
			Future<Object> f = req_.isCancellable() ? _executor.schedule(req_, req_.getDelay(), TimeUnit.MILLISECONDS) : _executor.submit(req_);
			ATCallableFuture cf = new ATCallableFuture(id, f, req_.getTimeout(), req_.isCancellable());
			_manager.execute(cf);
			Thread.sleep(_reqDelay);
			return true;
		} catch (Exception ex) {
			log.error(String.format("Error adding [%s]", req_));
			return false;
		}
	}

	class ATCallableFuture implements Runnable {

		private String _id;
		private Future<Object> _future;
		// private ScheduledFuture<Object> _future2;
		private long _timeoutMs;
		private boolean _isCancellable;

		private ATCallableFuture(String id_, Future<Object> future_, long timeoutMs_, boolean isCancellable_) {
			_id = id_;
			_future = future_;
			_timeoutMs = timeoutMs_;
			_isCancellable = isCancellable_;
		}

		@Override
		public void run() {
			try {
				Object o = _isCancellable ? _future.get(_timeoutMs, TimeUnit.MILLISECONDS) : _future.get(30, TimeUnit.MINUTES);
				log.debug(String.format("[%-16s] Done", o));
			} catch (TimeoutException toex) {
				ATDelayedRequestRunner2 _this_ = ATDelayedRequestRunner2.this;
				int queueSize = _executor.getQueue().size();
				log.error(String.format("Timeout [queue:%4d/%d, %-16s]", queueSize, _this_._maxQueueSize, _id));
				if (System.currentTimeMillis() - _this_._timeoutTimestamp > _timeoutMaxDuration) {
					_this_._timeoutTimestamp = System.currentTimeMillis();
					_this_._timeoutCount = 0;
				}
				_this_._timeoutCount++;
				if (_this_._timeoutCount >= _timeoutMaxCount) {
					double duration = (System.currentTimeMillis() - _this_._timeoutTimestamp) / 1000.0;
					log.error(String.format("Excessive timeouts [%.2f/s, delay: %d]", _this_._timeoutCount / duration, _reqDelay));
					_this_._timeoutCount = 0;
					_this_._timeoutTimestamp = System.currentTimeMillis();
				}
				if (queueSize > _this_._maxQueueSize) {
					ATDelayedRequestRunner2.this._isQueueSizeExceeded = true;
					log.warn(String.format("Queue size exceeded [%d/%d, delay: %d]. Consider throttling.", queueSize, _this_._maxQueueSize,
							_reqDelay));
				}
			} catch (Exception ex) {
				String error = String.format("[%-16s] Error", _id);
				if ((Throwable) ex instanceof OutOfMemoryError) {
					long heapSize = Runtime.getRuntime().totalMemory();
					long heapMaxSize = Runtime.getRuntime().maxMemory();
					long heapFreeSize = Runtime.getRuntime().freeMemory();
					error = String.format("%s [heap: %d; max: %d; free: %d]", error, heapSize, heapMaxSize, heapFreeSize);
				}
				log.error(error, ex);
				ATSysLogger.error(error, ex);
				_future.cancel(true);
			} finally {
				ATCallable call = ATDelayedRequestRunner2.this._workerMap.remove(_id);
				call.purge();
			}
		}

		// public boolean isCancellable() {
		// return _isCancellable;
		// }

	}

}
