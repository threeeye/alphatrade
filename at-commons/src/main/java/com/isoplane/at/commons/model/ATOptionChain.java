package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.Set;

import com.isoplane.at.commons.util.ATFormats;

public class ATOptionChain implements Comparable<ATOptionChain> {

	private String symbol;
	private Date expiration;
	private Set<Double> strikes;
	// TODO: Meta data about multiplicity outliers

	public ATOptionChain() {
	}

	public ATOptionChain(String symbol_, Date expiration_, Set<Double> strikes_) {
		this.symbol = symbol_;
		this.expiration = expiration_;
		this.strikes = strikes_;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public Set<Double> getStrikes() {
		return strikes;
	}

	public void setStrikes(Set<Double> strikes) {
		this.strikes = strikes;
	}

	@Override
	public int compareTo(ATOptionChain other_) {
		if (other_ == null)
			return -1;
		int result = this.getSymbol().compareTo(other_.getSymbol());
		if (result != 0)
			return result;
		result = this.getExpiration().compareTo(other_.getExpiration());
		if (result != 0)
			return result;
		return Integer.compare(this.getStrikes().size(), other_.getStrikes().size());
	}
	
	@Override
	public String toString() {
		String str = String.format("%-5s%s (%d)", getSymbol(), ATFormats.DATE_yyyy_MM_dd.get().format(getExpiration()), getStrikes().size());
		return str;
	}
}
