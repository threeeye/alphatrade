package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;

public interface IATUserAlertListener {

	void notify(ATUserAlertRulePack alertPack);

}
