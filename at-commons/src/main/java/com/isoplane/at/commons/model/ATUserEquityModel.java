package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATUserEquityModel extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	static public final String MODE_EQUITY = "E";
	static public final String MODE_OPTIONS = "O";

	public ATUserEquityModel() {
	}

	public ATUserEquityModel(Map<String, Object> other_) {
		this.setFilters(ATUserEquityModel.getFilters(other_));
		this.setId(ATUserEquityModel.getId(other_));
		this.setLabel(ATUserEquityModel.getLabel(other_));
		this.setMode(ATUserEquityModel.getMode(other_));
		this.setUserId(ATUserEquityModel.getUserId(other_));
	}

	public String getId() {
		return ATUserEquityModel.getId(this);
	}

	static public String getId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATLookupMap.AT_ID);
	}

	public void setId(String value) {
		if (value == null)
			this.remove(IATLookupMap.AT_ID);
		else
			this.put(IATLookupMap.AT_ID, value);
	}

	public String getUserId() {
		return ATUserEquityModel.getUserId(this);
	}

	static public String getUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value) {
		if (value == null)
			this.remove(IATAssetConstants.USER_ID);
		else
			this.put(IATAssetConstants.USER_ID, value);
	}

	public String getLabel() {
		return ATUserEquityModel.getLabel(this);
	}

	static public String getLabel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LABEL);
	}

	public void setLabel(String value) {
		if (value == null)
			this.remove(IATAssetConstants.LABEL);
		else
			this.put(IATAssetConstants.LABEL, value);
	}

	public String getMode() {
		return ATUserEquityModel.getMode(this);
	}

	static public String getMode(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODE);
	}

	public void setMode(String value) {
		if (value == null)
			this.remove(IATAssetConstants.MODE);
		else
			this.put(IATAssetConstants.MODE, value);
	}

	public ArrayList<ATModelFilter> getFilters() {
		return ATUserEquityModel.getFilters(this);
	}

	static public ArrayList<ATModelFilter> getEquityAbsoluteFilters(ArrayList<ATModelFilter> filters_) {
		if (filters_ == null || filters_.isEmpty())
			return null;
		ArrayList<ATModelFilter> filters = new ArrayList<>(filters_);
		Iterator<ATModelFilter> i = filters.iterator();
		while (i.hasNext()) {
			String type = i.next().getType();
			if (type != null && ATModelFilter.TYPE_PERCENTILE.equals(type)) {
				i.remove();
			}
		}
		return filters.isEmpty() ? null : filters;
	}

	public ArrayList<ATModelFilter> getEquityAbsoluteFilters() {
		ArrayList<ATModelFilter> filters = getFilters();
		return getEquityAbsoluteFilters(filters);
	}

	static public ArrayList<ATModelFilter> getEquityPercentileFilters(ArrayList<ATModelFilter> filters_) {
		if (filters_ == null || filters_.isEmpty())
			return null;
		ArrayList<ATModelFilter> filters = new ArrayList<>(filters_);
		Iterator<ATModelFilter> i = filters.iterator();
		while (i.hasNext()) {
			String asset = i.next().getAsset();
			String type = i.next().getType();
			if (type == null || !ATModelFilter.TYPE_PERCENTILE.equals(type) || ATModelFilter.ASSET_OPTION.equals(asset)) {
				i.remove();
			}
		}
		return filters.isEmpty() ? null : filters;
	}

	public ArrayList<ATModelFilter> getEquityPercentileFilters() {
		ArrayList<ATModelFilter> filters = getFilters();
		return getEquityPercentileFilters(filters);
	}

	static public ArrayList<ATModelFilter> getOptionFilters(ArrayList<ATModelFilter> filters_) {
		if (filters_ == null || filters_.isEmpty())
			return null;
		ArrayList<ATModelFilter> filters = new ArrayList<>(filters_);
		Iterator<ATModelFilter> i = filters.iterator();
		while (i.hasNext()) {
			String asset = i.next().getAsset();
			if (!ATModelFilter.ASSET_OPTION.equals(asset)) {
				i.remove();
			}
		}
		return filters.isEmpty() ? null : filters;
	}

	public ArrayList<ATModelFilter> getOptionFilters() {
		ArrayList<ATModelFilter> filters = getFilters();
		return getOptionFilters(filters);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static public ArrayList<ATModelFilter> getFilters(Map<String, Object> map_) {
		ArrayList<Map> f = (ArrayList<Map>) map_.get("filters");
		if (f == null || f.isEmpty())
			return null;
		boolean isFilter = f.get(0) instanceof ATModelFilter;
		if (isFilter) {
			return (ArrayList<ATModelFilter>) (Object) f;
		} else {
			ArrayList<ATModelFilter> filters = new ArrayList<>();
			for (Map filter : f) {
				filters.add(new ATModelFilter(filter));
			}
			map_.put("filters", filters);
			return filters;
		}
	}

	public void setFilters(ArrayList<ATModelFilter> value) {
		if (value == null || value.isEmpty())
			this.remove("filters");
		else {
			// ArrayList<ATModelFilter> filters = new ArrayList<>();
			// for (ATModelFilter filter : value) {
			// filters.add(new ATModelFilter(filter));
			// }
			this.put("filters", value);
		}
	}

	public static enum EATPriceReference {
		px_cb, px_strk;

	}

	public static class ATModelFilter extends HashMap<String, Object> {

		private static final long serialVersionUID = 1L;

		static public final String KEY_ASSET = "asset";
		static public final String KEY_MAX = "max";
		static public final String KEY_MIN = "min";
		static public final String KEY_TYPE = "type";
		static public final String KEY_VALUE = "value";

		static public final String ASSET_OPTION = "O";
		static public final String ASSET_STOCK = "S";

		static public final String DIVIDEND_YIELD = "div_yld";
		static public final String EXP_DAYS = "exp_d";
		static public final String INDUSTRY = "industry";
		static public final String MCAP = "mcap";
		static public final String HV = "hv";
		static public final String HV_60 = "hv60";
		static public final String HV_125 = "hv125";
		static public final String HV_254 = "hv254";
		static public final String IV = "iv";
		static public final String TRAILING_PE = IATAssetConstants.TRAILING_PE;
		static public final String PE_GROWTH = IATAssetConstants.PE_GROWTH_RATIO;
		static public final String PX = "px";
		// static public final String PX_CB = "px_cb";
		static public final String PX_MAX_60 = "max60";
		static public final String PX_MAX_125 = "max125";
		static public final String PX_MAX_254 = "max254";
		static public final String PX_MIN_60 = "min60";
		static public final String PX_MIN_125 = "min125";
		static public final String PX_MIN_254 = "min254";
		static public final String PX_CHANGE = "px_chng";
		static public final String PX_REFERENCE = "px_ref";
		
		// static public final String PX_STRIKE = "px_strk";

		static public final String REV_GROWTH_3Y = IATAssetConstants.REV_GROWTH_AVG_3Y;
		static public final String REV_MLT = IATAssetConstants.REV_MLT;
		static public final String SECTOR = "sector";
		static public final String SMA_60 = "sma60";
		static public final String SMA_125 = "sma125";
		static public final String SMA_254 = "sma254";
		static public final String SPREAD_PX = "sprd_px";
		static public final String SPREAD_STEPS = "sprd_stp";
		static public final String STRATEGY_OPT = "strat_opt";
		static public final String STRATEGY_OPT_CC = "CC";
		static public final String STRATEGY_OPT_SVCS = "SVCS";
		static public final String STRATEGY_OPT_SVPS = "SVPS";
		static public final String STRATEGY_YLD = "strat_yld";
		static public final String STRATEGY_YLD_H2L = "h2l"; // Hi 2 Lo: Start at max yield; Return 1st match (highest risk)
		static public final String STRATEGY_YLD_L2H = "l2h"; // Lo 2 Hi: Start at min yield; Return 1st match (lowest risk)
		static public final String STRATEGY_YLD_DEEP = "deep"; // Analyze all yields and pick optimal
		static public final String SYMBOLS = "symbols";
		static public final String WATCHLIST = "watchlist";

		static public final String TYPE_ABSOLUTE = "abs";
		static public final String TYPE_PERCENTILE = "pct";
		
		static public final String YIELD_APY = "yld_apy";
		// static public final String TYPE_OPTION_PRE = "o_";
		// static public final String TYPE_OPTION_ABSOLUTE = "o_abs";

		transient private Set<String> _symbols;

		public ATModelFilter() {
			this.setType(TYPE_ABSOLUTE);
		}

		public ATModelFilter(Map<String, Object> other_) {
			this();
			this.setId(ATModelFilter.getId(other_));
			this.setAsset(ATModelFilter.getAsset(other_));
			this.setMax(ATModelFilter.getMax(other_));
			this.setMin(ATModelFilter.getMin(other_));
			this.setValue(ATModelFilter.getValue(other_));
			String type = ATModelFilter.getType(other_);
			this.setType(StringUtils.isBlank(type) ? TYPE_ABSOLUTE : type);
		}

		public String getId() {
			return ATModelFilter.getId(this);
		}

		static public String getId(Map<String, Object> map_) {
			return IATAsset.getString(map_, IATLookupMap.AT_ID);
		}

		public void setId(String value) {
			if (value == null)
				this.remove(IATLookupMap.AT_ID);
			else
				this.put(IATLookupMap.AT_ID, value);
		}

		public String getAsset() {
			return ATModelFilter.getAsset(this);
		}

		static public String getAsset(Map<String, Object> map_) {
			return IATAsset.getString(map_, KEY_ASSET);
		}

		public void setAsset(String value) {
			if (StringUtils.isBlank(value))
				this.remove(KEY_ASSET);
			else
				this.put(KEY_ASSET, value);
		}

		public String getType() {
			return ATModelFilter.getType(this);
		}

		static public String getType(Map<String, Object> map_) {
			return IATAsset.getString(map_, KEY_TYPE);
		}

		public void setType(String value) {
			if (StringUtils.isBlank(value))
				this.remove(KEY_TYPE);
			else
				this.put(KEY_TYPE, value);
		}

		public Double getMin() {
			return ATModelFilter.getMin(this);
		}

		static public Double getMin(Map<String, Object> map_) {
			return IATAsset.getDouble(map_, KEY_MIN);
		}

		public void setMin(Double value) {
			if (value == null)
				this.remove(KEY_MIN);
			else
				this.put(KEY_MIN, value);
		}

		public Double getMax() {
			return ATModelFilter.getMax(this);
		}

		static public Double getMax(Map<String, Object> map_) {
			return IATAsset.getDouble(map_, KEY_MAX);
		}

		public void setMax(Double value) {
			if (value == null)
				this.remove(KEY_MAX);
			else
				this.put(KEY_MAX, value);
		}

		public Object getValue() {
			Object value = ATModelFilter.getValue(this);
			if (SYMBOLS.equals(this.getId())) {
				if (_symbols == null) {
					String symStr = (String) value;
					_symbols = StringUtils.isBlank(symStr) ? new HashSet<>()
							: Arrays.stream(symStr.split(",")).map(String::trim).collect(Collectors.toSet());
				}
				return _symbols;
			} else {
				return value;
			}
		}

		static public Object getValue(Map<String, Object> map_) {
			return map_.get(KEY_VALUE);
		}

		public void setValue(Object value) {
			if (value == null)
				this.remove(KEY_VALUE);
			else
				this.put(KEY_VALUE, value);
		}
	}
}
