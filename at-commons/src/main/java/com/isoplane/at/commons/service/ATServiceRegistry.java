package com.isoplane.at.commons.service;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.IATEvent;
import com.isoplane.at.commons.util.IATEventSubscriber;

public class ATServiceRegistry {

	static final Logger log = LoggerFactory.getLogger(ATServiceRegistry.class);

	static private IATFundamentalsProvider _fundamentalsProvider;
	static private IATMarketDataService _marketDataService;
	static private IATMarketStatusService _statusService;
	static private IATPortfolioService _portfolioService;
	private static IATSymbolProvider _symbolProvider;
	static private IATUserProvider _userService;
	static private IATWebSocketService _wsService;

	static protected Set<IATService> _refreshServices = new HashSet<>();

	static private Set<IATEventSubscriber> _eventSubscribers = new HashSet<>();

	public static void init() {
		_eventSubscribers.clear();
	}

	public static void subscribeEvents(IATEventSubscriber subscriber_) {
		if (subscriber_ != null) {
			_eventSubscribers.add(subscriber_);
		}
	}

	public static void notifyEventSubscribers(IATEvent event_) {
		for (IATEventSubscriber sub : _eventSubscribers) {
			try {
				sub.notify(event_);
			} catch (Exception ex_) {
				log.error(String.format("Error notifying [%s]", sub), ex_);
			}
		}
	}

	public static IATFundamentalsProvider getFundamentalsProvider() {
		if (_fundamentalsProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATFundamentalsProvider.class.getSimpleName()));
		}
		return _fundamentalsProvider;
	}

	public static void setFundamentalsProvider(IATFundamentalsProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_fundamentalsProvider = provider_;
		log.info(String.format("Registering fundamentals provider [%s]", _fundamentalsProvider.getClass().getSimpleName()));
	}

	final public static IATSymbolProvider getSymbolProvider() {
		if (_symbolProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATSymbolProvider.class.getSimpleName()));
		}
		return _symbolProvider;
	}

	final public static void setSymbolProvider(IATSymbolProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_symbolProvider = provider_;
		log.info(String.format("Registering symbol provider [%s]", _symbolProvider.getClass().getSimpleName()));
	}

	final public static void setMarketDataService(IATMarketDataService service_, boolean isRefresh_) {
		initService(service_, isRefresh_);
		_marketDataService = service_;
		log.info(String.format("Registering market data service [%s]", service_.getClass().getSimpleName()));
	}

	final public static IATMarketDataService getMarketDataService() {
		if (_marketDataService == null) {
			throw new ATException(String.format("Missing [%s]", IATMarketDataService.class.getSimpleName()));
		}
		return _marketDataService;
	}

	final public static void setPortfolioService(IATPortfolioService service_, boolean isRefresh_) {
		initService(service_, isRefresh_);
		_portfolioService = service_;
		log.info(String.format("Registering portfolio service [%s]", service_.getClass().getSimpleName()));
	}

	final public static IATPortfolioService getPortfolioService() {
		if (_portfolioService == null) {
			throw new ATException(String.format("Missing [%s]", IATPortfolioService.class.getSimpleName()));
		}
		return _portfolioService;
	}

	final public static void setWebSocketService(IATWebSocketService service_, boolean isRefresh_) {
		initService((IATService) service_, isRefresh_);
		_wsService = service_;
		log.info(String.format("Registering WebSocket service [%s]", service_.getClass().getSimpleName()));
	}

	final public static IATWebSocketService getWebSocketService() {
		if (_wsService == null) {
			throw new ATException(String.format("Missing [%s]", IATWebSocketService.class.getSimpleName()));
		}
		return _wsService;
	}

	final public static void setStatusService(IATMarketStatusService service_, boolean isRefresh_) {
		initService(service_, isRefresh_);
		_statusService = service_;
		log.info(String.format("Registering status service [%s]", service_.getClass().getSimpleName()));
	}

	final public static IATMarketStatusService getStatusService() {
		if (_statusService == null) {
			throw new ATException(String.format("Missing [%s]", IATMarketStatusService.class.getSimpleName()));
		}
		return _statusService;
	}

	final public static void setUserService(IATUserProvider service_, boolean isRefresh_) {
		initService(service_, isRefresh_);
		_userService = service_;
		log.info(String.format("Registering user service [%s]", service_.getClass().getSimpleName()));
	}

	final public static IATUserProvider getUserService() {
		if (_userService == null) {
			throw new ATException(String.format("Missing [%s]", IATUserProvider.class.getSimpleName()));
		}
		return _userService;
	}

	static protected void initService(IATService svc_, boolean isRefresh_) {
		svc_.initService();
		_refreshServices.remove(svc_);
		if (isRefresh_) {
			_refreshServices.add(svc_);
		}
	}

}
