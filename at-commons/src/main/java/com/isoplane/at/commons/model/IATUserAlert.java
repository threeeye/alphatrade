package com.isoplane.at.commons.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATAlertRule.ATAlertRuleComponent;

public interface IATUserAlert {

	static final String PROP_DATE = "DATE";
	static final String PROP_PX = "PX";

	static final String GROUP_A = "A";
	static final String GROUP_B = "B";

	static final String GROUP_LOGIC_A_AND = "A_AND";
	static final String GROUP_LOGIC_A_OR = "A_OR";
	static final String GROUP_LOGIC_A_AND_OR_B_AND = "A_AND_OR_B_AND";
	static final String GROUP_LOGIC_A_OR_AND_B_OR = "A_OR_AND_B_OR";

	static final String OPERATOR_EQ = "EQ";
	static final String OPERATOR_GT = "GT";
	static final String OPERATOR_GTE = "GTE";
	static final String OPERATOR_LT = "LT";
	static final String OPERATOR_LTE = "LTE";

	static final String ALERT_GROUP_LOGIC = "groupLogic";

	static final String ALERT_IS_ACTIVE = "act";
	static final String ALERT_PROPERTY = "prp";
	static final String ALERT_OPERATOR = "opr";
	static final String ALERT_VALUE = "val";
	static final String ALERT_GROUP = "grp";
	static final String ALERT_MESSAGE = "msg";
	static final String ALERT_TITLE = "ttl";
	static final String ALERT_TYPE = "typ";
	static final String ALERT_COMPONENTS = "cmp";

	static final List<String> ALERT_PROPS = Arrays.asList(
			IATAssetConstants.OCCID,
			ALERT_COMPONENTS,
			ALERT_GROUP,
			ALERT_IS_ACTIVE,
			ALERT_MESSAGE,
			ALERT_OPERATOR,
			ALERT_PROPERTY,
			ALERT_TITLE,
			ALERT_TYPE,
			ALERT_VALUE);

//	@Deprecated // In alert pack
//	String getUserId();

	String getOccId();

	String getProperty();

	String getOperator();

	String getValue();

	String getGroup();

	String getMessage();

	String getTitle();

	String getType();

	List<ATAlertRuleComponent> getComponents(); // NOTE: Not good referencing this class in IF

	Boolean isActive();

	@Deprecated // In alert pack
	static String getUserId(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.USER_ID);
	}

	@Deprecated // In alert pack
	static void setUserId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.USER_ID, value_);
	}

	static String getOccId(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static String getProperty(Map<String, Object> map_) {
		return (String) map_.get(ALERT_PROPERTY);
	}

	static void setProperty(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_PROPERTY, value_);
	}

	static String getOperator(Map<String, Object> map_) {
		return (String) map_.get(ALERT_OPERATOR);
	}

	static void setOperator(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_OPERATOR, value_);
	}

	static String getMessage(Map<String, Object> map_) {
		return (String) map_.get(ALERT_MESSAGE);
	}

	static void setMessage(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_MESSAGE, value_);
	}

	static String getValue(Map<String, Object> map_) {
		return (String) map_.get(ALERT_VALUE);
	}

	static void setValue(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_VALUE, value_);
	}

	static String getGroup(Map<String, Object> map_) {
		return (String) map_.get(ALERT_GROUP);
	}

	static void setGroup(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_GROUP, value_);
	}

	static String getTitle(Map<String, Object> map_) {
		return (String) map_.get(ALERT_TITLE);
	}

	static void setTitle(Map<String, Object> map_, String value_) {
		String title = StringUtils.isBlank(value_) ? null : value_;
		setMapValue(map_, ALERT_TITLE, title);
	}

	static String getType(Map<String, Object> map_) {
		return (String) map_.get(ALERT_TYPE);
	}

	static void setType(Map<String, Object> map_, String value_) {
		setMapValue(map_, ALERT_TYPE, value_);
	}

	@SuppressWarnings("unchecked")
	static List<ATAlertRuleComponent> getComponents(Map<String, Object> map_) {
		Object value = map_.get(ALERT_COMPONENTS);
		return (List<ATAlertRuleComponent>) value;
	}

	static void setComponents(Map<String, Object> map_, List<ATAlertRuleComponent> value_) {
		setMapValue(map_, ALERT_COMPONENTS, value_);
	}

	static Boolean isActive(Map<String, Object> map_) {
		Boolean val = (Boolean) map_.get(ALERT_IS_ACTIVE);
		return val != null ? val : false;
	}

	static void setActive(Map<String, Object> map_, Boolean value_) {
		setMapValue(map_, ALERT_IS_ACTIVE, value_);
	}

	static void setMapValue(Map<String, Object> map_, String key_, Object value_) {
		if (value_ == null) {
			map_.remove(key_);
		} else {
			map_.put(key_, value_);
		}
	}

	static String toString(IATUserAlert alert_) {
		String str = String.format("%s|%s|%s|%s", alert_.getOccId(), alert_.getProperty(), alert_.getOperator(),
				alert_.getValue());
		return str;
	}

}
