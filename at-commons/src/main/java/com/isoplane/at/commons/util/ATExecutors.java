package com.isoplane.at.commons.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;

public class ATExecutors implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATExecutors.class);

	static private ATExecutors instance;

	private int _standardThreadCount;
	private int _scheduledThreadCount;
	private ForkJoinPool _standardExecutor;
	private ScheduledThreadPoolExecutor _scheduledExecutor;
	private Map<String, Long> _requestCountMap;

	private ATExecutors(int standardThreadCount_, int scheduledThreadCount_) {
		_standardThreadCount = standardThreadCount_;
		_scheduledThreadCount = scheduledThreadCount_;
		_requestCountMap = new ConcurrentHashMap<>();
		initInstance();

		String statusId = String.format("core.%s", this.getClass().getSimpleName());
		ATSystemStatusManager.register(this, statusId);

	}

	/**
	 * Ideally call this from 'main' methods, not constructor etc, so it only gets called once.
	 * 
	 * @param config_
	 */
	static public void init() {
		if (instance == null) {
			Configuration config = ATConfigUtil.config();
			int standardThreadCount = config.getInt("executors.size.standard");
			int scheduledThreadCount = config.getInt("executors.size.scheduled");
			instance = new ATExecutors(standardThreadCount, scheduledThreadCount);
		} else {
			log.debug(String.format("Executor already initialized: %s", instance.getStatus()));
		}
	}

	static public boolean isInitialized() {
		return instance != null;
	}

	static private void checkInitialized(String id_) {
		if (instance == null)
			throw new ATException(String.format("%s not initialized", ATExecutors.class.getSimpleName()));
		String id = String.format("count.%s", id_);
		Long count = instance._requestCountMap.get(id);
		long newCount = count != null ? count + 1 : 1;
		instance._requestCountMap.put(id, newCount);
	}

	static public Future<?> submit(String id_, Runnable command_) {
		log.debug(String.format("Submitting thread [%s]", id_));
		checkInitialized(id_);
		ForkJoinTask<?> future = instance._standardExecutor.submit(command_);
		return future;
	}

	static public <T> Future<T> submit(String id_, Callable<T> call_) {
		log.debug(String.format("Submitting thread [%s]", id_));
		checkInitialized(id_);
		ForkJoinTask<T> future = instance._standardExecutor.submit(call_);
		return future;
	}

	static public ScheduledFuture<?> schedule(String id_, Runnable command_, long delay_) {
		log.debug(String.format("Scheduling thread [%s]", id_));
		checkInitialized(id_);
		ScheduledFuture<?> future = instance._scheduledExecutor.schedule(command_, delay_, TimeUnit.MILLISECONDS);
		return future;
	}

	/**
	 * If any execution of the task encounters an exception, subsequent executions are suppressed.Otherwise, the task will only terminate via
	 * cancellation or termination of the executor. If any execution of this task takes longer than its period, then subsequent executionsmay start
	 * late, but will not concurrently execute.
	 */
	static public ScheduledFuture<?> scheduleAtFixedRate(String id_, Runnable command_, long initialDelay_, long period_) {
		log.debug(String.format("Scheduling periodic thread [%s]", id_));
		checkInitialized(id_);
		ScheduledFuture<?> future = instance._scheduledExecutor.scheduleAtFixedRate(command_, initialDelay_, period_, TimeUnit.MILLISECONDS);
		return future;
	}

	static public void shutdown() {
		if (instance == null)
			return;
		if (instance._scheduledExecutor != null)
			instance._scheduledExecutor.shutdown();
		if (instance._standardExecutor != null)
			instance._standardExecutor.shutdown();
	}

	private void initInstance() {
		log.info(String.format("Initializing Executor {Standard: %d, Scheduled: %d}", _standardThreadCount, _scheduledThreadCount));
		_scheduledExecutor = new ScheduledThreadPoolExecutor(_scheduledThreadCount);
		_scheduledExecutor.setRemoveOnCancelPolicy(true);

		_standardExecutor = new ForkJoinPool(_standardThreadCount, ForkJoinPool.defaultForkJoinWorkerThreadFactory, null, true);

		_scheduledExecutor.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				log.debug(getStatus());
			}
		}, 0, 5, TimeUnit.MINUTES);
	}

	private String getStatus() {
		return String.format("Executor status: %s", getMonitoringStatus());
		// return String.format("Executor status {Standard: %d/%d/%d, Scheduled: %d/%d}", _standardExecutor.getActiveThreadCount(),
		// _standardThreadCount, _standardExecutor.getStealCount(), _scheduledExecutor.getActiveCount(), _scheduledThreadCount);
	}

	// @Override
	private Map<String, String> getMonitoringStatus() {
		BlockingQueue<Runnable> scheduledQueue = _scheduledExecutor.getQueue();
		List<String> scheduledIds = new ArrayList<>();
		if (scheduledQueue != null) {
			for (Runnable runnable : scheduledQueue) {
				String id = (runnable instanceof IATIdable) ? ((IATIdable) runnable).getId() : "n/a";
				scheduledIds.add(id);
			}
		}

		Map<String, String> status = new HashMap<>();
		status.put("standard.count.active", _standardExecutor.getActiveThreadCount() + "");
		status.put("standard.count.init", _standardThreadCount + "");
		status.put("standard.count.steal", _standardExecutor.getStealCount() + "");
		status.put("scheduler.count.init", _scheduledThreadCount + "");
		status.put("scheduler.count.active", _scheduledExecutor.getActiveCount() + "");
		status.put("scheduler.count.completed", _scheduledExecutor.getCompletedTaskCount() + "");
		// status.put("scheduler.threadIds", String.join(", ", scheduledIds));
		for (Entry<String, Long> entry : _requestCountMap.entrySet()) {
			status.put(entry.getKey(), entry.getValue() + "");
		}
		// status.putAll(_requestCountMap);
		return status;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		return getMonitoringStatus();
	}

	@Override
	public boolean isRunning() {
		return true;
	}
}
