package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Slow changing properties (>24h)
 * 
 * @author miron
 */
@Deprecated
public class ATProductDetail extends HashMap<String, Object> implements IATMarketData, IATTrade {

	private static final long serialVersionUID = 1L;

	// public String id;
	public Long ts;

	// public Double pxAvg;
	// public String pxAvgMode;
	// public String pxHVMode;
	// public String note;

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public int compareTo(Object other_) {
		String otherId = other_ != null && (other_ instanceof ATProductDetail) ? ((ATProductDetail) other_).getAtId() : null;
		if (otherId == null)
			return -1;
		return this.getAtId().compareTo(otherId);
	}

	// IATMarketData

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	@Override
	public Double getPxLast() {
		return IATMarketData.getPxLast(this);
	}

	@Override
	public String getUnderlying() {
		return IATMarketData.getUnderlying(this);
	}
	
	@Override
	public Integer getSzLast() {
		return IATAsset.getSzLast(this);
	}

	@Override
	public Long getTsLast() {
		return IATMarketData.getTsLast(this);
	}

	@Override
	public Double getPxAsk() {
		return IATMarketData.getPxAsk(this);
	}

	@Override
	public Long getTsAsk() {
		return IATMarketData.getTsAsk(this);
	}

	@Override
	public Integer getSzAsk() {
		return IATAsset.getSzAsk(this);
	}

	@Override
	public Double getPxBid() {
		return IATMarketData.getPxBid(this);
	}

	@Override
	public Long getTsBid() {
		return IATMarketData.getTsBid(this);
	}

	@Override
	public Integer getSzBid() {
		return IATAsset.getSzBid(this);
	}

	@Override
	public Long getDailyVol() {
		return IATMarketData.getDailyVol(this);
	}

	@Override
	public Double getPxOpen() {
		return IATMarketData.getPxOpen(this);
	}

	@Override
	public Double getPxClose() {
		return IATMarketData.getPxClose(this);
	}

	@Override
	public Double getPxClosePrev() {
		return IATMarketData.getPxClosePrev(this);
	}

	@Override
	public Double getPxHigh() {
		return IATMarketData.getPxHigh(this);
	}

	@Override
	public Double getPxLow() {
		return IATMarketData.getPxLow(this);
	}
	
	@Override
	public Long getOpenInterest() {
		return IATMarketData.getOpenInterest(this);
	}

	public void setOpenInterest(Long value_) {
		IATMarketData.setOpenInterest(this, value_);
	}
	
	@Override
	public Double getPxStrike() {
		return IATMarketData.getPxStrike(this);
	}

//	public void setPxStrike(Double value_) {
//		IATMarketData.setPxStrike(this, value_);
//	}
	
	@Override
	public Double getPxChange() {
		return IATMarketData.getPxChange(this);
	}

//	public void setPxChange(Double value_) {
//		IATMarketData.setPxChange(this, value_);
//	}
	
	@Override
	public String getOptionRight() {
		return IATMarketData.getOptionRight(this);
	}
	
	@Override
	public <T> T getMeta(String key_) {
		return IATMarketData.getMeta(this, key_);
	}

//	public void setOptionRight(String value_) {
//		IATMarketData.setOptionRight(this, value_);
//	}	
	

	// IATTrade

	@Override
	public String getOccId() {
		return IATTrade.getOccId(this);
	}

	public void setOccId(String value_) {
		IATTrade.setOccId(this, value_);
	}

	@Override
	public String getTradeUserId() {
		return IATTrade.getTradeUserId(this);
	}

	public void setTradeUserId(String value_) {
		IATTrade.setTradeUserId(this, value_);
	}

	@Override
	public Date getTsTrade() {
		return IATTrade.getTsTrade(this);
	}

	public void setTsTrade(Date value_) {
		IATTrade.setTsTrade(this, value_);
	}

	@Override
	public Long getTsTrade2() {
		return IATTrade.getTsTrade2(this);
	}

	public void setTsTrade2(Long value_) {
		IATTrade.setTsTrade2(this, value_);
	}

	@Override
	public Double getSzTrade() {
		return IATTrade.getSzTrade(this);
	}

	public void setSzTrade(Double value_) {
		IATTrade.setSzTrade(this, value_);
	}

	@Override
	public Double getSzTradeActive() {
		return IATTrade.getSzTradeActive(this);
	}

	public void setSzTradeActive(Double value_) {
		IATTrade.setSzTradeActive(this, value_);
	}

	@Override
	public Double getPxNet() {
		return IATTrade.getPxNet(this);
	}

	public void setPxNet(Double value_) {
		IATTrade.setPxNet(this, value_);
	}
	
	@Override
	public Double getPxTrade() {
		return IATTrade.getPxTrade(this);
	}

	public void setPxTrade(Double value_) {
		IATTrade.setPxTrade(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATTrade.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATTrade.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxUl() {
		return IATTrade.getPxUl(this);
	}

	public void setPxUl(Double value_) {
		IATTrade.setPxUl(this, value_);
	}
	
	@Override
	public Integer getTradeType() {
		return IATTrade.getTradeType(this);
	}

	public void setTradeType(int value_) {
		IATTrade.setTradeType(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATTrade.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		IATTrade.setTradeAccount(this, value_);
	}

	@Override
	public String getTradeComment() {
		return IATTrade.getTradeComment(this);
	}

	public void setTradeComment(String value_) {
		IATTrade.setTradeComment(this, value_);
	}

	@Override
	public String getTradeGroup() {
		return IATTrade.getTradeGroup(this);
	}

	public void setTradeGroup(String value_) {
		IATTrade.setTradeGroup(this, value_);
	}

	// Other

	public void setGeneralReference(String value_) {
		IATAsset.setMapValue(this, "g_ref", value_);
	}

	public Double getRisk() {
		return IATAsset.getDouble(this, IATAssetConstants.RISK);
	}

	public void setRisk(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.RISK, value_);
	}

	public Double getMargin() {
		return IATAsset.getDouble(this, IATAssetConstants.MARGIN);
	}

	public void setMargin(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.MARGIN, value_);
	}

	@Override
	public String getTraceId() {
		// TODO Auto-generated method stub
		return null;
	}


}