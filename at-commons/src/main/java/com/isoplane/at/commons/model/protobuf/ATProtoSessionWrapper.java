package com.isoplane.at.commons.model.protobuf;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.protobuf.UserSessionProtos.UserSession;
import com.isoplane.at.commons.util.EATChangeOperation;

public class ATProtoSessionWrapper {

	private UserSession _proto;
	private ATSession _at;
	private EATChangeOperation _action;

	public ATProtoSessionWrapper(UserSession proto_) {
		this._proto = proto_;
	}

	public UserSession proto() {
		return this._proto;
	}

	public ATSession value() {
		if (this._at == null) {
			this._at = toUserSession(this._proto);
		}
		return this._at;
	}

	static public ATSession toUserSession(UserSession proto_) {
		ATSession at = new ATSession();
		// at.setExpiration(proto_.getExpiration());
		at.setToken(proto_.getSessionId());
		at.setUserId(proto_.getUserId());
		Map<String, String> data = proto_.getDataMap();
		if (data != null && !data.isEmpty()) {
			at.setData(data);
		}
		if (proto_.hasUserName()) {
			at.setName(proto_.getUserName());
		}
		return at;
	}

	static public UserSession toUserSession(ATSession at_, EATChangeOperation action_) {
		return toUserSession(at_, action_, null);
	}

	static public UserSession toUserSession(ATSession at_, EATChangeOperation action_, String source_) {
		return toUserSession(at_, action_, source_, false);
	}

	static public UserSession toUserSession(ATSession at_, EATChangeOperation action_, String source_, boolean isTest_) {
		UserSession.Builder builder = UserSession.newBuilder();
		if (action_ != null) {
			builder.setActionCode(action_.name());
		}
		// builder.setExpiration(at_..getExpiration());
		builder.setSessionId(at_.getToken());
		builder.setUserId(at_.getUserId());
		String name = at_.getName();
		if (name != null) {
			builder.setUserName(name);
		}
		Map<String, String> data = at_.getData();
		if (data != null) {
			builder.putAllData(data);
		}
		if (source_ != null) {
			builder.setSrc(source_);
		}
		if (isTest_) {
			builder.setIsTest(isTest_);
		}
		UserSession proto = builder.build();
		return proto;
	}

	public EATChangeOperation getAction() {
		if (_action == null) {
			String tmp = _proto.getActionCode();
			_action = StringUtils.isBlank(tmp) ? EATChangeOperation.UPDATE : EATChangeOperation.valueOf(tmp);
		}
		return _action;
	}

	public void setAction(EATChangeOperation _action) {
		this._action = _action;
	}

}
