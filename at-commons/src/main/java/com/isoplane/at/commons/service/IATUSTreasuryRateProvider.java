package com.isoplane.at.commons.service;

public interface IATUSTreasuryRateProvider extends IATService {

	Double getRiskFreeInterestRate();

}
