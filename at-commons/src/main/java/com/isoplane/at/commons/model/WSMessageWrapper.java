package com.isoplane.at.commons.model;

public class WSMessageWrapper {
	
	public static final String TARGET_ALL = "all";

	private String source;
	private String target;
	private String type;
	private String function;
	private Object data;
	private Object error;

	public transient String raw;

	public WSMessageWrapper() {
	}

	public WSMessageWrapper(String source_, String target_, String type_, String function_, Object data_) {
		setSource(source_);
		setTarget(target_);
		setType(type_);
		setFunction(function_);
		setData(data_);
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Object getError() {
		return error;
	}

	public void setError(Object error) {
		this.error = error;
	}

	@Override
	public String toString() {
		String str = String.format("%s -> %s(%s:%s)", getSource(), getTarget(), getType(), getFunction());
		return str;
	}

}
