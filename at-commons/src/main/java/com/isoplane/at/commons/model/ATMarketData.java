package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.util.ATModelUtil;

public class ATMarketData extends HashMap<String, Object> implements IATMarketData, Comparable<ATMarketData> {

	private static final long serialVersionUID = 1L;

	transient private String exp_ds6;
	transient private Date dateExp;

	public ATMarketData() {
	}

	public ATMarketData(IATMarketData data_) {
		this.setOccId(data_.getAtId());
		this.setPxAsk(data_.getPxAsk());
		this.setSzAsk(data_.getSzAsk());
		this.setTsAsk(data_.getTsAsk());
		this.setPxBid(data_.getPxBid());
		this.setSzBid(data_.getSzBid());
		this.setTsBid(data_.getTsBid());
		this.setPxLast(data_.getPxLast());
		this.setSzLast(data_.getSzLast());
		this.setTsLast(data_.getTsLast());
		this.setDailyVol(data_.getDailyVol());
		this.setPxOpen(data_.getPxOpen());
		this.setPxHigh(data_.getPxHigh());
		this.setPxLow(data_.getPxLow());
		this.setPxClosePrev(data_.getPxClosePrev());
		this.setPxClose(data_.getPxClose());
		this.setOpenInterest(data_.getOpenInterest());
		this.setPxStrike(data_.getPxStrike());
		this.setOptionRight(data_.getOptionRight());
		this.setPxChange(data_.getPxChange());
		// this.setPxYield(data_.getPxYield());
		this.calculatePxChange();
	}

	public ATMarketData(Map<String, Object> map_) {
		String id = IATAsset.getAtId(map_);
		if (StringUtils.isBlank(id)) {
			id = IATAsset.getString(map_, IATAssetConstants.OCCID);
		}
		this.setOccId(id);
		this.setPxAsk(IATMarketData.getPxAsk(map_));
		this.setSzAsk(IATAsset.getSzAsk(map_));
		this.setTsAsk(IATMarketData.getTsAsk(map_));
		this.setPxBid(IATMarketData.getPxBid(map_));
		this.setSzBid(IATAsset.getSzBid(map_));
		this.setTsBid(IATMarketData.getTsBid(map_));
		this.setPxLast(IATMarketData.getPxLast(map_));
		this.setSzLast(IATAsset.getSzLast(map_));
		this.setTsLast(IATMarketData.getTsLast(map_));
		this.setDailyVol(IATMarketData.getDailyVol(map_));
		this.setPxOpen(IATMarketData.getPxOpen(map_));
		this.setPxHigh(IATMarketData.getPxHigh(map_));
		this.setPxLow(IATMarketData.getPxLow(map_));
		this.setPxClosePrev(IATMarketData.getPxClosePrev(map_));
		this.setPxClose(IATMarketData.getPxClose(map_));
		this.setOpenInterest(IATMarketData.getOpenInterest(map_));
		this.setPxStrike(IATMarketData.getPxStrike(map_));
		this.setOptionRight(IATMarketData.getOptionRight(map_));
		this.setPxChange(IATMarketData.getPxChange(map_));
		// this.setPxYield(IATMarketData.getPxYield(map_));
		this.calculatePxChange();
	}

	private boolean calculatePxChange() {
		Double pxc = this.getPxChange();
		if (pxc != null)
			return true;
		pxc = IATMarketData.calculatePxChange(this);
		if (pxc != null) {
			super.put(IATAssetConstants.PX_CHANGE, pxc);
			return true;
		} else {
			return false;
		}
	}

	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	public Double getRisk() {
		return IATAsset.getDouble(this, IATAssetConstants.RISK);
	}

	public void setRisk(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.RISK, value_);
	}

	@Override
	public String getAtId() {
		return this.getOccId();
	}

	public Double getPxMid() {
		Double ask = this.getPxAsk();
		Double bid = this.getPxBid();
		Double px = ((ask != null ? ask : 0) + (bid != null ? bid : 0)) / 2;
		return px != 0 ? px : null;
	}

	@Override
	public Double getPxLast() {
		return IATMarketData.getPxLast(this);
	}

	public void setPxLast(Double value_) {
		IATMarketData.setPxLast(this, value_);
	}

	@Override
	public Integer getSzLast() {
		return IATAsset.getSzLast(this);
	}

	public void setSzLast(Integer value_) {
		IATAsset.setSzLast(this, value_);
	}

	@Override
	public Long getTsLast() {
		return IATMarketData.getTsLast(this);
	}

	public void setTsLast(Long value_) {
		IATMarketData.setTsLast(this, value_);
	}

	@Override
	public Double getPxAsk() {
		return IATMarketData.getPxAsk(this);
	}

	public void setPxAsk(Double value_) {
		IATMarketData.setPxAsk(this, value_);
	}

	@Override
	public Long getTsAsk() {
		return IATMarketData.getTsAsk(this);
	}

	public void setTsAsk(Long value_) {
		IATMarketData.setTsAsk(this, value_);
	}

	@Override
	public Integer getSzAsk() {
		return IATAsset.getSzAsk(this);
	}

	public void setSzAsk(Integer value_) {
		IATAsset.setSzAsk(this, value_);
	}

	@Override
	public Double getPxBid() {
		return IATMarketData.getPxBid(this);
	}

	public void setPxBid(Double value_) {
		IATMarketData.setPxBid(this, value_);
	}

	@Override
	public Long getTsBid() {
		return IATMarketData.getTsBid(this);
	}

	public void setTsBid(Long value_) {
		IATMarketData.setTsBid(this, value_);
	}

	@Override
	public Integer getSzBid() {
		return IATAsset.getSzBid(this);
	}

	public void setSzBid(Integer value_) {
		IATAsset.setSzBid(this, value_);
	}

	@Override
	public Long getDailyVol() {
		return IATMarketData.getDailyVol(this);
	}

	public void setDailyVol(Long value_) {
		IATMarketData.setDailyVol(this, value_);
	}

	@Override
	public Double getPxOpen() {
		return IATMarketData.getPxOpen(this);
	}

	public void setPxOpen(Double value_) {
		IATMarketData.setPxOpen(this, value_);
	}

	@Override
	public Double getPxClose() {
		return IATMarketData.getPxClose(this);
	}

	public void setPxClose(Double value_) {
		IATMarketData.setPxClose(this, value_);
	}

	@Override
	public Double getPxClosePrev() {
		return IATMarketData.getPxClosePrev(this);
	}

	public void setPxClosePrev(Double value_) {
		IATMarketData.setPxClosePrev(this, value_);
	}

	@Override
	public Double getPxHigh() {
		return IATMarketData.getPxHigh(this);
	}

	public void setPxHigh(Double value_) {
		IATMarketData.setPxHigh(this, value_);
	}

	@Override
	public Double getPxLow() {
		return IATMarketData.getPxLow(this);
	}

	public void setPxLow(Double value_) {
		IATMarketData.setPxLow(this, value_);
	}

	public Double getPxTheo() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_THEORETICAL);
	}

	public void setPxTheo(Double value_) {
		super.put(IATAssetConstants.PX_THEORETICAL, value_);
	}

	@Override
	public Long getOpenInterest() {
		return IATMarketData.getOpenInterest(this);
	}

	public void setOpenInterest(Long value_) {
		IATMarketData.setOpenInterest(this, value_);
	}

	@Override
	public Double getPxStrike() {
		return IATMarketData.getPxStrike(this);
	}

	public void setPxStrike(Double value_) {
		IATAsset.setPxStrike(this, value_);
	}

	@Override
	public Double getPxChange() {
		return IATMarketData.getPxChange(this);
	}

	public void setPxChange(Double value_) {
		IATMarketData.setPxChange(this, value_);
	}

	// @Override
	// public Double getPxYield() {
	// return IATMarketData.getPxYield(this);
	// }
	//
	// public void setPxYield(Double value_) {
	// IATMarketData.setPxYield(this, value_);
	// }

	@Override
	public String getOptionRight() {
		return IATMarketData.getOptionRight(this);
	}

	public void setOptionRight(String value_) {
		IATMarketData.setOptionRight(this, value_);
	}

	public Double getOptionDelta() {
		return IATMarketData.getOptionDelta(this);
	}

	public void setOptionDelta(Double value_) {
		IATMarketData.setOptionDelta(this, value_);
	}

	public Double getOptionGamma() {
		return IATMarketData.getOptionGamma(this);
	}

	public void setOptionGamma(Double value_) {
		IATMarketData.setOptionGamma(this, value_);
	}

	public Double getOptionRho() {
		return IATMarketData.getOptionRho(this);
	}

	public void setOptionRho(Double value_) {
		IATMarketData.setOptionRho(this, value_);
	}

	public Double getOptionTheta() {
		return IATMarketData.getOptionTheta(this);
	}

	public void setOptionTheta(Double value_) {
		IATMarketData.setOptionTheta(this, value_);
	}

	public Double getOptionVega() {
		return IATMarketData.getOptionVega(this);
	}

	public void setOptionVega(Double value_) {
		IATMarketData.setOptionVega(this, value_);
	}

	public Double getIV() {
		return IATMarketData.getIV(this);
	}

	public void setIV(Double value_) {
		IATMarketData.setIV(this, value_);
	}

	@Override
	public <T> T getMeta(String key_) {
		return IATMarketData.getMeta(this, key_);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public String getUnderlying() {
		String occId = getOccId();
		if (occId.length() <= 6)
			return occId;
		return occId.substring(0, 6).trim();
		// return IATMarketData.getUnderlying(this);
	}

	public String getExpDS6() {
		if (exp_ds6 == null) {
			String occId = getOccId();
			if (occId.length() < 10)
				return null;
			exp_ds6 = ATModelUtil.getExpirationDS6(occId);
		}
		return exp_ds6;
	}

	public Date getDateExp() {
		if (dateExp == null) {
			String occId = getOccId();
			if (occId.length() < 10)
				return null;
			dateExp = ATModelUtil.getExpiration(occId);
		}
		return dateExp;
	}

	@Override
	public int compareTo(ATMarketData other_) {
		if (other_ == null)
			return -1;
		return this.getOccId().compareTo(other_.getOccId());
	}

}
