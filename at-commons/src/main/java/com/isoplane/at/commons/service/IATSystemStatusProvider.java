package com.isoplane.at.commons.service;

import java.util.Map;

public interface IATSystemStatusProvider {

	boolean isRunning();

	Map<String, String> getSystemStatus();
}
