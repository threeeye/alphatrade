package com.isoplane.at.commons.util;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.commons.lang3.StringUtils;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.joran.spi.NoAutoStart;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.RollingPolicy;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import ch.qos.logback.core.rolling.TriggeringPolicyBase;

public class ATFileAppender extends RollingFileAppender<ILoggingEvent> {

	private static String _file = null;
	public static String FILE;

	static public void overrideFile(String file_) {
		_file = file_;
	}

	@Override
	public void setFile(String file_) {
		String file = StringUtils.isBlank(_file) ? file_ : _file;
		super.setFile(file);
	}

	@Override
	public void setRollingPolicy(RollingPolicy policy_) {
		if (!StringUtils.isBlank(_file) && policy_ instanceof TimeBasedRollingPolicy) {
			@SuppressWarnings("rawtypes")
			TimeBasedRollingPolicy policy = (TimeBasedRollingPolicy) policy_;
			int idx = _file.lastIndexOf('/');
			if (idx < 0) {
				policy.setFileNamePattern("%d{yyyy-MM-dd}_" + _file);
			} else {
				String pre = _file.substring(0, idx + 1);
				String post = _file.substring(idx + 1);
				policy.setFileNamePattern(pre + "%d{yyyy-MM-dd}_" + post);
			}
			policy.setFileNamePattern("");
			super.setRollingPolicy(policy);
		} else {
			super.setRollingPolicy(policy_);
		}
	}

	@NoAutoStart
	public static class ATTimeBasedRollingPolicy<E> extends TimeBasedRollingPolicy<E> {

		@Override
		public void setFileNamePattern(String file_) {
			String file = file_;
			if (!StringUtils.isBlank(_file)) {
				int idx = _file.lastIndexOf('/');
				if (idx < 0) {
					file = "%d{yyyy-MM-dd}_" + _file;
				} else if (idx + 2 <= _file.length()) {
					String pre = _file.substring(0, idx + 1);
					String post = _file.substring(idx + 1);
					file = pre + "%d{yyyy-MM-dd}_" + post;
				}
			}
			super.setFileNamePattern(file);
		}

	}


	public static class ATStartupTriggeringPolicy<E> extends TriggeringPolicyBase<E> {
		private final AtomicReference<Boolean> isFirstTime = new AtomicReference<Boolean>(true);

		@Override
		public boolean isTriggeringEvent(final File activeFile, final E event) {
			return isFirstTime.compareAndSet(true, false);// || result;
		}
	}
}
