package com.isoplane.at.commons.store;

import java.util.HashMap;

public abstract class ATPersistentBridgeMap extends HashMap<String, Object> implements IATLookupMap {

	static final long serialVersionUID = 1L;

	private String _idField;

	protected ATPersistentBridgeMap(String idField_) {
		setIdField(idField_);
	}

	public String getIdField() {
		return _idField;
	}

	public void setIdField(String value_) {
		this._idField = value_;
	}

	@Override
	public void setAtId(String value_) {
		this.put(_idField, value_);
	}

	@Override
	public String getAtId() {
		return (String) this.get(_idField);
	}

	@Override
	public Long getInsertTime() {
		return IATLookupMap.getInsertTime(this);
	}

	@Override
	public void setUpdateTime(long value_) {
		IATLookupMap.setUpdateTime(this, value_);
	}

	@Override
	public Long getUpdateTime() {
		return IATLookupMap.getUpdateTime(this);
	}

}
