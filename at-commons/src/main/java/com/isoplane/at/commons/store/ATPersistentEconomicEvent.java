package com.isoplane.at.commons.store;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATEconomicEvent;
import com.isoplane.at.commons.util.ATFormats;

public class ATPersistentEconomicEvent extends ATPersistentLookupBase
		implements IATEconomicEvent, Comparable<ATPersistentEconomicEvent>, IATAssetConstants {

	private static final long serialVersionUID = 1L;

	@Override
	public String getDescription() {
		return (String) get(DESCRIPTION);
	}

	public void setDescription(String value_) {
		put(DESCRIPTION, value_);
	}

	@Override
	public int compareTo(ATPersistentEconomicEvent other_) {
		int result = this.getDate().compareTo(other_.getDate());
		if (result != 0)
			return result;
		result = this.getRegion().compareTo(other_.getRegion());
		if (result != 0)
			return result;
		return this.getDescription().compareTo(other_.getDescription());
	}

	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public String getSource() {
		return (String) super.get(IATAssetConstants.SOURCE);
	}

	@Override
	public Date getDate() {
		String dstr = getDateStr();
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			Date date = ATFormats.DATE_TIME_mid.get().parse(dstr);
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}

	@Override
	public String getDateStr() {
		return (String) super.get(IATAssetConstants.DATE_STR);
	}

	public void setDateStr(String value_) {
		super.put(IATAssetConstants.DATE_STR, value_);
	}

	@Override
	public Double getActual() {
		return (Double) super.get(VAL_ACTUAL);
	}

	public void setActual(Double value_) {
		super.put(IATAssetConstants.VAL_ACTUAL, value_);
	}

	@Override
	public Double getConsensus() {
		return (Double) super.get(VAL_CONSENSUS);
	}

	public void setConsensus(Double value_) {
		super.put(IATAssetConstants.VAL_CONSENSUS, value_);
	}
	
	@Override
	public Double getForecast() {
		return (Double) super.get(VAL_EXPECT);
	}

	public void setForecast(Double value_) {
		super.put(IATAssetConstants.VAL_EXPECT, value_);
	}

	@Override
	public Double getPrevious() {
		return (Double) super.get(VAL_PREVIOUS);
	}

	public void setPrevious(Double value_) {
		super.put(IATAssetConstants.VAL_PREVIOUS, value_);
	}

	@Override
	public String getUnit() {
		return (String) super.get(VAL_UNIT);
	}

	public void setUnit(String value_) {
		super.put(IATAssetConstants.VAL_UNIT, value_);
	}

	@Override
	public String toString() {
		Date d = getDate();
		String str = String.format("%s (%s) %2d - %s, %.2f, %.2f, %.2f (%s)", ATFormats.DATE_TIME_mid.get().format(d), getRegion(), getImportance(),
				getDescription(), getActual(), getForecast(), getPrevious(), getUnit());
		return str;
	}

	@Override
	public String getRegion() {
		return (String) super.get(IATAssetConstants.REGION);
	}

	public void setRegion(String value_) {
		super.put(IATAssetConstants.REGION, value_);
	}

	@Override
	public Integer getImportance() {
		return (Integer) super.get(IATAssetConstants.IMPORTANCE);
	}

	public void setImportance(Integer value_) {
		super.put(IATAssetConstants.IMPORTANCE, value_);
	}

}