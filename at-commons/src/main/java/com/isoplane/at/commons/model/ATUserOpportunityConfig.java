package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATUserOpportunityConfig extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	static public final String STRAT_HV = "hv";
	static public final String STRAT_USER = "user";
	static public final String STRAT_ATM = "atm";
	static public final String STRAT_ITM = "itm";
	static public final String STRAT_OTM = "otm";
	static public final String STRAT_VLT = "vlt";

	public ATUserOpportunityConfig() {
	}

	public ATUserOpportunityConfig(Map<String, Object> tmpl_) {
		// this.setFilters(ATUserEquityModel.getFilters(tmpl_));
		this.setId(ATUserOpportunityConfig.getId(tmpl_));
		this.setLabel(ATUserOpportunityConfig.getLabel(tmpl_));
		this.setUserId(ATUserOpportunityConfig.getUserId(tmpl_));

		this.setDataAge(ATUserOpportunityConfig.getDataAge(tmpl_));
		this.setDaysMax(ATUserOpportunityConfig.getDaysMax(tmpl_));
		this.setDaysMin(ATUserOpportunityConfig.getDaysMin(tmpl_));
		this.setDividendOverride(ATUserOpportunityConfig.getDividendOverride(tmpl_));
		this.setFlags(ATUserOpportunityConfig.getFlags(tmpl_));
		this.setOpportunityCount(ATUserOpportunityConfig.getOpportunityCount(tmpl_));
		this.setSpreadMax(ATUserOpportunityConfig.getSpreadMax(tmpl_));
		this.setSpreadMin(ATUserOpportunityConfig.getSpreadMin(tmpl_));
		this.setStrikeMax(ATUserOpportunityConfig.getStrikeMax(tmpl_));
		this.setStrikeMin(ATUserOpportunityConfig.getStrikeMin(tmpl_));
		this.setStrikeStrategy(ATUserOpportunityConfig.getStrikeStrategy(tmpl_));
		this.setVolatilityOverride(ATUserOpportunityConfig.getVolatilityOverride(tmpl_));
		this.setYieldMinAbs(ATUserOpportunityConfig.getYieldMinAbs(tmpl_));
		this.setYieldMinPct(ATUserOpportunityConfig.getYieldMinPct(tmpl_));
	}

	// public int daysMin = 0;
	// public int daysMax = Integer.MAX_VALUE;

	// public int dataAge = 4320; // 3 days in h
	// public int oppCount = 5;

	// public Double overrideDiv = null;
	// public Double overrideVlt = null;

	// public double spreadMin = 0.0;
	// public double spreadMax = Double.MAX_VALUE;

	// public double strikeMin = 0.0;
	// public double strikeMax = Double.MAX_VALUE;
	// public String strikeStrat = "user";

	// public double yieldMinAbs = 0.0;
	// public double yieldMinPct = 0.0;

	// public List<String> flags;

	public String getId() {
		return ATUserOpportunityConfig.getId(this);
	}

	static public String getId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATLookupMap.AT_ID);
	}

	public void setId(String value) {
		if (value == null)
			this.remove(IATLookupMap.AT_ID);
		else
			this.put(IATLookupMap.AT_ID, value);
	}

	public String getUserId() {
		return ATUserOpportunityConfig.getUserId(this);
	}

	static public String getUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value) {
		if (value == null)
			this.remove(IATAssetConstants.USER_ID);
		else
			this.put(IATAssetConstants.USER_ID, value);
	}

	public String getLabel() {
		return ATUserOpportunityConfig.getLabel(this);
	}

	static public String getLabel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LABEL);
	}

	public void setLabel(String value) {
		if (value == null)
			this.remove(IATAssetConstants.LABEL);
		else
			this.put(IATAssetConstants.LABEL, value);
	}

	public Integer getDataAge() {
		return ATUserOpportunityConfig.getDataAge(this);
	}

	static public Integer getDataAge(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.DATA_AGE);

	}

	public void setDataAge(Integer value) {
		if (value == null)
			this.remove(IATAssetConstants.DATA_AGE);
		else
			this.put(IATAssetConstants.DATA_AGE, value);
	}

	public Integer getDaysMax() {
		return ATUserOpportunityConfig.getDaysMax(this);
	}

	static public Integer getDaysMax(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.DAYS_MAX);

	}

	public void setDaysMax(Integer value) {
		if (value == null)
			this.remove(IATAssetConstants.DAYS_MAX);
		else
			this.put(IATAssetConstants.DAYS_MAX, value);
	}

	public Integer getDaysMin() {
		return ATUserOpportunityConfig.getDaysMin(this);
	}

	static public Integer getDaysMin(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.DAYS_MIN);

	}

	public void setDaysMin(Integer value) {
		if (value == null)
			this.remove(IATAssetConstants.DAYS_MIN);
		else
			this.put(IATAssetConstants.DAYS_MIN, value);
	}

	public Double getDividendOverride() {
		return ATUserOpportunityConfig.getDividendOverride(this);
	}

	static public Double getDividendOverride(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.DIVIDEND_OVERRIDE);

	}

	public void setDividendOverride(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.DIVIDEND_OVERRIDE);
		else
			this.put(IATAssetConstants.DIVIDEND_OVERRIDE, value);
	}

	public List<String> getFlags() {
		return ATUserOpportunityConfig.getFlags(this);
	}

	static public List<String> getFlags(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		List<String> flags = map_ != null ? (List<String>) map_.get(IATAssetConstants.FLAGS) : null;
		return flags;

	}

	public void setFlags(List<String> value) {
		if (value == null)
			this.remove(IATAssetConstants.FLAGS);
		else
			this.put(IATAssetConstants.FLAGS, value);
	}

	public Integer getOpportunityCount() {
		return ATUserOpportunityConfig.getOpportunityCount(this);
	}

	static public Integer getOpportunityCount(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.OPPORTUNITY_COUNT);

	}

	public void setOpportunityCount(Integer value) {
		if (value == null)
			this.remove(IATAssetConstants.OPPORTUNITY_COUNT);
		else
			this.put(IATAssetConstants.OPPORTUNITY_COUNT, value);
	}

	public Double getSpreadMax() {
		return ATUserOpportunityConfig.getSpreadMax(this);
	}

	static public Double getSpreadMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.SPREAD_MAX);

	}

	public void setSpreadMax(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.SPREAD_MAX);
		else
			this.put(IATAssetConstants.SPREAD_MAX, value);
	}

	public Double getSpreadMin() {
		return ATUserOpportunityConfig.getSpreadMin(this);
	}

	static public Double getSpreadMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.SPREAD_MIN);

	}

	public void setSpreadMin(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.SPREAD_MIN);
		else
			this.put(IATAssetConstants.SPREAD_MIN, value);
	}

	public Double getStrikeMax() {
		return ATUserOpportunityConfig.getStrikeMax(this);
	}

	static public Double getStrikeMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.STRIKE_MAX);

	}

	public void setStrikeMax(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.STRIKE_MAX);
		else
			this.put(IATAssetConstants.STRIKE_MAX, value);
	}

	public Double getStrikeMin() {
		return ATUserOpportunityConfig.getStrikeMin(this);
	}

	static public Double getStrikeMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.STRIKE_MIN);

	}

	public void setStrikeMin(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.STRIKE_MIN);
		else
			this.put(IATAssetConstants.STRIKE_MIN, value);
	}

	public String getStrikeStrategy() {
		return ATUserOpportunityConfig.getStrikeStrategy(this);
	}

	static public String getStrikeStrategy(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.STRIKE_STRATEGY);
	}

	public void setStrikeStrategy(String value) {
		if (value == null)
			this.remove(IATAssetConstants.STRIKE_STRATEGY);
		else
			this.put(IATAssetConstants.STRIKE_STRATEGY, value);
	}

	public Double getVolatilityOverride() {
		return ATUserOpportunityConfig.getVolatilityOverride(this);
	}

	static public Double getVolatilityOverride(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.VOLATILITY_OVERRIDE);

	}

	public void setVolatilityOverride(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.VOLATILITY_OVERRIDE);
		else
			this.put(IATAssetConstants.VOLATILITY_OVERRIDE, value);
	}

	public Double getYieldMinAbs() {
		return ATUserOpportunityConfig.getYieldMinAbs(this);
	}

	static public Double getYieldMinAbs(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.YIELD_MIN_ABS);

	}

	public void setYieldMinAbs(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.YIELD_MIN_ABS);
		else
			this.put(IATAssetConstants.YIELD_MIN_ABS, value);
	}

	public Double getYieldMinPct() {
		return ATUserOpportunityConfig.getYieldMinPct(this);
	}

	static public Double getYieldMinPct(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.YIELD_MIN_PCT);

	}

	public void setYieldMinPct(Double value) {
		if (value == null)
			this.remove(IATAssetConstants.YIELD_MIN_PCT);
		else
			this.put(IATAssetConstants.YIELD_MIN_PCT, value);
	}

}
