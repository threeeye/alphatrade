package com.isoplane.at.commons.model.alertcriteria;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.util.ATFormats;

public class ATDateCriteria extends ATBaseCriteria {

	private long _dateLong;
	private int _condition;
	private String _note;

	static public ATDateCriteria create(String userId_, ATAlertRule alert_) {
		if (StringUtils.isBlank(userId_) || alert_ == null || StringUtils.isBlank(alert_.getOccId()) || StringUtils.isBlank(alert_.getOperator())
				|| StringUtils.isBlank(alert_.getValue()))
			return null;
		Integer condition = getCondition(alert_);
		if (condition == null)
			return null;
		String criteria = getOperatorString(condition);
		Long value = null;
		try {
			Date date = ATFormats.DATE_yyyy_MM_dd.get().parse(alert_.getValue());
			value = date.getTime();
		} catch (ParseException e) {
			log.error(String.format("Error processing date [%s]", alert_));
		}
		if (value == null || criteria == null)
			return null;
		criteria = String.format("%s %s", criteria, alert_.getValue());
		ATDateCriteria crit = new ATDateCriteria(userId_, alert_.getOccId(), criteria, condition, value, alert_.getMessage());
		return crit;
	}

	static public ATDateCriteria create(String occId_, String str_, String note_) throws ParseException {
		int condition = -1;
		String numStr = null;
		String criteria = "";
		if (str_.startsWith("<=")) {
			numStr = str_.substring(2);
			condition = LTE;
			criteria = "≤";
		} else if (str_.startsWith(">=")) {
			numStr = str_.substring(2);
			condition = GTE;
			criteria = "≥";
		} else if (str_.startsWith("<")) {
			numStr = str_.substring(1);
			condition = LT;
			criteria = "<";
		} else if (str_.startsWith(">")) {
			numStr = str_.substring(1);
			condition = GT;
			criteria = ">";
		}
		Date date = ATFormats.DATE_yyyyMMdd.get().parse(numStr);
		criteria = String.format("%s %s", criteria, ATFormats.DATE_yyyy_MM_dd.get().format(date));
		ATDateCriteria crit = new ATDateCriteria(null, occId_, criteria, condition, date.getTime(), note_);
		return crit;
	}

	private ATDateCriteria(String userId_, String occId_, String criteria_, int condition_, long dateLong_, String note_) {
		super(userId_, occId_, criteria_);
		_condition = condition_;
		_dateLong = dateLong_;
		_note = note_;
	}

	@Override
	@Deprecated
	public Boolean test(ATSecurity sec_) {
		String occId = sec_ != null ? sec_.getOccId() : null;
		return test(occId);
	}

	@Override
	@Deprecated
	public Boolean test(ATPersistentSecurity sec_) {
		String occId = sec_ != null ? sec_.getOccId() : null;
		return test(occId);
	}

	@Override
	public Boolean test(IATMarketData data_, IATStatistics stats_) {
		String occId = data_ != null ? data_.getAtId() : null;
		return test(occId);
	}

	private Boolean test(String occId_) {
		if (!isOccIdMatch(occId_))
			return false;
		long now = System.currentTimeMillis();
		switch (_condition) {
		case GT:
			return now > _dateLong;
		case GTE:
			return now >= _dateLong;
		case LT:
			return now < _dateLong;
		case LTE:
			return now <= _dateLong;
		}
		return false;
	}

	@Override
	public String label() {
		return StringUtils.isBlank(_note) ? super.label() : String.format("'%s'", _note);
	}

	@Override
	public String label(IATMarketData data_, IATStatistics stats) {
		return label();
	}

	@Override
	@Deprecated()
	public String label(ATPersistentSecurity sec_) {
		return label();
	}

}
