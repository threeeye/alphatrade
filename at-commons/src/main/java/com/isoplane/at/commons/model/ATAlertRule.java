package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class ATAlertRule extends HashMap<String, Object> implements Comparable<ATAlertRule>, IATUserAlert {

	private static final long serialVersionUID = 1L;

	public ATAlertRule() {
	}

	private ATAlertRule(/* String userId_, */ String type_, String occId_, String property_, String operator_, String value_, String group_,
			String msg_, List<ATAlertRuleComponent> cmp_, String title_) {
		// this.setUserId(userId_);
		this.setOccId(occId_);
		this.setProperty(property_);
		String operator = StringUtils.isBlank(operator_) ? null : operator_.toUpperCase();
		if (operator != null)
			this.setOperator(operator);
		this.setValue(value_);
		this.setMessage(msg_);
		this.setTitle(title_);
		this.setType(type_);
		this.setComponents(cmp_);
		cleanup();
	}

	public ATAlertRule(IATUserAlert tmpl_) {
		this(/* tmpl_.getUserId(), */ tmpl_.getType(), tmpl_.getOccId(), tmpl_.getProperty(), tmpl_.getOperator(), tmpl_.getValue(), tmpl_.getGroup(),
				tmpl_.getMessage(), tmpl_.getComponents(), tmpl_.getTitle());
		this.setActive(tmpl_.isActive());
	}

	public ATAlertRule(Map<String, Object> map_) {
		// this.setUserId(IATUserAlert.getUserId(map_));
		this.setOccId(IATUserAlert.getOccId(map_));
		this.setProperty(IATUserAlert.getProperty(map_));
		this.setOperator(IATUserAlert.getOperator(map_));
		this.setValue(IATUserAlert.getValue(map_));
		this.setGroup(IATUserAlert.getGroup(map_));
		this.setMessage(IATUserAlert.getMessage(map_));
		this.setTitle(IATUserAlert.getTitle(map_));
		this.setType(IATUserAlert.getType(map_));
		this.setComponents(IATUserAlert.getComponents(map_));
	}

	@Override
	public String getOccId() {
		return IATUserAlert.getOccId(this);
	}

	public void setOccId(String value_) {
		IATUserAlert.setOccId(this, value_);
	}

	// @Deprecated // In alert pack
	// @Override
	// public String getUserId() {
	// return IATUserAlert.getUserId(this);
	// }
	//
	// @Deprecated // In alert pack
	// public void setUserId(String value_) {
	// IATUserAlert.setUserId(this, value_);
	// }

	@Override
	public String getProperty() {
		return IATUserAlert.getProperty(this);
	}

	public void setProperty(String value_) {
		IATUserAlert.setProperty(this, value_);
	}

	@Override
	public String getOperator() {
		return IATUserAlert.getOperator(this);
	}

	public void setOperator(String value_) {
		IATUserAlert.setOperator(this, value_);
	}

	@Override
	public String getValue() {
		return IATUserAlert.getValue(this);
	}

	public void setValue(String value_) {
		IATUserAlert.setValue(this, value_);
	}

	@Override
	public String getGroup() {
		return IATUserAlert.getGroup(this);
	}

	public void setGroup(String value_) {
		IATUserAlert.setGroup(this, value_);
	}

	@Override
	public String getTitle() {
		return IATUserAlert.getTitle(this);
	}

	public void setTitle(String value_) {
		IATUserAlert.setTitle(this, value_);
	}

	@Override
	public String getType() {
		return IATUserAlert.getType(this);
	}

	public void setType(String value_) {
		IATUserAlert.setType(this, value_);
	}

	@Override
	public String getMessage() {
		return IATUserAlert.getMessage(this);
	}

	public void setMessage(String value_) {
		IATUserAlert.setMessage(this, value_);
	}

	@Override
	public List<ATAlertRuleComponent> getComponents() {
		return IATUserAlert.getComponents(this);
	}

	public void setComponents(List<ATAlertRuleComponent> value_) {
		IATUserAlert.setComponents(this, value_);
	}

	@Override
	public Boolean isActive() {
		return IATUserAlert.isActive(this);
	}

	public void setActive(Boolean value_) {
		IATUserAlert.setActive(this, value_);
	}

	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public int compareTo(ATAlertRule other_) {
		if (other_ == null)
			return -1;
		int result = this.getOccId().compareTo(other_.getOccId());
		if (result != 0)
			return result;
		result = this.getProperty().compareTo(other_.getProperty());
		if (result != 0)
			return result;
		return 0;
	}

	@Override
	public String toString() {
		return IATUserAlert.toString(this);
	}

	public ATAlertRule cleanup() {
		// if (StringUtils.isBlank(userId) || StringUtils.isBlank(occId) || StringUtils.isBlank(prp) || StringUtils.isBlank(opr)
		// || StringUtils.isBlank(val))
		// return false;
		String occId = getOccId();
		if (occId != null) {
			occId = occId.trim().toUpperCase();
		}
		String prp = getProperty();
		if (prp != null) {
			prp = prp.trim().toUpperCase();
			setProperty(prp);
		}
		String opr = getOperator();
		if (opr != null) {
			opr = opr.trim().toUpperCase();
			setOperator(opr);
		}
		Iterator<Entry<String, Object>> i = this.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> entry = i.next();
			if (!IATUserAlert.ALERT_PROPS.contains(entry.getKey())) {
				i.remove();
			}
		}
		List<ATAlertRuleComponent> oldCmps = this.getComponents();
		if (oldCmps != null && !oldCmps.isEmpty()) {
			List<ATAlertRuleComponent> newCmps = new ArrayList<>();
			for (Map<String, Object> oldCmp : oldCmps) {
				newCmps.add(ATAlertRuleComponent.cleanup(oldCmp));
			}
			this.setComponents(newCmps);
		}
		return this;
	}

	public static class ATUserAlertRulePack {
		public ATAlertRule[] alerts;
		// public ATUserAlertGroup[] groups;
		public String usrId;
		public String occId;
		public String groupLogic;

		public void cleanup() {
			if (alerts != null) {
				for (ATAlertRule alert : alerts) {
					alert.cleanup();
				}
			}
		}
	}

	public static class ATAlertRuleComponent extends HashMap<String, Object> {

		private static final long serialVersionUID = 1L;
		static final String AC_OCCID = "occId";
		// static final String AC_ACTION = "action";
		static final String AC_COUNT = "count";

		public static final List<String> AC_PROPS = Arrays.asList(AC_COUNT, AC_OCCID);

		public static ATAlertRuleComponent cleanup(Map<String, Object> tmpl_) {
			ATAlertRuleComponent cmp = new ATAlertRuleComponent();
			String occId = (String) tmpl_.get(AC_OCCID);
			if (occId != null) {
				cmp.setOccId(occId);
			}
			// String action = (String) tmpl_.get(AC_ACTION);
			// if (action != null) {
			// cmp.setAction(action);
			// }
			Object countObj = tmpl_.get(AC_COUNT);
			Double count = (countObj == null || countObj instanceof Double)
					? (Double) countObj
					: Double.parseDouble(countObj.toString());
			if (count != null) {
				cmp.setCount(count);
			}
			return cmp;
		}

		public void cleanup() {
			Iterator<Entry<String, Object>> i = this.entrySet().iterator();
			while (i.hasNext()) {
				Entry<String, Object> entry = i.next();
				if (!AC_PROPS.contains(entry.getKey())) {
					i.remove();
				}
			}
		}

		public String getOccId() {
			return (String) get(AC_OCCID);
		}

		public void setOccId(String occId) {
			this.put(AC_OCCID, occId);
		}

		// public String getAction() {
		// return (String) get(AC_ACTION);
		// }
		//
		// public void setAction(String action) {
		// this.put(AC_ACTION, action);
		// }

		public Double getCount() {
			return (Double) get(AC_COUNT);
		}

		public void setCount(Double count) {
			this.put(AC_COUNT, count);
		}
	}

}
