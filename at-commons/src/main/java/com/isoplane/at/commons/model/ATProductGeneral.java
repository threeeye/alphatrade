package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Slow changing properties (>24h)
 * 
 * @author miron
 */
@Deprecated
public class ATProductGeneral extends HashMap<String, Object>
		implements IATProductDescription, IATFundamental, Comparable<ATProductGeneral> {

	private static final long serialVersionUID = 1L;

	public Long ts;

	// public Double mktCap;
	// public Double HV;
	// public Double pxOpen;
	// public Double pxClose;

	// public String dateEarn;
	// public String dateDiv;
	public Double divAmt;

	// public List<ATProductDetail> details;

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public int compareTo(ATProductGeneral other_) {
		String otherId = other_ != null ? other_.getAtId() : null;
		if (otherId == null)
			return -1;
		return this.getAtId().compareTo(otherId);
	}

	public void setDetails(Set<ATProductDetail> value_) {
		super.put("details", value_);
	}

	@SuppressWarnings("unchecked")
	public Set<ATProductDetail> getDetails() {
		return (Set<ATProductDetail>) super.get("details");
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	// IATProductDescription

	@Override
	public String getName() {
		return IATProductDescription.getName(this);
	}

	public void setName(String value_) {
		IATProductDescription.setName(this, value_);
	}

	@Override
	public String getLabel() {
		return IATProductDescription.getLabel(this);
	}

	public void setLabel(String value_) {
		IATProductDescription.setLabel(this, value_);
	}

	@Override
	public String getDescription() {
		return IATProductDescription.getDescription(this);
	}

	public void setDescription(String value_) {
		IATProductDescription.setDescription(this, value_);
	}

	@Override
	public String getSector() {
		return IATProductDescription.getSector(this);
	}

	public void setSector(String value_) {
		IATProductDescription.setSector(this, value_);
	}

	@Override
	public String getIndustry() {
		return IATProductDescription.getIndustry(this);
	}

	public void setIndustry(String value_) {
		IATProductDescription.setIndustry(this, value_);
	}

	@Override
	public String getModel() {
		return IATProductDescription.getModel(this);
	}

	public void setModel(String value_) {
		IATProductDescription.setModel(this, value_);
	}

	// IATFundamental

	@Override
	public String getLatestDS8() {
		return IATFundamental.getLatestDS8(this);
	}

	public void setLatestDS8(String value_) {
		IATFundamental.setLatestDS8(this, value_);
	}
	
	@Override
	public Double getIV() {
		return IATFundamental.getIV(this);
	}
	
	@Override
	public Double getMarketCap() {
		return IATFundamental.getMarketCap(this);
	}

	public void setMarketCap(Double value_) {
		IATFundamental.setMarketCap(this, value_);
	}

	@Override
	public String getEarningsDS8() {
		return IATFundamental.getEarningsDS8(this);
	}

	public void setEarningsDS8(String value_) {
		IATFundamental.setEarningsDS8(this, value_);
	}

	@Override
	public String getDividendExDS8() {
		return IATFundamental.getDividendExDS8(this);
	}

	public void setDividendExDS8(String value_) {
		IATFundamental.setDividendExDS8(this, value_);
	}
	
	@Override
	public String getDividendEstDS8() {
		return IATFundamental.getDividendEstDS8(this);
	}

	public void setDividendEstDS8(String value_) {
		IATFundamental.setDividendEstDS8(this, value_);
	}

	@Override
	public Double getDividend() {
		return IATFundamental.getDividend(this);
	}

	public void setDividend(Double value_) {
		IATFundamental.setDividend(this, value_);
	}

	@Override
	public Integer getDividendFrequency() {
		return IATFundamental.getDividendFrequency(this);
	}
	
	// Other

	public Double getPxLast() {
		return IATMarketData.getPxLast(this);
	}

	public void setPxLast(Double value_) {
		IATMarketData.setPxLast(this, value_);
	}

	// @Override
	public Double getPxOpen() {
		return IATMarketData.getPxOpen(this);
	}

	public void setPxOpen(Double value_) {
		IATMarketData.setPxOpen(this, value_);
	}

	// @Override
	public Double getPxClose() {
		return IATMarketData.getPxClose(this);
	}

	public void setPxClose(Double value_) {
		IATMarketData.setPxClose(this, value_);
	}

	@Override
	public String getModeHV() {
		return IATFundamental.getModeHV(this);
	}

	public void setModeHV(String value_) {
		IATFundamental.setModeHV(this, value_);
	}

	@Override
	public Double getPxHV() {
		return IATFundamental.getPxHV(this);
	}

	public void setPxHV(Double value_) {
		IATFundamental.setPxHV(this, value_);
	}

	@Override
	public String getModeAvg() {
		return IATFundamental.getModeAvg(this);
	}

	public void setModeAvg(String value_) {
		IATFundamental.setModeAvg(this, value_);
	}

	@Override
	public Double getPxAvg() {
		return IATFundamental.getPxAvg(this);
	}

	public void setPxAvg(Double value_) {
		IATFundamental.setPxAvg(this, value_);
	}

	@Override
	public Double getPEGrowthRatio() {
		return IATFundamental.getPEGrowthRatio(this);
	}

	public void setPEGrowthRatio(Double value_) {
		IATFundamental.setPEGrowthRatio(this, value_);
	}

	@Override
	public Double getTrailingPE() {
		return IATFundamental.getTrailingPE(this);
	}

	public void setTrailingPE(Double value_) {
		IATFundamental.setTrailingPE(this, value_);
	}
	
	@Override
	public Double getRevenue1Q() {
		return IATFundamental.getRevenue1Q(this);
	}

	public void setRevenue1Q(Double value_) {
		IATFundamental.setRevenue1Q(this, value_);
	}

	@Override
	public Double getRevenue1Y() {
		return IATFundamental.getRevenue1Y(this);
	}

	public void setRevenue1Y(Double value_) {
		IATFundamental.setRevenue1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Q() {
		return IATFundamental.getRevenueGrowth1Q(this);
	}

	public void setRevenueGrowth1Q(Double value_) {
		IATFundamental.setRevenueGrowth1Q(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Y() {
		return IATFundamental.getRevenueGrowth1Y(this);
	}

	public void setRevenueGrowth1Y(Double value_) {
		IATFundamental.setRevenueGrowth1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthAvg3Y() {
		return IATFundamental.getRevenueGrowthAvg3Y(this);
	}

	public void setRevenueGrowthAvg3Y(Double value_) {
		IATFundamental.setRevenueGrowthAvg3Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthYoY() {
		return IATFundamental.getRevenueGrowthYoY(this);
	}

	public void setRevenueGrowthYoY(Double value_) {
		IATFundamental.setRevenueGrowthYoY(this, value_);
	}
	
	@Override
	public Double getRevenueMultiple() {
		return IATFundamental.getRevenueMultiple(this);
	}

	public void setRevenueMultiple(Double value_) {
		IATFundamental.setRevenueMultiple(this, value_);
	}
	
	@Override
	public Map<String, Double> getAvgMap() {
		return IATFundamental.getAvgMap(this);
	}

	public void setAvgMap(Map<String, Double> map_) {
		IATFundamental.setAvgMap(this, map_);
	}

	@Override
	public Map<String, Double> getHvMap() {
		return IATFundamental.getHvMap(this);
	}

	public void setHvMap(Map<String, Double> map_) {
		IATFundamental.setHvMap(this, map_);
	}

	@Override
	public Map<String, Double> getMaxMap() {
		return IATFundamental.getMaxMap(this);
	}

	public void setMaxMap(Map<String, Double> map_) {
		IATFundamental.setMaxMap(this, map_);
	}

	@Override
	public Map<String, Double> getMinMap() {
		return IATFundamental.getMinMap(this);
	}

	public void setMinMap(Map<String, Double> map_) {
		IATFundamental.setMinMap(this, map_);
	}
	
}