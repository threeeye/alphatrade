package com.isoplane.at.commons.service;

import java.util.Map;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.IATSession;

public interface IATSessionTokenProcessor {
	
	Map<String, String> listUsers();

	ATSession verifySessionToken(IATSession session);

	void revokeSessionToken(String token);
}
