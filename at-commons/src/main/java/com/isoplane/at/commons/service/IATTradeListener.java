package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.util.EATChangeOperation;

public interface IATTradeListener {

	void notify(EATChangeOperation operation, IATTrade trade);
}
