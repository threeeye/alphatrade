package com.isoplane.at.commons.model.protobuf;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.protobuf.AlertRuleProtos.AlertRule;;

public class ATProtoAlertRuleWrapper {

	static public final String TYPE_ALERT_RULE = "alert_rule";
	static private Gson gson = new Gson();

	private AlertRule _protoAlertRule;
	private ATUserAlertRulePack _value;

	public ATProtoAlertRuleWrapper(AlertRule proto_) {
		this._protoAlertRule = proto_;
	}

	public AlertRule proto() {
		return this._protoAlertRule;
	}

	public ATUserAlertRulePack value() {
		if (_value == null) {
			_value = toAlertRule(_protoAlertRule);
		}
		return _value;
	}

	static public ATUserAlertRulePack toAlertRule(AlertRule tmpl_) {
		String json = tmpl_.getData();
		ATUserAlertRulePack pack = toAlertRule(json);
		return pack;
	}

	static public ATUserAlertRulePack toAlertRule(String json_) {
		if (StringUtils.isBlank(json_))
			return null;
		ATUserAlertRulePack pack = gson.fromJson(json_, ATUserAlertRulePack.class);
		pack.cleanup();
		return pack;
	}

	static public AlertRule toAlertRule(ATUserAlertRulePack rule_) {
		if (rule_ == null)
			return null;

		String type = "SEC";
		String userId = rule_.usrId;
		String targetId = rule_.occId;
		String id = String.format("%s:%s", targetId, userId);
		String json = gson.toJson(rule_);

		AlertRule.Builder builder = AlertRule.newBuilder();
		builder.setId(id);
		builder.setUserId(userId);
		builder.setTargetId(targetId);
		builder.setType(type);
		builder.setData(json);
		AlertRule protoRule = builder.build();
		return protoRule;
	}

}
