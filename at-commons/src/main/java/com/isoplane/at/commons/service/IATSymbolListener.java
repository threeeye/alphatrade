package com.isoplane.at.commons.service;

import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.util.EATChangeOperation;

public interface IATSymbolListener {

	// @Deprecated
	// void notify(Map<String, IATSymbol> symbolMap);

	void notify(EATChangeOperation operation, ATSymbol symbol);

	void notifyDynamicSymbols(Map<String, ATSymbol> dynamicSymbols, Map<String, Set<String>> userSymbols);

}
