package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ATOptionExpiration extends HashMap<String, Object> implements IATOptionExpiration, Comparable<IATOptionExpiration> {

	private static final long serialVersionUID = 1L;

	public ATOptionExpiration() {
	}

	public ATOptionExpiration(String occId_, List<ATOptionExpirationDate> exps_) {
		setOccId(occId_);
		setExpirations(exps_);
	}

	@Override
	public int compareTo(IATOptionExpiration other_) {
		return IATOptionExpiration.compareTo(this, other_);
	}

	@Override
	public String toString() {
		return getOccId();
	}

	@Override
	public Map<String, Object> getAsMap() {
		return new HashMap<>(this);
	}

	@Override
	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public List<ATOptionExpirationDate> getExpirations() {
		return IATOptionExpiration.getExpirations(this);
	}

	public void setExpirations(List<ATOptionExpirationDate> value_) {
		IATOptionExpiration.setExpirations(this, value_);
	}

	public static class ATOptionExpirationDate extends HashMap<String, Object> {

		public ATOptionExpirationDate() {
		}

		public ATOptionExpirationDate(String expDS6_, List<Double> strikes_) {
			setExpDS6(expDS6_);
			setStrikes(strikes_);
		}

		private static final long serialVersionUID = 1L;

		public String getExpDS6() {
			return (String) super.get(IATAssetConstants.EXPIRATION_DATE_STR);
		}

		public void setExpDS6(String value_) {
			super.put(IATAssetConstants.EXPIRATION_DATE_STR, value_);
		}

		public List<Double> getStrikes() {
			@SuppressWarnings("unchecked")
			List<Double> strikes = (List<Double>) super.get(STRIKES);
			return strikes;
		}

		public void setStrikes(List<Double> value_) {
			super.put(STRIKES, value_);
		}
	}

}
