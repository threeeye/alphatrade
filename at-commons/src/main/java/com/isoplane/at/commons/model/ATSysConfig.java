package com.isoplane.at.commons.model;

import java.util.List;

public class ATSysConfig {
	
	public String mode;
	public String serverUrl;
	public List<String> extraSymbols;

}
