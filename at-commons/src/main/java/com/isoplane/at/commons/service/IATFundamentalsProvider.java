package com.isoplane.at.commons.service;

import java.util.Map;

import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATDividend;

public interface IATFundamentalsProvider extends IATService {

	Map<String, IATDividend> getDividends(String... symbols);

	Map<String, ATFundamental> getFundamentals(String... symbols);

	boolean updateFundamental(ATFundamental fnd);

}
