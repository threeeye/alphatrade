package com.isoplane.at.commons.store;

import java.util.Iterator;
import java.util.Map;

import com.isoplane.at.commons.model.IATAsset;

public interface IATLookupMap extends Map<String, Object> {

	String INSERT_TIME = "t_ins";
	String UPDATE_TIME = "t_upd";
	String AT_ID = "_atid";
	String IGNORE = "ignore";
	// final String DISABLED = "_off";

	void setUpdateTime(long updateTime);

	Long getUpdateTime();

	// void setInsertTime(long ins);

	Long getInsertTime();

	void setAtId(String id);

	String getAtId();

	static String getAtId(Map<String, Object> map_) {
		String value = (String) map_.get(AT_ID);
		return value;
	}

	static void setAtId(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(AT_ID);
		} else {
			map_.put(AT_ID, value_);
		}
	}

	static void removeNulls(Map<String, Object> map_) {
		if (map_ == null)
			return;
		Iterator<Entry<String, Object>> i = map_.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> entry = i.next();
			if (entry.getValue() == null) {
				i.remove();
			}
		}
	}

	static Long getInsertTime(Map<String, Object> map_) {
		return IATAsset.getLong(map_, INSERT_TIME);
	}

	static Long getUpdateTime(Map<String, Object> map_) {
		Object value = map_.get(UPDATE_TIME);
		if (value instanceof Double) {
			value = Math.round((Double) value);
			map_.put(UPDATE_TIME, (Long) value);
		}
		return (Long) value;
	}

	static void setUpdateTime(Map<String, Object> map_, long value_) {
		map_.put(UPDATE_TIME, value_); // Bypass changed
	}

}
