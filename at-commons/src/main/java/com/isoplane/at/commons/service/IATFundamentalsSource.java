package com.isoplane.at.commons.service;

import java.util.Map;

import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATSymbol;

public interface IATFundamentalsSource {

	Map<String, ATFundamental> processFundamentals(IATSymbol... symbols);
}
