package com.isoplane.at.commons.model;

import java.util.Date;

public interface IATEconomicEvent {

	Date getDate();

	String getDateStr();

	String getRegion();

	String getDescription();

	Double getActual();

	Double getConsensus();

	Double getForecast();

	Double getPrevious();

	String getUnit();

	Integer getImportance();

}
