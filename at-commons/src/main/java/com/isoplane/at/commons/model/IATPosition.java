package com.isoplane.at.commons.model;

import java.util.Map;

public interface IATPosition extends IATMapBase {

    String getOccId();

    void setOccId(String value);

    Double getSize();

    void setSize(Double value);

    Double getPxTrade();

    void setPxTrade(Double value);

    Double getPxTradeTotal();

    void setPxTradeTotal(Double value);

    Double getPxTradeAdjusted();

    void setPxTradeAdjusted(Double value);

    String getUserId();

    void setUserId(String value);

    String getAccountId();

    void setAccountId(String value);

    String getUnderlying();

    void setUnderlying(String value);

    Integer getExp6();

    void setExp6(Integer value);

    Long getTsLast();

    void setTsLast(Long value);

    static void setOccId(Map<String, Object> map_, String value_) {
        IATMapBase.setValue(map_, IATAssetConstants.OCCID, value_, true);
    }

    static String getOccId(Map<String, Object> map_) {
        var value = IATMapBase.getString(map_, IATAssetConstants.OCCID);
        return value;
    }

    static void setSize(Map<String, Object> map_, Double value_) {
        IATMapBase.setValue(map_, IATAssetConstants.SIZE, value_, true);
    }

    static Double getSize(Map<String, Object> map_) {
        var value = IATMapBase.getDouble(map_, IATAssetConstants.SIZE);
        return value;
    }

    static void setPxTrade(Map<String, Object> map_, Double value_) {
        IATMapBase.setValue(map_, IATAssetConstants.PX_TRADE, value_, true);
    }

    static Double getPxTrade(Map<String, Object> map_) {
        var value = IATMapBase.getDouble(map_, IATAssetConstants.PX_TRADE);
        return value;
    }

    static void setPxTradeTotal(Map<String, Object> map_, Double value_) {
        IATMapBase.setValue(map_, IATAssetConstants.PX_TRADE_TOTAL, value_, true);
    }

    static Double getPxTradeTotal(Map<String, Object> map_) {
        var value = IATMapBase.getDouble(map_, IATAssetConstants.PX_TRADE_TOTAL);
        return value;
    }

    static void setPxTradeAdjusted(Map<String, Object> map_, Double value_) {
        IATMapBase.setValue(map_, IATAssetConstants.PX_TRADE_ADJ, value_, true);
    }

    static Double getPxTradeAdjusted(Map<String, Object> map_) {
        var value = IATMapBase.getDouble(map_, IATAssetConstants.PX_TRADE_ADJ);
        return value;
    }

    static void setUserId(Map<String, Object> map_, String value_) {
        IATMapBase.setValue(map_, IATAssetConstants.TRADE_USER, value_, true);
    }

    static String getUserId(Map<String, Object> map_) {
        var value = IATMapBase.getString(map_, IATAssetConstants.TRADE_USER);
        return value;
    }

    static void setAccountId(Map<String, Object> map_, String value_) {
        IATMapBase.setValue(map_, IATAssetConstants.TRADE_ACCOUNT, value_, true);
    }

    static String getAccountId(Map<String, Object> map_) {
        var value = IATMapBase.getString(map_, IATAssetConstants.TRADE_ACCOUNT);
        return value;
    }

    static void setUnderlying(Map<String, Object> map_, String value_) {
        IATMapBase.setValue(map_, IATTrade.UNDERLYING, value_, true);
    }

    static String getUnderlying(Map<String, Object> map_) {
        var value = IATMapBase.getString(map_, IATTrade.UNDERLYING);
        return value;
    }

    static void setExp6(Map<String, Object> map_, Integer value_) {
        IATMapBase.setValue(map_, IATAssetConstants.EXPIRATION_DATE_6, value_, true);
    }

    static Integer getExp6(Map<String, Object> map_) {
        var value = IATMapBase.getInt(map_, IATAssetConstants.EXPIRATION_DATE_6);
        return value;
    }

    static void setTsLast(Map<String, Object> map_, Long value_) {
        IATMapBase.setValue(map_, IATAssetConstants.TS_LAST, value_, true);
    }

    static Long getTsLast(Map<String, Object> map_) {
        var value = IATMapBase.getLong(map_, IATAssetConstants.TS_LAST);
        return value;
    }

}
