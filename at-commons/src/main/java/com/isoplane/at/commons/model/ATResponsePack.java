package com.isoplane.at.commons.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATOptionExpiration.ATOptionExpirationDate;
import com.isoplane.at.commons.model.ATStrategy.ATStrategyGroup;

public abstract class ATResponsePack {

	private Serializable meta;
	private Map<String, ATFundamental> fundamentalMap;
	private Map<String, ATEquityDescription> descriptionMap;
	private Map<String, ATMarketData> marketMap;

	public ATResponsePack() {
	}

	public ATResponsePack(Map<String, ATEquityDescription> descriptions_, Map<String, ATFundamental> fundamentals_,
			Map<String, ATMarketData> marketData_) {
		this.descriptionMap = descriptions_;
		this.fundamentalMap = fundamentals_;
		this.marketMap = marketData_;
	}

	public void merge(ATResponsePack other_) {
		if (other_ == null)
			return;
		if (other_.descriptionMap != null) {
			if (this.descriptionMap == null) {
				this.descriptionMap = new TreeMap<>();
			}
			this.descriptionMap.putAll(other_.descriptionMap);
		}
		if (other_.fundamentalMap != null) {
			if (this.fundamentalMap == null) {
				this.fundamentalMap = new TreeMap<>();
			}
			this.fundamentalMap.putAll(other_.fundamentalMap);
		}
		if (other_.marketMap != null) {
			if (this.marketMap == null) {
				this.marketMap = new TreeMap<>();
			}
			this.marketMap.putAll(other_.marketMap);
		}
	}

	public Map<String, ATFundamental> getFundamentals() {
		return fundamentalMap;
	}

	public Map<String, ATEquityDescription> getDescriptions() {
		return descriptionMap;
	}

	public Map<String, ATMarketData> getMarketMap() {
		return marketMap;
	}

	public Serializable getMeta() {
		return meta;
	}

	public void setMeta(Serializable meta) {
		this.meta = meta;
	}

	public static class ATStrategyPack extends ATResponsePack {

		private List<ATStrategyGroup> strategies;

		public ATStrategyPack(List<ATStrategyGroup> strategies_, Map<String, ATEquityDescription> descriptions_,
				Map<String, ATFundamental> fundamentals_,
				Map<String, ATMarketData> marketData_) {
			super(descriptions_, fundamentals_, marketData_);
			this.strategies = strategies_;
		}

		public void merge(ATStrategyPack other_) {
			if (other_ == null)
				return;
			super.merge(other_);
			if (other_.strategies != null) {
				if (this.strategies == null) {
					this.strategies = new ArrayList<>();
				}
				this.strategies.addAll(other_.strategies);
			}
		}

		public List<ATStrategyGroup> getStrategies() {
			return strategies;
		}

	}

	// public static class ATTradePack extends ATResponsePack {
	//
	// private List<ATTrade> trades;
	//
	// public ATTradePack(List<ATTrade> trades_, Map<String, ATEquityDescription> descriptions_, Map<String, ATFundamental> fundamentals_,
	// Map<String, ATMarketData> marketData_) {
	// super(descriptions_, fundamentals_, marketData_);
	// this.trades = trades_;
	// }
	//
	// public List<ATTrade> getTrades() {
	// return trades;
	// }
	//
	// }

	public static class ATSecurityPack extends ATResponsePack {

		private Map<String, Double> cumulativeMap;
		private Map<String, List<ATHistoryData>> historyMap;
		private Map<String, List<ATTradeDigest>> digestMap;
		private Map<String, List<ATTradeGroup>> strategyMap;
		private Map<String, List<ATTrade>> tradeMap;
		private Map<String, List<ATTrade>> tradeLedgerMap;
		private Map<String, List<ATTradeTraceGroup>> tradeTraceMap;
		private Map<String, List<ATPortfolio>> portfolioMap;
		private Map<String, List<ATOptionExpirationDate>> optionExpirationMap;
		@Deprecated
		private Map<String, Map<String, Set<ATMarketData>>> callMap;
		@Deprecated
		private Map<String, Map<String, Set<ATMarketData>>> putMap;
		private Map<String, Map<String, List<ATOpportunity2>>> opportunityMap;

		private TreeMap<String, TreeMap<String, TreeSet<String>>> callChain;
		private TreeMap<String, TreeMap<String, TreeSet<String>>> putChain;
		private List<ATPosition> positions;

		public ATSecurityPack(Map<String, List<ATHistoryData>> history_) {
			super();
			this.historyMap = history_;
		}

		public ATSecurityPack(Map<String, ATEquityDescription> descriptions_, Map<String, ATFundamental> fundamentals_,
				Map<String, ATMarketData> marketData_) {
			super(descriptions_, fundamentals_, marketData_);
		}

		public ATSecurityPack(Map<String, ATEquityDescription> descriptions_, Map<String, ATFundamental> fundamentals_,
				Map<String, ATMarketData> marketData_, Map<String, List<ATPortfolio>> portfolio_) {
			super(descriptions_, fundamentals_, marketData_);
			this.portfolioMap = portfolio_;
		}

		public ATSecurityPack(Map<String, ATEquityDescription> descriptions_, Map<String, ATFundamental> fundamentals_,
				Map<String, ATMarketData> marketData_, Map<String, List<ATHistoryData>> history_,
				Map<String, List<ATTrade>> tradeMap_) {
			super(descriptions_, fundamentals_, marketData_);
			this.historyMap = history_;
			this.tradeMap = tradeMap_;
		}

		public void merge(ATSecurityPack other_) {
			if (other_ == null)
				return;
			super.merge(other_);
			if (other_.historyMap != null) {
				if (this.historyMap == null) {
					this.historyMap = new TreeMap<>();
				}
				this.historyMap.putAll(other_.historyMap);
			}
			if (other_.tradeMap != null) {
				if (this.tradeMap == null) {
					this.tradeMap = new TreeMap<>();
				}
				this.tradeMap.putAll(other_.tradeMap);
			}
			if (other_.portfolioMap != null) {
				if (this.portfolioMap == null) {
					this.portfolioMap = new TreeMap<>();
				}
				this.portfolioMap.putAll(other_.portfolioMap);
			}
			if (other_.optionExpirationMap != null) {
				if (this.optionExpirationMap == null) {
					this.optionExpirationMap = new TreeMap<>();
				}
				this.optionExpirationMap.putAll(other_.optionExpirationMap);
			}
			if (other_.digestMap != null) {
				if (this.digestMap == null) {
					this.digestMap = new TreeMap<>();
				}
				this.digestMap.putAll(other_.digestMap);
			}
			if (other_.strategyMap != null) {
				if (this.strategyMap == null) {
					this.strategyMap = new TreeMap<>();
				}
				this.strategyMap.putAll(other_.strategyMap);
			}
			if (other_.tradeLedgerMap != null) {
				if (this.tradeLedgerMap == null) {
					this.tradeLedgerMap = new TreeMap<>();
				}
				this.tradeLedgerMap.putAll(other_.tradeLedgerMap);
			}
			if (other_.callMap != null) {
				if (this.callMap == null) {
					this.callMap = new TreeMap<>();
				}
				Map<String, Map<String, Set<ATMarketData>>> tempMap = new HashMap<>(other_.callMap);
				for (Entry<String, Map<String, Set<ATMarketData>>> entry : this.callMap.entrySet()) {
					Map<String, Set<ATMarketData>> otherMap = tempMap.remove((entry.getKey()));
					if (otherMap != null) {
						entry.getValue().putAll(otherMap);
					}
				}
				this.callMap.putAll(tempMap);
			}
			if (other_.putMap != null) {
				if (this.putMap == null) {
					this.putMap = new TreeMap<>();
				}
				Map<String, Map<String, Set<ATMarketData>>> tempMap = new HashMap<>(other_.putMap);
				for (Entry<String, Map<String, Set<ATMarketData>>> entry : this.putMap.entrySet()) {
					Map<String, Set<ATMarketData>> otherMap = tempMap.remove((entry.getKey()));
					if (otherMap != null) {
						entry.getValue().putAll(otherMap);
					}
				}
				this.putMap.putAll(tempMap);
			}
			if (other_.opportunityMap != null) {
				if (this.opportunityMap == null) {
					this.opportunityMap = new TreeMap<>();
				}
				Map<String, Map<String, List<ATOpportunity2>>> tempMap = new HashMap<>(other_.opportunityMap);
				for (Entry<String, Map<String, List<ATOpportunity2>>> entry : this.opportunityMap.entrySet()) {
					Map<String, List<ATOpportunity2>> otherMap = tempMap.remove((entry.getKey()));
					if (otherMap != null) {
						entry.getValue().putAll(otherMap);
					}
				}
				this.opportunityMap.putAll(tempMap);
			}
			if (other_.positions != null) {
				if (this.positions == null) {
					this.positions = new ArrayList<>();
				}
				this.positions.addAll(other_.positions);
			}
		}

		public Map<String, List<ATTradeDigest>> getDigest() {
			return this.digestMap;
		}

		public void setDigest(Map<String, List<ATTradeDigest>> digest_) {
			this.digestMap = digest_;
		}

		public Map<String, List<ATTradeGroup>> getStrategy() {
			return this.strategyMap;
		}

		public void setStrategy(Map<String, List<ATTradeGroup>> value_) {
			this.strategyMap = value_;
		}

		public Map<String, List<ATHistoryData>> getHistory() {
			return historyMap;
		}

		public void setHistory(Map<String, List<ATHistoryData>> value_) {
			this.historyMap = value_;
		}

		public Map<String, List<ATTrade>> getTrades() {
			return tradeMap;
		}

		public void setTrades(Map<String, List<ATTrade>> value_) {
			this.tradeMap = value_;
		}

		public Map<String, List<ATTrade>> getTradeLedger() {
			return tradeLedgerMap;
		}

		public void setTradeLedger(Map<String, List<ATTrade>> value_) {
			this.tradeLedgerMap = value_;
		}

		public Map<String, List<ATTradeTraceGroup>> getTradeTrace() {
			return tradeTraceMap;
		}

		public void setTradeTrace(Map<String, List<ATTradeTraceGroup>> value_) {
			this.tradeTraceMap = value_;
		}

		public void setPortfolio(Map<String, List<ATPortfolio>> value_) {
			this.portfolioMap = value_;
		}

		public Map<String, List<ATPortfolio>> getPortfolio() {
			return portfolioMap;
		}

		public List<ATPosition> getPositions() {
			return positions;
		}

		public void setPositions(List<ATPosition> positions) {
			this.positions = positions;
		}

		public void setCumulative(Map<String, Double> map_) {
			this.cumulativeMap = map_;
		}

		public Map<String, Double> getCumulative() {
			return this.cumulativeMap;
		}

		@Deprecated
		public Map<String, Map<String, Set<ATMarketData>>> getCalls() {
			return callMap;
		}

		@Deprecated
		public void setCalls(Map<String, Map<String, Set<ATMarketData>>> value_) {
			callMap = value_;
		}

		@Deprecated
		public Map<String, Map<String, Set<ATMarketData>>> getPuts() {
			return putMap;
		}

		@Deprecated
		public void setPuts(Map<String, Map<String, Set<ATMarketData>>> value_) {
			putMap = value_;
		}

		public void setOptionExpirations(Map<String, List<ATOptionExpirationDate>> value_) {
			this.optionExpirationMap = value_;
		}

		public Map<String, List<ATOptionExpirationDate>> getOptionExpirations() {
			return optionExpirationMap;
		}

		public void setOpportunities(Map<String, Map<String, List<ATOpportunity2>>> value_) {
			this.opportunityMap = value_;
		}

		public Map<String, Map<String, List<ATOpportunity2>>> getOpportunities() {
			return opportunityMap;
		}

		public TreeMap<String, TreeMap<String, TreeSet<String>>> getCallChain() {
			return callChain;
		}

		public void setCallChain(TreeMap<String, TreeMap<String, TreeSet<String>>> callChain) {
			this.callChain = callChain;
		}

		public TreeMap<String, TreeMap<String, TreeSet<String>>> getPutChain() {
			return putChain;
		}

		public void setPutChain(TreeMap<String, TreeMap<String, TreeSet<String>>> putChain) {
			this.putChain = putChain;
		}

	}

	// public static class ATDashboardPack extends ATResponsePack {
	// private Map<String, List<ATMarketData>> indexMap;
	// private Map<String, List<ATMarketData>> moverMap;
	// private Map<String, List<ATMarketData>> sectorMap;
	// private Map<String, List<ATMarketData>> trendMap;
	//
	// private Map<String, List<ATMarketData>> userCustomMap;
	// private Map<String, List<ATMarketData>> userMoverMap;
	//
	//
	// }

}