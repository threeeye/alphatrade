package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.List;

public class ATHistoryData extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public String getDateStr() {
		return IATAsset.getString(this, IATAssetConstants.DATE_STR);
	}

	public void setDateStr(String value_) {
		put(IATAssetConstants.DATE_STR, value_);
	}

	public Double getClose() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_CLOSE);
	}

	public void setClose(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_CLOSE);
		} else {
			put(IATAssetConstants.PX_CLOSE, value_);
		}
	}

	public Double getHigh() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_HIGH);
	}

	public void setHigh(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_HIGH);
		} else {
			put(IATAssetConstants.PX_HIGH, value_);
		}
	}

	public Double getLow() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_LOW);
	}

	public void setLow(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_LOW);
		} else {
			put(IATAssetConstants.PX_LOW, value_);
		}
	}

	public Double getOpen() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_OPEN);
	}

	public void setOpen(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_OPEN);
		} else {
			put(IATAssetConstants.PX_OPEN, value_);
		}
	}

	public Long getVolume() {
		return IATAsset.getLong(this, IATAssetConstants.VOLUME_DAILY);
	}

	public void setVolume(Long value_) {
		if (value_ == null) {
			remove(IATAssetConstants.VOLUME_DAILY);
		} else {
			put(IATAssetConstants.VOLUME_DAILY, value_);
		}
	}

	public Double getHV() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_HV);
	}

	public void setHV(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_HV);
		} else {
			put(IATAssetConstants.PX_HV, value_);
		}
	}

	public Double getMax() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_MAX);
	}

	public void setMax(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_MAX);
		} else {
			put(IATAssetConstants.PX_MAX, value_);
		}
	}

	public Double getMin() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_MIN);
	}

	public void setMin(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_MIN);
		} else {
			put(IATAssetConstants.PX_MIN, value_);
		}
	}

	public Double getAvg() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_AVG);
	}

	public void setAvg(Double value_) {
		if (value_ == null) {
			remove(IATAssetConstants.PX_AVG);
		} else {
			put(IATAssetConstants.PX_AVG, value_);
		}
	}

	public static class ATHistoryDataWrapper {

		private String occId;
		private List<ATHistoryData> history;

		public String getOccId() {
			return occId;
		}

		public void setOccId(String occId) {
			this.occId = occId;
		}

		public List<ATHistoryData> getHistory() {
			return history;
		}

		public void setHistory(List<ATHistoryData> history) {
			this.history = history;
		}
	}

}
