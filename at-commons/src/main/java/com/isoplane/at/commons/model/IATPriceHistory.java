package com.isoplane.at.commons.model;

public interface IATPriceHistory {

	static public final String CLOSE = "pxC";
	static public final String HIGH = "pxH";
	static public final String LOW = "pxL";
	static public final String OPEN = "pxO";
	static public final String VOLUME = "pxV";

	String getOccId();

	String getDateStr();

	Double getClose();

	Double getHigh();

	Double getLow();

	Double getOpen();

	Long getVolume();
}
