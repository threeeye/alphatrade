package com.isoplane.at.commons.service;

import java.util.Map;

public interface IATProtoEventbusSubscriber {

	final String TYPE_SESSION_OPEN = "ssn_open";
	final String TYPE_SESSION_CLOSE = "ssn_close";

	void notifyEventbus(String source, String target, String type, Long timestamp, Map<String, String> data);

}
