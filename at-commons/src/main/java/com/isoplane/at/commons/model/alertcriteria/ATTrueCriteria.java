package com.isoplane.at.commons.model.alertcriteria;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;

public class ATTrueCriteria extends ATBaseCriteria {

	static public ATTrueCriteria create(String occId_) {
		if (StringUtils.isBlank(occId_))
			return null;
		String criteria = "➜ true";
		ATTrueCriteria crit = new ATTrueCriteria(occId_, criteria);
		return crit;
	}

	private ATTrueCriteria(String occId_, String criteria_) {
		super(null, occId_, criteria_);
	}

	@Override
	@Deprecated
	public Boolean test(ATSecurity sec_) {
		if (sec_ == null || !getOccId().equals(sec_.getOccId()))
			return false;
		return true;
	}

	@Override
	public Boolean test(IATMarketData data_, IATStatistics stats_) {
		if (data_ == null || !isOccIdMatch(data_.getAtId()))
			return false;
		return true;
	}

	@Override
	@Deprecated
	public Boolean test(ATPersistentSecurity sec_) {
		if (sec_ == null || !getOccId().equals(sec_.getOccId()))
			return false;
		return true;
	}

	@Override
	@Deprecated
	public String label(ATPersistentSecurity sec_) {
		return label();
	}

	@Override
	public String label(IATMarketData data_, IATStatistics stats) {
		return label();
	}

}
