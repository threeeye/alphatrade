package com.isoplane.at.commons.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATLookupStore;

public class ATPersistentConfiguration implements IATConfiguration {

	static final Logger log = LoggerFactory.getLogger(ATPersistentConfiguration.class);

	private String _table;
	private String _configId;
	private IATLookupStore _store;
	private Timer _timer;
	private IATLookupMap _config;
	private List<IATConfigurationListener> _listeners;
	static private Map<String, ATPersistentConfiguration> _instances;

	static public IATConfiguration getConfiguration(IATLookupStore store_, String table_, String configId_) {
		if (_instances == null) {
			_instances = new HashMap<>();
		}
		String key = String.format("%s%s%s", store_.getDbId().trim(), table_.trim(), configId_.trim());
		ATPersistentConfiguration config = _instances.get(key);
		if (config == null) {
			config = new ATPersistentConfiguration(store_, table_, configId_);
			_instances.put(key, config);
		}
		return config;
	}

	private Object getValue(String key_) {
		if (_config == null) {
			log.debug(String.format("Config is null for key [%s]", key_));
			return null;
		} else {
			Object value = _config.get(key_);
			if (value == null) {
				log.debug(String.format("Value not found [%s]", key_));
			}
			return value;
		}
	}

	private Object getValue(String key_, Object fallback_) {
		if (_config == null) {
			log.debug(String.format("Config is null for key [%s], falling back to [%s]", key_, fallback_));
			return fallback_;
		} else {
			Object value = _config.get(key_);
			if (value == null) {
				log.debug(String.format("Value not found [%s], falling back to [%s]", key_, fallback_));
				return fallback_;
			} else {
				return value;
			}
		}
	}

	private ATPersistentConfiguration(IATLookupStore store_, String table_, String configId_) {
		_store = store_;
		_table = table_;
		_configId = configId_;
		_listeners = new ArrayList<>();
		_config = _store.get(_table, _configId);
		setupTimer();
		log.info(String.format("Configuration active [%s/%s]", table_, configId_));
	}

	@Override
	public void printConfiguration() {
		if (_config == null) {
			log.info(String.format("Configuration [%s] not initialized", _configId));
			return;
		}
		log.info(String.format("Configuration [%s]:", _configId));
		for (String key : _config.keySet()) {
			log.info(String.format("%-24s: %s", key, _config.get(key)));
		}
	}

	@Override
	public void registerListener(IATConfigurationListener listener_) {
		if (_listeners.contains(listener_)) {
			return;
		}
		_listeners.add(listener_);
	}

	@Override
	public Boolean getBoolean(String key_) {
		Boolean value = (Boolean) getValue(key_);
		return value;
	}

	@Override
	public boolean getBoolean(String key_, boolean default_) {
		Boolean value = (Boolean) getValue(key_, default_);
		return value;
	}

	@Override
	public Double getDouble(String key_) {
		Double value = (Double) getValue(key_);
		return value;
	}

	@Override
	public double getDouble(String key_, double default_) {
		Double value = (Double) getValue(key_, default_);
		return value;
	}

	@Override
	public Integer getInt(String key_) {
		Integer value = (Integer) getValue(key_);
		return value;
	}

	@Override
	public int getInt(String key_, int default_) {
		Integer value = (Integer) getValue(key_, default_);
		return value;
	}

	@Override
	public Long getLong(String key_) {
		Long value = (Long) getValue(key_);
		return value;
	}

	@Override
	public long getLong(String key_, long default_) {
		Long value = (Long) getValue(key_, default_);
		return value;
	}

	@Override
	public String getString(String key_) {
		String value = (String) getValue(key_);
		return value;
	}

	@Override
	public String getString(String key_, String default_) {
		String value = (String) getValue(key_, default_);
		return value;
	}

	public void shutdown() {
		if (_timer != null) {
			_timer.cancel();
		}
	}

	private void setupTimer() {
		_timer = new Timer(String.format("%s-%s", ATPersistentConfiguration.class.getSimpleName(), _configId), false);
		long period = 30000;
		_timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					IATLookupMap newConfig = _store.get(_table, _configId);
					Set<String> changedKeys = new HashSet<>();
					for (String key : newConfig.keySet()) {
						Object newValue = newConfig.get(key);
						Object oldValue = _config.get(key);
						if (!newValue.equals(oldValue)) {
							log.debug(String.format("Change detected [%s/%s.%s: %s -> %s]", _table, _configId, key, oldValue, newValue));
							changedKeys.add(key);
						}
					}
					for (String key : _config.keySet()) {
						if (changedKeys.contains(key))
							continue;
						Object oldValue = _config.get(key);
						Object newValue = newConfig.get(key);
						if (!oldValue.equals(newValue)) {
							log.debug(String.format("Change detected [%s/%s.%s: %s -> %s]", _table, _configId, key, oldValue, newValue));
							changedKeys.add(key);
						}
					}
					_config = newConfig;
					if (changedKeys.isEmpty())
						return;
					for (IATConfigurationListener listener : _listeners) {
						try {
							listener.configurationAlert(changedKeys);
						} catch (Exception ex) {
							log.error(String.format("Error notifying listener [%s]: %s", listener, changedKeys), ex);
						}
					}
				} catch (Exception ex) {
					log.error(String.format("Error reloading configuration [%s/%s]", _table, _configId), ex);
				}
			}
		}, period, period);
	}
}
