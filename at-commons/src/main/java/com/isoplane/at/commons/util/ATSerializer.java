package com.isoplane.at.commons.util;

import java.util.Arrays;
import java.util.List;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATSerializer {

	static final private List<String> MONGO_EXCLUDES = Arrays.asList(IATLookupMap.INSERT_TIME, IATLookupMap.UPDATE_TIME, "_id");

	static final private Gson _defaultGson = new Gson();
	static final private Gson _mongoGson = new GsonBuilder().addSerializationExclusionStrategy(new MongoExclusionStrategy()).create();

	static public String toJson(Object obj_, boolean excludeMongo_) {
		if (excludeMongo_) {
			return _mongoGson.toJson(obj_);
		} else {
			return _defaultGson.toJson(obj_);
		}
	}

	public static class MongoExclusionStrategy implements ExclusionStrategy {

		@Override
		public boolean shouldSkipField(FieldAttributes f_) {
			if (!IATLookupMap.class.isAssignableFrom(f_.getDeclaredClass()))
				return false;
			String field = f_.getName();
			boolean isExclude = MONGO_EXCLUDES.contains(field);
			return isExclude;
		}

		@Override
		public boolean shouldSkipClass(Class<?> clazz_) {
			return false;
		}

	}
}
