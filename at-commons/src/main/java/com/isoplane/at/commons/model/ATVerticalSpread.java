package com.isoplane.at.commons.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.util.ATFormats;

public class ATVerticalSpread implements Serializable, Comparable<ATVerticalSpread> {

	private static final long serialVersionUID = 1L;

	private String occIdStock;
	private String occIdBuy;
	private String occIdSell;
	private Date expirationDate;
	private String right;
	private double strikeBuy;
	private double strikeSell;
	private double priceSpread;
	private double priceStock;
	private double pxBuyAsk;
	private double pxBuyBid;
	private Double pxBuyLoD;
	private Double pxBuyHiD;
	private Double pxBuyLast;
	private double pxSellAsk;
	private double pxSellBid;
	private Double pxSellLoD;
	private Double pxSellHiD;
	private Double pxSellLast;
	private double apy;
	private double safetyMargin;
	private double probability;
	private double score;
	private String flags;
	private Map<String, Object> meta = new HashMap<>();
//	private String meta;

	public String getOccIdBuy() {
		return occIdBuy;
	}

	public void setOccIdBuy(String occIdBuy) {
		this.occIdBuy = occIdBuy;
	}

	public String getOccIdSell() {
		return occIdSell;
	}

	public void setOccIdSell(String occIdSell) {
		this.occIdSell = occIdSell;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getRight() {
		return right;
	}

	public void setRight(String right) {
		this.right = right;
	}

	public double getStrikeBuy() {
		return strikeBuy;
	}

	public void setStrikeBuy(double strikeBuy) {
		this.strikeBuy = strikeBuy;
	}

	public double getStrikeSell() {
		return strikeSell;
	}

	public void setStrikeSell(double strikeSell) {
		this.strikeSell = strikeSell;
	}

	public double getPriceSpread() {
		return priceSpread;
	}

	public void setPriceSpread(double price) {
		this.priceSpread = price;
	}

	public String getOccIdStock() {
		return occIdStock;
	}

	public void setOccIdStock(String occIdStock) {
		this.occIdStock = occIdStock;
	}

	public double getApy() {
		return apy;
	}

	public void setApy(double apy) {
		this.apy = apy;
	}

	public double getSafetyMargin() {
		return safetyMargin;
	}

	public void setSafetyMargin(double safetyMargin) {
		this.safetyMargin = safetyMargin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((occIdBuy == null) ? 0 : occIdBuy.hashCode());
		result = prime * result + ((occIdSell == null) ? 0 : occIdSell.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ATVerticalSpread other = (ATVerticalSpread) obj;
		if (occIdBuy == null) {
			if (other.occIdBuy != null)
				return false;
		} else if (!occIdBuy.equals(other.occIdBuy))
			return false;
		if (occIdSell == null) {
			if (other.occIdSell != null)
				return false;
		} else if (!occIdSell.equals(other.occIdSell))
			return false;
		return true;
	}

	@Override
	public int compareTo(ATVerticalSpread other_) {
		if (this == other_)
			return 0;
		if (other_ == null || other_.getOccIdBuy() == null || other_.getOccIdSell() == null)
			return -1;
		if (occIdBuy == null || occIdSell == null)
			return 1;
		return (occIdSell + occIdBuy).compareTo(other_.getOccIdSell() + other_.getOccIdBuy());
	}

	@Override
	public String toString() {
		String str = String.format("Spread: %-6s%s [%s %7.2f/%-7.2f(%5.2f) %6.2f, %.2f]", getOccIdStock(),
				ATFormats.DATE_yyyyMMdd.get().format(getExpirationDate()), getRight(), getStrikeBuy(), getStrikeSell(), getStrikeSell() - getStrikeBuy(),
				getPriceSpread(), getApy() * 100.0);
		return str;
	}

	public double getPriceStock() {
		return priceStock;
	}

	public void setPriceStock(double priceStock) {
		this.priceStock = priceStock;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public Map<String, Object> getMeta() {
		return meta;
	}

	public void setMeta(Map<String, Object> meta) {
		this.meta = meta;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public double getProbability() {
		return probability;
	}

	public void setProbability(double probability) {
		this.probability = probability;
	}

	public double getPxBuyAsk() {
		return pxBuyAsk;
	}

	public void setPxBuyAsk(double pxBuyAsk) {
		this.pxBuyAsk = pxBuyAsk;
	}

	public double getPxBuyBid() {
		return pxBuyBid;
	}

	public void setPxBuyBid(double pxBuyBid) {
		this.pxBuyBid = pxBuyBid;
	}

	public double getPxSellAsk() {
		return pxSellAsk;
	}

	public void setPxSellAsk(double pxSellAsk) {
		this.pxSellAsk = pxSellAsk;
	}

	public double getPxSellBid() {
		return pxSellBid;
	}

	public void setPxSellBid(double pxSellBid) {
		this.pxSellBid = pxSellBid;
	}

	public Double getPxBuyLast() {
		return pxBuyLast;
	}

	public void setPxBuyLast(Double pxBuyLast) {
		this.pxBuyLast = pxBuyLast;
	}

	public Double getPxSellLast() {
		return pxSellLast;
	}

	public void setPxSellLast(Double pxSellLast) {
		this.pxSellLast = pxSellLast;
	}

	public Double getPxBuyLoD() {
		return pxBuyLoD;
	}

	public void setPxBuyLoD(Double pxBuyLoD) {
		this.pxBuyLoD = pxBuyLoD;
	}

	public Double getPxBuyHiD() {
		return pxBuyHiD;
	}

	public void setPxBuyHiD(Double pxBuyHiD) {
		this.pxBuyHiD = pxBuyHiD;
	}

	public Double getPxSellLoD() {
		return pxSellLoD;
	}

	public void setPxSellLoD(Double pxSellLoD) {
		this.pxSellLoD = pxSellLoD;
	}

	public Double getPxSellHiD() {
		return pxSellHiD;
	}

	public void setPxSellHiD(Double pxSellHiD) {
		this.pxSellHiD = pxSellHiD;
	}

}
