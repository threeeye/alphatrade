package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;

public interface IATProtoPushNotificationSubscriber {

	void notifyPushNotification(PushNotification msg);

}
