package com.isoplane.at.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ATFormats {

	static final Logger log = LoggerFactory.getLogger(ATFormats.class);
	static final Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();

	static final DateTimeFormatter _formatter_yyyyMMdd = DateTimeFormatter.ofPattern("yyyyMMdd");

	static public String formatYYYYMMDD(LocalDate date_) {
		if (date_ == null)
			return null;
		String str = date_.format(_formatter_yyyyMMdd);
		return str;
	}

	public static String serialize(Object obj_) {
		if (obj_ == null)
			return null;
		try {
			String str = gson.toJson(obj_);
			return str;
		} catch (Exception ex) {
			log.error("Failed to serialize: " + obj_, ex);
			return null;
		}
	}

	public static final ThreadLocal<SimpleDateFormat> DATE_MMdd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("MMdd");
		}
	};

	// public static String date_MMdd(Date date_) {
	// String str = DATE_MMdd.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_MM_dd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("MM-dd");
		}
	};

	// public static String date_MM_dd(Date date_) {
	// String str = DATE_MM_dd.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_yyMMdd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyMMdd");
		}
	};

	// public static String date_yyMMdd(Date date_) {
	// String str = DATE_yyMMdd.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_yyyyMMdd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMdd");
		}
	};

	// public static String date_yyyyMMdd(Date date_) {
	// String str = DATE_yyyyMMdd.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_yyyy_MM_dd = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd");
		}
	};

	// public static String date_yyyy_MM_dd(Date date_) {
	// String str = DATE_yyyy_MM_dd.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_full = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
		}
	};

	// public static String date_full(Date date_) {
	// String str = DATE_full.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_compact = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyMMdd HH:mm:ss");
		}
	};

	// public static String date_compact(Date date_) {
	// String str = DATE_compact.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> DATE_json = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		}
	};

	public static final ThreadLocal<SimpleDateFormat> DATE_TIME_mid = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		}
	};

	// public static String date_json(Date date_) {
	// String str = DATE_json.get().format(date_);
	// return str;
	// }

	public static final ThreadLocal<SimpleDateFormat> TIME_HH_mm_ss = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("HH:mm:ss");
		}
	};

	// public static String time_HH_mm_ss(Date date_) {
	// String str = TIME_HH_mm_ss.get().format(date_);
	// return str;
	// }

	static public Long dateToNumber(Date date_) {
		if (date_ == null)
			return null;
		Calendar c = Calendar.getInstance();
		c.setTime(date_);
		long number = c.get(Calendar.YEAR) * 10000000000000L + (c.get(Calendar.MONTH) + 1) * 100000000000L
				+ c.get(Calendar.DAY_OF_MONTH) * 1000000000L + c.get(Calendar.HOUR_OF_DAY) * 10000000L + c.get(Calendar.MINUTE) * 100000L
				+ c.get(Calendar.SECOND) * 1000L + c.get(Calendar.MILLISECOND);
		return number;
	}

	static public Long dateToNumber(LocalDateTime date_) {
		long number = date_.getYear() * 10000000000000L + date_.getMonthValue() * 100000000000L
				+ date_.getDayOfMonth() * 1000000000L + date_.getHour() * 10000000L + date_.getMinute() * 100000L
				+ date_.getSecond() * 1000L + date_.get(ChronoField.MILLI_OF_SECOND);
		return number;
	}

	static public Long dateToNumber(LocalDate date_) {
		long number = date_.getYear() * 10000000000000L + date_.getMonthValue() * 100000000000L
				+ date_.getDayOfMonth() * 1000000000L;
		return number;
	}

	static public String ds8Tods10(String ds8_) {
		String str = new StringBuilder(ds8_).insert(6, '-').insert(4, '-').toString();
		return str;
	}

	static public String ds10Tods8(String ds10_) {
		String str = ds10_.replace("-", "");
		return str;
	}

	/**
	 * 
	 * @param ds6
	 *            ds6 formatted Date
	 * @param century
	 *            two-digit centory either as String or Integer/Long
	 * @return ds8 formatted date
	 */
	static public String ds6Tods8(String ds6, Object century) {
		if (century == null) {
			int year = Year.now().getValue() / 100;
			String ds8 = year + ds6;
			return ds8;
		}
		return century + ds6;
	}

	/**
	 * 
	 * @param ds6
	 *            ds6 formatted Date
	 * @param century
	 *            two-digit centory either as String or Integer/Long
	 * @return ds10 formatted date
	 */
	static public String ds6Tods10(String ds6, Object century) {
		String ds8 = ds6Tods8(ds6, century);
		return ds8Tods10(ds8);
	}

	static public Date numberToDate(Long num_) {
		if (num_ == null)
			return null;
		int ms = (int) (num_ % 1000L);
		num_ /= 1000L;
		int sec = (int) (num_ % 100);
		num_ /= 100L;
		int min = (int) (num_ % 100);
		num_ /= 100L;
		int hour = (int) (num_ % 100);
		num_ /= 100L;
		int day = (int) (num_ % 100);
		num_ /= 100L;
		int month = (int) (num_ % 100);
		num_ /= 100L;
		int year = num_.intValue();
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, ms);
		c.set(Calendar.SECOND, sec);
		c.set(Calendar.MINUTE, min);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.DAY_OF_MONTH, day);
		c.set(Calendar.MONTH, month - 1);
		c.set(Calendar.YEAR, year);
		return c.getTime();
	}

	// https://en.wikipedia.org/wiki/Option_symbol
	static public String normalizeOccId(String symbol_) {
		if (StringUtils.isBlank(symbol_))
			return null;
		String symStr = symbol_.replace(" ", "").trim();
		if (symStr.length() < 9)
			return symStr;
		int i = 0;
		while (i < symStr.length() && !Character.isDigit(symStr.charAt(i)))
			i++;
		String symbol = symStr.substring(0, i).trim();
		symStr = symStr.substring(i);
		i = 0;
		while (i < symStr.length() && Character.isDigit(symStr.charAt(i)))
			i++;
		String dateStr = symStr.substring(0, i);
		String optionRight = Character.toString(symStr.charAt(i));
		String strikeStr = symStr.substring(i + 1);
		Double strike = Double.parseDouble(strikeStr);
		String occId = toOccId(symbol, dateStr, optionRight, strike);
		return occId;
	}

	static public String toSymbol(String occId_) {
		if (occId_ == null || occId_.length() <= 6)
			return occId_;
		String underlyingId = occId_.substring(0, 6).trim();
		return underlyingId;
	}

	static public String toOccId(String symbol_, String dateStr_, String optionRight_, Double strike_) {
		if (StringUtils.isBlank(symbol_) || StringUtils.isBlank(dateStr_))
			return null;
		Date date = toDate(dateStr_);
		String result = toOccId(symbol_, date, optionRight_, strike_);
		return result;
	}

	static public String toOccId(String symbol_, Date date_, String optionRight_, Double strike_) {
		if (StringUtils.isBlank(symbol_) || date_ == null || StringUtils.isBlank(optionRight_) || strike_ == null)
			return null;
		String rightStr = optionRight_.trim().toUpperCase();
		if (!("C".equals(rightStr) || "P".equals(rightStr) || "_".equals(rightStr))) {
			log.error(String.format("Invalid option right [%s]", rightStr));
			return null;
		}
		String dateStr = DATE_yyMMdd.get().format(date_);
		int strikeInt = (int) (strike_ * 1000);

		String result = String.format("%-6s%s%s%08d", symbol_.trim(), dateStr, rightStr, strikeInt);
		return result;
	}

	static public String toLabel(String symbol_, Date date_, String optionRight_, Double strike_) {
		String dstr = DATE_MMdd.get().format(date_);
		if (dstr != null && optionRight_ != null && strike_ != null) {
			String label = String.format("%-5s%s %s%7.2f", symbol_, dstr, optionRight_, strike_);
			return label;
		}
		return null;
	}

	static public String toLabel(String occId_) {
		try {
			if (occId_.length() <= 6) {
				return occId_.trim();
			}
			String symbol = occId_.substring(0, 6).trim();
			String expStr = occId_.substring(6, 12);
			String right = occId_.substring(12, 13);
			String strikeStr = occId_.substring(13);
			Date exp = ATFormats.DATE_yyMMdd.get().parse(expStr);
			Long strikeL = Long.parseLong(strikeStr);
			Double strike = strikeL / 1000.0;
			String label = toLabel(symbol, exp, right, strike);
			return label;
		} catch (Exception ex) {
			log.error(String.format("Error generating label [%s]", occId_), ex);
			return null;
		}
	}

	static private Date toDate(String dateStr_) {
		try {
			String dateStr = dateStr_.replaceAll("-", "").trim();
			Date date = null;
			if (dateStr.length() == 6) {
				date = DATE_yyMMdd.get().parse(dateStr);
			} else if (dateStr.length() == 8) {
				date = DATE_yyyyMMdd.get().parse(dateStr);
			}
			return date;
		} catch (ParseException e) {
			log.error(String.format("Invalid date [%s]", dateStr_));
			return null;
		}

	}

}
