package com.isoplane.at.commons;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class ATException extends RuntimeException {

	private static final long serialVersionUID = 20140719181500L;

	public static final int ERROR_UNDEFINED = -1;
	public static final int ERROR_ENTITY_EXISTS = -800;
	public static final int ERROR_ENTITY_UNKNOWN = -850;
	public static final int ERROR_PASSWORD_INVALID = -900;
	public static final int ERROR_PASSWORD_WEAK = -950;
	public static final int ERROR_TIMEOUT = -400;
	public static final int ERROR_TOKEN_EXPIRED = -300;
	public static final int ERROR_NUMERIC_SYMBOL = -51000;
	public static final int ERROR_TRADE_TYPE = -52000;

	public static final Map<Integer, String> MESSAGES = new HashMap<>();
	static {
		MESSAGES.put(ERROR_ENTITY_EXISTS, "entity_exists");
		MESSAGES.put(ERROR_ENTITY_UNKNOWN, "entity_unknown");
		MESSAGES.put(ERROR_PASSWORD_INVALID, "password_invalid");
		MESSAGES.put(ERROR_PASSWORD_INVALID, "password_weak");
		MESSAGES.put(ERROR_TIMEOUT, "timeout");
		MESSAGES.put(ERROR_TOKEN_EXPIRED, "token_expired");
	}

	private Integer errorId;

	public ATException() {
	}

	public ATException(String message) {
		super(message);
	}

	public ATException(String message, int id) {
		super(message);
		errorId = id;
	}

	public ATException(Throwable cause) {
		super(cause);
	}

	public ATException(Throwable cause, int id) {
		super(cause);
		errorId = id;
	}

	public ATException(String message, Throwable cause) {
		super(message, cause);
	}

	public ATException(String message, int id, Throwable cause) {
		super(message, cause);
		errorId = id;
	}

	public ATException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public String toStackTraceString() {
		if (this.getCause() == null)
			return this.getMessage();

		Writer result = new StringWriter();
		PrintWriter printWriter = new PrintWriter(result);
		this.getCause().printStackTrace(printWriter);
		String msg = this.getMessage() + ": " + result.toString();
		return msg;
	}

	public int getErrorId() {
		return errorId != null ? errorId : ERROR_UNDEFINED;
	}

	public void setErrorId(int errorId) {
		this.errorId = errorId;
	}

	public Map<String, Object> getMap() {
		Map<String, Object> errorDoc = new HashMap<>();
		if (errorId != null) {
			errorDoc.put("id", errorId);
			String code = MESSAGES.get(errorId);
			if (StringUtils.isNotBlank(code)) {
				errorDoc.put("code", code);
			}
		}
		if (StringUtils.isNotBlank(getMessage())) {
			errorDoc.put("msg", getMessage());
		}
		if (getStackTrace() != null) {
			errorDoc.put("trace", ExceptionUtils.getStackTrace(this));
		}
		return errorDoc;
	}

}
