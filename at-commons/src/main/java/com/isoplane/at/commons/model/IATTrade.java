package com.isoplane.at.commons.model;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATFormats;

public interface IATTrade extends Comparable<Object>, IATAsset {

	static final String UNDERLYING = "_ul";

	static final String RISK_ABS = "rsk_abs";
	static final String RISK_LOSS = "rsk_loss";

	static final String STRATEGY = "strt";
	static final String STRATEGY_MX = "MX"; // Mixed
	static final String STRATEGY_CC = "CC"; // Covered Call
	static final String STRATEGY_CP = "CP"; // Covered Put
	static final String STRATEGY_LE = "LE"; // Long Equity
	static final String STRATEGY_SE = "SE"; // Short Equity
	static final String STRATEGY_LC = "LC"; // Long Call
	static final String STRATEGY_SC = "SC"; // Short/Naked Call
	static final String STRATEGY_LP = "LP"; // Long Put
	static final String STRATEGY_SP = "SP"; // Short/Naked Put
	static final String STRATEGY_LVCS = "LVCS"; // Long Vertical Call Spread
	static final String STRATEGY_SVCS = "SVCS"; // Short Vertical Call Spread
	static final String STRATEGY_LVPS = "LVPS"; // Long Vertical Put Spread
	static final String STRATEGY_SVPS = "SVPS"; // Short Vertical Put Spread

	static final int SECURITY_BTO = 1000;
	static final int SECURITY_BTO_ASG = 1001;
	static final int SECURITY_BTO_EXEC = 1002;
	static final int SECURITY_STO = 1010;
	static final int SECURITY_STO_ASG = 1011;
	static final int SECURITY_STO_EXEC = 1012;
	static final int SECURITY_BTC = 1020;
	static final int SECURITY_BTC_ASG = 1021;
	static final int SECURITY_BTC_EXEC = 1022;
	static final int SECURITY_SPLIT_C = 1026;
	static final int SECURITY_SPLIT_O = 1027;
	static final int SECURITY_STC = 1030;
	static final int SECURITY_STC_ASG = 1031;
	static final int SECURITY_STC_EXEC = 1032;
	static final int MF_IN = 1100;
	static final int MF_OUT = 1110;
	static final int REINVEST = 1200;
	static final int IN_LEU_OF = 1300;
	static final int CALL_BTO = 2000;
	static final int CALL_STO = 2010;
	static final int CALL_BTC = 2020;
	static final int CALL_STC = 2030;
	static final int CALL_ASG = 2040;
	static final int CALL_EXEC = 2045;
	static final int CALL_EXP = 2050;
	static final int PUT_BTO = 2100;
	static final int PUT_STO = 2110;
	static final int PUT_BTC = 2120;
	static final int PUT_STC = 2130;
	static final int PUT_ASG = 2140;
	static final int PUT_EXEC = 2145;
	static final int PUT_EXP = 2150;

	static final int ASSET_BOUNDARY = 5000;

	static final int DIVIDEND_IN = 5000;
	static final int DIVIDEND_OUT = 5010;
	static final int ADR_FEE = 5020;
	static final int CAP_GAIN_ST = 5100;
	static final int CAP_GAIN_LT = 5110;
	static final int INTEREST_IN = 5200;
	static final int INTEREST_OUT = 5210;
	static final int TAX_FOREIGN = 6000;
	static final int CASH_IN = 9000;
	static final int CASH_OUT = 9010;
	static final int OTHER = 10000;

	enum Type {
		ADR,
		ASG,
		BTC,
		BTO,
		/** Capital Gains Long Term */
		CGLT,
		/** Capital Gains Short Term */
		CGST,
		DIV,
		EXR,
		EXP,
		/** In Lieu Of */
		ILO,
		INT,
		OTH,
		STC,
		/** Foreign Tax */
		TXF,
		/** Re-invest */
		RI,
		STO
	}

	static final Map<Integer, Type> TRADE_TYPE_MAP = new HashMap<Integer, Type>() {
		private static final long serialVersionUID = 1L;
		{
			put(ADR_FEE, Type.ADR);
			put(IATTrade.CALL_ASG, Type.ASG);
			put(IATTrade.CALL_BTC, Type.BTC);
			put(IATTrade.CALL_BTO, Type.BTO);
			put(IATTrade.CALL_EXEC, Type.EXR);
			put(IATTrade.CALL_EXP, Type.EXP);
			put(IATTrade.CALL_STC, Type.STC);
			put(IATTrade.CALL_STO, Type.STO);
			put(IATTrade.CAP_GAIN_LT, Type.CGLT);
			put(IATTrade.CAP_GAIN_ST, Type.CGST);
			put(IATTrade.CASH_IN, Type.OTH);
			put(IATTrade.CASH_OUT, Type.OTH);
			put(IATTrade.DIVIDEND_IN, Type.DIV);
			put(IATTrade.DIVIDEND_OUT, Type.DIV);
			put(IATTrade.IN_LEU_OF, Type.ILO);
			put(IATTrade.INTEREST_IN, Type.INT);
			put(IATTrade.INTEREST_OUT, Type.INT);
			put(IATTrade.MF_IN, Type.BTO);
			put(IATTrade.MF_OUT, Type.STC);
			put(IATTrade.OTHER, Type.OTH);
			put(IATTrade.PUT_ASG, Type.ASG);
			put(IATTrade.PUT_BTC, Type.BTC);
			put(IATTrade.PUT_BTO, Type.BTO);
			put(IATTrade.PUT_EXEC, Type.EXR);
			put(IATTrade.PUT_EXP, Type.EXP);
			put(IATTrade.PUT_STC, Type.STC);
			put(IATTrade.PUT_STO, Type.STO);
			put(IATTrade.REINVEST, Type.RI);
			put(IATTrade.SECURITY_BTC, Type.BTC);
			put(IATTrade.SECURITY_BTC_ASG, Type.BTC);
			put(IATTrade.SECURITY_BTC_EXEC, Type.BTC);
			put(IATTrade.SECURITY_BTO, Type.BTO);
			put(IATTrade.SECURITY_BTO_ASG, Type.BTO);
			put(IATTrade.SECURITY_BTO_EXEC, Type.BTO);
			put(IATTrade.SECURITY_SPLIT_C, Type.STC);
			put(IATTrade.SECURITY_SPLIT_O, Type.BTO);
			put(IATTrade.SECURITY_STC, Type.STC);
			put(IATTrade.SECURITY_STC_ASG, Type.STC);
			put(IATTrade.SECURITY_STC_EXEC, Type.STC);
			put(IATTrade.SECURITY_STO, Type.STO);
			put(IATTrade.SECURITY_STO_ASG, Type.STO);
			put(IATTrade.SECURITY_STO_EXEC, Type.STO);
			put(IATTrade.TAX_FOREIGN, Type.TXF);
		}
	};

	static final Set<Integer> TYPES_CALLS = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(CALL_BTO);
			add(CALL_STO);
			add(CALL_BTC);
			add(CALL_STC);
			add(CALL_ASG);
			add(CALL_EXEC);
			add(CALL_EXP);
		}
	};
	static final Set<Integer> TYPES_PUTS = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(PUT_BTO);
			add(PUT_STO);
			add(PUT_BTC);
			add(PUT_STC);
			add(PUT_ASG);
			add(PUT_EXEC);
			add(PUT_EXP);
		}
	};

	static final Set<Integer> SIZE_NEGATIVES = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_SPLIT_C);
			add(SECURITY_STO);
			add(SECURITY_STO_ASG);
			add(SECURITY_STO_EXEC);
			add(SECURITY_STC);
			add(SECURITY_STC_ASG);
			add(SECURITY_STC_EXEC);
			add(MF_OUT);
			add(CALL_STO);
			add(CALL_STC);
			add(CALL_EXEC);
			add(PUT_STO);
			add(PUT_STC);
			add(PUT_EXEC);
			// TODO: REVIEW
			// add(DIVIDEND_OUT);
			// add(ADR_FEE);
			// add(INTEREST_OUT);
			// add(TAX_FOREIGN);
			add(IN_LEU_OF);
		}
	};

	static final Set<Integer> SIZE_POSITIVES = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_BTO);
			add(SECURITY_BTO_ASG);
			add(SECURITY_BTO_EXEC);
			add(SECURITY_BTC);
			add(SECURITY_BTC_ASG);
			add(SECURITY_BTC_EXEC);
			add(SECURITY_SPLIT_O);
			add(MF_IN);
			add(REINVEST);
			add(CALL_BTO);
			add(CALL_BTC);
			add(CALL_ASG);
			add(PUT_BTO);
			add(PUT_BTC);
			add(PUT_ASG);
			// TODO: REVIEW
			// add(DIVIDEND_IN);
			// add(CAP_GAIN_LT);
			// add(CAP_GAIN_ST);
			// add(INTEREST_IN);
			// add(IN_LEU_OF); // <- Not sure
		}
	};

	static final Set<Integer> PX_NEGATIVES = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_BTO);
			add(SECURITY_BTO_ASG);
			add(SECURITY_BTO_EXEC);
			add(SECURITY_BTC);
			add(SECURITY_BTC_ASG);
			add(SECURITY_BTC_EXEC);
			add(SECURITY_SPLIT_O);
			add(MF_IN);
			add(REINVEST);
			add(CALL_BTO);
			add(CALL_BTC);
			add(PUT_BTO);
			add(PUT_BTC);

			add(DIVIDEND_OUT);
			add(ADR_FEE);
			add(INTEREST_OUT);
			add(TAX_FOREIGN);
		}
	};

	static final Set<Integer> PX_POSITIVES = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_SPLIT_C);
			add(SECURITY_STO);
			add(SECURITY_STO_ASG);
			add(SECURITY_STO_EXEC);
			add(SECURITY_STC);
			add(SECURITY_STC_ASG);
			add(SECURITY_STC_EXEC);
			add(MF_OUT);
			add(CALL_STO);
			add(CALL_STC);
			add(PUT_STO);
			add(PUT_STC);

			add(DIVIDEND_IN);
			add(CAP_GAIN_LT);
			add(CAP_GAIN_ST);
			add(INTEREST_IN);
			add(IN_LEU_OF);
		}
	};

	static final Set<Integer> TYPES_OPEN = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_BTO);
			add(SECURITY_BTO_ASG);
			add(SECURITY_BTO_EXEC);
			add(SECURITY_SPLIT_O);
			add(SECURITY_STO);
			add(SECURITY_STO_ASG);
			add(SECURITY_STO_EXEC);
			add(MF_IN);
			add(REINVEST);
			add(CALL_BTO);
			add(CALL_STO);
			add(PUT_BTO);
			add(PUT_STO);
		}
	};

	static final Set<Integer> TYPES_CLOSE = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(SECURITY_BTC);
			add(SECURITY_BTC_ASG);
			add(SECURITY_BTC_EXEC);
			add(SECURITY_SPLIT_C);
			add(SECURITY_STC);
			add(SECURITY_STC_ASG);
			add(SECURITY_STC_EXEC);
			add(MF_OUT);
			add(CALL_BTC);
			add(CALL_STC);
			add(CALL_ASG);
			add(CALL_EXEC);
			add(CALL_EXP);
			add(PUT_BTC);
			add(PUT_STC);
			add(PUT_ASG);
			add(PUT_EXEC);
			add(PUT_EXP);
			add(IN_LEU_OF);
		}
	};

	static final Set<Integer> TYPES_IMMEDIATE = new HashSet<Integer>() {
		private static final long serialVersionUID = 1L;
		{
			add(DIVIDEND_IN);
			add(DIVIDEND_OUT);
			add(ADR_FEE);
			add(CAP_GAIN_ST);
			add(CAP_GAIN_LT);
			add(INTEREST_IN);
			add(INTEREST_OUT);
			add(TAX_FOREIGN);
			// add(IN_LEU_OF);
		}
	};

	static final Map<Integer, String> TYPE_MAP = new HashMap<Integer, String>() {
		private static final long serialVersionUID = 1L;
		{
			put(SECURITY_BTO, "Equity/ETF BTO");
			put(SECURITY_BTO_ASG, "Assign BTO");
			put(SECURITY_BTO_EXEC, "Exercise BTO");
			put(SECURITY_STO, "Equity/ETF STO");
			put(SECURITY_STO_ASG, "Assign STO");
			put(SECURITY_STO_EXEC, "Exercise STO");
			put(SECURITY_BTC, "Equity/ETF BTC");
			put(SECURITY_BTC_ASG, "Assign BTC");
			put(SECURITY_BTC_EXEC, "Exercise BTC");
			put(SECURITY_SPLIT_C, "Split (close)");
			put(SECURITY_SPLIT_O, "Split (Open)");
			put(SECURITY_STC, "Equity/ETF STC");
			put(SECURITY_STC_ASG, "Assign STC");
			put(SECURITY_STC_EXEC, "Exercise STC");
			put(MF_IN, "MF Buy");
			put(MF_OUT, "MF Sell");
			put(REINVEST, "Reinvest");
			put(CALL_BTO, "Call BTO");
			put(CALL_BTC, "Call BTC");
			put(CALL_STO, "Call STO");
			put(CALL_STC, "Call STC");
			put(CALL_ASG, "Call Assign");
			put(CALL_EXEC, "Call Exercise");
			put(CALL_EXP, "Call Expire");
			put(PUT_BTO, "Put BTO");
			put(PUT_BTC, "Put BTC");
			put(PUT_STO, "Put STO");
			put(PUT_STC, "Put STC");
			put(PUT_ASG, "Put Assign");
			put(PUT_EXEC, "Put Exercise");
			put(PUT_EXP, "Put Expire");
			put(DIVIDEND_IN, "Dividend in");
			put(DIVIDEND_OUT, "Dividend out");
			put(ADR_FEE, "ADR fees"); // Foreign
										// proxy
										// fees
			put(CAP_GAIN_ST, "Cap Gain (ST)");
			put(CAP_GAIN_LT, "Cap Gain (LT)");
			put(INTEREST_IN, "Interest in");
			put(INTEREST_OUT, "Interest out");
			put(TAX_FOREIGN, "Foreign Tax");
			put(IN_LEU_OF, "In Leu Of");

			put(CASH_IN, "Cash In");
			put(CASH_OUT, "Cash Out");

			put(OTHER, "Other");
		}
	};

	static public void cleanLedger(Map<String, Object> trade_) {
		trade_.remove(IATTrade.TRADE_GROUP);
		trade_.remove(IATTrade.SIZE_ACTIVE);
		trade_.remove(IATTrade.PX_NET);
	}

	String getAtId();

	String getOccId();

	String getTradeUserId();

	Date getTsTrade();

	Long getTsTrade2();

	Double getSzTrade();

	Double getSzTradeActive();

	Double getPxTrade();

	Double getPxTradeTotal();

	Double getPxNet();

	Double getPxUl();

	// String getBroker();
	Integer getTradeType(); // TODO: Make enum?

	// IATTrade.Type getTradeTypeCode();

	String getTradeAccount();

	String getTradeComment();

	String getTradeGroup();

	String getTraceId();

	static void sortChronological(List<IATTrade> trades_) {
		if (trades_ == null)
			return;
		trades_.sort(new Comparator<IATTrade>() {
			@Override
			public int compare(IATTrade a_, IATTrade b_) {
				int comp = a_.getTsTrade().compareTo(b_.getTsTrade());
				if (comp == 0) {
					if (IATTrade.TYPES_CLOSE.contains(a_.getTradeType())) {
						comp = IATTrade.TYPES_CLOSE.contains(b_.getTradeType()) ? 0 : -1;
					}
				}
				return comp;
			}
		});
	}

	static void verify(IATTrade trade_, String userId_) {
		String occId = trade_.getOccId();
		if (StringUtils.isBlank(occId)) {
			throw new ATException(String.format("Missing OccId: %s", trade_));
		}
		// trade_.setOccId(occId.toUpperCase());
		if (userId_ != null && !userId_.equals(trade_.getTradeUserId())) {
			throw new ATException(String.format("Trade owner mismatch [%s]: %s", userId_, trade_));
		}
		String account = trade_.getTradeAccount();
		if (StringUtils.isBlank(account)) {
			// TODO: Check account belongs to user
			throw new ATException(String.format("Missing account [%s]: %s", occId, trade_));
		}

		Integer tType = trade_.getTradeType();
		String tTypeStr = IATTrade.TYPE_MAP.get(tType);
		if (StringUtils.isBlank(tTypeStr)) {
			throw new ATException(String.format("Unknown trade type [%s/%s - %d].", account, occId, tType));
		}
		Double sz = trade_.getSzTrade();
		Double pxTotal = trade_.getPxTradeTotal();
		if (PX_POSITIVES.contains(tType) || PX_NEGATIVES.contains(tType)) {
			if (pxTotal == null) {
				throw new ATException(String.format("NULL trade total px [%s/%s - %s].", account, occId, tTypeStr));
			}
			if (!TYPES_IMMEDIATE.contains(tType) && sz == null) {
				throw new ATException(String.format("NULL trade size [%s/%s - %s]", account, occId, tTypeStr));
			}
		}
		if ((PX_POSITIVES.contains(tType) && (pxTotal <= 0)) || (PX_NEGATIVES.contains(tType) && (pxTotal >= 0))) {
			throw new ATException(
					String.format("Invalid trade type/total px [%s/%s - %s/%.2f].", account, occId, tTypeStr, pxTotal));
		}
		if ((SIZE_POSITIVES.contains(tType) && (sz <= 0)) || (SIZE_NEGATIVES.contains(tType) && (sz >= 0))) {
			throw new ATException(
					String.format("Invalid trade type/size [%s/%s - %s/%.2f]", account, occId, tTypeStr, sz));
		}
		if (occId.length() <= 6)
			return;
		if (StringUtils.isNumeric(occId)) {
			throw new ATException(
					String.format("Numeric ticker (review splits) [%s/%s - %s/%.2f]", account, occId, tTypeStr, sz),
					ATException.ERROR_NUMERIC_SYMBOL);
		}
		try {
			String optionRight = occId.substring(12, 13);
			if ((IATAssetConstants.CALL.equals(optionRight) && !TYPES_CALLS.contains(tType))
					|| (IATAssetConstants.PUT.equals(optionRight) && !TYPES_PUTS.contains(tType))) {
				throw new ATException(
						String.format("Invalid trade type [%s/%s - %s/%s]", account, occId, optionRight, tTypeStr),
						ATException.ERROR_TRADE_TYPE);
			}
		} catch (Exception ex_) {
			if (ex_ instanceof ATException) {
				throw ex_;
			} else {
				throw new ATException(String.format("occid [%s]", occId), ex_);
			}
		}
	}

	static String getOccId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static String getTraceId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRACE_ID);
	}

	static void setTraceId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TRACE_ID, value_);
	}

	static String getTradeUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_USER);
	}

	static void setTradeUserId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TRADE_USER, value_);
	}

	static Date getTsTrade(Map<String, Object> map_) {
		try {
			Object tsn = map_.get(IATAssetConstants.TS_TRADE);
			if (tsn != null) {
				if (tsn instanceof Double) {
					tsn = ((Double) tsn).longValue();
				}
				Date d = ATFormats.numberToDate((Long) tsn);
				return d;
			}
			Object tso = map_.get(IATAssetConstants.TS_TRADE_OLD);
			if (tso == null)
				return null;
			Long ts = (tso instanceof Double) ? ((Double) tso).longValue() : (Long) tso;
			return ts == null ? null : new Date(ts);
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static void setTsTrade(Map<String, Object> map_, Date value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.TS_TRADE);
		} else {
			Long l = ATFormats.dateToNumber(value_);
			IATAsset.setMapValue(map_, IATAssetConstants.TS_TRADE, l);
		}
		// Long ts = value_ == null ? null : value_.getTime();
		// IATAsset.setMapValue(map_, IATAssetConstants.TS_TRADE_OLD, ts);
	}

	static Long getTsTrade2(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_TRADE);
	}

	static void setTsTrade2(Map<String, Object> map_, Long value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.TS_TRADE, value_);
	}

	static Double getSzTrade(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.SIZE_TRADE);
	}

	static void setSzTrade(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.SIZE_TRADE, value_);
	}

	static Double getSzTradeActive(Map<String, Object> map_) {
		Double result = IATAsset.getDouble(map_, IATAssetConstants.SIZE_ACTIVE);
		if (result == null)
			result = getSzTrade(map_);
		return result;
	}

	static void setSzTradeActive(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.SIZE_ACTIVE, value_);
	}

	static Double getPxTrade(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE);
		return value;// != null ? value : 0;
	}

	static void setPxTrade(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.PX_TRADE, value_);
	}

	static Double getPxTradeTotal(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE_TOTAL);
		return value;// != null ? value : 0;
	}

	static void setPxTradeTotal(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.PX_TRADE_TOTAL, value_);
	}

	static Double getPxNet(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_NET);
		return value;// != null ? value : 0;
	}

	static void setPxNet(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.PX_NET, value_);
	}

	static Double getPxUl(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_UL);
		// if (value == null) {
		// value = getPxTrade(map_);
		// }
		return value;
	}

	static void setPxUl(Map<String, Object> map_, Double value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.PX_UL, value_);
	}

	static int getTradeType(Map<String, Object> map_) {
		Integer result = IATAsset.getInt(map_, IATAssetConstants.TRADE_TYPE, true);
		// if(result!=-1) {
		// 	map_.put(IATAssetConstants.TRADE_TYPE, result);
		// 		}
		// Object tto = map_.get(IATAssetConstants.TRADE_TYPE);
		// if (tto == null)
		// 	return -1;
		// Integer tt = (tto instanceof Double) ? ((Double) tto).intValue() : (Integer) tto;
		// map_.put(IATAssetConstants.TRADE_TYPE, tt);
		return result == null ? -1 : result;
	}

	static void setTradeType(Map<String, Object> map_, int varlue_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.TRADE_TYPE, varlue_);
	}

	static String getTradeAccount(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_ACCOUNT);
	}

	static void setTradeAccount(Map<String, Object> map_, String value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.TRADE_ACCOUNT, value_);
	}

	static String getTradeComment(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_COMMENT);
	}

	static void setTradeComment(Map<String, Object> map_, String value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.TRADE_COMMENT, value_);
	}

	static String getTradeGroup(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_GROUP);
	}

	static void setTradeGroup(Map<String, Object> map_, String value_) {
		IATAsset.setRemovableValue(map_, IATAssetConstants.TRADE_GROUP, value_);
	}

	static String getUnderlyingSymbol(Map<String, Object> map_) {
		return IATAsset.getString(map_, UNDERLYING);
	}

	static void setUnderlyingSymbol(Map<String, Object> map_, String value_) {
		IATAsset.setRemovableValue(map_, UNDERLYING, value_);
	}

	static Date getTsExpiration(IATTrade trade_) throws ParseException {
		String occId = trade_.getOccId();
		if (occId.length() <= 6)
			return null;
		String ds6 = occId.substring(6, 12);
		// NOTE: Y2100 bug!
		Date d = ATFormats.DATE_yyMMdd.get().parse(ds6);
		return d;
	}

	static String toString(IATTrade trade_) {
		String str = String.format("%1$-21s (%2$tF %2$tR/%5s) %3$.2f/%4$.2f", trade_.getOccId(),
				trade_.getTsTrade(), trade_.getSzTradeActive(), trade_.getSzTrade(), trade_.getTradeAccount());
		return str;
	}

}
