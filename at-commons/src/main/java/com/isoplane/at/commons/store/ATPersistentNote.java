package com.isoplane.at.commons.store;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATNote;

public class ATPersistentNote extends ATPersistentLookupBase implements IATNote, IATAssetConstants, Comparable<ATPersistentNote> {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		String str = String.format("%s: %s", getAtId(), getNote());
		return str;
	}

	public String getNote() {
		return (String) get(NOTE);
	}

	public void setNote(String note_) {
		put(NOTE, note_);
	}

	public String getOccId() {
		return (String) get(OCCID);
	}

	public void setOccId(String value_) {
		put(OCCID, value_);
	}

	public String getOwnerId() {
		return (String) get(OWNER_ID);
	}

	public void setOwnerId(String value_) {
		put(OWNER_ID, value_);
	}

	@Override
	public int compareTo(ATPersistentNote other_) {
		if (other_ == null)
			return -1;
		int result = this.getAtId().compareTo(other_.getAtId());
		if (result == 0) {
			result = this.getNote().compareTo(other_.getNote());
		}
		return result;
	}

	@Override
	public boolean equals(Object other_) {
		if (other_ == null || !(other_ instanceof ATPersistentNote))
			return false;
		return this.hashCode() == other_.hashCode();
	}

	@Override
	public int hashCode() {
		String hstr = getAtId() + getNote();
		return hstr.hashCode();
	}

}
