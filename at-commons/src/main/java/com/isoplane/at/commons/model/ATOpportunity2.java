package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ATOpportunity2 {

	public static final String CC = "CC";
	public static final String VCS = "VCS";
	public static final String VPS = "VPS";

	private Double div;
	private String divExDS10;
	// private List<String> occIds;
	// private List<Integer> expDays;
	private List<ATOpportunitySegment> segments;
	private Double px;
	private Double pxTheo;
	private String symbol;
	private String type;
	private Double yieldPct;
	private Double yieldAbs;
	private Double yieldApy;
	private Double yieldAsg; // Assigned
	private Integer daysExp;

	public ATOpportunity2() {
		this.setSegments(new ArrayList<>());
		// this.occIds = new ArrayList<>();
		// this.expDays = new ArrayList<>();
	}

	public ATOpportunity2(String type_) {
		this();
		this.setType(type_);
	}

	static public boolean isSafeBelowLast(String strategy_) {
		return CC.equals(strategy_) || VPS.equals(strategy_);
	}

	public boolean isSafeBelowLast() {
		return isSafeBelowLast(this.type);
	}

//	public List<String> getOccIds() {
//		return occIds;
//	}
//
//	public void setOccIds(List<String> occIds) {
//		this.occIds = occIds;
//	}

	public Double getPx() {
		return px;
	}

	public void setPx(Double px) {
		this.px = px;
	}

	public Double getPxTheo() {
		return pxTheo;
	}

	public void setPxTheo(Double px) {
		this.pxTheo = px;
	}

	public Double getYieldPct() {
		return yieldPct;
	}

	public void setYieldPct(Double yieldPct) {
		this.yieldPct = yieldPct;
	}

	public Double getYieldAbs() {
		return yieldAbs;
	}

	public void setYieldAbs(Double yieldAbs) {
		this.yieldAbs = yieldAbs;
	}

	public Double getDiv() {
		return div;
	}

	public void setDiv(Double div) {
		this.div = div;
	}

	// String divDS8;
	// Integer divFreq;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getYieldAsg() {
		return yieldAsg;
	}

	public void setYieldAsg(Double yieldAsg) {
		this.yieldAsg = yieldAsg;
	}

	public String getDivExDS10() {
		return divExDS10;
	}

	public void setDivExDS10(String divDstr) {
		this.divExDS10 = divDstr;
	}

	@Override
	public int hashCode() {
		String occIds = this.segments != null ? this.segments.stream().map(c -> c.occId).collect(Collectors.joining()) : "";
		String str = this.type + occIds;
		return str.hashCode();
	}

	public List<ATOpportunitySegment> getSegments() {
		return segments;
	}

	public void setSegments(List<ATOpportunitySegment> components) {
		this.segments = components;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getYieldApy() {
		return yieldApy;
	}

	public void setYieldApy(Double yieldApy) {
		this.yieldApy = yieldApy;
	}

	public Integer getDaysExp() {
		return daysExp;
	}

	public void setDaysExp(Integer daysExp) {
		this.daysExp = daysExp;
	}

	public static class ATOpportunitySegment {

		public String occId;
		public Integer daysExp;
		public Double count;
	//	public Double mrgReq;

	public ATOpportunitySegment(String occId_, Double count_, Integer daysExp_/* , Double mrgReq_ */) {
			this.occId = occId_;
			this.count = count_;
			this.daysExp = daysExp_;
//			this.mrgReq = mrgReq_;
		}

	}
}