package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;

public class ATPosition extends HashMap<String, Object> implements IATPosition {

    public ATPosition() {
    }

    public ATPosition(Map<String, Object> map_) {
        this();
        this.putAll(map_);
    }

    @Override
    public String getOccId() {
        return IATPortfolio.getOccId(this);
    }

    @Override
    public void setOccId(String value_) {
        IATPosition.setOccId(this, value_);
    }

    @Override
    public Double getSize() {
        return IATPosition.getSize(this);
    }

    @Override
    public void setSize(Double value) {
        IATPosition.setSize(this, value);
    }

    @Override
    public Double getPxTrade() {
        return IATPosition.getPxTrade(this);
    }

    @Override
    public void setPxTrade(Double value) {
        IATPosition.setPxTrade(this, value);
    }

    @Override
    public Double getPxTradeTotal() {
        return IATPosition.getPxTradeTotal(this);
    }

    @Override
    public void setPxTradeTotal(Double value) {
        IATPosition.setPxTradeTotal(this, value);
    }

    @Override
    public Double getPxTradeAdjusted() {
        return IATPosition.getPxTradeAdjusted(this);
    }

    @Override
    public void setPxTradeAdjusted(Double value) {
        IATPosition.setPxTradeAdjusted(this, value);
    }

    @Override
    public String getUserId() {
        return IATPosition.getUserId(this);
    }

    @Override
    public void setUserId(String value) {
        IATPosition.setUserId(this, value);
    }

    @Override
    public String getAccountId() {
        return IATPosition.getAccountId(this);
    }

    @Override
    public void setAccountId(String value) {
        IATPosition.setAccountId(this, value);
    }

    @Override
    public String getUnderlying() {
        return IATPosition.getUnderlying(this);
    }

    @Override
    public void setUnderlying(String value) {
        IATPosition.setUnderlying(this, value);
    }

    @Override
    public Integer getExp6() {
        return IATPosition.getExp6(this);
    }

    @Override
    public void setExp6(Integer value) {
        IATPosition.setExp6(this, value);
    }

    @Override
    public Long getTsLast() {
        return IATPosition.getTsLast(this);
    }

    @Override
    public void setTsLast(Long value) {
        IATPosition.setTsLast(this, value);
    }

    // @Override
    // public int hashCode() {
    //     var h = super.hashCode();
    //     var str = getOccId();
    //     var hash = str  ! = null ? str.hashCode () : 0;
    //     return hash;
    // }

    // @Override
    // public boolean equals(Object other_) {
    //     if (!(other_ instanceof ATPosition))
    //         return false;
    //     return super.equals(o);
    // }

}
