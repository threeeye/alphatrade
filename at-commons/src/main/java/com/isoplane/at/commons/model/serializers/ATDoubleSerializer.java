package com.isoplane.at.commons.model.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class ATDoubleSerializer implements JsonSerializer<Double> {

	@Override
	public JsonElement serialize(Double src_, Type typeOfSrc_, JsonSerializationContext context_) {
		return src_ == null || src_.isNaN() || src_.isInfinite() ? JsonNull.INSTANCE : new JsonPrimitive(src_);
	}

}
