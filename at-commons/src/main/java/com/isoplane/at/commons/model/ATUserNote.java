package com.isoplane.at.commons.model;

import java.util.HashMap;

public class ATUserNote extends HashMap<String, Object> implements Comparable<ATUserNote>, IATUserNote {

	private static final long serialVersionUID = 1L;

	public ATUserNote() {
	}

	public ATUserNote(String userId_, String occId_, String note_) {
		this.setUserId(userId_);
		this.setOccId(occId_);
		this.setNote(note_);
		cleanup();
	}

	public ATUserNote(IATUserNote tmpl_) {
		this(tmpl_.getUserId(), tmpl_.getOccId(), tmpl_.getNote());
	}

	@Override
	public String getOccId() {
		return IATUserAlert.getOccId(this);
	}

	public void setOccId(String value_) {
		IATUserAlert.setOccId(this, value_);
	}

	@Override
	public String getUserId() {
		return IATUserAlert.getUserId(this);
	}

	public void setUserId(String value_) {
		IATUserAlert.setUserId(this, value_);
	}

	@Override
	public String getNote() {
		return IATUserNote.getNote(this);
	}

	public void setNote(String value_) {
		IATUserNote.setNote(this, value_);
	}

	@Override
	public int compareTo(ATUserNote other_) {
		if (other_ == null)
			return -1;
		int result = this.getOccId().compareTo(other_.getOccId());
		if (result != 0)
			return result;
		result = this.getNote().compareTo(other_.getNote());
		if (result != 0)
			return result;
		return 0;
	}

	@Override
	public String toString() {
		return IATUserNote.toString(this);
	}

	public ATUserNote cleanup() {
		String occId = getOccId();
		if (occId != null) {
			occId = occId.trim().toUpperCase();
		}
		return this;
	}

	public static class ATUserNotesPack {
		public ATUserNote[] notes;
		public String usrId;
		public String occId;
	}

}
