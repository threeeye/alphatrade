package com.isoplane.at.commons.model.alertcriteria;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;

//http://csbruce.com/software/utf-8.html
public abstract class ATBaseCriteria implements Comparable<ATBaseCriteria> {

	static final Logger log = LoggerFactory.getLogger("Criteria");

	static final protected int EQ = 0;
	static final protected int GT = 10;
	static final protected int GTE = 20;
	static final protected int LT = 30;
	static final protected int LTE = 40;

	private String _userId;
	private String _occId;
	private String _criteria;

	public ATBaseCriteria(String userId_, String occId_, String criteria_) {
		_userId = userId_;
		_occId = occId_;
		_criteria = criteria_;
	}

	/**
	 * @param sec
	 * @return Meanings: true = pass criteria; false = fail criteria; NULL = stop processing
	 */
	@Deprecated
	abstract public Boolean test(ATSecurity sec);

	@Deprecated
	abstract public Boolean test(ATPersistentSecurity sec);

	@Deprecated
	abstract public String label(ATPersistentSecurity sec);

	abstract public String label(IATMarketData mkt, IATStatistics stats);

	abstract public Boolean test(IATMarketData mkt, IATStatistics stats);

	public String label() {
		return String.format("%s %s", getOccId(), getCriteria());
	}

	public String getUserId() {
		return _userId;
	}

	public String getOccId() {
		return _occId;
	}

	public String getCriteria() {
		return _criteria;
	}

	protected boolean isOccIdMatch(String occId_) {
		return getOccId().equals(occId_);
	}

	static protected Integer getCondition(ATAlertRule alert_) {
		Integer condition = null;
		String operator = alert_ != null ? alert_.getOperator() : null;
		if (operator == null)
			return null;
		switch (operator.toUpperCase()) {
		case "EQ":
			condition = EQ;
			break;
		case "GT":
			condition = GT;
			break;
		case "GTE":
			condition = GTE;
			break;
		case "LT":
			condition = LT;
			break;
		case "LTE":
			condition = LTE;
			break;
		}
		return condition;
	}

	static protected String getOperatorString(int opNum_) {
		switch (opNum_) {
		case EQ:
			return "=";
		case GT:
			return ">";
		case GTE:
			return "≥";
		case LT:
			return "<";
		case LTE:
			return "≤";
		}
		return null;
	}

	@Override
	public String toString() {
		return label();// String.format("%-5s%s", _occId, _criteria);
	}

	@Override
	public int compareTo(ATBaseCriteria other_) {
		if (other_ == null)
			return 1;
		return this.label().compareTo(other_.label());
	}

	@Override
	public int hashCode() {
		return label().hashCode();
	}

	@Override
	public boolean equals(Object obj_) {
		if (obj_ == null || !(obj_ instanceof ATFalseCriteria))
			return false;
		return label().equals(((ATFalseCriteria) obj_).label());
	}

	static protected boolean testNumber(int condition_, Double ref_, Double value_) {
		switch (condition_) {
		case EQ:
			return value_ == ref_;
		case GT:
			return value_ > ref_;
		case GTE:
			return value_ >= ref_;
		case LT:
			return value_ < ref_;
		case LTE:
			return value_ <= ref_;
		}
		return false;
	}
}
