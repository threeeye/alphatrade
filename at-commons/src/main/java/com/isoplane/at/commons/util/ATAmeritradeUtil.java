package com.isoplane.at.commons.util;

import java.util.Map;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOption;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;

public class ATAmeritradeUtil {

	public static ATOption toOption(Map<String, Object> tmpl_) {
		if (tmpl_ == null)
			return null;
		ATOption option = new ATOption();
		IATAsset.setOccId(option, IATAsset.getOccId(tmpl_));
		IATAsset.setExpDS6(option, IATAsset.getExpDS6(tmpl_));
		IATAsset.setPxStrike(option, IATAsset.getDouble(tmpl_, "strikePrice"));
		String right = IATAsset.getString(tmpl_, "putCall");
		IATAsset.setRight(option, "PUT".equals(right) ? IATAssetConstants.PUT : IATAssetConstants.CALL);
		IATAsset.setOptionMultiplier(option, IATAsset.getDouble(tmpl_, "multiplier"));

		IATAsset.setDelta(option, IATAsset.getDouble(tmpl_, "vega"));
		IATAsset.setGamma(option, IATAsset.getDouble(tmpl_, "gamma"));
		IATAsset.setRho(option, IATAsset.getDouble(tmpl_, "rho"));
		IATAsset.setTheta(option, IATAsset.getDouble(tmpl_, "theta"));
		IATAsset.setVega(option, IATAsset.getDouble(tmpl_, "delta"));

		IATAsset.setPxAsk(option, IATAsset.getDouble(tmpl_, "ask"));
		IATAsset.setSzAsk(option, IATAsset.getInt(tmpl_, "askSize"));
		IATAsset.setPxBid(option, IATAsset.getDouble(tmpl_, "bid"));
		IATAsset.setSzBid(option, IATAsset.getInt(tmpl_, "bidSize"));
		IATAsset.setPxLast(option, IATAsset.getDouble(tmpl_, "last"));
		IATAsset.setSzLast(option, IATAsset.getInt(tmpl_, "lastSize"));
		IATAsset.setTsLast(option, IATAsset.getLong(tmpl_, "tradeTimeInLong"));

		IATAsset.setPxTheo(option, IATAsset.getDouble(tmpl_, "theoreticalOptionValue"));
		IATAsset.setPxTime(option, IATAsset.getDouble(tmpl_, "timeValue"));
		IATAsset.setIV(option, IATAsset.getDouble(tmpl_, "volatility")); // theoreticalVolatility?
		return option;
	}

	public static ATMarketData toMarketData(Map<String, Object> tmpl_) {
		if (tmpl_ == null)
			return null;
		ATMarketData mkt = new ATMarketData();
		IATAsset.setOccId(mkt, IATAsset.getString(tmpl_, "symbol"));

		IATAsset.setPxAsk(mkt, IATAsset.getDouble(tmpl_, "ask"));
		IATAsset.setSzAsk(mkt, IATAsset.getInt(tmpl_, "askSize"));
		IATAsset.setPxBid(mkt, IATAsset.getDouble(tmpl_, "bid"));
		IATAsset.setSzBid(mkt, IATAsset.getInt(tmpl_, "bidSize"));
		IATAsset.setPxLast(mkt, IATAsset.getDouble(tmpl_, "last"));
		IATAsset.setSzLast(mkt, IATAsset.getInt(tmpl_, "lastSize"));
		IATAsset.setTsLast(mkt, IATAsset.getLong(tmpl_, "tradeTimeInLong"));

		mkt.setDailyVol(IATAsset.getLong(tmpl_, "totalVolume"));
		mkt.setPxChange(IATAsset.getDouble(tmpl_, "change"));
		mkt.setPxHigh(IATAsset.getDouble(tmpl_, "highPrice"));
		mkt.setPxLow(IATAsset.getDouble(tmpl_, "lowPrice"));
		mkt.setPxOpen(IATAsset.getDouble(tmpl_, "openPrice"));
		mkt.setPxClose(IATAsset.getDouble(tmpl_, "close"));

		// this.setPxAsk(IATMarketData.getPxAsk(map_));
		// this.setSzAsk(IATMarketData.getSzAsk(map_));
		// this.setTsAsk(IATMarketData.getTsAsk(map_));
		// this.setPxBid(IATMarketData.getPxBid(map_));
		// this.setSzBid(IATMarketData.getSzBid(map_));
		// this.setTsBid(IATMarketData.getTsBid(map_));
		// this.setPxLast(IATMarketData.getPxLast(map_));
		// this.setSzLast(IATMarketData.getSzLast(map_));
		// this.setTsLast(IATMarketData.getTsLast(map_));
		// this.setDailyVol(IATMarketData.getDailyVol(map_));
		// this.setPxOpen(IATMarketData.getPxOpen(map_));
		// this.setPxHigh(IATMarketData.getPxHigh(map_));
		// this.setPxLow(IATMarketData.getPxLow(map_));
		// this.setPxClosePrev(IATMarketData.getPxClosePrev(map_));
		// this.setPxClose(IATMarketData.getPxClose(map_));
		// this.setOpenInterest(IATMarketData.getOpenInterest(map_));
		// this.setPxStrike(IATMarketData.getPxStrike(map_));
		// this.setOptionRight(IATMarketData.getOptionRight(map_));
		// this.setPxChange(IATMarketData.getPxChange(map_));

		return mkt;
	}

}
