package com.isoplane.at.commons.store;

import java.lang.reflect.Constructor;

import com.isoplane.at.commons.ATException;

public class ATPersistenceRegistry {

	static private Constructor<?> _ctr;

	static public  <T extends IATWhereQuery> void setProviderClass(Class<T> clazz_) {
		try {
			_ctr = clazz_.getConstructor();
			_ctr.newInstance(); // -> Test
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static public IATWhereQuery query() {
		IATWhereQuery query;
		try {
			query = (IATWhereQuery) _ctr.newInstance();
			return query;
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}
}
