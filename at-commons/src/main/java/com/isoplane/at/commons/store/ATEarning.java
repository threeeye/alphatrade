package com.isoplane.at.commons.store;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATEarning;
import com.isoplane.at.commons.util.ATFormats;

public class ATEarning extends ATPersistentLookupBase implements IATEarning, Comparable<ATEarning> {

	private static final long serialVersionUID = 1L;

	public ATEarning() {
		super();
	}

	public ATEarning(Map<String, Object> data_) {
		super(data_);
	}

	@Override
	public int compareTo(ATEarning other_) {
		return getAtId().compareTo(other_.getAtId());
	}

	@Override
	public String toString() {
		String str = String.format("%-5s%s", getOccId(), getEarningsDateStr());
		return str;
	}

	@Override
	public String getOccId() {
		return (String) get(IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getEarningsDateStr() {
		return (String) get(IATAssetConstants.EARNINGS_DATE_STR_OLD);
	}

	public void setEarningsDateStr(String value_) {
		put(IATAssetConstants.EARNINGS_DATE_STR_OLD, value_);
	}

	@Override
	public Date getEarningsDate() {
		if (StringUtils.isBlank(getEarningsDateStr()))
			return null;
		try {
			Date date = ATFormats.DATE_yyyyMMdd.get().parse(getEarningsDateStr());
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}
	
	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public String getSource() {
		return (String) super.get(IATAssetConstants.SOURCE);
	}

}
