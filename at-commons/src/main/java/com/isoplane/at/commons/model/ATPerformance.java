package com.isoplane.at.commons.model;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ATPerformance extends HashMap<String, Object> implements Comparable<ATPerformance>, IATAsset {

	private static final long serialVersionUID = 1L;

	public static final String DETAILS = "details";
	public static final String VALUE_TOTAL = "val_tot";
	public static final String RISK_TOTAL = "rsk_tot";
	public static final String RISK_CALL = "rsk_call";
	public static final String RISK_PUT = "rsk_put";

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public String getAtId() {
		return getOccId();
	}

	// @Override
	public String getOccId() {
		return IATTrade.getOccId(this);
	}

	public void setOccId(String value_) {
		IATTrade.setOccId(this, value_);
	}

	// @Override
	public String getTradeUserId() {
		return IATTrade.getTradeUserId(this);
	}

	public void setTradeUserId(String value_) {
		IATTrade.setTradeUserId(this, value_);
	}

	// @Override
	public Double getSzTrade() {
		return IATTrade.getSzTrade(this);
	}

	public void setSzTrade(Double value_) {
		IATTrade.setSzTrade(this, value_);
	}

	// @Override
	public Double getPxTrade() {
		return IATTrade.getPxTrade(this);
	}

	public void setPxTrade(Double value_) {
		IATTrade.setPxTrade(this, value_);
	}

	// @Override
	public Double getPxTradeTotal() {
		return IATTrade.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATTrade.setPxTradeTotal(this, value_);
	}

	public Double getValueTotal() {
		return (Double) this.get(VALUE_TOTAL);
	}

	public void setValueTotal(Double value_) {
		this.put(VALUE_TOTAL, value_);
	}

	public Double getRiskTotal() {
		return (Double) this.get(RISK_TOTAL);
	}

	public void setRiskTotal(Double value_) {
		this.put(RISK_TOTAL, value_);
	}

	public void setCallRisk(Double value_) {
		this.put(RISK_CALL, value_);
	}

	public Double getCallRisk() {
		return (Double) this.get(RISK_CALL);
	}

	public void setPutRisk(Double value_) {
		this.put(RISK_PUT, value_);
	}

	public Double getPutRisk() {
		return (Double) this.get(RISK_PUT);
	}

	public Double getPxLast() {
		return (Double) this.get(IATAssetConstants.PX_LAST);
	}

	public void setPxLast(Double value_) {
		this.put(IATAssetConstants.PX_LAST, value_);
	}

	@SuppressWarnings("unchecked")
	public Set<ATPerformance> getDetails() {
		return (Set<ATPerformance>) super.get(DETAILS);
	}

	public void setDetails(Set<ATPerformance> value_) {
		super.put(DETAILS, value_);
	}

	@Override
	public int compareTo(ATPerformance other_) {
		if (other_ == null)
			return -1;
		if (this.getOccId() == null) {
			return other_.getOccId() == null ? 0 : 1;
		}
		return this.getOccId().compareTo(other_.getOccId());
	}

	public String getHashStr(boolean includeRisk_) {
		double risk = includeRisk_ ? (getRiskTotal() == null ? 0 : getRiskTotal()) : 0;
		String str = String.format("%s%2.0f%10.2f", getOccId(), Math.signum(getSzTrade()), risk);
		return str;
	}

	public static class ATPerformanceComparator implements Comparator<ATPerformance> {

		@Override
		public int compare(ATPerformance a_, ATPerformance b_) {
			if (b_ == null) {
				return a_ == null ? 0 : Integer.MIN_VALUE;
			}
			if (a_ == null)
				return Integer.MAX_VALUE;
			int result = a_.getHashStr(true).compareTo(b_.getHashStr(true));
			return result;
		}

	}

}
