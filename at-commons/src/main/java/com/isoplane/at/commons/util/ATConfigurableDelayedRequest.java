package com.isoplane.at.commons.util;

public class ATConfigurableDelayedRequest extends ATDelayedRequest {

	private IATIdentifyableRunnable _run;

	protected ATConfigurableDelayedRequest(IATIdentifyableRunnable run_, long delay_) {
		_run = run_;
		super.setDelay(delay_);
	}

	@Override
	public void run() {
		_run.run();
	}

	@Override
	public void setDelay(long delay_) {
		if (super.getDelay() < delay_) {
			super.setDelay(delay_);
		}
	}

	@Override
	public Object getIdentifyableId() {
		return _run.getIdentification();
	}

}
