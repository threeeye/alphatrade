package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;

public interface IATProtoSseSubscriber {

	void notifySseMessage(SseMessage msg);

}
