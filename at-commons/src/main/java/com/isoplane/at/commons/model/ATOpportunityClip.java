package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATOpportunityClip extends HashMap<String, Object> implements Comparable<ATOpportunityClip> {

	private static final long serialVersionUID = 1L;

	public static final String COMPONENTS = "components";
	// private String label;
	// public String _atid: string;
	// public label: string;
	// public components: Map<string, number>;

	static public Set<ATOpportunityClip> fromJsonMaps(String userId_, HashMap<String, Object>[] maps_) {
		if (maps_ == null || maps_.length == 0 || StringUtils.isBlank(userId_))
			return null;
		Set<ATOpportunityClip> clips = new TreeSet<>();
		for (HashMap<String, Object> map : maps_) {
			ATOpportunityClip clip = new ATOpportunityClip(map);
			clip.setUserId(userId_);
			clips.add(clip);
		}
		return clips;
	}

	public ATOpportunityClip() {

	}

	public ATOpportunityClip(Map<String, Object> tmpl_) {
		if (tmpl_ == null || tmpl_.isEmpty()) {
			throw new ATException(String.format("Invalid template"));
		}
		String id = IATLookupMap.getAtId(tmpl_);
		String label = IATAsset.getString(tmpl_, IATAssetConstants.LABEL);
		String symbol = IATAsset.getString(tmpl_, IATAssetConstants.SYMBOL);
		String userId = IATAsset.getString(tmpl_, IATAssetConstants.USER_ID);
		this.setAtId(id);
		this.setLabel(label);
		this.setSymbol(symbol);
		this.setUserId(userId);
		Object cmpsO = tmpl_.get(COMPONENTS);
		if (cmpsO instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Double> cmpsM = (Map<String, Double>) cmpsO;
			this.setComponents(cmpsM);
		} else {
			@SuppressWarnings("unchecked")
			ArrayList<ArrayList<Object>> cmps = (ArrayList<ArrayList<Object>>) cmpsO;
			if (cmps != null && !cmps.isEmpty()) {
				Map<String, Double> cmpMap = new TreeMap<>();
				for (ArrayList<Object> entry : cmps) {
					if (entry.size() != 2)
						continue;
					String occId = (String) entry.get(0);
					Double count = (Double) entry.get(1);
					if (!StringUtils.isBlank(occId) && !count.isNaN()) {
						cmpMap.put(occId, count);
					}
				}
				if (!cmpMap.isEmpty()) {
					IATAsset.setMapValue(this, COMPONENTS, cmpMap);
				}
			}
		}
	}

	public String getAtId() {
		return IATLookupMap.getAtId(this);
	}

	public void setAtId(String value_) {
		IATLookupMap.setAtId(this, value_);
	}

	public String getSymbol() {
		return getSymbol(this);
	}

	static public String getSymbol(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.SYMBOL);
	}

	public void setSymbol(String value_) {
		IATAsset.setMapValue(this, IATAssetConstants.SYMBOL, value_, true);
	}

	public String getLabel() {
		return getLabel(this);
	}

	static public String getLabel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LABEL);
	}

	public void setLabel(String value_) {
		IATAsset.setMapValue(this, IATAssetConstants.LABEL, value_, true);
	}

	public String getUserId() {
		return getUserId(this);
	}

	static public String getUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value_) {
		IATAsset.setMapValue(this, IATAssetConstants.USER_ID, value_, true);
	}

	public Map<String, Double> getComponents() {
		@SuppressWarnings("unchecked")
		Map<String, Double> cmps = (Map<String, Double>) this.get(COMPONENTS);
		if (cmps != null) {
			cmps = new TreeMap<>(cmps);
		}
		return cmps;
	}

	public void setComponents(Map<String, Double> value_) {
		IATAsset.setMapValue(this, COMPONENTS, value_, true);
	}

	@Override
	public int compareTo(ATOpportunityClip other_) {
		String thisLabel = IATAsset.getString(this, IATAssetConstants.LABEL);
		String otherLabel = IATAsset.getString(other_, IATAssetConstants.LABEL);
		if (thisLabel == null) {
			return (otherLabel != null) ? 1 : 0;
		} else {
			return thisLabel.compareTo(otherLabel);
		}
	}

}
