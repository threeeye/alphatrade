package com.isoplane.at.commons.model.protobuf;

import com.isoplane.at.commons.model.protobuf.EventubusMessageProtos.EventbusMessage;;

public class ATProtoEventbusMessageImportHelperClass {

	private EventbusMessage _protoMsg;

	public ATProtoEventbusMessageImportHelperClass(EventbusMessage msg_) {
		this._protoMsg = msg_;
	}

	public EventbusMessage proto() {
		return this._protoMsg;
	}


}
