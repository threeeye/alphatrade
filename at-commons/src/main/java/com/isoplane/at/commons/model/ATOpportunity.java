package com.isoplane.at.commons.model;

import java.util.Date;

import com.isoplane.at.commons.ATException;

public class ATOpportunity implements Comparable<ATOpportunity> {

	public static final String HEADER = "H"; // Header
	public static final String CALL = "C"; // Put Row
	public static final String CC = "CC"; // Covered Call Row
	public static final String SPREAD = "S"; // Spread
	public static final String PUT = "P"; // Put Row
	public static final String WEEK = "W";
	public static final String MONTH = "m";

	private Boolean isActive;
	private Date timestamp;
	private Date date;
	private String occId;
	private String symbol;
	private String label;
	private Double priceAsk;
	private Double priceAvg;
	private Double priceBid;
	private Double priceClose;
	private Double priceMid;
	private Double priceTheo;
	private Double strikeA;
	private Double strikeB;
	private Double yieldAbs;
	private Double yieldPctA;
	private Double safetyMgn;
	private Double probability;
	private int days;
	private double scoreInternal;
	private double scoreExternal;
	private String strategy;
	private String periodicity;
	private String industry;
	private String flags;
	private String meta;

	transient private String sortKey;
	transient private boolean isTriggered;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getOccId() {
		return occId;
	}

	public void setOccId(String occId) {
		this.occId = occId;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getPriceBid() {
		return priceBid;
	}

	public void setPriceBid(Double priceBid) {
		validateNumber(priceBid);
		this.priceBid = priceBid;
	}

	public Double getPriceMid() {
		return priceMid;
	}

	public void setPriceMid(Double priceMid) {
		validateNumber(priceMid);
		this.priceMid = priceMid;
	}

	public Double getPriceTheo() {
		return priceTheo;
	}

	public void setPriceTheo(Double price_) {
		validateNumber(price_);
		this.priceTheo = price_;
	}

	public Double getPriceAvg() {
		return priceAvg;
	}

	public void setPriceAvg(Double priceAvg) {
		validateNumber(priceAvg);
		this.priceAvg = priceAvg;
	}
	
	public Double getPriceAsk() {
		return priceAsk;
	}

	public void setPriceAsk(Double priceAsk) {
		validateNumber(priceAsk);
		this.priceAsk = priceAsk;
	}

	public Double getYieldAbs() {
		return yieldAbs;
	}

	public void setYieldAbs(Double yieldAbs) {
		validateNumber(yieldAbs);
		this.yieldAbs = yieldAbs;
	}

	public Double getYieldPctA() {
		return yieldPctA;
	}

	public void setYieldPctA(Double yieldPctA) {
		validateNumber(yieldPctA);
		this.yieldPctA = yieldPctA;
	}

	public Double getSafetyMgn() {
		return safetyMgn;
	}

	public void setSafetyMgn(Double safetyMgn) {
		validateNumber(safetyMgn);
		this.safetyMgn = safetyMgn;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public Double getScoreInternal() {
		return scoreInternal;
	}

	public void setScoreInternal(Double score) {
		validateNumber(score);
		this.scoreInternal = score;
	}

	public String getStrategy() {
		return strategy;
	}

	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}

	@Override
	public int compareTo(ATOpportunity other_) {
		if (other_ == null)
			return -1;
		// Note: ignore timestamp
		// String thisStrategyCode = ATOpportunity.HEADER.equals(this.getStrategy()) ? " " : this.getStrategy();
		// String otherStrategyCode = ATOpportunity.HEADER.equals(other_.getStrategy()) ? " " : other_.getStrategy();
		// String thisSortId = String.format("%-5s%s%s", this.getSymbol(), thisStrategyCode, this.getOccId());
		// String otherSortId = String.format("%-5s%s%s", other_.getSymbol(), otherStrategyCode, other_.getOccId());
		int result = getSortKey().compareTo(other_.getSortKey());
		return result;
	}

	@Override
	public String toString() {
		return getLabel();
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public double getScoreExternal() {
		return scoreExternal;
	}

	public void setScoreExternal(Double score) {
		validateNumber(score);
		this.scoreExternal = score;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Double getPriceClose() {
		return priceClose;
	}

	public void setPriceClose(Double priceClose) {
		validateNumber(priceClose);
		this.priceClose = priceClose;
	}

	public boolean isTriggered() {
		return isTriggered;
	}

	public void setTriggered(boolean isTriggered) {
		this.isTriggered = isTriggered;
	}

	public String getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(String interval) {
		this.periodicity = interval;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getFlags() {
		return flags;
	}

	public void setFlags(String flags) {
		this.flags = flags;
	}

	public Double getStrikeA() {
		return strikeA;
	}

	public void setStrikeA(Double strikeA) {
		validateNumber(strikeA);
		this.strikeA = strikeA;
	}

	public Double getStrikeB() {
		return strikeB;
	}

	public void setStrikeB(Double strikeB) {
		validateNumber(strikeB);
		this.strikeB = strikeB;
	}

	public String getSortKey() {
		return sortKey;
	}

	public void setSortKey(String sortKey) {
		this.sortKey = sortKey;
	}

	private void validateNumber(Double d_) {
		if (d_ == Double.NaN || d_ == Double.NEGATIVE_INFINITY || d_ == Double.POSITIVE_INFINITY) {
			throw new ATException("Invalid number");
		}
	}

	public Double getProbability() {
		return probability;
	}

	public void setProbability(Double probability) {
		this.probability = probability;
	}

}
