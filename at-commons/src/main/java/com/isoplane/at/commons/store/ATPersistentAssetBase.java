package com.isoplane.at.commons.store;

import java.time.LocalDate;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
//import org.junit.Ignore;
//import org.junit.Ignore;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.util.ATFormats;

public abstract class ATPersistentAssetBase extends ATPersistentLookupBase implements Comparable<ATPersistentAssetBase>, IATAssetConstants {

	private static final long serialVersionUID = 1L;

	transient private Date _expirationDate;
	transient private String _label;
	transient private Date _tradeDate;

	transient protected Boolean _isOption;

	public ATPersistentAssetBase() {
	}

	public ATPersistentAssetBase(String occId_) {
		setOccId(occId_);
	}

	public ATPersistentAssetBase(Map<String, Object> data_) {
		super(data_);
	}

	protected void setOccId(String occId_) {
		setAtId(occId_);
		_isOption = isOption(occId_);
		if (!_isOption) {
			setType(STOCK_CODE);
		} else {
			String symStr = occId_.substring(0, 6).trim();
			setUnderlyingOccId(symStr);

			int century = LocalDate.now().getYear() / 100;
			String dateStr = occId_.substring(6, 12);
			setExpirationDateStr(String.format("%d%s", century, dateStr));

			String rightStr = occId_.substring(12, 13);
			setOptionRight(rightStr);
			String strikeStr = occId_.substring(13);
			Double strike = Double.parseDouble(strikeStr) / 1000.0;
			setStrike(strike);
		}
	}

	public String getUnderlyingOccId() {
		return (String) super.get(OCCID_UNDERLYING);
	}

	public void setUnderlyingOccId(String occId_) {
		super.put(OCCID_UNDERLYING, occId_);
	}

	public String getOptionRight() {
		return (String) super.get(OPTION_RIGHT);
	}

	public void setOptionRight(String right_) {
		if (CALL.equals(right_)) {
			setType(OPTION_CALL_CODE);
		} else if (PUT.equals(right_)) {
			setType(OPTION_PUT_CODE);
		}
		super.put(OPTION_RIGHT, right_);
	}

	public String getExpirationDateStr() {
		return (String) super.get(EXPIRATION_DATE_STR_OLD);
	}

	public void setExpirationDateStr(String yyyyMMdd_) {
		super.put(EXPIRATION_DATE_STR_OLD, yyyyMMdd_);
	}

	public Double getStrike() {
		return (Double) super.get(STRIKE);
	}

	public void setStrike(Double strike_) {
		super.put(STRIKE, strike_);
	}

	public Date getExpirationDate() {
		if (_expirationDate == null) {
			String dstr = this.getExpirationDateStr();
			if (StringUtils.isBlank(dstr) && isOption()) {
				// TODO: Investigate why this condition can occur
				dstr = getOccId().substring(6, 12);
			}
			if (dstr != null) {
				try {
					if (dstr.length() == 6) {
						int century = LocalDate.now().getYear() / 100;
						dstr = String.format("%d%s", century, dstr);
						setExpirationDateStr(dstr);
					}
					_expirationDate = ATFormats.DATE_yyyyMMdd.get().parse(dstr);
				} catch (Exception ex) {
					throw new ATException(String.format("Date error [%s / '%s']", getOccId(), dstr), ex);
				}
			}
		}
		// if(_expirationDate==null) {
		// // String dstr = getOccId().substring(5,11);
		// // String d2 = dstr;
		// }
		return _expirationDate;
	}

	public void setExpirationDate(Date _expirationDate) {
		this._expirationDate = _expirationDate;
	}

//	@Ignore
	public Date getTradeDate() {
		return _tradeDate;
	}

//	@Ignore
	public void setTradeDate(Date tradeDate_) {
		this._tradeDate = tradeDate_;
	}

	abstract protected String getSortId();
	// {
	// String str = this.getSymbol();
	// if (this.getUnderlyingOccId() != null) {
	// str += this.getOptionRight() + this.getOccId();
	// }
	// return str;
	// }

	public String getOccId() {
		return super.getAtId();
	}

	public String getLabel() {
		try {
			if (_label == null) {
				if (isOption()) {
					String sym = getSymbol();
					Date exDate = getExpirationDate();
					String right = getOptionRight();
					_label = ATFormats.toLabel(sym, exDate, right, getStrike());
					// String dstr = getExpirationDateStr();
					// String right = getOptionRight();
					// Double strike = getStrike();
					// if (dstr != null && right != null && strike != null) {
					// _label = String.format("%-5s%s %s%7.2f", getOccId(), dstr.substring(2), right, strike);
					// }
				} else {
					_label = getOccId();
				}
			}
			return _label;
		} catch (Exception ex) {
			throw new ATException(String.format("Error [%s]", getOccId()), ex);
		}
	}

	public String getSymbol() {
		String str = isOption() ? getUnderlyingOccId() : getOccId();
		return str;
	}

	@Override
	public boolean equals(Object other_) {
		if (other_ == null || !(other_ instanceof ATPersistentAssetBase))
			return false;
		return this.getSortId().equals(((ATPersistentAssetBase) other_).getSortId());
	}

	@Override
	public int hashCode() {
		if (getSortId() == null) {
			return super.hashCode();
		} else {
			return getSortId().hashCode();
		}
	}

	@Override
	public int compareTo(ATPersistentAssetBase other_) {
		String thisSortId = getSortId();
		if (thisSortId == null) {
			throw new ATException(String.format("Sort ID null [%s - %s - %s]", this, getSymbol(), getUnderlyingOccId()));
		}
		String otherSortId = other_ != null ? other_.getSortId() : null;
		if (otherSortId == null) {
			throw new ATException(String.format("Sort ID null [%s]", other_));
		}
		int result = thisSortId.compareTo(otherSortId);
		return result;
	}

	@Override
	public String toString() {
		return StringUtils.isNotBlank(getOccId()) ? getOccId() : super.toString();
	}

	static public boolean isOption(String occId_) {
		return occId_ != null && occId_.length() > 7;
	}

	public boolean isOption() {
		if (_isOption == null) {
			_isOption = isOption(getOccId());
		}
		return _isOption;
	}

	public boolean isIndex() {
		String occId = getOccId();
		return occId != null && occId.startsWith("$");
	}

}
