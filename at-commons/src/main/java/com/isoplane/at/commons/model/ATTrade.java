package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATTrade extends HashMap<String, Object> implements IATTrade {

	private static final long serialVersionUID = 1L;

	public static final String DETAILS = "details";
	public static final String ORIGINAL_ID = "_origid";

	static public ATTrade create(IATTrade tmpl_) {
		ATTrade trd = new ATTrade(tmpl_);
		return trd;
	}

	public ATTrade() {
	}

	public ATTrade(IATTrade template_) {
		this.setAtId(template_.getAtId());
		this.setOccId(template_.getOccId());
		this.setTraceId(template_.getTraceId());
		this.setPxNet(template_.getPxNet());
		this.setPxTrade(template_.getPxTrade());
		this.setPxTradeTotal(template_.getPxTradeTotal());
		this.setSzTrade(template_.getSzTrade());
		this.setSzTradeActive(template_.getSzTradeActive());
		this.setTradeAccount(template_.getTradeAccount());
		this.setTradeComment(template_.getTradeComment());
		this.setTradeGroup(template_.getTradeGroup());
		this.setTradeType(template_.getTradeType());
		this.setTradeUserId(template_.getTradeUserId());
		this.setTsTrade(template_.getTsTrade());
		this.setTsTrade2(template_.getTsTrade2());
	}

	public ATTrade(Map<String, Object> map_) {
		String id = IATLookupMap.getAtId(map_);
		IATLookupMap.setAtId(this, id);
		String occId = IATTrade.getOccId(map_);
		IATTrade.setOccId(this, occId);
		String traceId = IATTrade.getTraceId(map_);
		IATTrade.setTraceId(this, traceId);
		Double pxNet = IATTrade.getPxNet(map_);
		IATTrade.setPxNet(this, pxNet);
		Double pxTrade = IATTrade.getPxTrade(map_);
		IATTrade.setPxTrade(this, pxTrade);
		Double pxTradeTotal = IATTrade.getPxTradeTotal(map_);
		IATTrade.setPxTradeTotal(this, pxTradeTotal);
		Double szTrade = IATTrade.getSzTrade(map_);
		IATTrade.setSzTrade(this, szTrade);
		Double szTradeActive = IATTrade.getSzTradeActive(map_);
		IATTrade.setSzTradeActive(this, szTradeActive);
		String account = IATTrade.getTradeAccount(map_);
		IATTrade.setTradeAccount(this, account);
		String comment = IATTrade.getTradeComment(map_);
		IATTrade.setTradeComment(this, comment);
		String group = IATTrade.getTradeGroup(map_);
		IATTrade.setTradeGroup(this, group);
		Integer type = IATTrade.getTradeType(map_);
		IATTrade.setTradeType(this, type);
		String userId = IATTrade.getTradeUserId(map_);
		IATTrade.setTradeUserId(this, userId);
		Long ts = IATTrade.getTsTrade2(map_);
		IATTrade.setTsTrade2(this, ts);

		String origId = IATAsset.getString(this, ORIGINAL_ID);
		IATAsset.setRemovableValue(this, ORIGINAL_ID, origId);

		String ul = IATTrade.getUnderlyingSymbol(map_);
		IATTrade.setUnderlyingSymbol(this, ul);
		// this.setTsTrade(template_.getTsTrade());
	}

	public ATTrade prep4Save() {
		ATTrade trade = new ATTrade((IATTrade) this);
		Integer ttype = trade.getTradeType();
		if (TYPES_IMMEDIATE.contains(ttype)) {
			trade.remove(IATAssetConstants.SIZE_TRADE);
			trade.remove(IATAssetConstants.SIZE_ACTIVE);
		}
		String occId = trade.getOccId();
		if (occId.length() > 6) {
			String dateStr = occId.substring(6, 12);
			trade.put(IATAssetConstants.EXPIRATION_DATE_STR, dateStr);
		}
		if (trade.getSzTrade() != null && trade.getSzTradeActive() == null) {
			trade.setSzTradeActive(trade.getSzTrade());
		}
		return trade;
	}

	@Override
	public int compareTo(Object other_) {
		String otherId = other_ != null && (other_ instanceof ATTrade) ? ((ATTrade) other_).getAtId() : null;
		if (otherId == null)
			return -1;
		String atId = this.getAtId();
		if (atId == null)
			return 1;
		return atId.compareTo(otherId);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	public String getOriginalAtId() {
		return IATAsset.getString(this, ORIGINAL_ID);
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getOccId() {
		return IATTrade.getOccId(this);
	}

	public void setOccId(String value_) {
		IATTrade.setOccId(this, value_);
	}

	@Override
	public String getTraceId() {
		return IATTrade.getTraceId(this);
	}

	public void setTraceId(String value_) {
		IATTrade.setTraceId(this, value_);
	}

	@Override
	public String getTradeUserId() {
		return IATTrade.getTradeUserId(this);
	}

	public void setTradeUserId(String value_) {
		IATTrade.setTradeUserId(this, value_);
	}

	@Override
	public Date getTsTrade() {
		return IATTrade.getTsTrade(this);
	}

	public void setTsTrade(Date value_) {
		IATTrade.setTsTrade(this, value_);
	}

	@Override
	public Long getTsTrade2() {
		return IATTrade.getTsTrade2(this);
	}

	public void setTsTrade2(Long value_) {
		IATTrade.setTsTrade2(this, value_);
	}

	@Override
	public Double getSzTrade() {
		return IATTrade.getSzTrade(this);
	}

	public void setSzTrade(Double value_) {
		IATTrade.setSzTrade(this, value_);
	}

	@Override
	public Double getSzTradeActive() {
		return IATTrade.getSzTradeActive(this);
	}

	public void setSzTradeActive(Double value_) {
		IATTrade.setSzTradeActive(this, value_);
	}

	@Override
	public Double getPxNet() {
		return IATTrade.getPxNet(this);
	}

	public void setPxNet(Double value_) {
//		if (value_ != null && Double.isNaN(value_)) {
//			int i = 0;
//		}
		IATTrade.setPxNet(this, value_);
	}

	@Override
	public Double getPxTrade() {
		return IATTrade.getPxTrade(this);
	}

	public void setPxTrade(Double value_) {
		IATTrade.setPxTrade(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATTrade.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATTrade.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxUl() {
		return IATTrade.getPxUl(this);
	}

	public void setPxUl(Double value_) {
		IATTrade.setPxUl(this, value_);
	}

	@Override
	public Integer getTradeType() {
		return IATTrade.getTradeType(this);
	}

	public void setTradeType(int value_) {
		IATTrade.setTradeType(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATTrade.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		IATTrade.setTradeAccount(this, value_);
	}

	@Override
	public String getTradeComment() {
		return IATTrade.getTradeComment(this);
	}

	public void setTradeComment(String value_) {
		IATTrade.setTradeComment(this, value_);
	}

	@Override
	public String getTradeGroup() {
		return IATTrade.getTradeGroup(this);
	}

	public void setTradeGroup(String value_) {
		IATTrade.setTradeGroup(this, value_);
	}

	public void setDetails(Set<ATTrade> value_) {
		super.put(DETAILS, value_);
	}

	@SuppressWarnings("unchecked")
	public Set<ATTrade> getDetails() {
		return (Set<ATTrade>) super.get(DETAILS);
	}

	@Override
	public String toString() {
		return IATTrade.toString(this);
	}

}
