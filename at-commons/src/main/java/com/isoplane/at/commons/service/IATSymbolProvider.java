package com.isoplane.at.commons.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;

public interface IATSymbolProvider extends IATService {

	void register(IATSymbolListener listener);

	Set<String> getExpirations();

	Map<String, ATSymbol> getSymbols(String... group);

	List<IATSymbol> lookupSymbol(String symbol);

	Map<String, ATOptionExpiration> getOptionExpirations(Collection<String> symbols);

	void addDynamicSymbols(String userId, List<String> occIds);

	Map<String, Set<String>> getCategories();

	Map<String, String> getSymbolConversions(String srcId);

}
