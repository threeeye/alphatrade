package com.isoplane.at.commons.model;

import java.util.Map;

public interface IATUserNote {

	static final String NOTE = "note";

	String getUserId();

	String getOccId();

	String getNote();

	static String getUserId(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.USER_ID);
	}

	static void setUserId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.USER_ID, value_);
	}

	static String getOccId(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static String getNote(Map<String, Object> map_) {
		return (String) map_.get(NOTE);
	}

	static void setNote(Map<String, Object> map_, String value_) {
		setMapValue(map_, NOTE, value_);
	}

	static void setMapValue(Map<String, Object> map_, String key_, Object value_) {
		if (value_ == null) {
			map_.remove(key_);
		} else {
			map_.put(key_, value_);
		}
	}

	static String toString(IATUserNote alert_) {
		String str = String.format("%s|%s|%s", alert_.getUserId(), alert_.getOccId(), alert_.getNote());
		return str;
	}

}
