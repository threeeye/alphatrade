package com.isoplane.at.commons.model;

public interface IATOption {
	
	String getOccId();

	Double getStrike();
	String getRight();
	String getExpDS6();
	Double getOptionMultiplier();
	
	Double getPxLast();
	Integer getSzLast();
	Long getTsLast();
	
	Double getPxAsk();
	Integer getSzAsk();
	Double getPxBid();
	Integer getSzBid();
	
	Double getPxTime();
	Double getPxTheo();
	Double getVolatility();
	
	Double getDelta();
	Double getGamma();
	Double getRho();
	Double getTheta();
	Double getVega();

}
