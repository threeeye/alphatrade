package com.isoplane.at.commons.store;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATEquityDescription;

public class ATPersistentEquityDescription extends ATPersistentLookupBase
		implements IATEquityDescription, Comparable<ATPersistentEquityDescription>, IATAssetConstants {

	private static final long serialVersionUID = 1L;

	@Override
	public String getOccId() {
		return getAtId();
	}

	public void setOccId(String value_) {
		setAtId(value_);
	}

	@Override
	public String getLabel() {
		return getOccId();
	}

	@Override
	public String getSymbol() {
		return getOccId();
	}

	@Override
	public String getDescription() {
		return (String) get(DESCRIPTION);
	}

	public void setDescription(String value_) {
		put(DESCRIPTION, value_);
	}

	@Override
	public String getIndustry() {
		return (String) get(INDUSTRY);
	}

	public void setIndustry(String value_) {
		put(INDUSTRY, value_);
	}

	@Override
	public String getSector() {
		return (String) get(SECTOR);
	}

	public void setSector(String value_) {
		put(SECTOR, value_);
	}

//	@Override
	public Set<String> getIndex() {
		Set<String> result = new TreeSet<>();
		String idxStr = (String) get(INDEX);
		if (StringUtils.isNotBlank(idxStr)) {
			String[] idxArray = idxStr.split(",");
			result.addAll(Arrays.asList(idxArray));
		}
		return result;
	}

	public void setIndex(Collection<String> index_) {
		if (index_ != null && !index_.isEmpty()) {
			String idxStr = String.join(",", index_);
			put(INDEX, idxStr);
		}
	}

	@Override
	public int compareTo(ATPersistentEquityDescription other_) {
		return this.getOccId().compareTo(other_.getOccId());
	}

	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public String getSource() {
		return (String) super.get(IATAssetConstants.SOURCE);
	}

}