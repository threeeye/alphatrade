package com.isoplane.at.commons.util;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATCallable implements Callable<Object> {

	static final Logger log = LoggerFactory.getLogger(ATCallable.class);

	private String _id;
	private IATIdentifyableRunnable _run;
	private long _delayMs;
	private long _timeoutMs;
	private long _scheduleTime;
	private boolean _isCancellable;

	public ATCallable(String id_, IATIdentifyableRunnable run_, long delayMs_, long timeoutMs_) {
		_id = id_;
		_run = run_;
		_delayMs = delayMs_;
		_timeoutMs = timeoutMs_;
		_isCancellable = true;
	}

	@Override
	public Object call() {
		try {
			_run.run();
		} catch (Exception ex) {
			log.error(String.format("Error in [%s]", _id), ex);
		}
		return _id;
	}

	public String getId() {
		return _id;
	}

	public long getDelay() {
		return _delayMs;
	}

	public long getTimeout() {
		return _timeoutMs;
	}

	public long getScheduleTime() {
		return _scheduleTime;
	}

	public void setScheduleTime(long scheduleTime_) {
		this._scheduleTime = scheduleTime_;
	}

	@Override
	public String toString() {
		return _id;
	}

	public boolean isCancellable() {
		return _isCancellable;
	}

	public void setCancellable(boolean isCancellable) {
		this._isCancellable = isCancellable;
	}

	/** Clean up resources */
	public void purge() {
		try {
			_run.purge();
		} catch (Exception ex) {
			log.error(String.format("Error purging [%s]", _id), ex);
		}
	}

}
