package com.isoplane.at.commons.model.alertcriteria;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;

public class ATFalseCriteria extends ATBaseCriteria {

	static public ATFalseCriteria create(String occId_) {
		if (StringUtils.isBlank(occId_))
			return null;
		String criteria = "➜ false";
		ATFalseCriteria crit = new ATFalseCriteria(occId_, criteria);
		return crit;
	}

	private ATFalseCriteria(String occId_, String criteria_) {
		super(null, occId_, criteria_);
	}

	@Override
	@Deprecated
	public Boolean test(ATSecurity sec_) {
		if (sec_ != null && getOccId().equals(sec_.getOccId()))
			return false;
		return true;
	}

	@Override
	public Boolean test(IATMarketData data_, IATStatistics stats_) {
		if (data_ != null && isOccIdMatch(data_.getAtId()))
			return false;
		return true;
	}

	@Override
	@Deprecated
	public Boolean test(ATPersistentSecurity sec_) {
		if (sec_ != null && getOccId().equals(sec_.getOccId()))
			return false;
		return true;
	}

	@Override
	@Deprecated
	public String label(ATPersistentSecurity sec_) {
		return label();
	}

	@Override
	public String label(IATMarketData data_, IATStatistics stats) {
		return label();
	}

}
