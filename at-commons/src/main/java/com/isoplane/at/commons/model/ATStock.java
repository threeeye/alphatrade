package com.isoplane.at.commons.model;

public class ATStock extends ATSecurity implements IATEquityDescription {

	private static final long serialVersionUID = 1L;

	private String industry;
	private String sector;
	private String description;
	private boolean hasWeeklyOptions = false;

	private Integer statDayCount;
	private Double priceAtr;
	private Double priceHV;
//	private Double priceStdDev;
//	private Double priceVolatility;
	private Double priceWavg;
	private Double priceResistance;
	private Double priceSupport;

	transient private boolean _isOptionActive = false;

	public ATStock() {
		super.setSecType(STOCK_TYPE);
	}

	@Override
	protected String getSortId() {
		return this.getOccId();
	}

	@Override
	public void populateFrom(ATSecurity other_) {
		populateFrom(other_, false);
	}

	@Override
	public ATStock clone() {
		ATStock clone = new ATStock();
		clone.populateFrom(this);
		return clone;
	}

	@Override
	public boolean update(IATQuote quote_) {
		return super.updateBase(quote_);
	}

	public void populateFrom(ATSecurity other_, boolean sanitize_) {
		if (other_ == null || !(other_ instanceof ATStock))
			return;
		super.populateFrom(other_);
		if (!sanitize_) {
			ATStock other = (ATStock) other_;
			if (isChangedNotNull(this.sector, other.sector)) {
				setChanged(true);
				this.sector = other.sector;
			}
			if (isChangedNotNull(this.industry, other.industry)) {
				setChanged(true);
				this.industry = other.industry;
			}
			if (isChangedNotNull(this.description, other.description)) {
				setChanged(true);
				this.description = other.description;
			}
			if (isChangedNotNull(this.priceWavg, other.getPriceWavg())) {
				setChanged(true);
				this.priceWavg = other.getPriceWavg();
			}
			if (isChangedNotNull(this.priceAtr, other.getPriceAtr())) {
				setChanged(true);
				this.priceAtr = other.getPriceAtr();
			}
			if (isChangedNotNull(this.priceHV, other.getPriceHV())) {
				setChanged(true);
				this.priceHV = other.getPriceHV();
			}
			if (isChangedNotNull(this.statDayCount, other.getStatDayCount())) {
				setChanged(true);
				this.statDayCount = other.getStatDayCount();
			}
//			if (isChangedNotNull(this.priceStdDev, other.getPriceStdDev())) {
//				setChanged(true);
//				this.priceStdDev = other.getPriceStdDev();
//			}
//			if (isChangedNotNull(this.priceVolatility, other.getPriceVolatility())) {
//				setChanged(true);
//				this.priceVolatility = other.getPriceVolatility();
//			}
			if (isChangedNotNull(this.priceResistance, other.getPriceResistance())) {
				setChanged(true);
				this.priceResistance = other.getPriceResistance();
			}
			if (isChangedNotNull(this.priceSupport, other.getPriceSupport())) {
				setChanged(true);
				this.priceSupport = other.getPriceSupport();
			}
			this.hasWeeklyOptions = other.hasWeeklyOptions;
		}
	}

	public boolean isOptionActive() {
		return _isOptionActive;
	}

	public void setOptionActive(boolean active) {
		this._isOptionActive = active;
	}

	public boolean hasWeeklyOptions() {
		return hasWeeklyOptions;
	}

	public void setHasWeeklyOptions(boolean hasWeeklyOptions) {
		this.hasWeeklyOptions = hasWeeklyOptions;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public Double getPriceSupport() {
		return priceSupport;
	}

	public void setPriceSupport(Double priceSupport) {
		this.priceSupport = priceSupport;
	}

	public Double getPriceResistance() {
		return priceResistance;
	}

	public void setPriceResistance(Double priceResistance) {
		this.priceResistance = priceResistance;
	}

	public Double getPriceWavg() {
		return priceWavg;
	}

	public void setPriceWavg(Double priceWavg) {
		this.priceWavg = priceWavg;
	}

//	public Double getPriceStdDev() {
//		return priceStdDev;
//	}
//
//	public void setPriceStdDev(Double priceStdDev) {
//		this.priceStdDev = priceStdDev;
//	}

	public Double getPriceAtr() {
		return priceAtr;
	}

	public void setPriceAtr(Double priceAtr) {
		this.priceAtr = priceAtr == null || priceAtr.isNaN() ? null : priceAtr;
	}

//	public Double getPriceVolatility() {
//		return priceVolatility;
//	}
//
//	public void setPriceVolatility(Double priceVolatility) {
//		this.priceVolatility = priceVolatility;
//	}

	public Double getPriceHV() {
		return priceHV;
	}

	public void setPriceHV(Double priceHV) {
		this.priceHV = priceHV;
	}

	public Integer getStatDayCount() {
		return statDayCount;
	}

	public void setStatDayCount(Integer statDayCount) {
		this.statDayCount = statDayCount;
	}

}
