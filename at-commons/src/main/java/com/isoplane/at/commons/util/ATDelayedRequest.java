package com.isoplane.at.commons.util;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public abstract class ATDelayedRequest implements Delayed {

	private long _delay;
	private long _inserted;

	// protected ATDelayedRequest(long delay_) {
	// _delay = delay_;
	// }

	abstract public void run();

	abstract public Object getIdentifyableId();

	@Override
	public int compareTo(Delayed other_) {
		ATDelayedRequest other = (ATDelayedRequest) other_;
		if (getInserted() < other.getInserted())
			return -1;
		if (getInserted() > other.getInserted())
			return 1;
		return 0;
	}

	@Override
	public long getDelay(TimeUnit tu_) {
		long remaining = _inserted + _delay - System.nanoTime();
		long delay = tu_.convert(remaining, TimeUnit.NANOSECONDS);
		return delay;
	}

	@Override
	public boolean equals(Object other_) {
		if (!(other_ instanceof ATDelayedRequest) || this.getIdentifyableId() == null) {
			return false;
		}
		return this.getIdentifyableId().equals(((ATDelayedRequest) other_).getIdentifyableId());
	}

	public long getDelay() {
		return _delay;
	}

	/**
	 * @param delay_
	 *            - Delay in ns
	 */
	public void setDelay(long delay_) {
		this._delay = delay_;
	}

	public long getInserted() {
		return _inserted;
	}

	/**
	 * @param inserted_
	 *            - Insert nano time
	 */
	public void setInserted(long inserted_) {
		this._inserted = inserted_;
	}

}
