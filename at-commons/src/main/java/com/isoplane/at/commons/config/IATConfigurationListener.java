package com.isoplane.at.commons.config;

import java.util.Set;

public interface IATConfigurationListener {

	void configurationAlert(Set<String> changedKeys);
}
