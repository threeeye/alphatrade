package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;

public abstract class ATSecurity implements Comparable<ATSecurity>, IATChangeable {

	private static final long serialVersionUID = 1L;

	// static final Logger log = LoggerFactory.getLogger(ATSecurity.class);

	static public final String STOCK_TYPE = "S";
	static public final String OPTION_TYPE = "O";

	public ATSecurity() {

	}

	private String occId;
	private String label;
	private String symbol;
	private String secType;
	private Double price; // Theoretical price to use
	private Double priceAsk;
	private Double priceBid;
	private Double priceClose;
	private Double priceHigh;
	private Double priceLast;
	private Double priceLow;
	private Double priceOpen;
	// private Double priceTheo;

	private Double price3mLow;
	private Double price3mHigh;
	private Double price3mAvg;
	private Double price1yLow;
	private Double price1yHigh;
	private Double price1yAvg;
	private Double price60dAvg;
	private Double price60dVar;

	private Integer volume;
	private Integer sizeAsk;
	private Integer sizeBid;
	private Integer sizeLast;
	private Date dateAsk;
	private Date dateBid;
	private Date dateLast;

	transient private Date lastUpdate = new Date();
	transient private Boolean isActive;
	transient private boolean isChanged;
	transient private Set<String> userIds = new HashSet<>();

	public abstract ATSecurity clone();

	public void populateFrom(ATSecurity other_) {
		if (other_ == null)
			return;

		if (isChangedNotNull(this.occId, other_.getOccId())) {
			setChanged(true);
			this.occId = other_.getOccId();
		}
		if (isChangedNotNull(this.label, other_.getLabel())) {
			setChanged(true);
			this.label = other_.label;
		}
		if (isChangedNotNull(this.symbol, other_.symbol)) {
			setChanged(true);
			this.symbol = other_.symbol;
		}
		if (isChangedNotNull(this.secType, other_.secType)) {
			setChanged(true);
			this.secType = other_.secType;
		}
		if (isChangedNotNull(this.price, other_.getPrice())) {
			setChanged(true);
			this.price = other_.getPrice();
		}
		if (isChangedNotNull(this.priceAsk, other_.priceAsk)) {
			setChanged(true);
			this.priceAsk = other_.priceAsk;
		}
		if (isChangedNotNull(this.priceBid, other_.priceBid)) {
			setChanged(true);
			this.priceBid = other_.priceBid;
		}
		if (isChangedNotNull(this.priceClose, other_.priceClose)) {
			setChanged(true);
			this.priceClose = other_.priceClose;
		}
		if (isChangedNotNull(this.priceHigh, other_.priceHigh)) {
			setChanged(true);
			this.priceHigh = other_.priceHigh;
		}
		if (isChangedNotNull(this.priceLast, other_.priceLast)) {
			setChanged(true);
			this.priceLast = other_.priceLast;
		}
		if (isChangedNotNull(this.priceLow, other_.priceLow)) {
			setChanged(true);
			this.priceLow = other_.priceLow;
		}
		if (isChangedNotNull(this.priceOpen, other_.priceOpen)) {
			setChanged(true);
			this.priceOpen = other_.priceOpen;
		}
		if (isChangedNotNull(this.price1yAvg, other_.price1yAvg)) {
			setChanged(true);
			this.price1yAvg = other_.price1yAvg;
		}
		if (isChangedNotNull(this.price1yHigh, other_.price1yHigh)) {
			setChanged(true);
			this.price1yHigh = other_.price1yHigh;
		}
		if (isChangedNotNull(this.price1yLow, other_.price1yLow)) {
			setChanged(true);
			this.price1yLow = other_.price1yLow;
		}
		if (isChangedNotNull(this.price3mAvg, other_.price3mAvg)) {
			setChanged(true);
			this.price3mAvg = other_.price3mAvg;
		}
		if (isChangedNotNull(this.price3mHigh, other_.price3mHigh)) {
			setChanged(true);
			this.price3mHigh = other_.price3mHigh;
		}
		if (isChangedNotNull(this.price3mLow, other_.price3mLow)) {
			setChanged(true);
			this.price3mLow = other_.price3mLow;
		}

		if (isChangedNotNull(this.price60dAvg, other_.price60dAvg)) {
			setChanged(true);
			this.price60dAvg = other_.price60dAvg;
		}
		if (isChangedNotNull(this.price60dVar, other_.price60dVar)) {
			setChanged(true);
			this.price60dVar = other_.price60dVar;
		}
		// if (isChangedNotNull(this.priceTheo, other_.getPriceTheo())) {
		// setChanged(true);
		// this.priceTheo = other_.getPriceTheo();
		// }
		if (isChangedNotNull(this.volume, other_.volume)) {
			setChanged(true);
			this.volume = other_.volume;
		}
		if (isChangedNotNull(this.sizeAsk, other_.sizeAsk)) {
			setChanged(true);
			this.sizeAsk = other_.sizeAsk;
		}
		if (isChangedNotNull(this.sizeBid, other_.sizeBid)) {
			setChanged(true);
			this.sizeBid = other_.sizeBid;
		}
		if (isChangedNotNull(this.sizeLast, other_.getSizeLast())) {
			setChanged(true);
			this.sizeLast = other_.getSizeLast();
		}
		if (isChangedNotNull(this.dateAsk, other_.dateAsk)) {
			setChanged(true);
			this.dateAsk = other_.dateAsk;
		}
		if (isChangedNotNull(this.dateBid, other_.dateBid)) {
			setChanged(true);
			this.dateBid = other_.dateBid;
		}
		if (isChangedNotNull(this.dateLast, other_.dateLast)) {
			setChanged(true);
			this.dateLast = other_.dateLast;
		}
		if (isChangedNotNull(this.isActive, other_.isActive)) {
			// TODO: Check if this makes sense
			setChanged(true);
			this.isActive = other_.isActive;
		}
		setUserIds(other_.getUserIds());
		this.setLastUpdate(this.isChanged() ? new Date() : other_.getLastUpdate());
	}

	protected boolean isChangedNotNull(Object a_, Object b_) {
		if (a_ == null) {
			return (b_ == null) || ((b_ instanceof Double) && ((Double) b_).isNaN()) ? false : true;
		} else {
			return (b_ == null) || ((b_ instanceof Double) && ((Double) b_).isNaN()) ? false : !a_.equals(b_);
		}
	}

	abstract public boolean update(IATQuote quote);

	public boolean updateBase(IATQuote quote_) {
		if (!this.getOccId().equals(quote_.getOccId())) {
			throw new ATException(String.format("Invalid quote [%s]", quote_ != null ? quote_.getOccId() : null));
		}
		if (isChangedNotNull(this.getPrice(), quote_.getPrice())) {
			setPrice(quote_.getPrice());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceAsk(), quote_.getPriceAsk())) {
			setPriceAsk(quote_.getPriceAsk());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceBid(), quote_.getPriceBid())) {
			setPriceBid(quote_.getPriceBid());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceClose(), quote_.getPriceClose())) {
			setPriceClose(quote_.getPriceClose());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceHigh(), quote_.getPriceHigh())) {
			setPriceHigh(quote_.getPriceHigh());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceLast(), quote_.getPriceLast())) {
			setPriceLast(quote_.getPriceLast());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceLow(), quote_.getPriceLow())) {
			setPriceLow(quote_.getPriceLow());
			setChanged(true);
		}
		if (isChangedNotNull(this.getPriceOpen(), quote_.getPriceOpen())) {
			setPriceOpen(quote_.getPriceOpen());
			setChanged(true);
		}
		if (isChangedNotNull(this.getVolume(), quote_.getVolume())) {
			setVolume(quote_.getVolume());
			setChanged(true);
		}
		if (isChangedNotNull(this.getSizeAsk(), quote_.getSizeAsk())) {
			setSizeAsk(quote_.getSizeAsk());
			setChanged(true);
		}
		if (isChangedNotNull(this.getSizeBid(), quote_.getSizeBid())) {
			setSizeBid(quote_.getSizeBid());
			setChanged(true);
		}
		if (isChangedNotNull(this.getDateAsk(), quote_.getDateAsk())) {
			setDateAsk(quote_.getDateAsk());
			setChanged(true);
		}
		if (isChangedNotNull(this.getDateBid(), quote_.getDateBid())) {
			setDateBid(quote_.getDateBid());
			setChanged(true);
		}
		if (isChangedNotNull(this.getDateLast(), quote_.getDateLast())) {
			setDateLast(quote_.getDateLast());
			setChanged(true);
		}
		// security_.setDescription(temp.description); TODO: Disabled due to Tradier bug
		return true;
	}

	protected abstract String getSortId();

	@Override
	public boolean equals(Object other_) {
		if (other_ == null || !(other_ instanceof ATSecurity))
			return false;
		return this.getSortId().equals(((ATSecurity) other_).getSortId());
	}

	@Override
	public int hashCode() {
		if (getSortId() == null) {
			return super.hashCode();
		} else {
			return getSortId().hashCode();
		}
	}

	@Override
	public int compareTo(ATSecurity other_) {
		String otherSortId = other_ != null ? other_.getSortId() : null;
		String thisSortId = getSortId();
		int result = thisSortId.compareTo(otherSortId);
		return result;
	}

	@Override
	public String toString() {
		return StringUtils.isNotBlank(getOccId()) ? getOccId() : super.toString();
	}

	public boolean isOwnedBy(String userId_) {
		if (StringUtils.isBlank(userId_) || userIds == null)
			return false;
		return userIds.contains(userId_);
	}

	public String getOccId() {
		return occId;
	}

	public void setOccId(String occId) {
		this.occId = occId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getSecType() {
		return secType;
	}

	public void setSecType(String secType) {
		this.secType = secType;
	}

	public Double getPriceHigh() {
		return priceHigh;
	}

	public void setPriceHigh(Double priceHigh) {
		this.priceHigh = priceHigh;
	}

	public Double getPriceLow() {
		return priceLow;
	}

	public void setPriceLow(Double priceLow) {
		this.priceLow = priceLow;
	}

	public Double getPriceAsk() {
		return priceAsk;
	}

	public void setPriceAsk(Double priceAsk) {
		this.priceAsk = priceAsk;
	}

	public Double getPriceBid() {
		return priceBid;
	}

	public void setPriceBid(Double priceBid) {
		this.priceBid = priceBid;
	}

	public Double getPriceLast() {
		return priceLast;
	}

	public void setPriceLast(Double priceLast) {
		this.priceLast = priceLast;
	}

	public Double getPriceOpen() {
		return priceOpen;
	}

	public void setPriceOpen(Double priceOpen) {
		this.priceOpen = priceOpen;
	}

	public Double getPriceClose() {
		return priceClose;
	}

	public void setPriceClose(Double priceClose) {
		this.priceClose = priceClose;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public Integer getSizeBid() {
		return sizeBid;
	}

	public void setSizeBid(Integer sizeBid) {
		this.sizeBid = sizeBid;
	}

	public Integer getSizeAsk() {
		return sizeAsk;
	}

	public void setSizeAsk(Integer sizeAsk) {
		this.sizeAsk = sizeAsk;
	}

	public Date getDateAsk() {
		return dateAsk;
	}

	public void setDateAsk(Date dateAsk) {
		this.dateAsk = dateAsk;
	}

	public Date getDateBid() {
		return dateBid;
	}

	public void setDateBid(Date dateBid) {
		this.dateBid = dateBid;
	}

	public Date getDateLast() {
		return dateLast;
	}

	public void setDateLast(Date dateLast) {
		this.dateLast = dateLast;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Deprecated
	public boolean isActive() {
		return userIds != null && !userIds.isEmpty();
		// return isActive != null ? isActive : false;
	}

	// public void setActive(boolean isActive) {
	// this.isActive = isActive;
	// }

	public Double getPrice3mLow() {
		return price3mLow;
	}

	public void setPrice3mLow(Double price3mLow) {
		this.price3mLow = price3mLow;
	}

	public Double getPrice3mHigh() {
		return price3mHigh;
	}

	public void setPrice3mHigh(Double price3mHigh) {
		this.price3mHigh = price3mHigh;
	}

	public Double getPrice3mAvg() {
		return price3mAvg;
	}

	public void setPrice3mAvg(Double price3mAvg) {
		this.price3mAvg = price3mAvg;
	}

	public Double getPrice1yLow() {
		return price1yLow;
	}

	public void setPrice1yLow(Double price1yLow) {
		this.price1yLow = price1yLow;
	}

	public Double getPrice1yHigh() {
		return price1yHigh;
	}

	public void setPrice1yHigh(Double price1yHigh) {
		this.price1yHigh = price1yHigh;
	}

	public Double getPrice1yAvg() {
		return price1yAvg;
	}

	public void setPrice1yAvg(Double price1yAvg) {
		this.price1yAvg = price1yAvg;
	}

	public Double getPrice() {
		Double p = price != null ? price : priceLast != null ? priceLast : priceClose;
		if (p == null && priceAsk != null && priceBid != null) {
			p = (priceAsk + priceBid) / 2.0;
		}
		return p;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public boolean isChanged() {
		return isChanged;
	}

	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}

	public Integer getSizeLast() {
		return sizeLast;
	}

	public void setSizeLast(Integer sizeLast) {
		this.sizeLast = sizeLast;
	}

	public Set<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(Set<String> userIds) {
		this.userIds = userIds;
	}

	public Double getPrice60dAvg() {
		return price60dAvg;
	}

	public void setPrice60dAvg(Double price60dAvg) {
		this.price60dAvg = price60dAvg;
	}

	public Double getPrice60dVar() {
		return price60dVar;
	}

	public void setPrice60dVar(Double price60dStdev) {
		this.price60dVar = price60dStdev;
	}

	// public Double getPriceTheo() {
	// return priceTheo;
	// }
	//
	// public void setPriceTheo(Double priceTheo) {
	// this.priceTheo = priceTheo;
	// }

}
