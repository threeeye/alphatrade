package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ATSymbol implements IATSymbol {

	private String id;
	private String model;
	private String name;
	private String description;
	private String sector;
	private String industry;
	protected String source;
	private Boolean isSP500;
	private Boolean isR3000;
	private Set<String> references;
	private Map<String, String> converters;
	private Map<Date, ATOptionChain> options;

	public ATSymbol() {
	}

	public ATSymbol(String id_) {
		setId(id_);
	}

	public ATSymbol(IATSymbol template_) {
		if (template_ != null) {
			setConverters(template_.getConverters());
			setDescription(template_.getDescription());
			setId(template_.getId());
			setIndustry(template_.getIndustry());
			setModel(template_.getModel());
			setName(template_.getName());
			setOptions(template_.getOptions());
			setReferences(template_.getReferences());
			setSector(template_.getSector());
			setSP500(template_.isSP500());
			setR3000(template_.isR3000());
		}
	}

	@Override
	public int compareTo(IATSymbol other_) {
		return IATSymbol.compareTo(this, other_);
	}

	@Override
	public String convert(String provider_) {
		return IATSymbol.convert(this, provider_);
	}

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	@Override
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	@Override
	public String getSource() {
		return source;
	}

	@Override
	public Set<String> getReferences() {
		return references;
	}

	public void setReferences(Set<String> references) {
		this.references = references;
	}

	@Override
	public Map<String, String> getConverters() {
		return converters;
	}

	public void setConverters(Map<String, String> converters) {
		this.converters = converters;
	}

	@Override
	public Map<Date, ATOptionChain> getOptions() {
		return options;
	}

	public void setOptions(Map<Date, ATOptionChain> options) {
		this.options = options;
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public Boolean isSP500() {
		return isSP500;
	}

	public void setSP500(Boolean value_) {
		isSP500 = value_ != null && value_ == true;
		if (isSP500) {
			if (this.references == null) {
				this.references = new HashSet<>();
			}
			this.references.add(IATAssetConstants.SP500);
		} else if (this.references != null) {
			this.references.remove(IATAssetConstants.SP500);
		}
	}

	@Override
	public Boolean isR3000() {
		return isR3000;
	}

	public void setR3000(Boolean value_) {
		isR3000 = value_ != null && value_ == true;
		if (isR3000) {
			if (this.references == null) {
				this.references = new HashSet<>();
			}
			this.references.add(IATAssetConstants.RUSSELL3000);
		} else if (this.references != null) {
			this.references.remove(IATAssetConstants.RUSSELL3000);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ATSymbol other = (ATSymbol) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	// @Override
	// public boolean equals(Object other_) {
	// if (other_ == null || !IATSymbol.class.isAssignableFrom(other_.getClass()))
	// return false;
	// IATSymbol other = (IATSymbol) other_;
	// return this.getId().equals(other.getId());
	// }

}
