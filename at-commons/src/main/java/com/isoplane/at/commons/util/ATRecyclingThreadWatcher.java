package com.isoplane.at.commons.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;

public class ATRecyclingThreadWatcher {

	static final Logger log = LoggerFactory.getLogger(ATRecyclingThreadWatcher.class);

	private List<ATRecyclingThread> _list;
	private Timer _timer;
	private boolean _isRunning;

	public ATRecyclingThreadWatcher() {
		_isRunning = true;
		_list = Collections.synchronizedList(new LinkedList<ATRecyclingThread>());// <ATCyclingThread, Long>());
		_timer = new Timer();
		_timer.schedule(new TimerTask() {

			@Override
			public void run() {
				if (!_isRunning) {
					log.info("Process terminated");
					return;
				}
				try {
					synchronized (_list) {
						List<ATRecyclingThread> newThreads = new ArrayList<>();
						Iterator<ATRecyclingThread> i = _list.iterator();
						while (_isRunning && i.hasNext()) {
							ATRecyclingThread oldThread = i.next();
							// long period = oldThread.getPeriod();
							long timeout = oldThread.getTimeout();
							if (timeout <= 0) {
								timeout = Math.abs(timeout) * oldThread.getPeriod();
							}
							long lastRun = oldThread.getLastRunTime();
							// log.trace(String.format("W: period [%s - %d]", oldThread.getName(), period));
							// log.trace(String.format("W: last run [%s - %d]", oldThread.getName(), lastRun));
							if (System.currentTimeMillis() - timeout > lastRun) { // tolerance factor // (period * 2)
								log.warn(String.format("Resetting thread [%s]", oldThread.getName()));
								i.remove();
								oldThread.quit();
								timeout = 120000L;
								ATRecyclingThread newThread = new ATRecyclingThread(oldThread.getName(), oldThread.getFunction(), timeout);
								newThreads.add(newThread);
							}
						}
						for (ATRecyclingThread t : newThreads) {
							startThread(t);
						}
					}
				} catch (Exception ex) {
					log.error("Error in housekeeping", ex);
				}
			}
		}, 10000, 10000);
	}

	public void startThread(ATRecyclingThread thread_) {
		Long period = thread_ != null ? thread_.getPeriod() : null;
		if (period == null)
			throw new ATException("Thread without period");
		_list.add(thread_);
		thread_.setLastRunTime(System.currentTimeMillis());
		thread_.start();
		log.info(String.format("Started thread   [%s]", thread_.getName()));
	}

	public void shutdown() {
		_isRunning = false;
		for (ATRecyclingThread t : _list) {
			t.quit();
		}
		if (_timer != null) {
			_timer.cancel();
		}
	}
}
