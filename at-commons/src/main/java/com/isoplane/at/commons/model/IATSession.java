package com.isoplane.at.commons.model;

import java.util.List;

public interface IATSession {

	static final String USER_ID = "uid";
	static final String NAME = "name";
	static final String TOKEN = "token";
	static final String TOKEN_ALL = "all";

	static final String CLIENT_IP = "c_ip";
	static final String DATA = "data";
	static final String DATA_SYMBOLS_PORT = "sym_port";
	
	static final String DATA_SYMBOLS_TEMP = "sym_temp";
	static final String DATA_SYMBOLS_TYPE = "sym_type";

	Long getUpdateTime();

	String getEmail();

	String getToken();

	String getClientIp();

	String getUserId();

	String getName();

	List<String> getRoles();

}