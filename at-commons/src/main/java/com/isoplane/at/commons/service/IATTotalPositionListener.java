package com.isoplane.at.commons.service;

import com.isoplane.at.commons.util.EATChangeOperation;

public interface IATTotalPositionListener {

	void notify(EATChangeOperation operation, String occId);

}
