package com.isoplane.at.commons.config;

public interface IATConfiguration {

	void registerListener(IATConfigurationListener listener);

	Boolean getBoolean(String key);

	boolean getBoolean(String key, boolean fallback);

	Double getDouble(String key);

	double getDouble(String key, double fallback);

	Integer getInt(String key);

	int getInt(String key, int fallback);

	Long getLong(String key);

	long getLong(String key, long fallback);

	String getString(String key);

	String getString(String key, String fallback);

	void printConfiguration();

}
