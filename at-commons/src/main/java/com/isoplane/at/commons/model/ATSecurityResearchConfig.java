package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;

@Deprecated
public class ATSecurityResearchConfig extends HashMap<String, Object> implements IATSecurityResearchConfig {

	private static final long serialVersionUID = 1L;

	public ATSecurityResearchConfig() {
	}

	public ATSecurityResearchConfig(IATSecurityResearchConfig tmpl_) {
		IATAsset.setAtId(this, tmpl_.getAtId());
		IATAsset.setUserId(this, tmpl_.getUserId());
		IATAsset.setName(this, tmpl_.getName());
		IATAsset.setType(this, tmpl_.getType());
		IATAsset.setPxMax(this, tmpl_.getPxMax());
		IATAsset.setPxMin(this, tmpl_.getPxMin());
		IATAsset.setIndustry(this, tmpl_.getIndustry());
		IATAsset.setSector(this, tmpl_.getSector());
		IATAsset.setAvgMode(this, tmpl_.getAvgMode());
		IATAsset.setHVMode(this, tmpl_.getHVMode());
		IATSecurityResearchConfig.setAvgMarginMax(this, tmpl_.getAvgMarginMax());
		IATSecurityResearchConfig.setAvgMarginMin(this, tmpl_.getAvgMarginMin());
		IATSecurityResearchConfig.setEarningsRiskFactor(this, tmpl_.getEarningsRiskFactor());
		IATSecurityResearchConfig.setEquityMaxAge(this, tmpl_.getEquityMaxAge());
		IATSecurityResearchConfig.setHasAlerts(this, tmpl_.hasAlerts());
		IATSecurityResearchConfig.setHasNotes(this, tmpl_.hasNotes());
		IATSecurityResearchConfig.setHVMax(this, tmpl_.getHVMax());
		IATSecurityResearchConfig.setHVMin(this, tmpl_.getHVMin());
		IATSecurityResearchConfig.setOwned(this, tmpl_.isOwned());
		IATSecurityResearchConfig.setPxMaxChange(this, tmpl_.getPxMaxChange());
		IATSecurityResearchConfig.setPxMinChange(this, tmpl_.getPxMinChange());
		IATSecurityResearchConfig.setSkipEarnings(this, tmpl_.isSkipEarnings());
		IATSecurityResearchConfig.setSpreadApyMode(this, tmpl_.getSpreadApyMode());
		IATSecurityResearchConfig.setSpreadWidthPxMax(this, tmpl_.getSpreadWidthPxMax());
		IATSecurityResearchConfig.setSpreadWidthStepMax(this, tmpl_.getSpreadWidthStepMax());
		IATSecurityResearchConfig.setSpreadWidthStepRisk(this, tmpl_.getSpreadWidthStepRisk());
		IATSecurityResearchConfig.setStrategyApyMin(this, tmpl_.getStrategyApyMin());
		IATSecurityResearchConfig.setStrategyDaysMax(this, tmpl_.getStrategyDaysMax());
		IATSecurityResearchConfig.setStrategyMarginMin(this, tmpl_.getStrategyMarginMin());
		IATSecurityResearchConfig.setStrategyMaxCountPerDate(this, tmpl_.getStrategyMaxCountPerDate());
		IATSecurityResearchConfig.setStrategyPxMin(this, tmpl_.getStrategyPxMin());
		IATSecurityResearchConfig.setStrategyRiskMax(this, tmpl_.getStrategyRiskMax());
		IATSecurityResearchConfig.setSymbols(this, tmpl_.getSymbols());
		IATSecurityResearchConfig.setUnderlyingConfigId(this, tmpl_.getUnderlyingConfigId());
	}

	public void cleanup() {
		IATLookupMap.removeNulls(this);
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	@Override
	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getUserId() {
		return IATAsset.getUserId(this);
	}

	@Override
	public void setUserId(String value_) {
		IATAsset.setUserId(this, value_);
	}

	@Override
	public String getType() {
		return IATAsset.getType(this);
	}

	@Override
	public String getName() {
		return IATAsset.getName(this);
	}

	@Override
	public String getUnderlyingConfigId() {
		return IATSecurityResearchConfig.getUnderlyingConfigId(this);
	}

	public ATSecurityResearchConfig getUnderlyingConfig() {
		Object obj = super.get(IATSecurityResearchConfig.UL_CONFIG);
		ATSecurityResearchConfig config = null;
		if (obj != null && obj instanceof Map) {
			config = new ATSecurityResearchConfig();
			@SuppressWarnings("unchecked")
			Map<String, Object> tmpl = (Map<String, Object>) obj;
			config.putAll(tmpl);
		}
		return config;
	}

	@Override
	public Long getEquityMaxAge() {
		return IATSecurityResearchConfig.getEquityMaxAge(this);
	}

	@Override
	public Double getPxMin() {
		return IATAsset.getPxMin(this);
	}

	@Override
	public Double getPxMax() {
		return IATAsset.getPxMax(this);
	}

	@Override
	public Double getPxMinChange() {
		return IATSecurityResearchConfig.getPxMinChange(this);
	}

	@Override
	public Double getPxMaxChange() {
		return IATSecurityResearchConfig.getPxMaxChange(this);
	}

	@Override
	public String getIndustry() {
		return IATAsset.getIndustry(this);
	}

	@Override
	public String getSector() {
		return IATAsset.getSector(this);
	}

	@Override
	public String getAvgMode() {
		return IATAsset.getAvgMode(this);
	}

	@Override
	public Double getAvgMarginMin() {
		return IATSecurityResearchConfig.getAvgMarginMin(this);
	}

	@Override
	public Double getAvgMarginMax() {
		return IATSecurityResearchConfig.getAvgMarginMax(this);
	}

	@Override
	public String getHVMode() {
		return IATAsset.getHVMode(this);
	}

	@Override
	public Double getHVMin() {
		return IATSecurityResearchConfig.getHVMin(this);
	}

	@Override
	public Double getHVMax() {
		return IATSecurityResearchConfig.getHVMax(this);
	}

	@Override
	public Boolean isOwned() {
		return IATSecurityResearchConfig.isOwned(this);
	}

	@Override
	public Boolean hasAlerts() {
		return IATSecurityResearchConfig.hasAlerts(this);
	}

	@Override
	public Boolean hasNotes() {
		return IATSecurityResearchConfig.hasNotes(this);
	}

	@Override
	public Boolean isSkipEarnings() {
		return IATSecurityResearchConfig.isSkipEarnings(this);
	}

	@Override
	public List<String> getSymbols() {
		return IATSecurityResearchConfig.getSymbols(this);
	}

	@Override
	public Double getEarningsRiskFactor() {
		return IATSecurityResearchConfig.getEarningsRiskFactor(this);
	}

	@Override
	public Integer getStrategyDaysMax() {
		return IATSecurityResearchConfig.getStrategyDaysMax(this);
	}

	@Override
	public Double getStrategyApyMin() {
		return IATSecurityResearchConfig.getStrategyApyMin(this);
	}

	@Override
	public Double getStrategyPxMin() {
		return IATSecurityResearchConfig.getStrategyPxMin(this);
	}

	@Override
	public Integer getStrategyMaxCountPerDate() {
		return IATSecurityResearchConfig.getStrategyMaxCountPerDate(this);
	}

	@Override
	public Double getStrategyRiskMax() {
		return IATSecurityResearchConfig.getStrategyRiskMax(this);
	}

	@Override
	public Double getStrategyMarginMin() {
		return IATSecurityResearchConfig.getStrategyMarginMin(this);
	}

	@Override
	public Double getSpreadWidthPxMax() {
		return IATSecurityResearchConfig.getSpreadWidthPxMax(this);
	}

	@Override
	public Double getSpreadWidthStepMax() {
		return IATSecurityResearchConfig.getSpreadWidthStepMax(this);
	}

	@Override
	public Double getSpreadWidthStepRisk() {
		return IATSecurityResearchConfig.getSpreadWidthStepRisk(this);
	}

	@Override
	public String getSpreadApyMode() {
		return IATSecurityResearchConfig.getSpreadApyMode(this);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	public static class ATSecurityResearchConfigPack {
		public String usrId;
		public String type;
		public ATSecurityResearchConfig[] configs;
	}

}
