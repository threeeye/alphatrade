package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Slow changing properties (>24h)
 * 
 * @author miron
 */
public class ATProduct extends HashMap<String, Object>
		implements IATMarketData, IATTrade, IATProductDescription, IATFundamental {

	private static final long serialVersionUID = 1L;

	public static final String DETAILS = "details";
	public static final String MODE = "mode";
	public static final String MODE_DETAIL = "d";
	public static final String MODE_PARENT = "p";
	public static final String PARENT_REF = "p_ref";

	// public String id;
	public Long ts;

	// public Double pxAvg;
	// public String pxAvgMode;
	// public String pxHVMode;
	// public String note;

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public int compareTo(Object other_) {
		String otherId = other_ != null && (other_ instanceof ATProduct) ? ((ATProduct) other_).getAtId() : null;
		if (otherId == null)
			return -1;
		return this.getAtId().compareTo(otherId);
	}

	// IATMarketData

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getUnderlying() {
		return IATMarketData.getUnderlying(this);
	}

	@Override
	public Double getPxLast() {
		return IATMarketData.getPxLast(this);
	}

	public void setPxLast(Double value_) {
		IATMarketData.setPxLast(this, value_);
	}

	@Override
	public Integer getSzLast() {
		return IATAsset.getSzLast(this);
	}

	@Override
	public Long getTsLast() {
		return IATMarketData.getTsLast(this);
	}

	@Override
	public Double getPxAsk() {
		return IATMarketData.getPxAsk(this);
	}

	@Override
	public Long getTsAsk() {
		return IATMarketData.getTsAsk(this);
	}

	@Override
	public Integer getSzAsk() {
		return IATAsset.getSzAsk(this);
	}

	@Override
	public Double getPxBid() {
		return IATMarketData.getPxBid(this);
	}

	@Override
	public Long getTsBid() {
		return IATMarketData.getTsBid(this);
	}

	@Override
	public Integer getSzBid() {
		return IATAsset.getSzBid(this);
	}

	@Override
	public Long getDailyVol() {
		return IATMarketData.getDailyVol(this);
	}

	@Override
	public Double getPxOpen() {
		return IATMarketData.getPxOpen(this);
	}

	@Override
	public Double getPxClose() {
		return IATMarketData.getPxClose(this);
	}

	public void setPxOpen(Double value_) {
		IATMarketData.setPxOpen(this, value_);
	}

	public void setPxClose(Double value_) {
		IATMarketData.setPxClose(this, value_);
	}

	@Override
	public Double getPxClosePrev() {
		return IATMarketData.getPxClosePrev(this);
	}

	@Override
	public Double getPxHigh() {
		return IATMarketData.getPxHigh(this);
	}

	@Override
	public Double getPxLow() {
		return IATMarketData.getPxLow(this);
	}

	@Override
	public Long getOpenInterest() {
		return IATMarketData.getOpenInterest(this);
	}

	public void setOpenInterest(Long value_) {
		IATMarketData.setOpenInterest(this, value_);
	}

	@Override
	public Double getPxStrike() {
		return IATMarketData.getPxStrike(this);
	}

	public void setPxStrike(Double value_) {
		IATAsset.setPxStrike(this, value_);
	}

	@Override
	public Double getPxChange() {
		return IATMarketData.getPxChange(this);
	}

	public void setPxChange(Double value_) {
		IATMarketData.setPxChange(this, value_);
	}

	@Override
	public String getOptionRight() {
		return IATMarketData.getOptionRight(this);
	}

	public void setOptionRight(String value_) {
		IATMarketData.setOptionRight(this, value_);
	}

	@Override
	public <T> T getMeta(String key_) {
		return IATMarketData.getMeta(this, key_);
	}

	// IATTrade

	@Override
	public String getOccId() {
		return IATTrade.getOccId(this);
	}

	public void setOccId(String value_) {
		IATTrade.setOccId(this, value_);
	}

	@Override
	public String getTradeUserId() {
		return IATTrade.getTradeUserId(this);
	}

	public void setTradeUserId(String value_) {
		IATTrade.setTradeUserId(this, value_);
	}

	@Override
	public Date getTsTrade() {
		return IATTrade.getTsTrade(this);
	}

	public void setTsTrade(Date value_) {
		IATTrade.setTsTrade(this, value_);
	}

	@Override
	public Long getTsTrade2() {
		return IATTrade.getTsTrade2(this);
	}

	public void setTsTrade2(Long value_) {
		IATTrade.setTsTrade2(this, value_);
	}

	@Override
	public Double getSzTrade() {
		return IATTrade.getSzTrade(this);
	}

	public void setSzTrade(Double value_) {
		IATTrade.setSzTrade(this, value_);
	}

	@Override
	public Double getSzTradeActive() {
		return IATTrade.getSzTradeActive(this);
	}

	public void setSzTradeActive(Double value_) {
		IATTrade.setSzTradeActive(this, value_);
	}

	@Override
	public Double getPxNet() {
		return IATTrade.getPxNet(this);
	}

	public void setPxNet(Double value_) {
		IATTrade.setPxNet(this, value_);
	}

	@Override
	public Double getPxTrade() {
		return IATTrade.getPxTrade(this);
	}

	public void setPxTrade(Double value_) {
		IATTrade.setPxTrade(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATTrade.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATTrade.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxUl() {
		return IATTrade.getPxUl(this);
	}

	public void setPxUl(Double value_) {
		IATTrade.setPxUl(this, value_);
	}

	@Override
	public Integer getTradeType() {
		return IATTrade.getTradeType(this);
	}

	public void setTradeType(int value_) {
		IATTrade.setTradeType(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATTrade.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		IATTrade.setTradeAccount(this, value_);
	}

	@Override
	public String getTradeComment() {
		return IATTrade.getTradeComment(this);
	}

	public void setTradeComment(String value_) {
		IATTrade.setTradeComment(this, value_);
	}

	@Override
	public String getTradeGroup() {
		return IATTrade.getTradeGroup(this);
	}

	public void setTradeGroup(String value_) {
		IATTrade.setTradeGroup(this, value_);
	}

	// IATProductDescription

	@Override
	public String getName() {
		return IATProductDescription.getName(this);
	}

	public void setName(String value_) {
		IATProductDescription.setName(this, value_);
	}

	@Override
	public String getLabel() {
		return IATProductDescription.getLabel(this);
	}

	public void setLabel(String value_) {
		IATProductDescription.setLabel(this, value_);
	}

	@Override
	public String getDescription() {
		return IATProductDescription.getDescription(this);
	}

	public void setDescription(String value_) {
		IATProductDescription.setDescription(this, value_);
	}

	@Override
	public String getSector() {
		return IATProductDescription.getSector(this);
	}

	public void setSector(String value_) {
		IATProductDescription.setSector(this, value_);
	}

	@Override
	public String getIndustry() {
		return IATProductDescription.getIndustry(this);
	}

	public void setIndustry(String value_) {
		IATProductDescription.setIndustry(this, value_);
	}

	@Override
	public String getModel() {
		return IATProductDescription.getModel(this);
	}

	public void setModel(String value_) {
		IATProductDescription.setModel(this, value_);
	}

	// IATFundamental

	@Override
	public String getLatestDS8() {
		return IATFundamental.getLatestDS8(this);
	}

	public void setLatestDS8(String value_) {
		IATFundamental.setLatestDS8(this, value_);
	}
	
	@Override
	public Double getIV() {
		return IATFundamental.getIV(this);
	}

	@Override
	public Double getMarketCap() {
		return IATFundamental.getMarketCap(this);
	}

	public void setMarketCap(Double value_) {
		IATFundamental.setMarketCap(this, value_);
	}

	@Override
	public String getEarningsDS8() {
		return IATFundamental.getEarningsDS8(this);
	}

	public void setEarningsDS8(String value_) {
		IATFundamental.setEarningsDS8(this, value_);
	}

	@Override
	public String getDividendExDS8() {
		return IATFundamental.getDividendExDS8(this);
	}

	public void setDividendExDS8(String value_) {
		IATFundamental.setDividendExDS8(this, value_);
	}

	@Override
	public String getDividendEstDS8() {
		return IATFundamental.getDividendEstDS8(this);
	}

	public void setDividendEstDS8(String value_) {
		IATFundamental.setDividendEstDS8(this, value_);
	}

	@Override
	public Double getDividend() {
		return IATFundamental.getDividend(this);
	}

	public void setDividend(Double value_) {
		IATFundamental.setDividend(this, value_);
	}

	@Override
	public Integer getDividendFrequency() {
		return IATFundamental.getDividendFrequency(this);
	}

	@Override
	public String getModeHV() {
		return IATFundamental.getModeHV(this);
	}

	public void setModeHV(String value_) {
		IATFundamental.setModeHV(this, value_);
	}

	@Override
	public Double getPxHV() {
		return IATFundamental.getPxHV(this);
	}

	public void setPxHV(Double value_) {
		IATFundamental.setPxHV(this, value_);
	}

	@Override
	public String getModeAvg() {
		return IATFundamental.getModeAvg(this);
	}

	public void setModeAvg(String value_) {
		IATFundamental.setModeAvg(this, value_);
	}

	@Override
	public Double getPxAvg() {
		return IATFundamental.getPxAvg(this);
	}

	public void setPxAvg(Double value_) {
		IATFundamental.setPxAvg(this, value_);
	}

	// Other

	public void setProductMode(String value_) {
		IATAsset.setMapValue(this, MODE, value_);
	}

	public void setParentReference(String value_) {
		IATAsset.setMapValue(this, PARENT_REF, value_);
	}

	public void setDetails(List<ATProduct> value_) {
		super.put(DETAILS, value_);
	}

	@SuppressWarnings("unchecked")
	public List<ATProduct> getDetails() {
		return (List<ATProduct>) super.get(DETAILS);
	}

	public Double getRisk() {
		return IATAsset.getDouble(this, IATAssetConstants.RISK);
	}

	public void setRisk(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.RISK, value_);
	}

	public Double getMargin() {
		return IATAsset.getDouble(this, IATAssetConstants.MARGIN);
	}

	public void setMargin(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.MARGIN, value_);
	}

	public Double getPxTheo() {
		return IATAsset.getDouble(this, IATAssetConstants.PX_THEORETICAL);
	}

	public void setPxTheo(Double value_) {
		IATAsset.setMapValue(this, IATAssetConstants.PX_THEORETICAL, value_);
	}

	public String getTradeId() {
		return IATAsset.getString(this, IATAssetConstants.TRADE_ID);
	}

	public void setTradeId(String value_) {
		IATAsset.setMapValue(this, IATAssetConstants.TRADE_ID, value_);
	}

	@Override
	public Double getPEGrowthRatio() {
		return IATFundamental.getPEGrowthRatio(this);
	}

	public void setPEGrowthRatio(Double value_) {
		IATFundamental.setPEGrowthRatio(this, value_);
	}

	@Override
	public Double getTrailingPE() {
		return IATFundamental.getTrailingPE(this);
	}

	public void setTrailingPE(Double value_) {
		IATFundamental.setTrailingPE(this, value_);
	}

	@Override
	public Double getRevenue1Q() {
		return IATFundamental.getRevenue1Q(this);
	}

	public void setRevenue1Q(Double value_) {
		IATFundamental.setRevenue1Q(this, value_);
	}

	@Override
	public Double getRevenue1Y() {
		return IATFundamental.getRevenue1Y(this);
	}

	public void setRevenue1Y(Double value_) {
		IATFundamental.setRevenue1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Q() {
		return IATFundamental.getRevenueGrowth1Q(this);
	}

	public void setRevenueGrowth1Q(Double value_) {
		IATFundamental.setRevenueGrowth1Q(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Y() {
		return IATFundamental.getRevenueGrowth1Y(this);
	}

	public void setRevenueGrowth1Y(Double value_) {
		IATFundamental.setRevenueGrowth1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthAvg3Y() {
		return IATFundamental.getRevenueGrowthAvg3Y(this);
	}

	public void setRevenueGrowthAvg3Y(Double value_) {
		IATFundamental.setRevenueGrowthAvg3Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthYoY() {
		return IATFundamental.getRevenueGrowthYoY(this);
	}

	public void setRevenueGrowthYoY(Double value_) {
		IATFundamental.setRevenueGrowthYoY(this, value_);
	}
	
	@Override
	public Double getRevenueMultiple() {
		return IATFundamental.getRevenueMultiple(this);
	}

	public void setRevenueMultiple(Double value_) {
		IATFundamental.setRevenueMultiple(this, value_);
	}

	@Override
	public Map<String, Double> getAvgMap() {
		return IATFundamental.getAvgMap(this);
	}

	public void setAvgMap(Map<String, Double> map_) {
		IATFundamental.setAvgMap(this, map_);
	}

	@Override
	public Map<String, Double> getHvMap() {
		return IATFundamental.getHvMap(this);
	}

	public void setHvMap(Map<String, Double> map_) {
		IATFundamental.setHvMap(this, map_);
	}

	@Override
	public Map<String, Double> getMaxMap() {
		return IATFundamental.getMaxMap(this);
	}

	public void setMaxMap(Map<String, Double> map_) {
		IATFundamental.setMaxMap(this, map_);
	}

	@Override
	public Map<String, Double> getMinMap() {
		return IATFundamental.getMinMap(this);
	}

	public void setMinMap(Map<String, Double> map_) {
		IATFundamental.setMinMap(this, map_);
	}

	@Override
	public String getTraceId() {
		// TODO Auto-generated method stub
		return null;
	}
	
}