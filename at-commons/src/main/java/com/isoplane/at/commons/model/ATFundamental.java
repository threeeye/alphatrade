package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;

public class ATFundamental extends HashMap<String, Object> implements IATFundamental {

	private static final long serialVersionUID = 1L;

	public ATFundamental() {
	}

	protected ATFundamental(IATFundamental tmpl_) {
		if (tmpl_ instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Object> map = (Map<String, Object>) tmpl_;
			this.init(map, null, null);
			// this.setOccId(IATAsset.getString(map, IATAssetConstants.OCCID));
			// this.setMarketCap(IATFundamental.getMarketCap(map));
			// this.setEarningsDS8(IATFundamental.getEarningsDS8(map));
			// this.setDividendExDS8(IATFundamental.getDividendExDS8(map));
			// this.setDividendEstDS8(IATFundamental.getDividendEstDS8(map));
			// this.setDividend(IATFundamental.getDividend(map));
			// this.setModeHV(IATFundamental.getModeHV(map));
			// this.setPxHV(IATFundamental.getPxHV(map));
			// this.setModeAvg(IATFundamental.getModeAvg(map));
			// this.setPxAvg(IATFundamental.getPxAvg(map));
			// this.setPEGrowthRatio(IATFundamental.getPEGrowthRatio(map));
			// this.setTrailingPE(IATFundamental.getTrailingPE(map));
			// this.setRevenueGrowth1Q(IATFundamental.getRevenueGrowth1Q(map));
			// this.setRevenueGrowth1Y(IATFundamental.getRevenueGrowth1Y(map));
			// this.setRevenueGrowthAvg3Y(IATFundamental.getRevenueGrowthAvg3Y(map));
			// this.setRevenueGrowthYoY(IATFundamental.getRevenueGrowthYoY(map));
			// this.setIV(IATAsset.getDouble(map, IATAssetConstants.IV));
			// this.setDividendFrequency(IATAsset.getInt(map, IATAssetConstants.DIV_FREQUENCY));
		} else {
			this.setAvgMap(tmpl_.getAvgMap());
			this.setDividend(tmpl_.getDividend());
			this.setDividendEstDS8(tmpl_.getDividendEstDS8());
			this.setDividendExDS8(tmpl_.getDividendExDS8());
			this.setDividendFrequency(tmpl_.getDividendFrequency());
			this.setEarningsDS8(tmpl_.getEarningsDS8());
			this.setHvMap(tmpl_.getHvMap());
			this.setIV(tmpl_.getIV());
			this.setLatestDS8(tmpl_.getLatestDS8());
			this.setMarketCap(tmpl_.getMarketCap());
			this.setMaxMap(tmpl_.getMaxMap());
			this.setMinMap(tmpl_.getMinMap());
			this.setModeHV(tmpl_.getModeHV());
			this.setModeAvg(tmpl_.getModeAvg());
			this.setOccId(tmpl_.getAtId());
			this.setPEGrowthRatio(tmpl_.getPEGrowthRatio());
			this.setPxAvg(tmpl_.getPxAvg());
			this.setPxHV(tmpl_.getPxHV());
			this.setRevenue1Q(tmpl_.getRevenue1Q());
			this.setRevenue1Y(tmpl_.getRevenue1Y());
			this.setRevenueGrowth1Q(tmpl_.getRevenueGrowth1Q());
			this.setRevenueGrowth1Y(tmpl_.getRevenueGrowth1Y());
			this.setRevenueGrowthAvg3Y(tmpl_.getRevenueGrowthAvg3Y());
			this.setRevenueGrowthYoY(tmpl_.getRevenueGrowthYoY());
			this.setRevenueMultiple(tmpl_.getRevenueMultiple());
			this.setTrailingPE(tmpl_.getTrailingPE());
			// this.setIV(IATAsset.getDouble(tmpl_.geti);
		}
	}

	public ATFundamental(Map<String, Object> map_, String avgMode_, String hvMode_) {
		this.init(map_, avgMode_, hvMode_);
	}

	private void init(Map<String, Object> map_, String avgMode_, String hvMode_) {
		this.setAvgMap(IATFundamental.getAvgMap(map_));
		this.setDividend(IATFundamental.getDividend(map_));
		this.setDividendEstDS8(IATFundamental.getDividendEstDS8(map_));
		this.setDividendExDS8(IATFundamental.getDividendExDS8(map_));
		this.setDividendFrequency(IATFundamental.getDividendFrequency(map_));
		this.setEarningsDS8(IATFundamental.getEarningsDS8(map_));
		this.setHvMap(IATFundamental.getHvMap(map_));
		this.setIV(IATAsset.getDouble(map_, IATAssetConstants.IV));
		this.setLatestDS8(IATFundamental.getLatestDS8(map_));
		this.setMarketCap(IATFundamental.getMarketCap(map_));
		this.setMaxMap(IATFundamental.getMaxMap(map_));
		this.setMinMap(IATFundamental.getMinMap(map_));

		this.setOccId(IATAsset.getString(map_, IATAssetConstants.OCCID));

		this.setPEGrowthRatio(IATFundamental.getPEGrowthRatio(map_));
		this.setRevenue1Q(IATFundamental.getRevenue1Q(map_));
		this.setRevenue1Y(IATFundamental.getRevenue1Y(map_));
		this.setRevenueGrowth1Q(IATFundamental.getRevenueGrowth1Q(map_));
		this.setRevenueGrowth1Y(IATFundamental.getRevenueGrowth1Y(map_));
		this.setRevenueGrowthAvg3Y(IATFundamental.getRevenueGrowthAvg3Y(map_));
		this.setRevenueGrowthYoY(IATFundamental.getRevenueGrowthYoY(map_));
		this.setRevenueMultiple(IATFundamental.getRevenueMultiple(map_));
		this.setTrailingPE(IATFundamental.getTrailingPE(map_));

		if (avgMode_ == null) {
			this.setModeAvg(IATFundamental.getModeAvg(map_));
			this.setPxAvg(IATFundamental.getPxAvg(map_));
		} else {
			this.setModeAvg(avgMode_);
			this.setPxAvg(IATAsset.getDouble(map_, avgMode_));
		}

		if (hvMode_ == null) {
			this.setModeHV(IATFundamental.getModeHV(map_));
			this.setPxHV(IATFundamental.getPxHV(map_));
		} else {
			this.setModeHV(hvMode_);
			this.setPxHV(IATAsset.getDouble(map_, hvMode_));
		}
	}

	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getLatestDS8() {
		return IATFundamental.getLatestDS8(this);
	}

	public void setLatestDS8(String value_) {
		IATFundamental.setLatestDS8(this, value_);
	}

	public Double getIV() {
		return IATAsset.getDouble(this, IATAssetConstants.IV);
	}

	public void setIV(Double value_) {
		super.put(IATAssetConstants.IV, value_);
	}

	public Integer getDividendFrequency() {
		return IATAsset.getInt(this, IATAssetConstants.DIV_FREQUENCY);
	}

	public void setDividendFrequency(Integer value_) {
		super.put(IATAssetConstants.DIV_FREQUENCY, value_);
	}

	@Override
	public String getAtId() {
		return getOccId();
	}

	@Override
	public Double getMarketCap() {
		return IATFundamental.getMarketCap(this);
	}

	public void setMarketCap(Double value_) {
		IATFundamental.setMarketCap(this, value_);
	}

	@Override
	public String getEarningsDS8() {
		return IATFundamental.getEarningsDS8(this);
	}

	public void setEarningsDS8(String value_) {
		IATFundamental.setEarningsDS8(this, value_);
	}

	@Override
	public String getDividendExDS8() {
		return IATFundamental.getDividendExDS8(this);
	}

	public void setDividendExDS8(String value_) {
		IATFundamental.setDividendExDS8(this, value_);
	}

	@Override
	public String getDividendEstDS8() {
		return IATFundamental.getDividendEstDS8(this);
	}

	public void setDividendEstDS8(String value_) {
		IATFundamental.setDividendEstDS8(this, value_);
	}

	@Override
	public Double getDividend() {
		return IATFundamental.getDividend(this);
	}

	public void setDividend(Double value_) {
		IATFundamental.setDividend(this, value_);
	}

	@Override
	public String getModeHV() {
		return IATFundamental.getModeHV(this);
	}

	public void setModeHV(String value_) {
		IATFundamental.setModeHV(this, value_);
	}

	@Override
	public Double getPxHV() {
		return IATFundamental.getPxHV(this);
	}

	public void setPxHV(Double value_) {
		IATFundamental.setPxHV(this, value_);
	}

	@Override
	public String getModeAvg() {
		return IATFundamental.getModeAvg(this);
	}

	public void setModeAvg(String value_) {
		IATFundamental.setModeAvg(this, value_);
	}

	@Override
	public Double getPxAvg() {
		return IATFundamental.getPxAvg(this);
	}

	public void setPxAvg(Double value_) {
		IATFundamental.setPxAvg(this, value_);
	}

	@Override
	public Double getPEGrowthRatio() {
		return IATFundamental.getPEGrowthRatio(this);
	}

	public void setPEGrowthRatio(Double value_) {
		IATFundamental.setPEGrowthRatio(this, value_);
	}

	@Override
	public Double getTrailingPE() {
		return IATFundamental.getTrailingPE(this);
	}

	public void setTrailingPE(Double value_) {
		IATFundamental.setTrailingPE(this, value_);
	}

	@Override
	public Double getRevenue1Q() {
		return IATFundamental.getRevenue1Q(this);
	}

	public void setRevenue1Q(Double value_) {
		IATFundamental.setRevenue1Q(this, value_);
	}

	@Override
	public Double getRevenue1Y() {
		return IATFundamental.getRevenue1Y(this);
	}

	public void setRevenue1Y(Double value_) {
		IATFundamental.setRevenue1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Q() {
		return IATFundamental.getRevenueGrowth1Q(this);
	}

	public void setRevenueGrowth1Q(Double value_) {
		IATFundamental.setRevenueGrowth1Q(this, value_);
	}

	@Override
	public Double getRevenueGrowth1Y() {
		return IATFundamental.getRevenueGrowth1Y(this);
	}

	public void setRevenueGrowth1Y(Double value_) {
		IATFundamental.setRevenueGrowth1Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthAvg3Y() {
		return IATFundamental.getRevenueGrowthAvg3Y(this);
	}

	public void setRevenueGrowthAvg3Y(Double value_) {
		IATFundamental.setRevenueGrowthAvg3Y(this, value_);
	}

	@Override
	public Double getRevenueGrowthYoY() {
		return IATFundamental.getRevenueGrowthYoY(this);
	}

	public void setRevenueGrowthYoY(Double value_) {
		IATFundamental.setRevenueGrowthYoY(this, value_);
	}

	@Override
	public Double getRevenueMultiple() {
		return IATFundamental.getRevenueMultiple(this);
	}

	public void setRevenueMultiple(Double value_) {
		IATFundamental.setRevenueMultiple(this, value_);
	}

	@Override
	public Map<String, Double> getAvgMap() {
		return IATFundamental.getAvgMap(this);
	}

	public void setAvgMap(Map<String, Double> map_) {
		IATFundamental.setAvgMap(this, map_);
	}

	public Double getAvg(String key_) {
		Map<String, Double> map = getAvgMap();
		Double value = map != null ? map.get(key_) : null;
		return value;
	}

	@Override
	public Map<String, Double> getHvMap() {
		return IATFundamental.getHvMap(this);
	}

	public void setHvMap(Map<String, Double> map_) {
		IATFundamental.setHvMap(this, map_);
	}

	public Double getHv(String key_) {
		Map<String, Double> map = getHvMap();
		Double value = map != null ? map.get(key_) : null;
		return value;
	}

	@Override
	public Map<String, Double> getMaxMap() {
		return IATFundamental.getMaxMap(this);
	}

	public void setMaxMap(Map<String, Double> map_) {
		IATFundamental.setMaxMap(this, map_);
	}

	public Double getMax(String key_) {
		Map<String, Double> map = getMaxMap();
		Double value = map != null ? map.get(key_) : null;
		return value;
	}

	@Override
	public Map<String, Double> getMinMap() {
		return IATFundamental.getMinMap(this);
	}

	public void setMinMap(Map<String, Double> map_) {
		IATFundamental.setMinMap(this, map_);
	}

	public Double getMin(String key_) {
		Map<String, Double> map = getMinMap();
		Double value = map != null ? map.get(key_) : null;
		return value;
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map_) {
		if (map_ == null || map_.isEmpty())
			return;
		for (Entry<? extends String, ? extends Object> entry : map_.entrySet()) {
			if (entry.getValue() != null) {
				super.put(entry.getKey(), entry.getValue());
			}
		}
	}

}
