package com.isoplane.at.commons.util;

import java.util.Date;
import java.util.Map;

public interface IATMapUitil {

	// Primitives

	static String getString(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (String) map_.get(key_);
	}

	static Long getLong(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		Object obj = map_.get(key_);
		if (obj instanceof Double)
			return ((Double) obj).longValue();
		return (Long) obj;
	}

	static Integer getInt(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		Object obj = map_.get(key_);
		if (obj instanceof Double)
			return ((Double) obj).intValue();
		if (obj instanceof Long)
			return ((Long) obj).intValue();
		return (Integer) obj;
	}

	static Integer getInt(Map<String, Object> map_, String key_, int default_) {
		if (map_ == null)
			return default_;
		return (Integer) map_.get(key_);
	}

	static Double getDouble(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		Object value = map_.get(key_);
		if (value instanceof Double) {
			return (Double) value;
		} else if (value instanceof String) {
			try {
				Double d = Double.parseDouble((String) value);
				return d;
			} catch (Exception ex) {
				return null;
			}
		} else {
			return null;
		}
	}

	static Date getDate(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (Date) map_.get(key_);
	}

	static Boolean getBoolean(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (Boolean) map_.get(key_);
	}

	static void setMapValue(Map<String, Object> map_, String key_, Object value_) {
		map_.put(key_, value_);
	}

}
