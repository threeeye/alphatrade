package com.isoplane.at.commons.model;

import java.util.Date;

@Deprecated
public class ATLegacyDividend extends ATSecEvent implements Comparable<ATLegacyDividend> {

	private Double cashAmount;
	private Date declarationDate;
	private Date exDate;
	private Date recordDate;
	private Date payDate;

	public Double getCashAmount() {
		return cashAmount;
	}

	public void setCashAmount(Double cashAmount) {
		this.cashAmount = cashAmount;
	}

	public Date getDeclarationDate() {
		return declarationDate;
	}

	public void setDeclarationDate(Date declarationDate) {
		this.declarationDate = declarationDate;
	}

	public Date getExDate() {
		return exDate;
	}

	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	@Override
	public int compareTo(ATLegacyDividend other_) {
		Date otherDate = other_ != null ? other_.getExDate() : null;
		Date thisDate = getExDate();
		if (thisDate == null) {
			return otherDate == null ? 0 : -1;
		}
		int result = thisDate.compareTo(otherDate);
		return result;
	}

	@Override
	public String toString() {
		String str = String.format("ATLegacyDividend{\"%tF\" : %.2f}", this.getExDate(), this.getCashAmount());
		return str;
	}
}
