package com.isoplane.at.commons.model.protobuf;

import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.protobuf.SymbolPackProtos.SymbolPack;;

public class ATProtoSymbolPackWrapper {

	private SymbolPack _proto;

	public ATProtoSymbolPackWrapper(SymbolPack value_) {
		this._proto = value_;
	}

	public SymbolPack proto() {
		return this._proto;
	}

	public String getSource() {
		return this._proto.getSource();
	}

	public String getTarget() {
		return this._proto.getTarget();
	}

	public long getTimestamp() {
		return this._proto.getTimestamp();
	}

	public String getKey() {
		return this._proto.hasKey() ? this._proto.getKey() : null;
	}

	public String getAction() {
		return this._proto.hasAction() ? this._proto.getAction() : null;
	}

	public boolean isTest() {
		return this._proto.hasIsTest() && this._proto.getIsTest();
	}

	public Iterator<String> getSymbolsIterator() {
		return this._proto.getSymbolsList().iterator();
	}

	static public SymbolPack toSymbolPack(String source_, String target_, String action_, String key_, Date date_, Iterable<String> occIds_,
			boolean isTest_) {
		SymbolPack.Builder builder = SymbolPack.newBuilder();
		builder.setSource(source_);
		builder.setTarget(target_);
		builder.setTimestamp(date_.getTime());
		builder.addAllSymbols(occIds_);
		if (!StringUtils.isBlank(action_)) {
			builder.setAction(action_);
		}
		if (!StringUtils.isBlank(key_)) {
			builder.setKey(key_);
		}
		if (isTest_) {
			builder.setIsTest(isTest_);
		}
		SymbolPack pack = builder.build();
		return pack;
	}

}
