package com.isoplane.at.commons.store;

import java.util.TreeMap;

public interface IATLookupStore {

	IATLookupMap get(String table, String id);

	<T extends IATLookupMap> TreeMap<String, T> groupMax(String table, String groupField, String maxField, Class<T> clazz);

	String getDbId();

}
