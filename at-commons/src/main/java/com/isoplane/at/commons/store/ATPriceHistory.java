package com.isoplane.at.commons.store;

import java.util.Map;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATPriceHistory;
import com.isoplane.at.commons.model.IATStatistics;

public class ATPriceHistory extends ATPersistentLookupBase implements IATPriceHistory, IATStatistics, Comparable<ATPriceHistory> {

	private static final long serialVersionUID = 1L;

	static public double ANNUALIZED_FACTOR = Math.sqrt(254);

	public ATPriceHistory() {
		super();
	}

	public ATPriceHistory(Map<String, Object> data_) {
		super(data_);
	}

	@Override
	public int compareTo(ATPriceHistory other_) {
		return getAtId().compareTo(other_.getAtId());
	}

	@Override
	public String toString() {
		String str = String.format("%-5s%s O:%.2f H:%.2f L:%.2f C:%.2f V:%d", getOccId(), getDateStr(), getOpen(), getHigh(), getLow(), getClose(),
				getVolume());
		return str;
	}

	@Override
	public String getOccId() {
		return (String) get(IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getDateStr() {
		return (String) get(IATAssetConstants.DATE_STR);
	}

	public void setDateStr(String value_) {
		put(IATAssetConstants.DATE_STR, value_);
	}

	@Override
	public Double getClose() {
		return (Double) get(CLOSE);
	}

	public void setClose(Double value_) {
		if (value_ == null) {
			remove(CLOSE);
		} else {
			put(CLOSE, value_);
		}
	}

	@Override
	public Double getHigh() {
		return (Double) get(HIGH);
	}

	public void setHigh(Double value_) {
		if (value_ == null) {
			remove(HIGH);
		} else {
			put(HIGH, value_);
		}
	}

	@Override
	public Double getLow() {
		return (Double) get(LOW);
	}

	public void setLow(Double value_) {
		if (value_ == null) {
			remove(LOW);
		} else {
			put(LOW, value_);
		}
	}

	@Override
	public Double getOpen() {
		return (Double) get(OPEN);
	}

	public void setOpen(Double value_) {
		if (value_ == null) {
			remove(OPEN);
		} else {
			put(OPEN, value_);
		}
	}

	@Override
	public Long getVolume() {
		return (Long) get(VOLUME);
	}

	public void setVolume(Long value_) {
		if (value_ == null) {
			remove(VOLUME);
		} else {
			put(VOLUME, value_);
		}
	}

	@Override
	public Double getHV10() {
		return (Double) get(HV10);
	}

	public void setHV10(Double value_) {
		if (value_ == null) {
			remove(HV10);
		} else {
			put(HV10, value_);
		}
	}

	// @Override
	@Override
	public Double getHV20() {
		return (Double) get(HV20);
	}

	public void setHV20(Double value_) {
		if (value_ == null) {
			remove(HV20);
		} else {
			put(HV20, value_);
		}
	}

	@Override
	public Double getHV30() {
		return (Double) get(HV30);
	}

	public void setHV30(Double value_) {
		if (value_ == null) {
			remove(HV30);
		} else {
			put(HV30, value_);
		}
	}

	@Override
	public Double getHV60() {
		return (Double) get(HV60);
	}

	public void setHV60(Double value_) {
		if (value_ == null) {
			remove(HV60);
		} else {
			put(HV60, value_);
		}
	}

	@Override
	public Double getHV90() {
		return (Double) get(HV90);
	}

	public void setHV90(Double value_) {
		if (value_ == null) {
			remove(HV90);
		} else {
			put(HV90, value_);
		}
	}

	@Override
	public Double getHV125() {
		return (Double) get(HV125);
	}

	public void setHV125(Double value_) {
		if (value_ == null) {
			remove(HV125);
		} else {
			put(HV125, value_);
		}
	}

	@Override
	public Double getHV254() {
		return (Double) get(HV254);
	}

	public void setHV254(Double value_) {
		if (value_ == null) {
			remove(HV254);
		} else {
			put(HV254, value_);
		}
	}

	@Override
	public Double getMin60() {
		return (Double) get(MIN60);
	}

	public void setMin60(Double value_) {
		if (value_ == null) {
			remove(MIN60);
		} else {
			put(MIN60, value_);
		}
	}

	@Override
	public Double getMin125() {
		return (Double) get(MIN125);
	}

	public void setMin125(Double value_) {
		if (value_ == null) {
			remove(MIN125);
		} else {
			put(MIN125, value_);
		}
	}

	@Override
	public Double getMin254() {
		return (Double) get(MIN254);
	}

	public void setMin254(Double value_) {
		if (value_ == null) {
			remove(MIN254);
		} else {
			put(MIN254, value_);
		}
	}

	public Double getMin(String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case "60":
			return getMin60();
		case "125":
			return getMin125();
		case "254":
			return getMin254();
		default:
			Object o = get(mode_);
			return (o instanceof Double) ? (Double) o : null;
		}
	}

	@Override
	public Double getMax60() {
		return (Double) get(MAX60);
	}

	public void setMax60(Double value_) {
		if (value_ == null) {
			remove(MAX60);
		} else {
			put(MAX60, value_);
		}
	}

	@Override
	public Double getMax125() {
		return (Double) get(MAX125);
	}

	public void setMax125(Double value_) {
		if (value_ == null) {
			remove(MAX125);
		} else {
			put(MAX125, value_);
		}
	}

	@Override
	public Double getMax254() {
		return (Double) get(MAX254);
	}

	public void setMax254(Double value_) {
		if (value_ == null) {
			remove(MAX254);
		} else {
			put(MAX254, value_);
		}
	}

	public Double getMax(String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case "60":
			return getMax60();
		case "125":
			return getMax125();
		case "254":
			return getMax254();
		default:
			Object o = get(mode_);
			return (o instanceof Double) ? (Double) o : null;
		}
	}

	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public String getSource() {
		return (String) super.get(IATAssetConstants.SOURCE);
	}

	@Override
	public Double getHV(String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case "hv10":
			return getHV10();
		case "hv20":
			return getHV20();
		case "hv30":
			return getHV30();
		case "hv60":
			return getHV60();
		case "hv90":
			return getHV90();
		case "hv125":
			return getHV125();
		case "hv254":
			return getHV254();
		default:
			Object o = get(mode_);
			return (o instanceof Double) ? (Double) o : null;
		}
	}

	@Override
	public Double getAvg(String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case "sma60":
			return getSma60();
		case "sma125":
			return getSma125();
		case "sma254":
			return getSma254();
		default:
			Object o = get(mode_);
			return (o instanceof Double) ? (Double) o : null;
		}
	}

	@Override
	public Double getSma60() {
		return (Double) get(SMA60);
	}

	public void setSma60(Double value_) {
		if (value_ == null) {
			remove(SMA60);
		} else {
			put(SMA60, value_);
		}
	}

	@Override
	public Double getSma125() {
		return (Double) get(SMA125);
	}

	public void setSma125(Double value_) {
		if (value_ == null) {
			remove(SMA125);
		} else {
			put(SMA125, value_);
		}
	}

	@Override
	public Double getSma254() {
		return (Double) get(SMA254);
	}

	public void setSma254(Double value_) {
		if (value_ == null) {
			remove(SMA254);
		} else {
			put(SMA254, value_);
		}
	}

	@Override
	public Double getIV() {
		return IATAsset.getIV(this);
	}

	public void setIV(Double value_) {
		IATAsset.setIV(this, value_);
	}
}
