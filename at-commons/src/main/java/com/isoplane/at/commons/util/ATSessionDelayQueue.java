package com.isoplane.at.commons.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;

public class ATSessionDelayQueue implements IATProtoUserSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATSessionDelayQueue.class);

	private DelayQueue<DelayedSessionNotification> _delayedSessionQueue;

	private SessionRunner _sessionRunner;

	private Set<IATProtoUserSubscriber> _subscribers;

	public ATSessionDelayQueue() {
		this._delayedSessionQueue = new DelayQueue<>();
		this._subscribers = new HashSet<>();
	}

	public void start() {
		this._sessionRunner = new SessionRunner();
		ATExecutors.submit(String.format("%s", this.getClass().getSimpleName()), this._sessionRunner);
	}

	public void stop() {
		if (this._sessionRunner != null) {
			this._sessionRunner.stop();
		}
	}

	public void subscribe(IATProtoUserSubscriber sub_) {
		this._subscribers.add(sub_);
	}
	
	public int size() {
		return this._delayedSessionQueue.size();
	}

	@Override
	public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
		String userId = session_.getUserId();
		if (StringUtils.isBlank(userId))// || IATUser.STATIC_USERS.contains(userId))
			return;
		Iterator<DelayedSessionNotification> dsi = _delayedSessionQueue.iterator();

		// Update action on unprocessed sessions
		while (dsi.hasNext()) {
			DelayedSessionNotification ds = dsi.next();
			if (ds.userId.equals(userId)) {
				ds.action = action_;
				ds.session = session_;
				return;
			}
		}
		this._delayedSessionQueue.offer(new DelayedSessionNotification(action_, session_, 1000));
	}

	private class SessionRunner implements Runnable {

		private boolean _isRunning;

		public void stop() {
			this._isRunning = false;
		}

		@Override
		public void run() {
			this._isRunning = true;
			try {
				while (this._isRunning) {
					// Configuration config = ATConfigUtil.config();
					// long queueInterval = config.getLong("usr_mkt.queue.session.interval");

					DelayedSessionNotification session = _delayedSessionQueue.poll(1000, TimeUnit.MILLISECONDS);
					if (session == null)
						continue;
					for (IATProtoUserSubscriber subscriber : _subscribers) {
						subscriber.notifyUserSession(session.action, session.session);
					}
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (this._isRunning) {
					run();
				}
			}
		}
	}

	private static class DelayedSessionNotification implements Delayed {

		EATChangeOperation action;
		ATSession session;
		String userId;
		public long timeout;

		public DelayedSessionNotification(EATChangeOperation action_, ATSession session_, long delay_) {
			action = action_;
			userId = session_.getUserId();
			session = session_;
			timeout = System.currentTimeMillis() + delay_;
		}

		@Override
		public int compareTo(Delayed other_) {
			long otherTimeout = other_.getDelay(TimeUnit.MILLISECONDS);
			int result = Long.compare(timeout, otherTimeout);
			return result;
		}

		@Override
		public long getDelay(TimeUnit unit_) {
			long remainingMs = timeout - System.currentTimeMillis();
			return unit_.convert(remainingMs, TimeUnit.MILLISECONDS);
		}

		@Override
		public boolean equals(Object other_) {
			boolean result = other_ instanceof DelayedSessionNotification && userId.equals(((DelayedSessionNotification) other_).userId);
			return result;
		}

		@Override
		public int hashCode() {
			return userId.hashCode();
		}

	}

}
