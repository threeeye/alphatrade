package com.isoplane.at.commons.model.protobuf;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData.Builder;
import com.isoplane.at.commons.util.EATChangeOperation;

public class ATProtoMarketDataWrapper {

	static public final String TYPE_ANY = "any"; // For Any message type wrapper
	static public final String TYPE_MARKET_AVAILABLE = "mktAvail";
	static public final String TYPE_MARKET_DATA = "mkt";
	static public final String TYPE_MARKET_DATA_PACK = "mktPack"; // For Any message type wrapper
	static public final String TYPE_MOVERS = "mov";
	static public final String TYPE_TRENDS = "trnd";
	static public final String SUB_TYPE_OPTION_CHAIN = "option_chain";

	private MarketData _protoMkt;
	private ATMarketData _atMkt;
	private EATChangeOperation _action;

	public ATProtoMarketDataWrapper(MarketData mkt_) {
		this._protoMkt = mkt_;
	}

	public MarketData proto() {
		return this._protoMkt;
	}

	public ATMarketData value() {
		if (this._atMkt == null) {
			this._atMkt = toMarketData(this._protoMkt);
		}
		return this._atMkt;
	}

	static public ATMarketData toMarketData(MarketData protoMkt_) {
		ATMarketData mkt = mergeMarketData(new ATMarketData(), protoMkt_);
		return mkt;
	}

	static public ATMarketData mergeMarketData(ATMarketData oldMkt__, MarketData protoMkt_) {
		ATMarketData mkt = oldMkt__ != null ? oldMkt__ : new ATMarketData();
		mkt.setOccId(protoMkt_.getId());

		// >> PX
		if (protoMkt_.hasPxClose()) {
			mkt.setPxClose(protoMkt_.getPxClose());
		}
		if (protoMkt_.hasPxClosePre()) {
			mkt.setPxClosePrev(protoMkt_.getPxClosePre());
		}
		if (protoMkt_.hasPxDayCh()) {
			mkt.setPxChange(protoMkt_.getPxDayCh());
		}
		if (protoMkt_.hasPxDayHi()) {
			mkt.setPxHigh(protoMkt_.getPxDayHi());
		}
		if (protoMkt_.hasPxDayLo()) {
			mkt.setPxLow(protoMkt_.getPxDayLo());
		}
		if (protoMkt_.hasPxOpen()) {
			mkt.setPxOpen(protoMkt_.getPxOpen());
		}
		if (protoMkt_.hasPxTheo()) {
			mkt.setPxTheo(protoMkt_.getPxTheo());
		}
		if (protoMkt_.hasPxStrk()) {
			mkt.setPxStrike(protoMkt_.getPxStrk());
		}
		
		// >> ASK
		if (protoMkt_.hasTsAsk()) {
			long newTs = protoMkt_.getTsAsk();
			Long oldTs = mkt.getTsAsk();
			if (oldTs == null || oldTs < newTs) {
				mkt.setTsAsk(newTs);
				if (protoMkt_.hasPxAsk()) {
					mkt.setPxAsk(protoMkt_.getPxAsk());
				}
				if (protoMkt_.hasSzAsk()) {
					mkt.setSzAsk(protoMkt_.getSzAsk());
				}
			}
		}
		// << ASK
		// >> BID
		if (protoMkt_.hasTsBid()) {
			long newTs = protoMkt_.getTsBid();
			Long oldTs = mkt.getTsBid();
			if (oldTs == null || oldTs < newTs) {
				mkt.setTsBid(newTs);
				if (protoMkt_.hasPxBid()) {
					mkt.setPxBid(protoMkt_.getPxBid());
				}
				if (protoMkt_.hasSzBid()) {
					mkt.setSzBid(protoMkt_.getSzBid());
				}
			}
		}
		// << BID
		// >> LAST
		if (protoMkt_.hasTsLast()) {
			long newTs = protoMkt_.getTsLast();
			Long oldTs = mkt.getTsLast();
			if (oldTs == null || oldTs < newTs) {
				mkt.setTsLast(newTs);
				if (protoMkt_.hasPxLast()) {
					mkt.setPxLast(protoMkt_.getPxLast());
				}
				if (protoMkt_.hasSzLast()) {
					mkt.setSzLast(protoMkt_.getSzLast());
				}
			}
		}
		// << LAST

		// >> OTHER
		if (protoMkt_.hasOpenInt()) {
			mkt.setOpenInterest(protoMkt_.getOpenInt());
		}
		if (protoMkt_.hasVolDay()) {
			mkt.setDailyVol(protoMkt_.getVolDay());
		}
		// << OTHER
		// >> OPTION
		if (protoMkt_.hasDelta()) {
			mkt.setOptionDelta(protoMkt_.getDelta());
		}
		if (protoMkt_.hasGamma()) {
			mkt.setOptionGamma(protoMkt_.getGamma());
		}
		if (protoMkt_.hasRho()) {
			mkt.setOptionRho(protoMkt_.getRho());
		}
		if (protoMkt_.hasTheta()) {
			mkt.setOptionTheta(protoMkt_.getTheta());
		}
		if (protoMkt_.hasVega()) {
			mkt.setOptionVega(protoMkt_.getVega());
		}
		if (protoMkt_.hasIv()) {
			mkt.setIV(protoMkt_.getIv());
		}
		if (protoMkt_.hasRight()) {
			mkt.setOptionRight(protoMkt_.getRight());
		}
		// << OPTION

		return mkt;
	}

	static public MarketData toMarketData(IATMarketData mkt_) {
		ATMarketData mkt = mkt_ instanceof ATMarketData ? (ATMarketData) mkt_ : new ATMarketData(mkt_);
		return toMarketData(mkt, false);
	}

	static public MarketData toMarketData(ATMarketData mkt_, boolean isTest_) {
		Builder mbld = MarketData.newBuilder();

		String id = mkt_.getAtId();
		mbld.setId(id);

		String idUl = mkt_.getUnderlying();
		if (idUl != null) {
			mbld.setIdUl(idUl);
		}

		// >> PX
		Double pxAsk = mkt_.getPxAsk();
		if (pxAsk != null) {
			mbld.setPxAsk(pxAsk);
		}
		Double pxBid = mkt_.getPxBid();
		if (pxBid != null) {
			mbld.setPxBid(pxBid);
		}
		Double pxClose = mkt_.getPxClose();
		if (pxClose != null) {
			mbld.setPxClose(pxClose);
		}
		Double pxClosePre = mkt_.getPxClosePrev();
		if (pxClosePre != null) {
			mbld.setPxClosePre(pxClosePre);
		}
		Double pxChange = mkt_.getPxChange();
		if (pxChange != null) {
			mbld.setPxDayCh(pxChange);
		}
		Double pxDayHi = mkt_.getPxHigh();
		if (pxDayHi != null) {
			mbld.setPxDayHi(pxDayHi);
		}
		Double pxDayLo = mkt_.getPxLow();
		if (pxDayLo != null) {
			mbld.setPxDayLo(pxDayLo);
		}
		Double pxLast = mkt_.getPxLast();
		if (pxLast != null) {
			mbld.setPxLast(pxLast);
		}
		Double pxOpen = mkt_.getPxOpen();
		if (pxOpen != null) {
			mbld.setPxOpen(pxOpen);
		}
		Double pxTheo = mkt_.getPxTheo();
		if (pxTheo != null) {
			mbld.setPxTheo(pxTheo);
		}
		// << PX

		// >> SZ
		Integer szAsk = mkt_.getSzAsk();
		if (szAsk != null) {
			mbld.setSzAsk(szAsk);
		}
		Integer szBid = mkt_.getSzBid();
		if (szBid != null) {
			mbld.setSzBid(szBid);
		}
		Integer szLast = mkt_.getSzLast();
		if (szLast != null) {
			mbld.setSzLast(szLast);
		}
		// << SZ

		// >> TS
		Long tsAsk = mkt_.getTsAsk();
		if (tsAsk != null) {
			mbld.setTsAsk(tsAsk);
		}
		Long tsBid = mkt_.getTsBid();
		if (tsBid != null) {
			mbld.setTsBid(tsBid);
		}
		Long tsLast = mkt_.getTsLast();
		if (tsLast != null) {
			mbld.setTsLast(tsLast);
		}
		// << TS

		// >> OTHER
		Long oi = mkt_.getOpenInterest();
		if (oi != null) {
			mbld.setOpenInt(oi);
		}
		Long volDay = mkt_.getDailyVol();
		if (volDay != null) {
			mbld.setVolDay(volDay);
		}
		// << OTHER
		// >> OPTION
		Double pxStrike = mkt_.getPxStrike();
		if (pxStrike != null) {
			mbld.setPxStrk(pxStrike);
		}
		Double delta = mkt_.getOptionDelta();
		if (delta != null) {
			mbld.setDelta(delta);
		}
		Double gamma = mkt_.getOptionGamma();
		if (gamma != null) {
			mbld.setGamma(gamma);
		}
		Double rho = mkt_.getOptionRho();
		if (rho != null) {
			mbld.setRho(rho);
		}
		Double theta = mkt_.getOptionTheta();
		if (theta != null) {
			mbld.setTheta(theta);
		}
		Double vega = mkt_.getOptionVega();
		if (vega != null) {
			mbld.setVega(vega);
		}
		Double iv = mkt_.getIV();
		if (iv != null) {
			mbld.setIv(iv);
		}
		String right = mkt_.getOptionRight();
		if (right != null) {
			mbld.setRight(right);
		}
		// << OPTION

		if (isTest_) {
			mbld.setIsTest(isTest_);
		}

		MarketData mkt = mbld.build();
		return mkt;
	}

	static public Iterable<ATMarketData> toMarketData(Iterable<MarketData> data_) {
		List<ATMarketData> mkts = new ArrayList<ATMarketData>();
		for (MarketData protoMkt : data_) {
			ATMarketData mkt = ATProtoMarketDataWrapper.toMarketData(protoMkt);
			mkts.add(mkt);
		}
		return mkts;
	}

	// static public MarketDataPack toMarketDataPack(String type_, MarketData... mkt_) {
	// MarketDataPack.Builder builder = MarketDataPack.newBuilder().setType(type_);
	// for (MarketData marketData : mkt_) {
	// builder.addMarketData(marketData);
	// }
	// MarketDataPack pack = builder.build();
	// return pack;
	// }
	//
	// static public MarketDataPack toMarketDataPack(String type_, Iterable<MarketData> mkt_) {
	// MarketDataPack.Builder builder = MarketDataPack.newBuilder().setType(type_);
	// builder.addAllMarketData(mkt_);
	// MarketDataPack pack = builder.build();
	// return pack;
	// }

	public EATChangeOperation getAction() {
		if (_action == null) {
			String tmp = _protoMkt.getActionCode();
			_action = StringUtils.isBlank(tmp) ? EATChangeOperation.UPDATE : EATChangeOperation.valueOf(tmp);
		}
		return _action;
	}

	public void setAction(EATChangeOperation _action) {
		this._action = _action;
	}

}
