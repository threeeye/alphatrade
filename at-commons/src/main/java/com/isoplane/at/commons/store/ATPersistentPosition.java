package com.isoplane.at.commons.store;

import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATFormats;

public class ATPersistentPosition extends ATPersistentAssetBase {

	private static final long serialVersionUID = 1L;

	transient private Date tradeDate;

	public ATPersistentPosition() {
	}

	@Override
	public String getAtId() {
		String id = super.getAtId();
		if (id == null) {
			id = String.format("%s%s%s", getOccId(), getBroker(), getTradeDateStr());
			setAtId(id);
		}
		return id;
	}

	@Override
	protected String getSortId() {
		String str = this.getSymbol();
		if (isOption()) {
			str += this.getOptionRight() + this.getOccId();
		}
		str = str + getBroker() + getTradeDateStr();
		return str;
	}

	public Date getTradeDate() {
		if (this.tradeDate == null) {
			String dstr = this.getTradeDateStr();
			if (dstr != null) {
				try {
					tradeDate = ATFormats.DATE_json.get().parse(dstr);
				} catch (Exception ex) {
					throw new ATException(String.format("Date error [%s]", getOccId()), ex);
				}
			}
		}
		return tradeDate;
	}

	public void setTradeDate(Date tradeDate_) {
		this.tradeDate = tradeDate_;
		if (tradeDate_ == null) {
			super.remove(TRADE_DATE_STR);
		} else {
			String str = ATFormats.DATE_json.get().format(tradeDate_);
			super.put(TRADE_DATE_STR, str);
		}
	}

	public String getTradeDateStr() {
		return (String) super.get(TRADE_DATE_STR);
	}

	public void setTradeDateStr(String tradeDateStr_) {
		super.put(TRADE_DATE_STR, tradeDateStr_);
	}

	public String getBroker() {
		return (String) super.get(BROKER);
	}

	public void setBroker(String broker_) {
		super.put(BROKER, broker_);
	}

	public Double getCount() {
		Double count = (Double) get(SIZE_TRADE);
		if (count == null) {
			count = (Double) get("count");
			if (count != null) {
				setCount(count);
			}
		}
		return count != null ? count : 0;
	}

	public void setCount(Double count_) {
		super.put(SIZE_TRADE, count_);
	}

	public Double getTradePrice() {
		return (Double) get(PX_TRADE);
	}

	public void setTradePrice(Double value_) {
		super.put(PX_TRADE, value_);
	}

	public Double getTradeTotal() {
		return (Double) get(TRADE_TOTAL);
	}

	public void setTradeTotal(Double value_) {
		super.put(TRADE_TOTAL, value_);
	}

	public String getOccId() {
		return (String) get(OCCID);
	}

	public void setOccId(String value_) {
		super.put(OCCID, value_);
	}

	public String getOwnerId() {
		return (String) get(OWNER_ID);
	}

	public void setOwnerId(String value_) {
		super.put(OWNER_ID, value_);
	}

	@Override
	public boolean equals(Object other_) {
		if (other_ == null) {
			return false;
		}
		if (other_ == this) {
			return true;
		}
		if (other_.getClass() != getClass()) {
			return false;
		}
		ATPersistentPosition other = (ATPersistentPosition) other_;
		boolean result = new EqualsBuilder().append(this.getAtId(), other.getAtId()).append(this.getOwnerId(), other.getOwnerId()).isEquals();
		return result;
	}

	@Override
	public int hashCode() {
		int result = new HashCodeBuilder(17, 23).append(getAtId()).append(getOwnerId()).toHashCode();
		return result;
	}

}
