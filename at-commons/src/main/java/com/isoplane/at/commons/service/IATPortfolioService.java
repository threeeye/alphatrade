package com.isoplane.at.commons.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.IATWhereQuery;

public interface IATPortfolioService extends IATService {

	final String CASH_KEY = "$$$";

	void register(IATTradeListener listener);

	void register(IATTotalPositionListener listener);

	Map<String, Map<String, Double>> assessPerformance(String userId);

	// void verifySheets(String userId);

	Set<String> getOwnerIds(String occId);

	List<IATTrade> getTrades(IATWhereQuery query);

	boolean saveTrades(String uid, ATTrade[] trades);

	long expireOptions();

	List<ATUserAsset> getAssets(String userId);

	/**
	 * @param userId
	 * @param includeOptions
	 * @return Retrieves OCC ids of owned active positions. Does not return underlying symbols.
	 */
	Set<String> getActiveOccIds(String userId, boolean includeOptions);

	IATPortfolio updatePortfolio(String userId, String account, String ul);

	List<IATPortfolio> getPortfolio(String userId, String symbol);
	
    List<ATPosition> getPositions(String userId_, String symbol_);

	Map<String, Object> importTrades(String userId, ATTrade[] trades, boolean isPurgeExisting);

	Set<String> getAffiliatedUserIds(String symbol);

	List<ATTradeDigest> getTradeDigest(String userId, String symbol);

	List<ATTradeGroup> getTradeGroups(String userId, String symbol);

	List<ATTrade> getTradeLedger(String userId, String... symbols);

	ATTrade getTradeCumulative(String userId, String symbol);

	List<ATTradeTraceGroup> getTradeTrace(String userId, String symbol);

	long saveTradeTrace(ATTradeTraceGroup data, Long replaceId_);

	boolean deleteTradeTrace(String uid, Long id);

	boolean archiveTradeTrace(String userId, Object id);

}
