package com.isoplane.at.commons.model.protobuf;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;

public class ATProtoMarketDataPackWrapper {

	private MarketDataPack _protoData;
	private Iterable<ATMarketData> _atData;
	private String _type;

	public ATProtoMarketDataPackWrapper(MarketDataPack data_) {
		this._protoData = data_;
	}

	public MarketDataPack proto() {
		return this._protoData;
	}

	public Iterable<ATMarketData> value() {
		if (this._atData == null) {
			this._atData = ATProtoMarketDataWrapper.toMarketData(this._protoData.getMarketDataList());
		}
		return this._atData;
	}

	static public MarketDataPack toMarketDataPack(String type_, String subtype_, String source_, IATMarketData... data_) {
		MarketDataPack.Builder builder = MarketDataPack.newBuilder();
		builder.setType(type_);
		builder.setSubType(subtype_);
		builder.setSource(source_);
		Set<String> symbols = new TreeSet<>();
		for (IATMarketData mkt : data_) {
			MarketData protoMkt = ATProtoMarketDataWrapper.toMarketData(mkt);
			builder.addMarketData(protoMkt);
			String symbol = mkt.getUnderlying();
			if (symbol != null) {
				symbols.add(symbol);
			}
		}
		if (!symbols.isEmpty()) {
			builder.addAllSymbols(symbols);
		}
		MarketDataPack pack = builder.build();
		return pack;
	}

	static public MarketDataPack toMarketDataPack(String type_, String subtype_, String source_, MarketData... mkt_) {
		MarketDataPack.Builder builder = MarketDataPack.newBuilder();
		builder.setType(type_);
		builder.setSubType(subtype_);
		builder.setSource(source_);
		Set<String> symbols = new TreeSet<>();
		for (MarketData marketData : mkt_) {
			builder.addMarketData(marketData);
			if (marketData.hasIdUl()) {
				String symbol = marketData.getIdUl();
				symbols.add(symbol);
			}
		}
		if (!symbols.isEmpty()) {
			builder.addAllSymbols(symbols);
		}
		MarketDataPack pack = builder.build();
		return pack;
	}

	static public MarketDataPack toMarketDataPack(String type_, String subtype_, String source_, Iterable<MarketData> mkt_) {
		MarketDataPack.Builder builder = MarketDataPack.newBuilder();
		builder.setType(type_);
		builder.setSubType(subtype_);
		builder.setSource(source_);
		builder.addAllMarketData(mkt_);
		Set<String> symbols = new TreeSet<>();
		Iterator<MarketData> itr = mkt_.iterator();
		while (itr.hasNext()) {
			MarketData marketData = itr.next();
			if (marketData.hasIdUl()) {
				String symbol = marketData.getIdUl();
				symbols.add(symbol);
			}
		}
		if (!symbols.isEmpty()) {
			builder.addAllSymbols(symbols);
		}
		MarketDataPack pack = builder.build();
		return pack;
	}

	public String getType() {
		_type = _protoData.getType();
		return _type;

	}

	public void setType(String type_) {
		_type = type_;
	}

}
