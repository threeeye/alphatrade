package com.isoplane.at.commons.store;

public class ATPersistentFundamentals extends ATPersistentAssetBase {

	private static final long serialVersionUID = 1L;

	public ATPersistentFundamentals() {
	}

	public ATPersistentFundamentals(String occId_) {
		setOccId(occId_);
	}

	@Override
	protected String getSortId() {
		return getOccId();
	}

}