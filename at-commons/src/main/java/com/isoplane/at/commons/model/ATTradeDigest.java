package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.util.Precision;

import com.isoplane.at.commons.util.ATModelUtil;

/**
 * Trade Digest is a rollup of currently held underlying security value per account.
 */
public class ATTradeDigest extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public transient ArrayList<ATTrade> trades; // TODO: Convert into list of sizes (Double)?
	public transient Map<IATTrade.Type, Double> typeYieldMap;
	private transient Double _szEff;

	public ATTradeDigest() {
		this.trades = new ArrayList<>();
		this.typeYieldMap = new HashMap<>();
		this.reset();
		this.setPxCumulative(0.0);
//		this.setTsTrade(0L);
	}

	public ATTradeDigest(String userId_, String account_, String occId_) {
		this();
		this.setAccount(account_);
		this.setOccId(occId_);
		this.setTradeUserId(userId_);
		this.setUnderlying(ATModelUtil.getSymbol(occId_));
	}

	public ATTradeDigest(ATTradeDigest tmpl_) {
		this.putAll(tmpl_);
		this.trades = tmpl_.trades;
		this.typeYieldMap = tmpl_.typeYieldMap;
	}

	public ATTradeDigest(Map<String, Object> map_) {
		this();
		String id = IATAsset.getString(map_, IATTrade.OCCID);
		this.setOccId(id);
		String userId = IATAsset.getString(map_, IATTrade.TRADE_USER);
		this.setTradeUserId(userId);
		String account = IATAsset.getString(map_, IATTrade.TRADE_ACCOUNT);
		this.setAccount(account);
		Double pxNet = IATAsset.getDouble(map_, IATTrade.PX_NET);
		this.setPxNet(pxNet);
		Double szNet = IATAsset.getDouble(map_, IATTrade.SIZE_NET);
		this.setSzNet(szNet);
		Double pxCum = IATAsset.getDouble(map_, IATTrade.PX_ACU);
		this.setPxCumulative(pxCum);
		Double pxTrd = IATAsset.getDouble(map_, IATTrade.PX_TRADE);
		this.setPxTrade(pxTrd);
		// Long tsTrd = IATAsset.getLong(map_, IATTrade.TS_TRADE);
		// this.setTsTrade(tsTrd);
		String ul = IATAsset.getString(map_, IATTrade.UNDERLYING);
		this.setUnderlying(ul);
		String traceId = IATAsset.getString(map_, IATTrade.TRACE_ID);
		this.setTraceId(traceId);
	}

	public String getAccount() {
		return IATAsset.getString(this, IATTrade.TRADE_ACCOUNT);
	}

	public void setAccount(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.TRADE_ACCOUNT, value_);
	}

	public String getOccId() {
		return IATAsset.getString(this, IATTrade.OCCID);
	}

	public void setOccId(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.OCCID, value_);
	}
	public String getTraceId() {
		return IATAsset.getString(this, IATTrade.TRACE_ID);
	}

	public void setTraceId(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.TRACE_ID, value_);
	}

	public Double getSzNet() {
		return IATAsset.getDouble(this, IATTrade.SIZE_NET);
	}

	public void setSzNet(Double value_) {
		if (value_ != null) {
			value_ = Precision.round(value_, 4);
		}
		IATAsset.setRemovableValue(this, IATTrade.SIZE_NET, value_);
	}

	public Double getSzEff() {
		Double sz = _szEff != null ? _szEff : getSzNet();
		return sz;
	}

	public void setSzEff(Double value_) {
		_szEff = value_;
	}

	public String getTradeUserId() {
		return IATAsset.getString(this, IATTrade.TRADE_USER);
	}

	public void setTradeUserId(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.TRADE_USER, value_);
	}

	/** Invested amount of current holding. Recognizing Dividends, Fees, Covered Calls, etc. Resets on position close. */
	public Double getPxNet() {
		return IATAsset.getDouble(this, IATTrade.PX_NET);
	}

	public void setPxNet(Double value_) {
		if (value_ != null) {
			value_ = Precision.round(value_, 4);
		}
		IATAsset.setRemovableValue(this, IATTrade.PX_NET, value_);
	}

	/**
	 * Invested amount over life of account - Mostly relevant to equity. In addition to 'actual' recognizes Spread yield on equity. Does not
	 * reset on position close.
	 */
	public Double getPxCumulative() {
		return IATAsset.getDouble(this, IATTrade.PX_ACU);
	}

	public void setPxCumulative(Double value_) {
		if (value_ != null) {
			value_ = Precision.round(value_, 4);
		}
		IATAsset.setRemovableValue(this, IATTrade.PX_ACU, value_);
	}

	/** Actual averaged traded price */
	public Double getPxTrade() {
		return IATAsset.getDouble(this, IATTrade.PX_TRADE);
	}

	public void setPxTrade(Double value_) {
		if (value_ != null) {
			value_ = Precision.round(value_, 4);
		}
		IATAsset.setRemovableValue(this, IATTrade.PX_TRADE, value_);
	}

	// 	/** Averaged traded timestamp */
	// public Long getTsTrade() {
	// 	return IATAsset.getLong(this, IATTrade.TS_TRADE);
	// }

	// public void setTsTrade(Long value_) {
	// 	IATAsset.setRemovableValue(this, IATTrade.TS_TRADE, value_);
	// }


	public String getUnderlying() {
		return IATAsset.getString(this, IATTrade.UNDERLYING);
	}

	public void setUnderlying(String value_) {
		IATAsset.setRemovableValue(this, IATTrade.UNDERLYING, value_);
	}

	public void reset() {
		this.setSzNet(0.0);
		this.setPxNet(0.0);
		this.setPxTrade(0.0);
	}
}
