package com.isoplane.at.commons.model;

import java.util.Map;

public class ATProductUtil {

	static public String getOccId(Map<String, Object> product_) {
		String occId = (product_ instanceof ATProductGeneral) ? ((ATProductGeneral) product_).getAtId()
				: (product_ instanceof ATProductDetail) ? ((ATProductDetail) product_).getAtId() : null;
		// TODO: Maybe check Map for OCC_ID?
		return occId;
	}

	static public Double getPxLast(Map<String, Object> product_) {
		Double price = (product_ instanceof ATProductGeneral) ? ((ATProductGeneral) product_).getPxLast()
				: (product_ instanceof ATProductDetail) ? ((ATProductDetail) product_).getPxLast() : null;
		return price;
	}

	static public String getSymbol(Map<String, Object> product_) {
		String occId = getOccId(product_);
		if (occId != null && occId.length() > 6) {
			occId = occId.substring(0, 6).trim();
		}
		return occId;
	}
}
