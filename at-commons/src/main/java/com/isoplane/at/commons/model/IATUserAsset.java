package com.isoplane.at.commons.model;

public interface IATUserAsset {

	static final String ALERT_IS_ACTIVE = "act";
	static final String ALERT_PROPERTY = "prp";
	static final String ALERT_OPERATOR = "opr";
	static final String ALERT_VALUE = "val";
	static final String ALERT_MESSAGE = "msg";

	String getOccId();

	String getDateStr();

	Double getSize();

	String getUserId();

	Double getValue();

}