package com.isoplane.at.commons.util;

public class ATThreadingDelayedRequest extends ATDelayedRequest {

	private IATIdentifyableRunnable _run;
	private Thread _thread;

	public ATThreadingDelayedRequest(IATIdentifyableRunnable run_) {
		_run = run_;
	}

	public ATThreadingDelayedRequest(IATIdentifyableRunnable run_, long delay_) {
		new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		};
		_run = run_;
		setDelay(delay_);
	}

	@Override
	public void run() {
		_thread = new Thread(_run, _run.getIdentification().toString());
		_thread.start();
		// _run.run();
	}

	@Override
	public Object getIdentifyableId() {
		return _run.getIdentification();
	}

	public Runnable getRunnable() {
		return _run;
	}

}
