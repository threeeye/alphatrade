package com.isoplane.at.commons.model;

public class ATPair<T, U> {

	private T _a;
	private U _b;

	public ATPair(T a_, U b_) {
		setA(a_);
		setB(b_);
	}

	public T getA() {
		return _a;
	}

	public void setA(T a) {
		this._a = a;
	}

	public U getB() {
		return _b;
	}

	public void setB(U b) {
		this._b = b;
	}

}
