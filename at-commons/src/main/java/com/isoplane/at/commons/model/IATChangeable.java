package com.isoplane.at.commons.model;

import java.io.Serializable;

public interface IATChangeable extends Serializable {

	boolean isChanged();
}
