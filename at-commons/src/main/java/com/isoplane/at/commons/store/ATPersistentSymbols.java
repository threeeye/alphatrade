package com.isoplane.at.commons.store;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;

public class ATPersistentSymbols extends ATPersistentLookupBase {

	private static final long serialVersionUID = 1L;

	public ATPersistentSymbols() {
		super();
	}

	public ATPersistentSymbols(Map<String, Object> data_) {
		super(data_);
	}

	@Override
	public String getAtId() {
		return "symbols";
	}

	@Override
	public void setAtId(String id_) {
		super.setAtId("symbols");
	}

	public Set<String> getBlacklist() {
		Set<String> symbols = getSymbols("blacklist");
		if (symbols == null) {
			symbols = new HashSet<>();
		}
		return symbols;
	}

	public void setBlacklist(Collection<String> symbols_) {
		setSymbols("blacklist", symbols_);
	}

	public Set<String> getPositions() {
		Set<String> symbols = getSymbols("positions");
		if (symbols == null) {
			symbols = new HashSet<>();
		}
		return symbols;
	}

	public void setPositions(Collection<String> symbols_) {
		setSymbols("positions", symbols_);
	}

	public Set<String> getWatchlist() {
		Set<String> symbols = getSymbols("watchlist");
		if (symbols == null) {
			symbols = new HashSet<>();
		}
		return symbols;
	}

	public void setWatchlist(Collection<String> symbols_) {
		setSymbols("watchlist", symbols_);
	}

	private Set<String> getSymbols(String id_) {
		String symbolStr = StringUtils.isNotBlank(id_) ? (String) super.get(id_) : null;
		Set<String> symbols = symbolStr != null ? new TreeSet<>(Arrays.asList(symbolStr.split(","))) : new TreeSet<>();
		return symbols;
	}

	private void setSymbols(String id_, Collection<String> symbols_) {
		if (StringUtils.isBlank(id_) || symbols_ == null || symbols_.isEmpty())
			return;
		String joinStr = String.join(",", symbols_);
		super.put(id_, joinStr);
	}

}
