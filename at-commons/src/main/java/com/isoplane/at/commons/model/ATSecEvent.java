package com.isoplane.at.commons.model;

public class ATSecEvent {

	transient private boolean isChanged;
	transient private String occId;

	public String getOccId() {
		return occId;
	}

	public void setOccId(String occId) {
		this.occId = occId;
	}

	public boolean isChanged() {
		return isChanged;
	}

	public void setChanged(boolean isChanged) {
		this.isChanged = isChanged;
	}
}
