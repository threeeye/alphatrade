package com.isoplane.at.commons.model;

import java.util.Map;

public interface IATProductDescription extends IATAsset {

	/** @return Formatted ID */
	String getLabel();

	/** @return Common name */
	String getName();

	/** @return Detailed asset description */
	String getDescription();

	/** @return Stock, Option, Commodity, Future, etc... */
	String getModel();

	public String getSector();

	public String getIndustry();

	static public String getName(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.NAME);
	}

	static public void setName(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.NAME, value_);
	}

	static public String getLabel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LABEL);
	}

	static public void setLabel(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.LABEL, value_);
	}

	static public String getDescription(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DESCRIPTION);
	}

	static public void setDescription(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.DESCRIPTION, value_);
	}

	static public String getSector(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.SECTOR);
	}

	static public void setSector(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.SECTOR, value_);
	}

	static public String getIndustry(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.INDUSTRY);
	}

	static public void setIndustry(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.INDUSTRY, value_);
	}

	static public String getModel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODEL);
	}

	static public void setModel(Map<String, Object> map_, String value_) {
		map_.put(IATAssetConstants.MODEL, value_);
	}

}
