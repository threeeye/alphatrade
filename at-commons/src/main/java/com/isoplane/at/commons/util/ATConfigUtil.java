package com.isoplane.at.commons.util;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isoplane.at.commons.ATException;

import org.apache.commons.configuration2.BaseConfiguration;
import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATConfigUtil {

	static final Logger log = LoggerFactory.getLogger(ATConfigUtil.class);

	static private Configuration _config;
	static private String[] _propertiesPath;
	static int _loadCount;
	static Map<String, Object> _runtimeConfig;

	static public Configuration init(String propertiesPath_, boolean isReloading_) {
		return init(null, propertiesPath_, isReloading_);
	}

	static public Configuration init(String rootPath_, String propertiesPath_, boolean isReloading_) {
		_runtimeConfig = new HashMap<>();
		if (StringUtils.isBlank(propertiesPath_)) {
			_config = new BaseConfiguration();
			_propertiesPath = new String[0];
			log.warn(String.format("Empty config path!"));
		} else {
			if (StringUtils.isBlank(rootPath_)) {
				_propertiesPath = propertiesPath_.split(",");
			} else {
				String[] pathArray = propertiesPath_.split(",");
				ArrayList<String> pathList = new ArrayList<>();
				for (String relPath : pathArray) {
					String absPath = Paths.get(rootPath_, relPath).toString();
					pathList.add(absPath);
				}
				_propertiesPath = pathList.toArray(new String[0]);
			}
			log.info(String.format("Reading properties: %s", Arrays.asList(_propertiesPath)));
			_config = loadConfiguration();
			if (isReloading_) {
				ATExecutors.init();
				ATExecutors.scheduleAtFixedRate(ATConfigUtil.class.getSimpleName(), new Runnable() {
					@Override
					public void run() {
						loadConfiguration();
					}
				}, 30000, 30000);
			}
		}
		return _config;
	}

	static public boolean isConfigured() {
		return _config != null;
	}

	static public Configuration config() {
		if (_config == null)
			throw new ATException(String.format("Configuration not initialized!"));
		return _config;
	}

	static public void overrideConfiguration(String key_, Object value_) {
		// Object o = _config.getList(key_);
		_runtimeConfig.put(key_, value_);
		_config.setProperty(key_, value_);
		// o = _config.getList(key_);
		// Object b = o;
	}

	static private Configuration loadConfiguration() {
		try {
			CompositeConfiguration cconfig = new CompositeConfiguration();
			for (String path : _propertiesPath) {
				if (_loadCount++ == 0) {
					log.info(String.format("Reading properties [%s]", path));
				} else {
					log.debug(String.format("Reading properties [%s]", path));
				}
				PropertiesConfiguration config;
				config = new Configurations().properties(new File(path));
				cconfig.addConfigurationFirst(config);
			}
			for (Entry<String, Object> entry : _runtimeConfig.entrySet()) {
				cconfig.setProperty(entry.getKey(), entry.getValue());
			}
			// cconfig.setListDelimiterHandler(new DefaultListDelimiterHandler(','));
			_config = cconfig;
			return _config;
		} catch (ConfigurationException ex) {
			log.error(String.format("Error loading configuration"), ex);
			return null;
		}
	}


}
