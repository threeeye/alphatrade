package com.isoplane.at.commons.util;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;

public class ATMorningstarCodes {

	static final Logger log = LoggerFactory.getLogger(ATMorningstarCodes.class);

	static private Map<Integer, ATMorningstarCode> _codeMap;

	public static void main(String[] args_) {
		Configuration config = new CompositeConfiguration();
		config.addProperty("file.morningstar.codes", args_[0]);
		ATMorningstarCodes.init(config);
	}

	static public void init(Configuration config_) {
		String path = config_.getString("file.morningstar.codes");
		File file = new File(path);
		// InputStream inputStream = ATMorningstarCodes.class.getResourceAsStream(path);
		try (Scanner scanner = new Scanner(file)) {
			_codeMap = new HashMap<>();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				ATMorningstarCode code = new ATMorningstarCode(line);
				_codeMap.put(code.industryId, code);
				log.debug("Code: " + code);
			}
		} catch (Exception ex) {
			throw new ATException(String.format("Error reading file [%s]", path), ex);
		}

	}

	static public ATMorningstarCode getIndustryCode(int code_) {
		if (_codeMap == null) {
			throw new ATException(String.format("%s not initialized", ATMorningstarCodes.class.getSimpleName()));
		}
		return _codeMap.get(code_);
	}

	public void generate() {
		InputStream inputStream = ATMorningstarCodes.class.getResourceAsStream("ATMorningstarCodes.txt");
		try (Scanner scanner = new Scanner(inputStream)) { // .useDelimiter("\\A")
			List<String> lineMongo = new ArrayList<>();
			List<String> lineTxt = new ArrayList<>();
			Map<String, String> lookupnMap = new HashMap<>();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				String[] codes = line.split(":");
				if (codes.length < 2) {
					continue;
				} else {
					String code = codes[0].trim();
					String value = codes[1].replaceAll("\"", "").trim();
					if (value.endsWith(",")) {
						value = value.substring(0, value.length() - 1);
					}
					value = String.format("\"%s\"", value);
					if (code.length() < 6) {
						lookupnMap.put(code, value);
					} else {
						String industryCode = code;
						String industry = value;
						String sectorCode = code.substring(0, 3);
						String sector = lookupnMap.get(sectorCode);
						String industryGroupCode = code.substring(0, 5);
						String industryGroup = lookupnMap.get(industryGroupCode);
						List<String> newLineMongo = new ArrayList<>();
						List<String> newLineTxt = new ArrayList<>();
						newLineMongo.add("_id:" + industryCode);
						newLineMongo.add("sectorCode:" + sectorCode);
						newLineMongo.add("industryGroupCode:" + industryGroupCode);
						newLineMongo.add("industryCode:" + industryCode);
						newLineMongo.add("sector:" + sector);
						newLineMongo.add("industryGroup:" + industryGroup);
						newLineMongo.add("industry:" + industry);
						String newLineMongoStr = String.format("{%s}", String.join(",", newLineMongo));
						lineMongo.add(newLineMongoStr);
						newLineTxt.add(sectorCode);
						newLineTxt.add(industryGroupCode);
						newLineTxt.add(industryCode);
						newLineTxt.add(sector);
						newLineTxt.add(industryGroup);
						newLineTxt.add(industry);
						String newLineTxtStr = String.join(",", newLineTxt);
						lineTxt.add(newLineTxtStr);
					}
				}
			}
			System.out.println("MONGO");
			System.out.println(String.join("\n", lineMongo));
			System.out.println("TXT");
			System.out.println(String.join("\n", lineTxt).replaceAll("\"", ""));
		}
	}

	public static class ATMorningstarCode {
		public int sectorId;
		public String sectorIdStr;
		public String sector;
		public int industryGroupId;
		public String industryGroupIdStr;
		public String industryGroup;
		public int industryId;
		public String industryIdStr;
		public String industry;

		public ATMorningstarCode(String line_) {
			String[] tokens = line_.split(",");

			sectorIdStr = tokens[0];
			sectorId = Integer.parseInt(sectorIdStr);
			sector = tokens[3];

			industryGroupIdStr = tokens[1];
			industryGroupId = Integer.parseInt(industryGroupIdStr);
			industryGroup = tokens[4];

			industryIdStr = tokens[2];
			industryId = Integer.parseInt(industryIdStr);
			industry = tokens[5];
		}

		@Override
		public String toString() {
			String str = String.format("{sector: %d, industryGroup: %d, industry: %d, %s, %s, %s}", sectorId, industryGroupId, industryId, sector,
					industryGroup, industry);
			return str;
		}

	}
}
