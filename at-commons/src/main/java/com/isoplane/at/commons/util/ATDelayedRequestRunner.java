package com.isoplane.at.commons.util;

import java.util.concurrent.DelayQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATDelayedRequestRunner {

	static final Logger log = LoggerFactory.getLogger(ATDelayedRequestRunner.class);

	private DelayQueue<ATDelayedRequest> _queue;
	private boolean _isRunning;
	private long _nanoDelay;
	private long _nextRunTime;
	private String _name;
	private boolean _isRejectingDuplicates;
	private ExecutorService _executor;

	/**
	 * @param delay_
	 *            - Delay in ms
	 */
	public ATDelayedRequestRunner(int poolSize_, long delay_, String name_, boolean isRejectingDuplicates_) {
		_name = name_;
		_queue = new DelayQueue<>();
		_executor = Executors.newWorkStealingPool(poolSize_);
		_isRejectingDuplicates = isRejectingDuplicates_;
		setDelay(delay_);
		run();
	}

	public long getDelay() {
		return _nanoDelay / 1000000;
	}

	public void setDelay(long delay_) {
		long delay = delay_ * 1000000;
		if (delay != _nanoDelay) {
			log.info(String.format("Changing [%s] period [%dms -> %dms]", _name, _nanoDelay / 1000000, delay_));
			_nanoDelay = delay;
		}
	}

	public void abort() {
		_isRunning = false;
		_executor.shutdownNow();
	}

	public int size() {
		return _queue.size();
	}

	public void run() {
		if (_isRunning)
			return;
		_isRunning = true;
		log.info(String.format("Starting [%s] with period [%dms]", _name, _nanoDelay / 1000000));
		Thread consumer = new Thread(new Runnable() {
			@Override
			public void run() {
				long logTime = System.currentTimeMillis() + 30000;
				while (_isRunning) {
					try {
						ATDelayedRequest req = _queue.poll(1000, TimeUnit.MILLISECONDS);
						if (req != null) {
							if (req instanceof ATThreadingDelayedRequest) {
								_executor.execute(((ATThreadingDelayedRequest) req).getRunnable());
							} else {
								req.run();
							}
						}
						if (_queue.isEmpty() && logTime - System.currentTimeMillis() < 0) {
							log.debug(String.format("Queue empty", _name));
							logTime = System.currentTimeMillis() + 30000;
						}
					} catch (Exception ex) {
						log.error(String.format("Error processing queue [%s]", _name), ex);
					}
				}
				_isRunning = false;
			}
		});
		consumer.start();
		// consumer.join();
	}

	synchronized public boolean add(ATDelayedRequest req_) {
		if (_isRejectingDuplicates && _queue.contains(req_)) {
			if (log.isDebugEnabled()) {
				log.debug(String.format("Rejecting duplicate [%s]", req_.getIdentifyableId()));
			}
			return false;
		}
		long time = System.nanoTime();
		long delay = Math.max(0, _nextRunTime - time);
		req_.setDelay(delay);
		req_.setInserted(time);
		_nextRunTime = time + _nanoDelay + delay;
		boolean result = _queue.add(req_);
		return result;
	}
}
