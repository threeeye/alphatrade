package com.isoplane.at.commons.util;

public interface IATIdentifyableRunnable extends Runnable {

	Object getIdentification();
	
	/** To reclaim memory when terminated */
	void purge();
}
