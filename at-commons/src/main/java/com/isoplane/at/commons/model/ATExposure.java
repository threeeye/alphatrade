package com.isoplane.at.commons.model;

public class ATExposure implements Comparable<ATExposure> {

	private String occId;
	private Double callAmount;
	private Double putAmount;
	private Double equityAmount;
	private Double totalAmount;
	private String meta;

	public ATExposure() {
	}

	public ATExposure(String occId_) {
		setOccId(occId_);
	}

	public String getOccId() {
		return occId;
	}

	public void setOccId(String value_) {
		this.occId = value_;
	}

	@Override
	public int compareTo(ATExposure other_) {
		if (this.getOccId() == null)
			return 1;
		if (other_.getOccId() == null)
			return -1;
		return (this.getOccId()).compareTo(other_.getOccId());
	}

	public Double getCallAmount() {
		return callAmount;
	}

	public void setCallAmount(Double callAmount) {
		this.callAmount = callAmount;
	}

	public Double getPutAmount() {
		return putAmount;
	}

	public void setPutAmount(Double putAmount) {
		this.putAmount = putAmount;
	}

	public Double getEquityAmount() {
		return equityAmount;
	}

	public void setEquityAmount(Double equityAmount) {
		this.equityAmount = equityAmount;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta_) {
		this.meta = meta_;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

}
