package com.isoplane.at.commons.model;

import java.util.HashMap;

import com.isoplane.at.commons.util.IATMapUitil;

public class ATUserSession extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	static public final String SESSION_ID = "ssn_id";
	static public final String USER_ID = "user_id";
	static public final String USER_NAME = "user_name";
	static public final String EXPIRATION = "exp";

	public String getSessionId() {
		return IATMapUitil.getString(this, SESSION_ID);
	}

	public void setSessionId(String value_) {
		IATMapUitil.setMapValue(this, SESSION_ID, value_);
	}

	public String getUserId() {
		return IATMapUitil.getString(this, USER_ID);
	}

	public void setUserId(String value_) {
		IATMapUitil.setMapValue(this, USER_ID, value_);
	}

	public long getExpiration() {
		return IATMapUitil.getLong(this, EXPIRATION);
	}

	public void setExpiration(long value_) {
		IATMapUitil.setMapValue(this, EXPIRATION, value_);
		;
	}

	public String getUserName() {
		return IATMapUitil.getString(this, USER_NAME);
	}

	public void setUserName(String value_) {
		IATMapUitil.setMapValue(this, USER_NAME, value_);
		;
	}

}
