package com.isoplane.at.commons.model;

import java.text.ParseException;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.util.ATFormats;

public interface IATDividend {

	String getOccId();

	String getDividendExDateStr();

	Date getDividendExDate();

	String getDividendRecordDateStr();

	Date getDividendRecordDate();

	String getDividendPayDateStr();

	Date getDividendPayDate();

	String getDividendEstimatedDS8();

	Date getDividendEstimatedDate();

	Double getDividend();

	Integer getDividendFrequency();

	Map<String, Object> getAsMap();

	static String getOccId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static Double getDividend(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.DIVIDEND);
	}

	static void setDividend(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIVIDEND, value_);
	}

	static Integer getDividendFrequency(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.DIV_FREQUENCY);
	}

	static void setDividendFrequency(Map<String, Object> map_, Integer value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_FREQUENCY, value_);
	}

	static String getDividendExDateStr(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DIV_EX_DATE_STR_OLD);
	}

	static void setDividendExDateStr(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_EX_DATE_STR_OLD, value_);
	}

	static Date getDividendExDate(Map<String, Object> map_) {
		String dstr = getDividendExDateStr(map_);
		return strToDate(dstr);
	}

	static String getDividendEstimatedDS8(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DIV_EST_DATE_STR);
	}

	static void setDividendEstimatedDS8(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_EST_DATE_STR, value_);
	}

	static Date getDividendEstimatedDate(Map<String, Object> map_) {
		String dstr = getDividendEstimatedDS8(map_);
		return strToDate(dstr);
	}

	public static String getDividendRecordDateStr(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DIV_REC_DATE_STR);
	}

	static void setDividendRecordDateStr(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_REC_DATE_STR, value_);
	}

	static Date getDividendRecordDate(Map<String, Object> map_) {
		String dstr = getDividendRecordDateStr(map_);
		return strToDate(dstr);
	}

	static String getDividendPayDateStr(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DIV_PAY_DATE_STR);
	}

	static void setDividendPayDateStr(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_PAY_DATE_STR, value_);
	}

	static Date getDividendPayDate(Map<String, Object> map_) {
		String dstr = getDividendPayDateStr(map_);
		return strToDate(dstr);
	}

	static String getSource(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.SOURCE);
	}

	static void setSource(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.SOURCE, value_);
	}

	static String getATId(IATDividend div_) {
		String dstr = div_.getDividendExDateStr();
		if (dstr == null)
			dstr = div_.getDividendEstimatedDS8();
		String atid = String.format("%s %s", div_.getOccId(), dstr);
		return atid;
	}

	static int compareTo(IATDividend this_, IATDividend other_) {
		if (other_ == null) {
			return this_ == null ? 0 : -1;
		}
		if (this_ == null) {
			return 1;
		}
		int cmp1 = this_.getOccId().compareTo(other_.getOccId());
		if (cmp1 != 0)
			return cmp1;
		String dstr1 = this_.getDividendExDateStr();
		if (dstr1 == null)
			dstr1 = this_.getDividendEstimatedDS8();
		String dstr2 = other_.getDividendExDateStr();
		if (dstr2 == null)
			dstr2 = other_.getDividendEstimatedDS8();
		if (dstr2 == null) {
			return dstr1 == null ? 0 : -1;
		}
		if (dstr1 == null) {
			return 1;
		}
		return dstr1.compareTo(dstr2);
	}

	static String toString(IATDividend div_) {
		String dStr = div_.getDividendExDateStr();
		if (dStr == null)
			dStr = div_.getDividendEstimatedDS8();
		String str = String.format("%-5s%s %.2f", div_.getOccId(), dStr, div_.getDividend());
		return str;
	}

	static Date strToDate(String dstr_) {
		if (StringUtils.isBlank(dstr_))
			return null;
		try {
			Date date = ATFormats.DATE_yyyyMMdd.get().parse(dstr_);
			return date;
		} catch (ParseException ex) {
			return null;
		}
	}

}
