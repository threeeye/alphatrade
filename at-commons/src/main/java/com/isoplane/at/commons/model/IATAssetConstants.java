package com.isoplane.at.commons.model;

public interface IATAssetConstants {

	// Timing
	static final Long MS_1D = 1000 * 60 * 60 * 24L;

	// General
	static final String OCCID = "occId";
	static final String TYPE = "typ";
	static final String DATA = "data";
	static final String DATE_NUM = "dnum";
	static final String DATE_STR = "dstr";
	static final String SOURCE = "src";
	static final String REGION = "rgn";
	static final String IMPORTANCE = "impt";
	static final String OWNER_ID = "ownId";
	// static final String USER_ID = "user";
	static final String ACTIVE = "actv";
	static final String INACTIVE = "inact";
	static final String PRODUCT_TYPE = "prd_typ";
	static final String USER_ID = "usrId";
	static final String TARGET_ID = "tgtId";
	static final String TRACE_ID = "id_trc";

	// Classification
	static final String NAME = "name";
	static final String DESCRIPTION = "dscr";
	static final String INDUSTRY = "ind";
	static final String SECTOR = "sect";
	static final String INDEX = "index";
	static final String IDX_SP500 = "sp500";
	static final String IDX_SP400 = "sp400";
	static final String IDX_DOW = "dow";
	static final String IDX_R3000 = "r3000";
	static final String MODEL = "model";

	// Option related
	static final String CALL = "C";
	static final String PUT = "P";
	static final String EXPIRATION_DATE_STR_OLD = "exp_s";
	static final String EXPIRATION_DATE_STR = "exp_ds6";
	static final String EXPIRATION_DATE_6 = "exp_6";
	static final String MARGIN = "mrgn";
	static final String OCCID_UNDERLYING = "undr";
	static final String OPTION_RIGHT = "o_rgt";
	static final String OPTION_TYPE = "o_typ";
	static final String PX_STRIKE = "px_strk";
	static final String RISK = "risk";
	static final String STRIKE = "strk";
	static final String DELTA = "o_dlt";
	static final String THETA = "o_the";
	static final String RHO = "o_rho";
	static final String GAMMA = "o_gam";
	static final String VEGA = "o_veg";
	static final String OPTION_MULTIPLIER = "o_mult";

	// Type Codes
	static final int STOCK_CODE = 1;
	static final int OPTION_CALL_CODE = 50;
	static final int OPTION_PUT_CODE = 55;
	static final int FUNDAMENTAL_CODE = 100;

	// Values
	static final String VAL_ACTUAL = "v_act";
	static final String VAL_CONSENSUS = "v_cns";
	static final String VAL_CUMULATIVE = "v_cum";
	static final String VAL_EXPECT = "v_exp";
	static final String VAL_MID = "v_mid";
	static final String VAL_PREVIOUS = "v_prv";
	static final String VAL_UNIT = "v_unit";
	static final String VAL_TOTAL = "v_tot";

	// Size
	static final String SIZE = "sz";
	static final String SIZE_ACU = "sz_acu"; /* accumulated */
	static final String SIZE_ASK = "sz_ask";
	static final String SIZE_BID = "sz_bid";
	static final String SIZE_LAST = "sz_lst";
	static final String SIZE_NET = "sz_net";
	static final String SIZE_TRADE = "sz_trd";
	static final String SIZE_ACTIVE = "sz_act";
	static final String SIZE_TOTAL = "sz_tot";

	// Price
	static final String PX_ACT = "px_act";
	static final String PX_ACU = "px_acu"; /* accumulated */
	static final String PX_ASK = "px_ask";
	static final String PX_BID = "px_bid";
	static final String PX_CHANGE = "px_chng";
	static final String PX_CLOSE = "px_cls";
	static final String PX_CLOSE_PREVIOUS = "px_cls_p";
	static final String PX_CB = "px_cb"; // Cost Basis
	static final String PX_HIGH = "px_hi";
	static final String PX_LAST = "px_lst";
	static final String PX_LOW = "px_lo";
	static final String PX_OPEN = "px_opn";
	static final String PX_TRADE = "px_trd";
	static final String PX_MAX = "px_max";
	static final String PX_MIN = "px_min";
	static final String PX_NET = "px_net";
	static final String PX_TIME = "px_tim";
	static final String PX_UL = "px_ul";
	// static final String PX_UNIT = "px_1";
	static final String PX_TRADE_ADJ = "px_trd_adj";
	static final String PX_TRADE_TOTAL = "px_trd_tot";
	static final String PX_THEORETICAL = "px_theo";
	static final String PX_YIELD = "px_yld";

	// Timestamps
	static final String TS = "ts";
	static final String TS_ASK = "ts_ask";
	static final String TS_BID = "ts_bid";
	static final String TS_LAST = "ts_lst";
	static final String TS_TRADE_OLD = "ts_trd_x";
	static final String TS_TRADE = "ts_trd";
	static final String TS_EXP = "ts_exp";
	static final String TS_EARN = "ts_earn";

	// Market Related
	static final String EXCH_ASK = "exc_ask";
	static final String EXCH_BID = "exc_bid";
	static final String OPEN_INTEREST = "opn_int";

	// Trade Related
	static final String TRADE_ID = "trd_id";
	static final String TRADE_ACCOUNT = "trd_acc";
	static final String TRADE_TYPE = "trd_typ";
	@Deprecated
	static final String TRADE_TALLY = "tly_on";
	static final String TRADE_USER = "trd_usr";
	static final String TRADE_GROUP = "trd_grp";
	static final String BROKER = "brkr";
	// static final String COUNT = "count";
	static final String EXCHANGE = "exch";
	@Deprecated
	static final String TRADE_DATE_STR = "trd_s"; // Replaced with TS_TRADE_OLD
	@Deprecated
	static final String TRADE_TOTAL = "tot"; // Replaced with PX_TRADE_TOTAL
	static final String TRADE_COMMENT = "trd_cmt";

	// Fundamentals related
	static final String EARNINGS_DATE_STR = "earn_ds8";
	static final String EARNINGS_DATE_STR_OLD = "edstr";
	static final String DIV_EX_DATE_STR = "div_ex_ds8";
	static final String DIV_EST_DATE_STR = "div_est_ds8";
	static final String DIV_EX_DATE_STR_OLD = "divex_dstr";
	static final String DIV_REC_DATE_STR = "divrc_dstr";
	static final String DIV_PAY_DATE_STR = "divpy_dstr";
	static final String DIVIDEND = "div";
	static final String DIV_FREQUENCY = "div_frq";
	static final String LATEST_DATE_STR = "latest_ds8";
	static final String MARKET_CAP = "mcap";
	static final String DIV_ANNUAL = "div_a";

	static final String PERIOD = "period";
	public static final String PERIOD_ANNUAL = "A";
	public static final String PERIOD_QUARTERLY = "Q";
	static final String SYMBOL = "symbol";

	static final String PE_GROWTH_RATIO = "pe_gr";
	static final String TRAILING_PE = "pe_tr";

	static final String REV_ANNUAL = "rev_a";
	static final String REV_MLT = "rev_mlt";
	static final String REV_QUARTERLY = "rev_q";
	static final String REV_GROWTH_1Q = "rev_growth_1q";
	static final String REV_GROWTH_1Y = "rev_growth_1y";
	static final String REV_GROWTH_AVG_3Y = "rev_growth_avg_3y";
	static final String REV_GROWTH_YoY = "rev_growth_yoy";

	// Statistics related
	static final String AVG_MAP = "avg_map";
	static final String HV_MAP = "hv_map";
	static final String IV = "iv";
	static final String MAX_MAP = "max_map";
	static final String MIN_MAP = "min_map";
	static final String MODE_AVG = "mode_avg";
	static final String MODE_HV = "mode_hv";
	static final String PX_AVG = "px_avg";
	static final String PX_HV = "px_hv";
	static final String YIELD_APY = "yld_apy";
	static final String YIELD_PCT = "yld_pct";

	// QUery related
	static final String DATA_AGE = "dataAge";
	static final String DAYS_MAX = "daysMax";
	static final String DAYS_MIN = "daysMin";
	static final String DIVIDEND_OVERRIDE = "overrideDiv";
	static final String FLAGS = "flags";
	static final String OPPORTUNITY_COUNT = "oppCount";
	static final String SPREAD_MAX = "spreadMax";
	static final String SPREAD_MIN = "spreadMin";
	static final String STRIKE_MAX = "strikeMax";
	static final String STRIKE_MIN = "strikeMin";
	static final String STRIKE_STRATEGY = "strikeStrat";
	static final String VOLATILITY_OVERRIDE = "overrideVlt";
	static final String YIELD_MIN_ABS = "yieldMinAbs";
	static final String YIELD_MIN_PCT = "yieldMinPct";

	// Other
	static final String VOLUME = "vol";
	static final String VOLUME_DAILY = "vol_d";
	static final String OPTION_IV = "opt_iv";
	static final String OPTION_HV = "opt_hv";
	static final String NOTE = "note";
	// static final String COMMENT = "cmt";
	static final String LABEL = "label";
	static final String MODE = "mode";
	static final String RATIO = "ratio";
	static final String SP500 = "sp500";
	static final String RUSSELL3000 = "r3000";
	static final String EXTERNAL_ID = "ext_id";
	static final String REFERENCE_ID = "ref_id";

}
