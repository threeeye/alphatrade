package com.isoplane.at.commons.service;

public interface IATMarketDataPublisher {

	void subscribe(IATMarketDataSubscriber subscriber);

	void unsubscribe(IATMarketDataSubscriber subscriber);

}
