package com.isoplane.at.commons.model.serializers;

import java.lang.reflect.Type;
import java.util.Date;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.isoplane.at.commons.util.ATFormats;

public class ATDateSerializer implements JsonSerializer<Date> {

	@Override
	public JsonElement serialize(Date src_, Type typeOfSrc_, JsonSerializationContext context_) {
		JsonElement result = src_ == null ? null : new JsonPrimitive(ATFormats.DATE_json.get().format(src_));
		return result;
	}

}
