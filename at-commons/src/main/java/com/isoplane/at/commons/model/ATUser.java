package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class ATUser extends HashMap<String, Object> implements IATUser {

	private static final long serialVersionUID = 1L;

	private static final String MESSAGE_COMBINED = "combined";

	transient private Map<String, Set<String>> _tokenMap;

	public ATUser() {
		_tokenMap = new HashMap<>();
		_tokenMap.put(MESSAGE_MIN, new HashSet<>());
		_tokenMap.put(MESSAGE_ALL, new HashSet<>());
		_tokenMap.put(MESSAGE_COMBINED, new HashSet<>()); // Includes min+all -> target min includes all
	}

	public ATUser(IATUser tmpl_) {
		this();
		if (tmpl_ != null) {
			this.setAtId(tmpl_.getAtId());
			this.setEmail(tmpl_.getEmail());
			this.setRoles(tmpl_.getRoles());
			this.setMessageTokens(tmpl_.getMessageTokens());
		}
	}

	public ATUser(Map<String, Object> tmpl_) {
		this();
		if (tmpl_ != null) {
			this.setAtId(IATAsset.getAtId(tmpl_));
			this.setEmail(IATAsset.getString(tmpl_, EMAIL));
			@SuppressWarnings("unchecked")
			List<String> roles = (List<String>) tmpl_.get(ROLE);
			this.setRoles(roles);
			@SuppressWarnings("unchecked")
			List<String> mtokens = (List<String>) tmpl_.get(MESSAGE_TOKENS);
			this.setMessageTokens(mtokens);
		}
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> tmpl_) {
		super.putAll(tmpl_);
	}

	@Override
	public String getAtId() {
		return IATAsset.getAtId(this);
	}

	// @Override
	public void setAtId(String value_) {
		IATAsset.setAtId(this, value_);
	}

	@Override
	public String getEmail() {
		return IATAsset.getString(this, EMAIL);
	}

	public void setEmail(String value_) {
		IATAsset.setMapValue(this, EMAIL, value_);
	}

	@Override
	public List<String> getMessageTokens() {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) super.get(MESSAGE_TOKENS);
		return list;
	}

	public void setMessageTokens(List<String> value_) {
		if (value_ == null)
			return;
		for (String token : value_) {
			int idx = token.indexOf(":");
			String mode = token.substring(0, idx);
			String value = token.substring(idx + 1);
			updateTokenMap(mode, value);
			// String[] tokens = token.split(":");
			// _tokenMap.get(mode).add(token.substring(idx + 1));
		}
		IATAsset.setMapValue(this, MESSAGE_TOKENS, value_);
	}

	public Set<String> getMessageTokens(String mode_) {
		switch (mode_) {
		case MESSAGE_MIN:
			return _tokenMap.get(MESSAGE_COMBINED); // ALL subscribers also get MIN
		default:
			return _tokenMap.get(mode_);
		}
	}

	private boolean updateTokenMap(String mode_, String token_) {
		if (StringUtils.isBlank(mode_) || StringUtils.isBlank(token_))
			return false;
		Set<String> modeTokens = _tokenMap.get(mode_);
		switch (mode_) {
		case "min":
			if (modeTokens.contains(token_))
				return false;
			_tokenMap.get(MESSAGE_ALL).remove(token_);
			_tokenMap.get(MESSAGE_COMBINED).add(token_);
			modeTokens.add(token_);
			return true;
		case "all":
			if (modeTokens.contains(token_))
				return false;
			_tokenMap.get(MESSAGE_MIN).remove(token_);
			_tokenMap.get(MESSAGE_COMBINED).add(token_);
			modeTokens.add(token_);
			return true;
		case "none":
			_tokenMap.get(MESSAGE_ALL).remove(token_);
			_tokenMap.get(MESSAGE_MIN).remove(token_);
			_tokenMap.get(MESSAGE_COMBINED).remove(token_);
			return true;
		default:
			return false;
		}
	}

	public boolean updateMessageToken(String mode_, String token_) {
		boolean result = updateTokenMap(mode_, token_);
		if (result) {
			List<String> tokens = new ArrayList<>();
			_tokenMap.get(MESSAGE_MIN).forEach(t -> tokens.add(String.format("%s:%s", MESSAGE_MIN, t)));
			_tokenMap.get(MESSAGE_ALL).forEach(t -> tokens.add(String.format("%s:%s", MESSAGE_ALL, t)));
			IATAsset.setMapValue(this, MESSAGE_TOKENS, tokens);
		}
		return result;
	}

	@Override
	public List<String> getRoles() {
		@SuppressWarnings("unchecked")
		List<String> list = (List<String>) super.get(ROLE);
		return list;
	}

	public void setRoles(List<String> value_) {
		if (value_ == null) {
			this.remove(ROLE);
		} else {
			IATAsset.setMapValue(this, ROLE, value_);
		}
	}

}
