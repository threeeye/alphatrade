package com.isoplane.at.commons.model;

public interface IATEquityDescription {

	String getOccId();

	String getLabel();

	String getSymbol();

	String getDescription();

	String getIndustry();

	String getSector();
		
}
