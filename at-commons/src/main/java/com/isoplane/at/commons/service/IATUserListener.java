package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.util.EATChangeOperation;

public interface IATUserListener {

	void notify(EATChangeOperation operation, IATUser user);

}
