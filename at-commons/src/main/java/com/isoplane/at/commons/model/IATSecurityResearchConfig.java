package com.isoplane.at.commons.model;

import java.util.List;
import java.util.Map;

public interface IATSecurityResearchConfig extends IATAsset {

	static final String TYPE_OPTION = "opt";
	static final String TYPE_SECURITY = "sec";

	static final String AGE_MAX = "age_max";
	static final String AVG_MIN_MRG = "avg_min_mrg";
	static final String AVG_MAX_MRG = "avg_max_mrg";
	static final String HV_MIN = "hv_min";
	static final String HV_MAX = "hv_max";
	static final String PX_MIN_CHG_PCT = "px_min_chg";
	static final String PX_MAX_CHG_PCT = "px_max_chg";
	static final String IS_OWNED = "is_owned";
	static final String IS_NOTES = "is_notes";
	static final String IS_ALERTS = "is_alerts";
	static final String SYMBOLS = "symbols";
	static final String IS_SKIP_EARNINGS = "is_skip_earn";
	static final String RISK_FACTOR_EARNINGS = "risk_earn";

	static final String UL_CONFIG_ID = "ul_config_id";
	static final String UL_CONFIG = "ul_config";

	static final String STRAT_DAYS_MAX = "strt_days_max";
	static final String STRAT_APY_MIN = "strt_apy_min";
	static final String STRAT_PX_MIN = "strt_px_min";
	static final String STRAT_DATECOUNT_MAX = "strt_dcnt_max";
	static final String STRAT_RISK_MAX = "strt_risk_max";
	static final String STRAT_MARGIN_MIN = "strt_mrg_min";
	static final String SPREAD_WIDTH_MAX = "sprd_width_max";
	static final String SPREAD_STEP_MAX = "sprd_step_max";
	static final String SPREAD_STEP_RISK = "sprd_step_risk";
	static final String SPREAD_MODE_APY = "sprd_mode_apy";

	// public Double eqtPxMin;
	// public Double eqtPxMax;

	// public Double eqtPxChgMinD;
	// public Double eqtPxChgMaxD;

	// public String eqtIndustry;
	// public String eqtSector;
	// public String eqtSymbols;

	// public String eqtAvgMode;
	// public Double eqtAvgMin;
	// public Double eqtAvgMax;

	// public String eqtHVMode;
	// public Double eqtHVMin;
	// public Double eqtHVMax;

	// public Long eqtRecMaxAge;
	// public Boolean eqtIsOwned;
	// public Boolean eqtHasNotes;
	// public Boolean eqtHasAlerts;

	// transient public Set<String> symbols;

	// public Boolean skipEarn;

	// public Long recMaxAge;
	// public Double earningsRiskFactor;

	// Strategy
	// public Integer stratMaxDays;
	// public Double stratApyMin;
	// public Double stratPxMin;
	// public Integer stratDateMaxCount;
	// public Double stratRiskMax;

	// public Double stratMarginMin;

	// Options

	// public Double spreadWidthMaxPx;
	// public Integer spreadWidthMaxCount;
	// public Double spreadWidthCountRiskFactor;
	// public String spreadApyMode;

	void setAtId(String value);

	String getUserId();

	void setUserId(String value);

	String getType();

	String getName();

	String getUnderlyingConfigId();

	Long getEquityMaxAge();

	Double getPxMin();

	Double getPxMax();

	Double getPxMinChange();

	Double getPxMaxChange();

	String getIndustry();

	String getSector();

	String getAvgMode();

	Double getAvgMarginMin();

	Double getAvgMarginMax();

	String getHVMode();

	Double getHVMin();

	Double getHVMax();

	Boolean isOwned();

	Boolean hasAlerts();

	Boolean hasNotes();

	Boolean isSkipEarnings();

	List<String> getSymbols();

	Double getEarningsRiskFactor();

	Integer getStrategyDaysMax();

	Double getStrategyApyMin();

	Double getStrategyPxMin();

	Integer getStrategyMaxCountPerDate();

	Double getStrategyRiskMax();

	Double getStrategyMarginMin();

	Double getSpreadWidthPxMax();

	Double getSpreadWidthStepMax();

	Double getSpreadWidthStepRisk();

	String getSpreadApyMode();

	void cleanup();

	static String getUnderlyingConfigId(Map<String, Object> map_) {
		return IATAsset.getString(map_, UL_CONFIG_ID);
	}

	static void setUnderlyingConfigId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, UL_CONFIG_ID, value_);
	}

	static String getDefaultAvgMode() {
		return IATStatistics.SMA60;
	}

	static String getDefaultHVMode() {
		return IATStatistics.HV60;
	}

	static Long getEquityMaxAge(Map<String, Object> map_) {
		return IATAsset.getLong(map_, AGE_MAX);
	}

	static void setEquityMaxAge(Map<String, Object> map_, Long value_) {
		IATAsset.setMapValue(map_, AGE_MAX, value_);
	}

	static Double getAvgMarginMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, AVG_MAX_MRG);
	}

	static void setAvgMarginMax(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, AVG_MAX_MRG, value_);
	}

	static Double getAvgMarginMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, AVG_MIN_MRG);
	}

	static void setAvgMarginMin(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, AVG_MIN_MRG, value_);
	}

	static Double getHVMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, HV_MAX);
	}

	static void setHVMax(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, HV_MAX, value_);
	}

	static Double getHVMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, HV_MIN);
	}

	static void setHVMin(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, HV_MIN, value_);
	}

	static Double getPxMaxChange(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, PX_MAX_CHG_PCT);
	}

	static void setPxMaxChange(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, PX_MAX_CHG_PCT, value_);
	}

	static Double getPxMinChange(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, PX_MIN_CHG_PCT);
	}

	static void setPxMinChange(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, PX_MIN_CHG_PCT, value_);
	}

	static Double getEarningsRiskFactor(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, RISK_FACTOR_EARNINGS);
	}

	static void setEarningsRiskFactor(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, RISK_FACTOR_EARNINGS, value_);
	}

	static Boolean isOwned(Map<String, Object> map_) {
		return IATAsset.getBoolean(map_, IS_OWNED);
	}

	static void setOwned(Map<String, Object> map_, Boolean value_) {
		IATAsset.setMapValue(map_, IS_OWNED, value_);
	}

	static Boolean isSkipEarnings(Map<String, Object> map_) {
		return IATAsset.getBoolean(map_, IS_SKIP_EARNINGS);
	}

	static void setSkipEarnings(Map<String, Object> map_, Boolean value_) {
		IATAsset.setMapValue(map_, IS_SKIP_EARNINGS, value_);
	}

	static Boolean hasAlerts(Map<String, Object> map_) {
		return IATAsset.getBoolean(map_, IS_ALERTS);
	}

	static void setHasAlerts(Map<String, Object> map_, Boolean value_) {
		IATAsset.setMapValue(map_, IS_ALERTS, value_);
	}

	static Boolean hasNotes(Map<String, Object> map_) {
		return IATAsset.getBoolean(map_, IS_NOTES);
	}

	static void setHasNotes(Map<String, Object> map_, Boolean value_) {
		IATAsset.setMapValue(map_, IS_NOTES, value_);
	}

	static List<String> getSymbols(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		List<String> symbols = (List<String>) map_.get(SYMBOLS);
		return symbols;
	}

	static void setSymbols(Map<String, Object> map_, List<String> symbols_) {
		map_.put(SYMBOLS, symbols_);
	}

	static Integer getStrategyDaysMax(Map<String, Object> map_) {
		return IATAsset.getInt(map_, STRAT_DAYS_MAX);
	}

	static void setStrategyDaysMax(Map<String, Object> map_, Integer value_) {
		IATAsset.setMapValue(map_, STRAT_DAYS_MAX, value_);
	}

	static Double getStrategyApyMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, STRAT_APY_MIN);
	}

	static void setStrategyApyMin(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, STRAT_APY_MIN, value_);
	}

	static Double getStrategyPxMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, STRAT_PX_MIN);
	}

	static void setStrategyPxMin(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, STRAT_PX_MIN, value_);
	}

	static Integer getStrategyMaxCountPerDate(Map<String, Object> map_) {
		return IATAsset.getInt(map_, STRAT_DATECOUNT_MAX);
	}

	static void setStrategyMaxCountPerDate(Map<String, Object> map_, Integer value_) {
		IATAsset.setMapValue(map_, STRAT_DATECOUNT_MAX, value_);
	}

	static Double getStrategyRiskMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, STRAT_RISK_MAX);
	}

	static void setStrategyRiskMax(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, STRAT_RISK_MAX, value_);
	}

	static Double getStrategyMarginMin(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, STRAT_MARGIN_MIN);
	}

	static void setStrategyMarginMin(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, STRAT_MARGIN_MIN, value_);
	}

	static Double getSpreadWidthPxMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, SPREAD_WIDTH_MAX);
	}

	static void setSpreadWidthPxMax(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, SPREAD_WIDTH_MAX, value_);
	}

	static Double getSpreadWidthStepMax(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, SPREAD_STEP_MAX);
	}

	static void setSpreadWidthStepMax(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, SPREAD_STEP_MAX, value_);
	}

	static Double getSpreadWidthStepRisk(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, SPREAD_STEP_RISK);
	}

	static void setSpreadWidthStepRisk(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, SPREAD_STEP_RISK, value_);
	}

	static String getSpreadApyMode(Map<String, Object> map_) {
		return IATAsset.getString(map_, SPREAD_MODE_APY);
	}

	static void setSpreadApyMode(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, SPREAD_MODE_APY, value_);
	}

}