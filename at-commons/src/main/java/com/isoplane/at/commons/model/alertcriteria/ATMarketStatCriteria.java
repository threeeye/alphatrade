package com.isoplane.at.commons.model.alertcriteria;

import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;

public class ATMarketStatCriteria extends ATBaseCriteria {

	// private String _prop;
	private Function<IATStatistics, Double> _fnc;
	private double _refNumber;
	private int _condition;
	private String _note;

	private ATMarketStatCriteria(String userId_, String occId_, String criteria_, Function<IATStatistics, Double> fnc_, int condition_,
			double number_, String note_) {
		super(userId_, occId_, criteria_);
		_fnc = fnc_;
		_condition = condition_;
		_refNumber = number_;
		_note = note_;
	}

	static public ATMarketStatCriteria create(String userId_, ATAlertRule alert_) {
		if (StringUtils.isBlank(userId_) || alert_ == null || StringUtils.isBlank(alert_.getOccId()) || StringUtils.isBlank(alert_.getOperator())
				|| StringUtils.isBlank(alert_.getValue()))
			return null;
		String prop = alert_.getProperty();
		Function<IATStatistics, Double> fnc = buildFunction(prop);
		if (fnc == null)
			return null;
		Integer condition = getCondition(alert_);
		if (condition == null)
			return null;
		String criteria = getOperatorString(condition);
		Double value = NumberUtils.createDouble(alert_.getValue());
		if (value == null || criteria == null)
			return null;
		criteria = String.format("%s %s %.2f", prop, criteria, value);
		ATMarketStatCriteria crit = new ATMarketStatCriteria(userId_, alert_.getOccId(), criteria, fnc, condition, value, alert_.getMessage());
	//	crit = null;
		return crit;
	}

	@Override
	public Boolean test(IATMarketData mkt_, IATStatistics stats_) {
		if (mkt_ == null || stats_ == null || !isOccIdMatch(stats_.getOccId()))
			return false;
		Double value = _fnc.apply(stats_);
		Double px = mkt_.getPxLast();
		if (value == null || value == 0 || px == null)
			return false;
		double pct = (px - value) / value;
		return testNumber(_condition, _refNumber, pct);
	}

	@Override
	public String label(IATMarketData data_, IATStatistics stats) {
		return StringUtils.isBlank(_note) ? super.label() : String.format("'%s'", _note);
		// Double price = data_ != null ? data_.getPxLast() : null;
		// if (price == null)
		// return null;
		// try {
		// double pct = 100 * (price - _refNumber) / _refNumber;
		// String sign = pct <= 0 ? "" : "+";
		// String label = String.format("%s (%s%.2f%%)", label(), sign, pct);
		// return label;
		// } catch (Exception ex) {
		// String label = label();
		// log.error(String.format("Error labeling [%s/%s]", this.getClass().getSimpleName(), label), ex);
		// return label;
		// }
	}

	static private Function<IATStatistics, Double> buildFunction(String prop_) {
		Function<IATStatistics, Double> fnc = null;
		if (prop_ != null && prop_.contains(":")) {
			prop_ = prop_.split(":")[1].toLowerCase();
		}
		if (!IATStatistics.MARKET_PROPS.contains(prop_))
			return null;
		final String prop = prop_;
		if (prop.startsWith("sma")) {
			fnc = s -> s.getAvg(prop);
		} else if (prop.startsWith("min")) {
			fnc = s -> s.getMin(prop);
		} else if (prop.startsWith("max")) {
			fnc = s -> s.getMax(prop);
		}
		return fnc;
	}

	@Override
	@Deprecated
	public Boolean test(ATSecurity sec_) {
		return false;
	}

	@Override
	@Deprecated
	public Boolean test(ATPersistentSecurity sec_) {
		return false;
	}

	@Override
	@Deprecated
	public String label(ATPersistentSecurity sec_) {
		return label();
	}

}
