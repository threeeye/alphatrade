package com.isoplane.at.commons.model;

import java.util.Arrays;
import java.util.List;

public interface IATStatistics {

	String HV10 = "hv10";
	String HV20 = "hv20"; // ~ 1 month (21.1667 days)
	String HV30 = "hv30";
	String HV60 = "hv60"; // ~ 3 months (63.5 days)
	String HV90 = "hv90";
	String HV125 = "hv125";
	String HV254 = "hv254";
	String[] HVS = new String[] { HV10, HV20, HV30, HV60, HV90, HV125, HV254 };

	String MAX60 = "max60";
	String MAX125 = "max125";
	String MAX254 = "max254";
	String[] MAXS = new String[] { MAX60, MAX125, MAX254 };

	String MIN60 = "min60";
	String MIN125 = "min125";
	String MIN254 = "min254";
	String[] MINS = new String[] { MIN60, MIN125, MIN254 };

	String SMA60 = "sma60";
	String SMA125 = "sma125";
	String SMA254 = "sma254";
	String[] AVGS = new String[] { SMA60, SMA125, SMA254 };

	List<String> MARKET_PROPS = Arrays.asList(MAX60, MAX125, MAX254, MIN60, MIN125, MIN254, SMA60, SMA125, SMA254);
	List<String> SCALAR_PROPS = Arrays.asList(HV10, HV20, HV30, HV60, HV90, HV125, HV254, IATAssetConstants.IV);

	String getOccId();

	Double getMax60();

	Double getMax125();

	Double getMax254();

	Double getMax(String mode);

	Double getMin60();

	Double getMin125();

	Double getMin254();

	Double getMin(String mode);

	Double getIV();

	Double getHV(String mode);

	Double getHV10();

	Double getHV20();

	Double getHV30();

	Double getHV60();

	Double getHV90();

	Double getHV125();

	Double getHV254();

	Double getAvg(String mode);

	Double getSma60();

	Double getSma125();

	Double getSma254();

}
