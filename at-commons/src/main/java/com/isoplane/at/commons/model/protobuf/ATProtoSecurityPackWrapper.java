package com.isoplane.at.commons.model.protobuf;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.protobuf.FundamentalProtos.Fundamental;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.ProductDescriptionProtos.ProductDescription;
import com.isoplane.at.commons.model.protobuf.SecurityPackProtos.SecurityPack;;

public class ATProtoSecurityPackWrapper {

	static public final String TYPE_MOVER_PACK = "pack_mov";
	static public final String TYPE_TREND_PACK = "pack_trnd";

	private SecurityPack _protoSecurityPack;
	private ATSecurityPack _value;

	public ATProtoSecurityPackWrapper(SecurityPack proto_) {
		this._protoSecurityPack = proto_;
	}

	public SecurityPack proto() {
		return this._protoSecurityPack;
	}

	public ATSecurityPack value() {
		if (_value == null) {
			_value = toSecurityPack(_protoSecurityPack);
		}
		return _value;
	}

	static public ATSecurityPack toSecurityPack(SecurityPack tmpl_) {
		if (tmpl_ == null)
			return null;
		Map<String, ATMarketData> mktMap = null;
		Map<String, ATFundamental> fndMap = null;
		Map<String, ATEquityDescription> dscMap = null;
		Map<String, MarketData> protoMktMap = tmpl_.getMarketDataMap();
		if (protoMktMap != null && !protoMktMap.isEmpty()) {
			mktMap = new TreeMap<>();
			for (Entry<String, MarketData> entry : protoMktMap.entrySet()) {
				ATMarketData mkt = ATProtoMarketDataWrapper.toMarketData(entry.getValue());
				mktMap.put(entry.getKey(), mkt);
			}
		}
		Map<String, Fundamental> protoFndMap = tmpl_.getFundamentalsMap();
		if (protoFndMap != null && !protoFndMap.isEmpty()) {
			fndMap = new TreeMap<>();
			for (Entry<String, Fundamental> entry : protoFndMap.entrySet()) {
				ATFundamental fnd = ATProtoFundamentalWrapper.toFundamental(entry.getValue());
				fndMap.put(entry.getKey(), fnd);
			}
		}
		Map<String, ProductDescription> protoDscMap = tmpl_.getDescriptionsMap();
		if (protoDscMap != null && !protoDscMap.isEmpty()) {
			dscMap = new TreeMap<>();
			for (Entry<String, ProductDescription> entry : protoDscMap.entrySet()) {
				ATEquityDescription dsc = ATProtoProductDescriptionWrapper.toProductDescription(entry.getValue());
				dscMap.put(entry.getKey(), dsc);
			}
		}
		ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);
		return pack;
	}

	static public SecurityPack toSecurityPack(ATSecurityPack pack_) {
		if (pack_ == null)
			return null;

		String type = "SEC_PACK";

		SecurityPack.Builder builder = SecurityPack.newBuilder();
		builder.setType(type);
		Date now = new Date();
		Map<String, ATMarketData> mktMap = pack_.getMarketMap();
		if (mktMap != null) {
			for (Entry<String, ATMarketData> entry : mktMap.entrySet()) {
				MarketData mkt = ATProtoMarketDataWrapper.toMarketData(entry.getValue());
				builder.putMarketData(entry.getKey(), mkt);
			}
		}
		Map<String, ATFundamental> fndMap = pack_.getFundamentals();
		if (fndMap != null) {
			for (Entry<String, ATFundamental> entry : fndMap.entrySet()) {
				Fundamental fnd = ATProtoFundamentalWrapper.toFundamental(entry.getValue(), now);
				builder.putFundamentals(entry.getKey(), fnd);
			}
		}
		Map<String, ATEquityDescription> dscMap = pack_.getDescriptions();
		if (dscMap != null) {
			for (Entry<String, ATEquityDescription> entry : dscMap.entrySet()) {
				ProductDescription dsc = ATProtoProductDescriptionWrapper.toProductDescription(entry.getValue());
				builder.putDescriptions(entry.getKey(), dsc);
			}
		}
		SecurityPack protoRule = builder.build();
		return protoRule;
	}

}
