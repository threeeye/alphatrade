package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;

/**
 * General purpose access utility class. Works in tandem with IATAssetConstants
 * 
 * @author miron
 */
public interface IATAsset extends IATAssetConstants {

	// General purpose

	String getAtId();

	Map<String, Object> getAsMap();

	static public String getAtId(Map<String, Object> map_) {
		return getString(map_, IATLookupMap.AT_ID);
	}

	static public void setAtId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATLookupMap.AT_ID, value_);
	}

	static String getOccId(Map<String, Object> map_) {
		String occId = getString(map_, IATAssetConstants.OCCID);
		if (occId == null) {
			occId = IATAsset.getAtId(map_);
		}
		return getString(map_, IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static String getTraceId(Map<String, Object> map_) {
		return getString(map_, IATAssetConstants.TRACE_ID);
	}

	static void setTraceId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.TRACE_ID, value_);
	}

	// Pricing

	static Double getPxMin(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_MIN);
	}

	static void setPxMin(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_MIN, value_);
	}

	static Double getPxMax(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_MAX);
	}

	static void setPxMax(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_MAX, value_);
	}

	static Double getPxLast(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_LAST);
	}

	static void setPxLast(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_LAST, value_);
	}

	static Double getPxAsk(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_ASK);
	}

	static void setPxAsk(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_ASK, value_);
	}

	static Double getPxBid(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_BID);
	}

	static void setPxBid(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_BID, value_);
	}

	static Double getPxHigh(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_HIGH);
	}

	static void setPxHigh(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_HIGH, value_);
	}

	static Double getPxLow(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.PX_LOW);
	}

	static void setPxLow(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_LOW, value_);
	}

	static Double getPxTrade(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE);
		return value;
	}

	static void setPxTrade(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_TRADE, value_);
	}

	static Double getPxTradeTotal(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE_TOTAL);
		return value;
	}

	static void setPxTradeTotal(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_TRADE_TOTAL, value_);
	}

	static Double getPxNet(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_NET);
		return value;
	}

	static void setPxNet(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.PX_NET, value_);
	}

	static public Double getPxOpen(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_OPEN);
	}

	static public void setPxOpen(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_OPEN);
		else
			map_.put(IATAssetConstants.PX_OPEN, value_);
	}

	static public Double getPxClose(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_CLOSE);
	}

	static public void setPxClose(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_CLOSE);
		else
			map_.put(IATAssetConstants.PX_CLOSE, value_);
	}

	static public Double getPxClosePrev(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_CLOSE_PREVIOUS);
	}

	static public void setPxClosePrev(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_CLOSE_PREVIOUS);
		else
			map_.put(IATAssetConstants.PX_CLOSE_PREVIOUS, value_);
	}

	static public Double getPxTheo(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_THEORETICAL);
	}

	static public void setPxTheo(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_THEORETICAL);
		else
			map_.put(IATAssetConstants.PX_THEORETICAL, value_);
	}

	static public Double getPxTime(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_TIME);
	}

	static public void setPxTime(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_TIME);
		else
			map_.put(IATAssetConstants.PX_TIME, value_);
	}

	// Time

	static public Long getTsLast(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_LAST);
	}

	static public void setTsLast(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_LAST);
		else
			map_.put(IATAssetConstants.TS_LAST, value_);
	}

	static Long getTsTrade(Map<String, Object> map_) {
		return getLong(map_, IATAssetConstants.TS_TRADE);
	}

	static void setTsTrade(Map<String, Object> map_, Long value_) {
		setMapValue(map_, IATAssetConstants.TS_TRADE, value_);
	}

	static public Long getTsAsk(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_ASK);
	}

	static public void setTsAsk(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_ASK);
		else
			map_.put(IATAssetConstants.TS_ASK, value_);
	}

	static public Long getTsBid(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_BID);
	}

	static public void setTsBid(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_BID);
		else
			map_.put(IATAssetConstants.TS_BID, value_);
	}

	// Size

	static public Integer getSzLast(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.SIZE_LAST);
	}

	static public void setSzLast(Map<String, Object> map_, Integer value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.SIZE_LAST);
		else
			map_.put(IATAssetConstants.SIZE_LAST, value_);
	}

	static public Integer getSzAsk(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.SIZE_ASK);
	}

	static public void setSzAsk(Map<String, Object> map_, Integer value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.SIZE_ASK);
		else
			map_.put(IATAssetConstants.SIZE_ASK, value_);
	}

	static public Integer getSzBid(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.SIZE_BID);
	}

	static public void setSzBid(Map<String, Object> map_, Integer value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.SIZE_BID);
		else
			map_.put(IATAssetConstants.SIZE_BID, value_);
	}

	static Double getSzTrade(Map<String, Object> map_) {
		return getDouble(map_, IATAssetConstants.SIZE_TRADE);
	}

	static void setSzTrade(Map<String, Object> map_, Double value_) {
		setMapValue(map_, IATAssetConstants.SIZE_TRADE, value_);
	}

	// Categories

	static public String getType(Map<String, Object> map_) {
		return getString(map_, IATAssetConstants.TYPE);
	}

	static public void setType(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.TYPE, value_);
	}

	static public String getName(Map<String, Object> map_) {
		return getString(map_, IATAssetConstants.NAME);
	}

	static public void setName(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.NAME, value_);
	}

	static String getIndustry(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.INDUSTRY);
	}

	static void setIndustry(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.INDUSTRY, value_);
	}

	static String getSector(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.SECTOR);
	}

	static void setSector(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.SECTOR, value_);
	}

	// Options

	static public Double getPxStrike(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_STRIKE);
	}

	static public void setPxStrike(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			value_ = null;
		IATAsset.setMapValue(map_, IATAssetConstants.PX_STRIKE, value_, true);
	}

	static public Double getDelta(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.DELTA);
	}

	static public void setDelta(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.DELTA);
		else
			map_.put(IATAssetConstants.DELTA, value_);
	}

	static public Double getTheta(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.THETA);
	}

	static public void setTheta(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.THETA);
		else
			map_.put(IATAssetConstants.THETA, value_);
	}

	static public Double getRho(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.RHO);
	}

	static public void setRho(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.RHO);
		else
			map_.put(IATAssetConstants.RHO, value_);
	}

	static public Double getGamma(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.GAMMA);
	}

	static public void setGamma(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.GAMMA);
		else
			map_.put(IATAssetConstants.GAMMA, value_);
	}

	static public Double getOptionMultiplier(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.OPTION_MULTIPLIER);
	}

	static public void setOptionMultiplier(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.OPTION_MULTIPLIER);
		else
			map_.put(IATAssetConstants.OPTION_MULTIPLIER, value_);
	}

	static public Double getVega(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.VEGA);
	}

	static public void setVega(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.VEGA);
		else
			map_.put(IATAssetConstants.VEGA, value_);
	}

	static public String getRight(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OPTION_TYPE);
	}

	static public void setRight(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.OPTION_TYPE, value_);
	}

	static public String getExpDS6(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.EXPIRATION_DATE_STR);
	}

	static public void setExpDS6(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.EXPIRATION_DATE_STR, value_);
	}

	// Statistics

	static String getAvgMode(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODE_AVG);
	}

	static void setAvgMode(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MODE_AVG, value_);
	}

	static String getHVMode(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODE_HV);
	}

	static void setHVMode(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MODE_HV, value_);
	}

	static public Double getIV(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.IV);
	}

	static public void setIV(Map<String, Object> map_, Double value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.IV);
		else
			map_.put(IATAssetConstants.IV, value_);
	}

	// Other

	static public String getUserId(Map<String, Object> map_) {
		return getString(map_, IATAssetConstants.USER_ID);
	}

	static public void setUserId(Map<String, Object> map_, String value_) {
		setMapValue(map_, IATAssetConstants.USER_ID, value_);
	}

	// Primitives

	static String getString(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (String) map_.get(key_);
	}

	static Long getLong(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		Object obj = map_.get(key_);
		if (obj instanceof Double) {
			obj = ((Double) obj).longValue();
			map_.put(key_, obj);
		}
		return (Long) obj;
	}

	static Integer getInt(Map<String, Object> map_, String key_) {
		Integer result = getInt(map_, key_, false);
		return result;
		// if (map_ == null)
		// 	return null;
		// Object obj = map_.get(key_);
		// if (obj instanceof Double)
		// 	return ((Double) obj).intValue();
		// if (obj instanceof Long)
		// 	return ((Long) obj).intValue();
		// return (Integer) obj;
	}

	static Integer getInt(Map<String, Object> map_, String key_, boolean updateMap_) {
		if (map_ == null)
			return null;
		Object obj = map_.get(key_);
		if (obj instanceof Integer) {
			return (Integer) obj;
		}
		if (obj instanceof Double) {
			int result = ((Double) obj).intValue();
			if (updateMap_) {
				map_.put(key_, result);
			}
			return result;
		}
		if (obj instanceof Long) {
			int result = ((Long) obj).intValue();
			if (updateMap_) {
				map_.put(key_, result);
			}
			return result;
		}
		return null;
	}

	static Integer getInt(Map<String, Object> map_, String key_, int default_) {
		if (map_ == null)
			return default_;
		return (Integer) map_.get(key_);
	}

	static Double getDouble(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		Object value = map_.get(key_);
		if (value instanceof Double) {
			return (Double) value;
		} else if (value instanceof String) {
			try {
				Double d = Double.parseDouble((String) value);
				return d;
			} catch (Exception ex) {
				return null;
			}
		} else {
			return null;
		}
	}

	static Date getDate(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (Date) map_.get(key_);
	}

	static Boolean getBoolean(Map<String, Object> map_, String key_) {
		if (map_ == null)
			return null;
		return (Boolean) map_.get(key_);
	}

	static void setMapValue(Map<String, Object> map_, String key_, Object value_) {
		setMapValue(map_, key_, value_, false);
	}

	static void setMapValue(Map<String, Object> map_, String key_, Object value_, boolean isRemoveNull_) {
		if (isRemoveNull_ && value_ == null) {
			map_.remove(key_);
		} else {
			map_.put(key_, value_);
		}
	}

	static void setRemovableValue(Map<String, Object> map_, String prop_, Object value_) {
		if (value_ == null) {
			map_.remove(prop_);
		} else {
			map_.put(prop_, value_);
		}
	}

	static void setNullableValue(Map<String, Object> map_, String prop_, Object value_) {
		map_.put(prop_, value_);
	}

}
