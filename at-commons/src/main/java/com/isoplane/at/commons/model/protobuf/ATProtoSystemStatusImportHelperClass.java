package com.isoplane.at.commons.model.protobuf;

import com.isoplane.at.commons.model.protobuf.SystemStatusProtos.SystemStatus;

public class ATProtoSystemStatusImportHelperClass {

	private SystemStatus _protoMsg;

	public ATProtoSystemStatusImportHelperClass(SystemStatus msg_) {
		this._protoMsg = msg_;
	}

	public SystemStatus proto() {
		return this._protoMsg;
	}


}
