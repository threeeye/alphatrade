package com.isoplane.at.commons.model.protobuf;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATBaseNotification.ATNotificationContent;
import com.isoplane.at.commons.model.ATGenericNotification;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.util.ATConfigUtil;;

public class ATProtoPushNotificationWrapper {

	static public final String TYPE_ALERT = "alert";

	static private Map<String, ATBaseNotification> _notificationMap = new HashMap<>();

	private PushNotification _protoNotification;
	private ATGenericNotification _value;

	public ATProtoPushNotificationWrapper(PushNotification proto_) {
		this._protoNotification = proto_;
	}

	public PushNotification proto() {
		return this._protoNotification;
	}

	public ATGenericNotification value() {
		if (_value == null) {
			_value = toNotification(_protoNotification);
		}
		return _value;
	}

	// public ATBaseNotification value() {
	// if (this._protoNotification == null) {
	// this._protoNotification = toNotification(this._protoNotification);
	// }
	// return this._atNotification;
	// }

	// static public EATMarketStatus toNotification(PushNotification status_) {
	// EATMarketStatus status = EATMarketStatus.valueOf(status_.getStatus());
	// return status;
	// }

	static public ATGenericNotification toNotification(PushNotification tmpl_) {
		ATNotificationContent content = new ATNotificationContent(tmpl_.getTitle(), tmpl_.getBody(), tmpl_.getIcon());

		ATGenericNotification pojo = new ATGenericNotification();
		pojo.setActive(tmpl_.getIsActive());
		pojo.setClickAction(tmpl_.getClickAction());
		pojo.setDate(new Date(tmpl_.getTimestamp()));
		pojo.setLabel(tmpl_.getTitle());
		pojo.setNotificationContent(content);
		pojo.setNotificationTag(tmpl_.getTag());
		pojo.setSilent(tmpl_.getIsSilent());
		pojo.setSymbol(tmpl_.getSymbol());
		pojo.setType(tmpl_.getType());
		return pojo;
	}

	static public PushNotification toNotification(ATBaseNotification notification_, Date date_) {
		return toNotification(notification_, date_, false);
	}

	static public PushNotification toNotification(ATBaseNotification notification_, Date date_, boolean isTest_) {
		PushNotification.Builder builder = ATProtoPushNotificationWrapper.toBuilder(notification_, date_, isTest_);
		if (builder == null)
			return null;
		PushNotification notification = builder.build();
		return notification;
	}

	static public PushNotification.Builder toBuilder(
			ATBaseNotification notification_, Date date_, boolean isTest_) {
		long timeout = ATConfigUtil.isConfigured() ? ATConfigUtil.config().getLong("notification.timeout", 60000) : 60000L;
		ATNotificationContent content = notification_.getNotificationContent(_notificationMap, timeout);
		if (content == null)
			return null;

		String mode = IATUser.MESSAGE_ALL;
		if (isTest_ || notification_.isActive() || ATProtoPushNotificationWrapper.TYPE_ALERT.equals(notification_.getType())) {
			mode = IATUser.MESSAGE_MIN;
		}

		PushNotification.Builder builder = PushNotification.newBuilder();
		builder.setBody(content.getBody());
		builder.setIcon(content.getIcon());
		builder.setSymbol(notification_.getSymbol());
		builder.setTitle(content.getTitle());
		builder.setTimestamp(date_.getTime());
		builder.setType(notification_.getType());
		builder.setMode(mode);
		if (isTest_) {
			builder.setIsTest(isTest_);
		}
		String clickAction = notification_.getClickAction();
		if (clickAction != null) {
			builder.setClickAction(clickAction);
		}
		builder.setIsActive(notification_.isActive());
		builder.setIsSilent(notification_.isSilent());
		builder.setTag(notification_.getNotificationTag());
		builder.addAllUserIds(notification_.getUserIds());
		Object data = notification_.getMeta(ATBaseNotification.META_DATA);
		if (data != null) {
			builder.setMeta(data.toString());
		}
		return builder;
	}

}
