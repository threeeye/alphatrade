package com.isoplane.at.commons.model;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.isoplane.at.commons.ATException;

public class ATOptionSecurity extends ATSecurity {

	// occId: symbol + yymmdd + right + (strike * 1000 -> pad to 8) -> symbol + 15 chars

	private static final long serialVersionUID = 1L;

	static public final String Call = "C";
	static public final String Put = "P";
	static public final String Monthly = "M";
	static public final String Weekly = "W";
	static public final String INCLUDE_EARNINGS = "E";

	private Integer openInterest;
	private Double strike;
	private Date expirationDate;
	private String expirationType;
	private String optionRight;
	private Integer contractSize;
	private String stockOccId;
	private Double priceTheoretical;
	
	// transient private ATStock stock;
	@Expose(serialize = true, deserialize = false)
	private Double priceUnderlying;// = stock != null ? stock.getPrice() : null;

	public ATOptionSecurity() {
		super.setSecType(OPTION_TYPE);
	}

	@Override
	public ATOptionSecurity clone() {
		ATOptionSecurity clone = new ATOptionSecurity();
		clone.populateFrom(this);
		return clone;
	}

	@Override
	public void populateFrom(ATSecurity other_) {
		if (other_ == null || !(other_ instanceof ATOptionSecurity))
			return;
		super.populateFrom(other_);
		ATOptionSecurity other = (ATOptionSecurity) other_;
		if (isChangedNotNull(this.priceUnderlying, other.getPriceUnderlying())) {
			setChanged(true);
			this.priceUnderlying = other.getPriceUnderlying();
		}
		if (isChangedNotNull(this.openInterest, other.getOpenInterest())) {
			setChanged(true);
			this.openInterest = other.getOpenInterest();
		}
		if (isChangedNotNull(this.priceTheoretical, other.getPriceTheoretical())) {
			setChanged(true);
			this.priceTheoretical = other.getPriceTheoretical();
		}
		if (isChangedNotNull(this.strike, other.getStrike())) {
			// TODO: Is this necessary?
			setChanged(true);
			this.strike = other.getStrike();
		}
		if (isChangedNotNull(this.expirationDate, other.getExpirationDate())) {
			// TODO: Is this necessary?
			setChanged(true);
			this.expirationDate = other.getExpirationDate();
		}
		if (isChangedNotNull(this.expirationType, other.expirationType)) {
			// TODO: Is this necessary?
			setChanged(true);
			this.expirationType = other.expirationType;
		}
		if (isChangedNotNull(this.optionRight, other.optionRight)) {
			// TODO: Is this necessary?
			setChanged(true);
			this.optionRight = other.optionRight;
		}
		if (isChangedNotNull(this.contractSize, other.contractSize)) {
			// TODO: Is this necessary?
			setChanged(true);
			this.contractSize = other.contractSize;
		}
	}

	@Override
	public boolean update(IATQuote quote_) {
		if (!super.updateBase(quote_))
			return false;
		if (!quote_.isOption()) {
			throw new ATException(String.format("Invalid quote [%s]", quote_.getOccId()));
		}
		boolean isChanged = false;
		if (isChangedNotNull(this.getStrike(), quote_.getOptionStrike())) {
			// TODO: Check if not set already
			setStrike(quote_.getOptionStrike());
			isChanged = true;
		}
		if (isChangedNotNull(this.getContractSize(), quote_.getOptionContractSize())) {
			setContractSize(quote_.getOptionContractSize());
			isChanged = true;
		}
		if (isChangedNotNull(this.getExpirationType(), quote_.getOptionExpirationType())) {
			setExpirationType(quote_.getOptionExpirationType());
			isChanged = true;
		}
		if (isChangedNotNull(this.getOptionRight(), quote_.getOptionRight())) {
			setOptionRight(quote_.getOptionRight());
			isChanged = true;
		}
		// expirationDate: Done previously
		if (isChanged) {
			setChanged(true);
		}
		return true;
	}

	@Override
	protected String getSortId() {
		return this.getSymbol() + this.getOptionRight() + this.getOccId();
	}

	public Integer getOpenInterest() {
		return openInterest;
	}

	public void setOpenInterest(Integer openInterest) {
		this.openInterest = openInterest;
	}

	public Double getStrike() {
		return strike;
	}

	public void setStrike(Double strike) {
		this.strike = strike;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getOptionRight() {
		return optionRight;
	}

	public void setOptionRight(String optionRight) {
		this.optionRight = optionRight;
	}

	public Integer getContractSize() {
		return contractSize;
	}

	public void setContractSize(Integer contractSize) {
		this.contractSize = contractSize;
	}

	public String getExpirationType() {
		return expirationType;
	}

	public void setExpirationType(String expirationType) {
		this.expirationType = expirationType;
	}

	public String getStockOccId() {
		return stockOccId;
	}

	public void setStockOccId(String stockOccId) {
		this.stockOccId = stockOccId;
	}

	public Double getPriceUnderlying() {
		return priceUnderlying;
	}

	public void setPriceUnderlying(Double priceUnderlying) {
		this.priceUnderlying = priceUnderlying;
	}

	public Double getPriceTheoretical() {
		return priceTheoretical;
	}

	public void setPriceTheoretical(Double priceTheoretical) {
		this.priceTheoretical = priceTheoretical;
	}

}
