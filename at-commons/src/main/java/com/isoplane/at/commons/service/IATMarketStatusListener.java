package com.isoplane.at.commons.service;

import java.util.Map;

public interface IATMarketStatusListener {

	void notify(Map<String,Object> status);
}
