package com.isoplane.at.commons.model;

import java.util.Date;

public interface IATQuote {

	String getOccId();
	String getSymbol();
	String getDescription();

	Double getPriceAsk();
	Double getPriceBid();
	
	Double getPrice();
	Double getPriceHigh();
	Double getPriceLow();
	Double getPriceLast();
	
	Double getPriceOpen();
	Double getPriceClose();

	Integer getVolume();
	Integer getSizeAsk();
	Integer getSizeBid();
	
	Date getDateAsk();
	Date getDateBid();
	Date getDateLast();
	
	boolean isOption();
	Double getOptionStrike();
	String getOptionExpirationType();
	Integer getOptionContractSize();
	String getOptionRight();

}
