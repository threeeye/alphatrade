package com.isoplane.at.commons.store;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;

public class ATPersistentAlertRule extends ATPersistentLookupBase {

	private static final long serialVersionUID = 1L;

	public void setUserId(String value_) {
		super.put(IATAssetConstants.USER_ID, value_);
	}

	public String getUserId() {
		String value = IATAsset.getString(this, IATAssetConstants.USER_ID);
		return value;
	}

	public void setTargetId(String value_) {
		super.put(IATAssetConstants.TARGET_ID, value_);
	}

	public String getTargetId() {
		String value = IATAsset.getString(this, IATAssetConstants.TARGET_ID);
		return value;
	}

	public void setData(String value_) {
		super.put(IATAssetConstants.DATA, value_);
	}

	public String getData() {
		String value = IATAsset.getString(this, IATAssetConstants.DATA);
		return value;
	}

}
