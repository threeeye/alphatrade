package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;

public interface IATProtoMapMessageSubscriber {

	void notifyMapMessage(MapMessage msg);

}
