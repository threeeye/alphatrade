package com.isoplane.at.commons.model;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

//http://csbruce.com/software/utf-8.html
public abstract class ATBaseNotification {

	static public final String META_DATA = "data";

	private String clickAction;
	private Date date;
	private boolean isActive;
	private boolean isSilent;
	private String label;
	private String symbol;
	private Map<String, Object> metaMap = new HashMap<>(); // Note: Used to add unique details for key
	private Set<String> userIds = new HashSet<>();

	public transient long _timestamp;

	abstract public String getNotificationTag();

	abstract public String getNotificationKey();

	abstract public String getType();

	// Note: Update notification timestamp
	abstract public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap, long timeout);

	public String getClickAction() {
		return clickAction;
	}

	public void setClickAction(String clickAction) {
		this.clickAction = clickAction;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public boolean isSilent() {
		return isSilent;
	}

	public void setSilent(boolean isSilent) {
		this.isSilent = isSilent;
	}
	
//	abstract public void setSymbol(String value_);

	public void setSymbol(String symbol) {
		this.symbol = symbol;
		this.setMeta("symbol", symbol);
	}

	public String getSymbol() {
		return this.symbol;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Object getMeta(String key_) {
		return metaMap.get(key_);
	}

	public void setMeta(String key_, Object meta_) {
		this.metaMap.put(key_, meta_);
	}

	public Set<String> getUserIds() {
		return userIds;
	}

	public void addUserId(String userId) {
		this.userIds.add(userId);
	}

	public void addUserIds(Collection<String> userIds_) {
		this.userIds.addAll(userIds_);
	}

	public static class ATNotificationContent {
		private String title;
		private String body;
		private String icon;

		public ATNotificationContent(String title_, String body_, String icon_) {
			setTitle(title_);
			setBody(body_);
			setIcon(icon_);
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public String getBody() {
			return body;
		}

		public void setBody(String body) {
			this.body = body;
		}

		public String getIcon() {
			return icon;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}
	}

}
