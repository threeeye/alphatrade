package com.isoplane.at.commons.service;

import java.util.Map;

public interface IATMarketStatusService extends IATService {

//	void register(IATMarketStatusListener listener);

	Map<String, Object> getStatus();

	boolean update(Map<String, Object> status);
}
