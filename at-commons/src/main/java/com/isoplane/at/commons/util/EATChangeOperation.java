package com.isoplane.at.commons.util;

public enum EATChangeOperation {

	ADD, CREATE, DELETE, READ, UPDATE, SWAP;
}
