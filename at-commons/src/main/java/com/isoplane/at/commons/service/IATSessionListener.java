package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.util.EATChangeOperation;

public interface IATSessionListener {

	void notify(EATChangeOperation operation, ATSession session);

}
