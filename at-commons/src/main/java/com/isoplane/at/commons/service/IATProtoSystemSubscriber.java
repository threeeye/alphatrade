package com.isoplane.at.commons.service;

import java.util.Map;

public interface IATProtoSystemSubscriber {

	void notifySystemStatus(String source, Map<String,String> status);

}
