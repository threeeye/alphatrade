package com.isoplane.at.commons.model;

import java.util.List;
import java.util.Map;

import com.isoplane.at.commons.model.ATOptionExpiration.ATOptionExpirationDate;

public interface IATOptionExpiration {

	static final String STRIKES = "strikes";

	String getOccId();

	List<ATOptionExpirationDate> getExpirations();

	Map<String, Object> getAsMap();

	static int compareTo(IATOptionExpiration this_, IATOptionExpiration other_) {
		if (other_ == null) {
			return this_ == null ? 0 : -1;
		}
		if (this_ == null) {
			return 1;
		}
		return this_.getOccId().compareTo(other_.getOccId());
	}

	static List<ATOptionExpirationDate> getExpirations(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		List<ATOptionExpirationDate> o = (List<ATOptionExpirationDate>) map_.get(IATAssetConstants.EXPIRATION_DATE_STR);
		return o;
	}

	static void setExpirations(Map<String, Object> map_, List<ATOptionExpirationDate> value_) {
		map_.put(IATAssetConstants.EXPIRATION_DATE_STR, value_);
	}

	static String getExpDS6(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.EXPIRATION_DATE_STR);
	}

	static List<Double> getStrikes(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		List<Double> strikes = (List<Double>) map_.get(STRIKES);
		return strikes;
	}

}
