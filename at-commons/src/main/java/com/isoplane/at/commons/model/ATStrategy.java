package com.isoplane.at.commons.model;

public class ATStrategy {

	public static String TYPE_VBuPS = "VBuPS";
	public static String TYPE_VBrCS = "VBrCS";
	public static String TYPE_PUT = "PUT";
	public static String TYPE_CALL = "CALL";

	public Double apy;
	public Long daysToExp;
	public String earningsDS8;
	public Double margin;
	public Double owned;
	public Double px;
	public Double risk;
	public String symbol;
	public String type;
	public Double width;

	@Override
	public ATStrategy clone() {
		ATStrategy strat = new ATStrategy();
		strat.apy = this.apy;
		strat.daysToExp = this.daysToExp;
		strat.earningsDS8 = this.earningsDS8;
		strat.margin = this.margin;
		strat.owned = this.owned;
		strat.px = this.px;
		strat.risk = this.risk;
		strat.symbol = this.symbol;
		strat.type = this.type;
		strat.width = this.width;
		return strat;
	}

	public static class ATStrategyGroup {

		public String symbol;
		public ATStrategy[] strategies;

	}
}
