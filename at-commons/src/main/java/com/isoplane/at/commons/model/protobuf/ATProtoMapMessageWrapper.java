package com.isoplane.at.commons.model.protobuf;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;;

public class ATProtoMapMessageWrapper {

	static public final String TYPE_MAP_MSG = "map_msg";

	static final String MAP_KEY = "__MAP__";

	private MapMessage _protoMapMessage;
	private Map<String, Object> _value;

	public ATProtoMapMessageWrapper(MapMessage proto_) {
		this._protoMapMessage = proto_;
	}

	public MapMessage proto() {
		return this._protoMapMessage;
	}

	public Map<String, Object> value() {
		if (_value == null) {
			_value = toMap(_protoMapMessage, null);
		}
		return _value;
	}

	static public Map<String, Object> toMap(MapMessage tmpl_, Map<String, Object> target_) {
		Map<String, Object> target = target_ != null ? target_ : new HashMap<>();
		Map<String, Double> doubleMap = tmpl_.getDoubleMapMap();
		if (doubleMap != null && !doubleMap.isEmpty()) {
			for (Entry<String, Double> entry : doubleMap.entrySet()) {
				populateMap(entry.getKey(), entry.getValue(), target);
			}
		}
		Map<String, Integer> intMap = tmpl_.getIntMapMap();
		if (intMap != null && !intMap.isEmpty()) {
			for (Entry<String, Integer> entry : intMap.entrySet()) {
				populateMap(entry.getKey(), entry.getValue(), target);
			}
		}
		Map<String, Long> longMap = tmpl_.getLongMapMap();
		if (longMap != null && !longMap.isEmpty()) {
			for (Entry<String, Long> entry : longMap.entrySet()) {
				populateMap(entry.getKey(), entry.getValue(), target);
			}
		}
		Map<String, String> stringMap = tmpl_.getStringMapMap();
		if (stringMap != null && !stringMap.isEmpty()) {
			for (Entry<String, String> entry : stringMap.entrySet()) {
				populateMap(entry.getKey(), entry.getValue(), target);
			}
		}
		return target;
	}

	static private void populateMap(String key_, Object value_, Map<String, Object> target_) {
		if (key_.startsWith(MAP_KEY)) {
			int idx = key_.indexOf(MAP_KEY, MAP_KEY.length());
			String mapId = key_.substring(MAP_KEY.length(), idx);
			key_ = key_.substring(idx + MAP_KEY.length());
			@SuppressWarnings("unchecked")
			Map<String, Object> subMap = (Map<String, Object>) target_.get(mapId);
			if (subMap == null) {
				subMap = new HashMap<String, Object>();
				target_.put(mapId, subMap);
			}
			subMap.put(key_, value_);
		} else {
			target_.put(key_, value_);
		}
	}

	static public MapMessage toMapMessage(String type_, String id_, String source_, String target_, long ts_, Map<String, Object> map_) {
		if (map_ == null)
			return null;
		MapMessage.Builder builder = MapMessage.newBuilder();
		builder.setType(type_);
		builder.setId(id_);
		builder.setSource(source_);
		builder.setTarget(target_);
		builder.setTimestamp(ts_);
		if (!map_.isEmpty()) {
			Map<String, Double> doubleMap = new HashMap<>();
			Map<String, Integer> intMap = new HashMap<>();
			Map<String, Long> longMap = new HashMap<>();
			Map<String, String> stringMap = new HashMap<>();
			for (Entry<String, Object> entry : map_.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				populateMapMessage(key, value, doubleMap, intMap, longMap, stringMap);
			}
			if (!doubleMap.isEmpty()) {
				builder.putAllDoubleMap(doubleMap);
			}
			if (!intMap.isEmpty()) {
				builder.putAllIntMap(intMap);
			}
			if (!longMap.isEmpty()) {
				builder.putAllLongMap(longMap);
			}
			if (!stringMap.isEmpty()) {
				builder.putAllStringMap(stringMap);
			}
		}
		MapMessage protoMap = builder.build();
		return protoMap;
	}

	static private void populateMapMessage(
			String key_,
			Object value_,
			Map<String, Double> doubleMap_,
			Map<String, Integer> intMap_,
			Map<String, Long> longMap_,
			Map<String, String> stringMap_) {
		if (value_ instanceof Double) {
			doubleMap_.put(key_, (Double) value_);
		} else if (value_ instanceof Integer) {
			intMap_.put(key_, (Integer) value_);
		} else if (value_ instanceof Long) {
			longMap_.put(key_, (Long) value_);
		} else if (value_ instanceof String) {
			stringMap_.put(key_, (String) value_);
		} else if (value_ instanceof Map && !((Map<?, ?>) value_).isEmpty()) {
			@SuppressWarnings("unchecked")
			Map<String, Object> map = ((Map<String, Object>) value_);
			for (Entry<String, Object> entry : map.entrySet()) {
				String mapKey = String.format("%s%s%s%s", MAP_KEY, key_, MAP_KEY, entry.getKey());
				Object mapValue = entry.getValue();
				populateMapMessage(mapKey, mapValue, doubleMap_, intMap_, longMap_, stringMap_);
			}
		} else {
			// NOTE: log?
		}

	}

}
