package com.isoplane.at.commons.util;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.IATOptionDescription.Right;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATMathUtil {

	static final Logger log = LoggerFactory.getLogger(ATMathUtil.class);

	static public long DAY_IN_MS = 1000 * 60 * 60 * 24;

	static public Double round(Double d_, int precission_) {
		if (d_ == null || d_.isNaN() || d_.isInfinite())
			return d_;
		BigDecimal bd = new BigDecimal(d_);
		bd = bd.setScale(4, BigDecimal.ROUND_HALF_DOWN);
		double d = bd.doubleValue();
		return d;
	}

	static public Double calcApy(Double basis_, Double net_, Long days_) {
		if (basis_ == null || basis_ == 0 || net_ == null || days_ == null || days_ == 0)
			return null;
		double yield = (basis_ - net_) / basis_;
		double apy = yield * 365.0 / days_;
		return apy;
	}

	static public Double calculateProbability(String occId_, Double pxUl_, Double hv_, ATOptionDescription option_, Instant now_) {
		if (pxUl_ == null) {
			log.debug(String.format("Missing underlying Px [%-5s]", occId_));
			return null;
		}
		if (hv_ == null) {
			log.debug(String.format("Missing HV [%-5s]", occId_));
			return null;
		}
		double zFactor = 1.0; // TODO: Consider adjusting for earnings etc
		long days = ChronoUnit.DAYS.between(now_, option_.getExpiration().toInstant()) + 1;
		double stdev = pxUl_ * hv_ * Math.sqrt(days / 365.0);
		double z = Math.abs(pxUl_ - option_.getStrike()) / (stdev * zFactor);
		int pDir = Right.Put == option_.getRight() ? 1 : -1;
		int pItm = option_.getStrike() < pxUl_ ? 1 : -1;
		double p = (pItm * pDir) > 0 ? ATStandardNormalTable.getProbabilityGreaterThan(z)
				: ATStandardNormalTable.getProbabilityLessThan(z);
		return p;
	}

}
