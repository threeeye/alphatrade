package com.isoplane.at.commons.model.alertcriteria;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentSecurity;

public class ATPriceCriteria extends ATBaseCriteria {

	private double _number;
	private int _condition;
	private String _note;

	// @Deprecated
	// static public ATPriceCriteria create(String userId_, TdnAlert alert_) {
	// if (StringUtils.isBlank(userId_) || alert_ == null || StringUtils.isBlank(alert_.occId) || StringUtils.isBlank(alert_.opr)
	// || StringUtils.isBlank(alert_.val))
	// return null;
	// int condition = -1;
	// switch (alert_.opr.toUpperCase()) {
	// case "EQ":
	// condition = EQ;
	// break;
	// case "GT":
	// condition = GT;
	// break;
	// case "GTE":
	// condition = GTE;
	// break;
	// case "LT":
	// condition = LT;
	// break;
	// case "LTE":
	// condition = LTE;
	// break;
	// default:
	// return null;
	// }
	// String criteria = getOperatorString(condition);
	// Double value = NumberUtils.createDouble(alert_.val);
	// if (value == null || criteria == null)
	// return null;
	// criteria = String.format("%s %.2f", criteria, value);
	// ATPriceCriteria crit = new ATPriceCriteria(userId_, alert_.occId, criteria, condition, value, alert_.msg);
	// return crit;
	// }

	static public ATPriceCriteria create(String userId_, ATAlertRule alert_) {
		if (StringUtils.isBlank(userId_) || alert_ == null || StringUtils.isBlank(alert_.getOccId()) || StringUtils.isBlank(alert_.getOperator())
				|| StringUtils.isBlank(alert_.getValue()))
			return null;
		Integer condition = getCondition(alert_);
		if (condition == null)
			return null;
		String criteria = getOperatorString(condition);
		Double value = NumberUtils.createDouble(alert_.getValue());
		if (value == null || criteria == null)
			return null;
		criteria = String.format("%s %.2f", criteria, value);
		ATPriceCriteria crit = new ATPriceCriteria(userId_, alert_.getOccId(), criteria, condition, value, alert_.getMessage());
		return crit;
	}

//	static public ATPriceCriteria create(String occId_, String str_, String note_) {
//		int condition = -1;
//		String numStr = null;
//		String criteria = "";
//		if (str_.startsWith("<=")) {
//			numStr = str_.substring(2);
//			condition = LTE;
//			criteria = "≤";
//		} else if (str_.startsWith(">=")) {
//			numStr = str_.substring(2);
//			condition = GTE;
//			criteria = "≥";
//		} else if (str_.startsWith("<")) {
//			numStr = str_.substring(1);
//			condition = LT;
//			criteria = "<";
//		} else if (str_.startsWith(">")) {
//			numStr = str_.substring(1);
//			condition = GT;
//			criteria = ">";
//		}
//		Double num = NumberUtils.createDouble(numStr);
//		if (num == null || condition < 0 || StringUtils.isBlank(occId_))
//			return null;
//		criteria = String.format("%s%6.2f", criteria, num);
//		ATPriceCriteria crit = new ATPriceCriteria(null, occId_, criteria, condition, num, note_);
//		return crit;
//	}

	private ATPriceCriteria(String userId_, String occId_, String criteria_, int condition_, double number_, String note_) {
		super(userId_, occId_, criteria_);
		_condition = condition_;
		_number = number_;
		_note = note_;
	}

	@Override
	@Deprecated
	public Boolean test(ATSecurity sec_) {
		if (sec_ == null)
			return false;
		return test(sec_.getOccId(), _condition, _number, sec_.getPrice());
	}

	@Override
	@Deprecated
	public Boolean test(ATPersistentSecurity sec_) {
		if (sec_ == null)
			return false;
		return test(sec_.getOccId(), _condition, _number, sec_.getPrice());
	}

	@Override
	public Boolean test(IATMarketData data_, IATStatistics stats_) {
		if (data_ == null)
			return false;
		return test(data_.getAtId(), _condition, _number, data_.getPxLast());
	}

	@Override
	public String label() {
		return StringUtils.isBlank(_note) ? super.label() : String.format("'%s'", _note);
	}

	@Override
	@Deprecated
	public String label(ATPersistentSecurity sec_) {
		try {
			double pct = 100 * (sec_.getPrice() - _number) / _number;
			String sign = pct <= 0 ? "" : "+";
			String label = String.format("%s (%s%.2f%%)", label(), sign, pct);
			return label;
		} catch (Exception ex) {
			String label = label();
			log.error(String.format("Error labeling [%s/%s]", this.getClass().getSimpleName(), label), ex);
			return label;
		}
	}

	@Override
	public String label(IATMarketData data_, IATStatistics stats) {
		Double price = data_ != null ? data_.getPxLast() : null;
		if (price == null)
			return null;
		try {
			double pct = 100 * (price - _number) / _number;
			String sign = pct <= 0 ? "" : "+";
			String label = String.format("%s (%s%.2f%%)", label(), sign, pct);
			return label;
		} catch (Exception ex) {
			String label = label();
			log.error(String.format("Error labeling [%s/%s]", this.getClass().getSimpleName(), label), ex);
			return label;
		}
	}

	private Boolean test(String occId_, int condition_, Double ref_, Double value_) {
		if (value_ == null || !isOccIdMatch(occId_))
			return false;
		return testNumber(condition_, ref_, value_);
	}

}
