package com.isoplane.at.commons.model;

import java.util.Map;

public interface IATFundamental extends IATAsset {

	Double getDividend();

	String getDividendExDS8();

	String getDividendEstDS8();

	Integer getDividendFrequency();

	String getEarningsDS8();

	Double getIV();

	String getLatestDS8();

	Double getMarketCap();

	String getModeAvg();

	String getModeHV();

	Double getPEGrowthRatio();

	Double getPxAvg();

	Double getPxHV();

	Map<String, Double> getAvgMap();

	Map<String, Double> getHvMap();

	Map<String, Double> getMaxMap();

	Map<String, Double> getMinMap();

	Double getRevenue1Y();

	Double getRevenue1Q();

	Double getRevenueGrowth1Q();

	Double getRevenueGrowth1Y();

	Double getRevenueGrowthAvg3Y();

	Double getRevenueGrowthYoY();

	Double getRevenueMultiple();

	Double getTrailingPE();

	static public String getLatestDS8(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LATEST_DATE_STR);
	}

	static public void setLatestDS8(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.LATEST_DATE_STR, value_);
	}

	static public Double getRevenue1Y(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_ANNUAL);
	}

	static public void setRevenue1Y(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_ANNUAL, value_);
	}

	static public Double getRevenue1Q(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_QUARTERLY);
	}

	static public void setRevenue1Q(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_QUARTERLY, value_);
	}

	static public Double getPEGrowthRatio(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PE_GROWTH_RATIO);
	}

	static public void setPEGrowthRatio(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.PE_GROWTH_RATIO, value_);
	}

	static public Double getRevenueMultiple(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_MLT);
	}

	static public void setRevenueMultiple(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_MLT, value_);
	}

	static public Double getTrailingPE(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.TRAILING_PE);
	}

	static public void setTrailingPE(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TRAILING_PE, value_);
	}

	static public Double getRevenueGrowth1Q(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_GROWTH_1Q);
	}

	static public void setRevenueGrowth1Q(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_GROWTH_1Q, value_);
	}

	static public Double getRevenueGrowth1Y(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_GROWTH_1Y);
	}

	static public void setRevenueGrowth1Y(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_GROWTH_1Y, value_);
	}

	static public Double getRevenueGrowthAvg3Y(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_GROWTH_AVG_3Y);
	}

	static public void setRevenueGrowthAvg3Y(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_GROWTH_AVG_3Y, value_);
	}

	static public Double getRevenueGrowthYoY(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.REV_GROWTH_YoY);
	}

	static public void setRevenueGrowthYoY(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.REV_GROWTH_YoY, value_);
	}

	static public Double getIV(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.IV);
	}

	static public void setIV(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.IV, value_);
	}

	static public Double getMarketCap(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.MARKET_CAP);
	}

	static public void setMarketCap(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MARKET_CAP, value_);
	}

	static public String getEarningsDS8(Map<String, Object> map_) {
		String value = IATAsset.getString(map_, IATAssetConstants.EARNINGS_DATE_STR);
		if (value == null)
			value = IATAsset.getString(map_, IATAssetConstants.EARNINGS_DATE_STR_OLD);
		return value;
	}

	static public void setEarningsDS8(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.EARNINGS_DATE_STR, value_);
	}

	static public String getDividendExDS8(Map<String, Object> map_) {
		String value = IATAsset.getString(map_, IATAssetConstants.DIV_EX_DATE_STR);
		if (value == null)
			value = IATAsset.getString(map_, IATAssetConstants.DIV_EX_DATE_STR_OLD);
		return value;
	}

	static public void setDividendExDS8(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_EX_DATE_STR, value_);
	}

	static public String getDividendEstDS8(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.DIV_EST_DATE_STR);
	}

	static public void setDividendEstDS8(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_EST_DATE_STR, value_);
	}

	static public Double getDividend(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.DIVIDEND);
	}

	static public void setDividend(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIVIDEND, value_);
	}

	static public Integer getDividendFrequency(Map<String, Object> map_) {
		return IATAsset.getInt(map_, IATAssetConstants.DIV_FREQUENCY);
	}

	static public void setDividendFrequency(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.DIV_EX_DATE_STR, value_);
	}

	static public Double getPxHV(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_HV);
	}

	static public void setPxHV(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.PX_HV, value_);
	}

	static public String getModeHV(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODE_HV);
	}

	static public void setModeHV(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MODE_HV, value_);
	}

	static public Double getPxAvg(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_AVG);
	}

	static public void setPxAvg(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.PX_AVG, value_);
	}

	static public String getModeAvg(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.MODE_AVG);
	}

	static public void setModeAvg(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MODE_AVG, value_);
	}

	static public Map<String, Double> getAvgMap(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) map_.get(IATAssetConstants.AVG_MAP);
		return map;
	}

	static public void setAvgMap(Map<String, Object> map_, Map<String, Double> value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.AVG_MAP, value_);
	}

	static public Map<String, Double> getHvMap(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) map_.get(IATAssetConstants.HV_MAP);
		return map;
	}

	static public void setHvMap(Map<String, Object> map_, Map<String, Double> value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.HV_MAP, value_);
	}

	static public Map<String, Double> getMaxMap(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) map_.get(IATAssetConstants.MAX_MAP);
		return map;
	}

	static public void setMaxMap(Map<String, Object> map_, Map<String, Double> value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MAX_MAP, value_);
	}

	static public Map<String, Double> getMinMap(Map<String, Object> map_) {
		@SuppressWarnings("unchecked")
		Map<String, Double> map = (Map<String, Double>) map_.get(IATAssetConstants.MIN_MAP);
		return map;
	}

	static public void setMinMap(Map<String, Object> map_, Map<String, Double> value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.MIN_MAP, value_);
	}

}
