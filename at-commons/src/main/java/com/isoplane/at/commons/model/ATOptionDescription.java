package com.isoplane.at.commons.model;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATFormats;

public class ATOptionDescription implements IATOptionDescription {

	private String occId;
	private String symbol;
	private String expStr;
	private Date expiration;
	private Double strike;
	private Double multiplier = 100.0;
	private Right right;

	public static ATOptionDescription create(String occId_) {
		if (occId_ == null || occId_.length() <= 6)
			return null;
		ATOptionDescription od = new ATOptionDescription(occId_);
		return od;
	}

	public ATOptionDescription(String symbol_, Date expiration_, Double strike_, String rightStr_) {
		this(symbol_, expiration_, strike_, Right.string2Right(rightStr_));
	}

	public ATOptionDescription(String symbol_, Date expiration_, Double strike_, Right right_) {
		if (StringUtils.isBlank(symbol_) || (expiration_ == null) || (strike_ == null)) {
			throw new ATException(String.format("Invalid option description [%s/%s/%f]", symbol_, expiration_, strike_));
		}
		symbol = symbol_;
		expiration = expiration_;
		strike = strike_;
		right = right_ != null ? right_ : Right.UNSET;
		generateOccId();
	}

	private ATOptionDescription(String occId_) {
		symbol = occId_.substring(0, 6).trim();
		expStr = occId_.substring(6, 12);
		right = Right.string2Right(occId_.substring(12, 13));
		String strikeStr = occId_.substring(13);
		// expiration = ATFormats.DATE_yyMMdd.get().parse(expStr);
		Long strikeL = Long.parseLong(strikeStr);
		strike = strikeL / 1000.0;
	}

	@Override
	public String getOccId() {
		if (occId == null)
			generateOccId();
		return occId;
	}

	private void generateOccId() {
		String right = Right.right2String(getRight());
		String occIdStr = ATFormats.toOccId(getSymbol(), getExpiration(), right != null ? right : "_", getStrike());
		occId = occIdStr;
	}

	@Override
	public String getSymbol() {
		return symbol;
	}

	@Override
	public String getExpirationDS6() {
		if (expStr == null && expiration != null) {
			expStr = ATFormats.DATE_yyMMdd.get().format(expiration);
		}
		return expStr;
	}

	@Override
	public Date getExpiration() {
		if (expiration == null && expStr != null) {
			try {
				expiration = ATFormats.DATE_yyMMdd.get().parse(expStr);
			} catch (ParseException e) {
				return null;
			}
		}
		return expiration;
	}

	@Override
	public Double getStrike() {
		return strike;
	}

	@Override
	public Double getMultiplier() {
		return multiplier;
	}

	// public void setMultiplier(Double value_) {
	// multiplier = value_;
	// }

	@Override
	public Right getRight() {
		return right;
	}

	// public void setRight(Right value_) {
	// this.right = value_;
	// }

	@Override
	public int compareTo(IATOptionDescription other_) {
		if (other_ == null)
			return -1;
		int result = this.getSymbol().compareTo(other_.getSymbol());
		if (result != 0)
			return result;
		result = this.getExpiration().compareTo(other_.getExpiration());
		if (result != 0)
			return result;
		return this.getStrike().compareTo(other_.getStrike());
	}

	@Override
	public String toString() {
		return getOccId();
	}

}
