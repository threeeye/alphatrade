package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATTradeTraceGroup extends HashMap<String, Object> {

    static public ATTradeTraceGroup fromJson(String userId_, Gson gson_, String json_) {
        JsonObject json = gson_.fromJson(json_, JsonObject.class);
        var groupId = json.get(IATLookupMap.AT_ID).getAsLong();
        var jSymbolArray = json.get("symbols").getAsJsonArray();
        if (jSymbolArray == null || jSymbolArray.isJsonNull() || jSymbolArray.isEmpty())
            return null;
        var symbols = new ArrayList<String>();
        jSymbolArray.forEach(s_ -> symbols.add(s_.getAsString()));
        var jTradesArray = json.get("trades").getAsJsonArray();
        var trades = new TradeTrace[jTradesArray.size()];
        for (var i = 0; i < trades.length; i++) {
            var jTrade = jTradesArray.get(i).getAsJsonObject();
            var tradeId = jTrade.get(IATLookupMap.AT_ID).getAsString();
            var jTradeSize = jTrade.get(IATAssetConstants.SIZE_TRADE);
            Double tradeSize = jTradeSize != null && jTradeSize.isJsonPrimitive() ? jTradeSize.getAsDouble() : null;
            var jTradePxTotal = jTrade.get(IATAssetConstants.PX_TRADE_TOTAL);
            Double tradePxTotal = jTradePxTotal != null && jTradePxTotal.isJsonPrimitive() ? jTradePxTotal.getAsDouble()
                    : null;
            var trade = new TradeTrace();
            trade.setId(tradeId);
            if (tradeSize != null) {
                trade.setCount(tradeSize);
            }
            if (tradePxTotal != null) {
                trade.setPxTradeTotal(tradePxTotal);
            }
            if (jTrade.has(IATAssetConstants.ACTIVE)) {
                trade.setActive(true);
            }
            trades[i] = trade;
        }
        var group = new ATTradeTraceGroup();
        group.setUserId(userId_);
        group.setId(groupId);
        group.setSymbols(symbols);
        group.setItems(trades);
        return group;
    }

    public void setId(Long data_) {
        this.put(IATLookupMap.AT_ID, data_);
    }

    public Long getId() {
        var data = (Long) this.get(IATLookupMap.AT_ID);
        return data;
    }

    public void setUserId(String data_) {
        this.put(IATTrade.TRADE_USER, data_);
    }

    public String getUserId() {
        var data = (String) this.get(IATTrade.TRADE_USER);
        return data;
    }

    public void setSymbols(List<String> data_) {
        this.put(IATTrade.UNDERLYING, data_);
    }

    public List<String> getSymbols() {
        @SuppressWarnings("unchecked")
        var data = (ArrayList<String>) this.get(IATTrade.UNDERLYING);
        return data;
    }

    public void setItems(TradeTrace[] data_) {
        this.put(IATAssetConstants.DATA, data_);
    }

    public TradeTrace[] getItems() {
        var data = (TradeTrace[]) this.get(IATAssetConstants.DATA);
        return data;
    }

    public static class TradeTrace extends HashMap<String, Object> {

        public void setId(String data_) {
            this.put(IATAssetConstants.REFERENCE_ID, data_);
        }

        public String getId() {
            var data = (String) this.get(IATAssetConstants.REFERENCE_ID);
            return data;
        }

        public void setCount(Double data_) {
            this.put(IATAssetConstants.SIZE_TRADE, data_);
        }

        public Double getCount() {
            var data = (Double) this.get(IATAssetConstants.SIZE_TRADE);
            return data;
        }

        public void setPxTradeTotal(Double data_) {
            this.put(IATAssetConstants.PX_TRADE_TOTAL, data_);
        }

        public Double getPxTradeTotal() {
            var data = (Double) this.get(IATAssetConstants.PX_TRADE_TOTAL);
            return data;
        }

        public void setActive(Boolean data_) {
            this.put(IATAssetConstants.ACTIVE, data_);
        }

        public Boolean isActive() {
            var data = (Boolean) this.get(IATAssetConstants.ACTIVE);
            return data;
        }

    }
}