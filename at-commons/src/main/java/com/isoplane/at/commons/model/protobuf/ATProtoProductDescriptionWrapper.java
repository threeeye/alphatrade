package com.isoplane.at.commons.model.protobuf;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.protobuf.ProductDescriptionProtos.ProductDescription;;

public class ATProtoProductDescriptionWrapper {

	private ProductDescription _protoProductDescription;
	private ATEquityDescription _value;

	public ATProtoProductDescriptionWrapper(ProductDescription proto_) {
		this._protoProductDescription = proto_;
	}

	public ProductDescription proto() {
		return this._protoProductDescription;
	}

	public ATEquityDescription value() {
		if (_value == null) {
			_value = toProductDescription(_protoProductDescription);
		}
		return _value;
	}

	static public ATEquityDescription toProductDescription(ProductDescription tmpl_) {
		if (tmpl_ == null)
			return null;

		String occId = tmpl_.getId();
		String name = tmpl_.hasLabel() ? tmpl_.getLabel() : null;
		String dscr = tmpl_.hasDescription() ? tmpl_.getDescription() : null;
		String industry = tmpl_.hasIndustry() ? tmpl_.getIndustry() : null;
		String sector = tmpl_.hasSector() ? tmpl_.getSector() : null;

		ATEquityDescription eq = new ATEquityDescription(occId, name, dscr, sector, industry);
		return eq;
	}

	static public ProductDescription toProductDescription(ATEquityDescription dsc_) {
		if (dsc_ == null)
			return null;

		String id = dsc_.getOccId();

		String dscr = dsc_.getDescription();
		String industry = dsc_.getIndustry();
		String label = dsc_.getName();
		String sector = dsc_.getSector();
		String type = "n/a";

		ProductDescription.Builder builder = ProductDescription.newBuilder();
		builder.setId(id);
		builder.setType(type);

		if (!StringUtils.isBlank(dscr)) {
			builder.setDescription(dscr);
		}
		if (!StringUtils.isBlank(industry)) {
			builder.setIndustry(industry);
		}
		if (!StringUtils.isBlank(label)) {
			builder.setLabel(label);
		}
		if (!StringUtils.isBlank(sector)) {
			builder.setSector(sector);
		}
		ProductDescription protoRule = builder.build();
		return protoRule;
	}

}
