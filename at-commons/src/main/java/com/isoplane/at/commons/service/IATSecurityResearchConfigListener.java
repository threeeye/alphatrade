package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.ATSecurityResearchConfig.ATSecurityResearchConfigPack;

public interface IATSecurityResearchConfigListener {

	void notify(ATSecurityResearchConfigPack configPack);

}
