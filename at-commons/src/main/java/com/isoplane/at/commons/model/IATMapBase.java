package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.Map;

public interface IATMapBase extends Map<String, Object> {

    static void setBoolean(Map<String, Object> map_, String key_, Boolean value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static Boolean getBoolean(Map<String, Object> map_, String key_) {
        Boolean value = getValue(map_, key_, null);
        return value;
    }

    static Boolean getBoolean(Map<String, Object> map_, String key_, Boolean default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static void setDate(Map<String, Object> map_, String key_, Date value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static Date getDate(Map<String, Object> map_, String key_) {
        Date value = getValue(map_, key_, null);
        return value;
    }

    static Date getDate(Map<String, Object> map_, String key_, Date default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static void setDouble(Map<String, Object> map_, String key_, Double value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static Double getDouble(Map<String, Object> map_, String key_) {
        Double value = getValue(map_, key_, null);
        return value;
    }

    static Double getDouble(Map<String, Object> map_, String key_, Double default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static void setInt(Map<String, Object> map_, String key_, Integer value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static Integer getInt(Map<String, Object> map_, String key_) {
        Integer value = getValue(map_, key_, null);
        return value;
    }

    static Integer getInt(Map<String, Object> map_, String key_, Integer default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static void setLong(Map<String, Object> map_, String key_, Long value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static Long getLong(Map<String, Object> map_, String key_) {
        Long value = getValue(map_, key_, null);
        return value;
    }

    static Long getLong(Map<String, Object> map_, String key_, Long default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static void setString(Map<String, Object> map_, String key_, String value_, boolean isRemoveNull_) {
        setValue(map_, key_, value_, isRemoveNull_);
    }

    static String getString(Map<String, Object> map_, String key_) {
        String value = getValue(map_, key_, null);
        return value;
    }

    static String getString(Map<String, Object> map_, String key_, String default_) {
        var value = getValue(map_, key_, default_);
        return value;
    }

    static <T> T getValue(Map<String, Object> map_, String key_, T default_) {
        if (map_ == null)
            return null;
        @SuppressWarnings("unchecked")
        var value = (T) map_.get(key_);
        return value == null ? default_ : value;
    }

    static void setValue(Map<String, Object> map_, String prop_, Object value_, boolean isRemoveNull_) {
        if (value_ == null && isRemoveNull_) {
            map_.remove(prop_);
        } else {
            map_.put(prop_, value_);
        }
    }

}
