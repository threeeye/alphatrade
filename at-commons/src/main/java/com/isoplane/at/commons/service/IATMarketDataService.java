package com.isoplane.at.commons.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.IATMarketData.OptionRight;
import com.isoplane.at.commons.store.IATWhereQuery;

public interface IATMarketDataService extends IATService, IATMarketDataPublisher {

	List<ATMarketData> getMovers();

	Map<String, ATMarketData> getMarketData(IATWhereQuery query);

	ATMarketData getMarketData(String occId);

	Map<String, List<ATHistoryData>> getHistory(IATWhereQuery query, List<String> flags, String avgMode, String hvMode);

	ATSecurityPack getOptionChain(String symbol, OptionRight right);

	@Deprecated
	Map<String, Map<String, Set<ATMarketData>>> getOptionChainOld(String symbol, String right);

	Map<String, ATMarketData> getOptionUnderlying(String symbol);

}