package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.IATMarketData;

public interface IATMarketDataSubscriber {

	void notify(IATMarketData data);
	
	void notify(EATMarketStatus status);

}
