package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.WSMessageWrapper;

public interface IATWebSocketService  {

	void send(WSMessageWrapper data);
}
