package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATBroker extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public static final String LABEL_SHORT = "abbr";

	public ATBroker() {
	}

	public ATBroker(Map<String, Object> tmpl_) {
		if (tmpl_ == null)
			return;
		this.setId(IATAsset.getAtId(tmpl_));
		this.setLabel((String) tmpl_.get(IATAsset.LABEL));
		this.setLabelShort((String) tmpl_.get(LABEL_SHORT));
		this.setUser(IATAsset.getString(tmpl_, IATAsset.USER_ID));
		this.setReferenceId(IATAsset.getString(tmpl_, IATAsset.REFERENCE_ID));
		this.setBrokerId(IATAsset.getString(tmpl_, IATAsset.BROKER));
	}

	public String getUser() {
		return IATAsset.getString(this, IATAsset.USER_ID);
	}

	public void setUser(String value) {
		this.put(IATAsset.USER_ID, value);
	}

	public String getId() {
		return IATAsset.getAtId(this);
	}

	public void setId(String value) {
		this.put(IATLookupMap.AT_ID, value);
	}

	public String getLabel() {
		return (String) this.get(IATAsset.LABEL);
	}

	public void setLabel(String value) {
		this.put(IATAsset.LABEL, value);
	}

	public String getLabelShort() {
		String ls = (String) get(LABEL_SHORT);
		return ls != null ? ls : getLabel();
	}

	public void setLabelShort(String value) {
		this.put(LABEL_SHORT, value);
	}

	public String getReferenceId() {
		return IATAsset.getString(this, IATAsset.REFERENCE_ID);
	}

	public void setReferenceId(String value_) {
		IATAsset.setRemovableValue(this, IATAsset.REFERENCE_ID, value_);
	}

	public String getBrokerId() {
		return IATAsset.getString(this, IATAsset.BROKER);
	}

	public void setBrokerId(String value_) {
		IATAsset.setRemovableValue(this, IATAsset.BROKER, value_);
	}

}
