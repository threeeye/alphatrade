package com.isoplane.at.commons.model;

import java.util.Date;

public interface IATOptionExtension extends IATSecurity {

	Date getExpirationDate();
	Double getPriceTheo();
}
