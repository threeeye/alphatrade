package com.isoplane.at.commons.model;

import java.util.Arrays;
import java.util.List;

public interface IATUser {

	String USER_ALL = "all";
	String USER_JUPYTER = "jupyter";
	String USER_MOVERS = "movers";
	String USER_TRENDS = "trends";

	List<String> STATIC_USERS = Arrays.asList(USER_JUPYTER, USER_MOVERS, USER_TRENDS);

	String EMAIL = "email";
	String MESSAGE_TOKENS = "token_msg";
	String MESSAGE_MIN = "min";
	String MESSAGE_ALL = "all";
	String MESSAGE_NONE = "none";
	String ROLE = "role";

	String getAtId();

	String getEmail();

	List<String> getMessageTokens();

	List<String> getRoles();
}
