package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.protobuf.ATProtoSymbolPackWrapper;

public interface IATProtoSymbolSubscriber {

	void notifySymbolPack(ATProtoSymbolPackWrapper wrapper);

}
