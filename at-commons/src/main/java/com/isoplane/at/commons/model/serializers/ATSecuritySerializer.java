package com.isoplane.at.commons.model.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;

public class ATSecuritySerializer implements JsonSerializer<ATSecurity> {

	@Override
	public JsonElement serialize(ATSecurity src_, Type typeOfSrc_, JsonSerializationContext context_) {
		JsonElement result = null;
		if (src_ instanceof ATStock) {
		//	try {
			result = context_.serialize(src_, ATStock.class);
//			} catch(Exception ex) {
//				System.out.println(ex.toString());
//			}
		} else if (src_ instanceof ATOptionSecurity) {
			result = context_.serialize(src_, ATOptionSecurity.class);
		}
		return result;
	}

}
