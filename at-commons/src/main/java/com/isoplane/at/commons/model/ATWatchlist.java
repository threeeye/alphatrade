package com.isoplane.at.commons.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATWatchlist extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public ATWatchlist() {
	}

	public ATWatchlist(Map<String, Object> other_) {
		this.setId(ATWatchlist.getId(other_));
		this.setLabel(ATWatchlist.getLabel(other_));
		this.setSymbols(ATWatchlist.getSymbols(other_));
		this.setUserId(ATWatchlist.getUserId(other_));
	}

	public String getId() {
		return ATWatchlist.getId(this);
	}

	static public String getId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATLookupMap.AT_ID);
	}

	public void setId(String value) {
		if (value == null)
			this.remove(IATLookupMap.AT_ID);
		else
			this.put(IATLookupMap.AT_ID, value);
	}

	public String getUserId() {
		return ATWatchlist.getUserId(this);
	}

	static public String getUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value) {
		if (value == null)
			this.remove(IATAssetConstants.USER_ID);
		else
			this.put(IATAssetConstants.USER_ID, value);
	}

	public String getLabel() {
		return ATWatchlist.getLabel(this);
	}

	static public String getLabel(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.LABEL);
	}

	public void setLabel(String value) {
		if (value == null)
			this.remove(IATAssetConstants.LABEL);
		else
			this.put(IATAssetConstants.LABEL, value);
	}

	public ArrayList<String> getSymbols() {
		return ATWatchlist.getSymbols(this);
	}

	@SuppressWarnings({ "unchecked" })
	static public ArrayList<String> getSymbols(Map<String, Object> map_) {
		ArrayList<String> symbols = (ArrayList<String>) map_.get("symbols");
		return symbols;
	}

	public void setSymbols(ArrayList<String> value) {
		if (value == null || value.isEmpty())
			this.remove("symbols");
		else {
			this.put("symbols", value);
		}
	}

}
