package com.isoplane.at.commons.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATSystemStatusManager {

	static private Map<IATSystemStatusProvider, String> _providerMap = new HashMap<>();

	static public void register(IATSystemStatusProvider provider_) {
		ATSystemStatusManager.register(provider_, null);
	}

	static public void register(IATSystemStatusProvider provider_, String id_) {
		if (!_providerMap.containsKey(provider_)) {
			String id = StringUtils.isBlank(id_) ? provider_.getClass().getSimpleName() : id_;
			_providerMap.put(provider_, id);
		}
	}

	static public void unregister(IATSystemStatusProvider provider_) {
		_providerMap.remove(provider_);
	}

	static public Map<String, String> getStatus() {
		TreeMap<String, String> status = new TreeMap<>();
		status.put(" timestamp", ATFormats.DATE_TIME_mid.get().format(new Date()));
		for (Entry<IATSystemStatusProvider, String> entry1 : _providerMap.entrySet()) {
			String pid = entry1.getValue();
			Map<String, String> pstat = entry1.getKey().getSystemStatus();
			if (pstat == null)
				continue;
			for (Entry<String, String> entry2 : pstat.entrySet()) {
				String key = String.format("%s.%s", pid, entry2.getKey());
				status.put(key, entry2.getValue());
			}
		}
		return status;
	}

	public static class PerformanceMetrics implements IATSystemStatusProvider {

		private String _configKey;
		private String _id;
		private boolean _isRunning = false;
		private Map<String, Long> _startMap;
		private Map<String, Pair<Long, Long>> _performanceMap;
		private long _lastRun = 0;

		public PerformanceMetrics(String id_, String configKey_) {
			this._configKey = configKey_;
			this._id = id_;
			this._startMap = new HashMap<>();
			this._performanceMap = new HashMap<>();
		}

		public void collectMetrics(boolean isOn_) {
			this._isRunning = isOn_;
			if (isOn_) {
				ATSystemStatusManager.register(this, this._id);
			} else {
				ATSystemStatusManager.unregister(this);
			}
		}

		public void start(String id_) {
			if (!this._isRunning || !ATConfigUtil.config().getBoolean(this._configKey, false))
				return;
			long start = System.currentTimeMillis();
			this._lastRun = start;
			this._startMap.put(id_, start);
		}

		public void stop(String id_) {
			if (!this._isRunning || !ATConfigUtil.config().getBoolean(this._configKey, false))
				return;
			Long start = this._startMap.get(id_);
			if (start == null)
				return;
			Pair<Long, Long> metric = this._performanceMap.get(id_);
			if (metric == null) {
				metric = Pair.of(0L, 0L);
			}
			long count = metric.getLeft() + 1;
			long timing = metric.getRight() + (System.currentTimeMillis() - start);
			this._performanceMap.put(id_, Pair.of(count, timing));
		}

		@Override
		public Map<String, String> getSystemStatus() {
			Map<String, String> statusMap = new TreeMap<>();
			for (Entry<String, Pair<Long, Long>> entry : this._performanceMap.entrySet()) {
				String key = String.format("performance.%s", entry.getKey());
				Long value = entry.getValue().getRight() / entry.getValue().getLeft();
				statusMap.put(key, String.format("%d", value));
			}
			statusMap.put("last_run", this._lastRun == 0 ? "n/a" : ATFormats.DATE_full.get().format(new Date(this._lastRun)));
			return statusMap;
		}

		@Override
		public boolean isRunning() {
			return this._isRunning;
		}
	}

}
