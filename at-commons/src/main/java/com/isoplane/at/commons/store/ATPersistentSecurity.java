package com.isoplane.at.commons.store;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.IATEquityDescription;
import com.isoplane.at.commons.model.IATOptionExtension;

public class ATPersistentSecurity extends ATPersistentAssetBase implements IATEquityDescription, IATOptionExtension {

	private static final long serialVersionUID = 1L;

	public static final String IS_POSITION = "is_pos";

	transient private Double _priceTheo;
	transient private boolean _initialized;
	transient private boolean _priced;
	transient private Date _addedDate;
	transient protected Boolean _isOption;
	transient private Double _probability;

	public ATPersistentSecurity() {
	}

	public ATPersistentSecurity(String occId_) {
		setOccId(occId_);
	}

	public ATPersistentSecurity(Map<String, Object> data_) {
		super(data_);
	}

	public boolean isInitialized() {
		return _initialized;
	}

	public void setInitialized(boolean _initialized) {
		this._initialized = _initialized;
	}

	public Date getAddedDate() {
		return _addedDate;
	}

	public void setAddedDate(Date _addedDate) {
		this._addedDate = _addedDate;
	}

	@Override
	protected String getSortId() {
		String str = this.getSymbol();
		if (this.getUnderlyingOccId() != null) {
			str += this.getOptionRight() + this.getOccId();
		}
		return str;
	}

	@Override
	public Double getPrice() {
		// 4: last
		Double px = (Double) get(PX_LAST);
		if (px == null) {
			Double pxB = getPriceBid();
			Double pxA = getPriceAsk();
			if (pxA != null && pxB != null) {
				px = (pxA + pxB) / 2.0;
			}
		}
		return px;
	}

	@Override
	public String getOccId() {
		return getAtId();
	}

	@Override
	public Double getPriceHigh() {
		return (Double) get(PX_HIGH);
	}

	public void setPriceHigh(Double value_) {
		put(PX_HIGH, value_);
	}

	@Override
	public Double getPriceLow() {
		return (Double) get(PX_LOW);
	}

	public void setPriceLow(Double value_) {
		put(PX_LOW, value_);
	}

	@Override
	public Double getPriceAsk() {
		return (Double) get(PX_ASK);
	}

	public void setPriceAsk(Double value_) {
		put(PX_ASK, value_);
	}

	@Override
	public Double getPriceBid() {
		return (Double) get(PX_BID);
	}

	public void setPriceBid(Double value_) {
		put(PX_BID, value_);
	}

	@Override
	public Double getPriceClose() {
		return (Double) get(PX_CLOSE);
	}

	@Override
	public Double getPriceLast() {
		return (Double) get(PX_LAST);
	}

	@Override
	public Double getPriceOpen() {
		return (Double) get(PX_OPEN);
	}

	// TODO: Change to system constant
	public Double getHV() {
		return (Double) get(OPTION_HV);
	}

	// TODO: Change to system constant
	public Double getIV() {
		return (Double) get(OPTION_IV);
	}

	@Override
	public Long getVolume() {
		return (Long) get(VOLUME);
	}

	// @Override
	// public int compareTo(ATPersistentSecurity other_) {
	// String thisSortId = getSortId();
	// if (thisSortId == null) {
	// throw new ATException(String.format("Sort ID null [%s - %s - %s]", this, getSymbol(), getUnderlyingOccId()));
	// }
	// String otherSortId = other_ != null ? other_.getSortId() : null;
	// if (otherSortId == null) {
	// throw new ATException(String.format("Sort ID null [%s]", other_));
	// }
	// int result = thisSortId.compareTo(otherSortId);
	// return result;
	// }

	@Override
	public String toString() {
		return StringUtils.isNotBlank(getOccId()) ? getOccId() : super.toString();
	}

	@Override
	public String getIndustry() {
		return (String) get(INDUSTRY);
	}

	@Override
	public String getSector() {
		return (String) get(SECTOR);
	}

	@Override
	public String getDescription() {
		return (String) get(DESCRIPTION);
	}

	public boolean isPriced() {
		return _priced;
	}

	public void setPriced(boolean _priced) {
		this._priced = _priced;
	}

	public Boolean isPosition() {
		Boolean result = (Boolean) get(IS_POSITION);
		return result != null ? result : false;
	}

	public void setPosition(Boolean value_) {
		boolean value = value_ != null && value_ && !getAtId().startsWith("$");
		super.put(IS_POSITION, value);
	}

	public Double getProbability() {
		return _probability;
	}

	public void setProbability(Double probability_) {
		this._probability = probability_;
	}

	@Override
	public Double getPriceTheo() {
		return _priceTheo;
	}

	public void setPriceTheo(Double value_) {
		this._priceTheo = value_;
	}

}
