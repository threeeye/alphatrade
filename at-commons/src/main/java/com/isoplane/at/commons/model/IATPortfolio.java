package com.isoplane.at.commons.model;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATFormats;

public interface IATPortfolio extends Comparable<Object>, IATAsset {

	// static final String UNDERLYING = "_ul";

	String getAtId();

	String getOccId();

	String getUserId();

	String getTradeAccount();

	String getUnderlying();

	Date getTs();

	Long getTsLong();

	String getExpDS6();

	Double getSz();

	Double getPxTradeTotal();

	Double getPxAccumulated();

	// String getUnderlying();

	static void sortChronological(List<IATPortfolio> trades_) {
		if (trades_ == null)
			return;
		trades_.sort(new Comparator<IATPortfolio>() {
			@Override
			public int compare(IATPortfolio a_, IATPortfolio b_) {
				int comp = a_.getTs().compareTo(b_.getTs());
				if (comp == 0) {
					comp = a_.getAtId().compareTo(b_.getAtId());
				}
				return comp;
			}
		});
	}

	static void verify(IATPortfolio prt_, String userId_) {
		String occId = prt_.getOccId();
		if (StringUtils.isBlank(occId)) {
			throw new ATException(String.format("Missing OccId: %s", prt_));
		}
		// prt_.setOccId(occId.toUpperCase());
		if (userId_ != null && !userId_.equals(prt_.getUserId())) {
			throw new ATException(String.format("Owner mismatch [%s]: %s", userId_, prt_));
		}
		String account = prt_.getTradeAccount();
		if (StringUtils.isBlank(account)) {
			// TODO: Check account belongs to user
			throw new ATException(String.format("Missing account [%s]: %s", occId, prt_));
		}
	}

	static String getOccId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID);
	}

	static void setOccId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.OCCID, value_);
	}

	static String getUnderlying(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID_UNDERLYING);
	}

	static void setUnderlying(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.OCCID_UNDERLYING, value_);
	}

	static String getUserId(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_USER);
	}

	static void setUserId(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TRADE_USER, value_);
	}

	static Date getTs(Map<String, Object> map_) {
		try {
			Object tsn = map_.get(IATAssetConstants.TS);
			if (tsn != null) {
				if (tsn instanceof Double) {
					tsn = ((Double) tsn).longValue();
				}
				Date d = ATFormats.numberToDate((Long) tsn);
				return d;
			}
			return null;
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static void setTs(Map<String, Object> map_, Date value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.TS);
		} else {
			Long l = ATFormats.dateToNumber(value_);
			IATAsset.setMapValue(map_, IATAssetConstants.TS, l);
		}
	}

	static Long getTsLong(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS);
	}

	static void setTsLong(Map<String, Object> map_, Long value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TS, value_);
	}

	static String getExpDS6(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.EXPIRATION_DATE_STR);
	}

	static void setExpDS6(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.EXPIRATION_DATE_STR, value_);
	}

	static Double getSz(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.SIZE);
	}

	static void setSz(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.SIZE, value_);
	}

	static Double getPxTradeTotal(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE_TOTAL);
		return value;// != null ? value : 0;
	}

	static void setPxTradeTotal(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.PX_TRADE_TOTAL, value_);
	}

	static Double getPxAccumulated(Map<String, Object> map_) {
		Double value = IATAsset.getDouble(map_, IATAssetConstants.PX_ACU);
		return value;// != null ? value : 0;
	}

	static void setPxAccumulated(Map<String, Object> map_, Double value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.PX_ACU, value_);
	}

	static String getTradeAccount(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.TRADE_ACCOUNT);
	}

	static void setTradeAccount(Map<String, Object> map_, String value_) {
		IATAsset.setMapValue(map_, IATAssetConstants.TRADE_ACCOUNT, value_);
	}

	static String getUnderlyingSymbol(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID_UNDERLYING);
	}

	static Date getTsExpiration(IATPortfolio prt_) throws ParseException {
		String occId = prt_.getOccId();
		if (occId.length() <= 6)
			return null;
		String ds6 = occId.substring(6, 12);
		// NOTE: Y2100 bug!
		Date d = ATFormats.DATE_yyMMdd.get().parse(ds6);
		return d;
	}

	static String toString(IATPortfolio prt_) {
		String str = String.format("%1$-21s (%2$tF %2$tR/%4s) %3$.2f/%5$.2f", prt_.getOccId(),
				prt_.getTs(), prt_.getSz(), prt_.getTradeAccount(), prt_.getPxTradeTotal());
		return str;
	}

}
