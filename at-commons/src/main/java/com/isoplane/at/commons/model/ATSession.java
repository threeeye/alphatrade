package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.IATJsonSerializationExclude;

public class ATSession implements IATSession {

	static final long serialVersionUID = 1L;

		// public static final String USER_ID = "uid";
	// public static final String NAME = "name";
	// public static final String TOKEN = "token";

	private String email;
	private String name;
	private String token;
	// private Long created;
	// private Long updated;

	@IATJsonSerializationExclude
	private String userId;
	@IATJsonSerializationExclude
	private String clientIp;
	@IATJsonSerializationExclude
	private String refreshToken;
	private Long updateTime;
	private List<String> roles;

	private Map<String, String> data;

	public ATSession() {
	}

	public ATSession(IATSession session_) {
		this.setClientIp(session_.getClientIp());
		this.setEmail(session_.getEmail());
		this.setUpdateTime(session_.getUpdateTime());
		// this.setInsertTime(session_.geti);
		this.setName(session_.getName());
		// this.setRefreshToken(session_.getr);
		this.setToken(session_.getToken());
		// this.setUpdateTime(session_.getu);
		this.setUserId(session_.getUserId());
		this.setRoles(session_.getRoles());
		if (session_ instanceof ATSession) {
			this.setData(((ATSession) session_).getData());
		}
	}

	public ATSession(Map<String, Object> map_) {
		Object token = map_.get(TOKEN);
		if (token instanceof String)
			setToken((String) token);
		Object clientIp = map_.get(CLIENT_IP);
		if (clientIp instanceof String)
			setClientIp((String) clientIp);
		Object updateTs = map_.get(IATLookupMap.UPDATE_TIME);
		if (updateTs instanceof Long)
			setUpdateTime((Long) updateTs);
		Object userId = map_.get(USER_ID);
		if (userId instanceof String)
			setUserId((String) userId);
		Object roles = map_.get(IATUser.ROLE);
		if (roles instanceof List) {
			@SuppressWarnings("unchecked")
			List<String> r = (List<String>) roles;
			setRoles(r);
		}
		Object data = map_.get(DATA);
		if (data instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, String> d = (Map<String, String>) data;
			setData(d);
		}
	}

	public Map<String, Object> getAsMap() {
		Map<String, Object> map = new HashMap<>();
		map.put(TOKEN, getToken());
		map.put(CLIENT_IP, getClientIp());
		map.put(NAME, getName());
		map.put(USER_ID, getUserId());
		map.put(IATUser.ROLE, roles);
		map.put(DATA, getData());
		return map;
	}

	@Override
	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long value_) {
		this.updateTime = value_;
	}

	@Override
	public String getEmail() {
		return email;
	}

	public void setEmail(String value_) {
		email = value_;
	}

	@Override
	public String getToken() {
		return token;
	}

	public void setToken(String value_) {
		token = value_;
	}

	@Override
	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	@Override
	public String getUserId() {
		return userId;
	}

	public void setUserId(String value_) {
		userId = value_;
	}

	@Override
	public String getName() {
		return name;
	}

	public void setName(String value_) {
		name = value_;
	}

	@Override
	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> value_) {
		roles = value_;
	}

	public Map<String, String> getData() {
		return data;
	}

	public void setData(Map<String, String> data) {
		this.data = data;
	}

	// @Override
	// public void setUpdateTime(long value_) {
	// super.put(UPDATE_TIME, value_);
	// }
	//
	// @Override
	// public Long getUpdateTime() {
	// return super.getLong(UPDATE_TIME);
	// }
	//
	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }
	//
	// @Override
	// public Long getInsertTime() {
	// return super.getLong(INSERT_TIME);
	// }

}
