package com.isoplane.at.commons.util;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.WSMessageWrapper;

public class ATWSMessageQueue {

	static final Logger log = LoggerFactory.getLogger(ATWSMessageQueue.class);

	private static LinkedBlockingQueue<WSMessageWrapper> _queue = new LinkedBlockingQueue<>();

	public static boolean offer(WSMessageWrapper msg_) {
		return _queue.offer(msg_);
	}

	public static WSMessageWrapper poll(long timeoutMs_) {
		try {
			return _queue.poll(timeoutMs_, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			log.error(String.format("poll Error", e));
			return null;
		}
	}

	public static int size() {
		return _queue.size();
	}
}
