package com.isoplane.at.commons.util;

public interface IATEventSubscriber {

	void notify(IATEvent event);
}
