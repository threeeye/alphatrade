package com.isoplane.at.commons.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.model.IATMarketData.OptionRight;
import com.isoplane.at.commons.model.IATTrade;

// https://en.wikipedia.org/wiki/Option_symbol

public class ATModelUtil {

	static final Logger log = LoggerFactory.getLogger(ATModelUtil.class);

	static private SimpleDateFormat _sdf = new SimpleDateFormat("yyyyMMdd");

	synchronized static public ATSecurity toSecurity(String ticker_) {
		if (StringUtils.isBlank(ticker_))
			return null;
		try {
			ATSecurity result;
			if (ticker_.length() > 7) {
				int i = 0;
				while (i < ticker_.length() && !Character.isDigit(ticker_.charAt(i))) {
					i++;
				}
				String symbol = ticker_.substring(0, i).trim();
				int j = i;
				while (i < ticker_.length() && Character.isDigit(ticker_.charAt(i))) {
					i++;
				}
				String dateStr = ticker_.substring(j, i);
				Date expirationDate = _sdf.parse("20" + dateStr);
				j = i++;
				String rightStr = ticker_.substring(j, i);
				if (rightStr.length() != 1 || !(ATOptionSecurity.Put.equals(rightStr) || ATOptionSecurity.Call.equals(rightStr)))
					return null;
				String strikeStr = ticker_.substring(i, ticker_.length());
				if (strikeStr.length() != 8)
					return null;
				double strike = Double.parseDouble(strikeStr) / 1000.0;

				String label = String.format("%-5s%s %s %6.2f", symbol, dateStr, rightStr, strike);
				String occId = ATFormats.toOccId(symbol, expirationDate, rightStr, strike);

				if (log.isTraceEnabled()) {
					log.trace("Symbol: " + symbol);
					log.trace("OccId : " + occId);
					log.trace("Date  : " + expirationDate);
					log.trace("Right : " + rightStr);
					log.trace("Strike: " + strike);
					log.trace("Label : " + label);
				}

				ATOptionSecurity option = new ATOptionSecurity();
				option.setOccId(occId);
				option.setSymbol(symbol);
				option.setLabel(label);
				option.setStrike(strike);
				option.setOptionRight(rightStr);
				option.setExpirationDate(expirationDate);
				option.setChanged(true);
				result = option;
			} else {
				String symbol = ticker_.trim();
				ATStock stock = new ATStock();
				stock.setSymbol(symbol);
				stock.setLabel(symbol);
				stock.setOccId(symbol);
				stock.setChanged(true);
				result = stock;
			}
			result.setLastUpdate(new Date());
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error processing [%s]", ticker_), ex);
			return null;
		}
	}

	static public String getExpirationDS6(String occId_) {
		if (occId_.length() <= 12) {
			return null;
		}
		String expStr = occId_.substring(6, 12);
		return expStr;
	}

	static public Date getExpiration(String occId_) {
		try {
			String expStr = getExpirationDS6(occId_);
			if (expStr == null)
				return null;
			Date exp = ATFormats.DATE_yyMMdd.get().parse(expStr);
			return exp;
		} catch (Exception ex) {
			log.error(String.format("Error extracting expiration [%s]", occId_), ex);
			return null;
		}
	}

	static public int getDaysToExpiration(String occId_) {
		// String expStr = getExpirationDS6(occId_);
		// DateTimeFormatter df = DateTimeFormatter.ofPattern("yyMMdd", Locale.US);
		// LocalDate d = LocalDate.parse(expStr, df);
		LocalDate ld = ATModelUtil.getExpiration(occId_).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate now = LocalDate.now();
		int days = (int) ChronoUnit.DAYS.between(now, ld);
		return days;
	}

	static public Double getStrike(String occId_) {
		try {
			if (occId_.length() <= 13) {
				return null;
			}
			String strikeStr = occId_.substring(13);
			Long strikeL = Long.parseLong(strikeStr);
			Double strike = strikeL / 1000.0;
			return strike;
		} catch (Exception ex) {
			log.error(String.format("Error extracting strike [%s]", occId_), ex);
			return null;
		}
	}

	static public String getRight(String occId_) {
		if (occId_ == null || occId_.length() < 13)
			return null;
		String right = occId_.substring(12, 13);
		return right;
	}

	static public OptionRight getOptionRight(String occId_) {
		if (occId_ == null || occId_.length() < 13)
			return null;
		char c = occId_.charAt(12);
		OptionRight right = OptionRight.toOptionRight(c);
		return right;
	}

	public static String getUnderlyingOccId(String occId_) {
		if (occId_ == null || occId_.length() < 6)
			return null;
		String underlyingId = occId_.substring(0, 6).trim();
		return underlyingId;
	}

	/** Converts occId to underlying symbol */
	public static String getSymbol(String occId_) {
		if (occId_ == null || occId_.length() <= 6)
			return occId_;
		String underlyingId = occId_.substring(0, 6).trim();
		return underlyingId;
	}

	public static boolean isSymbol(String occId_) {
		return (occId_.length() > 6) ? false : true;
	}

	public static boolean verifyOccId(String occId_) {
		if (StringUtils.isBlank(occId_))
			return false;
		String symbol = occId_.length() <= 6 ? occId_ : occId_.substring(0, 6).trim();
		if (StringUtils.isNumeric(symbol)) {
			throw new ATException(String.format("Invalid symbol [%s]", occId_));
		}
		if (occId_.length() <= 6)
			return true;
		String dateStr = occId_.substring(6, 12);
		try {
			ATFormats.DATE_yyMMdd.get().parse(dateStr);
		} catch (ParseException pe_) {
			throw new ATException(String.format("Invalid expiration [%s]", occId_));
		}
		String rightStr = occId_.substring(12, 13).toUpperCase();
		if (!(IATTrade.CALL.equals(rightStr) || IATTrade.PUT.equals(rightStr)))
			return false;
		String strikeStr = occId_.substring(13);
		if (!StringUtils.isNumeric(strikeStr)) {
			throw new ATException(String.format("Invalid strike [%s]", occId_));
		}
		return true;
	}

}
