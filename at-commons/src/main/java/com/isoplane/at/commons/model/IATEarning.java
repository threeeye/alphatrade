package com.isoplane.at.commons.model;

import java.util.Date;

public interface IATEarning {
	
	String getOccId();
	String getEarningsDateStr();
	Date getEarningsDate();

}
