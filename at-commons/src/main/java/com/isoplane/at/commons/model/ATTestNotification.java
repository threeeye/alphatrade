package com.isoplane.at.commons.model;

import java.util.Map;

public class ATTestNotification extends ATBaseNotification {

	static long _counter = 0;

	public ATTestNotification() {
		_counter++;
		this.setMeta(META_DATA, "test");
		this.setSymbol("TEST");
	}

	public void resetCounter() {
		_counter = 0;
	}

	@Override
	public String getNotificationKey() {
		String key = String.format("test %d", _counter++);
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		long count = _counter;
		return new ATNotificationContent(String.format("Title [test/%d]", count), String.format("Body [test/%d]", count), "globe.ico");
	}

	@Override
	public String getNotificationTag() {
		return "proxy_test";
	}

	@Override
	public String getType() {
		return "test";
	}

}
