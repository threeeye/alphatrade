package com.isoplane.at.commons.model;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Precision;

import com.isoplane.at.commons.util.ATModelUtil;

public interface IATMarketData extends IATAsset {

	String getUnderlying();

	Double getPxLast();

	Integer getSzLast();

	Long getTsLast();

	Double getPxAsk();

	Long getTsAsk();

	Integer getSzAsk();

	Double getPxBid();

	Long getTsBid();

	Integer getSzBid();

	Long getDailyVol();

	Double getPxOpen();

	Double getPxClose();

	Double getPxClosePrev();

	/** Daily */
	Double getPxChange();

	Double getPxHigh();

	Double getPxLow();

	// /** Yield purchase/last */
	// Double getPxYield();

	Long getOpenInterest();

	Double getPxStrike();

	String getOptionRight();

	// Double getIV();

	<T> T getMeta(String key);

	// String getExpDS6();

	static public String getUnderlying(Map<String, Object> map_) {
		return IATAsset.getString(map_, IATAssetConstants.OCCID_UNDERLYING);
	}

	static public void setUnderlying(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.OCCID_UNDERLYING);
		} else {
			map_.put(IATAssetConstants.OCCID_UNDERLYING, value_);
		}
	}

	static Double calculatePxChange(IATMarketData mkt_) {
		Double last = mkt_.getPxLast();
		Double close = mkt_.getPxClosePrev();
		if (close == null)
			close = mkt_.getPxClose();
		if (last == null || last == 0 || close == null)
			return null;
		// double change = 100 * (last - close) / last;
		double change = 100 * (last - close) / close;
		change = Precision.round(change, 2);
		return change;
	}

	static public Double getPxMid(IATMarketData mkt_) {
		if (mkt_ == null)
			return null;
		Double pxAsk = mkt_.getPxAsk();
		if (pxAsk == null)
			pxAsk = 0.0;
		Double pxBid = mkt_.getPxBid();
		if (pxBid == null)
			pxBid = 0.0;
		return (pxAsk + pxBid) / 2.0;
	}

	static public Double getPx(IATMarketData mkt_, Long timeout_) {
		if (mkt_ == null)
			return null;
		Long tsLast = mkt_.getTsLast();
		Double pxLast = mkt_.getPxLast();
		if (tsLast != null && tsLast != 0 && pxLast != null && tsLast > System.currentTimeMillis() - timeout_)
			return pxLast;
		Double pxMid = getPxMid(mkt_);
		Double px = (pxMid != null & pxMid != 0) ? pxMid : pxLast;
		return px;
	}

	static public String getExpDS6(IATMarketData mkt_) {
		if (mkt_ == null || !(mkt_ instanceof Map))
			return null;
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) mkt_;
		return IATAsset.getString(map, IATAssetConstants.EXPIRATION_DATE_STR);
	}

	static public Double getPxLast(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_LAST);
	}

	static public void setPxLast(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_LAST);
		else
			map_.put(IATAssetConstants.PX_LAST, value_);
	}

	// static public Long getSzLast(Map<String, Object> map_) {
	// return IATAsset.getLong(map_, IATAssetConstants.SIZE_LAST);
	// }
	//
	// static public void setSzLast(Map<String, Object> map_, Long value_) {
	// if (value_ == null)
	// map_.remove(IATAssetConstants.SIZE_LAST);
	// else
	// map_.put(IATAssetConstants.SIZE_LAST, value_);
	// }

	static public Long getTsLast(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_LAST);
	}

	static public void setTsLast(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_LAST);
		else
			map_.put(IATAssetConstants.TS_LAST, value_);
	}

	static public Double getPxAsk(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_ASK);
	}

	static public void setPxAsk(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_ASK);
		else
			map_.put(IATAssetConstants.PX_ASK, value_);
	}

	static public Long getTsAsk(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_ASK);
	}

	static public void setTsAsk(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_ASK);
		else
			map_.put(IATAssetConstants.TS_ASK, value_);
	}

	// static public Long getSzAsk(Map<String, Object> map_) {
	// return IATAsset.getLong(map_, IATAssetConstants.SIZE_ASK);
	// }

	// static public void setSzAsk(Map<String, Object> map_, Long value_) {
	// if (value_ == null)
	// map_.remove(IATAssetConstants.SIZE_ASK);
	// else
	// map_.put(IATAssetConstants.SIZE_ASK, value_);
	// }

	static public Double getPxBid(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_BID);
	}

	static public void setPxBid(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_BID);
		else
			map_.put(IATAssetConstants.PX_BID, value_);
	}

	static public Long getTsBid(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.TS_BID);
	}

	static public void setTsBid(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.TS_BID);
		else
			map_.put(IATAssetConstants.TS_BID, value_);
	}

	// static public Long getSzBid(Map<String, Object> map_) {
	// return IATAsset.getLong(map_, IATAssetConstants.SIZE_BID);
	// }

	// static public void setSzBid(Map<String, Object> map_, Long value_) {
	// if (value_ == null)
	// map_.remove(IATAssetConstants.SIZE_BID);
	// else
	// map_.put(IATAssetConstants.SIZE_BID, value_);
	// }

	static public Long getDailyVol(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.VOLUME_DAILY);
	}

	static public void setDailyVol(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.VOLUME_DAILY);
		else
			map_.put(IATAssetConstants.VOLUME_DAILY, value_);
	}

	static public Double getPxOpen(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_OPEN);
	}

	static public void setPxOpen(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_OPEN);
		else
			map_.put(IATAssetConstants.PX_OPEN, value_);
	}

	static public Double getPxClose(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_CLOSE);
	}

	static public void setPxClose(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_CLOSE);
		else
			map_.put(IATAssetConstants.PX_CLOSE, value_);
	}

	static public Double getPxClosePrev(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_CLOSE_PREVIOUS);
	}

	static public void setPxClosePrev(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_CLOSE_PREVIOUS);
		else
			map_.put(IATAssetConstants.PX_CLOSE_PREVIOUS, value_);
	}

	static public Double getPxHigh(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_HIGH);
	}

	static public void setPxHigh(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_HIGH);
		else
			map_.put(IATAssetConstants.PX_HIGH, value_);
	}

	static public Double getPxLow(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_LOW);
	}

	static public void setPxLow(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_LOW);
		else
			map_.put(IATAssetConstants.PX_LOW, value_);
	}

	static public Long getOpenInterest(Map<String, Object> map_) {
		return IATAsset.getLong(map_, IATAssetConstants.OPEN_INTEREST);
	}

	static public void setOpenInterest(Map<String, Object> map_, Long value_) {
		if (value_ == null)
			map_.remove(IATAssetConstants.OPEN_INTEREST);
		else
			map_.put(IATAssetConstants.OPEN_INTEREST, value_);
	}

	static public Double getPxStrike(Map<String, Object> map_) {
		Double strike = IATAsset.getPxStrike(map_);
		if (strike == null) {
			String occId = IATAsset.getOccId(map_);
			strike = ATModelUtil.getStrike(occId);
			if (strike != null) {
				IATAsset.setPxStrike(map_, strike);
			}
		}
		return strike;
	}

	// static public void setPxStrike(Map<String, Object> map_, Double value_) {
	// if (value_ == null || Double.isNaN(value_))
	// map_.remove(IATAssetConstants.PX_STRIKE);
	// else
	// map_.put(IATAssetConstants.PX_STRIKE, value_);
	// }

	static public Double getPxChange(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_CHANGE);
	}

	static public void setPxChange(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_CHANGE);
		else
			map_.put(IATAssetConstants.PX_CHANGE, value_);
	}

	// static public Double getPxYield(Map<String, Object> map_) {
	// return IATAsset.getDouble(map_, IATAssetConstants.PX_YIELD);
	// }
	//
	// static public void setPxYield(Map<String, Object> map_, Double value_) {
	// if (value_ == null || Double.isNaN(value_))
	// map_.remove(IATAssetConstants.PX_YIELD);
	// else
	// map_.put(IATAssetConstants.PX_YIELD, value_);
	// }

	static public String getOptionRight(Map<String, Object> map_) {
		String right = IATAsset.getString(map_, IATAssetConstants.OPTION_RIGHT);
		if (right == null) {
			String occId = IATAsset.getOccId(map_);
			right = ATModelUtil.getRight(occId);
			if (right != null) {
				map_.put(IATAssetConstants.OPTION_RIGHT, right);
			}
		}
		return right;
	}

	static public void setOptionRight(Map<String, Object> map_, String value_) {
		if (StringUtils.isBlank(value_))
			map_.remove(IATAssetConstants.OPTION_RIGHT);
		else
			map_.put(IATAssetConstants.OPTION_RIGHT, value_);
	}

	static <T> T getMeta(Map<String, Object> map_, String key_) {
		@SuppressWarnings("unchecked")
		T data = (T) map_.get(key_);
		return data;
	}

	// static public String getExpDS6(Map<String, Object> map_) {
	// return IATAsset.getString(map_, IATAssetConstants.EXPIRATION_DATE_STR);
	// }
	//
	// static public void setExpDS6(Map<String, Object> map_, String value_) {
	// if (StringUtils.isBlank(value_))
	// map_.remove(IATAssetConstants.EXPIRATION_DATE_STR);
	// else
	// map_.put(IATAssetConstants.EXPIRATION_DATE_STR, value_);
	// }

	static public Double getPxTheo(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.PX_THEORETICAL);
	}

	static public void setPxTheo(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.PX_THEORETICAL);
		else
			map_.put(IATAssetConstants.PX_THEORETICAL, value_);
	}

	static public Double getOptionDelta(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.DELTA);
	}

	static public void setOptionDelta(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.DELTA);
		else
			map_.put(IATAssetConstants.DELTA, value_);
	}

	static public Double getOptionGamma(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.GAMMA);
	}

	static public void setOptionGamma(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.GAMMA);
		else
			map_.put(IATAssetConstants.GAMMA, value_);
	}

	static public Double getOptionRho(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.RHO);
	}

	static public void setOptionRho(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.RHO);
		else
			map_.put(IATAssetConstants.RHO, value_);
	}

	static public Double getOptionTheta(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.THETA);
	}

	static public void setOptionTheta(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.THETA);
		else
			map_.put(IATAssetConstants.THETA, value_);
	}

	static public Double getOptionVega(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.VEGA);
	}

	static public void setOptionVega(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.VEGA);
		else
			map_.put(IATAssetConstants.VEGA, value_);
	}

	static public Double getIV(Map<String, Object> map_) {
		return IATAsset.getDouble(map_, IATAssetConstants.IV);
	}

	static public void setIV(Map<String, Object> map_, Double value_) {
		if (value_ == null || Double.isNaN(value_))
			map_.remove(IATAssetConstants.IV);
		else
			map_.put(IATAssetConstants.IV, value_);
	}

	static enum OptionRight {
		CALL, PUT;

		static public OptionRight toOptionRight(String rightStr_) {
			if (rightStr_ == null || rightStr_.length() == 0)
				return null;
			char char1 = Character.toLowerCase(rightStr_.charAt(0));
			OptionRight right = 'c' == char1 ? CALL : PUT;
			return right;
		}

		static public OptionRight toOptionRight(char rightChar_) {
			char char1 = Character.toLowerCase(rightChar_);
			OptionRight right = 'c' == char1 ? CALL : PUT;
			return right;
		}
	}

}
