package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.ATUserNote.ATUserNotesPack;

public interface IATUserNotesListener {

	void notify(ATUserNotesPack notesPack);

}
