package com.isoplane.at.commons.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public interface IATEquityFoundation {

	String getOccId();

	Double getMarketCap();

	String getModel();

	String getLabel();

	String getSymbol();

	String getDescription();

	String getIndustry();

	String getSector();

	String getEarningsDS8();

	String getDividendExDS8();

	String getDividendRecordDS8();

	String getDividendPayDS8();

	Double getDividend();

	Integer getDividendFrequency();

	Map<String, Double> getHvMap();

	Double getHV(String mode);

	Map<String, Double> getAvgMap();

	Double getAvg(String mode);

	Map<String, Double> getMinMap();

	Double getMin(String mode);

	Map<String, Double> getMaxMap();

	Double getMax(String mode);

	DateTimeFormatter _yyyyMMddFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

	// SimpleDateFormat _yyyyMMddFormat = ATFormats.DATE_yyyyMMdd.get();

	static Double getMarketCap(Map<String, Object> map_) {
		return (Double) map_.get(IATAssetConstants.MARKET_CAP);
	}

	static String getModel(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.MODEL);
	}

	static String getOccId(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.OCCID);
	}

	static String getLabel(Map<String, Object> map_) {
		return getOccId(map_);
	}

	static String getSymbol(Map<String, Object> map_) {
		return getOccId(map_);
	}

	static String getDescription(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.DESCRIPTION);
	}

	static void setDescription(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.DESCRIPTION);
		} else {
			map_.put(IATAssetConstants.DESCRIPTION, value_);
		}
	}

	static String getIndustry(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.INDUSTRY);
	}

	static void setIndustry(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.INDUSTRY);
		} else {
			map_.put(IATAssetConstants.INDUSTRY, value_);
		}
	}

	static String getSector(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.SECTOR);
	}

	static void setSector(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(IATAssetConstants.SECTOR);
		} else {
			map_.put(IATAssetConstants.SECTOR, value_);
		}
	}

	static String getEarningsDS8(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.EARNINGS_DATE_STR_OLD);
	}

	static Date getEarningsDate(Map<String, Object> map_) {
		String dstr = getEarningsDS8(map_);
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormatter.parse(dstr);
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			return null;
		}
	}

	static String getDividendExDS8(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.DIV_EX_DATE_STR_OLD);
	}

	static Date getDividendExDate(Map<String, Object> map_) {
		String dstr = getDividendExDS8(map_);
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			return null;
		}
	}

	static String getDividendRecordDS8(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.DIV_REC_DATE_STR);
	}

	static Date getDividendRecordDate(Map<String, Object> map_) {
		String dstr = getDividendRecordDS8(map_);
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			return null;
		}
	}

	static String getDividendPayDS8(Map<String, Object> map_) {
		return (String) map_.get(IATAssetConstants.DIV_PAY_DATE_STR);
	}

	static Date getDividendPayDate(Map<String, Object> map_) {
		String dstr = getDividendPayDS8(map_);
		if (StringUtils.isBlank(dstr))
			return null;
		try {
			// Date date = _yyyyMMddFormat.parse(dstr);
			// return date;
			LocalDate date = LocalDate.parse(dstr, _yyyyMMddFormatter);
			return java.sql.Date.valueOf(date);
		} catch (Exception ex) {
			return null;
		}
	}

	static Double getDividend(Map<String, Object> map_) {
		return (Double) map_.get(IATAssetConstants.DIVIDEND);
	}

	static Integer getDividendFrequency(Map<String, Object> map_) {
		return (Integer) map_.get(IATAssetConstants.DIV_FREQUENCY);
	}

	static Double getHV(Map<String, Object> map_, String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case IATStatistics.HV10:
			return getHV10(map_);
		case IATStatistics.HV20:
			return getHV20(map_);
		case IATStatistics.HV30:
			return getHV30(map_);
		case IATStatistics.HV60:
			return getHV60(map_);
		case IATStatistics.HV90:
			return getHV90(map_);
		case IATStatistics.HV125:
			return getHV125(map_);
		case IATStatistics.HV254:
			return getHV254(map_);
		}
		return null;
	}

	static Double getHV10(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV10);
	}

	static Double getHV20(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV20);
	}

	static Double getHV30(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV30);
	}

	static Double getHV60(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV60);
	}

	static Double getHV90(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV90);
	}

	static Double getHV125(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV125);
	}

	static Double getHV254(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.HV254);
	}

	static Double getMin(Map<String, Object> map_, String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case IATStatistics.MIN60:
			return getMin60(map_);
		case IATStatistics.MIN125:
			return getMin125(map_);
		case IATStatistics.MIN254:
			return getMin254(map_);
		}
		return null;
	}

	static Double getMin60(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MIN60);
	}

	static Double getMin125(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MIN125);
	}

	static Double getMin254(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MIN254);
	}

	static Double getMax(Map<String, Object> map_, String mode_) {
		if (mode_ == null)
			return null;
		switch (mode_) {
		case IATStatistics.MAX60:
			return getMax60(map_);
		case IATStatistics.MAX125:
			return getMax125(map_);
		case IATStatistics.MAX254:
			return getMax254(map_);
		}
		return null;
	}

	static Double getMax60(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MAX60);
	}

	static Double getMax125(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MAX125);
	}

	static Double getMax254(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.MAX254);
	}

	// static void setNote(String value_) {
	// _note = value_;
	// }

	//
	// static String getNote() {
	// return _note;
	// }

	static Double getAvg(Map<String, Object> map_, String mode_) {
		if (mode_ == null)
			return null;
		Object obj = map_.get(mode_);
		return (obj != null && obj instanceof Double) ? (Double) obj : null;
	}

	static Double getSma60(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.SMA60);
	}

	static Double getSma125(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.SMA125);
	}

	static Double getSma254(Map<String, Object> map_) {
		return (Double) map_.get(IATStatistics.SMA254);
	}

}
