package com.isoplane.at.commons.model.protobuf;

import java.util.Date;

import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.protobuf.FundamentalProtos.Fundamental;;

public class ATProtoFundamentalWrapper {

	private Fundamental _proto;
	private ATFundamental _at;

	public ATProtoFundamentalWrapper(Fundamental proto_) {
		this._proto = proto_;
	}

	public Fundamental proto() {
		return this._proto;
	}

	public ATFundamental value() {
		if (this._at == null) {
			this._at = toFundamental(this._proto);
		}
		return this._at;
	}

	static public ATFundamental toFundamental(Fundamental proto_) {
		ATFundamental fnd = new ATFundamental();
		fnd.setOccId(proto_.getSymbol());
		if (proto_.hasDividend()) {
			fnd.setDividend(proto_.getDividend());
		}
		if (proto_.hasDividendEstDs8()) {
			fnd.setDividendEstDS8(proto_.getDividendEstDs8());
		}
		if (proto_.hasDividendExDs8()) {
			fnd.setDividendExDS8(proto_.getDividendExDs8());
		}
		// fnd.setDividendFrequency(null);
		if (proto_.hasEarningsDs8()) {
			fnd.setEarningsDS8(proto_.getEarningsDs8());
		}
		if (proto_.hasIv()) {
			fnd.setIV(proto_.getIv());
		}
		if (proto_.hasMarketCap()) {
			fnd.setMarketCap(proto_.getMarketCap());
		}
		if (proto_.hasAvgMode()) {
			fnd.setModeAvg(proto_.getAvgMode());
		}
		if (proto_.hasHvMode()) {
			fnd.setModeHV(proto_.getHvMode());
		}
		if (proto_.hasAvgPx()) {
			fnd.setPxAvg(proto_.getAvgPx());
		}
		if (proto_.hasHvPx()) {
			fnd.setPxHV(proto_.getHvPx());
		}
		return fnd;
	}

	static public Fundamental toFundamental(ATFundamental value_, Date date_) {
		return toFundamental(value_, date_, false);
	}

	static public Fundamental toFundamental(ATFundamental value_, Date date_, boolean isTest_) {
		Fundamental.Builder builder = Fundamental.newBuilder();
		builder.setSymbol(value_.getOccId());
		// builder.setSource(value_.gets)
		builder.setTimestamp(date_.getTime());
		if (isTest_) {
			builder.setIsTest(isTest_);
		}
		String avgMode = value_.getModeAvg();
		if (avgMode != null) {
			builder.setAvgMode(avgMode);
		}
		Double avgPx = value_.getPxAvg();
		if (avgMode != null) {
			builder.setAvgPx(avgPx);
		}
		Double dividend = value_.getDividend();
		if (dividend != null) {
			builder.setDividend(dividend);
		}
		String dividendEstDs8 = value_.getDividendEstDS8();
		if (dividendEstDs8 != null) {
			builder.setDividendEstDs8(dividendEstDs8);
		}
		String dividendExDs8 = value_.getDividendExDS8();
		if (dividendExDs8 != null) {
			builder.setDividendExDs8(dividendExDs8);
		}
		String earningsDs8 = value_.getEarningsDS8();
		if (earningsDs8 != null) {
			builder.setEarningsDs8(earningsDs8);
		}
		String hvMode = value_.getModeHV();
		if (hvMode != null) {
			builder.setHvMode(hvMode);
		}
		Double hvPx = value_.getPxHV();
		if (hvPx != null) {
			builder.setHvPx(hvPx);
		}
		Double iv = value_.getIV();
		if (iv != null) {
			builder.setIv(iv);
		}
		Double marketCap = value_.getMarketCap();
		if (marketCap != null) {
			builder.setMarketCap(marketCap);
		}

		Fundamental fnd = builder.build();
		return fnd;
	}

}
