package com.isoplane.at.commons.util;

import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATRecyclingThread extends Thread {

	static final Logger log = LoggerFactory.getLogger(ATRecyclingThread.class);

	private boolean _isRunning;
	private long _period;
	private long _timeout;
	private long _lastRunTime;
	private Supplier<Long> _func;
	private int _sortId;

	/**
	 * The given supplier must return the next delay period. A value of -1 means exit.
	 * 
	 * @param name_
	 *            - name of the thread
	 * @param supplier_
	 *            - executing function
	 */
	public ATRecyclingThread(String name_, Supplier<Long> supplier_, long timeout_) {
		super(name_);
		_func = supplier_;
		_timeout = timeout_;
		// _period = sleepTime_;
	}

	@Override
	public void run() {
		while (_isRunning) {
			try {
				_period = _func.get();
				// log.trace(String.format("T: period [%s - %d]", getName(), _period));
				if (_period == -1) {
					_isRunning = false;
				} else {
					_lastRunTime = System.currentTimeMillis();
					// log.trace(String.format("T: last run [%s - %d]", getName(), _lastRunTime));
					Thread.sleep(_period);
				}
			} catch (Exception ex) {
				log.error("Error", ex);
			}
		}
		log.info(String.format("Exit [%s]", getName()));
	}

	@Override
	public synchronized void start() {
		_isRunning = true;
		super.start();
	}

	public void setPeriod(long period) {
		this._period = period;
	}

	public long getPeriod() {
		return _period;
	}
	public void setTimeout(long value) {
		this._timeout = value;
	}
	public long getTimeout() {
		return this._timeout;
	}

	public void quit() {
		_isRunning = false;
	}

	public long getLastRunTime() {
		return _lastRunTime;
	}

	public void setLastRunTime(long time) {
		this._lastRunTime = time;
	}

	public Supplier<Long> getFunction() {
		return _func;
	}

	public void setFunction(Supplier<Long> func) {
		this._func = func;
	}

	public int getSortId() {
		return _sortId;
	}

	public void setSortId(int sortId) {
		this._sortId = sortId;
	}

}
