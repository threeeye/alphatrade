package com.isoplane.at.commons.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Date;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATSysUtils {

	static final Logger log = LoggerFactory.getLogger(ATSysUtils.class);

	public static String getMemoryStatus() {
		try {
			long heapSize = Runtime.getRuntime().totalMemory();
			long heapMaxSize = Runtime.getRuntime().maxMemory();
			long heapFreeSize = Runtime.getRuntime().freeMemory();
			double pctFree = 100.0 * heapFreeSize / heapMaxSize;
			String msg = String.format("Memory status [heap: %d; max: %d; free: %d (%.2f%%)]", heapSize, heapMaxSize,
					heapFreeSize, pctFree);
			return msg;
		} catch (Exception ex) {
			log.error("Error getting memory status", ex);
			String msg = String.format("Error getting memory status: %s", ex);
			return msg;
		}
	}

	public static void waitForEnter() {
		printNotice();
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		log.info("Terminating");
	}

	public static void printNotice() {
		log.info("########################");
		log.info("# Hit enter to quit... #");
		log.info("########################");
	}

	public static String getIpAddress(String default_) {
		String ipStr = null;
		try (final DatagramSocket socket = new DatagramSocket()) {
			socket.connect(InetAddress.getByName("8.8.8.8"), 10002);
			ipStr = socket.getLocalAddress().getHostAddress();
			if (StringUtils.isBlank(ipStr)) {
				InetAddress ip = InetAddress.getLocalHost();
				ipStr = ip.getHostAddress();
			}
		} catch (Exception ex) {
			log.error(String.format("getIpAddress Error"), ex);
		}
		if (StringUtils.isBlank(ipStr)) {
			log.warn(String.format("Unable to obtain IP address. Returning default [%s]", default_));
			ipStr = default_;
		}
		return ipStr;
	}

	public static long currentTimeMillis() {
		var time = System.currentTimeMillis();
		return time;
	}

	public static Date newDate() {
		var date = new Date();
		return date;
	}

	/**
	* Reads given resource file as a string.
	*
	* @param path
	*            path to the resource file
	* @return the file's contents
	* @throws IOException
	*             if read fails for any reason
	*/
	public static String getResourceFileAsString(String path) {
		ClassLoader classLoader = ClassLoader.getSystemClassLoader();
		try (InputStream is = classLoader.getResourceAsStream(path)) {
			if (is == null)
				return null;
			try (InputStreamReader isr = new InputStreamReader(is);
					BufferedReader reader = new BufferedReader(isr)) {
				return reader.lines().collect(Collectors.joining(System.lineSeparator()));
			}
		} catch (Exception ex) {
			log.error(String.format("Couldn't read '%s'", path), ex);
			return null;
		}
	}

}
