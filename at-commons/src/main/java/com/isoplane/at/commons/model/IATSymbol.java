package com.isoplane.at.commons.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

public interface IATSymbol extends Comparable<IATSymbol> {

	// TODO
	// , sector, industry

	// Providers
	// static final String CONVERTERS = "prov";
	static final String ALLY = "ally";
	static final String AMERITRADE = "TDA";
	static final String ETRADE = "eTrd";
	static final String FIDELITY = "FID";
	static final String SCHWAB = "SCH";
	static final String TRADIER = "tradier";
	static final String YAHOO = "yahoo";

	// Types
	static final String EQUITY = "eqt";
	static final String COMMODITY = "comm";
	static final String INDEX = "index";

	// Sources
	// static final String REFERENCES = "src";
	static final String USER = "user";
	static final String POSITION = "pos";
	// static final String SP500 = "sp500";

	String getId();

	String getModel();

	String getName();

	String getDescription();

	String getSector();

	String getIndustry();

	String getSource();

	Boolean isSP500();

	Boolean isR3000();

	Set<String> getReferences();

	Map<String, String> getConverters();

	String convert(String provider);

	Map<Date, ATOptionChain> getOptions();

	static String convert(IATSymbol symbol_, String converterId_) {
		String id = symbol_.getId();
		Map<String, String> converters = symbol_.getConverters();
		String convertedId = converters != null ? converters.get(converterId_) : null;
		return convertedId != null ? convertedId : id;
	}

	static int compareTo(IATSymbol this_, IATSymbol other_) {
		if (other_ == null) {
			return this_ == null ? 0 : -1;
		}
		if (this_ == null) {
			return 1;
		}
		return this_.getId().compareTo(other_.getId());
	}
}
