package com.isoplane.at.commons.model.serializers;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.util.ATModelUtil;

public class ATSecurityDeserializer implements JsonDeserializer<ATSecurity> {

	@Override
	public ATSecurity deserialize(JsonElement json_, Type typeOfT_, JsonDeserializationContext context_)
			throws JsonParseException {
		if (json_ == null || json_.isJsonNull() || !json_.isJsonObject())
			return null;
		String ticker = ((JsonObject) json_).get("tickerId").getAsString();
		ATSecurity security = ATModelUtil.toSecurity(ticker);
		return security;
	}

}
