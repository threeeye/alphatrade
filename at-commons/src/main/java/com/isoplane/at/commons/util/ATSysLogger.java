package com.isoplane.at.commons.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/* 
 * Configure as follows / Substitute <logger name> with actual name
 * 
 * 
 * 	<appender name="SYSLOG-TLS" class="com.papertrailapp.logback.Syslog4jAppender">
		<layout class="ch.qos.logback.classic.PatternLayout">
			<pattern>%-5level | %-36c{36} | %msg%n</pattern>
		</layout>

		<syslogConfig
			class="org.productivity.java.syslog4j.impl.net.tcp.ssl.SSLTCPNetSyslogConfig">
			<host>logs3.papertrailapp.com</host>
			<port>50247</port>
			<ident>AT</ident>
			<maxMessageLength>128000</maxMessageLength>
		</syslogConfig>
	</appender>	
 * 
 * 
 * 	<logger name="<looger name>" level="INFO"
		additivity="false">
		<appender-ref ref="SYSLOG-TLS" />
	</logger>
 */


// logs3.papertrailapp.com:50247
public class ATSysLogger {

	static Logger log = LoggerFactory.getLogger(ATSysLogger.class);

	static public void setLoggerClass(Class<?> clazz_) {
		log = LoggerFactory.getLogger(clazz_);
	}

	static public void setLoggerClass(String name_) {
		log = LoggerFactory.getLogger(name_);
	}

	static public void info(String msg_) {
		log.info(msg_);
	}

	static public void error(String msg_) {
		log.error(msg_);
	}

	static public void error(String msg_, Exception ex_) {
		log.error(msg_, ex_);
	}
}
