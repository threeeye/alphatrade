package com.isoplane.at.commons.store;

import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.IATAssetConstants;

// TODO: Consider implementing Comparable<T>
public class ATPersistentLookupBase extends HashMap<String, Object> implements IATLookupMap {

	private static final long serialVersionUID = 1L;

	transient private boolean _isChanged;

	public ATPersistentLookupBase() {
		setInsertTime(System.currentTimeMillis());
		setUpdateTime(System.currentTimeMillis());
		_isChanged = false;
	}

	public ATPersistentLookupBase(Map<String, Object> data_) {
		super(data_ == null ? new HashMap<>() : data_);
		_isChanged = false;
	}

	@Override
	public Object put(String key_, Object value_) {
		Object old = get(key_);
		{
			if (old != null) {
				if (!old.equals(value_)) {
					setChanged(true);
				}
			} else if (value_ != null) {
				setChanged(true);
			}
		}
		return super.put(key_, value_);
	}

	public String getAtId() {
		return (String) get(AT_ID);
	}

	public void setAtId(String id_) {
		put(AT_ID, id_);
	}

	public Long getInsertTime() {
		return (Long) get(INSERT_TIME);
	}

	public void setInsertTime(long ins) {
		super.put(INSERT_TIME, ins); // Bypass changed
	}

	public Long getUpdateTime() {
		Object value = get(UPDATE_TIME);
		if (value instanceof Double) {
			value = Math.round((Double) value);
			setUpdateTime((Long) value);
		}
		return (Long) value;
	}

	public void setUpdateTime(long upd) {
		super.put(UPDATE_TIME, upd); // Bypass changed
	}

	public int getType() {
		Integer value = (Integer) get(IATAssetConstants.TYPE);
		return value != null ? value : -1;
	}

	final public void setType(int value_) {
		put(IATAssetConstants.TYPE, value_);
	}

	final public boolean isChanged() {
		return _isChanged;
	}

	final public void setChanged(boolean value_) {
		_isChanged = value_;
	}

	public void setIgnore(String reason_) {
		super.put(IGNORE, reason_);
	}

	public String getIgnore() {
		return (String) super.get(IGNORE);
	}

}
