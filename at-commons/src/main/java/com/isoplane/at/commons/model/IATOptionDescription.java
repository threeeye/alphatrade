package com.isoplane.at.commons.model;

import java.util.Date;

public interface IATOptionDescription extends Comparable<IATOptionDescription> {

	String getSymbol();

	Date getExpiration();
	
	String getExpirationDS6();

	Double getStrike();

	String getOccId();

	Double getMultiplier();

	Right getRight();

	public static enum Right {
		Call, Put, UNSET;
		
		static public String right2String(Right right_) {
			switch (right_) {
			case Call:
				return "C";
			case Put:
				return "P";
			default:
				return null;
			}
		}

		static public Right string2Right(String str_) {
			switch (str_) {
			case "C":
				return Right.Call;
			case "P":
				return Right.Put;
			default:
				return Right.UNSET;
			}
		}
	}
}
