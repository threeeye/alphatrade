package com.isoplane.at.commons.service;

import java.util.Collection;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;

public interface IATProtoMarketSubscriber {

	void notifyMarketData(Collection<ATProtoMarketDataWrapper> data);

	void notifyMarketDataPack(ATProtoMarketDataPackWrapper data);

	void notifyMarketStatus(ATProtoMarketStatusWrapper data);

}
