package com.isoplane.at.commons.service;

import com.isoplane.at.commons.model.protobuf.ATProtoAlertRuleWrapper;

public interface IATProtoAlertRuleSubscriber {

	void notifyAlertRule(ATProtoAlertRuleWrapper wrapper);

}
