package com.isoplane.at.commons.store;

import java.util.Collection;

public interface IATWhereQuery {

	IATWhereQuery empty();

	IATWhereQuery and();

	IATWhereQuery or();

	IATWhereQuery eq(String field, Object value);

	IATWhereQuery gte(String field, Object value);

	IATWhereQuery gt(String field, Object value);

	IATWhereQuery lte(String field, Object value);

	IATWhereQuery lt(String field, Object value);

	IATWhereQuery ne(String field, Object value);

	IATWhereQuery notNull(String field);

	IATWhereQuery isNull(String field);

	IATWhereQuery contains(String field, String value);

	IATWhereQuery startsWith(String field, String value);

	IATWhereQuery endsWith(String field, String value);

	IATWhereQuery in(String field_, Collection<? extends Object> value);

	IATWhereQuery nin(String field, Collection<? extends Object> value);

	IATWhereQuery where(String expr);

	IATWhereQuery expr(String expr);

	IATWhereQuery and(IATWhereQuery other);

	IATWhereQuery or(IATWhereQuery other);

	boolean isEmpty();


}
