package com.isoplane.at.commons.model;

import java.util.Map;

public class ATGenericNotification extends ATBaseNotification {

	// private String body;
	private ATNotificationContent content;
	private String key;
	private String tag;
	private String type;

	@Override
	public String getNotificationTag() {
		return tag;
	}

	public void setNotificationTag(String value_) {
		this.tag = value_;
	}

	@Override
	public String getNotificationKey() {
		return key;
	}

	public void setNotificationKey(String value_) {
		this.key = value_;
	}

	@Override
	public String getType() {
		return type;
	}

	public void setType(String value_) {
		this.type = value_;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap, long timeout) {
		return getNotificationContent();
	}

	// public String getBody() {
	// return body;
	// }
	//
	// public void setBody(String body) {
	// this.body = body;
	// }

	public ATNotificationContent getNotificationContent() {
		return content;
	}

	public void setNotificationContent(ATNotificationContent content) {
		this.content = content;
	}

}
