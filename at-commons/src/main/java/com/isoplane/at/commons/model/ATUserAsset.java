package com.isoplane.at.commons.model;

import java.util.HashMap;
import java.util.Map;

public class ATUserAsset extends HashMap<String, Object> implements Comparable<ATUserAsset>, IATUserAsset {

	private static final long serialVersionUID = 1L;

	public ATUserAsset() {
	}

	public ATUserAsset(String userId_, String dateStr_, String symbol_, Double size_, Double value_) {
		this.setUserId(userId_);
		this.setDateStr(dateStr_);
		this.setOccId(symbol_);
		this.setSize(size_);
		this.setValue(value_);
	}

	public ATUserAsset(IATUserAsset tmpl_) {
		this(tmpl_.getUserId(), tmpl_.getDateStr(), tmpl_.getOccId(), tmpl_.getSize(), tmpl_.getValue());
	}

	@Override
	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getDateStr() {
		return IATAsset.getString(this, IATAssetConstants.DATE_STR);
	}

	public void setDateStr(String value_) {
		super.put(IATAssetConstants.DATE_STR, value_);
	}

	@Override
	public Double getSize() {
		return IATAsset.getDouble(this, IATAssetConstants.SIZE_TOTAL);
	}

	public void setSize(Double value_) {
		super.put(IATAssetConstants.SIZE_TOTAL, value_);
	}

	@Override
	public String getUserId() {
		return IATAsset.getString(this, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value_) {
		super.put(IATAssetConstants.USER_ID, value_);
	}

	@Override
	public Double getValue() {
		return IATAsset.getDouble(this, IATAssetConstants.VAL_TOTAL);
	}

	public void setValue(Double value_) {
		super.put(IATAssetConstants.VAL_TOTAL, value_);
	}

	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public int compareTo(ATUserAsset other_) {
		if (other_ == null)
			return -1;
		return 0;
	}

}
