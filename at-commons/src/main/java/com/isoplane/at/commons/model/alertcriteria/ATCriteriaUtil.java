package com.isoplane.at.commons.model.alertcriteria;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;

public class ATCriteriaUtil {

	static final Logger log = LoggerFactory.getLogger(ATCriteriaUtil.class);

//	static public Set<ATBaseCriteria> createCriteria(String... strs_) {
//		if (strs_ == null || strs_.length == 0)
//			return null;
//		Set<ATBaseCriteria> crits = new HashSet<>();
//		for (String str : strs_) {
//			try {
//				String[] tokens = StringUtils.isNotBlank(str) ? str.split(":") : null;
//				if (tokens == null || tokens.length < 1 || StringUtils.isBlank(tokens[0]))
//					continue;
//				ATBaseCriteria crit = null;
//				String occId = tokens[0].trim().toUpperCase();
//				if (tokens.length == 1) {
//					crit = ATTrueCriteria.create(occId);
//				} else if (tokens.length >= 2 && StringUtils.isNotBlank(tokens[1])) {
//					String token2 = tokens[1].replace(" ", "").toUpperCase();
//					if ("OFF".equals(token2)) {
//						crit = ATNullCriteria.create(occId);
//					} else if (token2.startsWith("PX")) {
//						String sub = tokens[1].substring(2);
//						String note = tokens.length > 2 ? tokens[2].trim() : "";
//						crit = ATPriceCriteria.create(occId, sub, note);
//					} else if (token2.startsWith("D")) {
//						String sub = tokens[1].substring(2);
//						String note = tokens.length > 2 ? tokens[2].trim() : "";
//						crit = ATDateCriteria.create(occId, sub, note);
//					}
//				}
//				if (crit != null) {
//					crits.add(crit);
//				}
//			} catch (Exception ex) {
//				log.error(String.format("Error parsing criteria [%s]. Please use comma instead of semicolon.", str),
//						ex);
//			}
//		}
//		return crits;
//	}

	// @Deprecated
	// static public Set<ATBaseCriteria> createCriteria(String userId_, TdnAlert... alerts_) {
	// if (alerts_ == null || alerts_.length == 0)
	// return null;
	// Set<ATBaseCriteria> crits = new HashSet<>();
	// for (TdnAlert alert : alerts_) {
	// try {
	// if (alert == null || StringUtils.isBlank(alert.occId) || StringUtils.isBlank(alert.prp) || StringUtils.isBlank(alert.opr) ||
	// alert.val == null)
	// continue;
	// ATBaseCriteria crit = null;
	// String occId = alert.occId.trim().toUpperCase();
	// String property = alert.prp.trim().replace(" ", "").toUpperCase();
	// if ("OFF".equals(property)) {
	// crit = ATNullCriteria.create(occId);
	// } else if (property.startsWith("PX")) {
	// crit = ATPriceCriteria.create(userId_, alert);
	// } else if (property.startsWith("D")) {
	// crit = ATDateCriteria.create(userId_, alert);
	// // crit = ATDateCriteria.create(occId, alert.opr+alert.val, alert.msg);
	// }
	// if (crit != null) {
	// crits.add(crit);
	// }
	// } catch (Exception ex) {
	// log.error(String.format("Error parsing criteria [%s]. Please use comma instead of semicolon.", alert),
	// ex);
	// }
	//
	// }
	// return crits;
	// }

	static public Set<ATBaseCriteria> createCriteria(ATUserAlertRulePack pack_) {
		if (pack_ == null || pack_.alerts == null || pack_.alerts.length == 0)
			return null;
		Set<ATBaseCriteria> crits = new HashSet<>();
		// Map<String, List<ATUserAlertGroup>> groupMap = pack_.groups != null
		// ? Arrays.asList(pack_.groups).stream().collect(Collectors.groupingBy(g -> g.id))
		// : new HashMap<>();
		List<ATAlertRule> alerts = Arrays.asList(pack_.alerts);
		Map<String, List<ATAlertRule>> alertGroupMap = alerts.stream().collect(Collectors.groupingBy(ATAlertRule::getGroup));
		for (String groupId : alertGroupMap.keySet()) {
			if (StringUtils.isBlank(groupId)) {

				continue;
			} else {
				// List<ATUserAlertGroup> group = groupMap.get(groupId);
				// if (group == null) {
				// log.error(String.format("AlertPack missing group [%s]", groupId));
				// continue;
				// }
			}
		}

		return crits;
	}

	static public Set<ATBaseCriteria> createCriteria(String userId_, ATAlertRule... alerts_) {
		if (alerts_ == null || alerts_.length == 0)
			return null;
		Set<ATBaseCriteria> crits = new HashSet<>();
		for (ATAlertRule alert : alerts_) {
			try {
				if (alert == null || StringUtils.isBlank(alert.getOccId()) || StringUtils.isBlank(alert.getProperty())
						|| StringUtils.isBlank(alert.getOperator()) ||
						alert.getValue() == null)
					continue;
				ATBaseCriteria crit = null;
				String occId = alert.getOccId().trim().toUpperCase();
				String property = alert.getProperty().trim().replace(" ", "").toUpperCase();
				if ("OFF".equals(property)) {
					crit = ATNullCriteria.create(occId);
				} else if (property.startsWith("PX")) {
					crit = ATPriceCriteria.create(userId_, alert);
				} else if (property.startsWith("D")) {
					crit = ATDateCriteria.create(userId_, alert);
				} else if (property.startsWith("SSTAT")) {
					crit = ATScalarStatCriteria.create(userId_, alert);
				} else if (property.startsWith("MSTAT")) {
					crit = ATMarketStatCriteria.create(userId_, alert);
				}
				if (crit != null) {
					crits.add(crit);
				}
			} catch (Exception ex) {
				log.error(String.format("Error parsing criteria [%s]. Please use comma instead of semicolon.", alert),
						ex);
			}

		}
		return crits;
	}

}
