package com.isoplane.at.commons.model;

import java.util.HashMap;

public class ATOption extends HashMap<String, Object> implements IATOption {

	private static final long serialVersionUID = 1L;

	@Override
	public String getOccId() {
		return IATAsset.getOccId(this);
	}

	@Override
	public Double getStrike() {
		return IATAsset.getPxStrike(this);
	}

	@Override
	public String getRight() {
		return IATAsset.getRight(this);
	}

	@Override
	public String getExpDS6() {
		return IATAsset.getExpDS6(this);
	}

	@Override
	public Double getPxLast() {
		return IATAsset.getPxLast(this);
	}

	@Override
	public Integer getSzLast() {
		return IATAsset.getSzLast(this);
	}

	@Override
	public Long getTsLast() {
		return IATAsset.getTsLast(this);
	}

	@Override
	public Double getPxAsk() {
		return IATAsset.getPxAsk(this);
	}

	@Override
	public Integer getSzAsk() {
		return IATAsset.getSzAsk(this);
	}

	@Override
	public Double getPxBid() {
		return IATAsset.getPxBid(this);
	}

	@Override
	public Integer getSzBid() {
		return IATAsset.getSzBid(this);
	}

	@Override
	public Double getOptionMultiplier() {
		return IATAsset.getOptionMultiplier(this);
	}

	@Override
	public Double getPxTime() {
		return IATAsset.getPxTime(this);
	}

	@Override
	public Double getPxTheo() {
		return IATAsset.getPxTheo(this);
	}

	@Override
	public Double getVolatility() {
		return IATAsset.getIV(this);
	}

	@Override
	public Double getDelta() {
		return IATAsset.getDelta(this);
	}

	@Override
	public Double getGamma() {
		return IATAsset.getGamma(this);
	}

	@Override
	public Double getRho() {
		return IATAsset.getRho(this);
	}

	@Override
	public Double getTheta() {
		return IATAsset.getTheta(this);
	}

	@Override
	public Double getVega() {
		return IATAsset.getVega(this);
	}

}
