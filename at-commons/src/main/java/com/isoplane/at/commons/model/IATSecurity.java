package com.isoplane.at.commons.model;

public interface IATSecurity { // extends Comparable<T>

	String getLabel();

	String getOccId();

	Double getPrice();
	
	Double getPriceHigh();
	
	Double getPriceLow();

	Double getPriceAsk();

	Double getPriceBid();

	Double getPriceClose();

	Double getPriceLast();

	Double getPriceOpen();
	
	Long getVolume();

	String getSymbol(); // Same for Option or Underlying


}
