package com.isoplane.at.portfolio.model;

import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATUserAsset;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATMongoUserAsset extends Document implements IATUserAsset, IATLookupMap {

	private static final long serialVersionUID = 1L;

	@Override
	public String getOccId() {
		return IATAsset.getString(this, IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		super.put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public String getDateStr() {
		return IATAsset.getString(this, IATAssetConstants.DATE_STR);
	}

	public void setDateStr(String value_) {
		super.put(IATAssetConstants.DATE_STR, value_);
	}

	@Override
	public Double getSize() {
		return IATAsset.getDouble(this, IATAssetConstants.SIZE_TOTAL);
	}

	public void setSize(Double value_) {
		super.put(IATAssetConstants.SIZE_TOTAL, value_);
	}

	@Override
	public String getUserId() {
		return IATAsset.getString(this, IATAssetConstants.USER_ID);
	}

	public void setUserId(String value_) {
		super.put(IATAssetConstants.USER_ID, value_);
	}

	@Override
	public Double getValue() {
		return IATAsset.getDouble(this, IATAssetConstants.VAL_TOTAL);
	}

	public void setValue(Double value_) {
		super.put(IATAssetConstants.VAL_TOTAL, value_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map_) {
		super.putAll(map_);
	}

	@Override
	public void setUpdateTime(long upd) {
		// TODO Auto-generated method stub
	}

	@Override
	public Long getUpdateTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Long getInsertTime() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setAtId(String id_) {
		this.setOccId(id_);
	}

	@Override
	public String getAtId() {
		return this.getOccId();
	}

}
