package com.isoplane.at.portfolio.util;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;

public class ATTradeCumulator {

	static final Logger log = LoggerFactory.getLogger(ATTradeCumulator.class);

	static public Double cumulateTrades(String userId_, Collection<ATTrade> trades_) {
		if (trades_ == null || trades_.isEmpty()) {
			log.info(String.format("cumulateTrades No trades [%s]", userId_));
			return null;
		}

		String symbol = null;
		double total = 0;
		for (ATTrade trade : trades_) {
			String occId = trade.getOccId();
			Integer code = trade.getTradeType();
			if (code == null) {
				log.error(String.format("Error Missing Type [%s]", occId));
				continue;
			}
			String tradeSymbol = ATModelUtil.getSymbol(occId);
			if (symbol != null && !symbol.equals(tradeSymbol)) {
				log.error(String.format("Error Symbol mismatch [%s / %s]", symbol, tradeSymbol));
				continue;
			}
			symbol = tradeSymbol;

			Double pxTotal = trade.getPxTradeTotal();
			if (pxTotal != null) {
				total += pxTotal;
			}
			if (log.isTraceEnabled()) {
				IATTrade.Type type = IATTrade.TRADE_TYPE_MAP.get(code);
				String dStr = ATFormats.DATE_TIME_mid.get().format(trade.getTsTrade());
				log.trace(String.format("cumulateTrades: %s (%-4s): %-22s(%8.2f)%10.2f /%10.2f", dStr, type, occId,
						trade.getSzTrade(), trade.getPxTradeTotal(), total));
			}
		}
		log.debug(String.format("cumulateTrades [%s:%s]: %.2f", userId_, symbol, total));
		return total;
	}

}
