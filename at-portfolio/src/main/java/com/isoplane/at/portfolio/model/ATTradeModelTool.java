package com.isoplane.at.portfolio.model;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;

public class ATTradeModelTool {

	static final Logger log = LoggerFactory.getLogger(ATTradeModelTool.class);

	private static Map<Class<?>, Field> fieldMap = new HashMap<Class<?>, Field>();

	static public void register(Class<?> clazz_) {
		Field field = FieldUtils.getField(clazz_, "hashStr", true);
		fieldMap.put(clazz_, field);
	}

	static public String hashString(IATTrade trade_, boolean includeActive_) {
		if (trade_ == null)
			return null;
		Double activeCount = includeActive_ ? trade_.getSzTradeActive() : 0;
		long tsTrade = trade_.getTsTrade().getTime();// trade_.getTsTrade() != null ? trade_.getTsTrade().getTime() : 0;
		// String hstr = String.format("%1$s%2$d%3$.4f%4$.4f%5$2d%6$s%7$s%8$.2f", trade_.getOccId(), tsTrade, trade_.getPxTrade(),
		// trade_.getSzTrade(), trade_.getTradeType(), trade_.getTradeAccount(), trade_.getTradeComment(), activeCount);
		String hstr = String.format("%1$s%2$d%3$.4f%4$.4f%5$.4f%6$2d%7$s%8$s%9$.2f", trade_.getOccId(), tsTrade, trade_.getPxTrade(),
				trade_.getPxTradeTotal(), trade_.getSzTrade(), trade_.getTradeType(), trade_.getTradeAccount(), trade_.getTradeComment(),
				activeCount);
		return hstr;
	}

	static public String atId(IATTrade trade_) {
		try {
			Field f = fieldMap.get(trade_.getClass());
			String h = f != null ? (String) f.get(trade_) : null;
			if (h == null) {
				h = hashString(trade_, false);
				// f.set(trade_, h);
			}
			return String.format("T%d:%d", trade_.getTradeUserId().hashCode(), h.hashCode());
		} catch (Exception ex) {
			String msg = String.format("Hashing error [%s]", trade_);
			throw new ATException(msg, ex);
		}
	}

	static public String groupHashString(IATTrade trade_) {
		long tsTrade = trade_.getTsTrade().getTime();
		String occId = trade_.getOccId();
		String symbol = occId.length() > 6 ? occId.substring(0, 6) : occId;
		String hstr = String.format("%d%s%s", tsTrade, trade_.getTradeAccount(), symbol.trim());
		return hstr;

	}

	static public String groupId(IATTrade trade_) {
		String hstr = groupHashString(trade_).hashCode() + "";
		return hstr;
	}

	static public boolean equals(IATTrade trade_, Object other_) {
		if (!(other_ instanceof IATTrade))
			return false;
		try {
			Field f1 = fieldMap.get(trade_.getClass());
			Field f2 = fieldMap.get(other_.getClass());
			String h1 = (String) f1.get(trade_);
			if (h1 == null) {
				h1 = hashString(trade_, false);
				f1.set(trade_, h1);
			}
			String h2 = (String) f2.get(other_);
			if (h2 == null) {
				h2 = hashString((IATTrade) other_, false);
				f2.set(other_, h2);
			}
			return h1.equals(h2);
		} catch (Exception ex) {
			log.error(String.format("Hashing error [%s / %s]", trade_, other_), ex);
			return false;
		}
	}

	static public int compare(IATTrade t1_, Object t2_) {
		if (t1_ == null && t2_ == null)
			return 0;
		if (t1_ == null)
			return 1;
		if (t2_ == null || !(t2_ instanceof IATTrade))
			return -1;
		try {
			IATTrade t2 = (IATTrade) t2_;
			Field f1 = fieldMap.get(t1_.getClass());
			Field f2 = fieldMap.get(t2_.getClass());
			String h1 = (String) f1.get(t1_);
			if (h1 == null) {
				h1 = hashString(t1_, false);
				f1.set(t1_, h1);
			}
			String h2 = (String) f2.get(t2_);
			if (h2 == null) {
				h2 = hashString(t2, false);
				f2.set(t2_, h2);
			}
			int result = h1.compareTo(h2);
			return result;
		} catch (Exception ex) {
			log.error(String.format("Hashing error [%s / %s]", t1_, t2_), ex);
			return 0;
		}
	}

	// static public String toString(IATTrade trade_) {
	// // String str = String.format("%1$-21s %2$tF %2$tR %3$7.2f %4$8.2f %5$10.2f %6$3d %7$-7s active[%8$-5b] %9$s", trade_.getOccId(),
	// // trade_.getTradeTimestamp(), trade_.getCount(), trade_.getUnitPrice(), trade_.getTotalPrice(), trade_.getTradeType(),
	// // trade_.getAccount(), trade_.isActive(), trade_.getComment());
	// String str = String.format("%1$-21s %2$tF %2$tR (%3$.2f/%4$.2f)", trade_.getOccId(),
	// trade_.getTsTrade(), trade_.getSzTradeActive(), trade_.getSzTrade());
	// return str;
	// }

	static public boolean verify(IATTrade trade_) {
		if (trade_ == null)
			return false;
		if (!ATModelUtil.verifyOccId(trade_.getOccId()))
			return false;
		Date tradeDate = trade_.getTsTrade();
		if (tradeDate == null) {
			throw new ATException(String.format("Invalid trade date [%s]", trade_));
		}
		Integer type = trade_.getTradeType();
		String typeStr = type != null ? IATTrade.TYPE_MAP.get(type) : null;
		if (typeStr == null) {
			throw new ATException(String.format("Invalid trade type [%s]", trade_));
		}
		if (StringUtils.isBlank(trade_.getTradeAccount())) {
			throw new ATException(String.format("Invalid account [%s]", trade_));
		}
		if (trade_.getPxTradeTotal() == null) {
			throw new ATException(String.format("Invalid total [%s]", trade_));
		}
		if (trade_.getTradeGroup() == null) {
			throw new ATException(String.format("Invalid group [%s]", trade_));
		}
		if (trade_.getTradeType() < 20) {
			if (trade_.getPxTrade() == null) {
				throw new ATException(String.format("Invalid unit price [%s]", trade_));
			}
			if (trade_.getSzTrade() == null) {
				throw new ATException(String.format("Invalid count [%s]", trade_));
			}
		}
		return true;
	}

	static private SimpleDateFormat dfExp = ATFormats.DATE_yyyyMMdd.get();
	static private String centuryStr;
	static {
		LocalDate d = LocalDate.now();
		int century = d.getYear() / 100;
		centuryStr = century + "";
	}

	static public Date getExpiration(String occId_) {
		try {
			if (occId_ == null || occId_.length() <= 12)
				return null;
			String dStr = centuryStr + occId_.substring(6, 12);
			Date date = dfExp.parse(dStr);
			return date;
		} catch (Exception ex) {
			throw new ATException(String.format("Error parsing [%s]", occId_));
		}
	}

}
