package com.isoplane.at.portfolio.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.util.ATFormats;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATTradeDigester {

	static final Logger log = LoggerFactory.getLogger(ATTradeDigester.class);

	static public Map<String, ATTradeDigest> digestTrades(String userId_, String account_, String symbol_,
			Collection<ATTrade> trades_) {
		if (trades_ == null || trades_.isEmpty()) {
			log.info(String.format("digestTrades No trades [%s / %s / %s]", userId_, account_, symbol_));
			return null;
		}

		double cash = 0;
		Map<String, ATTradeDigest> digestMap = new TreeMap<>();
		ATTradeDigest ulGroup = new ATTradeDigest(userId_, account_, symbol_);
		digestMap.put(symbol_, ulGroup);
		// Level: Symbol - Account - Trade
		for (ATTrade trade : trades_) {
			// log.info(String.format("Trade: %s - %s", accTrade.getTsTrade(), accTrade.getOccId()));
			String occId = trade.getOccId();
			Integer code = trade.getTradeType();
			if (code == null) {
				log.error(String.format("Error Missing Type [%s]", occId));
				continue;
			}
			String traceId = trade.getTraceId();
			ATTradeDigest digest = digestMap.get(occId);
			if (digest == null) {
				digest = new ATTradeDigest(userId_, account_, occId);
				digest.setTraceId(traceId);
				digestMap.put(occId, digest);
			} else if (symbol_.equals(occId)) {
				String ulTrace = ulGroup.getTraceId();
				if (ulTrace == null) {
					ulGroup.setTraceId(traceId);
				} else if (!ulTrace.equals(traceId)) {
					log.error(String.format("digestTrades Error: UL trace id mismatch [%s/%s/%s - %s / %s]", userId_,
							account_, symbol_, ulTrace, traceId));
				}
			}

			IATTrade.Type type = IATTrade.TRADE_TYPE_MAP.get(code);
			Double px = null;
			switch (type) {
				case ADR:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case ASG:
					px = closePosition(type, trade, ulGroup, digest);
					break;
				case BTC:
					px = closePosition(type, trade, ulGroup, digest);
					break;
				case BTO:
					px = openPosition(type, trade, ulGroup, digest);
					break;
				case CGLT:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case CGST:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case DIV:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case EXP:
					px = closePosition(type, trade, ulGroup, digest);
					break;
				case EXR:
					px = closePosition(type, trade, ulGroup, digest);
					break;
				case ILO:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case INT:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				case OTH:
					log.info(String.format("digestTrades Ignoring: %s", trade));
					break;
				case RI:
					px = openPosition(type, trade, ulGroup, digest);
					break;
				case STC:
					px = closePosition(type, trade, ulGroup, digest);
					break;
				case STO:
					px = openPosition(type, trade, ulGroup, digest);
					break;
				case TXF:
					px = adjustCost(type, trade, ulGroup, digest);
					break;
				default:
					String msg = String.format("digestTrades Error Invalid TYPE [%s / %s / %d]", occId, type,
							trade.getTsTrade2());
					throw new ATException(msg);
			}
			if (log.isTraceEnabled()) {
				String dStr = ATFormats.DATE_TIME_mid.get().format(trade.getTsTrade());
				log.trace(String.format("digestTrades: %s %-10s(%-4s): %-22s(%8.2f /%8.2f)%10.2f /%10.2f /%10.2f", dStr,
						account_, type, occId,
						trade.getSzTrade(), digest.getSzNet(), trade.getPxTradeTotal(), ulGroup.getPxNet(),
						ulGroup.getPxCumulative()));
			}
			if (px != null) {
				cash += px;
				Double typeYield = ulGroup.typeYieldMap.get(type);
				ulGroup.typeYieldMap.put(type, typeYield == null ? px : typeYield + px);
			}
		}
		Iterator<Entry<String, ATTradeDigest>> ig = digestMap.entrySet().iterator();
		while (ig.hasNext()) {
			Entry<String, ATTradeDigest> entry = ig.next();
			ATTradeDigest digest = entry.getValue();
			Double sz = digest.getSzNet();
			if (sz == null || sz == 0) {
				ig.remove();
			}
			Double pxCum = digest.getPxCumulative();
			if (pxCum != null) {
				digest.setPxCumulative(Math.round(pxCum * 10000.0) / 10000.0);
			}
			Double pxNet = digest.getPxNet();
			if (pxNet != null) {
				digest.setPxNet(Math.round(pxNet * 10000.0) / 10000.0);
			}
		}
		log.debug(String.format("digestTrades [%s:%s:%s]: %.2f - %.2f : %.2f", userId_, account_, symbol_,
				ulGroup.getSzNet(), ulGroup.getPxCumulative(), cash));
		return digestMap;
	}

	static private Double adjustCost(IATTrade.Type type_, IATTrade trade_, ATTradeDigest ulGroup_,
			ATTradeDigest tradeGroup_) {
		double pxt = trade_.getPxTradeTotal();

		if (ulGroup_.getSzNet() != 0) {
			ulGroup_.setPxNet(ulGroup_.getPxNet() + pxt);
		}
		ulGroup_.setPxCumulative(ulGroup_.getPxCumulative() + pxt);
		// if (log.isTraceEnabled()) {
		// log.trace(String.format("adjustCost: %s - %s: %.2f / %.2f", ulGroup_.getOccId(), type_, ulGroup_.getPxNet(), ulGroup_.getPxCumulative()));
		// }
		return pxt;
	}

	static private Double openPosition(IATTrade.Type type_, ATTrade newTrade_, ATTradeDigest ulGroup_,
			ATTradeDigest tradeGroup_) {
		String occId = newTrade_.getOccId();
		double szGroupOld = tradeGroup_.getSzNet();
		double szNew = newTrade_.getSzTrade();
		switch (type_) {
			case BTO:
			case RI:
				if (szNew <= 0 || szGroupOld < 0) {
					String msg = String.format("Error Invalid sz_trd [%s / %s / %d]", occId, type_,
							newTrade_.getTsTrade2());
					throw new ATException(msg);
				}
				break;
			case STO:
				if (szNew >= 0 || szGroupOld > 0) {
					String msg = String.format("Error Invalid sz_trd [%s / %s / %d]", occId, type_,
							newTrade_.getTsTrade2());
					throw new ATException(msg);
				}
				break;
			default:
				String msg = String.format("Error Invalid type [%s / %s / %d]", occId, type_, newTrade_.getTsTrade2());
				throw new ATException(msg);
		}

		double pxNew = newTrade_.getPxTradeTotal();
		var szGroupNew = Precision.round(szGroupOld + szNew, 4);
		tradeGroup_.setSzNet(szGroupNew);
		tradeGroup_.setPxNet(tradeGroup_.getPxNet() + pxNew);
		tradeGroup_.setPxTrade(tradeGroup_.getPxTrade() + pxNew);
		tradeGroup_.setPxCumulative(tradeGroup_.getPxCumulative() + pxNew);
		tradeGroup_.trades.add(newTrade_);
		if (ulGroup_ != tradeGroup_) {
			ulGroup_.setPxCumulative(ulGroup_.getPxCumulative() + pxNew);
		}
		//	long tsGroupOld = tradeGroup_.getTsTrade();
		//	long tsNew = newTrade_.getTsTrade().getTime();
		//	long tsGroupNew = Math.round(((tsGroupOld * szGroupOld) + (tsNew * szNew)) / (szGroupOld + szNew));
		//	tradeGroup_.setTsTrade(tsGroupNew);

		// if (ulGroup_ != tradeGroup_) {
		// ulGroup_.setPxCumulative(ulGroup_.getPxCumulative() + pxNew);
		// Double szUl = ulGroup_.getSzNet();
		// String right = ATModelUtil.getRight(occId);
		// switch (right) {
		// case IATTrade.CALL:
		// if (szNew < 0 && szUl > 0) {
		// ulGroup_.setPxNet(ulGroup_.getPxNet() + pxNew);
		// }
		// break;
		// case IATTrade.PUT:
		// if (szNew > 0 && szUl < 0) {
		// ulGroup_.setPxNet(ulGroup_.getPxNet() + pxNew);
		// }
		// break;
		// }
		// }
		//	var rollup = new Rollup();
		//	rollup.px = pxNew;
		//	rollup.ts = tsGroupNew;
		return pxNew;
	}

	static private Double closePosition(IATTrade.Type type_, ATTrade newTrade_, ATTradeDigest ulGroup_,
			ATTradeDigest tradeGroup_) {
		String occId = newTrade_.getOccId();
		// if ("TEST  190815C00012500".equals(occId) && newTrade_.getSzTrade() == 1) {
		// 	var x = 1;
		// }
		var fraction = Math.abs(newTrade_.getSzTrade() / tradeGroup_.getSzNet());
		var pxTrade = newTrade_.getPxTradeTotal();
		var pxTradeYield = (tradeGroup_.getPxNet() * fraction) + (pxTrade != null ? pxTrade : 0);
		//		double pxTradeNetOld = tradeGroup_.getPxNet();
		//		double pxTradeNew = newTrade_.getPxNet();
		double szTradeNetOld = tradeGroup_.getSzNet();
		double szTradeNew = newTrade_.getSzTrade();
		int sign = (int) Math.signum(szTradeNew);
		Double pxTradeTotalNew = newTrade_.getPxTradeTotal();
		switch (type_) {
			case ASG:
			case EXP:
			case EXR:
				pxTradeTotalNew = null;
				break;
			case BTC:
				if (szTradeNew <= 0 || szTradeNetOld > 0 || (szTradeNetOld + szTradeNew > 0)) {
					String msg = String.format("Error Invalid sz_trd [%s / %s / %.2f / %s / %d]", occId, type_,
							szTradeNew, newTrade_.getTradeAccount(),
							newTrade_.getTsTrade2());
					throw new ATException(msg);
				}
				break;
			case STC:
				if (szTradeNew >= 0 || szTradeNetOld < 0 || (szTradeNetOld + szTradeNew < 0)) {
					String msg = String.format("Error Invalid sz_trd [%s / %s / %.2f / %s / %d]", occId, type_,
							szTradeNew, newTrade_.getTradeAccount(),
							newTrade_.getTsTrade2());
					throw new ATException(msg);
				}
				break;
			default:
				String msg = String.format("Error Invalid type [%s / %s / %s / %d]", occId, type_,
						newTrade_.getTradeAccount(), newTrade_.getTsTrade2());
				throw new ATException(msg);
		}

		double szTradeNetNew = Precision.round(szTradeNetOld + szTradeNew, 4);

		double pxTradeNetOld = tradeGroup_.getPxNet();
		double pxTradeCumOld = tradeGroup_.getPxCumulative();
		double pxTradeCumNew = pxTradeCumOld + (pxTradeTotalNew != null ? pxTradeTotalNew : 0);

		tradeGroup_.setSzNet(szTradeNetNew);
		tradeGroup_.setPxCumulative(pxTradeCumNew);

		if (szTradeNetNew == 0) {
			tradeGroup_.setPxCumulative(0.0);
			tradeGroup_.setPxNet(0.0);
			tradeGroup_.setPxTrade(0.0);
			tradeGroup_.trades.clear();
		} else {
			double pxTradeNetNew = pxTradeNetOld + (pxTradeTotalNew != null ? pxTradeTotalNew : 0);
			tradeGroup_.setPxNet(pxTradeNetNew);
			double pxTradeOld = tradeGroup_.getPxTrade();
			double pxTradeNew = pxTradeTotalNew != null
					? pxTradeOld + pxTradeTotalNew
					: szTradeNetNew * pxTradeNetOld / szTradeNetOld;
			tradeGroup_.setPxTrade(Precision.round(pxTradeNew, 4));
			Iterator<ATTrade> it = tradeGroup_.trades.iterator();
			//	double tsGroup = 0.0;
			//	double szGroup = 0.0;
			while (it.hasNext()) {
				ATTrade oldTrade = it.next();
				double szOld = oldTrade.getSzTrade();
				double sz = szOld + szTradeNew;
				if (sz == 0) {
					it.remove();
					szTradeNew = 0;
				} else if (sz > 0 && sign > 0) {
					it.remove();
					szTradeNew = sz;
				} else if (sz > 0 && sign < 0) {
					oldTrade.setSzTrade(sz);
					szTradeNew = 0;
					break;
				} else if (sz < 0 && sign > 0) {
					oldTrade.setSzTrade(sz);
					szTradeNew = 0;
					break;
				} else if (sz < 0 && sign < 0) {
					it.remove();
					szTradeNew = sz;
				}
				//long tsTrade = oldTrade.getTsTrade().getTime();
				//		tsGroup += (tsTrade * sz);
				//		szGroup += sz;
			}
			if (szTradeNew != 0) {
				String msg = String.format("Inclomplete CLOSE [%s / %s / %.2f / %d]", occId, type_, szTradeNew,
						newTrade_.getTsTrade2());
				throw new ATException(msg);
			}
			//		tradeGroup_.setTsTrade(Math.round(tsGroup / szGroup));
		}
		if (ulGroup_ != tradeGroup_) {
			double pxUlCumOld = ulGroup_.getPxCumulative();
			double pxUlCumNew = pxUlCumOld + (pxTradeTotalNew != null ? pxTradeTotalNew : 0);
			ulGroup_.setPxCumulative(Precision.round(pxUlCumNew, 4));
			ulGroup_.setPxNet(ulGroup_.getPxNet() + pxTradeYield);// + pxNetAdjust);
			// if (false) {
			// 	Double szUl = ulGroup_.getSzNet();
			// 	String right = ATModelUtil.getRight(occId);
			// 	switch (right) {
			// 		case IATTrade.CALL:
			// 			if (szTradeNew > 0 && szUl > 0) {
			// 				ulGroup_.setPxNet(ulGroup_.getPxNet() + pxNetAdjust);// + pxTotalNew);
			// 			}
			// 			break;
			// 		case IATTrade.PUT:
			// 			if (szTradeNew < 0 && szUl < 0) {
			// 				ulGroup_.setPxNet(ulGroup_.getPxNet() + pxNetAdjust);// + pxTotalNew);
			// 			}
			// 			break;
			// 	}
			// }
		}
		//var rollup = new Rollup();
		//rollup.px = pxTradeTotalNew;
		return pxTradeTotalNew;
	}

	// public static class Rollup {
	// 	Double px = null;
	// 	Long ts = null;
	// }

}
