package com.isoplane.at.portfolio.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup.TradeTrace;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.model.IATUserAsset;
import com.isoplane.at.commons.service.ATSystemStatusManager.PerformanceMetrics;
import com.isoplane.at.commons.service.IATTotalPositionListener;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.model.ATMongoPortfolio;
import com.isoplane.at.portfolio.model.ATMongoTrade;
import com.isoplane.at.portfolio.model.ATMongoUserAsset;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPortfolioMogodbTools {

	static final Logger log = LoggerFactory.getLogger(ATPortfolioMogodbTools.class);
	static final Long THROTTLE_DELAY = 5L;
	static final Gson _gson = new Gson();

	static final SimpleDateFormat _df = ATFormats.DATE_yyyy_MM_dd.get();

	boolean _isStreaming;

	private ATMongoDao _dao;
	private String _tradeTable;
	private String _tradeCumulatorTable;
	private String _tradeLedgerTable;
	private String _assetTable;
	private String _portfolioTable;

	//	private IATTradeListener _listener;
	private IATTotalPositionListener _totalListener;
	private Map<String, Set<String>> _activeUserMap;
	//	private ATUserPositionRefresher _userPositionRefresher;
	private PerformanceMetrics _perfMetrics;

	// NOTE: Index on _atid, user, active
	public ATPortfolioMogodbTools(boolean isStreaming_, /*IATTradeListener listener_,*/
			IATTotalPositionListener totalListener_) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;
		_isStreaming = isStreaming_;
		//		_listener = listener_;
		_totalListener = totalListener_;
		_assetTable = ATMongoTableRegistry.getAssetTable();
		_portfolioTable = ATMongoTableRegistry.getPortfolioTable();
		_tradeTable = ATMongoTableRegistry.getTradeTable();
		_tradeCumulatorTable = ATMongoTableRegistry.getTradeCumulatorTable();
		_tradeLedgerTable = ATMongoTableRegistry.getTradeLedgerTable();
		_perfMetrics = new PerformanceMetrics(this.getClass().getSimpleName(), "dao.portfolio.perf_metrics");
		if (totalListener_ != null) {
			_activeUserMap = new ConcurrentHashMap<>();
			//			_userPositionRefresher = new ATUserPositionRefresher();
		}
	}

	public ATPortfolioMogodbTools init() {
		_dao = new ATMongoDao();
		if (_isStreaming) {
			// _timer = new Timer(String.format("%s.timer", ATPortfolioMogodbTools.class.getSimpleName()), true);
			//			monitorTrades();
			log.error(String.format("#################################"));
			log.error(String.format("#                               #"));
			log.error(String.format("# REMOVE MONGO REACTIVE DRIVER! #"));
			log.error(String.format("#                               #"));
			log.error(String.format("#################################"));
		}
		if (_activeUserMap != null) {
			IATWhereQuery query = ATPersistenceRegistry.query().ne(IATAssetConstants.SIZE_ACTIVE, 0);
			List<IATTrade> activeTrades = this.getTrades(query);
			for (IATTrade trade : activeTrades) {
				String occId = trade.getOccId();
				String userId = trade.getTradeUserId();
				if (!StringUtils.isBlank(occId)) {
					addToActiveUserMap(occId, userId);
				} else {
					log.error(String.format("Invalid trade (OCCID==null) [%s]: %s", userId, trade));
				}
			}
			log.info(String.format("Initialized [%d] active positions", _activeUserMap.size()));
		}
		this._perfMetrics.collectMetrics(true);
		return this;
	}

	private void addToActiveUserMap(String occId_, String userId_) {
		Set<String> users = _activeUserMap.get(occId_);
		if (users == null) {
			users = new HashSet<>();
			_activeUserMap.put(occId_, users);
		}
		users.add(userId_);
		String underlyingId = occId_.length() > 10 ? ATModelUtil.getUnderlyingOccId(occId_) : null;
		if (underlyingId != null) {
			Set<String> underlyingUsers = _activeUserMap.get(underlyingId);
			if (underlyingUsers == null) {
				underlyingUsers = new HashSet<>();
				_activeUserMap.put(underlyingId, underlyingUsers);
			}
			underlyingUsers.add(userId_);
		}
	}

	@SuppressWarnings("unchecked")
	public List<IATTrade> getTrades(IATWhereQuery query_) {
		IATWhereQuery query = query_.and().ne(IATMongo.DISABLED, true);
		List<ATMongoTrade> trades = _dao.query(_tradeTable, query, ATMongoTrade.class);
		for (ATMongoTrade trade : trades) {
			String id = trade.getAtId();
			trade.remove(IATMongo.MONGO_ID);
			trade.put(IATLookupMap.AT_ID, id);
			// if("T4".equals(trade.getOccId())) {
			// log.info("T4");
			// }
		}
		return (List<IATTrade>) (Object) trades;
	}

	public List<IATPortfolio> getPortfolio(IATWhereQuery query_) {
		IATWhereQuery query = query_; // .and().ne(IATMongo.DISABLED, true);
		List<ATMongoPortfolio> portfolio = _dao.query(_portfolioTable, query, ATMongoPortfolio.class);
		// Set<IATPortfolio> apf = portfolio.stream().filter(p->p.getOccId().startsWith("AMD")).collect(Collectors.toSet());

		for (ATMongoPortfolio prt : portfolio) {
			String id = prt.getAtId();
			prt.remove(IATMongo.MONGO_ID);
			prt.put(IATLookupMap.AT_ID, id);
			// if ("AMD".equals(prt.getUnderlying())) {
			// log.info(prt.getUnderlying());
			// }
		}
		@SuppressWarnings("unchecked")
		List<IATPortfolio> result = (List<IATPortfolio>) (Object) portfolio;
		return result;
	}

	public Map<String, Double[]> getTradeAggregate(IATWhereQuery query_, String groupId_) {
		Map<String, Double[]> result = _dao.aggregateSum(_tradeTable, query_, groupId_,
				IATAssetConstants.PX_TRADE_TOTAL,
				IATAssetConstants.SIZE_TRADE);
		return result;
	}

	public boolean disableTrades(IATWhereQuery query_) {
		Map<String, Object> updates = new HashMap<>();
		updates.put(IATMongo.DISABLED, true);
		boolean result = _dao.updateValues(_tradeTable, query_, updates);
		return result;
	}

	public long deleteTrades(String userId_) {
		IATWhereQuery deleteQuery1 = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
		IATWhereQuery deleteQuery2 = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		long result = _dao.delete(_tradeTable, deleteQuery1);
		_dao.delete(_portfolioTable, deleteQuery1);
		_dao.delete(_assetTable, deleteQuery2);
		return result;
	}

	public Set<String> getOccIds(IATWhereQuery query_) {
		Set<String> result = _dao.distinct(_tradeTable, query_, IATAssetConstants.OCCID, String.class);
		return result;
	}

	public Set<String> getAffiliatedUserIds(String symbol_) {
		this._perfMetrics.start("getAffiliatedUserIds");
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.SIZE, 0).and()
				.eq(IATAssetConstants.OCCID_UNDERLYING, symbol_);
		Set<String> userIds = _dao.distinct(_portfolioTable, query, IATAssetConstants.TRADE_USER, String.class);
		this._perfMetrics.stop("getAffiliatedUserIds");
		return userIds;
	}

	public Set<String> getOwnerIds(String occId_) {
		if (_totalListener != null) {
			Set<String> users = _activeUserMap.get(occId_);
			return users;
		} else {
			log.warn(String.format("getOwnerIds SLOW mode"));
			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, occId_);
			Set<String> userIds = _dao.distinct(_tradeTable, query, IATAssetConstants.TRADE_USER, String.class);
			return userIds;
		}
	}

	public void updatePortfolios(String userId_, String account_, String ul_, Collection<IATPortfolio> data_) {
		if (data_ == null || data_.isEmpty() || StringUtils.isBlank(userId_) || StringUtils.isBlank(account_)
				|| StringUtils.isBlank(ul_)) {
			log.warn(String.format("Missing attributes [%s,%s,%s]: %s", userId_, account_, ul_, data_));
			return;
		}
		IATWhereQuery delQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_).and()
				.eq(IATAssetConstants.TRADE_ACCOUNT, account_).and().eq(IATAssetConstants.OCCID_UNDERLYING, ul_);
		long delCount = _dao.delete(_portfolioTable, delQuery);
		log.debug(String.format("Deleted portfolio [%s:%s:%s]: %d", userId_, account_, ul_, delCount));
		for (IATPortfolio ipf : data_) {
			ATMongoPortfolio mpf = new ATMongoPortfolio(ipf);
			boolean result = _dao.updateValues(_portfolioTable, mpf.getAtId(), mpf, true);
			log.debug(String.format("update [%s/%b]", mpf.getAtId(), result));
		}
	}

	public boolean updateTrade(String userId_, String key_, IATTrade trade_) {
		Map<String, Object> data = trade_.getAsMap();
		Iterator<Entry<String, Object>> i = data.entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> entry = i.next();
			if (entry.getValue() == null) {
				i.remove();
			}
		}
		data.remove(IATMongo.MONGO_ID);
		data.remove(IATLookupMap.AT_ID);
		String sym = trade_.getOccId();
		if (sym.length() > 6) {
			sym = sym.substring(0, 6).trim();
		}
		data.put(IATTrade.UNDERLYING, sym);
		boolean result = _dao.updateValues(_tradeTable, key_, data, true);
		log.debug(String.format("update [%s/%b]", key_, result));
		return result;
	}

	/**
	 * Updates given symbols in db.
	 * 
	 * @param userId_
	 *            - Only update trades per user
	 * @param trades_
	 *            - Updated trades
	 * @param isIncremental_
	 *            - Does update consist of all user trades (false; delete others) or is it a simple update (true)?
	 */
	public void updateTrades(String userId_, Collection<IATTrade> trades_, boolean isIncremental_, boolean isLegacy_) {
		if ((trades_ == null || trades_.isEmpty()) && StringUtils.isBlank(userId_)) {
			log.warn(String.format("No user or trades [%s]: %s", userId_, trades_));
			return;
		}
		String ttype = isLegacy_ ? "Legacy" : "Existing";
		try {
			String startsWith = isLegacy_ ? "L" : "T";
			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_).and()
					.startsWith(ATMongoTrade.MONGO_ID,
							startsWith);
			List<IATTrade> mongoTrades = getTrades(query);
			log.debug(String.format("Loaded [%d] %s trades [%s]", mongoTrades.size(), ttype, userId_));

			List<IATTrade> newTrades = new ArrayList<>(trades_);
			if (newTrades.removeAll(mongoTrades))
				log.debug(String.format("New %s trades [%s / %d]", ttype, userId_, newTrades.size()));
			List<IATTrade> oldTrades = isIncremental_ ? new ArrayList<>() : new ArrayList<>(mongoTrades);
			if (oldTrades.removeAll(trades_))
				log.info(String.format("%s trades to remove or update [%s / %d]", ttype, userId_, oldTrades.size()));

			// 1. Remove old
			if (!oldTrades.isEmpty()) {
				long delCount = 0;
				for (IATTrade old : oldTrades) {
					if (!userId_.equals(old.getTradeUserId())) {
						log.error(String.format("User id mismatch on %s trade delete [%s / %s]", ttype, userId_, old));
						continue;
					}
					String delId = ((ATMongoTrade) old).getAtId();
					IATWhereQuery delQuery = ATPersistenceRegistry.query().eq(ATMongoTrade.MONGO_ID, delId);
					delCount += _dao.delete(_tradeTable, delQuery);
				}
				log.info(String.format("Deleted [%d] %s trades [%s]", delCount, ttype, userId_));
			}
			// 2. Insert new
			if (!newTrades.isEmpty()) {
				List<ATMongoTrade> inserts = new ArrayList<>();
				for (IATTrade nt : newTrades) {
					if (!userId_.equals(nt.getTradeUserId())) {
						log.error(String.format("User id mismatch on %s trade insert [%s / %s]", ttype, userId_, nt));
						continue;
					}
					inserts.add(new ATMongoTrade(nt));
				}
				long insCount = _dao.insert(_tradeTable, inserts.toArray(new ATMongoTrade[0]));
				log.info(String.format("Inserted [%d] %s trades [%s]", insCount, ttype, userId_));
			}
		} catch (Exception ex) {
			log.error(String.format("Failed %s trade update [%s]", ttype, userId_), ex);
		}
	}

	public int saveTradeLedger(List<ATTrade> trades_) {
		if (trades_ == null || trades_.isEmpty())
			return 0;

		List<Document> inserts = new ArrayList<>();
		for (ATTrade trade : trades_) {
			ATMongoTrade t = new ATMongoTrade(trade);
			IATTrade.cleanLedger(t);
			while (t.values().remove(null))
				;
			inserts.add(t);
		}
		long count = _dao.fastBulkInsert(_tradeLedgerTable, inserts);
		return (int) count;
	}

	public List<ATTrade> getTradeLedger(IATWhereQuery query_) {
		List<ATTrade> trades = _dao.query(_tradeLedgerTable, query_, new Function<Document, ATTrade>() {

			@Override
			public ATTrade apply(Document doc_) {
				String id = doc_.getString(IATMongo.MONGO_ID);
				doc_.put(IATLookupMap.AT_ID, id);
				ATTrade trade = new ATTrade(doc_);
				return trade;
			}
		});
		return trades;
	}

	public boolean saveTradeCumulative(ATTrade trade_) {
		if (trade_ == null)
			return false;

		String occId = trade_.getOccId();
		IATWhereQuery deleteQ = ATPersistenceRegistry.query().eq(IATTrade.OCCID, occId);
		_dao.delete(_tradeCumulatorTable, deleteQ);
		Double pxCum = trade_.getPxNet();
		if (pxCum == null)
			return true;
		ATMongoTrade dbTrade = new ATMongoTrade(trade_);
		long count = _dao.fastBulkInsert(_tradeCumulatorTable, Arrays.asList(dbTrade));
		return count == 1;
	}

	public ATTrade getTradeCumulative(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_).and().eq(IATTrade.OCCID,
				symbol_);
		List<ATTrade> trades = _dao.query(_tradeCumulatorTable, query, new Function<Document, ATTrade>() {

			@Override
			public ATTrade apply(Document doc_) {
				String id = doc_.getString(IATMongo.MONGO_ID);
				doc_.put(IATLookupMap.AT_ID, id);
				ATTrade trade = new ATTrade(doc_);
				trade.remove(IATAssetConstants.TRADE_TYPE);
				return trade;
			}
		});
		ATTrade trade = trades != null && trades.size() == 1 ? trades.get(0) : null;
		return trade;
	}

	public int saveTradeDigest(String userId_, String account_, String symbol_, Collection<ATTradeDigest> digests_) {
		if (StringUtils.isBlank(userId_) || StringUtils.isBlank(symbol_) || StringUtils.isBlank(account_)) {
			log.error(String.format("saveTradeDigest Missing userId, symbol or account"));
			return -1;
		}
		String table = ATMongoTableRegistry.getTradeDigestTable();
		IATWhereQuery deleteQ = ATPersistenceRegistry.query()
				.eq(IATTrade.TRADE_USER, userId_).and().eq(IATTrade.UNDERLYING, symbol_).and()
				.eq(IATTrade.TRADE_ACCOUNT, account_);
		long deleteCount = _dao.delete(table, deleteQ);
		log.debug(String.format("saveTradeDigest Deleted  [%s:%s:%s]: %d", userId_, account_, symbol_, deleteCount));

		if (digests_ == null || digests_.isEmpty())
			return 0;

		ArrayList<Document> insertList = new ArrayList<>();
		for (ATTradeDigest trade : digests_) {
			String userId = trade.getTradeUserId();
			String occId = trade.getOccId();
			String symbol = ATModelUtil.getSymbol(occId);
			String account = trade.getAccount();
			if (!userId_.equals(userId) || !symbol_.equals(symbol) || !account_.equals(account)) {
				log.error(String.format("saveTradeDigest Only one User/Underlying/Account supported per call"));
				return -1;
			}
			String idStr = String.format("%s:%s:%s", userId, account, occId);
			Document doc = new Document(trade);
			doc.append(IATTrade.UNDERLYING, symbol_);
			doc.append(IATMongo.MONGO_ID, String.format("CT-%d", idStr.hashCode()));
			insertList.add(doc);
		}
		long insertCount = _dao.fastBulkInsert(table, insertList);
		log.debug(String.format("saveTradeDigest Inserted [%s:%s:%s]: %d", userId_, account_, symbol_, insertCount));
		return (int) insertCount;
	}

	public List<ATTradeDigest> getTradeDigest(IATWhereQuery query_) {
		String table = ATMongoTableRegistry.getTradeDigestTable();
		List<ATTradeDigest> digests = _dao.query(table, query_, new Function<Document, ATTradeDigest>() {

			@Override
			public ATTradeDigest apply(Document doc_) {
				String id = doc_.getString(IATMongo.MONGO_ID);
				doc_.put(IATLookupMap.AT_ID, id);
				ATTradeDigest digest = new ATTradeDigest(doc_);
				return digest;
			}
		});
		return digests;
	}

	public int saveTradeGroups(String userId_, String account_, String symbol_, Collection<ATTradeGroup> groups_) {
		if (StringUtils.isBlank(userId_) || StringUtils.isBlank(symbol_) || StringUtils.isBlank(account_)) {
			log.error(String.format("saveTradeGroups Missing userId, symbol or account"));
			return -1;
		}
		String table = ATMongoTableRegistry.getTradeGroupTable();
		IATWhereQuery deleteQ = ATPersistenceRegistry.query()
				.eq(IATTrade.TRADE_USER, userId_).and().eq(IATTrade.UNDERLYING, symbol_).and()
				.eq(IATTrade.TRADE_ACCOUNT, account_);
		long deleteCount = _dao.delete(table, deleteQ);
		log.debug(String.format("saveTradeGroups Deleted  [%s:%s:%s]: %d", userId_, account_, symbol_, deleteCount));

		if (groups_ == null || groups_.isEmpty())
			return 0;

		ArrayList<Document> insertList = new ArrayList<>();
		for (ATTradeGroup group : groups_) {
			String userId = group.getTradeUserId();
			String symbol = group.getUnderlying();
			String account = group.getAccount();
			if (!userId_.equals(userId) || !symbol_.equals(symbol) || !account_.equals(account)) {
				log.error(String.format("saveTradeGroups Only one User/Underlying/Account supported per call"));
				return -1;
			}
			String idStr = group.getAtId();
			Document doc = new Document(group);
			doc.append(IATTrade.UNDERLYING, symbol_);
			doc.append(IATMongo.MONGO_ID, String.format("CT-%d", idStr.hashCode()));
			ATTradeGroup.removeNonPersistable(doc);
			insertList.add(doc);
		}
		long insertCount = _dao.fastBulkInsert(table, insertList);
		log.debug(String.format("saveTradeGroups Inserted [%s:%s:%s]: %d", userId_, account_, symbol_, insertCount));
		return (int) insertCount;
	}

	public List<ATTradeGroup> getTradeGroups(IATWhereQuery query_) {
		String table = ATMongoTableRegistry.getTradeGroupTable();
		List<ATTradeGroup> groups = _dao.query(table, query_, new Function<Document, ATTradeGroup>() {

			@Override
			public ATTradeGroup apply(Document doc_) {
				String id = doc_.getString(IATMongo.MONGO_ID);
				doc_.put(IATLookupMap.AT_ID, id);
				ATTradeGroup group = new ATTradeGroup(doc_);
				return group;
			}
		});
		return groups;
	}

	public long saveTradeTrace(ATTradeTraceGroup data_) {
		var id = data_ != null ? data_.getId() : null;
		var userId = data_ != null ? data_.getUserId() : null;
		var symbols = data_ != null ? data_.getSymbols() : null;
		var items = data_ != null ? data_.getItems() : null;
		if (id == null || StringUtils.isBlank(userId) || symbols == null || symbols.size() == 0 || items == null
				|| items.length == 0) {
			log.error(String.format("saveTradeTrace: Incomplete data"));
			return -1;
		}
		String table = ATMongoTableRegistry.getTradeTraceTable();
		var doc = new Document();
		for (String key : data_.keySet()) {
			var value = data_.get(key);
			if (IATLookupMap.AT_ID.equals(key)) {
				doc.append(IATMongo.MONGO_ID, userId + ":" + value);
			}
			doc.append(key, value);
		}
		var docItems = new ArrayList<Document>();
		for (TradeTrace item : items) {
			var docItem = new Document();
			for (String key : item.keySet()) {
				var value = item.get(key);
				docItem.append(key, value);
			}
			docItems.add(docItem);
		}
		doc.append(IATAssetConstants.DATA, docItems);
		long insertCount = _dao.fastBulkInsert(table, Arrays.asList(doc));
		return insertCount;
	}

	public List<ATTradeTraceGroup> getTradeTraceGroup(IATWhereQuery query_) {
		String table = ATMongoTableRegistry.getTradeTraceTable();
		List<ATTradeTraceGroup> traceGroups = _dao.query(table, query_,
				new Function<Document, ATTradeTraceGroup>() {

					@Override
					public ATTradeTraceGroup apply(Document doc_) {
						var traceGroup = new ATTradeTraceGroup();
						for (String key : doc_.keySet()) {
							var value = doc_.get(key);
							if (IATAssetConstants.DATA.equals(key)) {
								@SuppressWarnings("unchecked")
								var docItems = (List<Document>) value;
								var items = new TradeTrace[docItems.size()];
								for (var i = 0; i < docItems.size(); i++) {
									var docItem = docItems.get(i);
									var item = new TradeTrace();
									item.putAll(docItem);
									items[i] = item;
								}
								traceGroup.setItems(items);
							} else {
								traceGroup.put(key, value);
							}
						}
						return traceGroup;
					}
				});
		return traceGroups;
	}

	public long deleteTradeTrace(IATWhereQuery query_) {
		String table = ATMongoTableRegistry.getTradeTraceTable();
		long count = _dao.delete(table, query_);
		return count;
	}

	public boolean archiveTradeTraceGroup(String traceId_) {
		var table = ATMongoTableRegistry.getTradeTraceTable();
		var archiveTable = String.format("%s_archive", table);
		var query = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, traceId_);
		var dataList = _dao.query(table, query, ATPersistentLookupBase.class);
		if (dataList == null || dataList.isEmpty()) {
			log.error(String.format("archiveTradeTraceGroup Error: Id not found [%s]", traceId_));
			return false;
		}
		var data = dataList.get(0);
		_dao.delete(archiveTable, query);
		var result = _dao.insert(archiveTable, data);
		if (result) {
			_dao.delete(table, query);
		}
		return result;
	}

	public void shutdown() {
		log.debug(String.format("shutdown"));
	}

	private long getActiveUserMapItemCount() {
		AtomicLong size = new AtomicLong(0);
		_activeUserMap.values().forEach(u_ -> size.addAndGet(u_.size()));
		return size.get();
	}

	public Map<String, String> getMonitoringStatus() {
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("activeUserMapSize", _activeUserMap.size() + "");
		statusMap.put("activeUserMapItemCount", getActiveUserMapItemCount() + "");
		// statusMap.put("actieUserMemory", getActieUserMemory());
		return statusMap;
	}

	public long expireOptions() {
		String nowDS6 = ATFormats.DATE_yyMMdd.get().format(new Date());
		IATWhereQuery query = ATPersistenceRegistry.query().ne(IATAssetConstants.SIZE_ACTIVE, 0).and().lt(
				IATAssetConstants.EXPIRATION_DATE_STR,
				nowDS6);
		List<IATTrade> expTrades = getTrades(query);
		long count = 0;
		for (IATTrade t : expTrades) {
			log.info(String.format("Found expired option [%s - %s]", t.getTradeUserId(), t.getOccId()));
			// if (false) {
			// ATMongoTrade trade = (ATMongoTrade) t;
			// trade.setSzTradeActive(0.0);
			// String userId = trade.getTradeUserId();
			// String key = trade.getAtId();
			// if (updateTrade(userId, key, trade)) {
			// count++;
			// }
			// }
		}
		List<IATTrade> errorTrades = getTrades(
				ATPersistenceRegistry.query().notNull(IATAssetConstants.EXPIRATION_DATE_STR));
		for (IATTrade trd : errorTrades) {
			ATMongoTrade trade = (ATMongoTrade) trd;
			String ul = trade.getUnderlying();
			String exp = trade.getString(IATAssetConstants.EXPIRATION_DATE_STR);
			String keyStart = String.format("%-6s%s", ul, exp);
			String occId = trade.getOccId();
			if (!occId.startsWith(keyStart)) {
				log.error(String.format("Trade inconsistency: %s -> %s", occId, exp));
			}
		}
		return count;
	}

	public List<IATUserAsset> getAssets(String userId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.USER_ID, userId_);
		List<ATMongoUserAsset> mongoAssets = _dao.query(_assetTable, query, ATMongoUserAsset.class);
		@SuppressWarnings("unchecked")
		List<IATUserAsset> result = (List<IATUserAsset>) (Object) mongoAssets;
		return result;
	}

	public Long saveAssets(Collection<ATUserAsset> assets_) {
		List<Document> docs = new ArrayList<>();
		for (ATUserAsset asset : assets_) {
			// int hash = (asset.getDateStr() + asset.getOccId()).hashCode();
			String id = String.format("%s:%s:%s", asset.getUserId(), asset.getDateStr(), asset.getOccId());
			Document doc = new Document(asset);
			doc.put(IATMongo.MONGO_ID, id);
			docs.add(doc);
		}
		long count = _dao.fastBulkInsert(_assetTable, docs);
		return count;
	}

	public long deletePositions(IATWhereQuery query_) {
		if (query_ == null)
			return -1;
		var table = ATMongoTableRegistry.getPositionTable();
		var count = _dao.delete(table, query_);
		return count;
	}

	public long savePositions(Collection<ATPosition> positions_) {
		if (positions_ == null || positions_.isEmpty()) {
			log.info(String.format("Nothing to save"));
			return 0;
		}
		var tsIns = System.currentTimeMillis();
		var table = ATMongoTableRegistry.getPositionTable();
		List<Document> docs = new ArrayList<>();
		for (ATPosition pos : positions_) {
			var doc = new Document(pos);
			var idStr = String.format("%s:%s:%s", pos.getOccId(), pos.getUserId(), pos.getAccountId());
			doc.put(IATMongo.MONGO_ID, idStr);
			doc.put(IATLookupMap.INSERT_TIME, tsIns);
			docs.add(doc);
		}
		long count = _dao.fastBulkInsert(table, docs);
		return count;
	}

	public List<ATPosition> queryPositions(IATWhereQuery query_) {
		if (query_ == null)
			return null;
		var table = ATMongoTableRegistry.getPositionTable();
		var positions = _dao.query(table, query_, new Function<Document, ATPosition>() {

			@Override
			public ATPosition apply(Document doc_) {
				var position = new ATPosition(doc_);
				var id = position.remove(IATMongo.MONGO_ID);
				position.put(IATLookupMap.AT_ID, id);
				position.remove(IATLookupMap.INSERT_TIME);
				return position;
			}
		});
		return positions;
	}


	public <T> Set<T> distinct(String table_, IATWhereQuery query_, String field_, Class<T> class_) {
		return _dao.distinct(table_, query_, field_, class_);
	}

}
