package com.isoplane.at.portfolio.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.util.ATModelUtil;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATTradeGrouper {

	static final Logger log = LoggerFactory.getLogger(ATTradeGrouper.class);

	static private Comparator<ATTradeDigest> OccIdComparator = (ATTradeDigest a_, ATTradeDigest b_) -> a_.getOccId()
			.compareTo(b_.getOccId());

	static public ArrayList<ATTradeGroup> groupTrades(String userId_, String account_, String symbol_,
			Collection<ATTradeDigest> digests_) {
		ArrayList<ATTradeGroup> groups = new ArrayList<>();
		if (digests_ == null || digests_.isEmpty())
			return groups;
		ArrayList<ATTradeDigest> cl = new ArrayList<>();
		ArrayList<ATTradeDigest> cs = new ArrayList<>();
		ArrayList<ATTradeDigest> pl = new ArrayList<>();
		ArrayList<ATTradeDigest> ps = new ArrayList<>();
		ATTradeDigest eqt = null;
		for (ATTradeDigest digest : digests_) {
			String occId = digest.getOccId();
			if (occId.length() < 10) {
				eqt = digest;
				continue;
			}
			String right = ATModelUtil.getRight(occId);
			Double sz = digest.getSzEff();
			ArrayList<ATTradeDigest> calls = sz > 0 ? cl : cs;
			ArrayList<ATTradeDigest> puts = sz > 0 ? pl : ps;
			switch (right) {
				case IATTrade.CALL:
					calls.add(digest);
					break;
				case IATTrade.PUT:
					puts.add(digest);
					break;
				default:
					log.error(String.format("Invalid right [%s]: %s", right, occId));
					break;
			}
		}
		// if ("CLOV".equals(symbol_)) {
		// log.debug(String.format("%s", symbol_));
		// }
		ArrayList<ATTradeGroup> vcs = identifyVerticalSpreadGroups(cs, cl, IATTrade.CALL);
		groups.addAll(vcs);
		ArrayList<ATTradeGroup> vps = identifyVerticalSpreadGroups(ps, pl, IATTrade.PUT);
		groups.addAll(vps);
		ArrayList<ATTradeGroup> ccs = identifyCoveredCallGroups(eqt, cs);
		groups.addAll(ccs);
		List<List<ATTradeDigest>> lists = eqt != null ? Arrays.asList(cs, cl, ps, pl, Arrays.asList(eqt))
				: Arrays.asList(cs, cl, ps, pl);
		ArrayList<ATTradeGroup> rms = identifyRemainingGroups(lists);
		groups.addAll(rms);

		groups.sort((a, b) -> a.getAtId().compareTo(b.getAtId()));

		log.debug(String.format("groupTrades [%s:%s:%s]: %d", userId_, account_, symbol_, groups.size()));
		return groups;
	}

	// static public ArrayList<ATTradeGroup> groupTrades(String userId_, String account_, String symbol_,
	// 		Collection<ATPosition> positions_) {
	// 	ArrayList<ATTradeGroup> groups = new ArrayList<>();
	// 	if (digests_ == null || digests_.isEmpty())
	// 		return groups;
	// 	ArrayList<ATTradeDigest> cl = new ArrayList<>();
	// 	ArrayList<ATTradeDigest> cs = new ArrayList<>();
	// 	ArrayList<ATTradeDigest> pl = new ArrayList<>();
	// 	ArrayList<ATTradeDigest> ps = new ArrayList<>();
	// 	ATTradeDigest eqt = null;
	// 	for (ATTradeDigest digest : digests_) {
	// 		String occId = digest.getOccId();
	// 		if (occId.length() < 10) {
	// 			eqt = digest;
	// 			continue;
	// 		}
	// 		String right = ATModelUtil.getRight(occId);
	// 		Double sz = digest.getSzEff();
	// 		ArrayList<ATTradeDigest> calls = sz > 0 ? cl : cs;
	// 		ArrayList<ATTradeDigest> puts = sz > 0 ? pl : ps;
	// 		switch (right) {
	// 			case IATTrade.CALL:
	// 				calls.add(digest);
	// 				break;
	// 			case IATTrade.PUT:
	// 				puts.add(digest);
	// 				break;
	// 			default:
	// 				log.error(String.format("Invalid right [%s]: %s", right, occId));
	// 				break;
	// 		}
	// 	}
	// 	// if ("CLOV".equals(symbol_)) {
	// 	// log.debug(String.format("%s", symbol_));
	// 	// }
	// 	ArrayList<ATTradeGroup> vcs = identifyVerticalSpreadGroups(cs, cl, IATTrade.CALL);
	// 	groups.addAll(vcs);
	// 	ArrayList<ATTradeGroup> vps = identifyVerticalSpreadGroups(ps, pl, IATTrade.PUT);
	// 	groups.addAll(vps);
	// 	ArrayList<ATTradeGroup> ccs = identifyCoveredCallGroups(eqt, cs);
	// 	groups.addAll(ccs);
	// 	List<List<ATTradeDigest>> lists = eqt != null ? Arrays.asList(cs, cl, ps, pl, Arrays.asList(eqt))
	// 			: Arrays.asList(cs, cl, ps, pl);
	// 	ArrayList<ATTradeGroup> rms = identifyRemainingGroups(lists);
	// 	groups.addAll(rms);

	// 	groups.sort((a, b) -> a.getAtId().compareTo(b.getAtId()));

	// 	log.debug(String.format("groupTrades [%s:%s:%s]: %d", userId_, account_, symbol_, groups.size()));
	// 	return groups;
	// }

	static private ArrayList<ATTradeGroup> identifyVerticalSpreadGroups(ArrayList<ATTradeDigest> shorts_,
			ArrayList<ATTradeDigest> longs_, String right_) {
		ArrayList<ATTradeGroup> groups = new ArrayList<>();
		if (shorts_ == null || shorts_.isEmpty() || longs_ == null || longs_.isEmpty())
			return groups;
		ATTradeDigest first = shorts_.get(0);
		String account = first.getAccount();
		String userId = first.getTradeUserId();
		String symbol = ATModelUtil.getSymbol(first.getOccId());
		String strt_1 = IATTrade.CALL.equals(right_) ? IATTrade.STRATEGY_LVCS : IATTrade.STRATEGY_SVPS;
		String strt_2 = IATTrade.CALL.equals(right_) ? IATTrade.STRATEGY_SVCS : IATTrade.STRATEGY_LVPS;
		shorts_.sort(OccIdComparator);
		longs_.sort(OccIdComparator);
		int idx = 0;
		for (ATTradeDigest shortOld : shorts_) {
			double szShortOld = shortOld.getSzEff();
			if (szShortOld >= 0)
				continue;
			String idShort = shortOld.getOccId();
			long tsExpShort = ATModelUtil.getExpiration(idShort).getTime();
			double pxStrkShort = ATModelUtil.getStrike(idShort);
			double pxShortOld = shortOld.getPxNet();
			// 1. Find by size and pull into 1st position to create matching pairs.
			ArrayList<ATTradeDigest> longs = longs_;
			final double szMatch = szShortOld;
			Optional<ATTradeDigest> lMatch = longs.stream().filter(l_ -> l_.getSzEff() + szMatch == 0).findFirst();
			if (lMatch.isPresent()) {
				ATTradeDigest longOld = lMatch.get();
				longs.remove(longOld);
				longs.add(0, longOld);
			}
			double shortTs = 0;
			double shortSz = 0;
			double longTs = 0;
			double longSz = 0;
			for (ATTrade trade : shortOld.trades) {
				var szTrd = Math.abs(trade.getSzTrade());
				shortTs += (trade.getTsTrade().getTime() * szTrd);
				shortSz += szTrd;
			}
			// 2. Assemble
			for (ATTradeDigest longOld : longs) {
				double szLongOld = longOld.getSzEff();
				String idLong = longOld.getOccId();
				long tsExpLong = ATModelUtil.getExpiration(idLong).getTime();
				if (szLongOld <= 0 || szShortOld >= 0 || tsExpShort != tsExpLong)
					continue;
				for (ATTrade trade : longOld.trades) {
					var szTrd = Math.abs(trade.getSzTrade());
					longTs += (trade.getTsTrade().getTime() * szTrd);
					longSz += szTrd;
				}
				idx++;
				double pxStrkLong = ATModelUtil.getStrike(idLong);
				double pxLongOld = longOld.getPxNet();
				double sz = szShortOld + szLongOld;
				double newRatioShort = 0;
				double newRatioLong = 0;
				ATTradeDigest shortNew = new ATTradeDigest(shortOld);
				ATTradeDigest longNew = new ATTradeDigest(longOld);
				if (sz == 0 /* Short == Long */ ) {
					newRatioShort = 1;
					newRatioLong = 1;
					szShortOld = 0.0;
					pxShortOld = 0.0;
					szLongOld = 0.0;
					pxLongOld = 0.0;
				} else if (sz < 0 /* Short > Long */) {
					newRatioLong = 1;
					newRatioShort = Math.abs(szLongOld / shortOld.getSzNet());
					double pxNetShortUnit = Precision.round(pxShortOld / szShortOld, 4);
					shortNew.setSzEff(-szLongOld);
					shortNew.setPxNet(Precision.round(-szLongOld * pxNetShortUnit, 2));
					szShortOld = sz;
					pxShortOld = Precision.round(sz * pxNetShortUnit, 2);
					szLongOld = 0.0;
					pxLongOld = 0.0;
				} else if (sz > 0 /* Long > Short */) {
					newRatioShort = 1;
					newRatioLong = Math.abs(szShortOld / longOld.getSzNet());
					double pxNetLongUnit = Precision.round(pxLongOld / szLongOld, 4);
					longNew.setSzEff(-szShortOld);
					longNew.setPxNet(Precision.round(-szShortOld * pxNetLongUnit, 2));
					szShortOld = 0.0;
					pxShortOld = 0.0;
					szLongOld = sz;
					pxLongOld = Precision.round(sz * pxNetLongUnit, 2);
				}
				shortOld.setSzEff(szShortOld);
				shortOld.setPxNet(pxShortOld);
				longOld.setSzEff(szLongOld);
				longOld.setPxNet(pxLongOld);

				ATTradeGroup group = new ATTradeGroup(userId, account, symbol);
				group.setTsExp(tsExpShort);
				String strt = pxStrkShort > pxStrkLong ? strt_1 : strt_2;
				group.setStrategy(strt);
				String id = String.format("%s-%s-%s-%s-%d", userId, account, symbol, strt, idx);
				group.setAtId(String.format("TG-%d", id.hashCode()));
				double pxGroup = Precision.round(shortNew.getPxNet() + longNew.getPxNet(), 2);
				group.setPxNet(pxGroup);

				Map<String, Double> ratios = new TreeMap<>();
				double szShortNew = shortNew.getSzEff();
				if (szShortNew != 0) {
					ratios.put(idShort, newRatioShort);
				}
				double szLongNew = longNew.getSzEff();
				if (szLongNew != 0) {
					ratios.put(idLong, newRatioLong);
				}
				if (!ratios.isEmpty()) {
					group.setRatios(ratios);
					long tsTrd = Math.round((shortTs + longTs) / (shortSz + longSz));
					group.setTsTrade(tsTrd);
					groups.add(group);
					switch (strt) {
						case IATTrade.STRATEGY_LVCS:
						case IATTrade.STRATEGY_LVPS: {
							double riskAbs = Math.abs(shortNew.getPxNet() + longNew.getPxNet());
							group.setRiskAbs(Precision.round(riskAbs, 2));
							break;
						}
						case IATTrade.STRATEGY_SVCS:
						case IATTrade.STRATEGY_SVPS: {
							double px = Math.abs(shortNew.getPxNet() + longNew.getPxNet());
							double riskAbs = Math.abs(szLongNew * 100 * (pxStrkShort - pxStrkLong)) - px;
							group.setRiskAbs(Precision.round(riskAbs, 2));
							break;
						}
					}
				}
				if (log.isTraceEnabled()) {
					log.trace(String.format("idSP: %s", group.getAccount()));
				}
			}
		}
		return groups;
	}

	// static private void evaluateSpread() {

	// }

	static private ArrayList<ATTradeGroup> identifyCoveredCallGroups(ATTradeDigest eqt_,
			ArrayList<ATTradeDigest> calls_) {
		ArrayList<ATTradeGroup> groups = new ArrayList<>();
		Double szEqtOld = eqt_ != null ? eqt_.getSzEff() : null;
		if (szEqtOld == null || calls_ == null || calls_.isEmpty())
			return groups;
		String symbol = eqt_.getOccId();
		String account = eqt_.getAccount();
		String userId = eqt_.getTradeUserId();
		int idx = 0;
		ATTradeDigest eqtOld = eqt_;
		double pxEqtOld = eqtOld.getPxNet();
		double eqtTs = 0;
		double eqtSz = 0;
		double callTs = 0;
		double callSz = 0;
		for (ATTrade trade : eqt_.trades) {
			var szTrd = Math.abs(trade.getSzTrade());
			var tsTrd = trade.getTsTrade().getTime();
			eqtTs += (tsTrd * szTrd);
			eqtSz += szTrd;
		}
		for (ATTradeDigest callOld : calls_) {
			if (szEqtOld < 100)
				break;
			for (ATTrade trade : callOld.trades) {
				var szTrd = Math.abs(trade.getSzTrade());
				var tsTrd = trade.getTsTrade().getTime();
				callTs += (tsTrd * szTrd);
				callSz += szTrd;
			}
			idx++;
			double szCallOld = callOld.getSzEff();
			double pxCallOld = callOld.getPxNet();
			double sz = szEqtOld + (100 * szCallOld);
			double newRatioEqt = 0;
			double newRatioCall = 0;
			ATTradeDigest eqtNew = new ATTradeDigest(eqtOld);
			ATTradeDigest callNew = new ATTradeDigest(callOld);
			if (sz == 0 /* Eqt == Calls */ ) {
				newRatioEqt = szEqtOld / eqtOld.getSzNet();
				newRatioCall = szCallOld / callOld.getSzNet();
				eqtNew.setSzEff(szEqtOld);
				callNew.setSzEff(szCallOld);
				szCallOld = 0.0;
				pxCallOld = 0.0;
				szEqtOld = 0.0;
				pxEqtOld = 0.0;
			} else if (sz < 0 /* Eqt < Calls */ ) {
				newRatioEqt = szEqtOld / eqtOld.getSzNet();
				newRatioCall = Math.abs(szEqtOld / callOld.getSzNet());

				double szCallRemaining = Math.round(sz / 100);
				double pxCallRemaining = Precision.round(szCallRemaining * pxCallOld / szCallOld, 4);
				callNew.setSzEff(szCallOld - szCallRemaining);
				callNew.setPxNet(pxCallOld - pxCallRemaining);
				szCallOld = szCallRemaining;
				pxCallOld = pxCallRemaining;
				szEqtOld = 0.0;
				pxEqtOld = 0.0;
			} else if (sz > 0 /* Eqt > Calls */ ) {
				newRatioCall = szCallOld / callOld.getSzNet();
				newRatioEqt = Math.abs(100 * szCallOld / eqtOld.getSzNet());

				double pxEqtUnit = eqtNew.getPxNet() / eqtNew.getSzEff();
				double szEqtNew = szEqtOld - sz;
				eqtNew.setSzEff(szEqtNew);
				eqtNew.setPxNet(Precision.round(szEqtNew * pxEqtUnit, 4));
				szCallOld = 0.0;
				pxCallOld = 0.0;
				szEqtOld = sz;
				pxEqtOld = Precision.round(sz * pxEqtUnit, 4);
			}
			callOld.setSzEff(szCallOld);
			callOld.setPxNet(pxCallOld);
			eqtOld.setSzEff(szEqtOld);
			eqtOld.setPxNet(pxEqtOld);
			Map<String, Double> ratios = new TreeMap<>();
			double pxEqtNew = eqtNew.getPxNet();
			double pxCallNew = callNew.getPxNet();
			if (pxEqtNew != 0) {
				ratios.put(symbol, newRatioEqt);
			}
			if (pxCallNew != 0) {
				ratios.put(callOld.getOccId(), newRatioCall);
			}
			if (!ratios.isEmpty()) {
				ATTradeGroup group = new ATTradeGroup(userId, account, symbol);
				long tsTrd = Math.round((eqtTs + callTs) / (eqtSz + callSz));
				group.setTsTrade(tsTrd);
				long tsExp = ATModelUtil.getExpiration(callOld.getOccId()).getTime();
				group.setTsExp(tsExp);
				String strt = IATTrade.STRATEGY_CC;
				group.setStrategy(strt);
				String id = String.format("%s-%s-%s-%s-%d", userId, account, symbol, strt, idx);
				group.setAtId(String.format("TG-%d", id.hashCode()));
				// double pxNet = Precision.round(pxEqtNew, 2);// + pxCallNew, 2); // NOTE: Call already baked in
				double pxRisk = Precision.round(pxEqtNew + pxCallNew, 4);
				group.setPxNet(pxRisk);
				group.setRatios(ratios);
				group.setRiskAbs(Math.abs(pxRisk));
				groups.add(group);
				if (log.isTraceEnabled()) {
					log.trace(String.format("idCC: %s: %-6s(%s) %10.2f /%10.2f /%10.2f /%8.2f", group.getAccount(),
							group.getUnderlying(),
							group.getStrategy(),
							group.getPxNet(), group.getRiskAbs(), group.getValueMid(), group.getYieldPct()));
				}
			}
		}
		return groups;
	}

	static private ArrayList<ATTradeGroup> identifyRemainingGroups(List<List<ATTradeDigest>> trades_) {
		ArrayList<ATTradeGroup> groups = new ArrayList<>();
		if (trades_ == null || trades_.isEmpty())
			return groups;
		int idx = 0;
		for (List<ATTradeDigest> trades : trades_) {
			if (trades.isEmpty())
				continue;
			ATTradeDigest first = trades.get(0);
			String account = first.getAccount();
			String userId = first.getTradeUserId();
			String symbol = ATModelUtil.getSymbol(first.getOccId());
			trades.sort(OccIdComparator);
			for (ATTradeDigest trade : trades) {
				double sz = trade.getSzEff();
				if (sz == 0)
					continue;
				double tsTot = 0;
				double szTot = 0;
				for (ATTrade tsTrade : trade.trades) {
					var szTrd = Math.abs(tsTrade.getSzTrade());
					var tsTrd = tsTrade.getTsTrade().getTime();
					tsTot += (tsTrd * szTrd);
					szTot += szTrd;
				}
				long tsTrd = Math.round(tsTot / szTot);
				idx++;
				String occId = trade.getOccId();
				double px = Precision.round(trade.getPxNet(), 2);
				double sign = Math.signum(sz);
				ATTradeGroup group = new ATTradeGroup(userId, account, symbol);
				group.setPxNet(px);
				double szEff = Math.abs(sz);
				String right = ATModelUtil.getRight(occId);
				if (right == null) {
					right = "";
				}
				String strt;
				switch (right) {
					case IATTrade.CALL:
						long tsExpC = ATModelUtil.getExpiration(occId).getTime();
						group.setTsExp(tsExpC);
						szEff *= 100;
						strt = sign > 0 ? IATTrade.STRATEGY_LC : IATTrade.STRATEGY_SC;
						group.setRiskAbs(Math.abs(sign > 0 ? px : Double.MAX_VALUE));
						break;
					case IATTrade.PUT:
						long tsExpP = ATModelUtil.getExpiration(occId).getTime();
						group.setTsExp(tsExpP);
						szEff *= 100;
						strt = sign > 0 ? IATTrade.STRATEGY_LP : IATTrade.STRATEGY_SP;
						double pxStrk = ATModelUtil.getStrike(occId);
						group.setRiskAbs(Math.abs(sign > 0 ? px : Precision.round(((szEff * pxStrk) - px), 2)));
						break;
					default:
						strt = sign > 0 ? IATTrade.STRATEGY_LE : IATTrade.STRATEGY_SE;
						group.setRiskAbs(sign > 0 ? Math.abs(px) : Double.MAX_VALUE);
						break;
				}
				group.setTsTrade(tsTrd);
				group.setStrategy(strt);
				String id = String.format("%s-%s-%s-%s-%d", userId, account, symbol, strt, idx);
				group.setAtId(String.format("TG-%d", id.hashCode()));
				double ratio = Math.abs(sz / trade.getSzNet());
				Map<String, Double> ratios = new TreeMap<>();
				ratios.put(occId, ratio);
				group.setRatios(ratios);

				groups.add(group);
				if (log.isTraceEnabled()) {
					log.trace(String.format("idX : %s", group.getAccount()));
				}
			}
		}
		return groups;
	}

}
