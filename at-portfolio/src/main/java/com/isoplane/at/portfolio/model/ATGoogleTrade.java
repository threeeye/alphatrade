package com.isoplane.at.portfolio.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATGoogleTrade implements IATTrade {

	private String atId;
	private String occId;
	private String traceId;
	private String userId;
	private Date tradeTimestamp;
	private Long tradeTimestamp2;
	private Double count;
	private Double count_actv;
	private Double unitPrice;
	private Double totalPrice;
	private Double ulPrice;
	private Double netPrice;
	private Integer tradeType;
	private String account;
	private String comment;
	private String group;

	transient private String hashStr;

	static {
		ATTradeModelTool.register(ATGoogleTrade.class);
	}

	public ATGoogleTrade() {

	}

	public ATGoogleTrade(IATTrade template_) {
		setTradeAccount(template_.getTradeAccount());
		setSzTrade(template_.getSzTrade());
		if (template_.getSzTrade() != template_.getSzTradeActive())
			setSzTradeActive(template_.getSzTradeActive());
		setOccId(template_.getOccId());
		setTraceId(template_.getTraceId());
		setPxTrade(template_.getPxTrade());
		setPxTradeTotal(template_.getPxTradeTotal());
		setPxNet(template_.getPxNet());
		setTsTrade(template_.getTsTrade());
		setTsTrade2(template_.getTsTrade2());
		setTradeType(template_.getTradeType());
		setTradeUserId(template_.getTradeUserId());
		setTradeGroup(template_.getTradeGroup());
		// Must be last
		setAtId(template_.getAtId());
	}

	@Override
	public int compareTo(Object other_) {
		return ATTradeModelTool.compare(this, other_);
	}

	@Override
	public int hashCode() {
		if (hashStr == null)
			hashStr = ATTradeModelTool.hashString(this, false);
		return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object obj_) {
		return ATTradeModelTool.equals(this, obj_);
	}

	@Override
	public String toString() {
		String str = IATTrade.toString(this);
		return str;
	}

	@Override
	public String getOccId() {
		return this.occId;
	}

	public void setOccId(String id) {
		hashStr = null;
		this.occId = id;
	}

	@Override
	public Date getTsTrade() {
		return this.tradeTimestamp;
	}

	@Override
	public Long getTsTrade2() {
		return this.tradeTimestamp2;
	}

	public void setTsTrade(Date tradeTimestamp) {
		hashStr = null;
		this.tradeTimestamp = tradeTimestamp;
	}

	public void setTsTrade2(Long value_) {
		hashStr = null;
		this.tradeTimestamp2 = value_;
	}

	@Override
	public Double getSzTrade() {
		return this.count;
	}

	public void setSzTrade(Double count) {
		hashStr = null;
		this.count = count;
	}

	@Override
	public Double getSzTradeActive() {
		return this.count_actv != null ? this.count_actv : this.count;
	}

	public void setSzTradeActive(Double count) {
		hashStr = null;
		this.count_actv = count;
	}

	@Override
	public Double getPxNet() {
		return this.netPrice;
	}

	public void setPxNet(Double value_) {
		// hashStr = null;
		this.netPrice = value_;
	}

	@Override
	public Double getPxTrade() {
		return this.unitPrice;
	}

	public void setPxTrade(Double unitPrice) {
		hashStr = null;
		this.unitPrice = unitPrice;
	}

	@Override
	public Double getPxTradeTotal() {
		return this.totalPrice;
	}

	public void setPxTradeTotal(Double totalPrice) {
		hashStr = null;
		this.totalPrice = totalPrice;
	}
	
	@Override
	public Double getPxUl() {
		return this.ulPrice;
	}

	public void setPxUl(Double value_) {
		this.ulPrice = value_;
	}

	@Override
	public Integer getTradeType() {
		return this.tradeType;
	}

	public void setTradeType(Integer tradeType) {
		hashStr = null;
		this.tradeType = tradeType;
	}

	@Override
	public String getTradeAccount() {
		return this.account;
	}

	public void setTradeAccount(String account) {
		hashStr = null;
		this.account = account;
	}

	@Override
	public String getTradeUserId() {
		return this.userId;
	}

	public void setTradeUserId(String userId) {
		hashStr = null;
		this.userId = userId;
	}

	@Override
	public String getTradeComment() {
		return this.comment;
	}

	public void setTradeComment(String comment) {
		hashStr = null;
		this.comment = comment;
	}

	@Override
	public String getTradeGroup() {
		return group;
	}

	public void setTradeGroup(String value_) {
		this.group = value_;
	}

	@Override
	public String getAtId() {
		if (atId == null) {
			atId = ATTradeModelTool.atId(this);
		}
		return atId;
	}

	public void setAtId(String value_) {
		this.atId = value_;
	}

	@Override
	public String getTraceId() {
		return this.traceId;
	}

	public void setTraceId(String value_) {
		this.traceId = value_;
	}


	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> result = new HashMap<>();
		result.put(IATAssetConstants.OCCID, getOccId());
		result.put(IATLookupMap.AT_ID, getAtId());
		result.put(IATAssetConstants.TRACE_ID, getTraceId());
		result.put(IATAssetConstants.TRADE_USER, getTradeUserId());
		result.put(IATAssetConstants.TS_TRADE_OLD, getTsTrade());
		result.put(IATAssetConstants.TS_TRADE, getTsTrade2());
		if (getSzTrade() != getSzTradeActive())
			result.put(IATAssetConstants.SIZE_ACTIVE, getSzTradeActive());
		result.put(IATAssetConstants.SIZE_TRADE, getSzTrade());
		result.put(IATAssetConstants.PX_TRADE, getPxTrade());
		result.put(IATAssetConstants.PX_TRADE_TOTAL, getPxTradeTotal());
		result.put(IATAssetConstants.TRADE_TYPE, getTradeType());
		result.put(IATAssetConstants.TRADE_ACCOUNT, getTradeAccount());
		result.put(IATAssetConstants.TRADE_COMMENT, getTradeComment());
		result.put(IATAssetConstants.TRADE_GROUP, getTradeGroup());
		return result;
	}

}
