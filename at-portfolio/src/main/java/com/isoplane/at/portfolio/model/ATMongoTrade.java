package com.isoplane.at.portfolio.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoTrade extends Document implements IATTrade, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	transient private String hashStr;

	static {
		ATTradeModelTool.register(ATMongoTrade.class);
	}

	public ATMongoTrade() {
	}

	public ATMongoTrade(IATTrade template_) {
		Map<String, Object> map = template_.getAsMap();
		putAll(map);
		this.setUnderlying(template_.getOccId());
		// NOTE: Overrides calculated -> Must be last
		this.setAtId(template_.getAtId());
		this.remove(IATLookupMap.AT_ID);
	}

	private void resetHash() {
		hashStr = null;
		setAtId(null);
	}

	@Override
	public int compareTo(Object other_) {
		return ATTradeModelTool.compare(this, other_);
	}

	@Override
	public int hashCode() {
		if (hashStr == null)
			hashStr = ATTradeModelTool.hashString(this, false);
		return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object obj_) {
		return ATTradeModelTool.equals(this, obj_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public String toString() {
		String str = IATTrade.toString(this);
		return str;
	}

	@Override
	public String getOccId() {
		return IATTrade.getOccId(this);
	}

	public void setOccId(String value_) {
		resetHash();
		IATTrade.setOccId(this, value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public void setAtId(String value_) {
		super.put(MONGO_ID, value_);
	}

	@Override
	public String getAtId() {
		String id = super.getString(MONGO_ID);
		if (id == null) {
			id = super.getString(IATLookupMap.AT_ID);
			if (id == null) {
				id = ATTradeModelTool.atId(this);
				setAtId(id);
			}
		}
		return id;
	}

	public void setTraceId(String value_) {
		super.put(TRACE_ID, value_);
	}

	@Override
	public String getTraceId() {
		return super.getString(TRACE_ID);
	}

	@Override
	public String getTradeUserId() {
		return IATTrade.getTradeUserId(this);
	}

	public void setTradeUserId(String value_) {
		resetHash();
		IATTrade.setTradeUserId(this, value_);
	}

	@Override
	public Date getTsTrade() {
		return IATTrade.getTsTrade(this);
	}

	public void setTsTrade(Date value_) {
		resetHash();
		IATTrade.setTsTrade(this, value_);
	}

	@Override
	public Long getTsTrade2() {
		return IATTrade.getTsTrade2(this);
	}

	public void setTsTrade2(Long value_) {
		resetHash();
		IATTrade.setTsTrade2(this, value_);
	}

	@Override
	public Double getSzTrade() {
		return IATTrade.getSzTrade(this);
	}

	public void setSzTrade(Double value_) {
		resetHash();
		IATTrade.setSzTrade(this, value_);
	}

	@Override
	public Double getSzTradeActive() {
		return IATTrade.getSzTradeActive(this);
	}

	public void setSzTradeActive(Double value_) {
		IATTrade.setSzTradeActive(this, value_);
	}

	@Override
	public Double getPxNet() {
		return IATTrade.getPxNet(this);
	}

	public void setPxNet(Double value_) {
		IATTrade.setPxNet(this, value_);
	}
	
	@Override
	public Double getPxTrade() {
		return IATTrade.getPxTrade(this);
	}

	public void setPxTrade(Double value_) {
		resetHash();
		IATTrade.setPxTrade(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATTrade.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		resetHash();
		IATTrade.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxUl() {
		return IATTrade.getPxUl(this);
	}

	public void setPxUl(Double value_) {
		IATTrade.setPxUl(this, value_);
	}
	
	@Override
	public Integer getTradeType() {
		return IATTrade.getTradeType(this);
	}

	public void setTradeType(int value_) {
		resetHash();
		IATTrade.setTradeType(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATTrade.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		resetHash();
		IATTrade.setTradeAccount(this, value_);
	}

	@Override
	public String getTradeComment() {
		return IATTrade.getTradeComment(this);
	}

	public void setTradeComment(String value_) {
		resetHash();
		IATTrade.setTradeComment(this, value_);
	}

	@Override
	public String getTradeGroup() {
		return IATTrade.getTradeGroup(this);
	}

	public void setTradeGroup(String value_) {
		IATTrade.setTradeGroup(this, value_);
	}

	public void setUnderlying(String occId_) {
		if (StringUtils.isBlank(occId_)) {
			super.remove(UNDERLYING);
			return;
		}
		String sym = occId_;
		if (sym.length() > 6) {
			sym = sym.substring(0, 6).trim();
		}
		super.put(UNDERLYING, sym);
	}

	public String getUnderlying() {
		return super.getString(UNDERLYING);
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> map = new HashMap<>(this);
		map.put(IATLookupMap.AT_ID, getAtId());
		map.remove(IATMongo.MONGO_ID);
		map.remove(UNDERLYING);
		return this;
	}

}
