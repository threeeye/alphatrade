package com.isoplane.at.portfolio.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.IATLookupMap;

public class ATTradeOld implements IATTrade {

	private String _atid;
	private String occId;
	private Long ts_trd;
	private Long ts_trd_2;
	private Double sz_trd;
	private Double sz_act;
	private Double px_net;
	private Double px_trd;
	private Double px_trd_tot;
	private Double px_ul;
	private String trd_usr;
	private Integer trd_typ;
	private String trd_acc;
	private String trd_cmt;
	private String trd_grp;

	public ATTradeOld() {
	}

	public ATTradeOld(IATTrade tmpl_) {
		initialize(tmpl_);
	}

	public ATTradeOld(Map<String, Object> map_) {
		if (map_ instanceof IATTrade) {
			initialize((IATTrade) map_);
		} else {
			_atid = IATAsset.getString(map_, IATLookupMap.AT_ID);
			occId = IATAsset.getString(map_, IATAssetConstants.OCCID);
			trd_usr = IATAsset.getString(map_, IATAssetConstants.TRADE_USER);
			ts_trd = IATAsset.getLong(map_, IATAssetConstants.TS_TRADE_OLD);
			ts_trd_2 = IATAsset.getLong(map_, IATAssetConstants.TS_TRADE);
			sz_trd = IATAsset.getDouble(map_, IATAssetConstants.SIZE_TRADE);
			sz_act = IATAsset.getDouble(map_, IATAssetConstants.SIZE_ACTIVE);
			px_net = IATTrade.getPxNet(map_);
			px_trd = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE);
			px_trd_tot = IATAsset.getDouble(map_, IATAssetConstants.PX_TRADE_TOTAL);
			trd_typ = IATAsset.getInt(map_, IATAssetConstants.TRADE_TYPE);
			trd_acc = IATAsset.getString(map_, IATAssetConstants.TRADE_ACCOUNT);
			trd_cmt = IATAsset.getString(map_, IATAssetConstants.TRADE_COMMENT);
		}
	}

	private void initialize(IATTrade tmpl_) {
		_atid = tmpl_.getAtId();
		occId = tmpl_.getOccId();
		trd_usr = tmpl_.getTradeUserId();
		Date d = tmpl_.getTsTrade();
		if (d != null)
			ts_trd = d.getTime();
		ts_trd_2 = tmpl_.getTsTrade2();
		sz_trd = tmpl_.getSzTrade();
		if (tmpl_.getSzTrade() != tmpl_.getSzTradeActive())
			sz_act = tmpl_.getSzTradeActive();
		px_net = tmpl_.getPxNet();
		px_trd = tmpl_.getPxTrade();
		px_trd_tot = tmpl_.getPxTradeTotal();
		trd_typ = tmpl_.getTradeType();
		trd_acc = tmpl_.getTradeAccount();
		trd_cmt = tmpl_.getTradeComment();
	}

	@Override
	public int compareTo(Object other_) {
		String otherId = other_ != null && (other_ instanceof ATTradeOld) ? ((ATTradeOld) other_).getAtId() : null;
		if (otherId == null)
			return -1;
		return this.getAtId().compareTo(otherId);
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> map = new HashMap<>();
		map.put(IATLookupMap.AT_ID, _atid);
		map.put(IATAssetConstants.OCCID, occId);
		map.put(IATAssetConstants.TRADE_USER, trd_usr);
		map.put(IATAssetConstants.TS_TRADE_OLD, ts_trd);
		map.put(IATAssetConstants.TS_TRADE, ts_trd_2);
		map.put(IATAssetConstants.SIZE_TRADE, sz_trd);
		if (sz_trd != sz_act)
			map.put(IATAssetConstants.SIZE_ACTIVE, sz_act);
		map.put(IATAssetConstants.PX_TRADE, px_trd);
		map.put(IATAssetConstants.PX_TRADE_TOTAL, px_trd_tot);
		map.put(IATAssetConstants.TRADE_TYPE, trd_typ);
		map.put(IATAssetConstants.TRADE_ACCOUNT, trd_acc);
		map.put(IATAssetConstants.TRADE_COMMENT, trd_cmt);
		return map;
	}

	@Override
	public String getAtId() {
		return _atid;
	}

	@Override
	public String getOccId() {
		return occId;
	}

	@Override
	public String getTradeUserId() {
		return trd_usr;
	}

	@Override
	public Date getTsTrade() {
		Date date = ts_trd != null ? new Date(ts_trd) : null;
		return date;
	}

	@Override
	public Long getTsTrade2() {
		return ts_trd_2;
	}

	@Override
	public Double getSzTrade() {
		return sz_trd;
	}

	@Override
	public Double getSzTradeActive() {
		return sz_act != null ? sz_act : sz_trd;
	}

	@Override
	public Double getPxNet() {
		return px_net;
	}

	@Override
	public Double getPxTrade() {
		return px_trd;
	}

	@Override
	public Double getPxTradeTotal() {
		return px_trd_tot;
	}

	@Override
	public Double getPxUl() {
		return px_ul;
	}

	@Override
	public Integer getTradeType() {
		return trd_typ;
	}

	@Override
	public String getTradeAccount() {
		return trd_acc;
	}

	@Override
	public String getTradeComment() {
		return trd_cmt;
	}

	@Override
	public String getTradeGroup() {
		return trd_grp;
	}

	@Override
	public String getTraceId() {
		// TODO Auto-generated method stub
		return null;
	}

}
