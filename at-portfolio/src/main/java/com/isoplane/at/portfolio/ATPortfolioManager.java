package com.isoplane.at.portfolio;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATPortfolio;
import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup.TradeTrace;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.model.IATUserAsset;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATPortfolioService;
import com.isoplane.at.commons.service.IATTotalPositionListener;
import com.isoplane.at.commons.service.IATTradeListener;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATMathUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.model.ATTradeModelTool;
import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;
import com.isoplane.at.portfolio.util.ATPositionUpdater;
import com.isoplane.at.portfolio.util.ATTradeCumulator;
import com.isoplane.at.portfolio.util.ATTradeDigester;
import com.isoplane.at.portfolio.util.ATTradeGrouper;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPortfolioManager implements IATPortfolioService, /*IATTradeListener,*/ IATTotalPositionListener {

	static final Logger log = LoggerFactory.getLogger(ATPortfolioManager.class);

	static final String REMAINING = ".REM";

	private boolean _isStreaming = false;
	protected ATPortfolioMogodbTools _mongodb;
	private ATPositionUpdater _positionUpdater;
	private Set<IATTradeListener> _listeners;
	private Set<IATTotalPositionListener> _totalListeners;
	private boolean _isInitialized = false;
	private long _legacySheetHash = -1;
	private long _sheetHash = -1;

	private Comparator<ATTrade> _tradeComparator = new Comparator<ATTrade>() {
		@Override
		public int compare(ATTrade a_, ATTrade b_) {
			int sortTs = Long.compare(a_.getTsTrade2(), b_.getTsTrade2());
			int sortId = sortTs == 0 ? a_.getOccId().compareTo(b_.getOccId()) : sortTs;
			int sort = sortId == 0 ? IATTrade.TYPES_OPEN.contains(a_.getTradeType()) ? -1 : 1 : sortId;
			return sort;
		}
	};

	public ATPortfolioManager(boolean isStreaming_) {
		_isStreaming = isStreaming_;
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
	}

	@Override
	public void initService() {
		Configuration config = ATConfigUtil.config();
		ATMongoTableRegistry.init();

		boolean isMasterList = config.getBoolean("portfolio.isMasterList");
		log.info(String.format("Maintain master trade list [%b]", isMasterList));

		_mongodb = new ATPortfolioMogodbTools(_isStreaming, /* this,*/ isMasterList ? this : null);
		_mongodb.init();
		_positionUpdater = new ATPositionUpdater(_mongodb);
		_isInitialized = true;
	}

	@Override
	public void register(IATTradeListener listener_) {
		if (_listeners == null) {
			_listeners = new HashSet<>();
		}
		_listeners.add(listener_);
	}

	@Override
	public void register(IATTotalPositionListener listener_) {
		if (_totalListeners == null) {
			_totalListeners = new HashSet<>();
		}
		_totalListeners.add(listener_);
	}

	@Override
	public Set<String> getActiveOccIds(String userId_, boolean includeOptions_) {
		IATWhereQuery query = ATPersistenceRegistry.query()
				.ne(IATAssetConstants.SIZE_ACTIVE, 0)
				.notNull(IATAssetConstants.SIZE_TRADE);
		if (!StringUtils.isBlank(userId_)) {
			query = query.eq(IATAssetConstants.TRADE_USER, userId_);
		}
		if (includeOptions_) {
			String nowDS6 = ATFormats.DATE_yyMMdd.get().format(new Date());
			IATWhereQuery q1 = ATPersistenceRegistry.query().isNull(IATAssetConstants.EXPIRATION_DATE_STR);
			IATWhereQuery q2 = ATPersistenceRegistry.query().gte(IATAssetConstants.EXPIRATION_DATE_STR, nowDS6);
			query = query.and(q2.or(q1));
		} else {
			query = query.isNull(IATAssetConstants.EXPIRATION_DATE_STR);
		}
		Set<String> occIds = _mongodb.getOccIds(query);
		return occIds;
	}

	@Override
	public Set<String> getAffiliatedUserIds(String symbol_) {
		Set<String> userIds = _mongodb.getAffiliatedUserIds(symbol_);
		return userIds;
	}

	@Override
	public Set<String> getOwnerIds(String occId_) {
		return _mongodb.getOwnerIds(occId_);
	}

	@Override
	public List<IATTrade> getTrades(IATWhereQuery query_) {
		IATWhereQuery query = query_ != null ? query_ : ATPersistenceRegistry.query().empty();
		query = query.and().ne(IATMongo.DISABLED, true);
		List<IATTrade> trades = _mongodb.getTrades(query);
		return trades;
	}

	// NOTE: trade table - index: occId, userId, account
	@Override
	public boolean saveTrades(String uid_, ATTrade[] trades_) {
		if (StringUtils.isBlank(uid_) || trades_ == null || trades_.length == 0)
			return false;
		if (log.isDebugEnabled()) {
			String msg = new Gson().toJson(trades_);
			log.debug(String.format("saveTrades: %s", msg));
		}
		final Date tomorrow = DateUtils.addDays(DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH), 1);
		// IATMarketDataService mktSvc = ATServiceRegistry.getMarketDataService();

		List<ATTrade> trades = Arrays.asList(trades_);
		Set<String> origIds = new HashSet<>();
		Set<String> atIds = new HashSet<>();
		Set<String> occIdTypes = new HashSet<>();
		String symbol = null;
		String account = null;

		String traceId = getTraceId(trades_);

		// 1. Verify & Clean
		for (ATTrade t : trades) {
			IATTrade.verify(t, uid_);
			t.setTraceId(traceId);
			String atId = t.getAtId();
			if (atId != null)
				atIds.add(atId);
			String origId = t.getOriginalAtId();
			if (StringUtils.isNotBlank(origId))
				origIds.add(origId);
			String occId = t.getOccId().toUpperCase();
			t.setOccId(occId);
			occIdTypes.add(String.format("%s.%d", occId, t.getTradeType()));
			String sym = occId.length() > 6 ? occId.substring(0, 6).trim() : occId;
			if (symbol == null) {
				symbol = sym;
			} else if (!symbol.equals(sym)) {
				throw new ATException(String.format("Mixed symbols [%s/%s]", symbol, sym));
			}
			String acc = t.getTradeAccount();
			if (account == null) {
				account = acc;
			} else if (!account.equals(acc)) {
				throw new ATException(String.format("Mixed accounts [%s/%s]", account, acc));
			}
			Date tsTrade = t.getTsTrade();
			if (tomorrow.before(tsTrade)) {
				throw new ATException(String.format("Future trade date"));
			}
		}
		if (!Collections.disjoint(atIds, origIds)) {
			// NOTE: Logic (timestamps, counts, etc) too complex
			throw new ATException(String.format("Update collision"));
		}
		if (trades_.length != occIdTypes.size()) {
			throw new ATException(String.format("Duplicate OCC Ids: %s - %s", atIds, occIdTypes));
		}

		@SuppressWarnings("unchecked")
		List<IATTrade> sort = (List<IATTrade>) (Object) trades;
		IATTrade.sortChronological(sort);

		// 2. Insert & Update
		var occIds = new TreeSet<String>();
		List<String> deleteIds = new ArrayList<>();
		for (ATTrade trade : trades) {
			String id = trade.getAtId();
			occIds.add(trade.getOccId());
			if (StringUtils.isBlank(id)) {
				id = ATTradeModelTool.atId(trade);
				trade.setAtId(id);
			} else if (id.startsWith("DELETE:")) {
				deleteIds.add(id.substring("DELETE:".length()));
				continue;
			}
			if (StringUtils.isBlank(trade.getTradeGroup())) {
				String groupId = ATTradeModelTool.groupId(trade);
				trade.setTradeGroup(groupId);
			}
			//		updateTraceId(trade);

			trade = trade.prep4Save();
			if (trade.getTradeType() < IATTrade.ASSET_BOUNDARY /* Position changing; Otherwise dividend etc */) {
				rollupTrade(trade, true, true);
				log.debug(String.format("Traded [%s]", trade));
			} else {
				trade.setPxNet(trade.getPxTradeTotal());
				_mongodb.updateTrade(trade.getTradeUserId(), id, trade);
			}
			// Double value_ = (Double) trade.get(IATTrade.PX_NET);
			// if (value_ != null && (!(value_ instanceof Double) || Double.isNaN(value_)))
			// {
			// int i = 0;
			// }

			// String ul = trade.getOccId();
			// if (ul.length() > 6) {
			// ul = ul.substring(0, 6).trim();
			// ATMarketData mktUl = mktSvc.getMarketData(ul);
			// Double pxUl = mktUl != null ? mktUl.getPxLast() : null;
			// if (pxUl != null) {
			// trade.setPxUl(pxUl);
			// }
			// }
		}
		_mongodb.saveTradeLedger(trades);
		var positions = _positionUpdater.updatePositions(uid_, account, occIds);
		if (positions != null) {

		} else {

		}

		Map<String, ATTradeDigest> digestMap = digestTrades(uid_, account, symbol);
		Collection<ATTradeDigest> digests = digestMap != null && !digestMap.isEmpty() ? digestMap.values()
				: new ArrayList<>();
		groupTrades(uid_, account, symbol, digests);

		// 3. Delete (virtual)
		if (!deleteIds.isEmpty()) {
			for (String deleteId : deleteIds) {
				IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, uid_).and()
						.eq(IATMongo.MONGO_ID, deleteId);
				boolean success = _mongodb.disableTrades(query);
				log.debug(String.format("Disabled [%s/%b]", deleteId, success));
			}
		}

		updatePortfolio(uid_, account, symbol);
		updateAssets(uid_, account, symbol);
		return true;
	}

	@Override
	public long saveTradeTrace(ATTradeTraceGroup data_, Long replaceId_) {
		var trades = data_ != null ? data_.getItems() : null;
		if (trades == null || trades.length == 0) {
			return -1;
		}
		var ids = new ArrayList<String>();
		for (TradeTrace trade : trades) {
			ids.add(trade.getId());
		}
		IATWhereQuery query = ATPersistenceRegistry.query().in(IATMongo.MONGO_ID, ids);
		var dbTrades = _mongodb.getTradeLedger(query);
		if (dbTrades == null || dbTrades.size() != trades.length) {
			log.error(String.format("saveTradeTrace [%s]: Invalid trade references", (Object) data_.getSymbols()));
			return -1;
		}
		long count = _mongodb.saveTradeTrace(data_);
		if (replaceId_ != null && count == 1) {
			var deleteId = String.format("%s:%s", data_.getUserId(), replaceId_);
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, deleteId);
			_mongodb.deleteTradeTrace(deleteQuery);
		}
		return count;
	}

	@Override
	public List<ATTradeTraceGroup> getTradeTrace(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		List<ATTradeTraceGroup> traces = _mongodb.getTradeTraceGroup(query);
		return traces;
	}

	@Override
	public boolean deleteTradeTrace(String uid_, Long id_) {
		if (StringUtils.isBlank(uid_) || id_ == null) {
			log.error(String.format("deleteTradeTrace: Missing ids"));
			return false;
		}
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, uid_).and().eq(IATLookupMap.AT_ID,
				id_);
		long count = _mongodb.deleteTradeTrace(query);
		return count == 1;
	}

	@Override
	public boolean archiveTradeTrace(String userId_, Object id_) {
		Long id = id_ instanceof Double
				? ((Double) id_).longValue()
				: id_ instanceof Long
						? (Long) id_
						: NumberUtils.toLong(id_.toString());
		if (StringUtils.isBlank(userId_) || id == null) {
			log.error(String.format("archiveTradeTrace Error: Missing Ids (user: %s, trace: %s}", userId_, id_));
			return false;
		}
		var traceId = String.format("%s:%d", userId_, id);
		var result = _mongodb.archiveTradeTraceGroup(traceId);
		return result;
	}

	@Override
	public List<ATTrade> getTradeLedger(String userId_, String... symbols_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		if (symbols_ != null) {
			if (symbols_.length == 1 && !StringUtils.isBlank(symbols_[0].toUpperCase())) {
				query = query.and().eq(IATTrade.UNDERLYING, symbols_[0]);
			} else if (symbols_.length > 1) {
				Set<String> ids = Arrays.asList(symbols_).stream().filter(Objects::nonNull).map(String::toUpperCase)
						.collect(Collectors.toSet());
				query = query.and().in(IATTrade.UNDERLYING, ids);
			}
		}
		List<ATTrade> trades = _mongodb.getTradeLedger(query);
		return trades;
	}

	private String lookupTraceId(ATTrade trade_) {
		if (trade_ == null)
			return null;
		String occId = trade_.getOccId();
		String userId = trade_.getTradeUserId();
		String account = trade_.getTradeAccount();
		IATWhereQuery query = ATPersistenceRegistry.query()
				.eq(IATAssetConstants.TRADE_USER, userId)
				.eq(IATAssetConstants.TRADE_ACCOUNT, account)
				.eq(IATAssetConstants.OCCID, occId);
		List<ATTradeDigest> digest = _mongodb.getTradeDigest(query);
		String traceId = digest != null && !digest.isEmpty() ? digest.get(0).getTraceId() : null;
		return traceId;
	}

	private String getTraceId(ATTrade[] trades_) {
		if (trades_ == null || trades_.length == 0)
			return null;
		String symbol = ATModelUtil.getSymbol(trades_[0].getOccId());
		Set<String> traceIds = new TreeSet<>();
		for (ATTrade trade : trades_) {
			String traceId = trade.getTraceId();
			if (StringUtils.isBlank(traceId)) {
				traceId = lookupTraceId(trade);
				if (StringUtils.isBlank(traceId))
					continue;
			}
			traceIds.add(traceId);
		}
		if (traceIds.isEmpty()) {
			String traceId = String.format("%s:%d", symbol, ATSysUtils.currentTimeMillis());
			return traceId;
		} else if (traceIds.size() == 1) {
			String traceId = traceIds.iterator().next();
			return traceId;
		} else {
			log.warn(String.format("getTraceId Multiple traces [%s / %s]", symbol, traceIds));
			String id = String.join("", traceIds).replace(symbol, "");
			String traceId = String.format("%s%s", symbol, id);
			return traceId;
		}
	}

	// @SuppressWarnings("unchecked")
	private void rollupTrade(ATTrade trade_, boolean ignoreZeroSize_, boolean includeTrade_) {
		if (trade_ == null)
			return;
		String occId = trade_.getOccId();
		String userId = trade_.getTradeUserId();
		String account = trade_.getTradeAccount();
		IATWhereQuery query = ATPersistenceRegistry.query()
				.eq(IATAssetConstants.TRADE_USER, userId)
				.eq(IATAssetConstants.TRADE_ACCOUNT, account)
				.eq(IATAssetConstants.OCCID, occId);

		// NOTE: Deprecate this and move everything to trade ledger
		IATWhereQuery tradeQuery = query.lt(IATAssetConstants.TRADE_TYPE, IATTrade.ASSET_BOUNDARY);
		if (ignoreZeroSize_) {
			tradeQuery = tradeQuery.notNull(IATAssetConstants.SIZE_ACTIVE).ne(IATAssetConstants.SIZE_ACTIVE, 0);
		}
		List<IATTrade> origTrades = _mongodb.getTrades(tradeQuery);
		List<ATTrade> trades = new ArrayList<>();
		if (origTrades != null && !origTrades.isEmpty()) {
			trades.addAll(origTrades.stream().map(t -> new ATTrade(t)).collect(Collectors.toList()));
		}
		if (includeTrade_) {
			trades.add(trade_);// instanceof ATTrade ? (ATTrade) trade_ : new ATTrade(trade_));
		}
		@SuppressWarnings({ "rawtypes", "unchecked" })
		List<IATTrade> list = (List) trades;
		IATTrade.sortChronological(list);
		double count = 0.0;
		double value = 0.0;
		for (int iA = 0; iA < trades.size(); iA++) {
			ATTrade tA = trades.get(iA);
			int type = tA.getTradeType();
			Double sizeA = (Double) tA.getSzTradeActive();
			if (sizeA == null) {
				sizeA = tA.getSzTrade();
			}
			double sizeA_new = sizeA;
			double sgnA = Math.signum(sizeA);
			if (IATTrade.TYPES_OPEN.contains(type)) {
				count += tA.getSzTrade();
				value += tA.getPxTradeTotal();
			} else if (IATTrade.TYPES_CLOSE.contains(type)) {
				Double pxTotal = tA.getPxTradeTotal();
				double frac = Math.abs(tA.getSzTrade()) / Math.abs(count);
				double pxFrac = frac * value;
				Double pxNet = pxFrac + (pxTotal != null ? pxTotal : 0);
				tA.setPxNet(ATMathUtil.round(pxNet, 2));
				count += tA.getSzTrade();
				value -= pxFrac;
			}

			ListIterator<ATTrade> iB = trades.listIterator(iA + 1);
			while (sizeA_new != 0 && iB.hasNext()) {
				ATTrade tB = iB.next();
				Double sizeB = (Double) tB.getSzTradeActive();
				if (sizeB == null) {
					sizeB = tB.getSzTrade();
				}
				if ((tA == tB) || (sgnA == Math.signum(sizeB)))
					continue;

				double delta = sizeA_new + sizeB;
				double sizeB_new = sizeB;
				if (sgnA >= 0) {
					sizeA_new = Math.max(delta, 0);
					sizeB_new = delta >= 0 ? 0.0 : delta;
				} else {
					sizeA_new = Math.min(delta, 0);
					sizeB_new = delta <= 0 ? 0.0 : delta;
				}
				tB.setSzTradeActive(ATMathUtil.round(sizeB_new, 4));
			}
			tA.setSzTradeActive(ATMathUtil.round(sizeA_new, 4));
			_mongodb.updateTrade(tA.getTradeUserId(), tA.getAtId(), tA);
		}
		log.debug(String.format("Rolled up [%s/%d]", occId, trades.size()));
	}

	// {$expr:{$ne:["$sz_act","$_temp_size"]}, trd_typ:{$lt:5000}}
	// private void fixTrades() {
	// if (true)
	// return;
	// IATWhereQuery passiveQuery =
	// ATPersistenceRegistry.query().gte(IATAssetConstants.TRADE_TYPE,
	// IATTrade.ASSET_BOUNDARY);
	// List<IATTrade> passiveTrades = _mongodb.getTrades(passiveQuery);
	// for (IATTrade itrade : passiveTrades) {
	// ATTrade trade = new ATTrade(itrade);
	// trade.setPxNet(trade.getPxTradeTotal());
	// _mongodb.updateTrade(trade.getTradeUserId(), trade.getAtId(), trade);
	// }
	//
	// if (true)
	// return;
	// IATWhereQuery activeQuery =
	// ATPersistenceRegistry.query().lt(IATAssetConstants.TRADE_TYPE,
	// IATTrade.ASSET_BOUNDARY);
	// List<IATTrade> activeTrades = _mongodb.getTrades(activeQuery);
	// if (activeTrades == null || activeTrades.isEmpty())
	// return;
	// Map<Object, List<IATTrade>> userMap =
	// activeTrades.stream().collect(Collectors.groupingBy(IATTrade::getTradeUserId));
	// for (List<IATTrade> userTrades : userMap.values()) {
	// Map<Object, List<IATTrade>> userAccountMap =
	// userTrades.stream().collect(Collectors.groupingBy(IATTrade::getTradeAccount));
	// for (List<IATTrade> userAccountTrades : userAccountMap.values()) {
	// userAccountTrades.sort((a, b) -> a.getOccId().compareTo(b.getOccId()));
	// Map<Object, List<IATTrade>> userAccountOccidMap = userAccountTrades.stream()
	// .collect(Collectors.groupingBy(IATTrade::getOccId, TreeMap::new,
	// Collectors.toList()));
	// for (List<IATTrade> userAccountOccidTrades : userAccountOccidMap.values()) {
	// if (userAccountOccidTrades.isEmpty())
	// continue;
	// IATTrade trade = userAccountOccidTrades.get(0);
	// log.debug(String.format("Rolling up [%s / %s]", trade.getTradeAccount(),
	// trade.getOccId()));
	// rollupTrade(trade, false, false);
	// }
	// }
	// }
	// }

	@Override
	public List<ATUserAsset> getAssets(String userId_) {
		List<IATUserAsset> iassets = _mongodb.getAssets(userId_);
		List<ATUserAsset> assets = iassets.stream()
				.map(a -> new ATUserAsset(a))
				.sorted((a1, a2) -> a1.getDateStr().compareTo(a2.getDateStr()))
				.collect(Collectors.toList());
		return assets;
	}

	public void saveAssets(Collection<ATUserAsset> assets_) {
		_mongodb.saveAssets(assets_);
	}

	// NOTE: May be too expensive for multi-user
	private void updateAssets(String userId_, String acctId_, String sym_) {
		IATWhereQuery tradeQuery = ATPersistenceRegistry.query()
				.eq(IATAssetConstants.TRADE_USER, userId_).eq(IATAssetConstants.TRADE_ACCOUNT, acctId_)
				.eq(IATAssetConstants.OCCID, sym_)
				.notNull(IATAssetConstants.SIZE_TRADE).isNull(IATAssetConstants.EXPIRATION_DATE_STR);
		List<IATTrade> trades = getTrades(tradeQuery);
		if (trades == null || trades.isEmpty())
			return;
		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;
		final DateTimeFormatter yyyy_mm_ddFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		trades.sort((a, b) -> a.getTsTrade().compareTo(b.getTsTrade()));
		Map<String, ATUserAsset> assetMap = new HashMap<>();
		Map<String, Double> sizeMap = new HashMap<>();
		for (IATTrade trade : trades) {
			// if (!"AAPL".equals(trade.getOccId())) {
			// log.debug(trade.getOccId());
			// continue;
			// }
			try {
				String sizeKey = trade.getOccId() + trade.getTradeUserId();
				Double oldSz = sizeMap.get(sizeKey);
				Double newSz = oldSz != null ? oldSz + trade.getSzTrade() : trade.getSzTrade();
				sizeMap.put(sizeKey, newSz);

				LocalDate assetDate = new java.sql.Date(trade.getTsTrade().getTime()).toLocalDate()
						.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
				String assetDateStr = yyyy_mm_ddFormatter.format(assetDate);
				String assetKey = String.format("%s:%s:%s", trade.getTradeUserId(), assetDateStr, trade.getOccId());
				ATUserAsset asset = new ATUserAsset(trade.getTradeUserId(), assetDateStr, trade.getOccId(), newSz,
						null);
				assetMap.put(assetKey, asset);
			} catch (Exception ex) {
				log.error(String.format("Error: %s", trade), ex);
				throw ex;
			}
		}
		IATMarketDataService mktSvc = ATServiceRegistry.getMarketDataService();
		Collection<ATUserAsset> assets = assetMap.values();
		try {
			Map<String, List<ATUserAsset>> symbolMap = assets.stream()
					.collect(Collectors.groupingBy(ATUserAsset::getOccId));
			LocalDate now = LocalDate.now();// .with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
			LocalDate nextSaturday = now.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
			LocalDate lastSaturday = nextSaturday.minusWeeks(1);
			Map<String, ATUserAsset> processedMap = new HashMap<>();
			for (String symbol : symbolMap.keySet()) {
				List<ATUserAsset> symbolAssets = symbolMap.get(symbol);
				symbolAssets.sort((a, b) -> a.getDateStr().compareTo(b.getDateStr()));
				String firstDateStr = symbolAssets.get(0).getDateStr().replace("-", "");
				IATWhereQuery query = ATPersistenceRegistry.query()
						.eq(IATAssetConstants.OCCID, symbol).gte(IATAssetConstants.DATE_STR, firstDateStr);
				List<String> flags = Arrays.asList("ohlc", "avg", "hv", "minmax");
				Map<String, List<ATHistoryData>> historyMap = mktSvc.getHistory(query, flags, avgMode, hvMode);
				Map<LocalDate, Double> dateCloseMap = historyMap.values().stream().filter(hl -> !hl.isEmpty())
						.flatMap(hl -> hl.stream().filter(h -> h.getClose() != null))
						.collect(Collectors.toMap(
								h -> LocalDate.parse(h.getDateStr(), yyyy_mm_ddFormatter),
								v -> v.getClose()));
				Map<String, List<ATUserAsset>> symbolUserMap = symbolAssets.stream()
						.collect(Collectors.groupingBy(ATUserAsset::getUserId));
				TreeMap<LocalDate, ATUserAsset> dateAssetMap = new TreeMap<>();
				for (String user : symbolUserMap.keySet()) {
					List<ATUserAsset> userAssets = symbolUserMap.get(user);
					for (ATUserAsset asset : userAssets) {
						LocalDate date = LocalDate.parse(asset.getDateStr(), yyyy_mm_ddFormatter);
						ATUserAsset oldAsset = dateAssetMap.get(date);
						if (oldAsset == null) {
							dateAssetMap.put(date, asset);
						} else {
							ATUserAsset newAsset = new ATUserAsset(asset);
							newAsset.setSize(asset.getSize() + oldAsset.getSize());
							dateAssetMap.put(date, newAsset);
						}
					}
					// TreeMap<LocalDate, ATUserAsset> dateAssetMap = userAssets.stream()
					// 		.collect(Collectors.toMap(
					// 				a -> LocalDate.parse(a.getDateStr(), yyyy_mm_ddFormatter),
					// 				Function.identity(),
					// 				(a1, a2) -> {
					// 					ATUserAsset merge = new ATUserAsset(a1);
					// 					merge.setSize(a1.getSize() + a2.getSize());
					// 					return merge;
					// 				},
					// 				TreeMap::new));
					LocalDate currentDate = dateAssetMap.firstKey();
					ATUserAsset currentAsset = dateAssetMap.get(currentDate);
					Double currentClose = dateCloseMap.get(currentDate);
					do {
						if (dateCloseMap.containsKey(currentDate)) {
							currentClose = dateCloseMap.get(currentDate);
						} else if (currentDate.isAfter(lastSaturday)) {
							IATMarketData mkt = mktSvc.getMarketData(currentAsset.getOccId());
							if (mkt != null) {
								currentClose = mkt.getPxClose();
							}
						}
						if (dateAssetMap.containsKey(currentDate)) {
							currentAsset = dateAssetMap.get(currentDate);
						}
						Double currentSz = ATMathUtil.round(currentAsset.getSize(), 4);
						if (currentClose != null && currentSz != 0) {
							Double value = ATMathUtil.round(currentClose * currentSz, 2);// bdValue.setScale(2,
																							// RoundingMode.HALF_DOWN).doubleValue();
							String dateStr = yyyy_mm_ddFormatter.format(currentDate);
							currentAsset = new ATUserAsset(user, dateStr, symbol, currentSz, value);
							String mapKey = String.format("%s:%s:%s", user, symbol, dateStr);
							processedMap.put(mapKey, currentAsset);
						} else {
							log.debug(String.format("Skipping [%s]: %s / %.2f / %.2f", symbol, currentDate, currentSz,
									currentClose));
						}
						currentDate = currentDate.plusWeeks(1);
					} while (currentDate.isBefore(nextSaturday));
				}
			}
			Collection<ATUserAsset> processedAssets = processedMap.values();
			saveAssets(processedAssets);
		} catch (Exception ex) {
			log.error(String.format("Error processing assets"), ex);
		}
	}

	@Override
	public List<IATPortfolio> getPortfolio(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
		if (StringUtils.isBlank(symbol_)) {
			query = query.ne(IATAssetConstants.SIZE, 0);
		} else {
			query = query.eq(IATAssetConstants.OCCID_UNDERLYING, symbol_);
		}
		List<IATPortfolio> portfolio = _mongodb.getPortfolio(query);
		return portfolio;
	}

	@Override
	public List<ATPosition> getPositions(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.eq(IATTrade.UNDERLYING, symbol_);
		}
		var positions = _mongodb.queryPositions(query);
		return positions;
	}

	@Override
	public Map<String, Map<String, Double>> assessPerformance(String userId_) {

		// _mongodb.convertTrades();
		performanceSchedule(userId_);

		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);// .startsWith(IATAssetConstants.OCCID,
																										// "MCD");
		List<IATTrade> trades = _mongodb.getTrades(query);
		trades.sort(new Comparator<IATTrade>() {
			@Override
			public int compare(IATTrade a_, IATTrade b_) {
				int tsComp = a_.getTsTrade().compareTo(b_.getTsTrade());
				if (tsComp != 0)
					return tsComp;
				Integer aO = IATTrade.TYPES_OPEN.contains(a_.getTradeType()) ? -1 : 1;
				Integer bO = IATTrade.TYPES_OPEN.contains(b_.getTradeType()) ? -1 : 1;
				return aO.compareTo(bO);
			}
		});
		log.info(String.format("User [%s] loaded [%d] trades", userId_, trades.size()));

		// Map<String, List<IATTrade>> duplicateMap = new TreeMap<>();
		Map<Date, Double> dateMap = new TreeMap<>();
		Map<String, Pair<Double, Double>> tradeIdMap = new HashMap<>();

		Calendar now = Calendar.getInstance();
		Calendar todayCal = DateUtils.truncate(now, Calendar.DAY_OF_MONTH);
		Calendar thisWeekCal = Calendar.getInstance();
		thisWeekCal.setTimeInMillis(todayCal.getTimeInMillis());
		thisWeekCal.set(Calendar.DAY_OF_WEEK, thisWeekCal.getFirstDayOfWeek());
		Calendar thisMtdCal = DateUtils.truncate(now, Calendar.MONTH);
		int month = Calendar.getInstance().get(Calendar.MONTH);
		int quarter = (month) / 3;
		Calendar thisQuarterCal = Calendar.getInstance();
		thisQuarterCal.setTimeInMillis(thisMtdCal.getTimeInMillis());
		thisQuarterCal.set(Calendar.MONTH, quarter * 3);
		Calendar thisYtdCal = DateUtils.truncate(now, Calendar.YEAR);

		Date todayStart = todayCal.getTime();
		Date thisWeekStart = thisWeekCal.getTime();
		Date thisMtdStart = thisMtdCal.getTime();
		Date this1mStart = DateUtils.addMonths(todayStart, -1);
		Date thisQuarterStart = thisQuarterCal.getTime();
		Date thisYtdStart = thisYtdCal.getTime();
		Date this1yStart = DateUtils.addYears(todayStart, -1);
		Date this5yStart = DateUtils.addYears(todayStart, -5);

		final String TODAY = "1. 1d";
		final String WEEK = "2. 1w";
		final String MTD = "3. mtd";
		final String MONTH = "4. 1m";
		final String QUARTER = "5. 1q";
		final String YTD = "6. ytd";
		final String ONE_Y = "7. 1y";
		final String FIVE_Y = "8. 5y";
		final String ALL = "9. all";
		Map<String, Double> txMap = new LinkedHashMap<String, Double>() {
			private static final long serialVersionUID = 1L;
			{
				put(TODAY, 0.0);
				put(WEEK, 0.0);
				put(MTD, 0.0);
				put(MONTH, 0.0);
				put(QUARTER, 0.0);
				put(YTD, 0.0);
				put(ONE_Y, 0.0);
				put(FIVE_Y, 0.0);
				put(ALL, 0.0);
			}
		};

		// SimpleDateFormat dfdup = ATFormats.DATE_TIME_mid.get();
		SimpleDateFormat dfT = ATFormats.DATE_yyyy_MM_dd.get();

		Date logDate = DateUtils.addMonths(DateUtils.truncate(new Date(), Calendar.MONTH), -1);
		Map<IATTrade, String> errorMap = new HashMap<>();
		Map<String, List<ATTrade>> tradesMap = new HashMap<>();
		Map<String, Map<String, Double>> resultMap = new TreeMap<>();
		Date currentDate = null;
		// String STICKER = "CHTR 180323P00330000";
		for (IATTrade trade : trades) {
			try {
				String occId = trade.getOccId();
				// if (false && STICKER.equals(occId) && trade.getSzTrade() != null &&
				// trade.getSzTrade() != 0) {
				// log.info(String.format("%s: %s", occId, trade));
				// }
				String tradeId = String.format("%s:%s", occId, trade.getTradeAccount());
				currentDate = DateUtils.truncate(trade.getTsTrade(), Calendar.MONTH);
				Double currentValue = dateMap.get(currentDate);
				if (currentValue == null) {
					currentValue = 0.0;
				}
				double tradeAmount = 0;
				int tradeType = trade.getTradeType();
				if (IATTrade.TYPES_IMMEDIATE.contains(tradeType)) {
					double txValue = trade.getPxTradeTotal();
					if (currentDate.equals(logDate))
						log.debug(String.format("%s Immediate: %s -> %9.2f -> %9.2f -> %9.2f", dfT.format(currentDate),
								occId,
								trade.getPxTradeTotal(), txValue, currentValue + txValue));
					dateMap.put(currentDate, currentValue + txValue);
					tradeAmount = txValue;
				} else {
					boolean isSkip = false;
					if (!isSkip) {
						Pair<Double, Double> pair = tradeIdMap.get(tradeId);
						double newSize = trade.getSzTrade();
						double oldSize = pair == null ? 0 : pair.getLeft();
						double oldUnitPx = pair == null ? 0 : pair.getValue();
						boolean isClosing = IATTrade.TYPES_CLOSE.contains(tradeType);
						if (isClosing) {
							double delta = Math.abs(newSize) - Math.abs(oldSize);
							BigDecimal rbd = new BigDecimal(delta);
							rbd = rbd.setScale(4, RoundingMode.HALF_DOWN);// BigDecimal.ROUND_HALF_DOWN);
							delta = rbd.doubleValue();
							if (pair == null) {
								log.error(String.format("Closing missing open [%s]", occId));
								tradeIdMap.remove(tradeId);
							} else if (delta > 0) {
								log.error(String.format("Size mismatch open/close [%s - %.2f/%.2f]", occId, oldSize,
										newSize));
								tradeIdMap.remove(tradeId);
							} else {
								double txValue = 0;
								double updatedSize = oldSize + newSize;
								if (updatedSize == 0) {
									tradeIdMap.remove(tradeId);
									txValue = (oldSize * oldUnitPx) + trade.getPxTradeTotal();
								} else {
									tradeIdMap.put(tradeId, Pair.of(updatedSize, oldUnitPx));
									txValue = (-newSize * oldUnitPx) + trade.getPxTradeTotal();
								}
								dateMap.put(currentDate, currentValue + txValue);
								if (currentDate.equals(logDate))
									log.debug(String.format("%s Closing  : %s -> %9.2f -> %9.2f -> %9.2f",
											dfT.format(currentDate), occId,
											trade.getPxTradeTotal(), txValue, currentValue + txValue));
							}
						} else {
							if (currentDate.equals(logDate))
								log.debug(String.format("%s Opening  : %s -> %9.2f", dfT.format(currentDate), occId,
										trade.getPxTradeTotal()));
							double newUnitPx = ((oldSize * oldUnitPx) + trade.getPxTradeTotal()) / (oldSize + newSize);
							pair = Pair.of(oldSize + newSize, newUnitPx);
							tradeIdMap.put(tradeId, pair);
						}

						List<ATTrade> history = tradesMap.get(tradeId);
						if (history == null) {
							if (isClosing) {
								log.error(String.format("Closing trade missing opening [%s / %s]", occId,
										trade.getTradeAccount()));
								errorMap.put(trade,
										String.format("%s, %s, %.2f", "missing opening trade", trade.getTradeAccount(),
												trade.getPxTradeTotal()));
							} else {
								history = new ArrayList<>();
								tradesMap.put(tradeId, history);
							}
						}
						if (log.isDebugEnabled()) {
							if (thisWeekStart.compareTo(trade.getTsTrade()) <= 0) {
								log.debug(String.format("1w, %s, %b, %s, %6.2f, %8.2f",
										ATFormats.DATE_TIME_mid.get().format(trade.getTsTrade()),
										isClosing, trade.getOccId(), trade.getSzTrade(), trade.getPxTradeTotal()));
							}
						}
						if (!isClosing) {
							history.add(new ATTrade(trade));
						} else if (history != null) {
							double amount = 0;
							double closeSize = Math.abs(trade.getSzTrade());
							Iterator<ATTrade> i = history.iterator();
							while (i.hasNext()) {
								ATTrade openTrade = i.next();
								double openSize = Math.abs(openTrade.getSzTrade());
								double remaining = openSize - closeSize;
								BigDecimal rbd = new BigDecimal(remaining);
								rbd = rbd.setScale(4, RoundingMode.HALF_DOWN);// BigDecimal.ROUND_HALF_DOWN);
								remaining = rbd.doubleValue();
								if (remaining == 0) {
									amount += openTrade.getPxTradeTotal() + trade.getPxTradeTotal();
									i.remove();
									closeSize = 0;
									break;
								} else if (remaining > 0) {
									double pxOpenFraction = openTrade.getPxTradeTotal() * closeSize / openSize;
									amount += pxOpenFraction + trade.getPxTradeTotal();
									// openTrade = new ATTrade(openTrade);
									openTrade.setSzTrade(remaining);
									closeSize = 0;
									break;
								} else /* remaining < 0 */ {
									amount += openTrade.getPxTradeTotal() + trade.getPxTradeTotal();
									closeSize = Math.abs(remaining);
									i.remove();
								}
							}
							if (closeSize != 0) {
								log.error(String.format("Closing trade size mismatch opening [%s / %s]", occId,
										trade.getTradeAccount()));
								errorMap.put(trade,
										String.format("%s, %s, %.2f", "opening size mismatch", trade.getTradeAccount(),
												trade.getPxTradeTotal()));
							} else {
								tradeAmount = amount;
							}
						}
					}
				}

				// dateMap.put(currentDate, dateMap.get(currentDate) + tradeAmount);
				Date tradeDate = trade.getTsTrade();
				txMap.put(ALL, txMap.get(ALL) + tradeAmount);
				if (todayStart.compareTo(tradeDate) <= 0) {
					txMap.put(TODAY, txMap.get(TODAY) + tradeAmount);
				}
				if (thisWeekStart.compareTo(tradeDate) <= 0) {
					txMap.put(WEEK, txMap.get(WEEK) + tradeAmount);
				}
				if (thisMtdStart.compareTo(tradeDate) <= 0) {
					txMap.put(MTD, txMap.get(MTD) + tradeAmount);
				}
				if (this1mStart.compareTo(tradeDate) <= 0) {
					txMap.put(MONTH, txMap.get(MONTH) + tradeAmount);
				}
				if (thisQuarterStart.compareTo(tradeDate) <= 0) {
					txMap.put(QUARTER, txMap.get(QUARTER) + tradeAmount);
				}
				if (thisYtdStart.compareTo(tradeDate) <= 0) {
					txMap.put(YTD, txMap.get(YTD) + tradeAmount);
				}
				if (this1yStart.compareTo(tradeDate) <= 0) {
					txMap.put(ONE_Y, txMap.get(ONE_Y) + tradeAmount);
				}
				if (this5yStart.compareTo(tradeDate) <= 0) {
					txMap.put(FIVE_Y, txMap.get(FIVE_Y) + tradeAmount);
				}

				String symbol = occId.length() > 5 ? occId.substring(0, 6).trim() : occId.trim();
				Map<String, Double> symbolMap = resultMap.get(symbol);
				if (symbolMap == null) {
					symbolMap = new TreeMap<>();
					symbolMap.put(CASH_KEY, 0.0);
					resultMap.put(symbol, symbolMap);
				}
				Double cash = symbolMap.get(CASH_KEY);
				symbolMap.put(CASH_KEY, cash + trade.getPxTradeTotal());
				if ((trade.getSzTradeActive() != null && trade.getSzTradeActive() == 0) || trade.getSzTrade() == null)
					continue;
				Double quantity = symbolMap.get(occId);
				Double newQuantity = quantity == null ? trade.getSzTrade() : quantity + trade.getSzTrade();
				if (newQuantity == 0) {
					symbolMap.remove(occId);
				} else {
					symbolMap.put(occId, newQuantity);
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing [%s]", trade), ex);
			}
		}

		Set<String> remainingTrades = new TreeSet<>();
		SimpleDateFormat dfE = ATFormats.DATE_yyyyMMdd.get();
		String edStr = dfE.format(new Date()).substring(2);
		for (Entry<String, List<ATTrade>> e : tradesMap.entrySet()) {
			if (e.getValue().isEmpty())
				continue;
			String key = e.getKey();
			if (key.length() > 16) {
				String dStr = key.substring(6, 12);
				if (dStr.compareTo(edStr) < 0)
					continue;
			}
			Double count = e.getValue().stream().map(v -> v.getSzTrade()).reduce(0.0, Double::sum);
			log.info(String.format("# Remain: %s -> %d -> %.2f", key, e.getValue().size(), count));
			String newKey = String.format("%s:%.2f", key, count);
			remainingTrades.add(newKey);
		}
		Map<String, Set<String>> currentTrades = new TreeMap<>();
		for (String t : remainingTrades) {
			String[] tokens = t.split(":");
			Set<String> accTrades = currentTrades.get(tokens[1]);
			if (accTrades == null) {
				accTrades = new TreeSet<>();
				currentTrades.put(tokens[1], accTrades);
			}
			accTrades.add(String.format("%s(%s)", tokens[0], tokens[2]));
		}
		log.info(String.format("Remaining trades: %s", currentTrades));

		for (String tradeId : tradeIdMap.keySet()) {
			try {
				String occId = tradeId.split(":")[0];
				if (occId.length() <= 6)
					continue;
				Pair<Double, Double> pair = tradeIdMap.get(tradeId);
				double txValue = pair.getValue() * pair.getKey();
				String dStr = "20" + occId.substring(6, 12);
				Date d = dfE.parse(dStr);
				Date dt = DateUtils.truncate(d, Calendar.MONTH);
				Double currentValue = dateMap.get(dt);
				double newValue = txValue + (currentValue == null ? 0 : currentValue);
				dateMap.put(dt, newValue);
				// if ("CTL 190719C00012000".equals(occId)) {
				// log.info("CTL 190719C00012000");
				// }
				if (currentDate.equals(logDate))
					log.debug(
							String.format("%s Expiring : %s -> %9.2f -> %9.2f -> %9.2f", dfT.format(currentDate), occId,
									txValue, txValue, newValue));
			} catch (Exception ex) {
				log.error(String.format("Error parsing [%s]", tradeId), ex);
			}
		}
		if (!errorMap.isEmpty()) {
			log.error(String.format("Error on trades: %s", errorMap));
		} else {
			log.debug(String.format("No errors detected"));
		}
		resultMap.put("@AGG", txMap);
		log.info(String.format("Transactions: %s", txMap));
		Map<String, Double> dMap = new TreeMap<>();
		SimpleDateFormat df = ATFormats.DATE_yyMMdd.get();
		dateMap.forEach(new BiConsumer<Date, Double>() {
			@Override
			public void accept(Date date_, Double value_) {
				dMap.put(df.format(date_), value_);
			}
		});
		resultMap.put("@DATE", dMap);
		log.info(String.format("Date tx: %s", dMap));
		return resultMap;
	}

	private void performanceSchedule(String userId_) {
		try {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			log.info(String.format("Start perf { 'user': '%s' }", userId_));

			//	final String STICKER = "QTNT";

			// 1. Get and prepare data
			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
			List<IATTrade> trades = _mongodb.getTrades(query);
			trades.sort(new Comparator<IATTrade>() {
				@Override
				public int compare(IATTrade a_, IATTrade b_) {
					int tsComp = a_.getTsTrade().compareTo(b_.getTsTrade());
					if (tsComp != 0)
						return tsComp;
					Integer aO = IATTrade.TYPES_OPEN.contains(a_.getTradeType()) ? -1 : 1;
					Integer bO = IATTrade.TYPES_OPEN.contains(b_.getTradeType()) ? -1 : 1;
					return aO.compareTo(bO);
				}
			});
			log.info(String.format("User [%s] loaded [%d] trades", userId_, trades.size()));

			// 2. Sort into open, close and immediate
			int closeCount = 0;
			int immediateCount = 0;
			int openCount = 0;
			int otherCount = 0;
			Date currentDate = null;
			Map<Date, Double> dateAmountMap = new TreeMap<>();
			Map<String, List<Map<String, Object>>> openMap = new HashMap<>();
			Map<String, List<Map<String, Object>>> closeMap = new HashMap<>();
			for (IATTrade trade : trades) {
				String tradeId = String.format("%s:%s", trade.getOccId(), trade.getTradeAccount());
				// if (tradeId.startsWith(STICKER)) {
				// log.info(String.format("%s", STICKER));
				// }
				currentDate = DateUtils.truncate(trade.getTsTrade(), Calendar.MONTH);
				if (IATTrade.TYPES_IMMEDIATE.contains(trade.getTradeType())) {
					update(dateAmountMap, currentDate, trade.getPxTradeTotal());
					immediateCount++;
				} else if (IATTrade.TYPES_OPEN.contains(trade.getTradeType())) {
					List<Map<String, Object>> openList = openMap.get(tradeId);
					if (openList == null) {
						openList = new ArrayList<>();
						openMap.put(tradeId, openList);
					}
					openList.add(trade.getAsMap());
					openCount++;
				} else if (IATTrade.TYPES_CLOSE.contains(trade.getTradeType())) {
					List<Map<String, Object>> closeList = closeMap.get(tradeId);
					if (closeList == null) {
						closeList = new ArrayList<>();
						closeMap.put(tradeId, closeList);
					}
					closeList.add(trade.getAsMap());
					closeCount++;
				} else {
					log.info(String.format("Unknown trade type [%s / %d]", trade.getOccId(), trade.getTradeType()));
					otherCount++;
				}
			}
			log.debug(String.format("User [%s] {'open': %d, 'close': %d, 'immediate': %d, 'other': %d, 'total': %d}",
					userId_, openCount, closeCount,
					immediateCount, otherCount, openCount + closeCount + immediateCount + otherCount));

			// int total = openMap.entrySet().stream().map(e ->
			// e.getValue().size()).reduce(0, Integer::sum);
			// 3. Match opened with closed and expired
			openCount = 0;
			closeCount = 0;
			int unClosedCount = 0;
			int expiredCount = 0;
			int remainingCount = 0;

			List<Map<String, Object>> missingExpired = new ArrayList<>();
			for (String tradeId : openMap.keySet()) {
				String occId = tradeId.split(":")[0];
				// if (occId.equals(STICKER)) {
				// log.info(String.format("%s", STICKER));
				// }
				List<Map<String, Object>> openList = openMap.get(tradeId);
				List<Map<String, Object>> closeList = closeMap.get(tradeId);
				if (closeList == null || closeList.isEmpty()) {
					Date dExp = ATTradeModelTool.getExpiration(occId);
					if (dExp != null && today.after(dExp)) {
						expiredCount += openList.size();
						missingExpired.addAll(openList);
						openList.clear();
					} else {
						unClosedCount += openList.size();
					}
					// int expCount = helpExpire(occId, openList, today);
					// if (expCount == 0) {
					// unClosedCount += openList.size();
					// } else {
					// openList.clear();
					// expiredCount += expCount;
					// }
					continue;
				}
				Iterator<Map<String, Object>> openI = openList.iterator();
				while (openI.hasNext()) {
					openCount++;
					Map<String, Object> openT = openI.next();
					Iterator<Map<String, Object>> closeI = closeList.iterator();
					while (closeI.hasNext()) {
						Map<String, Object> closeT = closeI.next();
						double szOpen = getTempSize(openT);
						double szClose = getTempSize(closeT);
						// double remaining = szOpen + szClose;
						double sgnOpen = Math.signum(szOpen);
						double sgnClose = Math.signum(szClose);
						if (sgnOpen == sgnClose) {
							log.error(String.format("Symmetric Open/Close size [%s]", tradeId));
							continue;
						}
						double delta = Math.abs(szOpen) - Math.abs(szClose);
						BigDecimal rbd = new BigDecimal(delta);
						rbd = rbd.setScale(4, RoundingMode.HALF_DOWN);// BigDecimal.ROUND_HALF_DOWN);
						delta = rbd.doubleValue();
						double remaining = sgnOpen * delta;
						if (delta <= 0) {
							log.debug(String.format("Found match [%s / %.2f]", occId, szOpen));
							openI.remove();
							// openT.put(REMAINING, 0.0);
							if (delta == 0) {
								closeI.remove();
							} else {
								closeT.put(REMAINING, remaining);
							}
							closeCount++;
							break;
						} else {
							log.debug(String.format("Remaining  [%s / %.2f]", occId, delta));
							closeI.remove();
							openT.put(REMAINING, remaining);
							remainingCount++;
						}
					}
					// openT.remove(REMAINING);
				}
			}
			log.debug(String.format(
					"User [%s] {'open': %d, 'closed': %d, 'unClosed': %d, 'expired': %d, 'remain': %d, 'total': %d}",
					userId_,
					openCount,
					closeCount, unClosedCount, expiredCount, remainingCount,
					openCount + closeCount + unClosedCount + expiredCount + remainingCount));
			openCount = openMap.entrySet().stream().mapToInt(e -> e.getValue().size()).sum();
			closeCount = closeMap.entrySet().stream().mapToInt(e -> e.getValue().size()).sum();

			List<String> ignoreList = Arrays.asList("----"/*
															 * , "A     190816P00047500", "A     190816P00057500",
															 * "AAL   190816C00024000",
															 * "AAL   190816C00025000", "AAL   191220C00030000", "AAL",
															 * "AAPL",
															 * "ADT   200117C00010000",
															 * "ADT   200117C00012500", "AG",
															 * "AG    191220C00006000", "AMZN", "ANET  190621P00227500",
															 * "ANET  190621P00230000",
															 * "ANET  190920P00185000", " ANET  190920P00195000",
															 * "ARAV", "ARKG", "ATVI  191115C00032500", "ATVI"
															 */);
			// StringBuffer openBuffer = new StringBuffer();
			Map<String, Double> openCountMap = new TreeMap<>();
			List<String> openList = new ArrayList<>();
			Double zero = Double.valueOf(0.0);
			for (List<Map<String, Object>> tList : openMap.values()) {
				if (tList == null)
					continue;
				Double szOccId = null;
				for (Map<String, Object> t : tList) {
					String occId = IATTrade.getOccId(t);
					Double szT = IATTrade.getSzTrade(t);
					if (ignoreList.contains(occId) || zero.equals(szT))
						continue;
					String acc = IATTrade.getTradeAccount(t);
					Double szA = IATTrade.getSzTradeActive(t);
					Double szR = (Double) t.get(REMAINING);
					double rem = szR != null ? szR : szT;
					// if (szR != null) {
					szOccId = szOccId == null ? rem : szOccId + rem;
					// }
					String tStr = String.format("%s, %s, %.2f, %.2f, %.2f, %.2f", occId, acc,
							szA != null ? szA : Double.NaN, szT, szR, szOccId);
					openList.add(tStr);
					openCountMap.put(String.format("%s, %s", occId, acc), szOccId);
					// openBuffer.append(tStr).append(System.lineSeparator());
				}
			}
			Collections.sort(openList);
			// StringBuffer closeBuffer = new StringBuffer();
			List<String> closeList = new ArrayList<>();
			for (List<Map<String, Object>> tList : closeMap.values()) {
				if (tList == null)
					continue;
				Double szOccId = null;
				for (Map<String, Object> t : tList) {
					String occId = IATTrade.getOccId(t);
					Double szT = IATTrade.getSzTrade(t);
					if (ignoreList.contains(occId) || zero.equals(szT))
						continue;
					String acc = IATTrade.getTradeAccount(t);
					Double szA = IATTrade.getSzTradeActive(t);
					Double szR = (Double) t.get(REMAINING);
					if (szR != null) {
						szOccId = szOccId == null ? szR : szOccId + szR;
					}
					String tStr = String.format("%s, %s, %.2f, %.2f, %.2f, %.2f", occId, acc,
							szA != null ? szA : Double.NaN, szT, szR, szOccId);
					closeList.add(tStr);
					// closeBuffer.append(tStr).append(System.lineSeparator());
				}
			}
			Collections.sort(closeList);
			// String openStr = openBuffer.toString();
			// String closeStr = closeBuffer.toString();

			// String openStr = String.join(System.lineSeparator(), openList);
			String openStr = openCountMap.entrySet().stream()
					.map(e -> String.format("%s, %.2f", e.getKey(), e.getValue()))
					.collect(Collectors.joining(System.lineSeparator()));

			log.info(String.format("MExp : %s", missingExpired));
			log.info(String.format("Skip : %s", ignoreList));
			log.info(String.format("Open : %s", openStr));
			log.info(String.format("Close: %s", closeList));
			log.info(String.format("Done perf { 'user': '%s', 'count': %d }", userId_, trades.size()));
		} catch (Exception ex) {
			log.error("Error processing performance", ex);
		}
	}

	private Double getTempSize(Map<String, Object> trade_) {
		Double value = (Double) trade_.get(REMAINING);
		if (value == null) {
			value = IATTrade.getSzTrade(trade_);
		}
		return value;
	}

	private void update(Map<Date, Double> map_, Date date_, Double value_) {
		Double current = map_.get(date_);
		if (current == null) {
			map_.put(date_, value_);
		} else {
			map_.put(date_, current + value_);
		}
	}

	@Deprecated
	protected void syncSheets(String userId_, List<IATTrade> dbTrades_, Set<IATTrade> legacyTrades_,
			Set<IATTrade> sheetTrades_) {
		if (legacyTrades_ == null || legacyTrades_.isEmpty()) {
			log.info(String.format("No legacy sheet portfolio [%s]", userId_));
		} else {
			AtomicLong hash = new AtomicLong(0);
			legacyTrades_.forEach(t_ -> hash.addAndGet(ATTradeModelTool.hashString(t_, true).hashCode()));
			if (hash.get() != _legacySheetHash) {
				_legacySheetHash = hash.get();
				log.info(String.format("Syncing legacy sheet portfolio [%s]...", userId_));
				_mongodb.updateTrades(userId_, legacyTrades_, false, true);
			}
		}

		if (sheetTrades_ == null || sheetTrades_.isEmpty()) {
			log.info(String.format("No sheet portfolio [%s]", userId_));
		} else {
			AtomicLong hash = new AtomicLong(0);
			sheetTrades_.forEach(t_ -> hash.addAndGet(ATTradeModelTool.hashString(t_, true).hashCode()));
			if (hash.get() != _sheetHash) {
				_sheetHash = hash.get();
				log.info(String.format("Syncing sheet portfolio [%s]...", userId_));
				_mongodb.updateTrades(userId_, sheetTrades_, false, false);
			}
		}
	}

	public Map<String, ATTradeDigest> digestTrades(String userId_, String account_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query()
				.eq(IATTrade.TRADE_USER, userId_).and().eq(IATTrade.UNDERLYING, symbol_);
		List<ATTrade> dbTrades = _mongodb.getTradeLedger(query);
		List<ATTrade> trades = dbTrades == null || dbTrades.isEmpty()
				? new ArrayList<>()
				: dbTrades.stream().sorted(_tradeComparator).collect(Collectors.toList());

		cumulateTrades(userId_, trades);

		List<ATTrade> accountTrades = new ArrayList<>(trades);
		accountTrades.removeIf(t_ -> !account_.equals(t_.getTradeAccount()));

		log.info(String.format("digestTrades Processing [%s:%s:%s]: %d", userId_, account_, symbol_,
				accountTrades.size()));
		Map<String, ATTradeDigest> digestMap = ATTradeDigester.digestTrades(userId_, account_, symbol_, accountTrades);
		int count = _mongodb.saveTradeDigest(userId_, account_, symbol_, digestMap.values());
		log.debug(String.format("digestTrades Saved      [%s:%s:%s]: %d", userId_, account_, symbol_, count));
		return digestMap;
	}

	public boolean cumulateTrades(String userId_, Collection<ATTrade> trades_) {
		boolean result = false;
		Double pxCum = ATTradeCumulator.cumulateTrades(userId_, trades_);
		if (pxCum != null) {
			String symbol = ATModelUtil.getSymbol(trades_.iterator().next().getOccId());
			ATTrade cum = new ATTrade();
			cum.setAtId(String.format("%s:%s", userId_, symbol));
			cum.setOccId(symbol);
			cum.setPxNet(Precision.round(pxCum, 4));
			cum.setTradeUserId(userId_);
			cum.setTsTrade(new Date());
			result = _mongodb.saveTradeCumulative(cum);
		}
		return result;
	}

	@Override
	public List<ATTradeDigest> getTradeDigest(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		List<ATTradeDigest> digests = _mongodb.getTradeDigest(query);
		return digests;
	}

	@Override
	public ATTrade getTradeCumulative(String userId_, String symbol_) {
		ATTrade trade = _mongodb.getTradeCumulative(userId_, symbol_);
		return trade;
	}

	public ArrayList<ATTradeGroup> groupTrades(String userId_, String account_, String symbol_,
			Collection<ATTradeDigest> digests_) {
		ArrayList<ATTradeGroup> groups = ATTradeGrouper.groupTrades(userId_, account_, symbol_, digests_);
		long count = _mongodb.saveTradeGroups(userId_, account_, symbol_, groups);
		log.info(String.format("groupTrades Saved [%s:%s:%s]: %d", userId_, account_, symbol_, count));
		return null;
	}

	@Override
	public List<ATTradeGroup> getTradeGroups(String userId_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		List<ATTradeGroup> groups = _mongodb.getTradeGroups(query);
		List<ATTradeDigest> digestList = getTradeDigest(userId_, symbol_);
		if (groups == null || groups.isEmpty() || digestList == null || digestList.isEmpty())
			return groups;
		Map<String, Map<String, List<ATTradeDigest>>> digestMap = digestList.stream()
				.collect(Collectors.groupingBy(ATTradeDigest::getAccount,
						Collectors.groupingBy(ATTradeDigest::getUnderlying)));
		long MILLS_IN_YEAR = 1000L * 60 * 60 * 24 * 365;
		for (ATTradeGroup group : groups) {
			String symbol = group.getUnderlying();
			String account = group.getAccount();
			try {
				Long tradeMillis = group.getTsTrade();
				if (tradeMillis != null) {
					tradeMillis = System.currentTimeMillis() - tradeMillis;
				}
				Map<String, Double> ratios = group.getRatios();
				List<ATTradeDigest> digests = digestMap.get(account).get(symbol);
				Set<String> keys = ratios.keySet();
				digests = digests != null
						? digests.stream().filter(d_ -> keys.contains(d_.getOccId())).collect(Collectors.toList())
						: null;
				if (digests == null || digests.isEmpty()) {
					log.warn(String.format("getTradeGroups NULL/empty digest [%s / %s]", userId_, symbol));
					continue;
				}
				String strt = group.getStrategy();
				switch (strt) {
					case IATTrade.STRATEGY_CC: {
						ATTradeDigest d1 = digests.get(0);
						ATTradeDigest d2 = digests.get(1);
						double sz = d1.getSzNet();
						ATTradeDigest call = sz < 0 ? d1 : d2;
						Double pxMktEqt = getPx(symbol, false, false);
						Double pxMktCall = getPx(call.getOccId(), false, false);
						if (pxMktCall != null && pxMktEqt != null) {
							double szEff = 100 * Math.abs(call.getSzEff());
							double valMid = Precision.round(szEff * (pxMktEqt - pxMktCall), 2);
							group.setValueMid(valMid);
							double pxNet = group.getPxNet();
							if (pxNet != 0) {
								double pxStrk = ATModelUtil.getStrike(call.getOccId());
								double pxYldMax = szEff * pxStrk + pxNet;
								double pxYldCurr = valMid + pxNet;
								double yld = Precision.round(100 * pxYldCurr / pxYldMax, 2);
								group.setYieldPct(yld);
							}
							double riskLoss = (pxMktEqt < pxMktCall) && (valMid < pxNet) ? -1 : 1;
							group.setRiskLoss(riskLoss);
							if (tradeMillis != null) {
								double valPaid = Math.abs(pxNet);
								double yieldTot = (valMid - valPaid) / valPaid;
								double yieldApy = yieldTot * MILLS_IN_YEAR / tradeMillis;
								group.setYieldApy(Precision.round(100 * yieldApy, 2));
							}
						}
						break;
					}
					case IATTrade.STRATEGY_LVCS:
					case IATTrade.STRATEGY_SVPS:
					case IATTrade.STRATEGY_SVCS:
					case IATTrade.STRATEGY_LVPS: {
						Double valMid = null;
						double pxAbs = Math.abs(group.getPxNet());
						ATTradeDigest d1 = digests.get(0);
						ATTradeDigest d2 = digests.get(1);
						double sz = d1.getSzNet();
						ATTradeDigest tLong = sz > 0 ? d1 : d2;
						ATTradeDigest tShort = sz < 0 ? d1 : d2;
						Double pxMktLong = getPx(tLong.getOccId(), true, false);
						Double pxMktShort = getPx(tShort.getOccId(), true, false);
						double pxStrkLong = ATModelUtil.getStrike(tLong.getOccId());
						double pxStrkShort = ATModelUtil.getStrike(tShort.getOccId());
						double szEff = Math.abs(sz) * ratios.get(d1.getOccId());
						if (pxMktShort != null && pxMktLong != null) {
							valMid = Precision.round(100 * szEff * (pxMktLong - pxMktShort), 2);
							group.setValueMid(valMid);
							valMid = Math.abs(valMid);
							if (pxAbs != 0) {
								var factor = (strt == IATTrade.STRATEGY_LVCS || strt == IATTrade.STRATEGY_LVPS)
										? 1.0
										: -1.0;
								double yldPct = factor * (valMid - pxAbs) / pxAbs;
								group.setYieldPct(Precision.round(100 * yldPct, 2));
								if (tradeMillis != null) {
									var valPaid = Math.abs(group.getPxNet());
									double yieldTot = (Math.abs(valMid) - valPaid) / valPaid;
									double yieldApy = factor * yieldTot * MILLS_IN_YEAR / tradeMillis;
									group.setYieldApy(Precision.round(100 * yieldApy, 2));
								}
							}
						}
						Double pxUl = getPx(symbol, true, true);
						switch (strt) {
							case IATTrade.STRATEGY_LVCS:
							case IATTrade.STRATEGY_LVPS:
								if (valMid != null && pxUl > 0) {
									int longF = IATTrade.STRATEGY_LVPS.equals(strt) ? 1 : -1;
									double riskLossL = pxAbs > valMid
											? longF * (pxUl > pxStrkLong ? -1 : 1)
											: 1;
									group.setRiskLoss(riskLossL);
								}
								break;
							case IATTrade.STRATEGY_SVCS:
							case IATTrade.STRATEGY_SVPS:
								if (valMid != null && pxUl > 0) {
									double shortF = IATTrade.STRATEGY_SVPS.equals(strt) ? 1 : -1;
									double riskLossS = pxAbs < valMid
											? shortF * (pxUl < pxStrkShort ? -1 : 1)
											: 1;
									group.setRiskLoss(riskLossS);
								}
								break;
						}
						break;
					}
					case IATTrade.STRATEGY_LC:
					case IATTrade.STRATEGY_SC:
					case IATTrade.STRATEGY_LP:
					case IATTrade.STRATEGY_SP:
					case IATTrade.STRATEGY_LE:
					case IATTrade.STRATEGY_SE: {
						ATTradeDigest trade = digests.get(0);
						String occId = trade.getOccId();
						Double pxUl = getPx(symbol, false, false);
						Double pxMkt = symbol.equals(occId) ? pxUl : getPx(occId, false, true);
						double ratio = ratios.get(occId);
						Double pxNet = trade.getPxNet() * ratio;
						Double szEff = trade.getSzNet() * ratio;
						if (pxMkt != null && pxMkt != 0 && pxNet != 0) {
							double valMid = szEff * pxMkt * (occId.length() < 10 ? 1 : 100);
							group.setValueMid(Precision.round(valMid, 2));
							double yldPct = (valMid + pxNet) / Math.abs(pxNet);
							group.setYieldPct(Precision.round(100 * yldPct, 2));
							if (tradeMillis != null) {
								var factor = szEff < 0 ? -1.0 : 1.0;
								var valPaid = Math.abs(group.getPxNet());
								double yieldTot = (valMid - valPaid) / valPaid;
								double yieldApy = factor * yieldTot * MILLS_IN_YEAR / tradeMillis;
								group.setYieldApy(Precision.round(100 * yieldApy, 2));
							}
						}
						if (pxUl != null && pxUl > 0) {
							double sign = Math.signum(szEff);
							String right = ATModelUtil.getRight(occId);
							if (right == null) {
								right = "";
							}
							switch (right) {
								case IATTrade.CALL:
									group.setRiskLoss(sign * (pxUl > ATModelUtil.getStrike(occId) ? 1 : -1));
									break;
								case IATTrade.PUT:
									group.setRiskLoss(sign * (pxUl < ATModelUtil.getStrike(occId) ? 1 : -1));
									break;
								default:
									group.setRiskLoss(sign * (pxUl > Math.abs(pxNet / szEff) ? 1 : -1));
									break;
							}
						}
						break;
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error [%s]", symbol), ex);
			}
		}
		return groups;

	}

	static private Double getPx(String occId_, boolean isDefaultZero_, boolean isFallbackLast_) {
		IATMarketDataService mktSvc = ATServiceRegistry.getMarketDataService();
		ATMarketData mkt = mktSvc.getMarketData(occId_);
		Double px = null;
		if (mkt != null) {
			px = mkt.getPxMid();
			Long tsLast = mkt.getTsLast();
			// NOTE: Overrides with PX LAST if (a) flag is set or (b) last px less than 5
			// min ago
			if (px == null && (isFallbackLast_ || (tsLast != null && System.currentTimeMillis() - tsLast < 300000))) {
				px = mkt.getPxLast();
			}
		}
		if (isDefaultZero_ && px == null) {
			px = 0.0;
		}
		return px;
	}

	// @Override
	// public void notify(EATChangeOperation operation_, IATTrade trade_) {
	// 	try {
	// 		if (!_isInitialized || !_isStreaming || _listeners == null)
	// 			return;
	// 		log.debug(String.format("Notify trade [%s]: %s", operation_, trade_));
	// 		_listeners.forEach(l -> {
	// 			try {
	// 				l.notify(operation_, trade_);
	// 			} catch (Exception ex) {
	// 				log.error(String.format("Error notifying trade listener [%s]", l), ex);
	// 			}
	// 		});
	// 	} catch (Exception ex) {
	// 		log.error("Error in notify", ex);
	// 	}
	// }

	@Override
	public void notify(EATChangeOperation operation_, String occId_) {
		try {
			if (!_isInitialized || !_isStreaming || _totalListeners == null)
				return;
			log.debug(String.format("Notify total positions [%s]: [%s]", operation_, occId_));
			_totalListeners.forEach(l -> {
				try {
					l.notify(operation_, occId_);
				} catch (Exception ex) {
					log.error(String.format("Error notifying total position listener [%s]", l), ex);
				}
			});
		} catch (Exception ex) {
			log.error("Error in notify", ex);
		}
	}

	// private void shutdown() {
	// log.debug(String.format("Shutting down"));
	// if (_listeners != null)
	// _listeners.clear();
	// if (_mongodb != null)
	// _mongodb.shutdown();
	// // if (_executor != null)
	// // _executor.shutdownNow();
	// }

	@Override
	public long expireOptions() {
		return _mongodb.expireOptions();
	}

	@Override
	public IATPortfolio updatePortfolio(String userId_, String account_, String ul_) {
		try {
			IATWhereQuery trdQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_)
					.eq(IATAssetConstants.TRADE_ACCOUNT,
							account_)
					.eq(IATTrade.UNDERLYING, ul_);
			// if("AMD".equals(ul_)) {
			// log.info(ul_);
			// }
			List<IATTrade> trades = _mongodb.getTrades(trdQuery);
			trades.sort((t1, t2) -> t1.getTsTrade2().compareTo(t2.getTsTrade2()));

			Map<String, ATPortfolio> prtMap = new TreeMap<>();
			String ulKey = String.format("%s:%s:%s", userId_, account_, ul_);
			ATPortfolio ulPrt = new ATPortfolio(userId_, account_, ul_);
			prtMap.put(ulKey, ulPrt);

			for (IATTrade trade : trades) {
				ulPrt.setTs(trade.getTsTrade());
				Double sz_trd_tmp = trade.getSzTrade();
				double sz_trd = sz_trd_tmp != null ? sz_trd_tmp : 0;
				Double px_trd_tmp = trade.getPxTradeTotal();
				double px_trd = px_trd_tmp != null ? px_trd_tmp : 0;

				if (ul_.equals(trade.getOccId())) {
					double sz_new = Precision.round(ulPrt.getSz() + sz_trd, 4);
					double px_new = Precision.round(sz_new == 0 ? 0 : ulPrt.getPxTradeTotal() + px_trd, 4);
					double px_ul_acc = Precision.round(ulPrt.getPxAccumulated() + px_trd, 4);
					ulPrt.setSz(sz_new);
					ulPrt.setPxAccumulated(px_ul_acc);
					ulPrt.setPxTradeTotal(px_new);
				} else {
					String key = String.format("%s:%s:%s", userId_, account_, trade.getOccId());
					ATPortfolio trdPrt = prtMap.get(key);
					if (trdPrt == null) {
						trdPrt = new ATPortfolio(userId_, account_, trade.getOccId());
						trdPrt.setPxAccumulated(null);
						prtMap.put(key, trdPrt);
					}
					double px_ul_acc = Precision.round(ulPrt.getPxAccumulated() + px_trd, 4);
					ulPrt.setPxAccumulated(px_ul_acc);

					double sz_new = Precision.round(trdPrt.getSz() + sz_trd, 4);
					if (sz_new == 0) {
						prtMap.remove(key);
					} else {
						double px_new = Precision.round(trdPrt.getPxTradeTotal() + px_trd, 4);
						trdPrt.setSz(sz_new);
						trdPrt.setPxTradeTotal(px_new);
						prtMap.put(key, trdPrt);
					}
				}
				// log.debug(String.format("Trade: %1$-21s: %2$tF:%2$tT", trade.getOccId(),
				// trade.getTsTrade()));
			}
			@SuppressWarnings("unchecked")
			Collection<IATPortfolio> portfolio = (Collection<IATPortfolio>) (Object) (prtMap.values());
			_mongodb.updatePortfolios(userId_, account_, ul_, portfolio);
			return ulPrt;
		} catch (Exception ex_) {
			log.error(String.format("Error updating portfolio [%s, %s, %s]", userId_, account_, ul_), ex_);
			return null;
		}
	}

	@Override
	public Map<String, Object> importTrades(String userId_, ATTrade[] trades_, boolean isPurgeExisting_) {
		Map<String, Object> resultMap = new HashMap<>();
		if (isPurgeExisting_) {
			long deleteTradeCount = _mongodb.deleteTrades(userId_);
			resultMap.put("trades.deleted", deleteTradeCount);

		}

		int tradeCount = trades_ != null ? trades_.length : 0;
		int tradeSaveCount = 0;
		if (trades_ != null && trades_.length != 0) {
			Map<String, Set<String>> accountSymbolMap = new TreeMap<>();
			for (ATTrade trade : trades_) {
				trade.setTradeUserId(userId_);
				String id = ATTradeModelTool.atId(trade);
				trade.setAtId(id);

				if (_mongodb.updateTrade(userId_, id, trade)) {
					tradeSaveCount++;
				}

				Set<String> accountSymbols = accountSymbolMap.get(trade.getTradeAccount());
				if (accountSymbols == null) {
					accountSymbols = new TreeSet<>();
					accountSymbolMap.put(trade.getTradeAccount(), accountSymbols);
				}
				accountSymbols.add(ATModelUtil.getSymbol(trade.getOccId()));
			}
			for (String account : accountSymbolMap.keySet()) {
				Set<String> symbols = accountSymbolMap.get(account);
				for (String symbol : symbols) {
					updatePortfolio(userId_, account, symbol);
					updateAssets(userId_, account, symbol);
				}
			}
		}
		resultMap.put("trades.inserted",
				tradeSaveCount == tradeCount ? tradeSaveCount : String.format("%d/%d", tradeSaveCount, tradeCount));

		return resultMap;
	}

}
