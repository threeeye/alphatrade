package com.isoplane.at.portfolio.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.MathUtils;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPositionUpdater {

    static final Logger log = LoggerFactory.getLogger(ATPositionUpdater.class);

    private static ATPositionUpdater instance;

    protected ATPortfolioMogodbTools _mongodb;

    public static void main(String[] args_) {
        if (args_ == null || args_.length < 1) {
            throw new ATException(
                    String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
        }
        String root = System.getProperty("alphatradePath");
        ATConfigUtil.init(root, args_[0], true);

        //    ATMongoTableRegistry.setTestMode(true);

        try {
            var db = new ATPortfolioMogodbTools(false, null);
            instance = new ATPositionUpdater(db);
            instance.initStandalone();

            //      instance.resetAllPositions();
              instance.resetUserPositions("ghzpjrSuVVbkkzioHDs7ynpBgck2");
        //      instance.resetUserPositions("TdW8PcdGC9NvNl17kebhqdf5oNX2");
//            instance.resetUserAccountPositions("TdW8PcdGC9NvNl17kebhqdf5oNX2", "FID SEP");
            //   instance.resetUserAccountSymbolPositions("TdW8PcdGC9NvNl17kebhqdf5oNX2", "FID SEP", "AAL");
        } catch (Exception ex) {
            log.error(String.format("Error"), ex);
        } finally {
            System.exit(0);
        }
    }

    public ATPositionUpdater(ATPortfolioMogodbTools mongodb_) {
        this._mongodb = mongodb_;
    }

    public void initStandalone() {
        _mongodb = new ATPortfolioMogodbTools(false, null);
        _mongodb.init();
    }

    /**
    * Rolls up all trades of referenced occIds into current positions.
    * @param userId_ - User id
    * @param accountId_ - Account id
    * @param occIds_ - OccIds to roll up
    * @return List of current positions; NULL if none.
    */
    public Collection<ATPosition> updatePositions(String userId_, String accountId_, Collection<String> occIds_) {
        if (StringUtils.isBlank(userId_)) {
            log.error(String.format("Missing userId"));
            return null;
        }
        if (StringUtils.isBlank(accountId_)) {
            log.error(String.format("Missing account id"));
            return null;
        }
        if (occIds_ == null || occIds_.isEmpty()) {
            log.error(String.format("Missing occIds"));
            return null;
        }
        var occIds = new HashSet<>(occIds_);
        var symbol = ATModelUtil.getSymbol(occIds_.iterator().next());
        occIds.add(symbol);
        var prtQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_)
                .eq(IATAssetConstants.TRADE_ACCOUNT, accountId_).in(IATTrade.OCCID, occIds);
        var allTrades = _mongodb.getTradeLedger(prtQuery);
        allTrades.sort(Comparator.comparing(ATTrade::getTsTrade2));
        var activeHolderMap = new HashMap<String, PosHolder>();
        var closedHolderMap = new HashMap<String, PosHolder>();
        var positionMap = new TreeMap<String, ATPosition>();
        for (ATTrade trade : allTrades) {
            var occId = trade.getOccId();
            var isUnderlying = occId.length() < 10;
            var tradeSymbol = ATModelUtil.getSymbol(occId);
            if (!symbol.equals(tradeSymbol)) {
                throw new ATException(String.format("Mixed symbols [%s / %s]", symbol, tradeSymbol));
            }

            var tsTrd = trade.getTsTrade2();
            var tradeSize = trade.getSzTrade();
            if (tradeSize == null) {
                tradeSize = 0.0;
            }
            var tradeTotal = trade.getPxTradeTotal();

            var pos = positionMap.get(occId);
            var count = pos == null ? 0.0 : pos.getSize();
            var total = pos == null ? 0.0 : pos.getPxTradeTotal();

            if (count != 0 && tradeSize != 0 && count + tradeSize != 0 && tradeTotal == null) {
                tradeTotal = tradeSize * total / count;
            }
            count += tradeSize;
            if (count != 0 && tradeTotal == null) {
                log.error(String.format("Invalid ledger: %d / %s / %s / %s", tsTrd, occId, userId_, accountId_));
            }
            total = count == 0
                    ? 0
                    : total + tradeTotal;

            /* 
            * NOTE: Adjusts underlying positions with option returns following these rules:
            * 1. Only considers options opened and closed on or after underlying trade.
            * 2. Only considers option quantities less or equal to matching underlying positions
            */
            if (!isUnderlying) {
                var ulPos = positionMap.get(symbol);
                if (ulPos != null && tradeSize != 0) {
                    var tradeSign = Math.signum(tradeSize);
                    var effTradeSizeAbs = Math.min(Math.abs(ulPos.getSize()), Math.abs(tradeSize * 100)) / 100;
                    var effTradeSize = tradeSign * effTradeSizeAbs;
                    var effTradeTotal = tradeTotal == null ? 0.0 : tradeTotal;
                    if (effTradeSize != tradeSize) {
                        log.warn(String.format(
                                "Option/stock mismatch {occId: %s, user: %s, account: %s, szUl: %.2f, szOpt: %.2f}",
                                occId, userId_, accountId_, ulPos.getSize(), tradeSize));
                        effTradeTotal = effTradeTotal * effTradeSize / tradeSize;
                    }
                    var tradeType = trade.getTradeType();
                    var holder = activeHolderMap.get(occId);
                    if (IATTrade.TYPES_OPEN.contains(tradeType)) {
                        if (holder == null) {
                            holder = new PosHolder();
                            activeHolderMap.put(occId, holder);
                            closedHolderMap.remove(occId);
                        }
                        holder.effSize += effTradeSize;
                        holder.effTotal += effTradeTotal;
                    } else if (IATTrade.TYPES_CLOSE.contains(tradeType)) {
                        if (holder == null) {
                            var closedHolder = closedHolderMap.get(occId);
                            var closeRatio = closedHolder != null ? Math.abs(closedHolder.tradeSize / tradeSign) : 1.0;
                            if (closeRatio < 1) {// closedHolder.tradeSize + tradeSize != 0) {
                                log.error(String.format("Closing unmatched option {occId: %s, user: %s, account: %s}",
                                        occId, userId_, accountId_));
                                closedHolder.tradeSize += tradeSize;
                                closedHolder.tradeTotal += (tradeTotal == null ? 0.0 : tradeTotal);
                            } else {
                                closedHolderMap.remove(occId);
                            }
                        } else {
                            var holderSign = Math.signum(holder.effSize);
                            var effHolderSizeAbs = Math.min(effTradeSizeAbs, Math.abs(holder.effSize));
                            var effHolderSize = holderSign * effHolderSizeAbs;
                            var isHolderClosed = effHolderSizeAbs == effTradeSizeAbs;
                            var effHolderTotal = isHolderClosed ? holder.effTotal
                                    : holder.effTotal * effHolderSize / holder.effSize;
                            effTradeTotal = isHolderClosed ? effTradeTotal
                                    : effTradeTotal * effHolderSizeAbs / effTradeSizeAbs;
                            var ulPxAdj = ulPos.getPxTradeAdjusted();
                            ulPos.setPxTradeAdjusted(
                                    (ulPxAdj == null ? 0.0 : ulPxAdj) + effHolderTotal + effTradeTotal);
                            if (isHolderClosed) {
                                activeHolderMap.remove(occId);
                                holder.isClosed = true;
                            } else {
                                holder.effSize += effTradeSize;
                                holder.effTotal += (effTradeTotal - effHolderTotal);
                            }
                        }
                    }
                    if (holder != null) {
                        holder.tradeSize += tradeSize;
                        holder.tradeTotal += (tradeTotal == null ? 0.0 : tradeTotal);
                        if (holder.isClosed && holder.tradeSize != 0) {
                            closedHolderMap.put(occId, holder);
                        }
                    }
                }
            }
            if (count == 0) {
                positionMap.remove(occId);
                if (isUnderlying) {
                    activeHolderMap.clear();
                }
            } else {
                if (pos == null) {
                    pos = new ATPosition();
                    positionMap.put(occId, pos);
                    pos.setAccountId(accountId_);
                    pos.setOccId(occId);
                    pos.setUnderlying(symbol);
                    pos.setUserId(userId_);
                    if (!isUnderlying) {
                        var expStr = ATModelUtil.getExpirationDS6(occId);
                        var exp = Integer.parseInt(expStr);
                        pos.setExp6(exp);
                    }
                }
                var factor = isUnderlying ? 1.0 : 100.0;
                var posPxTrade = Precision.round(Math.abs(total / (factor * count)), 6);
                var posPxTradeTotal = Precision.round(total, 6);
                pos.setPxTrade(posPxTrade);
                pos.setPxTradeTotal(posPxTradeTotal);
                pos.setSize(count);
                pos.setTsLast(tsTrd);
            }
        }
        var positions = positionMap.values();
        var deleteIds = occIds;// new HashSet<>(occIds_);
        deleteIds.removeAll(positionMap.keySet());
        if (!deleteIds.isEmpty()) {
            var delQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_)
                    .eq(IATAssetConstants.TRADE_ACCOUNT, accountId_).in(IATTrade.OCCID, deleteIds);
            var count = _mongodb.deletePositions(delQuery);
            log.info(String.format("Deleted positions [%d / %s]", count, deleteIds));
        }
        var count = _mongodb.savePositions(positions);
        log.info(String.format("Saved positions [%d / %s]", count, positionMap.keySet()));
        return positions;
    }

    protected void resetAllPositions() {
        var userQuery = ATPersistenceRegistry.query().empty();
        var userIds = this.getTradeUserIds(userQuery);
        for (String userId : userIds) {
            this.resetUserPositions(userId);
        }
    }

    protected void resetUserPositions(String userId_) {
        this.resetUserAccountPositions(userId_, null);
    }

    protected void resetUserAccountPositions(String userId_, String accountId_) {
        this.resetUserAccountSymbolPositions(userId_, accountId_, null);
    }

    protected void resetUserAccountSymbolPositions(String userId_, String accountId_, String symbol_) {
        if (StringUtils.isBlank(userId_)) {
            throw new ATException(String.format("Missing user id"));
        }
        var accountIds = StringUtils.isBlank(accountId_) ? this.getUserAccountIds(userId_) : Arrays.asList(accountId_);
        for (String accountId : accountIds) {
            var occIdQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_)
                    .eq(IATAssetConstants.TRADE_ACCOUNT, accountId);
            if (!StringUtils.isBlank(symbol_)) {
                occIdQuery = occIdQuery.eq(IATTrade.UNDERLYING, symbol_);
            }
            var occIds = this.getUserOccIds(occIdQuery);
            if (occIds == null || occIds.isEmpty()) {
                log.error(String.format("No trades for {user: %s, account: %s}", userId_, accountId));
                return;
            }
            var map = occIds != null
                    ? occIds.stream()
                            .collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_), TreeMap::new,
                                    Collectors.toList()))
                    : null;
            for (String symbol : map.keySet()) {
                var ids = map.get(symbol);
                this.updatePositions(userId_, accountId, ids);
            }
        }
    }

    private Set<String> getTradeUserIds(IATWhereQuery query_) {
        var table = ATMongoTableRegistry.getTradeLedgerTable();
        var userIds = _mongodb.distinct(table, query_, IATAssetConstants.TRADE_USER, String.class);
        return userIds;
    }

    private Set<String> getUserAccountIds(String userId_) {
        var table = ATMongoTableRegistry.getTradeLedgerTable();
        var query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
        var accountIds = _mongodb.distinct(table, query, IATAssetConstants.TRADE_ACCOUNT, String.class);
        return accountIds;
    }

    private Set<String> getUserOccIds(IATWhereQuery query_) {
        var table = ATMongoTableRegistry.getTradeLedgerTable();
        var occIds = _mongodb.distinct(table, query_, IATAssetConstants.OCCID, String.class);
        return occIds;
    }

    private static class PosHolder {

        boolean isClosed = false;

        double effSize = 0.0;
        double effTotal = 0.0;

        double tradeSize = 0.0;
        double tradeTotal = 0.0;
    }

}
