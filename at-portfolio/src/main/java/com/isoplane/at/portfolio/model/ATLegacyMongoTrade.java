package com.isoplane.at.portfolio.model;

import java.util.Date;
import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.mongodb.IATMongo;

public class ATLegacyMongoTrade extends Document implements IATTrade, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	transient private String hashStr;

	static {
		ATTradeModelTool.register(ATLegacyMongoTrade.class);
	}

	public ATLegacyMongoTrade() {
	}

	public ATLegacyMongoTrade(IATTrade template_) {
		this.setAccount(template_.getTradeAccount());
		this.setComment(template_.getTradeComment());
		this.setCount(template_.getSzTrade());
		if (template_.getSzTrade() != template_.getSzTradeActive())
			setCountActive(template_.getSzTradeActive());
		this.setOccId(template_.getOccId());
		this.setPxNet(template_.getPxNet());
		this.setTotalPrice(template_.getPxTradeTotal());
		this.setTradeTimestamp(template_.getTsTrade());
		this.setTradeTimestamp2(template_.getTsTrade2());
		this.setTradeType(template_.getTradeType());
		this.setUnitPrice(template_.getPxTrade());
		this.setUserId(template_.getTradeUserId());

		// Must be last
		this.setAtId(template_.getAtId());
		// this.put("_id", this.getAtId());
	}

	private void resetHash() {
		hashStr = null;
		setAtId(null);
	}

	@Override
	public int compareTo(Object other_) {
		return ATTradeModelTool.compare(this, other_);
	}

	@Override
	public int hashCode() {
		if (hashStr == null)
			hashStr = ATTradeModelTool.hashString(this, false);
		return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object obj_) {
		return ATTradeModelTool.equals(this, obj_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public String toString() {
		String str = IATTrade.toString(this);
		return str;
	}

	@Override
	public String getOccId() {
		return super.getString(IATAssetConstants.OCCID);
	}

	public void setOccId(String value_) {
		resetHash();
		super.put(IATAssetConstants.OCCID, value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public void setAtId(String value_) {
		super.put(AT_ID, value_);
	}

	@Override
	public String getAtId() {
		String id = super.getString(AT_ID);
		// if (id == null) {
		// id = ATTradeModelTool.atId(this);
		// setAtId(id);
		// }
		return id;
	}

	@Override
	public String getTradeUserId() {
		return super.getString(IATAssetConstants.OWNER_ID);
	}

	public void setUserId(String value_) {
		resetHash();
		super.put(IATAssetConstants.OWNER_ID, value_);
	}

	@Override
	public Date getTsTrade() {
		String dstr = super.getString(IATAssetConstants.TRADE_DATE_STR);
		try {
			Date tradeDate = ATFormats.DATE_json.get().parse(dstr);
			return tradeDate;
		} catch (Exception ex) {
			throw new ATException(String.format("Invalid trade date [s]", dstr), ex);
		}
	}

	public void setTradeTimestamp(Date value_) {
		resetHash();
		String dstr = value_ == null ? null : ATFormats.DATE_json.get().format(value_);
		super.put(IATAssetConstants.TRADE_DATE_STR, dstr);
	}

	@Override
	public Long getTsTrade2() {
		return super.getLong(IATAssetConstants.TS_TRADE);
	}

	public void setTradeTimestamp2(Long value_) {
		resetHash();
		super.put(IATAssetConstants.TS_TRADE, value_);
	}

	@Override
	public Double getSzTrade() {
		return super.getDouble(IATAssetConstants.SIZE_TRADE);
	}

	@Override
	public Double getSzTradeActive() {
		return super.getDouble(IATAssetConstants.SIZE_ACTIVE);
	}

	public void setCount(Double value_) {
		resetHash();
		super.put(IATAssetConstants.SIZE_TRADE, value_);
	}

	public void setCountActive(Double value_) {
		resetHash();
		super.put(IATAssetConstants.SIZE_ACTIVE, value_);
	}

	@Override
	public Double getPxNet() {
		return super.getDouble(IATAssetConstants.PX_NET);
	}

	public void setPxNet(Double value_) {
		//resetHash();
		super.put(IATAssetConstants.PX_NET, value_);
	}
	
	@Override
	public Double getPxTrade() {
		return super.getDouble(IATAssetConstants.PX_TRADE);
	}

	public void setUnitPrice(Double value_) {
		resetHash();
		super.put(IATAssetConstants.PX_TRADE, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return super.getDouble(IATAssetConstants.TRADE_TOTAL);
	}

	public void setTotalPrice(Double value_) {
		resetHash();
		super.put(IATAssetConstants.TRADE_TOTAL, value_);
	}
	
	@Override
	public Double getPxUl() {
		return IATTrade.getPxUl(this);
	}

	public void setPxUl(Double value_) {
		IATTrade.setPxUl(this, value_);
	}

	@Override
	public Integer getTradeType() {
		return super.getInteger(IATAssetConstants.TYPE, -1);
	}

	public void setTradeType(int varlue_) {
		resetHash();
		super.put(IATAssetConstants.TYPE, varlue_);
	}

	@Override
	public String getTradeAccount() {
		return super.getString(IATAssetConstants.BROKER);
	}

	public void setAccount(String value_) {
		resetHash();
		super.put(IATAssetConstants.BROKER, value_);
	}

	public void setActive(Boolean value_) {
		// resetHash();
		// super.put(IATAssetConstants.ACTIVE, value_);
	}

	@Override
	public String getTradeComment() {
		return null;
		// return super.getString(IATAssetConstants.COMMENT);
	}

	public void setComment(String value_) {
		// resetHash();
		// super.put(IATAssetConstants.COMMENT, value_);
	}

	@Override
	public String getTradeGroup() {
		return IATTrade.getTradeGroup(this);
	}

	public void setTradeGroup(String value_) {
		IATTrade.setTradeGroup(this, value_);
	}

	@Override
	public Map<String, Object> getAsMap() {
		return this;
	}

	@Override
	public String getTraceId() {
		// TODO Auto-generated method stub
		return null;
	}

}
