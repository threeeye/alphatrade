package com.isoplane.at.portfolio.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoPortfolio extends Document implements IATPortfolio, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	transient private String hashStr;

	static {
		ATTradeModelTool.register(ATMongoPortfolio.class);
	}

	public ATMongoPortfolio() {
		setSz(0.0);
		setPxTradeTotal(0.0);
	}

	public ATMongoPortfolio(IATPortfolio template_) {
		Map<String, Object> map = template_.getAsMap();
		putAll(map);
		this.setUnderlying(template_.getOccId());
		// NOTE: Overrides calculated -> Must be last
		this.setAtId(template_.getAtId());
		this.remove(IATLookupMap.AT_ID);

		Iterator<Entry<String, Object>> i = entrySet().iterator();
		while (i.hasNext()) {
			Entry<String, Object> entry = i.next();
			if (entry.getValue() == null) {
				i.remove();
			}
		}
	}

	private void resetHash() {
		hashStr = null;
		setAtId(null);
	}

	@Override
	public int compareTo(Object other_) {
		if (other_ == null || !(other_ instanceof IATPortfolio))
			return -1;
		return this.getAtId().compareTo(((IATPortfolio) other_).getAtId());
	}

	@Override
	public int hashCode() {
		if (hashStr == null)
			hashStr = getAtId();
		return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object other_) {
		if (!(other_ instanceof IATPortfolio))
			return false;
		return this.hashCode() == other_.hashCode();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public String toString() {
		String str = IATPortfolio.toString(this);
		return str;
	}

	@Override
	public String getOccId() {
		return IATPortfolio.getOccId(this);
	}

	public void setOccId(String value_) {
		resetHash();
		IATPortfolio.setOccId(this, value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public void setAtId(String value_) {
		super.put(MONGO_ID, value_);
	}

	@Override
	public String getAtId() {
		String id = super.getString(MONGO_ID);
		if (id == null) {
			id = String.format("%d:%s:%s", getUserId().hashCode(), getOccId(), getTradeAccount());
			setAtId(id);
		}
		return id;
	}

	@Override
	public String getUserId() {
		return IATPortfolio.getUserId(this);
	}

	public void setUserId(String value_) {
		resetHash();
		IATPortfolio.setUserId(this, value_);
	}

	@Override
	public Date getTs() {
		return IATPortfolio.getTs(this);
	}

	public void setTs(Date value_) {
		resetHash();
		IATPortfolio.setTs(this, value_);
	}

	@Override
	public Long getTsLong() {
		return IATPortfolio.getTsLong(this);
	}

	public void setTsLong(Long value_) {
		resetHash();
		IATPortfolio.setTsLong(this, value_);
	}

	@Override
	public String getExpDS6() {
		return IATPortfolio.getExpDS6(this);
	}

	public void setExpDS6(String value_) {
		IATPortfolio.setExpDS6(this, value_);
	}

	@Override
	public Double getSz() {
		return IATPortfolio.getSz(this);
	}

	public void setSz(Double value_) {
		resetHash();
		IATPortfolio.setSz(this, value_);
	}

	@Override
	public Double getPxTradeTotal() {
		return IATPortfolio.getPxTradeTotal(this);
	}

	public void setPxTradeTotal(Double value_) {
		IATPortfolio.setPxTradeTotal(this, value_);
	}

	@Override
	public Double getPxAccumulated() {
		return IATPortfolio.getPxAccumulated(this);
	}

	public void setPxAccumulated(Double value_) {
		IATPortfolio.setPxAccumulated(this, value_);
	}

	@Override
	public String getTradeAccount() {
		return IATPortfolio.getTradeAccount(this);
	}

	public void setTradeAccount(String value_) {
		resetHash();
		IATPortfolio.setTradeAccount(this, value_);
	}

	public void setUnderlying(String occId_) {
		if (StringUtils.isBlank(occId_)) {
			super.remove(IATAssetConstants.OCCID_UNDERLYING);
			return;
		}
		String sym = occId_;
		if (sym.length() > 6) {
			sym = sym.substring(0, 6).trim();
		}
		super.put(IATAssetConstants.OCCID_UNDERLYING, sym);
	}

	public String getUnderlying() {
		return super.getString(IATAssetConstants.OCCID_UNDERLYING);
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> map = new HashMap<>(this);
		map.put(IATLookupMap.AT_ID, getAtId());
		map.remove(IATMongo.MONGO_ID);
		map.remove(IATAssetConstants.OCCID_UNDERLYING);
		return this;
	}

}
