package com.isoplane.at.portfolio;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.model.ATMongoTrade;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ATPortfilioManagerTest {

	static final Logger log = LoggerFactory.getLogger(ATPortfilioManagerTest.class);

	static Gson gson;
	static Comparator<IATTrade> tComp;

	//	static final String TEST_TRADES_TABLE = "trades_test";
	// static private CompositeConfiguration _config;
	static private ATMongoDao _db;
	static private ATPortfolioManagerTestWrapper _pfM;
	static LocalDate _date_20190601;

	static final String TEST_USER = "TEST_USER";

	static final String[] TABLE_KEYS = new String[] { "mongo.t.trades", "mongo.t.trade.acu", "mongo.t.trade.group",
			"mongo.t.trade.ledger", "mongo.t.trade.digest", "mongo.t.portfolio", "mongo.t.position",
			"mongo.t.trade.trace" };

	@BeforeAll
	public static void beforeAll() throws ConfigurationException {
		gson = new GsonBuilder()
				.setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
				.create();
		tComp = new TestTradeComparator();

		ATMongoTableRegistry.setTestMode(true);

		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		Configuration config = ATConfigUtil.init(configPath, propertiesPath, false);
		// for (String key : TABLE_KEYS) {
		// 	String table = config.getString(key);
		// 	ATConfigUtil.overrideConfiguration(key, String.format("TEST_%s", table));
		// }

		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);

		ATExecutors.init();

		IATMarketDataService mktSvc = new ATMarketDataServiceTestWrapper();
		ATServiceRegistry.setMarketDataService(mktSvc, false);

		_db = new ATMongoDao();
		_pfM = new ATPortfolioManagerTestWrapper(true);
		_pfM.initService();

		_date_20190601 = LocalDate.of(2019, 06, 01);
	}

	@AfterAll
	public static void afterAll() {
		// Configuration config = ATConfigUtil.config();
		// for (String key : TABLE_KEYS) {
		// 	String table = config.getString(key);
		// 	if (table.startsWith("TEST_")) {
		// 		log.warn(String.format("clearing table [%s]", table));
		// 		_db.delete(table, ATPersistenceRegistry.query().empty());
		// 	}
		// }
	}

	@BeforeEach
	public void prepare() {
		//	Configuration config = ATConfigUtil.config();
		for (String key : TABLE_KEYS) {
			String table = ATMongoTableRegistry.getTable(key);
			if (table.startsWith("TEST_")) {
				log.warn(String.format("Preparing table [%s]", table));
				_db.delete(table, ATPersistenceRegistry.query().empty());
			}
		}
	}

	@AfterEach
	public void cleanup() {
		//	_db.delete(TEST_TRADES_TABLE, ATPersistenceRegistry.query().empty());
	}

	@Test
	//	@Disabled
	public void testInsert_A() throws Exception {
		runTest("portfolio-manager_test_insert-a.json");
	}

	@Test
	//	@Disabled
	public void testInsert_B() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json");
	}

	@Test
	//	@Disabled
	public void testInsert_C() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json",
				"portfolio-manager_test_insert-c.json");
	}

	@Test
	//	@Disabled
	public void testInsert_D() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json",
				"portfolio-manager_test_insert-c.json", "portfolio-manager_test_insert-d.json");
	}

	@Test
	//	@Disabled	
	public void testInsert_E1() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json",
				"portfolio-manager_test_insert-c.json", "portfolio-manager_test_insert-d.json",
				"portfolio-manager_test_insert-e1.json");
	}

	@Test
	//	@Disabled	
	public void testInsert_E2() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json",
				"portfolio-manager_test_insert-c.json", "portfolio-manager_test_insert-d.json",
				"portfolio-manager_test_insert-e2.json");
	}

	@Test
	//	@Disabled	
	public void testInsert_E3() throws Exception {
		runTest("portfolio-manager_test_insert-a.json", "portfolio-manager_test_insert-b.json",
				"portfolio-manager_test_insert-c.json", "portfolio-manager_test_insert-d.json",
				"portfolio-manager_test_insert-e3.json");
	}

	@Test
	@Disabled
	public void testBackfill_A() throws Exception {
		runTest("portfolio-manager_test_backfill_traceid-a.json", "portfolio-manager_test_backfill_traceid-b.json",
				"portfolio-manager_test_backfill_traceid-c.json");
		String table = ATConfigUtil.config().getString("mongo.t.trade.ledger");
		List<ATMongoTrade> trades = _db.query(table, null, ATMongoTrade.class);
		for (ATMongoTrade trade : trades) {
			trade.remove(IATAssetConstants.TRACE_ID);
		}
		prepare(); // NOTE: Deletes tables
		List<Document> documents = trades.stream().map(t -> (Document) t).toList();
		_db.fastBulkInsert(table, documents);

		// NOTE: Not available due to package change
		//	ATPortfolioTool tool = new ATPortfolioTool();
		//	tool.fixTraceIds("TEST_USER", "TEST", "TEST");

		int oldSize = trades.size();
		trades = _db.query(table, null, ATMongoTrade.class);
		assertEquals(oldSize, trades.size(), "Size");
		int cycle = 0;
		String traceId = null;
		for (ATMongoTrade trade : trades) {
			if (cycle == 0) {
				traceId = trade.getTraceId();
			} else {
				assertEquals(traceId, trade.getTraceId(), "traceId");
			}
			cycle++;
		}
	}

	private void runTest(String... paths_) throws Exception {
		ArrayList<TestData> testData = loadTestData(paths_);
		for (TestData data : testData) {
			data.trades_in.sort(tComp);
			var saveTrades = data.trades_in.toArray(new ATTrade[0]);
			_pfM.saveTrades(TEST_USER, saveTrades);
		}
		TestData data = merge(testData);
		ATTrade[] inTrades = data.trades_in.toArray(new ATTrade[0]);
		String symbol = ATModelUtil.getSymbol(inTrades[0].getOccId());

		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, TEST_USER);
		List<IATTrade> outTrades = _pfM.getMongoDb().getTrades(query);
		assertEquals(inTrades.length, outTrades.size(), "trade count");
		assertEquals(data.trades_out.size(), outTrades.size(), "trade count");
		outTrades.sort(tComp);
		for (int i = 0; i < data.trades_out.size(); i++) {
			var referenceKeys = data.trades_out.get(i).keySet();
			var referenceTrade = new ATTrade((IATTrade) data.trades_out.get(i));
			var testTrade = new ATTrade(outTrades.get(i));
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceTrade.get(key);
				Object testVal = testTrade.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceTrade.getOccId()));
			}
		}

		List<ATTrade> ledgerTrades = _pfM.getMongoDb().getTradeLedger(query);
		assertEquals(data.ledger.size(), ledgerTrades.size(), "ledger count");
		ledgerTrades.sort(tComp);
		for (int i = 0; i < data.trades_out.size(); i++) {
			var referenceKeys = data.ledger.get(i).keySet();
			var referenceTrade = new ATTrade((Map<String, Object>) data.ledger.get(i));
			var testTrade = ledgerTrades.get(i);
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceTrade.get(key);
				Object testVal = testTrade.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceTrade.getOccId()));
			}
		}

		List<ATTradeDigest> tradeDigests = _pfM.getMongoDb().getTradeDigest(query);
		assertEquals(data.digest.size(), tradeDigests.size(), "digest count");
		for (int i = 0; i < data.digest.size(); i++) {
			var referenceKeys = data.digest.get(i).keySet();
			var referenceMap = new ATTradeDigest(data.digest.get(i));
			var testMap = tradeDigests.get(i);
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceMap.get(key);
				Object testVal = testMap.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceMap.getOccId()));
			}
		}

		ATTrade tradeAcu = _pfM.getMongoDb().getTradeCumulative(inTrades[0].getTradeUserId(), symbol);
		assertNotNull(tradeAcu);
		{
			var referenceKeys = data.acu.keySet();
			var referenceTrade = new ATTrade((Map<String, Object>) data.acu);
			var testTrade = tradeAcu;
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceTrade.get(key);
				Object testVal = testTrade.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceTrade.getOccId()));
			}
		}

		List<ATTradeGroup> tradeGroups = _pfM.getMongoDb().getTradeGroups(query);
		assertEquals(data.group.size(), tradeGroups.size(), "group count");
		for (int i = 0; i < data.group.size(); i++) {
			var referenceKeys = data.group.get(i).keySet();
			var referenceMap = data.group.get(i);
			var testMap = tradeGroups.get(i);
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceMap.get(key);
				Object testVal = testMap.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceMap.getUnderlying()));
			}
		}

		List<ATPosition> positions = _pfM.getMongoDb().queryPositions(query);
		assertEquals(data.position.size(), positions.size(), "position count");
		for (int i = 0; i < data.position.size(); i++) {
			var referenceKeys = data.position.get(i).keySet();
			var referenceMap = data.position.get(i);
			var testMap = positions.get(i);
			for (String key : referenceKeys) {
				if ("_atid".equals(key))
					continue;
				Object referenceVal = referenceMap.get(key);
				Object testVal = testMap.get(key);
				assertEquals(referenceVal, testVal, String.format("%s(%s)", key, referenceMap.getUnderlying()));
			}
		}
	}

	static ArrayList<TestData> loadTestData(String... paths_) throws Exception {
		if (paths_ == null || paths_.length == 0)
			return null;
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		ArrayList<TestData> dataList = new ArrayList<>();
		for (String path : paths_) {
			String json = ATSysUtils.getResourceFileAsString(path);
			TestData data = objectMapper.readValue(json, TestData.class);
			log.info(String.format("Loading test config: %s (%s)", path, data.description));
			dataList.add(data);
		}
		return dataList;
	}

	/**
	* Merges TestData overriding values with data2.
	* 
	* @param data1_
	* @param data2_
	* @return
	*/
	static TestData merge(List<TestData> data_) {
		if (data_ == null || data_.size() == 0)
			return null;
		TestData tmpl = null;
		for (int i = data_.size() - 1; i >= 0; i--) {
			TestData tmp = data_.get(i);
			if (tmp != null) {
				tmpl = tmp;
				break;
			}
		}
		if (tmpl == null)
			return null;
		var inMap = new HashMap<String, ATTrade>();
		var outMap = new HashMap<String, ATTrade>();

		TestData data = new TestData();
		//	data.trades_in = new ArrayList<>();
		//	data.trades_out = new ArrayList<>();
		data.ledger = new ArrayList<>();
		data.digest = new ArrayList<>();
		data.group = new ArrayList<>();
		data.position = new ArrayList<>();
		for (TestData tmp : data_) {
			if (tmp != null) {
				// Joining
				if (tmp.trades_in != null) {
					var map = tmp.trades_in.stream()
							.collect(Collectors.toMap(t -> String.format("%s:%d", t.getOccId(), t.getTsTrade2()),
									t -> t));
					inMap.putAll(map);
				}
				if (tmp.trades_out != null) {
					var map = tmp.trades_out.stream()
							.collect(Collectors.toMap(t -> String.format("%s:%d", t.getOccId(), t.getTsTrade2()),
									t -> t));
					outMap.putAll(map);
				}
				if (tmp.ledger != null) {
					data.ledger.addAll(tmp.ledger);
				}
				// Replacing
				if (tmp.digest != null) {
					data.digest = tmp.digest;
				}
				if (tmp.acu != null) {
					data.acu = tmp.acu;
				}
				if (tmp.group != null) {
					data.group = tmp.group;// .addAll(tmp.group);
				}
				if (tmp.description != null) {
					data.description = tmp.description;
				}
				if (tmp.position != null) {
					data.position = tmp.position;
				}
			}
		}
		data.trades_in = new ArrayList<>(inMap.values());
		data.trades_in.sort(tComp);
		data.trades_out = new ArrayList<>(outMap.values());
		data.trades_out.sort(tComp);
		data.ledger.sort(tComp);
		return data;
	}

	public static class TestData {

		public String description;
		public List<ATTrade> trades_in;
		public List<ATTrade> trades_out;
		public List<ATTrade> ledger;
		public List<ATTradeDigest> digest;
		public ATTrade acu;
		public List<ATTradeGroup> group;
		public List<ATPosition> position;
	}

	public static class TestTradeComparator implements Comparator<IATTrade> {

		@Override
		public int compare(IATTrade t1, IATTrade t2) {
			int c1 = t1.getTsTrade2().compareTo(t2.getTsTrade2());
			if (c1 != 0)
				return c1;
			int c2 = t1.getOccId().compareTo(t2.getOccId());
			if (c2 != 0)
				return c2;
			return 0;
		}

	}

}
