package com.isoplane.at.portfolio;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;

class ATPortfilioMigrator {

	static final Logger log = LoggerFactory.getLogger(ATPortfilioMigrator.class);

	static final String TRADES_TABLE = "trades_test";
	static final String PORTFOLIO_TABLE = "portfolio_test";
	static private CompositeConfiguration _config;
	static private ATMongoDao _db;
	static private ATPortfolioManagerTestWrapper _pfM;
	static LocalDate _date_20190601;

	static final String TEST_USER = "TEST_USER";

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String testArgs = "C:\\Dev\\workspace\\alphatrade\\source.properties,C:\\Dev\\workspace\\alphatrade\\NOCHECKIN.properties,C:\\Dev\\workspace\\alphatrade\\local.properties";
		String[] propertiesPaths = testArgs.split(",");
		_config = new CompositeConfiguration();
		for (String path : propertiesPaths) {
			log.info(String.format("Reading properties [%s]", path));
			PropertiesConfiguration config = new Configurations().properties(new File(path));
			_config.addConfiguration(config);
		}
		ATConfigUtil.init(testArgs, false);

		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		ATExecutors.init();

		_db = new ATMongoDao();
		_pfM = new ATPortfolioManagerTestWrapper(true);
		_pfM.initService();

		_date_20190601 = LocalDate.of(2019, 06, 01);

		_db.delete(PORTFOLIO_TABLE, ATPersistenceRegistry.query().empty());
	}

	@AfterAll
	public static void tearDown() {
		// _db.delete(PORTFOLIO_TABLE, ATPersistenceRegistry.query().empty());
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	@Disabled
	public void test() {

	}

	@Test
	@Disabled
	public void testPopulatePortfolio() {
		IATWhereQuery tradeQuery = ATPersistenceRegistry.query().empty();
		List<IATTrade> trades = _pfM.getMongoDb().getTrades(tradeQuery);

		Map<String, String[]> posMap = new TreeMap<>();
		for (IATTrade trade : trades) {
			String occId = trade.getOccId();
			String sym = occId.length() > 6 ? occId.substring(0, 6).trim() : occId;
			String key = String.format("%s:%s:%s", trade.getTradeUserId(), sym, trade.getTradeAccount());
			posMap.put(key, new String[] { trade.getTradeUserId(), trade.getTradeAccount(), sym });
		}
		// Set<String[]> ulSet = trades.stream().filter(t -> t.getOccId().length() < 10)
		// .map(t -> new String[] { t.getTradeUserId(), t.getTradeAccount(), t.getOccId() })
		// .collect(Collectors.toSet());
		// log.info(String.format("Set: %s", ulSet));
		int i = 0;
		for (String[] strings : posMap.values()) {
			// strings[1] = "FID SEP";
			// strings[2] = "BAC";
			String userId = strings[0];
			String account = strings[1];
			String ul = strings[2];
			log.info(String.format("[%d/%d] Processing [%s, %s, %s]", ++i, posMap.size(), userId, ul, account));
			IATPortfolio prt = _pfM.updatePortfolio(userId, account, ul);
			// if (true)
			// return;
		}

		log.info(String.format("trade: %s", trades.get(0)));
		assertTrue(trades.size() > 0);
	}

	@Test
	public void testPopulateOne() {
		String userId = "TdW8PcdGC9NvNl17kebhqdf5oNX2";
		String account = "FID RO";
		String occId = "HYG";
		IATPortfolio prt = _pfM.updatePortfolio(userId, account, occId);
		log.info(String.format("prt: %s", prt));
		assertNotNull(prt);
	}

}
