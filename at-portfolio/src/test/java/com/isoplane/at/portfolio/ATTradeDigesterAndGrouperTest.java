package com.isoplane.at.portfolio;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.portfolio.util.ATTradeCumulator;
import com.isoplane.at.portfolio.util.ATTradeDigester;
import com.isoplane.at.portfolio.util.ATTradeGrouper;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class ATTradeDigesterAndGrouperTest {

	static final Logger log = LoggerFactory.getLogger(ATTradeDigesterAndGrouperTest.class);

	static Gson gson;

	@BeforeAll
	public static void setup() {
		gson = new Gson();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {
	}

	@AfterEach
	public void cleanup() {
	}

	// LE

	@Test
	@Order(1)
	// @Disabled
	public void testLE_01_Open() {
		TestData data = getTestData("digester_test_le_1.json");
		this.test(data);
	}

	@Test
	@Order(2)
	public void testLE_02_Add() {
		TestData data = getTestData("digester_test_le_1.json", "digester_test_le_2.json");
		this.test(data);
	}

	@Test
	@Order(3)
	public void testLE_03_Sell() {
		TestData data = getTestData("digester_test_le_1.json", "digester_test_le_2.json", "digester_test_le_3.json");
		this.test(data);
	}

	@Test
	@Order(4)
	public void testLE_04_Div() {
		TestData data = getTestData("digester_test_le_1.json", "digester_test_le_2.json", "digester_test_le_3.json", "digester_test_le_4.json");
		this.test(data);
	}

	@Test
	@Order(5)
	public void testLE_05_Close() {
		TestData data = getTestData("digester_test_le_1.json", "digester_test_le_2.json", "digester_test_le_3.json", "digester_test_le_4.json",
				"digester_test_le_5.json");
		this.test(data);
	}

	@Test
	@Order(6)
	public void testLE_06_ReOpen() {
		TestData data = getTestData("digester_test_le_1.json", "digester_test_le_2.json", "digester_test_le_3.json", "digester_test_le_4.json",
				"digester_test_le_5.json", "digester_test_le_6.json");
		this.test(data);
	}

	// CC

	@Test
	@Order(7)
	public void testCC_01_Open() {
		TestData data = getTestData("digester_test_cc_1.json");
		this.test(data);
	}

	@Test
	@Order(8)
	public void testCC_02_Add() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json");
		this.test(data);
	}

	@Test
	@Order(8)
	public void testCC_03a_Sell() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_3a.json");
		this.test(data);
	}

	@Test
	@Order(9)
	public void testCC_03b_Assign() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_3b.json");
		this.test(data);
	}

	@Test
	@Order(10)
	public void testCC_04_Dividend() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_4.json");
		this.test(data);
	}

	@Test
	@Order(11)
	public void testCC_05a_ExpireSold_OTM() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_3a.json", "digester_test_cc_5a.json");
		this.test(data);
	}

	@Test
	@Order(12)
	public void testCC_05b_ExpireAssigned_OTM() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_3b.json", "digester_test_cc_5b.json");
		this.test(data);
	}

	@Test
	@Order(13)
	public void testCC_05b_ExpireSold_ITM() {
		TestData data = getTestData("digester_test_cc_1.json", "digester_test_cc_2.json", "digester_test_cc_3a.json", "digester_test_cc_5c.json");
		this.test(data);
	}

	// SVPS

	@Test
	public void testSVPS_01_Open() {
		TestData data = getTestData("digester_test_svps_1.json");
		this.test(data);
	}

	@Test
	public void testSVPS_02_Add() {
		TestData data = getTestData("digester_test_svps_1.json", "digester_test_svps_2.json");
		this.test(data);
	}

	@Test
	public void testSVPS_03_Sell() {
		TestData data = getTestData("digester_test_svps_1.json", "digester_test_svps_2.json", "digester_test_svps_3.json");
		this.test(data);
	}

	@Test
	public void testSVPS_04a_Close() {
		TestData data = getTestData("digester_test_svps_1.json", "digester_test_svps_2.json", "digester_test_svps_3.json",
				"digester_test_svps_4a.json");
		this.test(data);
	}

	@Test
	public void testSVPS_04b_Expire_OTM() {
		TestData data = getTestData("digester_test_svps_1.json", "digester_test_svps_2.json", "digester_test_svps_3.json",
				"digester_test_svps_4b.json");
		this.test(data);
	}

	@Test
	public void testSVPS_04c_Expire_ITM() {
		TestData data = getTestData("digester_test_svps_1.json", "digester_test_svps_2.json", "digester_test_svps_3.json",
				"digester_test_svps_4c.json");
		this.test(data);
	}

	@Test
	public void testSVPS_05_Multi() {
		TestData data = getTestData("digester_test_svps_5.json");
		this.test(data);
	}

	// Helpers

	static TestData getTestData(String... paths_) {
		if (paths_ == null || paths_.length == 0)
			return null;
		ArrayList<TestData> dataList = new ArrayList<>();
		for (String path : paths_) {
			String json = ATSysUtils.getResourceFileAsString(path);
			TestData data = gson.fromJson(json, TestData.class);
			dataList.add(data);
		}
		TestData data = merge(dataList.toArray(new TestData[0]));
		return data;
	}

	private void test(TestData data_) {
		Double pxCum = ATTradeCumulator.cumulateTrades(data_.userId, data_.trades);
		assertEquals(data_.result_cum, pxCum, "px_cum");

		Map<String, ATTradeDigest> digestMap = ATTradeDigester.digestTrades(data_.userId, data_.account, data_.symbol, data_.trades);
		if (digestMap != null && !digestMap.isEmpty()) {
			Set<String> testIds = new HashSet<>();
			for (Map<String, Object> map : data_.result_digest) {
				String occId = IATAsset.getOccId(map);
				testIds.add(occId);
				ATTradeDigest digest = digestMap.get(occId);
				assertNotNull(digest, String.format("digest:missing [%s]", occId));
				for (Entry<String, Object> entry : map.entrySet()) {
					String key = entry.getKey().trim();
					if (key.startsWith("//"))
						continue;
					assertEquals(entry.getValue(), digest.get(key), String.format("digest:%s(%s)", key, occId));
				}
			}
			Set<String> digestIds = new HashSet<>(digestMap.keySet());
			assertTrue(testIds.containsAll(digestIds), String.format("digest:keys{%s,%s}", testIds, digestIds));
		} else {
			assertTrue(data_.result_digest == null || data_.result_digest.isEmpty());
		}
		ArrayList<ATTradeGroup> groupList = ATTradeGrouper.groupTrades(data_.userId, data_.account, data_.symbol, digestMap.values());
		if (groupList != null && !groupList.isEmpty() && data_.result_groups != null && !data_.result_groups.isEmpty()) {
			assertEquals(data_.result_groups.size(), groupList.size(), "group:size");
			// int idx = 0;
			for (Map<String, Object> map : data_.result_groups) {
				TreeSet<String> keys = new TreeSet<>(map.keySet());
				String testHash = createHash(keys, map);
				List<String> hashList = groupList.stream().map(t_ -> createHash(keys, t_)).collect(Collectors.toList());
				assertTrue(hashList.contains(testHash), String.format("group:%s", testHash));
				groupList.removeIf(t_ -> testHash.equals(createHash(keys, t_)));
				// ATTradeGroup group = groupList.get(idx++);
				// for (Entry<String, Object> entry : map.entrySet()) {
				// String key = entry.getKey().trim();
				// if (key.startsWith("//"))
				// continue;
				// assertEquals(entry.getValue(), group.get(key), String.format("group:%s", key));
				// }
			}
			log.info(groupList.toString());
		} else {
			assertTrue(data_.result_groups == null || data_.result_groups.isEmpty());
		}
	}

	private String createHash(Set<String> keys_, Map<String, Object> map_) {
		StringBuilder sb = new StringBuilder();
		for (String key : keys_) {
			Object value = map_.get(key);
			if (value != null) {
				sb.append(key).append("=").append(value).append(",");
			}
		}
		String hash = sb.toString();
		return hash;
	}

	/**
	 * Merges TestData overriding values with data2.
	 * 
	 * @param data1_
	 * @param data2_
	 * @return
	 */
	static TestData merge(TestData... data_) {
		if (data_ == null || data_.length == 0)
			return null;
		TestData tmpl = null;
		for (int i = data_.length - 1; i >= 0; i--) {
			TestData tmp = data_[i];
			if (tmp != null && tmp.result_digest != null && tmp.result_groups != null) {
				tmpl = tmp;
				break;
			}
		}
		if (tmpl == null)
			return null;
		TestData data = new TestData();
		data.account = tmpl.account;
		data.symbol = tmpl.symbol;
		data.userId = tmpl.userId;
		data.result_cum = tmpl.result_cum;
		data.result_digest = new ArrayList<>();// new HashMap<String, Object>();
		data.result_digest.addAll(tmpl.result_digest);// .putAll(tmpl.result_digest);
		data.result_groups = new ArrayList<>();// = new HashMap<>();
		data.result_groups.addAll(tmpl.result_groups);// .putAll(tmpl.result_groups);
		data.trades = new ArrayList<>();
		for (TestData tmp : data_) {
			if (tmp != null && tmp.trades != null) {
				data.trades.addAll(tmp.trades);
			}
		}
		// if (data1_ != null && data1_.result_digest != null) {
		// data.result_digest.putAll(data1_.result_digest);
		// }
		// if (tmpl != null && tmpl.result_digest != null) {
		// data.result_digest.putAll(tmpl.result_digest);
		// }
		// if (data1_ != null && data1_.result_groups != null) {
		// data.result_groups.putAll(data1_.result_groups);
		// }
		// data.result_groups = new HashMap<>();
		// if (data2_ != null && data2_.result_groups != null) {
		// data.result_groups.putAll(data2_.result_groups);
		// }
		// data.trades = new ArrayList<>();
		// if (data1_ != null && data1_.trades != null) {
		// data.trades.addAll(data1_.trades);
		// }
		// if (data2_ != null && data2_.trades != null) {
		// data.trades.addAll(data2_.trades);
		// }
		return data;
	}


	public static class TestData {

		public String account;
		public String userId;
		public String symbol;
		public List<ATTrade> trades;
		public Double result_cum;
		public List<Map<String, Object>> result_digest;
		public List<Map<String, Object>> result_groups;
	}

}
