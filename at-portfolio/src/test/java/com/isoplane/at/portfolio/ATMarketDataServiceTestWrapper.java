package com.isoplane.at.portfolio;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.IATMarketData.OptionRight;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATMarketDataSubscriber;
import com.isoplane.at.commons.store.IATWhereQuery;

import org.apache.commons.lang3.NotImplementedException;

public class ATMarketDataServiceTestWrapper implements IATMarketDataService {

    @Override
    public void initService() {
        // TODO Auto-generated method stub

    }

    @Override
    public void subscribe(IATMarketDataSubscriber subscriber) {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub

    }

    @Override
    public void unsubscribe(IATMarketDataSubscriber subscriber) {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub

    }

    @Override
    public List<ATMarketData> getMovers() {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub
        //      return null;
    }

    @Override
    public Map<String, ATMarketData> getMarketData(IATWhereQuery query) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ATMarketData getMarketData(String occId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<String, List<ATHistoryData>> getHistory(IATWhereQuery query, List<String> flags, String avgMode,
            String hvMode) {
        var result = new HashMap<String, List<ATHistoryData>>();
        return result;
    }

    @Override
    public ATSecurityPack getOptionChain(String symbol, OptionRight right) {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub
        //        return null;
    }

    @Override
    public Map<String, Map<String, Set<ATMarketData>>> getOptionChainOld(String symbol, String right) {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub
        //        return null;
    }

    @Override
    public Map<String, ATMarketData> getOptionUnderlying(String symbol) {
        throw new ATException(new NotImplementedException());
        // TODO Auto-generated method stub
        //        return null;
    }

}
