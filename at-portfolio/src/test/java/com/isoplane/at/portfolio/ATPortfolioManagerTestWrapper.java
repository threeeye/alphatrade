package com.isoplane.at.portfolio;

import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;

public class ATPortfolioManagerTestWrapper extends ATPortfolioManager {

	public ATPortfolioManagerTestWrapper(boolean isStreaming_) {
		super(isStreaming_);
	}

	public ATPortfolioMogodbTools getMongoDb() {
		return _mongodb;
	}

}
