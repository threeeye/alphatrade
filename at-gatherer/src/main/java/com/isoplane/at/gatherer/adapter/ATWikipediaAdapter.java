package com.isoplane.at.gatherer.adapter;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATOptionChain;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.gatherer.util.ATHttpHelper;

public class ATWikipediaAdapter {

	static final Logger log = LoggerFactory.getLogger(ATWikipediaAdapter.class);

	static final String SOURCE_KEY = "WIKP";

	public Set<ATSymbol> updateSymbols(Map<String, ATSymbol> oldSymbols_) {
		try {
			String html = ATHttpHelper.getHttp("https://en.wikipedia.org/wiki/List_of_S%26P_500_companies");
			// Set<ATPersistentEquityDescription> records = parseSP500(html);
			// Map<String, ATWikiSymbol> symMap = new HashMap<>();
			Map<String, ATSymbol> newSymbols = parseSP500(oldSymbols_, html);
			Set<ATSymbol> result = new TreeSet<>(newSymbols.values());
			return result;
		} catch (Exception ex) {
			log.error("Error getting SP500", ex);
			return null;
		}
	}

	// private ATPersistentEquityDescription merge(ATPersistentEquityDescription d1_, ATPersistentEquityDescription d2_) {
	// if (d1_ == null)
	// return d2_;
	//
	// if (StringUtils.isEmpty(d1_.getDescription()))
	// d1_.setDescription(d2_.getDescription());
	// if (StringUtils.isEmpty(d1_.getIndustry()))
	// d1_.setIndustry(d2_.getIndustry());
	// if (StringUtils.isEmpty(d1_.getSector()))
	// d1_.setSector(d2_.getSector());
	// if (StringUtils.isEmpty(((String) d1_.get("wp_hq"))))
	// d1_.put("wp_hq", d2_.get("wp_hq"));
	// if (StringUtils.isEmpty(((String) d1_.get("wp_added"))))
	// d1_.put("wp_added", d2_.get("wp_added"));
	// if (StringUtils.isEmpty(((String) d1_.get("wp_cik"))))
	// d1_.put("wp_cik", d2_.get("wp_cik"));
	//
	// Set<String> idx = d1_.getIndex();
	// idx.addAll(d2_.getIndex());
	// d1_.setIndex(idx);
	//
	// return d1_;
	// }

	private Map<String, ATSymbol> parseSP500(Map<String, ATSymbol> oldSymbols_, String html_) {
		if (StringUtils.isBlank(html_)) {
			throw new ATException(String.format("Missing content"));
		}

		Map<String, ATSymbol> resultMap = new HashMap<>();
		String[] expectedHeaders = { "Symbol", "Security", "SEC filings", "GICS Sector", "GICS Sub-Industry" };
		// 1. Prepare HTML parsing
		Document doc = Jsoup.parse(html_);
		Elements tables = doc.select("#mw-content-text table");
		if (tables == null || tables.isEmpty()) {
			throw new ATException("Missing content table");
		}
		Elements rows1 = tables.get(0).select("tr");
		// Set<ATPersistentEquityDescription> result_digest = new TreeSet<>();
		boolean isHeadersProcessed = false;
		for (Element row : rows1) {
			Elements headers = row.select("th");
			if (headers != null && !headers.isEmpty()) {
				for (int hi = 0; hi < expectedHeaders.length; hi++) {
					String headerContent = headers.get(hi).text();
					if (!expectedHeaders[hi].equals(headerContent)) {
						throw new ATException(
								String.format("Wikipedia S&P500 header format changed: %s -> %s", Arrays.asList(expectedHeaders), row.text()));
					}
				}
				isHeadersProcessed = true;
				continue;
			}
			Elements cells = row.select("td");
			if (cells == null || cells.isEmpty())
				continue;
			ATWikiSymbol sym = new ATWikiSymbol();
			if (cells.size() > 0) {
				String ticker = cells.get(0).text().trim();
				sym.setId(ticker);
				// String name = cells.get(0).text().trim();
				// sym.setName(name);
				// sym.setDescription(name);
			} else
				continue;
			sym.setSource(SOURCE_KEY);
			sym.setModel(IATSymbol.EQUITY);
			// sym.setSo
			sym.setReferences(new TreeSet<String>(Arrays.asList(IATAssetConstants.IDX_SP500)));
			if (cells.size() > 1) {
				String name = cells.get(1).text().trim();
				sym.setName(name);
				// String ticker = cells.get(1).text().trim();
				// sym.setId(ticker);
			}
			if (cells.size() > 3) {
				String sector = cells.get(3).text().trim();
				sym.setSector(sector);
			}
			if (cells.size() > 4) {
				String industry = cells.get(4).text().trim();
				sym.setIndustry(industry);
			}
			// if (cells.size() > 5) {
			// String headquarter = cells.get(5).text().trim();
			// stock.put("wp_hq", headquarter);
			// }
			// if (cells.size() > 6) {
			// String dstr = cells.get(6).text().trim();
			// stock.put("wp_added", dstr);
			// }
			// if (cells.size() > 7) {
			// String cik = cells.get(7).text().trim();
			// stock.put("wp_cik", cik);
			// }
			ATSymbol oldSym = oldSymbols_.get(sym.getId());
			if (oldSym != null) {
				if (merge(sym, oldSym)) {
					resultMap.put(sym.getId(), new ATSymbol(sym));
				}

			} else {
				resultMap.put(sym.getId(), new ATSymbol(sym));
			}
			// result_digest.add(stock);
		}
		if (!isHeadersProcessed) {
			throw new ATException(String.format("Unable to verify header"));
		}
		return resultMap;
	}

	private boolean merge(ATWikiSymbol to_, IATSymbol from_) {
		if (to_ == null)
			return false;

		boolean isChanged = false;
		String newName = to_.getName();
		String oldName = from_.getName();
		if (newName == null) {
			to_.setName(oldName);
		} else if (!newName.equals(oldName)) {
			isChanged = true;
		}
		String newModel = to_.getModel();
		String oldModel = from_.getModel();
		if (newModel == null) {
			to_.setModel(oldModel);
		} else if (!newModel.equals(oldModel)) {
			isChanged = true;
		}
		String newSource = to_.getSource();
		String oldSource = from_.getSource();
		if (newSource == null) {
			to_.setSource(oldSource);
		} else if (!newSource.equals(oldSource)) {
			isChanged = true;
		}
		String newIndustry = to_.getIndustry();
		String oldIndustry = from_.getIndustry();
		if (newIndustry == null) {
			to_.setIndustry(oldIndustry);
		} else if (!newIndustry.equals(oldIndustry)) {
			isChanged = true;
		}
		String newSector = to_.getSector();
		String oldSector = from_.getSector();
		if (newSector == null) {
			to_.setSector(oldSector);
		} else if (!newSector.equals(oldSector)) {
			isChanged = true;
		}
		if (isChanged) {
			to_.setReferences(from_.getReferences());
			to_.setConverters(from_.getConverters());
			to_.setDescription(from_.getDescription());
			to_.setOptions(from_.getOptions());
		}
		return isChanged;
	}

	public static class ATWikiSymbol implements IATSymbol {

		private String id;
		private String model;
		private String name;
		private String description;
		private String sector;
		private String industry;
		private String source;
		private Set<String> references = new TreeSet<>();
		private Map<String, String> converters;
		private Map<Date, ATOptionChain> options;

		@Override
		public int compareTo(IATSymbol other_) {
			if (other_ == null)
				return -1;
			return this.getId().compareTo(other_.getId());
		}

		@Override
		public String convert(String provider_) {
			Map<String, String> prov = getConverters();
			String provId = prov != null ? prov.get(provider_) : null;
			return provId != null ? provId : getId();
		}

		@Override
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		@Override
		public String getModel() {
			return model;
		}

		public void setModel(String model) {
			this.model = model;
		}

		@Override
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		@Override
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public String getSector() {
			return sector;
		}

		public void setSector(String sector) {
			this.sector = sector;
		}

		@Override
		public String getIndustry() {
			return industry;
		}

		public void setIndustry(String industry) {
			this.industry = industry;
		}

		@Override
		public String getSource() {
			return source;
		}

		public void setSource(String source) {
			this.source = source;
		}

		@Override
		public Set<String> getReferences() {
			return references;
		}

		public void setReferences(Set<String> references) {
			this.references = references;
		}

		@Override
		public Map<String, String> getConverters() {
			return converters;
		}

		public void setConverters(Map<String, String> converters) {
			this.converters = converters;
		}

		@Override
		public Map<Date, ATOptionChain> getOptions() {
			return options;
		}

		public void setOptions(Map<Date, ATOptionChain> options) {
			this.options = options;
		}

		@Override
		public Boolean isSP500() {
			return true;
		}

		@Override
		public Boolean isR3000() {
			return null;
		}

	}

}
