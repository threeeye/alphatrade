package com.isoplane.at.gatherer.adapter;

import java.net.URLEncoder;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.isoplane.at.adapter.tradier.ATTradierBaseAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATTradierHistoryAdapter extends ATTradierBaseAdapter implements IATHistoryGatherer {

	static final Logger log = LoggerFactory.getLogger(ATTradierHistoryAdapter.class);

	// static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	// static final SimpleDateFormat trdDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	// static final SimpleDateFormat atDF = new SimpleDateFormat("yyyyMMddHHmm");
	static private Gson _gson = new Gson();

	public static void main(String[] args) throws Exception {
	//	PropertiesConfiguration prop = new PropertiesConfiguration();
	//	prop.read(new FileReader("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties"));

		ATConfigUtil.init("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties", false);
		
		Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		Date minDate = DateUtils.addWeeks(now, -1);
		Date start = getDayLTE(now, Calendar.FRIDAY);
		start = DateUtils.addHours(start, 15);
		// Date end = DateUtils.addHours(start, 1);
		Map<IATSymbol, String> testMap = new HashMap<>();
		testMap.put(new ATSymbol("$IXB"), ATFormats.DATE_yyyyMMdd.get().format(start));

		ATTradierHistoryAdapter adapter = new ATTradierHistoryAdapter();
		TreeSet<ATPriceHistory> history = adapter.getHistory(testMap, minDate);
		log.info(String.format("History [%d]", history.size()));
	}

	public ATTradierHistoryAdapter() {
		super();
	}

	public TreeSet<ATPriceHistory> getHistory(Map<IATSymbol, String> symMap_, Date minDate_) {
		TreeSet<ATPriceHistory> tradierResult = new TreeSet<>();
		int count = 0;
		for (Entry<IATSymbol, String> entry : symMap_.entrySet()) {
			try {
				count++;
				IATSymbol symbol = entry.getKey();
				// Only for index data
				if (!symbol.getId().startsWith("$"))
					continue;
				String fromStr = entry.getValue();
				Date from = StringUtils.isNotBlank(fromStr) ? ATFormats.DATE_yyyyMMdd.get().parse(fromStr) : null;
				if (from == null)
					from = minDate_;
				log.debug(String.format("[%4d/%4d] getHistory [%-5s]", count, symMap_.size(), symbol));
				TreeSet<ATPriceHistory> symResult = getSymbolHistory(symbol, from);
				if (symResult != null) {
					tradierResult.addAll(symResult);
				}
			} catch (Exception ex) {
				log.error(String.format("getHistory/%s]: %s", entry.getKey(), entry.getValue(), ex.getMessage()));
			}
		}
		return tradierResult;
	}

	private TreeSet<ATPriceHistory> getSymbolHistory(IATSymbol sym_, Date from_) {
		if (sym_==null || from_ == null)
			return null;
		try {
			String fromStr = ATFormats.DATE_yyyy_MM_dd.get().format(from_);
			fromStr = URLEncoder.encode(fromStr, "UTF-8");
			String trdSym = sym_.convert(PROVIDER_KEY);
			log.trace(String.format("getHistory [%s]", sym_));
			String url = String.format("/v1/markets/history?symbol=%s&start=%s", trdSym, fromStr);
			String content = getHttp(url);
			// log.info("content: " + content);
			TreeSet<ATPriceHistory> result = parseHistory(sym_, from_, content);
			Thread.sleep(500);
			return result;
		} catch (Exception ex) {
			log.error(String.format("getSymbolHistory [%s]", sym_), ex);
			return null;
		}
	}

	private TreeSet<ATPriceHistory> parseHistory(IATSymbol sym_, Date minDate_, String json_) {
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonElement history = root.getAsJsonObject().get("history");
		if (history.isJsonNull() || !history.isJsonObject()) {
			log.warn(String.format("No data for [%s]", sym_));
			return null;
		}
		JsonArray dataArray;
		JsonElement day = history.getAsJsonObject().get("day");
		if (day.isJsonArray()) {
			dataArray = day.getAsJsonArray();
		} else {
			dataArray = new JsonArray();
			dataArray.add(day);
		}
		String minTimestamp = ATFormats.DATE_yyyy_MM_dd.get().format(minDate_);
		TreeSet<ATPriceHistory> result = new TreeSet<>();
		for (JsonElement jsonElement : dataArray) {
			try {
				TradierHistory th = _gson.fromJson(jsonElement, TradierHistory.class);
				if (minTimestamp.compareTo(th.date) >= 0)
					continue;
				ATPriceHistory hist = th.toPriceHistory(sym_);
				if (hist != null) {
					result.add(hist);
				}
			} catch (Exception ex) {
				log.error(String.format("parseHistory [%s]: %s", jsonElement, ex.getMessage()));
			}
		}
		return result;
	}

	/**
	 * Return last day of week <= specified date.
	 * 
	 * @param date
	 *            - reference date.
	 * @param day
	 *            - DoW field from Calendar class.
	 * @return
	 */
	public static Date getDayLTE(Date date_, int day_) {
		Date date = DateUtils.truncate(date_, Calendar.DAY_OF_MONTH);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (day_ == cal.get(Calendar.DAY_OF_WEEK))
			return date;
		cal.add(Calendar.WEEK_OF_YEAR, -1);
		cal.set(Calendar.DAY_OF_WEEK, day_);
		return cal.getTime();
	}

	public static class TradierHistory {
		public String date;
		public Double open;
		public Double high;
		public Double low;
		public Double close;
		public Long volume;

		public ATPriceHistory toPriceHistory(IATSymbol sym_) throws ParseException {

			Date d = ATFormats.DATE_yyyy_MM_dd.get().parse(date);
			String dateStr = ATFormats.DATE_yyyyMMdd.get().format(d);
			String atId = String.format("%-5s%s", sym_.getId(), dateStr);

			ATPriceHistory pxh = new ATPriceHistory();
			pxh.setSource(SOURCE_KEY);
			pxh.setAtId(atId);
			pxh.setOccId(sym_.getId());
			pxh.setDateStr(dateStr);
			pxh.setOpen(open);
			pxh.setHigh(high);
			pxh.setLow(low);
			pxh.setClose(close); // unadjusted (obviously)
			pxh.setVolume(volume);
			return pxh;
		}
	}

	public static class ATPersistentFundamentals extends ATPersistentLookupBase implements Comparable<ATPersistentFundamentals>, IATAssetConstants {

		private static final long serialVersionUID = 1L;

		public ATPersistentFundamentals() {
			super();
			setType(FUNDAMENTAL_CODE);
		}

		public ATPersistentFundamentals(Map<String, Object> data_) {
			super(data_);
		}

		public Double getMarketCap() {
			Object value = get(IATAssetConstants.MARKET_CAP);
			if (value == null)
				return null;
			if (value instanceof Double)
				return (Double) value;
			if (value instanceof Long)
				return ((Long) value).doubleValue();
			return null;
		}

		public void setOccId(String value_) {
			super.put(OCCID, value_);
		}

		protected String getSortId() {
			return getAtId();
		}

		@Override
		public int compareTo(ATPersistentFundamentals other_) {
			String thisSortId = getSortId();
			if (thisSortId == null) {
				throw new ATException(String.format("Sort ID null [%s]", this));
			}
			String otherSortId = other_ != null ? other_.getSortId() : null;
			if (otherSortId == null) {
				throw new ATException(String.format("Sort ID null [%s]", other_));
			}
			int result = thisSortId.compareTo(otherSortId);
			return result;
		}

	}

}
