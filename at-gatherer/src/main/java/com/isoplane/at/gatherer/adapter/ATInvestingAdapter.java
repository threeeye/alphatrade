package com.isoplane.at.gatherer.adapter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistentEconomicEvent;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATHttpHelper;

// <iframe src="https://sslecal2.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&importance=3&features=datepicker,timezone,filters&countries=37,72,17,5&calType=week&timeZone=8&lang=1" width="650" height="467" frameborder="0" allowtransparency="true" marginwidth="0" marginheight="0"></iframe><div class="poweredBy" style="font-family: Arial, Helvetica, sans-serif;"><span style="font-size: 11px;color: #333333;text-decoration: none;">Real Time Economic Calendar provided by <a href="https://www.investing.com/" rel="nofollow" target="_blank" style="font-size: 11px;color: #06529D; font-weight: bold;" class="underline_link">Investing.com</a>.</span></div>
public class ATInvestingAdapter {

	static final Logger log = LoggerFactory.getLogger(ATInvestingAdapter.class);
	static final String SOURCE = "inv";

	static private SimpleDateFormat _eventDateFormat = new SimpleDateFormat("EEEEE, MMMMM dd, yyyy HH:mm");

//	public static void main(String[] args_) {
//		ATInvestingAdapter a = new ATInvestingAdapter();
//		a.getEconomicEvents();
//	}

	public Set<ATPersistentEconomicEvent> getEconomicEvents() {
		String url = "https://www.investing.com/economic-calendar/Service/getCalendarFilteredData";
		url = "https://sslecal2.forexprostools.com/?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&importance=3&features=datepicker,timezone&countries=37,72,17,5&calType=week&timeZone=8&lang=1";
		String content = ATHttpHelper.getHttp(url, null);
		Set<ATPersistentEconomicEvent> result = parseContent(content);
		return result;
	}

	private Set<ATPersistentEconomicEvent> parseContent(String html_) {
		Set<ATPersistentEconomicEvent> result = new TreeSet<>();
		Document doc = Jsoup.parse(html_);
		Elements tableRows = doc.select("#ecEventsTable tbody tr");
		if (tableRows == null || tableRows.isEmpty())
			return result;
		String dstr = null;
		for (Element row : tableRows) {
			try {
				Elements cells = row.select("td");
				if (cells == null || cells.isEmpty())
					continue;
				Element cell1 = cells.get(0);
				Set<String> classes = cell1.classNames();
				if (classes.contains("theDay")) {
					dstr = cell1.text().trim();
					continue;
				} else if (classes.contains("time")) {
					if (dstr == null)
						continue;
					ATPersistentEconomicEvent evt = new ATPersistentEconomicEvent();

					String tstr = cell1.text().trim();
					String ts_str = String.format("%s %s", dstr, tstr);
					if (ts_str.contains("All Day")) {
						log.info(String.format("Swapping 'All Day' -> '00:00' on [%s]", ts_str));
						ts_str = ts_str.replace("All Day", "00:00"); // Fix uncommon date/time
					}
					Date d = _eventDateFormat.parse(ts_str);
					evt.setDateStr(ATFormats.DATE_TIME_mid.get().format(d));

					String rgn = cells.get(1).select("span").first().attr("title").trim();
					evt.setRegion(rgn);

					String impStr = cells.get(2).attr("title").toLowerCase();
					if (impStr.contains("low")) {
						evt.setImportance(1);
					} else if (impStr.contains("moderate")) {
						evt.setImportance(5);
					} else if (impStr.contains("high")) {
						evt.setImportance(10);
					}

					String dsc = cells.get(3).text();
					evt.setDescription(dsc);

					if (cells.size() > 4) {
						String actStr = cells.get(4).text();
						if (!StringUtils.isBlank(actStr)) {
							char lastChar = actStr.charAt(actStr.length() - 1);
							if (Character.isDigit(lastChar)) {
								evt.setActual(Double.parseDouble(actStr));
							} else if (actStr.length() > 1) {
								evt.setActual(Double.parseDouble(actStr.substring(0, actStr.length() - 1)));
							}
						}
						String foreStr = cells.get(5).text();
						if (!StringUtils.isBlank(foreStr)) {
							char lastChar = foreStr.charAt(foreStr.length() - 1);
							if (Character.isDigit(lastChar)) {
								evt.setForecast(Double.parseDouble(foreStr));
							} else if (foreStr.length() > 1) {
								evt.setForecast(Double.parseDouble(foreStr.substring(0, foreStr.length() - 1)));
							}
						}
						String prevStr = cells.get(6).text();
						if (!StringUtils.isBlank(prevStr)) {
							char lastChar = prevStr.charAt(prevStr.length() - 1);
							if (Character.isDigit(lastChar)) {
								evt.setPrevious(Double.parseDouble(prevStr));
							} else if (prevStr.length() > 1) {
								evt.setPrevious(Double.parseDouble(prevStr.substring(0, prevStr.length() - 1)));
								evt.setUnit(Character.toString(lastChar));
							}
						}
					}
					int atId = String.format("%s%s%s", evt.getDateStr(), evt.getRegion(), evt.getDescription()).hashCode();
					evt.setAtId("evt" + atId);
					evt.setSource(SOURCE);
					result.add(evt);
					log.debug(String.format("Event: %s", evt));
				}
			} catch (Exception ex) {
				log.error(String.format("Error parsing: %s", row.toString()), ex);
			}
		}
		return result;
	}

}
