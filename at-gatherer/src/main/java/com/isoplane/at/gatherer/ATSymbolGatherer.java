package com.isoplane.at.gatherer;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ATAdapterRegistry;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATMorningstarCodes;
import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.gatherer.ATGatherer.SelectSymbolsProvider;
import com.isoplane.at.gatherer.ATGatherer.SymbolsProvider;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

public class ATSymbolGatherer {

	static final Logger log = LoggerFactory.getLogger(ATSymbolGatherer.class);

	static public void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 2)
				throw new ATException(String.format("Invalid args '<config.path>, <symbol>'"));

			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			Configuration config = ATConfigUtil.init(configPath, true);
			String[] symbols = args_[1].trim().split("\\s*,\\s*");

			long start = System.currentTimeMillis();
			List<String> args = Arrays.asList(
					"once",
					"forceRefresh",
					// ATGatherer.EARNINGS,
					// ATGatherer.DIVIDENDS,
					// ATGatherer.DIVIDEND_EST,
					// ATGatherer.ECONOMIC,
					ATGatherer.HISTORY,
					ATGatherer.HISTORY_SHORT_TERM,
					// ATGatherer.IV,
					ATGatherer.FUNDAMENTALS_1,
					ATGatherer.FUNDAMENTALS_2,
					ATGatherer.STATISTICS,
					ATGatherer.FOUNDATION);

	//		args = Arrays.asList("once", "forceRefresh", ATGatherer.FOUNDATION);

			log.info(String.format("Running %s", args));

			ATExecutors.init();
			ATAdapterRegistry.init();
			ATMongoTableRegistry.init();
			ATMorningstarCodes.init(config);

			ATGatherer gatherer = new ATGatherer(args);
			SymbolsProvider symPrv = new SelectSymbolsProvider(symbols);
			gatherer.batch(symPrv);

			long duration = System.currentTimeMillis() - start;
			ATSysLogger.info(String.format("Finished in [%s]", DurationFormatUtils.formatDurationHMS(duration)));
			System.exit(0);
		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATSymbolGatherer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}
}
