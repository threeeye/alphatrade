package com.isoplane.at.gatherer.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.adapter.tradier.ATTradierBaseAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATMorningstarCodes;
import com.isoplane.at.commons.util.ATMorningstarCodes.ATMorningstarCode;

public class ATTradierFundamentalsAdapter extends ATTradierBaseAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierFundamentalsAdapter.class);

	static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	static private Gson _gson = new Gson();

	static String urlBase = "/beta/markets/fundamentals/company?symbols=%s";

	// private Configuration _config;
	private Set<ATSymbol> _updatedSymbols;

	public ATTradierFundamentalsAdapter() {
		super();
	}

	// public TreeSet<ATPersistentFundamentals> getFundamentals(Set<IATSymbol> symbols_) {
	// // syms_.clear();
	// // syms_.put("GOOGL", null);
	// Map<String, String> symbolMap = symMap_.keySet().stream().collect(Collectors.toMap(s -> s.convert(PROVIDER_KEY), s -> s.getId()));
	// TreeSet<ATPersistentFundamentals> fundamentals = getFundamentals(symbolMap.keySet());
	// TreeSet<ATPersistentFundamentals> result_digest = new TreeSet<>(); // NOTE: New map, ID used for compare
	// for (ATPersistentFundamentals fnd : fundamentals) {
	// // NOTE: Performance can be increased by mapping during parse
	// String id = symbolMap.get(fnd.getAtId());
	// fnd.setAtId(id);
	// result_digest.add(fnd);
	// }
	// return result_digest;
	// }

	public Set<ATSymbol> getUpdatedSymbols() {
		return _updatedSymbols;
	}

	public TreeSet<ATPersistentFundamentals> getFundamentals(Collection<IATSymbol> symbols_) {
		_updatedSymbols = new TreeSet<>();
		TreeSet<ATPersistentFundamentals> result = new TreeSet<>();
		if (symbols_ == null || symbols_.isEmpty())
			return result;
		try {
			Configuration config = ATConfigUtil.config();
			int bulkSize = config.getInt("gatherer.fnd.bulksize");
			long delay = config.getLong("gatherer.fnd.delay");
			// if(symbols_.contains("SUN")) {
			// log.info("SUN");
			// }

			log.trace(String.format("getFundamentals [%d/%d]", symbols_.size(), bulkSize));
			List<List<IATSymbol>> symbolPartitions = ListUtils.partition(new ArrayList<>(symbols_), bulkSize);
			List<IATSymbol> failedList = new ArrayList<>();
			int count = 0;
			for (List<IATSymbol> partition : symbolPartitions) {
				try {
					Set<ATPersistentFundamentals> partResult = getFundamentalsPartition(partition);
					if (partResult != null) {
						result.addAll(partResult);
						count += partResult.size();
						log.debug(String.format("[%3d/%3d] getFundamentals %s", count, symbols_.size(), partition));
					}
					Thread.sleep(delay);
				} catch (Exception exx) {
					log.error(String.format("getFundamentals %s - %s", partition, exx.getMessage()));
					failedList.addAll(partition);
				}
			}
			if (!failedList.isEmpty()) {
				log.info(String.format("Retrying failed fundamentals %s", failedList));
				List<IATSymbol> tmp = new ArrayList<>(failedList);
				for (IATSymbol symbol : tmp) {
					List<IATSymbol> partition = Arrays.asList(symbol);
					try {
						Set<ATPersistentFundamentals> partResult = getFundamentalsPartition(partition);
						if (partResult != null) {
							result.addAll(partResult);
							count += partResult.size();
							failedList.removeAll(partition);
							log.debug(String.format("[%3d/%3d] getFundamentals %s", count, symbols_.size(), partition));
						}
						Thread.sleep(delay);
					} catch (Exception exx) {
						log.error(String.format("Permanently failed retrieving fundamental [%s] - %s", symbol, exx.getMessage()));
						failedList.addAll(partition);
					}
				}
			}
			log.error(String.format("Permanently failed list %s", failedList));
			return result;
		} catch (Exception ex) {
			log.error(String.format("getFundamentals [%s]", symbols_), ex);
			return result;
		}
	}

	private Set<ATPersistentFundamentals> getFundamentalsPartition(List<IATSymbol> partition_) {
		Set<ATPersistentFundamentals> result = new TreeSet<>();
		// long delay = _config.getLong("gatherer.fnd.delay");
		Map<String, String> convMap = new HashMap<>();
		Map<String, IATSymbol> trdMap = new HashMap<>();
		Set<String> trdSyms = new TreeSet<>();
		for (IATSymbol psym : partition_) {
			String trdSym = psym.convert(PROVIDER_KEY);
			trdSyms.add(trdSym);
			trdMap.put(psym.getId(), psym);
			convMap.put(trdSym, psym.getId());
		}
		String trdUrlSym = String.join(",", trdSyms);
		String requestUrl = String.format(urlBase, trdUrlSym);
		String jsonStr = getHttp(requestUrl);
		// int count = 0;
		if (StringUtils.isNotBlank(jsonStr)) {
			JsonElement root = _gson.fromJson(jsonStr, JsonElement.class);
			JsonArray rootArray = root.getAsJsonArray();
			for (JsonElement rootElement : rootArray) {
				JsonObject rootObj = rootElement.getAsJsonObject();
				String rootType = rootObj.get("type").getAsString();
				JsonElement resultsElement = rootObj.get("results");
				if (!"Symbol".equals(rootType) || resultsElement == null || !resultsElement.isJsonArray())
					continue;
				// count++;
				String trdSym = rootObj.get("request").getAsString();
				String symbol = convMap.get(trdSym);
				ATPersistentFundamentals fnd = new ATPersistentFundamentals();
				fnd.setOccId(symbol);
				// http://morningstardirect.morningstar.com/clientcomm/DataDefinitions_EquityandExecutive.pdf
				JsonArray resultsArray = resultsElement.getAsJsonArray();
				for (JsonElement resultElement : resultsArray) {
					try {
						JsonObject resultObj = resultElement.getAsJsonObject();
						JsonElement tablesElement = resultObj.get("tables");
						if (tablesElement == null || tablesElement.isJsonNull()) {
							log.error(String.format("Missing tables [%s]", symbol));
							continue;
						}
						JsonObject resultTables = tablesElement.getAsJsonObject();
						String resultType = resultObj.get("type").getAsString();
						if ("Company".equals(resultType)) {
							extractString(resultTables, "long_descriptions", fnd, "desc_l");

							JsonElement cpElement = resultTables.get("company_profile");
							if (cpElement == null || cpElement.isJsonNull()) {
								log.error(String.format("Missing company_profile [%s]", symbol));
								continue;
							}
							JsonObject company_profile = cpElement.getAsJsonObject();
							extractString(company_profile, "short_description", fnd, "desc_s");

							JsonObject asset_classification = resultTables.get("asset_classification").getAsJsonObject();

							// Financial Heath Grade: Instead of using accounting-based ratios to formulate a measure to reflect the
							// financial
							// health of a firm, we use structural or contingent claim models. Structural models take advantage of both
							// market
							// information and accounting financial information. The firm's equity in such models is viewed as a call
							// option
							// on the value of the firm's assets. If the value of the assets is not sufficient to cover the firm's
							// liabilities
							// (the strike price), default is expected to occur, and the call option expires worthless and the firm is
							// turned
							// over to its creditors. To estimate a distance to default, the value of the firm's liabilities is obtained
							// from
							// the firm's latest balance sheet and incorporated into the model. We then rank the calculated distance to
							// default and award 10% of the universe A's, 20% B's, 40% C's, 20% D's, and 10% F's. Morningstar calculates
							// this
							// figure in-house on a daily basis
							extractString(asset_classification, "financial_health_grade", fnd, "mst_fhlt");
							extractDateString(asset_classification, "FinancialHealthGrade.asOfDate", fnd, "mst_fhlt_dstr");

							// Growth Grade: The growth grade is based on the trend in revenue per share using data from the past five
							// years.
							// For the purpose of calculating revenue per share we use the past five years' revenue figures and
							// corresponding
							// year-end fully diluted shares outstanding; if yearend fully diluted shares outstanding is not available, we
							// calculate this figure by dividing the company's reported net income applicable to common shareholders by
							// the
							// reported fully diluted earnings per share. A company must have a minimum of four consecutive years of
							// positive
							// and non-zero revenue, including the latest fiscal year, to qualify for a grade.
							// In calculating the revenue per share growth rate, we calculate the slope of the regression line of
							// historical
							// revenue per share. We then divide the slope of the regression line by the arithmetic average of historical
							// revenue per share figures. The result_digest of the regression is a normalized historical increase or decrease in
							// the
							// rate of growth for sales per share. We then calculate a z-score by subtracting the universe mean revenue
							// growth
							// from the company's revenue growth, and dividing by the standard deviation of the universe's growth rates.
							// Stocks are sorted based on the z-score of their revenue per share growth rate calculated above, from the
							// most
							// negative z-score to the most positive z-score. Stocks are then ranked based on their z-score from 1 to the
							// total number of qualified stocks. We assign grades based on this ranking. Stocks are assigned A, B, C, D,
							// or F.
							// Morningstar calculates this figure in-house on a monthly basis.
							extractString(asset_classification, "growth_grade", fnd, "mst_grw_grd");
							extractDateString(asset_classification, "GrowthGrade.asOfDate", fnd, "mst_grw_grd_dstr");

							// Growth Score: A high overall growth score indicates that a stocks per-share earnings, book value,
							// revenues,
							// and cash flow are expected to grow quickly relative to other stocks in the same scoring group. A weak
							// growth
							// orientation does not necessarily mean that a stock has a strong value orientation
							extractDouble(asset_classification, "growth_score", fnd, "mst_grw_sc");
							extractInteger(asset_classification, "morningstar_economy_sphere_code", fnd, "mst_ecosph");
							extractInteger(asset_classification, "morningstar_industry_code", fnd, "mst_ind");
							extractInteger(asset_classification, "morningstar_industry_group_code", fnd, "mst_ind_grp");
							extractInteger(asset_classification, "morningstar_sector_code", fnd, "mst_sect");

							/*
							 * Profitability Grade: The profitability grade for all qualified companies in Morningstar's stock universe is
							 * based on valuation of return on shareholders' equity (ROE) using data from the past five years.
							 * Morningstar's
							 * universe of stocks is first filtered for adequacy of historical ROE figures. Companies with less than four
							 * years of consecutive ROE figures including the ROE figure for the latest fiscal year are tossed from
							 * calculations and are assigned -- for the profitability grade. For the remaining qualified universe of
							 * stocks
							 * the profitability grade is based on the valuation of the following three components, which are assigned
							 * different weights; the historical growth rate of ROE, the average level of historical ROE, the level of ROE
							 * in
							 * the latest fiscal year of the company. Stocks are assigned A, B, C, D, or F. Morningstar calculates this
							 * figure
							 * in-house on a monthly basis
							 */
							extractString(asset_classification, "profitability_grade", fnd, "mst_prft_grd");
							extractDateString(asset_classification, "ProfitabilityGrade.asOfDate", fnd, "mst_prft_grd_dstr");

							// Size Score: The investment size indicator which serves as the Y-axis value of the Morningstar Style Box.
							// Stocks
							// are given a score based the cumulative market capitalization of its style zone. Morningstar calculates this
							// figure in-house monthly based on data form the past five years
							extractDouble(asset_classification, "size_score", fnd, "mst_sz_sc");

							Integer stype = extractInteger(asset_classification, "stock_type", fnd, "mst_stype");
							extractDateString(asset_classification, "StockType.asOfDate", fnd, "mst_stype_dstr");
							switch (stype) {
							case 1:
								// 1 - Aggressive Growth: companies whose sales and earnings have grown very rapidly over the trailing
								// five-year period. These firms tend to be a step up the quality ladder from speculative-growth firms.
								fnd.put("mstr_stype_dsc", "aggressive");
								break;
							case 2:
								// 2 - Classic Growth: companies that show moderate to rapid growth over the trailing five-year period in
								// two
								// of the following three categories: sales, earnings, dividends. These tend to be fairly mature firms,
								// but
								// ones that are still generating steady growth.
								fnd.put("mstr_stype_dsc", "classic");
								break;
							case 3:
								// 3 - Cyclicals: companies whose core business can be expected to fluctuate in line with the overall
								// economy.
								// In a booming economy such companies will look excellent; in a recession, their growth stalls and they
								// might
								// even lose money.
								fnd.put("mstr_stype_dsc", "cyclical");
								break;
							case 4:
								// 4 - Distressed: companies that are having serious operating problems. This could mean declining cash
								// flow,
								// negative earnings, high debt, or some combination of these. Such turnaround stocks tend to be highly
								// risky, but also harbor some intriguing investments.
								fnd.put("mstr_stype_dsc", "distressed");
								break;
							case 5:
								// 5 - Hard Asset-5: companies whose main business revolves around the ownership or exploitation of hard
								// assets like real estate, metals, timber, etc. Such companies typically sport a low correlation with the
								// overall stock market, and have traditionally been where investors look for inflation hedges.
								fnd.put("mstr_stype_dsc", "hard");
								break;
							case 6:
								// 6 - High Yield: companies whose stocks offer a high dividend yield. These tend to be mature companies
								// that
								// choose not to reinvest the bulk of their earnings. For investors interested in income, this is where to
								// look.
								fnd.put("mstr_stype_dsc", "highYld");
								break;
							case 7:
								// 7 - Slow Growth: companies that have grown slowly, if at all, over the trailing five-year period. These
								// companies tend to be mature firms.
								fnd.put("mstr_stype_dsc", "slow");
								break;
							case 8:
								// 8 - Speculative Growth: companies whose sales have grown very rapidly over the trailing five-year
								// period,
								// but whose earnings growth has been spotty. These tend to be companies in the early phase of their
								// growth
								// cycle.
								fnd.put("mstr_stype_dsc", "speculative");
								break;
							}
							Integer sstyle = extractInteger(asset_classification, "style_box", fnd, "mst_sstyle");
							extractDateString(asset_classification, "StyleBox.asOfDate", fnd, "mst_sstyle_dstr");
							switch (sstyle) {
							case 1:
								fnd.put("mstr_sstyle_dsc", "lg_value");
								break;
							case 2:
								fnd.put("mstr_sstyle_dsc", "lg_core");
								break;
							case 3:
								fnd.put("mstr_sstyle_dsc", "lg_growth");
								break;
							case 4:
								fnd.put("mstr_sstyle_dsc", "md_value");
								break;
							case 5:
								fnd.put("mstr_sstyle_dsc", "md_core");
								break;
							case 6:
								fnd.put("mstr_sstyle_dsc", "md_growth");
								break;
							case 7:
								fnd.put("mstr_sstyle_dsc", "sm_value");
								break;
							case 8:
								fnd.put("mstr_sstyle_dsc", "sm_core");
								break;
							case 9:
								fnd.put("mstr_sstyle_dsc", "sm_growth");
								break;
							}

							// Style Score: The investment style indicator which serves as X-axis value of the Morningstar Style Box.
							// Stocks
							// are given a value score based on five fundamental measures and a growth score based on five growth rates.
							// Stocks are scored against their style zone and size peers. The difference between the stocks growth and
							// value
							// scores is the net style score. Morningstar calculates this figure inhouse monthly based on data form the
							// past
							// five years.
							extractDouble(asset_classification, "style_score", fnd, "mst_sstyle_scr");

							// Value Score: A high value score indicates that a stocks price is relatively low, given the anticipated
							// per-sharing earnings, book value, revenues, cash flow, and dividends that the stock provides to investors.
							// A
							// high price relative to these measures indicates that a stocks value orientation is weak, but it does not
							// necessarily mean that the stock is growth-oriented
							extractDouble(asset_classification, "value_score", fnd, "mst_val_scr");

						} else if ("Stock".equals(resultType)) {
							JsonElement scpElement = resultTables.get("share_class_profile");
							if (scpElement == null || scpElement.isJsonNull()) {
								log.error(String.format("Missing share_class_profile [%s]", symbol));
								continue;
							}
							JsonObject share_class_profile = scpElement.getAsJsonObject();

							extractLong(share_class_profile, "enterprise_value", fnd, "ent_val");
							extractLong(share_class_profile, "market_cap", fnd, "mcap");
							String dstr = extractDateString(share_class_profile, "MarketCap.asOfDate", fnd, IATAssetConstants.DATE_STR);
							if (StringUtils.isBlank(dstr)) {
								log.error(String.format("Missing MarketCap.asOfDate [%s]", symbol));
								continue;
							}
							String atId = String.format("%s_%s", symbol, dstr);
							fnd.setAtId(atId);

							extractInteger(share_class_profile, "shares_outstanding", fnd, "sh_out");
							extractDateString(share_class_profile, "SharesOutstanding.asOfDate", fnd, "sh_out_dstr");

							JsonObject share_class = resultTables.get("share_class").getAsJsonObject();
							extractString(share_class, "c_u_s_i_p", fnd, "cusip");
							extractDateString(share_class, "i_p_o_date", fnd, "ipo_dstr");

							String dlst_dstr = extractDateString(share_class, "delisting_date", fnd, "dlst_dstr");
							if ("1970-01-01".equals(dlst_dstr)) {
								fnd.remove("dlst_dstr");
							}
						} else {
							log.warn(String.format("Unknown result_digest type [%s/%s - %s]", rootType, symbol, resultType));
						}
					} catch (Exception ex) {
						log.error(String.format("getFundamentalsPartition [%s]: %s", symbol, resultElement), ex);
					}
				}
				IATSymbol sym = trdMap.get(fnd.get(IATAssetConstants.OCCID));
				if (sym != null) {
					boolean isChanged = false;
					ATSymbol newSym = new ATSymbol(sym);
					if (StringUtils.isBlank(sym.getDescription())) {
						String descr = (String) fnd.get("desc_l");
						if (StringUtils.isBlank(descr)) {
							descr = (String) fnd.get("desc_s");
						}
						if (descr != null) {
							newSym.setDescription(descr);
							isChanged = true;
						}
					}
					Integer code = (Integer) fnd.get("mst_ind");
					ATMorningstarCode ms = code != null ? ATMorningstarCodes.getIndustryCode(code) : null;
					if (ms != null) {
						if (StringUtils.isBlank(sym.getSector())) {
							newSym.setSector(ms.sector);
							isChanged = true;
						}
						if (StringUtils.isBlank(sym.getIndustry())) {
							newSym.setIndustry(ms.industry);
							isChanged = true;
						}
					}
					if (isChanged) {
						Set<String> refs = newSym.getReferences();
						if (refs == null) {
							refs = new TreeSet<>();
							newSym.setReferences(refs);
						}
						refs.add("trd");
						_updatedSymbols.add(newSym);
					}
				}
				if (!StringUtils.isBlank(fnd.getAtId())) {
					fnd.put(IATAssetConstants.SOURCE, "TRD");
					result.add(fnd);
				}
			}
		}
		return result;
		// log.debug(String.format("Throttling [%d ms]", delay));
		// } catch (Exception exx) {
		// log.error(String.format("Error getting fundamentals [%s]", symStr), exx);
		// }
	}

	private Double extractDouble(JsonObject jobj_, String field_, ATPersistentFundamentals fnd_, String fndField_) {
		Double value = null;
		try {
			JsonElement je = jobj_.get(field_);
			if (je != null && !je.isJsonNull()) {
				value = je.getAsDouble();
				fnd_.put(fndField_, value);
			}
		} catch (Exception ex) {
			String msg = String.format("Error extracting [%s.%s]: %s - %s", fnd_.getAtId(), fndField_, jobj_, ex.getMessage());
			log.error(msg);
		}
		return value;
	}

	private Integer extractInteger(JsonObject jobj_, String field_, ATPersistentFundamentals fnd_, String fndField_) {
		Integer value = null;
		try {
			JsonElement je = jobj_.get(field_);
			if (je != null && !je.isJsonNull()) {
				value = je.getAsInt();
				fnd_.put(fndField_, value);
			}
		} catch (Exception ex) {
			String msg = String.format("extractInteger [%s.%s]: %s - %s", fnd_.getAtId(), fndField_, jobj_, ex.getMessage());
			log.error(msg);
		}
		return value;
	}

	private Long extractLong(JsonObject jobj_, String field_, ATPersistentFundamentals fnd_, String fndField_) {
		Long value = null;
		try {
			JsonElement je = jobj_.get(field_);
			if (je != null && !je.isJsonNull()) {
				value = je.getAsLong();
				fnd_.put(fndField_, value);
			}
		} catch (Exception ex) {
			String msg = String.format("extractLong [%s.%s]: %s - %s", fnd_.getAtId(), fndField_, jobj_, ex.getMessage());
			log.error(msg);
		}
		return value;
	}

	private String extractString(JsonObject jobj_, String field_, ATPersistentFundamentals fnd_, String fndField_) {
		String value = null;
		try {
			JsonElement je = jobj_.get(field_);
			if (je != null && !je.isJsonNull()) {
				value = je.getAsString();
				fnd_.put(fndField_, value);
			}
		} catch (Exception ex) {
			String msg = String.format("extractString [%s.%s]: %s - %s", fnd_.getAtId(), fndField_, jobj_, ex.getMessage());
			log.error(msg);
		}
		return value;
	}

	private String extractDateString(JsonObject jobj_, String field_, ATPersistentFundamentals fnd_, String fndField_) {
		String value = null;
		try {
			JsonElement je = jobj_.get(field_);
			if (je != null && !je.isJsonNull()) {
				value = je.getAsString();
				ATFormats.DATE_yyyy_MM_dd.get().parse(value); // validate
				fnd_.put(fndField_, value);
			}
		} catch (Exception ex) {
			String msg = String.format("extractDateString [%s.%s]: %s - %s", fnd_.getAtId(), fndField_, jobj_, ex.getMessage());
			log.error(msg);
		}
		return value;
	}

	public static class ATPersistentFundamentals extends ATPersistentLookupBase implements Comparable<ATPersistentFundamentals>, IATAssetConstants {

		private static final long serialVersionUID = 1L;

		public ATPersistentFundamentals() {
			super();
			setType(FUNDAMENTAL_CODE);
		}

		public ATPersistentFundamentals(Map<String, Object> data_) {
			super(data_);
		}

		public Double getMarketCap() {
			Object value = get(IATAssetConstants.MARKET_CAP);
			if (value == null)
				return null;
			if (value instanceof Double)
				return (Double) value;
			if (value instanceof Long)
				return ((Long) value).doubleValue();
			return null;
		}

		public void setOccId(String value_) {
			super.put(OCCID, value_);
		}

		protected String getSortId() {
			return getAtId();
		}

		@Override
		public int compareTo(ATPersistentFundamentals other_) {
			String thisSortId = getSortId();
			if (thisSortId == null) {
				throw new ATException(String.format("Sort ID null [%s]", this));
			}
			String otherSortId = other_ != null ? other_.getSortId() : null;
			if (otherSortId == null) {
				throw new ATException(String.format("Sort ID null [%s]", other_));
			}
			int result = thisSortId.compareTo(otherSortId);
			return result;
		}

	}

}
