package com.isoplane.at.gatherer.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATHttpHelper {

	static final Logger log = LoggerFactory.getLogger(ATHttpHelper.class);

	static private PoolingHttpClientConnectionManager _httpConManager = new PoolingHttpClientConnectionManager();

	static public String getHttp(String url_) {
		Map<String, String> headers = new HashMap<>();
		headers.put("Accept", "application/json");
		return getHttp(url_, headers);
	}

	// static public String getHttp(String url_, String accept_) {
	// try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
	// HttpGet httpGet = new HttpGet(url_);
	// httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
	// if (!StringUtil.isBlank(accept_)) {
	// httpGet.setHeader("Accept", accept_);
	// }
	//
	// CloseableHttpResponse response = httpclient.execute(httpGet);
	// // log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
	// HttpEntity entity = response.getEntity();
	//
	// StringBuffer sb = new StringBuffer();
	// BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
	// String line;
	// while ((line = rd.readLine()) != null) {
	// sb.append(line).append(System.lineSeparator());
	// }
	// String content = sb.toString();
	// EntityUtils.consume(entity);
	//
	// return content;
	// } catch (Exception ex) {
	// log.error(String.format("Error on [%s]", url_), ex);
	// return null;
	// }
	// }

	static public String getHttp(String url_, Map<String, String> headers_) {
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(url_);
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			if (headers_ != null && !headers_.isEmpty()) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpGet.setHeader(header.getKey(), header.getValue());
				}
			}
			// if (!StringUtil.isBlank(accept_)) {
			// httpGet.setHeader("Accept", accept_);
			// }

			CloseableHttpResponse response = httpclient.execute(httpGet);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			return content;
		} catch (Exception ex) {
			log.error(String.format("Error on [%s]", url_), ex);
			return null;
		}
	}

	static public String postHttp(String url_, Map<String, String> params_, boolean isJson_) {
		try (CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(_httpConManager).build()) {
			HttpPost httpPost = new HttpPost(url_);

			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			for (Entry<String, String> e : params_.entrySet()) {
				String[] tokens = e.getValue().split(",");
				for (String token : tokens) {
					postParameters.add(new BasicNameValuePair(e.getKey(), token));
				}
			}
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			if (isJson_) {
				httpPost.setHeader("Accept", "application/json");
			}
			httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");

			CloseableHttpResponse response = httpclient.execute(httpPost);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			return content;
		} catch (Exception ex) {
			log.error(String.format("Error on [%s]", url_), ex);
			return null;
		}
	}

	// public void downloadHttps() {
	// BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
	// credentialsProvider.setCredentials(AuthScope.ANY, new org.apache.http.auth. // UsernamePasswordCredentials(REST_USER, REST_PASS));
	// SSLContextBuilder builder = new SSLContextBuilder();
	// builder.loadTrustMaterial(null, new TrustStrategy() {
	// @Override
	// public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	// return true;
	// }
	// });
	// SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build(),
	// SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	// }
}
