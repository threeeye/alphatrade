package com.isoplane.at.gatherer.adapter;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATHttpHelper;

public class ATStooqAdapter {

	static final Logger log = LoggerFactory.getLogger(ATStooqAdapter.class);

	static final String SOURCE_KEY = "Stoq";
	private IATConfiguration _config;

	public ATStooqAdapter(IATConfiguration config_) {
		_config = config_;
	}

	public TreeSet<ATPriceHistory> getHistory(Map<String, String> syms_) {
		TreeSet<ATPriceHistory> result = new TreeSet<>();
		long period = _config.getLong("stooqPeriod", 1000);
		for (Entry<String, String> entry : syms_.entrySet()) {
			String sym = entry.getKey();
			String from = entry.getValue();
			String to = ATFormats.DATE_yyyyMMdd.get().format(new Date());
			if (from.equals(to))
				continue;
			try {
				log.debug(String.format("Getting history [%-5s: %s-%s]", sym, from, to));
				String url = String.format("https://stooq.com/q/d/l/?i=d&o=1111111&s=%s.us&d1=%s&d2=%s", sym, from, to);
				String content = ATHttpHelper.getHttp(url);
				if (StringUtils.isBlank(content)) {
					continue;
				} else if (content.toLowerCase().contains("exceeded")) {
					log.error(content);
					return result;
				}
				List<ATPriceHistory> history = parseCsv(sym, content);
				result.addAll(history);
				Thread.sleep(period);
			} catch (Exception ex) {
				log.error(String.format("Error processing [%-5s: %s-%s]", sym, from, to), ex);
			}
		}
		return result;
	}

	private List<ATPriceHistory> parseCsv(String occId_, String csv_) throws IOException {
		List<ATPriceHistory> result = new ArrayList<>();
		if (StringUtils.isEmpty(csv_))
			return result;
		CSVParser records = CSVFormat.DEFAULT.parse(new StringReader(csv_));
		for (CSVRecord rec : records) {
			if (rec.getRecordNumber() == 1)
				continue; // -> Header
			String dateStr = rec.get(0).replaceAll("-", "");
			String atId = String.format("%-5s%s", occId_, dateStr);
			try {
				ATPriceHistory pxh = new ATPriceHistory();
				pxh.setSource(SOURCE_KEY);
				pxh.setAtId(atId);
				pxh.setOccId(occId_);
				pxh.setDateStr(dateStr);
				pxh.setOpen(Double.parseDouble(rec.get(1)));
				pxh.setHigh(Double.parseDouble(rec.get(2)));
				pxh.setLow(Double.parseDouble(rec.get(3)));
				pxh.setClose(Double.parseDouble(rec.get(4)));
				pxh.setVolume(Long.parseLong(rec.get(5)));
				result.add(pxh);
			} catch (Exception ex) {
				log.error(String.format("Error parsing [%s]", atId), ex);
			}
		}
		return result;
	}

}
