package com.isoplane.at.gatherer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.InetAddress;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.adapter.ATAdapterRegistry;
import com.isoplane.at.adapter.ameritrade.ATAmeritradeWebApiAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATPriceHistory;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.ATEarning;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentAssetBase;
import com.isoplane.at.commons.store.ATPersistentBridgeMap;
import com.isoplane.at.commons.store.ATPersistentEconomicEvent;
import com.isoplane.at.commons.store.ATPersistentEquityDescription;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFileAppender;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATMathUtil;
import com.isoplane.at.commons.util.ATMorningstarCodes;
import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.gatherer.adapter.ATBenzingaAdapter;
import com.isoplane.at.gatherer.adapter.ATStockRoverAdapter;
import com.isoplane.at.gatherer.adapter.ATTradierFundamentalsAdapter;
import com.isoplane.at.gatherer.adapter.ATTradierFundamentalsAdapter.ATPersistentFundamentals;
import com.isoplane.at.gatherer.adapter.ATTradierHistoryAdapter;
import com.isoplane.at.gatherer.adapter.ATTradierSymbolNameAdapter;
import com.isoplane.at.gatherer.adapter.ATTradierTimeSalesAdapter;
import com.isoplane.at.gatherer.adapter.ATTradingBlockAdapter;
import com.isoplane.at.gatherer.adapter.ATTradingEconomicsAdapter;
import com.isoplane.at.gatherer.adapter.ATWikipediaAdapter;
import com.isoplane.at.gatherer.adapter.IATHistoryGatherer;
import com.isoplane.at.marketdata.ATMarketDataManager;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.symbols.ATSymbolManager;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Attachments;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

public class ATGatherer {

	static {
		String logPath = System.getProperty("logPath");
		ATFileAppender.overrideFile(logPath);
	}

	static final Logger log = LoggerFactory.getLogger(ATGatherer.class);

	final static public String COMPLETED = "completed";
	final static public String CURRENT = "current";

	final static public String TRADES = "trades";
	final static public String SYMBOLS = "symbols";
	final static public String EARNINGS = "earnings";
	final static public String DIVIDENDS = "dividends";
	final static public String DIVIDEND_EST = "est_dividends";
	final static public String ECONOMIC = "economic";
	final static public String SP500 = "sp500";
	final static public String HISTORY = "history";
	final static public String HISTORY_SHORT_TERM = "history-st";
	final static public String IV = "IV";
	final static public String FUNDAMENTALS_1 = "fundamentals_1";
	final static public String FUNDAMENTALS_2 = "fundamentals_2";
	final static public String STATISTICS = "statistics";
	final static public String FOUNDATION = "foundation";
	final static public String ASSETS = "assets";
	// final private String OPTIONS = "options";
	final static public String OPTION_EXPIRATIONS = "option_exp";
	final static public String EMAIL = "email";
	final static public String TEST = "test";
	final static public String TEST_SPECIAL = "special";

	private List<String> _args;
	// private IATConfiguration _atConfig;
	// private Configuration _config;
	private ATMongoDao _dao;
	private ATGathererController _ctrl;
	static private ATGatherer _instance;
	private boolean _isActive;
	static private boolean _skipWeekend = true;
	private SymbolsProvider _symbolsProvider;
	private ATSymbolManager _symbolsManager;
	private ATPortfolioManager _portfolioManager;
	private ATMarketDataManager _mktManager;
	private Map<String, String> _statusMap = new HashMap<>();
	private List<String> _completedList;
	private Map<String, Long> _startTimeMap = new HashMap<>();

	private String _foundationTable;
	// private String _historyTable;
	private Gson _gson;

	static public void main(String[] args_) {
		try {
			InetAddress ip = InetAddress.getLocalHost();
			final String ipStr = ip.getHostAddress();
			ATSysLogger.setLoggerClass(String.format("ATGatherer.[%s]", ipStr));

			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			long start = System.currentTimeMillis();
			ATSysLogger.info(String.format("Starting process..."));
			ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
			List<String> args = Arrays.asList(args_).stream().map(String::toLowerCase).collect(Collectors.toList());
			log.info(String.format("Running %s", args));
			_skipWeekend = !args.contains("weekend");

			ATExecutors.init();
			ATAdapterRegistry.init();
			ATMongoTableRegistry.init();
			ATMorningstarCodes.init(config);

			long timeout = config.getLong("gatherer.timeout");
			ATExecutors.schedule("timeout", new Runnable() {

				@Override
				public void run() {
					log.warn(String.format("Timeout. Exiting..."));
					System.exit(-1);
				}
			}, timeout);

			_instance = new ATGatherer(args);
			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					_instance.shutdown();
				}
			});

			boolean isTestMode = args.contains(TEST);
			SymbolsProvider symbolsProvider = isTestMode
					? new SelectSymbolsProvider("TSLA")
					: new BatchSymbolsProvider();

			_instance.batch(symbolsProvider);
			long duration = System.currentTimeMillis() - start;
			ATSysLogger.info(String.format("Finished in [%s]", DurationFormatUtils.formatDurationHMS(duration)));
			System.exit(0);
		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATGatherer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATGatherer(List<String> args_) {
		this._args = args_;
		this.init();
	}

	private void init() {
		_gson = new Gson();
		_foundationTable = ATMongoTableRegistry.getFoundationTable();

		_dao = new ATMongoDao();
		_ctrl = new ATGathererController();
		_symbolsManager = new ATSymbolManager(false);
		_symbolsManager.initService();
		if (this._args.contains("all") || this._args.contains(ASSETS) || this._args.contains(TRADES)) {
			_portfolioManager = new ATPortfolioManager(false);
			_portfolioManager.initService();
		} else {
			log.info(String.format("init Skipping portfolioManager"));
		}
		if (this._args.contains("all") || this._args.contains(ASSETS)) {
			_mktManager = new ATMarketDataManager(false);
			_mktManager.initService();
		} else {
			log.info(String.format("init Skipping mktManager"));
		}

		_isActive = ATConfigUtil.config().getBoolean("gatherer.isActive", true);// _atConfig.getBoolean("isActive",
																				// true);

		_completedList = new ArrayList<>();
		_statusMap.put(COMPLETED, String.join(", ", _completedList));

	}

	public void batch(SymbolsProvider symPrv_) {
		symPrv_.init(this._symbolsManager, this._portfolioManager);
		_symbolsProvider = symPrv_;
		gather(this._args);
	}

	private void gather(List<String> args_) {
		boolean isOnce = args_.contains("once") ? true : false;
		boolean isForceRefresh = args_.contains("forceRefresh") ? true : false;
		if (isOnce) {
			log.info(String.format("Running once only [%b]", isOnce));
		}

		boolean isTestMode = args_.contains(TEST);
		if (isTestMode) {
			log.info(String.format("Running in TEST mode"));
		}
		Set<ATSymbol> symbols = _symbolsProvider.getSymbols();

		if (!_isActive)
			return;

		// while (_isActive) {
		try {
			// if (args_.contains("treasury") || args_.contains("all")) {
			// start = System.currentTimeMillis();
			// processTreasuryRates();
			// duration = (System.currentTimeMillis() - start) / 1000.0;
			// log.info(String.format("Finished %-10s in %6.2fs", "Treasury Rates",
			// duration));
			// }
			if (args_.contains(TRADES) || args_.contains("all") || isTestMode) {
				start(TRADES);
				processTrades(); // NODE: Use actual symbols
				complete(TRADES);
			}
			if (args_.contains(SYMBOLS) || args_.contains("all") || isTestMode) {
				start(SYMBOLS);
				processSymbols(symbols); // NODE: Use actual symbols
				complete(SYMBOLS);
			}
			if (args_.contains(EARNINGS) || args_.contains("all") || isTestMode) {
				start(EARNINGS);
				processEarnings(new TreeSet<>(symbols));
				complete(EARNINGS);
			}
			if (args_.contains(DIVIDENDS) || args_.contains("all") || isTestMode) {
				start(DIVIDENDS);
				processDividends(new TreeSet<>(symbols));
				complete(DIVIDENDS);
			}
			if (args_.contains(DIVIDEND_EST) || args_.contains("all") || isTestMode) {
				start(DIVIDEND_EST);
				processDividendPredictions(new TreeSet<>(symbols));
				complete(DIVIDEND_EST);
			}
			if (args_.contains(ECONOMIC) || args_.contains("all")) {
				start(ECONOMIC);
				processEconomicEvents();
				complete(ECONOMIC);
			}
			if (args_.contains(SP500) || args_.contains("all")) {
				start(SP500);
				processSP500();
				complete(SP500);
			}
			if (args_.contains(HISTORY) || args_.contains("all") || isTestMode) {
				start(HISTORY);
				processHistory(new TreeSet<>(symbols), isForceRefresh);
				complete(HISTORY);
			}
			if (args_.contains(HISTORY_SHORT_TERM) || args_.contains("all") || isTestMode) {
				start(HISTORY_SHORT_TERM);
				processShortTermHistory(new TreeSet<>(symbols), isForceRefresh);
				complete(HISTORY_SHORT_TERM);
			}
			if (args_.contains(IV) || args_.contains("all")) {
				start(IV);
				processIV();
				complete(IV);
			}
			if (args_.contains(FUNDAMENTALS_1) || args_.contains("all") || isTestMode) {
				start(FUNDAMENTALS_1);
				processFundamentals1(new TreeSet<>(symbols), isForceRefresh);
				complete(FUNDAMENTALS_1);
			}
			if (args_.contains(FUNDAMENTALS_2) || args_.contains("all") || isTestMode) {
				start(FUNDAMENTALS_2);
				processFundamentals2(new TreeSet<>(symbols));
				complete(FUNDAMENTALS_2);
			}
			if (args_.contains(STATISTICS) || args_.contains("all") || isTestMode) {
				start(STATISTICS);
				processStatistics(new TreeSet<>(symbols));
				complete(STATISTICS);
			}
			if (args_.contains(FOUNDATION) || args_.contains("all") || isTestMode) {
				start(FOUNDATION);
				processFoundation(new TreeSet<>(symbols), isForceRefresh);
				complete(FOUNDATION);
			}
			if (args_.contains(ASSETS) || args_.contains("all") || isTestMode) {
				start(ASSETS);
				processAssets();
				complete(ASSETS);
			}
			// if (args_.contains(OPTIONS) || args_.contains("all") || isTestMode) {
			// start(OPTIONS);
			// processOptionChains(new TreeSet<>(symbols));
			// complete(OPTIONS);
			// }
			if (args_.contains(OPTION_EXPIRATIONS) || args_.contains("all") || isTestMode) {
				start(OPTION_EXPIRATIONS);
				processOptionExpirations(new TreeSet<>(symbols));
				complete(OPTION_EXPIRATIONS);
			}
			if (args_.contains(EMAIL)) {
				start(EMAIL);
				emailStatus();
				complete(EMAIL);
			}
			if (args_.contains(TEST_SPECIAL)) {
				start(TEST_SPECIAL);
				processTest();
				complete(TEST_SPECIAL);
			}
			if (isOnce) {
				log.info(String.format("Run once [%b] -> exiting", isOnce));
				return;
			}
			long period = ATConfigUtil.config().getLong("gatherer.period", 86400000L); // 24h
			Thread.sleep(period);
			_isActive = false;// _atConfig.getBoolean("isActive", false);
		} catch (Exception ex) {
			log.error("Error while gathering", ex);
		}
		// }
		// log.info(String.format("active [%b]", _isActive));
		log.info(String.format("Result: %s", _statusMap));
	}

	private void start(String processId_) {
		log.info(String.format("Starting '%S'", processId_));
		_statusMap.put(CURRENT, processId_);
		long start = System.currentTimeMillis();
		_startTimeMap.put(processId_, start);
	}

	private void complete(String processId_) {
		long start = _startTimeMap.get(processId_);
		double duration = (System.currentTimeMillis() - start) / 1000.0;
		log.info(String.format("Finished '%-10S' in %6.2fs", processId_, duration));
		_completedList.add(String.format("%-16s%.0f", processId_, duration));
	}

	private void processTest() {
		TreeMap<String, ATPersistentFundamentals> map = getFundamentals();
		log.info(String.format("test: %d", map != null ? map.size() : null));
	}

	private void processIV() {
		try {
			log.error(String.format("##################################"));
			log.error(String.format("##                             ###"));
			log.error(String.format("##   IV Service Disabled !!!   ###"));
			log.error(String.format("##                             ###"));
			log.error(String.format("##################################"));
			if (true)
				return;
			String historyTableId = ATMongoTableRegistry.getHistoryTable();
			int dateOffset = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY ? -3 : -1;
			String dateStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addDays(new Date(), dateOffset));
			IATWhereQuery query = ATPersistenceRegistry.query();
			query.eq(IATAssetConstants.DATE_STR, dateStr);
			List<ATPriceHistory> history = _dao.query(historyTableId, query, ATPriceHistory.class);
			if (history == null || history.isEmpty()) {
				log.info(String.format("No history for [%s]. Aborting IV process", dateStr));
				return;
			}

			Set<String> symbols = history.stream().map(h -> h.getOccId()).collect(Collectors.toSet());
			ATTradingBlockAdapter adapter = new ATTradingBlockAdapter();
			Map<String, Double> ivMap = adapter.getImpliedVolatility(symbols);

			ArrayList<ATPriceHistory> updateList = new ArrayList<>();
			for (ATPriceHistory atPriceHistory : history) {
				Double iv = ivMap.get(atPriceHistory.getOccId());
				if (iv != null) {
					atPriceHistory.setIV(iv);
					updateList.add(atPriceHistory);
				}
			}
			long start = System.currentTimeMillis();
			_dao.update(historyTableId, updateList.toArray(new IATLookupMap[0]));
			long duration = (System.currentTimeMillis() - start) / 1000;
			double rate = updateList.size() / (duration + 1.0);
			log.info(String.format("Updated [%d/%d] IV records [rate: %.2f/s]", updateList.size(), history.size(),
					rate));
			String data = _gson
					.toJson(createMap(IV, "total", history.size(), "updated", updateList.size(), "rate", rate));
			_statusMap.put(IV, data);
		} catch (Exception ex) {
			log.error("Error updating IV", ex);
		}
	}

	private void processTrades() {
		long count = _portfolioManager.expireOptions();
		log.info(String.format("Expired [%d] options", count));
		String data = _gson.toJson(createMap(TRADES, "expired", count));
		_statusMap.put(TRADES, data);
	}

	private void processSymbols(Set<ATSymbol> symbols_) {
		ATTradierSymbolNameAdapter adapter = new ATTradierSymbolNameAdapter();
		Set<ATSymbol> updated = adapter.populateSymbolNames(symbols_);
		_symbolsManager.update(updated, "gat");
		String data = _gson.toJson(createMap(SYMBOLS, "updated", updated.size()));
		_statusMap.put(SYMBOLS, data);
	}

	// private void processOptionChains(Set<IATSymbol> symbols_) {
	// ATTradierOptionChainAdapter adapter = new
	// ATTradierOptionChainAdapter(_config);
	// Map<String, Set<IATOptionDescription>> chains =
	// adapter.getOptionChains(symbols_);
	// _symbolsManager.updateOptions(chains);
	// }

	private void processOptionExpirations(Set<IATSymbol> symbols_) {
		try {
			long count = _ctrl.deleteOptionExpirations(ATPersistenceRegistry.query().empty());
			log.info(String.format("Deleted [%d] option expirations", count));

			List<String> symbols = symbols_.stream().map(s -> s.getId()).collect(Collectors.toList());
			Map<String, IATOptionExpiration> oe = _ctrl.getOptionExpirations(true, symbols.toArray(new String[0]));
			String data = _gson.toJson(createMap(OPTION_EXPIRATIONS, "total", oe.size()));
			_statusMap.put(IV, data);
			log.info(String.format("Updated [%d/%d] option expirations", oe.size(), symbols.size()));
		} catch (Exception ex) {
			log.error("Error updating option expirations", ex);
		}
	}

	private void processFundamentals1(Set<IATSymbol> symbols_, boolean isForceRefresh_) {
		try {
			Set<IATSymbol> symbols = new TreeSet<>(symbols_);
			ATTradierFundamentalsAdapter adapter = new ATTradierFundamentalsAdapter();
			TreeSet<ATPersistentFundamentals> fundamentals = adapter.getFundamentals(symbols);

			if (!isForceRefresh_) {
				Set<ATSymbol> updatedSymbols = adapter.getUpdatedSymbols();
				if (updatedSymbols != null) {
					_symbolsManager.update(updatedSymbols, "gat");
				}
			}
			Configuration config = ATConfigUtil.config();
			String fundamentalsTableId = config.getString("gatherer.table.fundamentals");// _atConfig.getString("fundamentalsTableId");
			ATPersistentFundamentals[] tosave = fundamentals.toArray(new ATPersistentFundamentals[0]);
			int updateCount = _dao.update(fundamentalsTableId, tosave) ? fundamentals.size() : 0;
			log.info(String.format("Fundamentals 1 [updated: %d]", updateCount));
			String data = _gson.toJson(createMap(FUNDAMENTALS_1, "updated", updateCount));
			_statusMap.put(FUNDAMENTALS_1, data);
		} catch (Exception ex) {
			log.error("Error updating Fundamentals 1", ex);
		}
	}

	private void processFundamentals2(Set<IATSymbol> symbols_) {
		try {
			// List<IATSymbol> symbols = Arrays.asList(new ATSymbol("AAPL"));
			Set<IATSymbol> symbols = new TreeSet<>(symbols_);
			ATAmeritradeWebApiAdapter adapter = ATAmeritradeWebApiAdapter.getInstance();
			List<IATLookupMap> fundamentals = adapter.getFundamentals(symbols);
			if (fundamentals == null || fundamentals.isEmpty())
				return;
			fundamentals.forEach(f -> {
				f.put(IATMongo.MONGO_ID, f.getAtId());
			});
			String fundamentals2TableId = ATMongoTableRegistry.getFundamentalAmeritradeTable();
			IATLookupMap[] tosave = fundamentals.toArray(new IATLookupMap[0]);
			int updateCount = _dao.update(fundamentals2TableId, tosave) ? fundamentals.size() : 0;
			log.info(String.format("Fundamentals 2 [updated: %d]", updateCount));
			String data = _gson.toJson(createMap(FUNDAMENTALS_1, "updated", updateCount));
			_statusMap.put(FUNDAMENTALS_2, data);
		} catch (Exception ex) {
			log.error("Error updating Fundamentals 2", ex);
		}
	}

	// NOTE: Bulk method doesn't process individual symbols
	private void processEarnings(Set<IATSymbol> symbols_) {
		try {
			// Get new earnings
			ATStockRoverAdapter earningsAdapter = new ATStockRoverAdapter();
			Map<String, TreeSet<ATEarning>> newEarnings = earningsAdapter.getEarnings();
			if (newEarnings == null || newEarnings.isEmpty()) {
				log.warn("No earnings retrieved");
				return;
			}

			// Get old earnings
			String earningsTableId = ATConfigUtil.config().getString("gatherer.table.earnings");// _atConfig.getString("earningsTableId");
			String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
			IATWhereQuery query = ATPersistenceRegistry.query().gte(IATAssetConstants.EARNINGS_DATE_STR_OLD, today);
			List<ATEarning> oldEarnings = _dao.query(earningsTableId, query, ATEarning.class);
			Set<String> oldEarningsIds = new HashSet<>();
			for (ATEarning e : oldEarnings) {
				oldEarningsIds.add(e.getAtId());
			}

			// Save new earnings
			long start = System.currentTimeMillis();
			int count = 0;
			for (Entry<String, TreeSet<ATEarning>> entry : newEarnings.entrySet()) {
				for (ATEarning earning : entry.getValue()) {
					if (oldEarningsIds.contains(earning.getAtId())) {
						oldEarningsIds.remove(earning.getAtId());
						continue;
					}
					_dao.update(earningsTableId, earning);
					count++;
				}
			}

			// Delete invalid earnings
			if (!oldEarningsIds.isEmpty()) {
				log.debug(String.format("Purging invalid earnings: %s", oldEarningsIds));
				long deleteCount = 0;
				for (String atId : oldEarningsIds) {
					IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, atId);
					deleteCount += _dao.delete(earningsTableId, deleteQuery);
				}
				if (deleteCount != 0) {
					log.info(String.format("Purged [%d] invalid earnings", deleteCount));
				}
			}

			long duration = System.currentTimeMillis() - start;
			double rate = (count * 1000.0) / duration;

			// Delete outdated records
			String purgeDateStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addYears(new Date(), -5));
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().lt(IATAssetConstants.EARNINGS_DATE_STR_OLD,
					purgeDateStr);
			long purgeCount = _dao.delete(earningsTableId, deleteQuery);
			log.info(String.format("Updated [%d] earnings [rate: %.2f/s], purged [%d < %s]", count, rate, purgeCount,
					purgeDateStr));

		} catch (Exception ex) {
			log.error("Error updating earnings", ex);
		}
	}

	// NOTE: Bulk method doesn't process individual symbols
	private void processDividends(Set<IATSymbol> symbols_) {
		try {
			// Get new dividends
			ATBenzingaAdapter dividendAdapter = new ATBenzingaAdapter();
			Map<String, TreeSet<IATDividend>> newDivMap = dividendAdapter.getDividends();
			if (newDivMap == null || newDivMap.isEmpty()) {
				log.warn("No dividends retrieved");
				return;
			}
			// Get old dividends
			Set<String> oldSyms = symbols_.stream().map(s -> s.getId()).collect(Collectors.toSet());
			Map<String, IATDividend> oldDivMap = _ctrl.getDividends(oldSyms, false);
			Set<String> oldDivIds = new HashSet<>();
			for (IATDividend d : oldDivMap.values()) {
				String atid = IATDividend.getATId(d);
				oldDivIds.add(atid);
			}
			int readCount = 0;
			// Save new dividends
			long start = System.currentTimeMillis();
			int newCount = 0;
			List<IATDividend> updateDivs = new ArrayList<>();
			for (Entry<String, TreeSet<IATDividend>> newDivEntry : newDivMap.entrySet()) {
				for (IATDividend newDiv : newDivEntry.getValue()) {
					readCount++;
					String atid = IATDividend.getATId(newDiv);
					if (oldDivIds.contains(atid)) {
						oldDivIds.remove(atid);
						continue;
					}
					updateDivs.add(newDiv);
					newCount++;
				}
			}
			boolean updateSuccess = _ctrl.updateDividends(updateDivs);
			log.debug(String.format("Updated [%d] dividends", updateSuccess ? updateDivs.size() : -1));

			// Delete invalid dividends
			if (!oldDivIds.isEmpty()) {
				log.debug(String.format("Purging invalid dividends: %s", oldDivIds));
				long deleteCount = 0;
				for (String atId : oldDivIds) {
					IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, atId);
					deleteCount += _ctrl.deleteDividends(deleteQuery);
				}
				if (deleteCount != 0) {
					log.info(String.format("Purged [%d] invalid dividends", deleteCount));
				}
			}
			long duration = System.currentTimeMillis() - start;
			double rate = newCount != 0 ? (newCount * 1000.0) / duration : 0;
			// Delete outdated dividends
			String purgeDateStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addYears(new Date(), -5));
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().lt(IATAssetConstants.DIV_EX_DATE_STR_OLD,
					purgeDateStr);
			long deleteCount = _ctrl.deleteDividends(deleteQuery);
			log.info(String.format("Dividends [get : %d; new: %d; rate: %.2f/s; purged(<%s): %d]", readCount, newCount,
					rate, purgeDateStr,
					deleteCount));

		} catch (Exception ex) {
			log.error("Error updating dividends", ex);
		}
	}

	private void processDividendPredictions(Set<IATSymbol> symbols_) {
		String dividendTableId = ATConfigUtil.config().getString("gatherer.table.dividends");

		IATWhereQuery delQuery2 = ATPersistenceRegistry.query().notNull(IATAssetConstants.DIV_EST_DATE_STR);
		long delCount2 = _ctrl.deleteDividends(delQuery2);
		log.info(String.format("Purged [%d] estimated dividends", delCount2));

		String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		IATWhereQuery query = ATPersistenceRegistry.query().gte(IATAssetConstants.DIV_EX_DATE_STR_OLD, today);
		TreeMap<String, DividendBridgeMap> existingDivMap = _dao.queryEdge(dividendTableId, query,
				IATAssetConstants.OCCID,
				IATAssetConstants.DIV_EX_DATE_STR_OLD, -1, Arrays.asList(IATAssetConstants.DIV_EX_DATE_STR_OLD),
				DividendBridgeMap.class);
		Set<String> skipIds = existingDivMap.keySet();
		Set<String> workIds = symbols_.stream().map(s -> s.getId()).filter(s -> !skipIds.contains(s))
				.collect(Collectors.toCollection(TreeSet::new));

		log.info(String.format("Retrieving [%d] estimated dividends", workIds.size()));
		Map<String, IATDividend> currentDivMap = _ctrl.getDividends(workIds, true);
		log.info(String.format("Processed [%d/%d] estimated dividends", currentDivMap.size(), symbols_.size()));
	}

	private void processEconomicEvents() {
		try {
			ATTradingEconomicsAdapter adapter = new ATTradingEconomicsAdapter();
			Set<ATPersistentEconomicEvent> events = adapter.getEconomicEvents();
			if (events == null || events.isEmpty()) {
				log.warn("No Economic Events retrieved");
				return;
			}
			String eventTableId = ATConfigUtil.config().getString("gatherer.table.econ_events");
			ATPersistentEconomicEvent[] tosave = events.toArray(new ATPersistentEconomicEvent[0]);
			int updateCount = _dao.update(eventTableId, tosave) ? events.size() : 0;
			log.info(String.format("Economic Events [updated: %d]", updateCount));
		} catch (Exception ex) {
			log.error("Error updating Economic Events", ex);
		}
	}

	private void processSP500() {
		try {
			Map<String, ATSymbol> oldSymbols = _symbolsManager.getSymbols();
			oldSymbols = oldSymbols.entrySet().stream()
					.filter(e_ -> (e_.getValue().getReferences() != null
							&& e_.getValue().getReferences().contains(IATAssetConstants.IDX_SP500)))
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

			ATWikipediaAdapter sp500Adapter = new ATWikipediaAdapter();
			Set<ATSymbol> sp500 = sp500Adapter.updateSymbols(oldSymbols);
			if (sp500 == null || sp500.isEmpty()) {
				log.info("No SP500 changes");
				return;
			}
			_symbolsManager.update(sp500, IATAssetConstants.IDX_SP500);
			log.info(String.format("Processed [%d] S&P 500 symbols", sp500.size()));
		} catch (Exception ex) {
			log.error("Error updating SP500", ex);
		}
	}

	private void processHistory(Set<IATSymbol> symbols_, boolean isForceRefresh_) {
		String historyTableId = ATMongoTableRegistry.getHistoryTable();

		int dayNum = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		if (!isForceRefresh_ && _skipWeekend && (dayNum == Calendar.SUNDAY || dayNum == Calendar.MONDAY)) {
			log.info(String.format("Skipping %s on weekends", "processHistory"));
			return;
		}

		TreeMap<IATSymbol, String> workMap = new TreeMap<>();
		Map<String, IATSymbol> newSymbolMap = symbols_.stream().collect(Collectors.toMap(x -> x.getId(), x -> x));
		Set<IATSymbol> logSyms = new TreeSet<>(); // Note: Just for logging
		if (!isForceRefresh_) {
			TreeMap<String, ATPriceHistory> oldSymbolMap = _dao.groupMax(historyTableId, IATAssetConstants.OCCID,
					IATAssetConstants.DATE_STR,
					ATPriceHistory.class);
			String nowStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addDays(new Date(), -1)); // Previous day
			String smallStr = "99999999";
			Iterator<Entry<String, ATPriceHistory>> i = oldSymbolMap.entrySet().iterator();
			while (i.hasNext()) {
				Entry<String, ATPriceHistory> e = i.next();
				IATPriceHistory h = e.getValue();
				String occId = h.getOccId();
				IATSymbol newSymbol = newSymbolMap.get(occId);
				if (newSymbol == null)
					continue;
				String ds8 = h.getDateStr();
				int days = nowStr.compareTo(ds8);
				if (days <= 0) {
					// Exists, or Sunday or Monday early morning
					newSymbolMap.remove(occId);
					continue;
				}
				workMap.put(newSymbol, ds8);
				if (smallStr.compareTo(ds8) > 0) {
					smallStr = ds8;
				}
			}

			log.info(String.format("Updating history [%s - %s / %d]", smallStr, nowStr, workMap.size()));
			for (Entry<String, IATSymbol> entry : newSymbolMap.entrySet()) {
				IATSymbol newSymbol = entry.getValue();
				if (!oldSymbolMap.containsKey(newSymbol.getId())) {
					workMap.put(newSymbol, null);
					logSyms.add(newSymbol);
				}
			}
		} else {
			Date startDate = DateUtils.addYears(new Date(), -10);
			String dstr = ATFormats.DATE_yyyyMMdd.get().format(startDate);
			for (IATSymbol symbol : symbols_) {
				workMap.put(symbol, dstr);
				logSyms.add(symbol);
			}
		}
		if (!logSyms.isEmpty()) {
			log.info(String.format("Adding new history %s", logSyms));
		}
		if (workMap.isEmpty()) {
			log.info(String.format("No history to update"));
			return;
		}

		Set<String> processSymbols = workMap.keySet().stream().map(s -> s.getId()).collect(Collectors.toSet());
		Date minDate = DateUtils.addMonths(new Date(), -13); // -13

		ATAmeritradeWebApiAdapter stockAdapter = ATAmeritradeWebApiAdapter.getInstance();
		TreeSet<ATPriceHistory> history = stockAdapter.getHistory(workMap, minDate);

		IATHistoryGatherer indexAdapter = new ATTradierHistoryAdapter();
		TreeSet<ATPriceHistory> indexHistory = indexAdapter.getHistory(workMap, minDate);
		history.addAll(indexHistory);

		long start = System.currentTimeMillis();
		int count = 0;
		for (ATPriceHistory data : history) {
			_dao.update(historyTableId, data);
			processSymbols.remove(data.getOccId());
			count++;
		}
		long duration = System.currentTimeMillis() - start;
		double rate = (count * 1000.0) / duration;
		if (!processSymbols.isEmpty()) {
			log.info(String.format("Failed to update history %s", processSymbols));
		}
		log.info(String.format("History [count: %d; insert rate: %.2f/s]", count, rate));
	}

	private void processShortTermHistory(Set<IATSymbol> symbols_, boolean isForceRefresh_) {
		int dayNum = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
		if (!isForceRefresh_ && _skipWeekend && (dayNum == Calendar.SUNDAY || dayNum == Calendar.MONDAY)) {
			log.info(String.format("Skipping %s on weekends", "processShortTermHistory"));
			return;
		}

		Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		Map<String, IATSymbol> newSymbolMap = symbols_.stream().collect(Collectors.toMap(x -> x.getId(), x -> x));

		String stHistoryTableId = ATConfigUtil.config().getString("gatherer.table.history.st");// _atConfig.getString("stHistoryTableId");
		Set<IATSymbol> logSyms = new TreeSet<>();

		TreeMap<IATSymbol, String> workMap = new TreeMap<>();
		if (!isForceRefresh_) {
			TreeMap<String, ATPriceHistory> oldSymbolMap = _dao.groupMax(stHistoryTableId, IATAssetConstants.OCCID,
					IATAssetConstants.DATE_STR,
					ATPriceHistory.class);
			String smallStr = "999999999999";
			Iterator<Entry<String, ATPriceHistory>> i = oldSymbolMap.entrySet().iterator();
			while (i.hasNext()) {
				Entry<String, ATPriceHistory> e = i.next();
				IATPriceHistory h = e.getValue();
				String occId = h.getOccId();
				IATSymbol symbol = newSymbolMap.get(occId);
				if (symbol == null)
					continue;
				String dStr = h.getDateStr();
				workMap.put(symbol, dStr);
				if (smallStr.compareTo(dStr) > 0) {
					smallStr = dStr;
				}
			}
			log.info(String.format("Updating short-term history [%s / %d]", smallStr, workMap.size()));
			for (Entry<String, IATSymbol> entry : newSymbolMap.entrySet()) {
				IATSymbol newSym = entry.getValue();
				if (!oldSymbolMap.containsKey(newSym.getId())) {
					workMap.put(newSym, null);
					logSyms.add(newSym);
				}
			}
		} else {
			for (IATSymbol symbol : symbols_) {
				workMap.put(symbol, null);
				logSyms.add(symbol);
			}
		}

		if (!logSyms.isEmpty()) {
			log.info(String.format("Adding new short-term history %s", logSyms));
		}

		Set<String> processSymbols = workMap.keySet().stream().map(s -> s.getId()).collect(Collectors.toSet());
		Date minDate = DateUtils.addDays(now, -14);
		ATTradierTimeSalesAdapter adapter = new ATTradierTimeSalesAdapter();
		Map<String, TreeSet<ATPriceHistory>> history = adapter.getHistory(workMap, minDate);
		long start = System.currentTimeMillis();
		int count = 0;

		for (String symbol : history.keySet()) {
			log.debug(String.format("Saving %s [%s]", stHistoryTableId, symbol));
			TreeSet<ATPriceHistory> hist = history.get(symbol);
			for (ATPriceHistory ph : hist) {
				_dao.update(stHistoryTableId, ph);
				processSymbols.remove(ph.getOccId());
				count++;
			}
		}
		long duration = System.currentTimeMillis() - start;
		double rate = (count * 1000.0) / duration;
		if (!processSymbols.isEmpty()) {
			log.info(String.format("Failed to update history %s", processSymbols));
		}

		// Purge outdated
		String purgeDateStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addDays(now, -14));
		IATWhereQuery deleteQuery = ATPersistenceRegistry.query().lt(IATAssetConstants.DATE_STR, purgeDateStr);
		long deleteCount = _dao.delete(stHistoryTableId, deleteQuery);
		log.info(String.format("Short-term History [count: %d; insert rate: %.2f/s; purged: %d < %s]", count, rate,
				deleteCount, purgeDateStr));

	}

	private void processStatistics(Set<IATSymbol> symbols_) {
		long start = System.currentTimeMillis();
		int rateCount = _ctrl.processStatistics(symbols_);
		long duration = System.currentTimeMillis() - start;
		double rate = (rateCount * 1000.0) / duration;
		log.info(String.format("Updated [%d] statistic records [rate: %.2f/s]", rateCount, rate));
	}

	private void processAssets() {
		IATWhereQuery tradeQuery = ATPersistenceRegistry.query()
				.notNull(IATAssetConstants.SIZE_TRADE).isNull(IATAssetConstants.EXPIRATION_DATE_STR);
		List<IATTrade> trades = _portfolioManager.getTrades(tradeQuery);
		if (trades == null || trades.isEmpty())
			return;
		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;
		final DateTimeFormatter yyyy_mm_ddFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		trades.sort((a, b) -> a.getTsTrade().compareTo(b.getTsTrade()));
		Map<String, ATUserAsset> assetMap = new HashMap<>();
		Map<String, Double> sizeMap = new HashMap<>();
		for (IATTrade trade : trades) {
			String sizeKey = trade.getOccId() + trade.getTradeUserId();
			Double oldSz = sizeMap.get(sizeKey);
			Double newSz = oldSz != null ? oldSz + trade.getSzTrade() : trade.getSzTrade();
			sizeMap.put(sizeKey, newSz);

			LocalDate assetDate = new java.sql.Date(trade.getTsTrade().getTime()).toLocalDate()
					.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
			String assetDateStr = yyyy_mm_ddFormatter.format(assetDate);
			String assetKey = String.format("%s:%s:%s", trade.getTradeUserId(), assetDateStr, trade.getOccId());
			ATUserAsset asset = new ATUserAsset(trade.getTradeUserId(), assetDateStr, trade.getOccId(), newSz, null);
			assetMap.put(assetKey, asset);
		}
		Collection<ATUserAsset> assets = assetMap.values();
		try {
			Map<String, List<ATUserAsset>> symbolMap = assets.stream()
					.collect(Collectors.groupingBy(ATUserAsset::getOccId));
			LocalDate now = LocalDate.now();
			LocalDate nextSaturday = now.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
			LocalDate lastSaturday = nextSaturday.minusWeeks(1);
			Map<String, ATUserAsset> processedMap = new HashMap<>();
			for (String symbol : symbolMap.keySet()) {
				List<ATUserAsset> symbolAssets = symbolMap.get(symbol);
				symbolAssets.sort((a, b) -> a.getDateStr().compareTo(b.getDateStr()));
				String firstDateStr = symbolAssets.get(0).getDateStr().replace("-", "");
				IATWhereQuery query = ATPersistenceRegistry.query()
						.eq(IATAssetConstants.OCCID, symbol).gte(IATAssetConstants.DATE_STR, firstDateStr);
				List<String> flags = Arrays.asList("ohlc", "avg", "hv", "minmax");
				Map<String, List<ATHistoryData>> historyMap = _mktManager.getHistory(query, flags, avgMode, hvMode);
				Map<LocalDate, Double> dateCloseMap = historyMap.values().stream().filter(hl -> !hl.isEmpty())
						.flatMap(hl -> hl.stream().filter(h -> h.getClose() != null))
						.collect(Collectors.toMap(
								h -> LocalDate.parse(h.getDateStr(), yyyy_mm_ddFormatter),
								v -> v.getClose()));
				Map<String, List<ATUserAsset>> symbolUserMap = symbolAssets.stream()
						.collect(Collectors.groupingBy(ATUserAsset::getUserId));
				for (String user : symbolUserMap.keySet()) {
					List<ATUserAsset> userAssets = symbolUserMap.get(user);
					TreeMap<LocalDate, ATUserAsset> dateAssetMap = new TreeMap<>();
					for (ATUserAsset asset : userAssets) {
						LocalDate d = LocalDate.parse(asset.getDateStr(), yyyy_mm_ddFormatter);
						ATUserAsset oldAsset = dateAssetMap.get(d);
						if (oldAsset == null) {
							dateAssetMap.put(d, asset);
						} else {
							ATUserAsset newAsset = new ATUserAsset(asset);
							newAsset.setSize(asset.getSize() + oldAsset.getSize());
							dateAssetMap.put(d, newAsset);
						}
					}
					LocalDate currentDate = dateAssetMap.firstKey();
					ATUserAsset currentAsset = dateAssetMap.get(currentDate);
					Double currentClose = dateCloseMap.get(currentDate);
					do {
						if (dateCloseMap.containsKey(currentDate)) {
							currentClose = dateCloseMap.get(currentDate);
						} else if (currentDate.isAfter(lastSaturday)) {
							IATMarketData mkt = _mktManager.getMarketData(currentAsset.getOccId());
							if (mkt != null) {
								currentClose = mkt.getPxClose();
							}
						}
						if (dateAssetMap.containsKey(currentDate)) {
							currentAsset = dateAssetMap.get(currentDate);
						}
						Double currentSz = ATMathUtil.round(currentAsset.getSize(), 4);
						if (currentClose != null && currentSz != 0) {
							// BigDecimal bdValue = new BigDecimal(currentClose * currentSz);
							Double value = ATMathUtil.round(currentClose * currentSz, 2);// bdValue.setScale(2,
																							// RoundingMode.HALF_DOWN).doubleValue();
							String dateStr = yyyy_mm_ddFormatter.format(currentDate);
							currentAsset = new ATUserAsset(user, dateStr, symbol, currentSz, value);
							String mapKey = String.format("%s:%s:%s", user, symbol, dateStr);
							processedMap.put(mapKey, currentAsset);
						} else {
							log.debug(String.format("Skipping [%s]: %s / %.2f / %.2f", symbol, currentDate, currentSz,
									currentClose));
						}
						currentDate = currentDate.plusWeeks(1);
					} while (currentDate.isBefore(nextSaturday));
				}
			}
			Collection<ATUserAsset> processedAssets = processedMap.values();
			_portfolioManager.saveAssets(processedAssets);
		} catch (Exception ex) {
			log.error(String.format("Error processing assets"), ex);
		}
	}

	private void processFoundation(Set<IATSymbol> symbols_, boolean isForceRefresh_) {
		long start = System.currentTimeMillis();

		Map<String, ATPersistentEquityFoundation> efMap = new HashMap<>();
		if (!isForceRefresh_) {
			// 1. Get SP500 symbols and descriptions
			List<ATPersistentEquityDescription> sp500 = getSP500();
			for (ATPersistentEquityDescription e : sp500) {
				e.put(IATAssetConstants.MODEL, IATSymbol.EQUITY);
				ATPersistentEquityFoundation f = new ATPersistentEquityFoundation(e);
				f.put(ATPersistentEquityFoundation.OCCID, e.getOccId());
				efMap.put(f.getOccId(), f);
			}
		}
		for (IATSymbol symbol : symbols_) {
			// String occId = symbol.getId();
			if (symbol.getId().length() > 10)
				continue;
			// String sym = occId.length() < 6 ? occId : occId.substring(0, 6).trim();
			// if (efMap.containsKey(sym))
			// continue;
			ATPersistentEquityFoundation f = new ATPersistentEquityFoundation(symbol);
			efMap.put(f.getOccId(), f);
		}

		// // 2a. Get extra symbols
		// ATPersistentSymbols symbols = getSymbols();
		// Set<String> watchlist = symbols.getWatchlist();
		// for (String e : watchlist) {
		// if (efMap.containsKey(e))
		// continue;
		// ATPersistentEquityFoundation f = new ATPersistentEquityFoundation(e);
		// efMap.put(f.getOccId(), f);
		// }
		// // 2b. Get positions
		// Set<String> positions = symbols.getPositions();
		// for (String p : positions) {
		// String sym = p.length() < 6 ? p : p.substring(0, 6).trim();
		// if (efMap.containsKey(sym))
		// continue;
		// ATPersistentEquityFoundation f = new ATPersistentEquityFoundation(sym);
		// efMap.put(f.getOccId(), f);
		// }
		// // 2c. TODO: Get exclude symbols

		Set<String> atSyms = isForceRefresh_ ? symbols_.stream().map(s -> s.getId()).collect(Collectors.toSet()) : null;
		// 3a. Get earnings
		Map<String, ATEarning> earnings = getNextEarnings(atSyms);
		// 3b.
		Map<String, IATDividend> dividends = _ctrl.getDividends(atSyms, false);// getNextDividends();
		// 4. Get statistics
		Map<String, ATPriceHistory> statistics = _ctrl.getPriceHistory();

		Map<String, ATPersistentFundamentals> fundamentals = getFundamentals();

		// 5. Merge
		for (String occId : efMap.keySet()) {
			ATPersistentEquityFoundation f = efMap.get(occId);
			ATEarning e = earnings.get(occId);
			merge(f, e);
			f.remove(IATAssetConstants.DIV_FREQUENCY);
			f.remove(IATAssetConstants.DIV_EST_DATE_STR);
			f.remove(IATAssetConstants.DIV_EX_DATE_STR);
			f.remove(IATAssetConstants.DIV_PAY_DATE_STR);
			f.remove(IATAssetConstants.DIV_REC_DATE_STR);
			f.remove(IATAssetConstants.DIVIDEND);
			IATDividend d = dividends.get(occId);
			if (d != null) {
				merge(f, d.getAsMap());
			}
			ATPriceHistory s = statistics.get(occId);
			this.populateStatistics(f, s);
			merge(f, s);

			ATPersistentFundamentals fund = fundamentals != null ? fundamentals.get(occId) : null;
			if (fund != null) {
				Double mcap = fund.getMarketCap();
				if (mcap != null) {
					f.put(IATAssetConstants.MARKET_CAP, mcap);
				}
			}
		}
		int updateCount = 0;
		if (!isForceRefresh_) {
			String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
			IATWhereQuery earningsQuery = ATPersistenceRegistry.query()
					.lt(ATPersistentEquityFoundation.EARNINGS_DATE_STR_OLD, today);
			List<ATPersistentEquityFoundation> expiredEarnings = _dao.query(_foundationTable, earningsQuery,
					ATPersistentEquityFoundation.class);

			long duration = System.currentTimeMillis() - start;
			log.info(String.format("Collected [%d] foundation records in [%.2fs]", efMap.size(), duration / 1000.0));
			start = System.currentTimeMillis();

			for (ATPersistentEquityFoundation e : expiredEarnings) {
				e.put(ATPersistentEquityFoundation.EARNINGS_DATE_STR_OLD, null);
				if (_dao.update(_foundationTable, e)) {
					updateCount++;
				}
			}
		}

		Long minUpdateTime = null;
		for (ATPersistentEquityFoundation data : efMap.values()) {
			data.setAtId(data.getOccId());
			data.remove("_id");
			_dao.update(_foundationTable, data);
			updateCount++;
			if (minUpdateTime == null) {
				minUpdateTime = data.getUpdateTime();
			}
		}
		if (!isForceRefresh_) {
			IATWhereQuery deleteQuery = ATPersistenceRegistry.query().lt(ATPersistentEquityFoundation.UPDATE_TIME,
					minUpdateTime);
			long deleteCount = _dao.delete(_foundationTable, deleteQuery);
			long duration = System.currentTimeMillis() - start;
			double rate = (updateCount * 1000.0) / duration;
			log.info(String.format("Updated   [%d] foundation records, deleted [%d] - [rate: %.2f/s]", updateCount,
					deleteCount, rate));
		} else {
			long duration = System.currentTimeMillis() - start;
			double rate = (updateCount * 1000.0) / duration;
			log.info(String.format("Updated   [%d] foundation records", updateCount, rate));
		}
	}

	private void populateStatistics(ATPersistentEquityFoundation fnd_, ATPriceHistory stat_) {
		if (fnd_ == null || stat_ == null)
			return;
		Map<String, Double> avgMap = new TreeMap<>();
		for (String key : IATStatistics.AVGS) {
			Double value = stat_.getAvg(key);
			if (value != null) {
				avgMap.put(key, value);
			}
		}
		fnd_.setAvgMap(avgMap);

		Map<String, Double> hvMap = new TreeMap<>();
		for (String key : IATStatistics.HVS) {
			Double value = stat_.getHV(key);
			if (value != null) {
				hvMap.put(key, value);
			}
		}
		fnd_.setHvMap(hvMap);

		Map<String, Double> maxMap = new TreeMap<>();
		for (String key : IATStatistics.MAXS) {
			Double value = stat_.getMax(key);
			if (value != null) {
				maxMap.put(key, value);
			}
		}
		fnd_.setMaxMap(maxMap);

		Map<String, Double> minMap = new TreeMap<>();
		for (String key : IATStatistics.MINS) {
			Double value = stat_.getMin(key);
			if (value != null) {
				minMap.put(key, value);
			}
		}
		fnd_.setMinMap(minMap);
	}

	private void merge(Map<String, Object> target_, Map<String, Object> source_) {
		if (target_ == null || source_ == null)
			return;
		Map<String, Object> temp = new HashMap<>(source_);
		temp.remove("_id");
		temp.remove(ATPersistentAssetBase.AT_ID);
		temp.remove(ATPersistentAssetBase.INSERT_TIME);
		temp.remove(ATPersistentAssetBase.UPDATE_TIME);
		temp.remove(IATAssetConstants.OCCID);
		temp.remove(IATAssetConstants.SOURCE);
		// temp.remove(IATAssetConstants.)
		target_.putAll(temp);
	}

	@Deprecated // NOTE: Should be contained in symbols
	private List<ATPersistentEquityDescription> getSP500() {
		String sp500TableId = ATConfigUtil.config().getString("gatherer.table.sp500");// _atConfig.getString("sp500TableId");
		IATWhereQuery query = ATPersistenceRegistry.query();
		List<ATPersistentEquityDescription> sp500 = _dao.query(sp500TableId, query,
				ATPersistentEquityDescription.class);
		return sp500;
	}

	private Map<String, ATEarning> getNextEarnings(Collection<String> symbols_) {
		Collection<String> symbols = symbols_ != null ? symbols_ : new ArrayList<>();
		String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		String earningsTableId = ATConfigUtil.config().getString("gatherer.table.earnings");// _atConfig.getString("earningsTableId");
		IATWhereQuery query = ATPersistenceRegistry.query().gte(IATAssetConstants.EARNINGS_DATE_STR_OLD, today);
		query = symbols.isEmpty() ? query : query.and().in(IATAssetConstants.OCCID, symbols);
		List<ATEarning> earnings = _dao.query(earningsTableId, query, ATEarning.class);
		Map<String, ATEarning> result = new HashMap<>();
		for (ATEarning e : earnings) {
			ATEarning o = result.get(e.getOccId());
			if (o == null) {
				result.put(e.getOccId(), e);
			} else if (o.getEarningsDateStr().compareTo(e.getEarningsDateStr()) > 0) {
				result.put(e.getOccId(), e);
			}
		}
		return result;
	}
	private TreeMap<String, ATPersistentFundamentals> getFundamentals() {
		try {
			String fundamentalsTableId = ATConfigUtil.config().getString("gatherer.table.fundamentals");// _atConfig.getString("fundamentalsTableId");
			TreeMap<String, ATPersistentFundamentals> fundamentalsMap = _dao.groupMax(fundamentalsTableId,
					IATAssetConstants.OCCID,
					IATAssetConstants.DATE_STR,
					ATPersistentFundamentals.class);
			return fundamentalsMap;
		} catch (Exception ex) {
			log.error(String.format("Error processing %s", FUNDAMENTALS_1), ex);
			return null;
		}
	}

	private void shutdown() {
		try {
			emailStatus();
			log.info("Shutting down...");
			_dao.close();
		} catch (Exception ex) {
			log.error("Error shutting down", ex);
		}
	}

	private static String zipB64(String path_) {
		try {
			File file = new File(path_);
			if (!file.exists())
				return null;
			byte[] buffer = new byte[1024];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try (ZipOutputStream zos = new ZipOutputStream(baos)) {
				try (FileInputStream fis = new FileInputStream(file)) {
					zos.putNextEntry(new ZipEntry(file.getName()));
					int length;
					while ((length = fis.read(buffer)) > 0) {
						zos.write(buffer, 0, length);
					}
					zos.closeEntry();
				}
			}
			byte[] bytes = baos.toByteArray();
			String encodedBase64 = new String(Base64.getEncoder().encodeToString(bytes));
			return encodedBase64;
		} catch (Exception ex) {
			log.error(String.format("Failed to encode [%s]", path_), ex);
			return null;
		}
	}

	private void emailStatus() {
		Configuration config = ATConfigUtil.config();
		String sender = config.getString("gatherer.email.sender");
		String target = config.getString("gatherer.email.target");
		String logFile = config.getString("gatherer.logfile");
		log.info(String.format("Emailing status [%s -> %s]", sender, target));
		Email from = new Email(sender, "Tradonado Alerts");
		String subject = String.format("Tradonado Gatherer Status %s",
				ATFormats.DATE_yyyy_MM_dd.get().format(new Date()));
		Email to = new Email(target);
		StringBuilder sb = new StringBuilder();
		sb.append(String.format("Attached is the latest Tradonado Gatherer log [%s]",
				ATFormats.DATE_TIME_mid.get().format(new Date())));
		sb.append("\n\n");
		sb.append("Status:");
		sb.append('\n');
		sb.append(String.format("%s", _statusMap));
		sb.append("\n\n");
		sb.append("Regards,");
		sb.append('\n');
		sb.append("Tradonado Support");
		sb.append('\n');
		String msg = sb.toString();
		Content content = new Content("text/plain", msg);
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid(config.getString("sendgrid.key"));
		Request request = new Request();
		try {
			String b64 = zipB64(logFile);
			if (b64 != null) {
				Attachments attachments = new Attachments();
				attachments.setDisposition("attachment");
				attachments.setContent(b64);
				attachments.setType("application/zip");
				attachments.setFilename("gatherer.zip");
				attachments.setContentId("gatherer.zip");
				mail.addAttachments(attachments);
			}
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.buildPretty());
			Response response = sg.api(request);
			log.info(String.format("Email complete [%d]: %s", response.getStatusCode(), response.getBody()));
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static public class ATFixedDoubleArray {

		private int _size;
		private int _idx;
		private double[] _array;

		public ATFixedDoubleArray(int size_) {
			_size = size_;
			_array = new double[_size];
			_idx = 0;
		}

		public void add(double d_) {
			_array[_idx++] = d_;
			if (_idx >= _size)
				_idx = 0;
		}

		public double[] get() {
			return _array;
		}
	}

	static private Map<Object, Object> createMap(String processId_, Object... content_) {
		Map<Object, Object> result = new TreeMap<>();
		if (content_ == null || (content_.length % 2) != 0) {
			log.error(String.format("Invalid content [%s]", processId_));
			return result;
		}
		for (int i = 0; i < content_.length; i += 2) {
			Object key = content_[i];
			Object value = content_[i + 1] + "";
			result.put(key, value);
		}
		return result;
	}
	public Map<String, String> getMonitoringStatus() {
		return _statusMap;
	}

	public static interface SymbolsProvider {
		Set<ATSymbol> getSymbols();

		void init(ATSymbolManager symbolsManager, ATPortfolioManager portfolioManager);
	}

	public static class SelectSymbolsProvider implements SymbolsProvider {

		Set<String> _symStrings;
		Set<ATSymbol> _symbols;

		public SelectSymbolsProvider(String... symbols_) {
			_symStrings = new HashSet<>(Arrays.asList(symbols_));
		}

		@Override
		public void init(ATSymbolManager symbolsManager_, ATPortfolioManager unused) {
			Map<String, ATSymbol> symbolMap = symbolsManager_.getSymbols();
			Set<ATSymbol> symbols = new TreeSet<>();
			for (String symStr : _symStrings) {
				ATSymbol symbol = symbolMap.get(symStr);
				if (symbol == null) {
					symbol = new ATSymbol(symStr);
				}
				symbols.add(symbol);
			}
			_symbols = Collections.unmodifiableSet(symbols);
		}

		@Override
		public Set<ATSymbol> getSymbols() {
			{
				return _symbols;
			}

		}

	}

	public static class BatchSymbolsProvider implements SymbolsProvider {

		private ATSymbolManager _symbolsManager;
		private ATPortfolioManager _portfolioManager;

		public BatchSymbolsProvider() {
		}

		@Override
		public void init(ATSymbolManager symbolsManager_, ATPortfolioManager portfolioManager_) {
			_symbolsManager = symbolsManager_;
			_portfolioManager = portfolioManager_;
		}

		@Override
		public Set<ATSymbol> getSymbols() {
			Configuration config = ATConfigUtil.config();
			// return new TreeSet<>(Arrays.asList("TSLA", "BIDU", "CGC", "CRSP", "BRZU",
			// "EDU", "EPI", "FMAT", "GES", "GSAT", "GSK", "ICPT",
			// "IEMG",
			// "INDA", "ITA", "IVE", "QTNT", "STMP", "TRLY", "VEU", "VNQ", "WIN", "XLB",
			// "XLE", "XLF", "XLI"));

			Set<ATSymbol> symbols = new TreeSet<>();
			Map<String, ATSymbol> symbolMap = _symbolsManager.getSymbols();
			symbols.addAll(symbolMap.values());

			String symStr = config.getString("gatherer.symbols.include");
			String[] symTokens = symStr != null ? symStr.split(",") : new String[0];
			for (String sym : symTokens) {
				ATSymbol symbol = _symbolsManager.getSymbol(sym.trim(), true);
				if (!symbols.contains(symbol)) {
					symbols.add(symbol);
				}
			}

			Set<String> missingSymbols = new TreeSet<>();
			Collection<String> positions = _portfolioManager != null ? _portfolioManager.getActiveOccIds(null, true)
					: new ArrayList<>();
			for (String position : positions) {
				String id = position.length() > 6 ? position.substring(0, 5).trim() : position;
				ATSymbol symbol = _symbolsManager.getSymbol(id, false);
				if (symbol != null) {
					symbols.add(symbol);
				} else if (!missingSymbols.contains(id)) {
					missingSymbols.add(id);
					log.warn(String.format("Adding missing symbol [%s]", id));
					symbols.add(new ATSymbol(id));
				}
			}
			if (!missingSymbols.isEmpty()) {
				log.info(String.format("Detected missing symbols %s", missingSymbols));
			}

			String excludeStr = config.getString("gatherer.symbols.exclude");
			if (!StringUtils.isBlank(excludeStr)) {
				List<String> excludeList = new ArrayList<>();
				String[] excludeTokens = excludeStr.split(",");
				for (String token : excludeTokens) {
					excludeList.add(token.trim());
				}
				symbols = symbols.stream().filter(s -> !excludeList.contains(s.getId())).collect(Collectors.toSet());
			}

			log.debug(String.format("Relevant symbols [%d]: %s", symbols.size(), symbols));
			return symbols;
		}

	}

	public static class DividendBridgeMap extends ATPersistentBridgeMap {
		private static final long serialVersionUID = 1L;

		public DividendBridgeMap() {
			super(IATMongo.MONGO_ID);
		}
	}

}
