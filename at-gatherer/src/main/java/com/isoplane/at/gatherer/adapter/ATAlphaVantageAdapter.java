package com.isoplane.at.gatherer.adapter;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATHttpHelper;

@Deprecated
public class ATAlphaVantageAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageAdapter.class);

	static final String SOURCE_KEY = "AV";

	private IATConfiguration _config;
	private String _avKey;
	private long _period;
	boolean _isErrorLogged;

	public ATAlphaVantageAdapter(IATConfiguration config_) {
		_config = config_;
	}

	public TreeSet<ATPriceHistory> getHistory(Map<String, String> syms_, Date minDate_) {
		TreeSet<ATPriceHistory> result = new TreeSet<>();
		Map<String, String> processMap = new TreeMap<>(syms_);

		log.info(String.format("Getting history [%s...]", ATFormats.DATE_yyyy_MM_dd.get().format(minDate_)));

		String minDateStr = ATFormats.DATE_yyyyMMdd.get().format(minDate_);
		final int maxRuns = 3;
		for (int i = maxRuns; i > 0; i--) {
			log.debug(String.format("History run [%d/%d]", maxRuns - i, maxRuns));
			TreeSet<ATPriceHistory> history = getHistoryRun(processMap, minDateStr);
			for (ATPriceHistory h : history) {
				result.add(h);
				processMap.remove(h.getOccId());
			}
			if (processMap.isEmpty()) {
				log.debug(String.format("History processing complete"));
				break;
			}
		}
		return result;
	}

	private TreeSet<ATPriceHistory> getHistoryRun(Map<String, String> syms_, String minDateStr_) {
		// return getHistory(syms_, minDateStr_, false);
		_isErrorLogged = false;
		TreeSet<ATPriceHistory> result = new TreeSet<>();
	//	_avKey = _config.getString("avKey");
		_period = _config.getLong("avPeriod", 1250);
		for (Entry<String, String> entry : syms_.entrySet()) {
			String sym = entry.getKey();
			String from = entry.getValue();
			try {
				List<ATPriceHistory> history = requestHistory(sym, from, minDateStr_, 0);
				if (history != null) {
					result.addAll(history);
				}
				Thread.sleep(_period); // 200 cpm ~ 300ms
			} catch (Exception ex) {
				log.error(String.format("Error processing [%-5s: %s]", sym, from), ex);
			}
		}
		return result;
	}

	private List<ATPriceHistory> requestHistory(String sym_, String from_, String minDateStr_, int iteration_) {
		try {
			int iteration = iteration_ + 1;
			if (iteration > 5) {
				log.error(String.format("Giving up [%-5s] history after [%d] iterations", sym_, iteration));
				return null;
			}

			log.debug(String.format("Getting history [%-5s: %s]", sym_, from_));
			String sizeStr = from_ == null ? "full" : "compact";
			String url = String.format(
					"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=%s&apikey=%s&datatype=csv&outputsize=%s",
					sym_, _avKey, sizeStr);
			String content = ATHttpHelper.getHttp(url);
			content = content != null ? content.toLowerCase() : null;
			if (StringUtils.isBlank(content) || content.contains("invalid api call")) {
				log.error(String.format("Error on [%s - %s]: %s", sym_, from_, content));
				return null;
			}
			if (content.contains("optimizing your api call frequency")) {
				log.debug(String.format("Reducing history call frequency [%-5s]...", sym_));
				Thread.sleep(7500);				
				return requestHistory(sym_, from_, minDateStr_, iteration);
			}
			List<ATPriceHistory> result = parseHistoryCsv(sym_, from_, content, minDateStr_, iteration_);
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error processing [%-5s: %s]", sym_, from_), ex);
			return null;
		}
	}

	private List<ATPriceHistory> parseHistoryCsv(String occId_, String from_, String csv_, String minDateStr_, int iteration_) throws IOException {
		List<ATPriceHistory> result = new ArrayList<>();
		if (StringUtils.isEmpty(csv_))
			return result;
		CSVParser records = CSVFormat.DEFAULT.parse(new StringReader(csv_));
		String from = from_ != null ? from_ : minDateStr_;
		boolean isFirst = true;
		for (CSVRecord rec : records) {
			if (rec.size() < 8) {
				if (_isErrorLogged) {
					log.error(String.format("Invalid record [%s - %s]", occId_, rec));
				} else {
					_isErrorLogged = true;
					log.error(String.format("Invalid record [%s - %s]: %s", occId_, rec, csv_));
				}
				continue;
			}
			if (rec.getRecordNumber() == 1)
				continue; // -> Header
			String dateStr = rec.get(0).replaceAll("-", "");
			if (from.compareTo(dateStr) >= 0)
				continue;
			String atId = String.format("%-5s%s", occId_, dateStr);
			try {
				// timestamp,open,high,low,close,adjusted_close,volume,dividend_amount,split_coefficient
				// 2018-01-03,67.6200,69.4900,67.6000,69.3200,69.3200,1698861,0.0000,1.0000
				ATPriceHistory pxh = new ATPriceHistory();
				pxh.setSource(SOURCE_KEY);
				pxh.setAtId(atId);
				pxh.setOccId(occId_);
				pxh.setDateStr(dateStr);
				pxh.setOpen(Double.parseDouble(rec.get(1)));
				pxh.setHigh(Double.parseDouble(rec.get(2)));
				pxh.setLow(Double.parseDouble(rec.get(3)));
				pxh.setClose(Double.parseDouble(rec.get(5)));
				pxh.setVolume(Long.parseLong(rec.get(6)));
				// AlphaVantage extra
				pxh.put("pxCua", Double.parseDouble(rec.get(4)));
				Double hdiv = Double.parseDouble(rec.get(7));
				if (hdiv != null && hdiv != 0) {
					pxh.put("hdiv", hdiv);
				}
				pxh.put("split", Double.parseDouble(rec.get(8)));
				result.add(pxh);
				if (from_ != null && isFirst && !pxh.getClose().equals(pxh.get("pxCua"))) {
					log.info(String.format("Retrying [%-5s]", occId_));
					result = requestHistory(occId_, null, minDateStr_, iteration_);
					return result;
				}
			} catch (Exception ex) {
				log.error(String.format("Error parsing [%s]: %s", atId, rec), ex);
			}
			isFirst = false;
		}
		return result;
	}

}
