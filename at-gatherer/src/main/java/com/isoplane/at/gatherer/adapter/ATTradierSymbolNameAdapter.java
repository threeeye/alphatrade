package com.isoplane.at.gatherer.adapter;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.isoplane.at.adapter.tradier.ATTradierBaseAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.gatherer.adapter.ATTradierSymbolNameAdapter.ATTradierSymbolLookupResponseWrapper.ATSymbolLookupResponse.ATSymbolLookup;

public class ATTradierSymbolNameAdapter extends ATTradierBaseAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierSymbolNameAdapter.class);

	// static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	static private Gson _gson = new Gson();

	// static String urlBase = "/beta/markets/fundamentals/company?symbols=%s";

	// private Configuration _config;

	public ATTradierSymbolNameAdapter() {
		super();
		// _config = config_;
	}

	public Set<ATSymbol> populateSymbolNames(Collection<ATSymbol> symbols_) {
		log.info(String.format("Checking names for [%d] symbols...", symbols_.size()));
		Set<ATSymbol> result = new TreeSet<>();
		Set<ATSymbol> incompleteSymbols = symbols_.stream()
				.filter(s -> StringUtils.isBlank(s.getName())).collect(Collectors.toSet());

		Set<String> missingNames = new TreeSet<>();
		long throttleTime = ATConfigUtil.config().getLong("tradier.throttletime");
		for (ATSymbol sym : incompleteSymbols) {
			try {
				String name = getName(sym);
				if (StringUtils.isNotBlank(name)) {
					log.debug(String.format("Adding name [%s -> %s]", sym.getId(), name));
					ATSymbol newSym = new ATSymbol(sym);
					newSym.setName(name);
					Set<String> refs = newSym.getReferences();
					if (refs == null) {
						refs = new TreeSet<>();
						newSym.setReferences(refs);
					}
					refs.add("trd");
					result.add(newSym);
				} else {
					missingNames.add(sym.getId());
				}
				Thread.sleep(throttleTime);
			} catch (InterruptedException ex) {
				log.error(String.format("Error processing [%s]", sym), ex);
			}
		}
		if (!missingNames.isEmpty())
			log.warn(String.format("Missing names %s", missingNames));
		log.info(String.format("Retrieved [%d] symbol names", result.size()));
		return result;
	}

	private String getName(IATSymbol sym_) {
		if (sym_ == null)
			return null;
		// long throttleTime = _config.getLong("tradier.throttletime");
		String trdsym = sym_.convert(PROVIDER_KEY);
		String url = String.format("/v1/markets/lookup?q=%s", trdsym);
		try {
			String json = getHttp(url);
			// log.info(String.format("getName [%s]:%s", sym_, json));
			ATTradierSymbolLookupResponseWrapper resp = _gson.fromJson(json, ATTradierSymbolLookupResponseWrapper.class);
			if (resp == null || resp.securities == null || resp.securities.security == null || resp.securities.security.isJsonNull())
				return null;
			JsonElement je = resp.securities.security;
			if (je.isJsonArray()) {
				JsonArray ja = je.getAsJsonArray();
				for (JsonElement jae : ja) {
					ATSymbolLookup lu = _gson.fromJson(jae, ATSymbolLookup.class);
					if (sym_.getId().equals(lu.symbol)) {
						return lu.description;
					}
				}
			} else {
				ATSymbolLookup lu = _gson.fromJson(je, ATSymbolLookup.class);
				return lu.description;
			}
			return null;
		} catch (Exception ex) {
			log.error(String.format("getName [%s]", sym_), ex);
			return null;
		}
	}

	static public class ATTradierSymbolLookupResponseWrapper {

		public ATSymbolLookupResponse securities;

		static public class ATSymbolLookupResponse {

			public JsonElement security;

			static public class ATSymbolLookup {

				public String symbol;
				public String description;
			}
		}
	}

}
