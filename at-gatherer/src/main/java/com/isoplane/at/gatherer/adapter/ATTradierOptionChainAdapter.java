package com.isoplane.at.gatherer.adapter;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.adapter.tradier.ATTradierBaseAdapter;
import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.IATOptionDescription;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

@Deprecated // Use ATTradierOptionExpirationAdapter instead
public class ATTradierOptionChainAdapter extends ATTradierBaseAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierOptionChainAdapter.class);

	// static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	static private Gson _gson = new Gson();

	static String urlBase = "/beta/markets/fundamentals/company?symbols=%s";

	// private Configuration _config;

	public ATTradierOptionChainAdapter() {
		super();
		// _config = config_;
	}

	public Map<String, Set<IATOptionDescription>> getOptionChains(Set<IATSymbol> symbols_) {
		log.info(String.format("Getting option chains for [%d] symbols...", symbols_.size()));
		Map<String, Set<IATOptionDescription>> result = new TreeMap<>();
		Set<String> expSet = new HashSet<>();
		Set<IATSymbol> missingExpirationsSet = new TreeSet<>();
		Set<IATSymbol> missingStrikesSet = new TreeSet<>();
		int optionCount = 0;
		long throttleTime = ATConfigUtil.config().getLong("tradier.throttletime");
		int count = 0;
		for (IATSymbol sym : symbols_) {
			try {
				count++;
				// String trdSymbol = sym.convert(PROVIDER_KEY);
				List<String> expirations = getExpirations(sym);
				if (expirations == null || expirations.isEmpty()) {
					log.debug(String.format("[%4d/%4d] NO option expirations [%s]", count, symbols_.size(), sym));
					missingExpirationsSet.add(sym);
					Thread.sleep(throttleTime);
					continue;
				}
				Set<IATOptionDescription> options = getOptionSymbols(sym, expirations);
				if (options == null || options.isEmpty()) {
					log.debug(String.format("[%4d/%4d] NO option strikes [%s]", count, symbols_.size(), sym));
					missingStrikesSet.add(sym);
					continue;
				}
				expSet.addAll(expirations);
				optionCount += options.size();
				result.put(sym.getId(), options);
				log.debug(String.format("[%4d/%4d] option chain [%-4s:%4d]", count, symbols_.size(), sym, options.size()));
			} catch (InterruptedException ex) {
				log.error(String.format("Error processing [%s]", sym), ex);
			}
		}
		if (!missingExpirationsSet.isEmpty())
			log.warn(String.format("Missing option expirations %s", missingExpirationsSet));
		if (!missingStrikesSet.isEmpty())
			log.warn(String.format("Missing option strikes %s", missingStrikesSet));
		log.info(String.format("Retrieved option chains [symbols: %d; expirations: %d; options: %d]", result.size(), expSet.size(), optionCount));
		return result;
	}

	private List<String> getExpirations(IATSymbol sym_) {
		if (sym_ == null)
			return null;
		long throttleTime = ATConfigUtil.config().getLong("tradier.throttletime");
		String trdsym = sym_.convert(PROVIDER_KEY);
		String url = String.format("/v1/markets/options/expirations?symbol=%s", trdsym);
		try {
			String json = getHttp(url);
			Thread.sleep(throttleTime);
			// log.info(String.format("Expirations [%s]:%s", sym_, json));
			ATTradierExpirationResponse resp = _gson.fromJson(json, ATTradierExpirationResponse.class);
			return resp != null && resp.expirations != null ? resp.expirations.date : null;
			// log.info(String.format("Expirations [%s]:%s", sym_, _gson.toJson(resp.expirations.date)));
		} catch (Exception ex) {
			log.error(String.format("getExpirations [%s]", sym_), ex);
			return null;
		}
	}

	private Set<IATOptionDescription> getOptionSymbols(IATSymbol sym_, List<String> expirations_) {
		if (sym_ == null || expirations_ == null || expirations_.isEmpty())
			return null;
		Set<IATOptionDescription> symbols = new TreeSet<>();
		Configuration config = ATConfigUtil.config();
		long throttleTime = config.getLong("tradier.throttletime");
		long pauseTime = config.getLong("tradier.pausetime");
		for (String exp : expirations_) {
			String json = "";
			try {
				String trdSym = sym_.convert(PROVIDER_KEY);
				String url = String.format("/v1/markets/options/strikes?symbol=%s&expiration=%s", trdSym, exp);
				json = getHttp(url);
				JsonObject root = _gson.fromJson(json, JsonObject.class);
				JsonElement jStrikes = root != null && !root.isJsonNull() ? root.get("strikes") : null;
				JsonElement jStrike = jStrikes != null && !jStrikes.isJsonNull() && jStrikes.isJsonObject() ? jStrikes.getAsJsonObject().get("strike")
						: null;
				if (jStrike == null || jStrike.isJsonNull())
					continue;
				String dstr = exp.replace("-", "");
				Date expiration = ATFormats.DATE_yyyyMMdd.get().parse(dstr);
				if (jStrike.isJsonArray()) {
					JsonArray strikeArray = jStrike.getAsJsonArray();
					strikeArray.forEach(s -> {
						if (s.isJsonNull())
							return;
						double strike = s.getAsDouble();
						IATOptionDescription option = new ATOptionDescription(sym_.getId(), expiration, strike, IATOptionDescription.Right.UNSET);
						symbols.add(option);
					});
				} else {
					double strike = jStrike.getAsDouble();
					IATOptionDescription option = new ATOptionDescription(sym_.getId(), expiration, strike, IATOptionDescription.Right.UNSET);
					symbols.add(option);
				}
				Thread.sleep(throttleTime);
			} catch (Exception ex) {
				if (json.contains("Quota Violation")) {
					log.warn(String.format("Tradier quota violation. Pausing [%dms]", pauseTime));
					try {
						Thread.sleep(pauseTime);
					} catch (InterruptedException exx) {
						log.error(String.format("Error throttling"), exx);
					}
				} else {
					log.error(String.format("getOptionSymbols [%s]: %s", sym_, json), ex);
				}
			}
		}
		return symbols;
	}

	static public class ATTradierExpirationResponse {

		public ATInternalExpirations expirations;

		public static class ATInternalExpirations {
			public List<String> date;
		}
	}

	static public class ATTradierStrikeResponse {

		public ATInternalStrikes strikes;

		public static class ATInternalStrikes {
			public List<Double> strike;
		}
	}

}
