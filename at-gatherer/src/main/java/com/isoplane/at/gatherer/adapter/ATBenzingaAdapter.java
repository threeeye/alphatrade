package com.isoplane.at.gatherer.adapter;

import java.io.File;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeSet;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATHttpHelper;

public class ATBenzingaAdapter {

	// https://www.benzinga.com/services/webapps/calendar/dividends?token=bcb21f0e5c2ec60fc1fa834444cf04665e04175a6e860cd5d27c9f25e31808d1&pagesize=500&parameters[date_from]=2018-02-16&parameters[date_to]=2018-04-23&parameters[date_sort]=ex&parameters[dividend_yield_operation]=gt&callback=calendarWebclientGetDateCB

	static final Logger log = LoggerFactory.getLogger(ATBenzingaAdapter.class);

	static final String SOURCE_KEY = "Bnzg";

	// private IATConfiguration _config;
	private Gson _gson = new Gson();

	public ATBenzingaAdapter() {
		// _config = confi_;
	}

	public Map<String, TreeSet<IATDividend>> getDividends() {
		String json = null;
		try {
			Configuration config = ATConfigUtil.config();
			String dataPath = config.getString("gatherer.dataPath");
			String path = String.format("%s/%s.dividends", dataPath, this.getClass().getSimpleName());

			String urlTmpl = "https://www.benzinga.com/services/webapps/calendar/dividends?token=%s&pagesize=500&parameters[date_from]=%s&parameters[date_to]=%s&parameters[date_sort]=ex&parameters[dividend_yield_operation]=gt&callback=calendarWebclientGetDateCB";

			// Random rand = new Random();
			// String token = String.format("bcb%df0e5c2ec%dfc1fa%dcf%de04175a6e860cd5d27c9f25e31808d1", 10 + rand.nextInt(89),
			// 10 + rand.nextInt(89), 100000 + rand.nextInt(899000), 10000 + rand.nextInt(89000));
			String token = getApiToken();
			Calendar from = Calendar.getInstance();
			from.set(Calendar.DAY_OF_MONTH, 1);
			String date_from = ATFormats.DATE_yyyy_MM_dd.get().format(from.getTime());
			Calendar to = Calendar.getInstance();
			to.add(Calendar.MONTH, 2);
			String date_to = ATFormats.DATE_yyyy_MM_dd.get().format(to.getTime());
			String url = String.format(urlTmpl, token, date_from, date_to);

			log.info(String.format("Getting dividends [%s - %s]: %s", date_from, date_to, url));

			json = ATHttpHelper.getHttp(url);
			try {
				if (StringUtils.isBlank(json)) {
					try (Scanner scn = new Scanner(new File(path)).useDelimiter("\\A")) {
						json = scn.next();
					}
					// json = new Scanner(new File(path)).useDelimiter("\\A").next();
				} else {
					json = json.substring(json.indexOf(":") + 1, json.lastIndexOf("}"));
					try (PrintWriter out = new PrintWriter(path)) {
						out.println(json);
					}
				}
			} catch (Exception ex2) {
				log.error(String.format("getDividends error writing [%s]", path), ex2);
			}
			// log.info("divjson: " + json);
			Map<String, TreeSet<IATDividend>> divMap = parseDividends(json);
			return divMap;
		} catch (Exception ex) {
			log.error(String.format("Error updating earings: " + json), ex);
			return null;
		}
	}

	private String getApiToken() {
		String url = "https://www.benzinga.com/calendar/dividends-ex";
		String content = ATHttpHelper.getHttp(url);
		int tIdx = content.indexOf("apiToken");
		String tokenSnip = content.substring(tIdx, tIdx + 75).replace("\"", "");
		String token = tokenSnip.split(":")[1];
		// log.info("Content: " + token);
		return token;
	}

	private Map<String, TreeSet<IATDividend>> parseDividends(String json_) {
		Map<String, TreeSet<IATDividend>> result = new HashMap<>();
		try {
			if (StringUtils.isBlank(json_))
				return result;
			String currentDateStr = ATFormats.DATE_yyyy_MM_dd.get().format(new Date());
			JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
			JsonArray array = root.getAsJsonArray("dividends");
			int count = 0;
			for (JsonElement jsonElement : array) {
				try {
					BenzingaDividend tmpl = _gson.fromJson(jsonElement, BenzingaDividend.class);
					if (currentDateStr.compareTo(tmpl.ex_dividend_date) > 0)
						continue;
					TreeSet<IATDividend> divs = result.get(tmpl.ticker);
					if (divs == null) {
						divs = new TreeSet<>();
						result.put(tmpl.ticker, divs);
					}
					//String atid = String.format("%s %s", tmpl.ticker, tmpl.ex_dividend_date);
					ATDividend div = new ATDividend();
					IATDividend.setSource(div, SOURCE_KEY);
					// div.setAtId(atid);
					div.setOccId(tmpl.ticker.trim());
					div.setDividend(Double.parseDouble(tmpl.dividend));
					// div.setChanged(true);
					div.setDividendExDateStr(formatDate(tmpl.ex_dividend_date, true));
					div.setDividendPayDateStr(formatDate(tmpl.payable_date, false));
					div.setDividendRecordDateStr(formatDate(tmpl.record_date, false));
					// div.setSource(SOURCE_KEY);
					div.setDividendFrequency(tmpl.frequency);
					divs.add(div);
					count++;
					// log.info("bnz: " + div);
				} catch (Exception iex) {
					log.error(String.format("Error parsing element: %s", jsonElement), iex);
				}
			}
			log.debug(String.format("Retrieved [%d] dividends", count));
		} catch (Exception ex) {
			log.error(String.format("Error parsing: %s", json_), ex);
		}
		return result;
	}

	private String formatDate(String dstr_, boolean isThrow_) throws ParseException {
		try {
			Date d = ATFormats.DATE_yyyy_MM_dd.get().parse(dstr_);
			String str = ATFormats.DATE_yyyyMMdd.get().format(d);
			return str;
		} catch (ParseException ex) {
			if (isThrow_) {
				throw ex;
			} else {
				log.error(String.format("Error parsing date [%s]", dstr_));
				return null;
			}
		}
	}

	static private class BenzingaDividend {
		@SuppressWarnings("unused")
		public String id;
		@SuppressWarnings("unused")
		public String date;
		public String ticker;
		@SuppressWarnings("unused")
		public String name;
		@SuppressWarnings("unused")
		public String exchange;
		public Integer frequency;
		public String dividend;
		@SuppressWarnings("unused")
		public String dividend_prior;
		@SuppressWarnings("unused")
		public String dividend_type;
		@SuppressWarnings("unused")
		public String dividend_yield;
		public String ex_dividend_date;
		public String payable_date;
		public String record_date;
		@SuppressWarnings("unused")
		public Integer updated;
	}
	// "id": "5a6911c3c7006f0001c513b1",
	// "date": "2018-01-24",
	// "ticker": "EDI",
	// "name": "Stone Harbor Emerging Markets Total Income Fund Common Shares of Beneficial Interests",
	// "exchange": "NYSE",
	// "frequency": 12,
	// "dividend": "0.1511",
	// "dividend_prior": 0,
	// "dividend_type": "Cash",
	// "dividend_yield": "0.1159335038363171",
	// "ex_dividend_date": "2018-04-13",
	// "payable_date": "2018-04-26",
	// "record_date": "2018-04-16",
	// "updated": 0

}
