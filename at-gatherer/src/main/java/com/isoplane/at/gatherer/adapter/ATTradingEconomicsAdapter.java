package com.isoplane.at.gatherer.adapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistentEconomicEvent;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATHttpHelper;

public class ATTradingEconomicsAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradingEconomicsAdapter.class);
	static final String SOURCE = "TrdEcn";

	static private SimpleDateFormat _eventDateFormat = new SimpleDateFormat("EEEEE MMMMM dd yyyy");
	static private SimpleDateFormat _eventTimeFormat = new SimpleDateFormat("hh:mm a");

	public Set<ATPersistentEconomicEvent> getEconomicEvents() {
		String url = "https://tradingeconomics.com/calendar";
		String cookie = "selected-cal-countries=usa,deu,emu,chn; selected-cal-importance=2; selected-custom-range-importance=%s|%s; ASP.NET_SessionId=ytt0x4qhujmd3ogngiyj1b3h; TECalendarOffset=-240";
		Date startDate = DateUtils.addWeeks(new Date(), -1);
		Date endDate = DateUtils.addWeeks(startDate, 5);
		cookie = String.format(cookie, ATFormats.DATE_yyyy_MM_dd.get().format(startDate), ATFormats.DATE_yyyy_MM_dd.get().format(endDate));
		// url =
		// "https://sslecal2.forexprostools.com/?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&importance=3&features=datepicker,timezone&countries=37,72,17,5&calType=week&timeZone=8&lang=1";
		Map<String, String> headers = new HashMap<>();
		headers.put("Cookie", cookie);
		String content = ATHttpHelper.getHttp(url, headers);
		Set<ATPersistentEconomicEvent> result = parseContent(content);
		return result;
	}

	private Set<ATPersistentEconomicEvent> parseContent(String html_) {
		Set<ATPersistentEconomicEvent> result = new TreeSet<>();
		Document doc = Jsoup.parse(html_);
		Element calTable = doc.selectFirst("#calendar"); // #calendar
		if (calTable == null)
			return result;
		Elements headers = calTable.select("thead.table-header"); // #ctl00_ContentPlaceHolder1_ctl02_Repeater1_ctl01_th1
		for (Element header : headers) {
			log.debug("header: " + header.text());
			// if(true) continue;
			try {
				Element tbody = header.nextElementSibling().nextElementSibling();
				if ("tbody".equals(tbody.tagName())) {
					Element eDate = header.selectFirst("tr th:nth-child(1)");
					String dStr = eDate.ownText().trim();
					Date day = _eventDateFormat.parse(dStr);
					// #ctl00_ContentPlaceHolder1_ctl02_Repeater1_ctl06_th1 > tr > th:nth-child(1)
					Element row = tbody.selectFirst("tr");
					while (row != null) {
						String logStr = String.format("[%s]: %s", dStr, row.text());
						log.debug("row  " + logStr);
						try {
							ATPersistentEconomicEvent event = new ATPersistentEconomicEvent();
							Element cell = row.selectFirst("td");

							Element eTime = cell.selectFirst("span");
							String tStr = eTime.ownText().trim();
							if (!StringUtils.isBlank(tStr)) {
								Date t = _eventTimeFormat.parse(tStr);
								Date eventDate = DateUtils.addMinutes(day, (int) DateUtils.getFragmentInMinutes(t, Calendar.DAY_OF_MONTH));
								event.setDateStr(ATFormats.DATE_TIME_mid.get().format(eventDate));
								String tClass = eTime.className();
								if (tClass.endsWith("1")) {
									event.setImportance(10);
								} else if (tClass.endsWith("2")) {
									event.setImportance(20);
								} else if (tClass.endsWith("3")) {
									event.setImportance(30);
								}
							} else {
								event.setDateStr(ATFormats.DATE_TIME_mid.get().format(day));
							}

							cell = cell.nextElementSibling();
							Element eRgn = cell.selectFirst("table tbody tr td:nth-child(1) div");
							String rgnStr = eRgn.attr("title").trim();
							event.setRegion(rgnStr);

							cell = cell.nextElementSibling();
							String dsc = cell.text().trim();
							event.setDescription(dsc);
							// if (dsc.startsWith("EIA Natural")) {
							// log.debug("hello");
							// }

							cell = cell.nextElementSibling();
							String actStr = cell.text().trim();
							ATMeasuredUnit actUnit = parseUnit(actStr, logStr);
							if (actUnit != null) {
								event.setActual(actUnit.value);
							}
							cell = cell.nextElementSibling();
							String prevStr = cell.text().trim();
							ATMeasuredUnit prevUnit = parseUnit(prevStr, logStr);
							if (prevUnit != null) {
								event.setPrevious(prevUnit.value);
								if (prevUnit.unit != null) {
									event.setUnit(prevUnit.unit);
								}
							}
							cell = cell.nextElementSibling();
							String consStr = cell.text().trim();
							ATMeasuredUnit consUnit = parseUnit(consStr, logStr);
							if (consUnit != null) {
								event.setConsensus(consUnit.value);
							}
							cell = cell.nextElementSibling();
							String foreStr = cell.text().trim();
							ATMeasuredUnit foreUnit = parseUnit(foreStr, logStr);
							if (foreUnit != null) {
								event.setForecast(foreUnit.value);
							}

							int atId = String.format("%s%s%s", event.getDateStr(), event.getRegion(), event.getDescription()).hashCode();
							event.setAtId("evt" + atId);
							event.setSource(SOURCE);
							result.add(event);
						} catch (Exception exx) {
							log.error(String.format("Error parsing row %s", logStr), exx);
						}
						row = row.nextElementSibling();
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error parsing section: %s", header), ex);
			}
		}
		return result;
	}

	private ATMeasuredUnit parseUnit(String value_, String logStr_) {
		value_ = value_ == null ? null : value_.replace("®", "").trim();
		if (StringUtils.isBlank(value_))
			return null;
		ATMeasuredUnit result = new ATMeasuredUnit();
		int charIdx = 0;
		char currentChar = value_.charAt(charIdx);
		String unit = "";
		while ('-' != currentChar && '+' != currentChar && !Character.isDigit(currentChar) && charIdx++ < value_.length()) {
			try {
				unit += Character.toString(currentChar);
				currentChar = value_.charAt(charIdx);
			} catch (Exception ex) {
				log.error(String.format("Error parsing [%s/%d]", value_, charIdx), ex);
			}
		}
		if (!StringUtils.isBlank(unit)) {
			result.unit = unit.trim();
			value_ = value_.substring(charIdx);
			// log.debug(String.format("Parsed [%s; %s]", value_, result_digest.unit));
		}
		if (StringUtils.isBlank(value_))
			return null;
		if (value_.contains("/")) {
			log.warn(String.format("Not numeric [%s]: %s", value_, logStr_));
			return null;
		}
		char lastChar = value_.charAt(value_.length() - 1);
		if (Character.isDigit(lastChar)) {
			Double value = Double.parseDouble(value_);
			result.value = value;
		} else if (value_.length() > 1) {
			Double value = null;
			if (value_.endsWith("K")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 1));
				value *= 1000.0;
			} else if (value_.endsWith("M")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 1));
				value *= 1000000.0;
			} else if (value_.endsWith("B")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 1));
				value *= 1000000000.0;
			} else if (value_.endsWith("T")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 1));
				value *= 100000000000.0;
			} else if (value_.endsWith("%")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 1));
			} else if (value_.endsWith("Bcf")) {
				value = Double.parseDouble(value_.substring(0, value_.length() - 3));
			} else {
				log.error(String.format("Unknown range or unit [%s]", value_));
				return null;
			}
			result.value = value;
		} else {
			log.warn(String.format("Not numeric [%s]: %s", value_, logStr_));
			return null;
		}
		return result;
	}

	private static class ATMeasuredUnit {
		Double value;
		String unit;
	}

}
