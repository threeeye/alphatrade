package com.isoplane.at.gatherer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ATAdapterRegistry;
import com.isoplane.at.adapter.IATDividendAdapter;
import com.isoplane.at.adapter.IATOptionExpirationAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.util.ATGathererMongodbTools;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.symbols.ATSymbolManager;

public class ATGathererController {

	static final Logger log = LoggerFactory.getLogger(ATGathererController.class);

	// private Configuration _config;
	private ATGathererMongodbTools _mongodb;
	private ATSymbolManager _symbolsManager;
	private IATDividendAdapter _divAdapter;
	private IATOptionExpirationAdapter _oexpAdapter;

	public ATGathererController() {
		// _config = config_;
		init();
	}

	private void init() {
		_mongodb = new ATGathererMongodbTools();
		_mongodb.init();

		_symbolsManager = new ATSymbolManager(false);
		_symbolsManager.initService();

		_divAdapter = ATAdapterRegistry.getDividendAdapter();// new ATTradierDividendAdapter(_config);
		_oexpAdapter = ATAdapterRegistry.getOptionExpirationAdapter();// new ATTradierOptionExpirationAdapter(_config);
	}

	public Map<String, IATDividend> getDividends(Collection<String> symbols_, boolean collectMissing_) {
		Collection<String> symbols = symbols_ != null ? symbols_ : new ArrayList<>();
		String today = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		IATWhereQuery query = ATPersistenceRegistry.query().gte(IATAssetConstants.DIV_EX_DATE_STR_OLD, today).or()
				.gte(IATAssetConstants.DIV_EST_DATE_STR, today);
		query = symbols.isEmpty() ? query : query.and().in(IATAssetConstants.OCCID, symbols);
		Map<String, IATDividend> resultMap = _mongodb.getDividends(query);
		if (collectMissing_ && !symbols.isEmpty()) {
			Set<String> ids = new TreeSet<>(symbols);
			ids.removeAll(resultMap.keySet());
			if (!ids.isEmpty()) {
				Set<IATSymbol> workSyms = convertToSymbols(ids);
				log.debug(String.format("Requesting [%d/%d] estimated dividends", workSyms.size(), symbols.size()));
				Map<String, IATDividend> newMap = _divAdapter.getDividends(workSyms);
				if (newMap != null && !newMap.isEmpty()) {
					resultMap.putAll(newMap);
					boolean success = _mongodb.updateDividends(newMap.values());
					log.debug(String.format("Updated [%d/%s] dividends", success ? newMap.size() : -1, symbols.size()));
				}
			}
		}
		return resultMap;
	}

	public boolean updateDividends(Collection<IATDividend> divs_) {
		return _mongodb.updateDividends(divs_);
	}

	public long deleteDividends(IATWhereQuery query_) {
		return _mongodb.deleteDividends(query_);
	}

	public Map<String, IATOptionExpiration> getOptionExpirations(boolean collectMissing_, String... symbols_) {
		if (symbols_ == null || symbols_.length == 0)
			return new HashMap<>();
		List<String> symbols = Arrays.asList(symbols_);
		IATWhereQuery query = ATPersistenceRegistry.query().in(IATMongo.MONGO_ID, symbols);
		Map<String, IATOptionExpiration> existingMap = _mongodb.getOptionExpirations(query);
		if (collectMissing_ && !symbols.isEmpty()) {
			Set<String> ids = new TreeSet<>(symbols);
			ids.removeAll(existingMap.keySet());
			if (!ids.isEmpty()) {
				Set<IATSymbol> workSyms = convertToSymbols(ids);
				log.debug(String.format("Processing [%d/%d] estimated dividends", workSyms.size(), symbols.size()));
				Map<String, IATOptionExpiration> newExpMap = _oexpAdapter.getOptionExpirations(workSyms);
				if (newExpMap != null && !newExpMap.isEmpty()) {
					existingMap.putAll(newExpMap);
					boolean success = _mongodb.updateOptionExpirations(newExpMap.values());
					log.info(String.format("Updated [%d] option expirations", success ? newExpMap.size() : -1));
				}
			}
		}
		return existingMap;
	}

	public long deleteOptionExpirations(IATWhereQuery query_) {
		return _mongodb.deleteOptionExpirations(query_);
	}

	private Set<IATSymbol> convertToSymbols(Collection<String> symbols_) {
		Set<IATSymbol> workSyms = new TreeSet<>();
		Map<String, ATSymbol> knownSyms = _symbolsManager.getSymbols();
		for (String sym : symbols_) {
			ATSymbol iSym = knownSyms.get(sym);
			if (iSym == null) {
				iSym = new ATSymbol(sym);
			}
			workSyms.add(iSym);
		}
		return workSyms;
	}

	public TreeMap<String, ATPriceHistory> getPriceHistory() {
		return _mongodb.getPriceHistory();
	}

	public int processStatistics(Set<IATSymbol> symbols_) {
		String minDateStr = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addYears(new Date(), -2));
		Set<String> inSymbols = symbols_ != null ? symbols_.stream().map(s -> s.getId()).collect(Collectors.toSet()) : null;
		Set<String> oldSymbols = _mongodb.getHistorySymbols();
		int rateCount = 0;
		int count = 0;
		// long start = System.currentTimeMillis();
		for (String sym : oldSymbols) {
			if (inSymbols != null && !inSymbols.contains(sym))
				continue;
			count++;
			log.debug(String.format("[%4d/%4d] processStatistics [%-5s]", count, oldSymbols.size(), sym));
			int newRateCount = processStatistics(sym, minDateStr);
			rateCount += newRateCount;
		}
		return rateCount;
		// long duration = System.currentTimeMillis() - start;
		// double rate = (rateCount * 1000.0) / duration;
		// log.info(String.format("Updated [%d] statistic records [rate: %.2f/s]", rateCount, rate));
	}

	public int processStatistics(String symbol_, String minDS8_) {
		int rateCount = 0;
		try {
			String minKey = String.format("%-5s%s", symbol_, minDS8_);

			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, symbol_).and().gte(ATPriceHistory.AT_ID, minKey);
			List<ATPriceHistory> history = _mongodb.getHistory(query);
			history.sort(new Comparator<ATPriceHistory>() {

				@Override
				public int compare(ATPriceHistory h1, ATPriceHistory h2) {
					return h1.getDateStr().compareTo(h2.getDateStr());
				}
			});
			DescriptiveStatistics s10 = new DescriptiveStatistics();
			s10.setWindowSize(10);
			DescriptiveStatistics s20 = new DescriptiveStatistics(); // ~ 1 month (21.1667 days)
			s20.setWindowSize(20);
			DescriptiveStatistics s30 = new DescriptiveStatistics();
			s30.setWindowSize(30);
			DescriptiveStatistics s60 = new DescriptiveStatistics(); // ~ 3 months (63.5 days)
			s60.setWindowSize(60);
			DescriptiveStatistics s90 = new DescriptiveStatistics();
			s90.setWindowSize(90);
			DescriptiveStatistics s125 = new DescriptiveStatistics(); // ~ 6 months (127 days)
			s125.setWindowSize(125);
			DescriptiveStatistics s254 = new DescriptiveStatistics();
			s254.setWindowSize(254);

			DescriptiveStatistics m60 = new DescriptiveStatistics();
			m60.setWindowSize(60);
			DescriptiveStatistics m125 = new DescriptiveStatistics();
			m125.setWindowSize(125);
			DescriptiveStatistics m254 = new DescriptiveStatistics();
			m254.setWindowSize(254);
			double previous = -1;
			for (ATPriceHistory h : history) {
				double close = h.getClose();
				double lret = previous == -1 ? 0 : Math.log(close / previous); // Math.abs((close-previous)/previous);
				previous = close;

				s10.addValue(lret);
				s20.addValue(lret);
				s30.addValue(lret);
				s60.addValue(lret);
				s90.addValue(lret);
				s125.addValue(lret);
				s254.addValue(lret);
				double stdev10 = s10.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev20 = s20.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev30 = s30.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev60 = s60.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev90 = s90.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev125 = s125.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;
				double stdev254 = s254.getStandardDeviation() * ATPriceHistory.ANNUALIZED_FACTOR;

				m60.addValue(close);
				m125.addValue(close);
				m254.addValue(close);

				boolean isPersist = true;// h.getHV10() == null; // TODO: investigate this condition
				h.setHV10(stdev10);
				h.setHV20(stdev20);
				h.setHV30(stdev30);
				h.setHV60(stdev60);
				h.setHV90(stdev90);
				h.setHV125(stdev125);
				h.setHV254(stdev254);
				h.setMin60(m60.getMin());
				h.setMax60(m60.getMax());
				h.setSma60(m60.getMean());
				h.setMin125(m125.getMin());
				h.setMax125(m125.getMax());
				h.setSma125(m125.getMean());
				h.setMin254(m254.getMin());
				h.setMax254(m254.getMax());
				h.setSma254(m254.getMean());

				if (isPersist) {
					_mongodb.updateHistory(h);
					rateCount++;
				}
			}
		} catch (Exception ex) {
			log.error(String.format("processStatistics [%s]", symbol_), ex);
		}
		return rateCount;
	}

}
