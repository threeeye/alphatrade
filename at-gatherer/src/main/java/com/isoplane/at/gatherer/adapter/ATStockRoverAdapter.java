package com.isoplane.at.gatherer.adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeSet;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.store.ATEarning;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATStockRoverAdapter {

	static final Logger log = LoggerFactory.getLogger(ATStockRoverAdapter.class);

	// static private final String EARNINGS_URL = "https://www.stockrover.com/markets/central/get_earnings_calendar_data";
	static private final String EARNINGS_URL = "https://www.stockrover.com/central/get_earnings_calendar_data";

	static final String SOURCE_KEY = "StRo";

	private Gson _gson = new Gson();
	// private IATConfiguration _config;

	public ATStockRoverAdapter() {
		// _config = config_;
	}

	// Used for daily call to update and persist latest earnings
	public Map<String, TreeSet<ATEarning>> getEarnings() {
		// Note: Implementation ignores input and retrieves all earnings with one call
		try {
			Configuration config = ATConfigUtil.config();
			String dataPath = config.getString("gatherer.dataPath");
			String path = String.format("%s/%s.earnings", dataPath, ATStockRoverAdapter.class.getSimpleName());
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.DAY_OF_MONTH, -1);
			while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				cal.add(Calendar.DAY_OF_MONTH, -1);
			}
			String startStr = String.format("%sT00:00:00", ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime()));
			for (int i = 1; i < 19; i++) {
				cal.add(Calendar.WEEK_OF_YEAR, 1);
			}
			cal.add(Calendar.DAY_OF_MONTH, -1);
			String endStr = String.format("%sT23:59:59", ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime()));

			log.info(String.format("Getting earnings [%s - %s]", startStr, endStr));

			Map<String, String> form = new HashMap<>();
			form.put("start_date", startStr);
			form.put("end_date", endStr);
			form.put("min_date", startStr);
			form.put("max_date", endStr);
			form.put("auth_window", "false");
			form.put("full_data_request", "0");
			form.put("ticker_list_request", "0");
			form.put("ticker_day_request", "0");
			form.put("tickers", "0");
			String json = postHttp(EARNINGS_URL, form);
			try {
				if (StringUtils.isBlank(json)) {
					json = new Scanner(new File(path)).useDelimiter("\\A").next();
				} else {
					try (PrintWriter out = new PrintWriter(path)) {
						out.println(json);
					}
				}
			} catch (Exception ex2) {
				log.error(String.format("getEarnings error writing [%s]", path), ex2);
			}
			Map<String, TreeSet<ATEarning>> earningsMap = parseEarnings(json);
			return earningsMap;
		} catch (Exception ex) {
			log.error(String.format("Error updating earings"), ex);
			return null;
		}
	}

	private Map<String, TreeSet<ATEarning>> parseEarnings(String json_) {
		Map<String, TreeSet<ATEarning>> earningsMap = new HashMap<>();
		JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
		JsonArray array = root.getAsJsonArray("earnings_calendar_next_quarter");
		int count = 0;
		for (JsonElement jsonElement : array) {
			String occId = null;
			String dateStr = null;
			try {
				JsonArray earnArray = jsonElement.getAsJsonArray();
				occId = earnArray.get(0).getAsString();
				TreeSet<ATEarning> earnings = earningsMap.get(occId);
				if (earnings == null) {
					earnings = new TreeSet<>();
					earningsMap.put(occId, earnings);
				}
				// dateStr = earnArray.get(3).getAsString();
				// Date date = ATFormats.DATE_full.get().parse(dateStr);
				dateStr = earnArray.get(4).getAsString();
				Date date = ATFormats.DATE_yyyy_MM_dd.get().parse(dateStr);
				dateStr = ATFormats.DATE_yyyyMMdd.get().format(date);
				String atId = String.format("%-5s%s", occId, dateStr);

				ATEarning ern = new ATEarning();
				ern.setSource(SOURCE_KEY);
				ern.setAtId(atId);
				ern.setOccId(occId);
				ern.setEarningsDateStr(dateStr);
				earnings.add(ern);
				count++;
			} catch (Exception ex) {
				log.error(String.format("Error parsing date [%s - %s]", occId, dateStr));
			}
		}
		log.debug(String.format("Retrieved [%d] earnings", count));
		return earningsMap;
	}

	private String postHttp(String url_, Map<String, String> form_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url_);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			for (Entry<String, String> nameValuePair : form_.entrySet()) {
				params.add(new BasicNameValuePair(nameValuePair.getKey(), nameValuePair.getValue()));
			}
			httpPost.setEntity(new UrlEncodedFormEntity(params));

			response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);
			return content;
		} finally {
			if (response != null)
				response.close();
		}
	}

}
