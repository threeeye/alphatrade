package com.isoplane.at.gatherer.adapter;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.annotation.Obsolete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.store.ATPersistentEquityDescription;
import com.isoplane.at.gatherer.util.ATHttpHelper;

@Obsolete() // Unreliable updates from wikipedia source
public class ATOknfAdapter {

	static final Logger log = LoggerFactory.getLogger(ATOknfAdapter.class);
	
	static final String SOURCE_KEY = "OKNF";

	private IATConfiguration _config;
	private Gson _gson = new Gson();

	public ATOknfAdapter(IATConfiguration config_) {
		_config = config_;
	}

	public Set<ATPersistentEquityDescription> getSP500() {
		try {
			String path = String.format("%s/%s.sp500", _config.getString("dataPath"), ATOknfAdapter.class.getSimpleName());
			String url = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents.json";
			String json = ATHttpHelper.getHttp(url);
			try {
				if (StringUtils.isBlank(json)) {
					json = new Scanner(new File(path)).useDelimiter("\\A").next();
				} else {
					try (PrintWriter out = new PrintWriter(path)) {
						out.println(json);
					}
				}
			} catch (Exception ex2) {
				log.error(String.format("getSP500 error writing [%s]", path), ex2);
			}
			Set<ATPersistentEquityDescription> result = parseConstituents(json);
			log.info(String.format("SP 500 [%,d]", result.size()));
			return result;
		} catch (Exception ex) {
			log.error("Error getting SP500", ex);
			return null;
		}
	}

	private Set<ATPersistentEquityDescription> parseConstituents(String json_) {
		Set<ATPersistentEquityDescription> result = new TreeSet<>();
		try {
			if (StringUtils.isBlank(json_))
				return result;
			OkfnConstituent[] cnsts = _gson.fromJson(json_, OkfnConstituent[].class);
			for (OkfnConstituent okfnConstituent : cnsts) {
				ATPersistentEquityDescription stock = new ATPersistentEquityDescription();
				stock.setSource(SOURCE_KEY);
				stock.setOccId(okfnConstituent.Symbol);
				stock.setDescription(okfnConstituent.Name);
				stock.setSector(okfnConstituent.Sector);
				result.add(stock);
			}
		} catch (Exception ex) {
			log.error(String.format("Error parsing: %s", json_), ex);
		}
		return result;
	}

	static private class OkfnConstituent {
		public String Name;
		public String Symbol;
		public String Sector;
		@SuppressWarnings("unused")
		public Double Price;
		@SerializedName("Dividend Yield")
		public Double DividendYield;
		@SerializedName("Price/Earnings")
		public Double PriceEarnings;
		@SerializedName("Earnings/Share")
		public Double EarningsShare;
		@SerializedName("Book Value")
		public Double BookValue;
		@SerializedName("52 week low")
		public Double Price52Low;
		@SerializedName("52 week high")
		public Double Price52High;
		@SerializedName("Market Cap")
		public Double MarketCap;
		@SuppressWarnings("unused")
		public Double EBITDA;
		@SerializedName("Price/Sales")
		public Double PriceSales;
		@SerializedName("Price/Book")
		public Double PriceBook;
		@SerializedName("SEC Filings")
		public String SECFilings;
	}

}
