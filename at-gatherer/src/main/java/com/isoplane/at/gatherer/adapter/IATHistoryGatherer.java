package com.isoplane.at.gatherer.adapter;

import java.util.Date;
import java.util.Map;
import java.util.TreeSet;

import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPriceHistory;

public interface IATHistoryGatherer {
	
	TreeSet<ATPriceHistory> getHistory(Map<IATSymbol, String> workMap, Date minDate_);

}