package com.isoplane.at.gatherer.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.gatherer.model.ATMongoDividend;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

public class ATGathererMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATGathererMongodbTools.class);

	// private Configuration _config;
	private ATMongoDao _dao;
	private String _divTable;
	private String _optionexpTable;
	private String _historyTable;

	public ATGathererMongodbTools() {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;

		_divTable = ATMongoTableRegistry.getDividendTable();
		_optionexpTable = ATMongoTableRegistry.getOptionExpiryTable();
		_historyTable = ATMongoTableRegistry.getHistoryTable();
	}

	public void init() {
		_dao = new ATMongoDao();
	}

	public Map<String, IATDividend> getDividends(IATWhereQuery query_) {
		IATWhereQuery query = query_ != null ? query_ : ATPersistenceRegistry.query().empty();
		List<String> keys = Arrays.asList("div", "div_frq", "divex_dstr", "divpy_dstr", "divrc_dstr", "div_est_ds8");
		Map<String, ATMongoDividend> divMap = _dao.queryEdge(_divTable, query, IATAssetConstants.OCCID, IATLookupMap.AT_ID, -1, keys,
				ATMongoDividend.class);
		Map<String, IATDividend> resultMap = new TreeMap<>();
		if (divMap != null) {
			for (ATMongoDividend mDiv : divMap.values()) {
				String occId = (String) mDiv.get(IATMongo.MONGO_ID);
				// mDiv.setOccId(occId);
				ATDividend div = new ATDividend(mDiv);
				// String occId = (String) mDiv.get(IATMongo.MONGO_ID);
				div.setOccId(occId);
				resultMap.put(occId, div);
			}
		}
		return resultMap;
	}

	public boolean updateDividends(Collection<IATDividend> divs_) {
		if (divs_ == null || divs_.isEmpty())
			return false;
		List<IATLookupMap> data = divs_.stream().map(d -> new ATMongoDividend(d)).collect(Collectors.toList());
		boolean success = _dao.update(_divTable, data.toArray(new IATLookupMap[0]));
		return success;
	}

	public long deleteDividends(IATWhereQuery query_) {
		if (query_ == null)
			return -1;
		long count = _dao.delete(_divTable, query_);
		return count;
	}

	public Map<String, IATOptionExpiration> getOptionExpirations(IATWhereQuery query_) {
		IATWhereQuery query = query_ != null ? query_ : ATPersistenceRegistry.query().empty();
		List<ATPersistentLookupBase> oeList = _dao.query(_optionexpTable, query, ATPersistentLookupBase.class);
		Map<String, IATOptionExpiration> resultMap = new TreeMap<>();
		if (oeList != null) {
			for (ATPersistentLookupBase base : oeList) {
				ATOptionExpiration oe = new ATOptionExpiration();
				oe.setOccId((String) base.get(IATMongo.MONGO_ID));
				oe.setExpirations(IATOptionExpiration.getExpirations(base));
				resultMap.put(oe.getOccId(), oe);
			}
		}
		return resultMap;
	}

	public boolean updateOptionExpirations(Collection<IATOptionExpiration> exps_) {
		if (exps_ == null || exps_.isEmpty())
			return false;
		int count = 0;
		for (IATOptionExpiration exp : exps_) {
			String occId = exp.getOccId();
			Map<String, Object> map = exp.getAsMap();
			map.put(IATMongo.MONGO_ID, occId);
			map.remove(IATAssetConstants.OCCID);
			boolean success = _dao.updateValues(_optionexpTable, occId, map, true);
			if (success)
				count++;
		}
		return count == exps_.size();
	}

	public long deleteOptionExpirations(IATWhereQuery query_) {
		if (query_ == null)
			return -1;
		long count = _dao.delete(_optionexpTable, query_);
		return count;
	}

	public TreeMap<String, ATPriceHistory> getPriceHistory() {
		TreeMap<String, ATPriceHistory> historyMap = _dao.groupMax(_historyTable, IATAssetConstants.OCCID, IATAssetConstants.DATE_STR,
				ATPriceHistory.class);
		return historyMap;
	}

	public Set<String> getHistorySymbols() {
		IATWhereQuery emptyQuery = ATPersistenceRegistry.query().empty();
		Set<String> symbols = new TreeSet<>(_dao.distinct(_historyTable, emptyQuery, IATAssetConstants.OCCID, String.class));
		return symbols;
	}

	public List<ATPriceHistory> getHistory(IATWhereQuery query_) {
		List<ATPriceHistory> history = _dao.query(_historyTable, query_, ATPriceHistory.class);
		return history;
	}

	public boolean updateHistory(ATPriceHistory hist_) {
		boolean result = _dao.update(_historyTable, hist_);
		return result;
	}

}
