package com.isoplane.at.gatherer.adapter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.gatherer.util.ATHttpHelper;

public class ATTradingBlockAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradingBlockAdapter.class);

	public Map<String, Double> getImpliedVolatility(Set<String> symbols_) {

		log.info(String.format("Getting IV for [%d] symbols", symbols_.size()));
		// http://externaltools.tradingblock.com/CBOE/volfinder/index.aspx?s=0&p=138&t=5
		Map<String, Double> result = new HashMap<>();
		Set<String> symbols = new HashSet<>(symbols_);
		String urlTmpl = "http://externaltools.tradingblock.com/CBOE/volfinder/index.aspx?s=0&p=%d&t=5";
		int page = 0;
		int count = 0;
		int countTrigger = 0;
		boolean isKeepGoing = true;
		String content = null;
		while (isKeepGoing) {
			try {
				String url = String.format(urlTmpl, page++);
				content = ATHttpHelper.getHttp(url);
				Map<String, Double> ivMap = parseContent(content, symbols, count, symbols_.size(), page);
				if (ivMap == null) {
					isKeepGoing = false;
				} else {
					result.putAll(ivMap);
					if (symbols.isEmpty()) {
						isKeepGoing = false;
					}
					count += ivMap.size();
				}
				int tempTrigger = count / 100;
				if (tempTrigger != countTrigger) {
					countTrigger = tempTrigger;
					log.info(String.format("IV progress [%d/%d]", countTrigger * 100, symbols.size()));
				}
				// if (log.isDebugEnabled()) {
				// log.debug(
				// String.format("[%4d/%4d - %3d] Processing IV: %s", count, symbols_.size(), page, ivMap != null ? ivMap.keySet() : null));
				// }
				// if (page > 5) // TODO: REMOVE!
				// isKeepGoing = false;
				Thread.sleep(5000);
			} catch (Exception ex) {
				log.error(String.format("getImpliedVolatility: %s", content), ex);
			}
		}
		log.info(String.format("IV Capture [%4d/%4d]", result.size(), symbols_.size()));
		return result;
	}

	private Map<String, Double> parseContent(String html_, Set<String> symbols_, int count_, int total_, int cycle_) {
		Map<String, Double> result = new HashMap<>();
		Document doc = Jsoup.parse(html_);
		Elements tableRows = doc.select("table tbody tr.volfinder_result, table tbody tr.volfinder_result2");
		// #Form1 > div:nth-child(13) > table > tbody > tr:nth-child(4)
		// //*[@id="Form1"]/div[2]/table/tbody/tr[4]/td/text()
		if (tableRows == null || tableRows.isEmpty())
			return null;
		// int count = 0;
		for (Element row : tableRows) {
			try {
				Elements cells = row.select("td");
				if (cells == null || cells.size() < 4)
					continue;
				String symbol = cells.get(0).text();
				if (symbol != null && symbols_.contains(symbol)) {
					symbols_.remove(symbol);
					String ivStr = cells.get(3).ownText();
					if (StringUtils.isBlank(ivStr))
						continue;
					Double iv = Double.parseDouble(ivStr) / 100.0;
					result.put(symbol, iv);
					log.debug(String.format("[%4d/%4d/%3d] IV capture [%-5s: %6.4f]", ++count_, total_, cycle_, symbol, iv));
				}
			} catch (Exception iex) {
				log.error(String.format("parseContent %s", row));
			}
		}
		// log.debug(String.format(" IV Capture cycle [%3d/%3d]", count, symbols_.size()));
		return result;
	}

	public static void main(String[] args_) {
		ATTradingBlockAdapter adapter = new ATTradingBlockAdapter();
		adapter.getImpliedVolatility(new HashSet<String>(Arrays.asList("AAPL")));
	}
}
