package com.isoplane.at.gatherer.adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeSet;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.symbols.model.ATMongoSymbol;

@Deprecated
public class ATYahooAdapter implements IATHistoryGatherer {

	static final Logger log = LoggerFactory.getLogger(ATYahooAdapter.class);
	static public final String SOURCE_KEY = "Y!";
	static public final String PROVIDER_KEY = "yahoo";

	// static Map<String, String> _symbolMap;

	// static String mapSymbol(String sym_) {
	// String symbol = _symbolMap.get(sym_);
	// return symbol != null ? symbol : sym_;
	// }

	private IATConfiguration _config;
	boolean _isErrorLogged = false;
	HttpClientContext _ctx;

	public static void main(String[] args) {

		ATYahooAdapter inst = new ATYahooAdapter(null);
		Map<IATSymbol, String> hMap = new HashMap<>();
		ATMongoSymbol sym = new ATMongoSymbol(); // TODO: Removed mongo dependency
		sym.setAtId("AAPL");
		hMap.put(sym, "20181001");
		TreeSet<ATPriceHistory> hist = inst.getHistory(hMap, new Date());
		log.debug("hist: " + new Gson().toJson(hist));
	}

	public ATYahooAdapter(IATConfiguration config_) {
		_config = config_;
		// _symbolMap = cnvMap_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.gatherer.adapter.IATHistoryGatherer#getHistory(java.util.Map, java.util.Date)
	 */
	@Override
	public TreeSet<ATPriceHistory> getHistory(Map<IATSymbol, String> symMap_, Date minDate_) {
		TreeSet<ATPriceHistory> yahooResult = new TreeSet<>();
		String minDateStr = ATFormats.DATE_yyyyMMdd.get().format(minDate_);
		try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
			IATSymbol dummySymbol = symMap_.keySet().iterator().next();
			String crumb = getCrumb(httpclient, dummySymbol);
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			long todayLong = today.getTime() / 1000;

			int count = 0;
			for (Entry<IATSymbol, String> entry : symMap_.entrySet()) {
				count++;
				IATSymbol symbol = entry.getKey();
				log.debug(String.format("[%4d/%4d] getHistory [%-5s]", count, symMap_.size(), symbol));
				String dateStr = entry.getValue();
				if (StringUtils.isBlank(dateStr)) {
					dateStr = minDateStr;
				}
				Date from = ATFormats.DATE_yyyyMMdd.get().parse(dateStr);
				try {
					long fromLong = (long) (from.getTime() / 1000);
					List<ATPriceHistory> hist = requestHistory(httpclient, crumb, symbol, "1d", fromLong, todayLong);
					if (hist != null) {
						yahooResult.addAll(hist);
					}
					Thread.sleep(getPeriod());
				} catch (Exception exx) {
					log.error(String.format("getHistory [%s/%s]: %s", symbol, dateStr, exx.getMessage()));
				}
			}
		} catch (Exception ex) {
			log.error(String.format("getHistory"), ex);
		}
		return yahooResult;
	}

	private List<ATPriceHistory> requestHistory(CloseableHttpClient client_, String crumb_, IATSymbol sym_, String interval_, long from_, long to_)
			throws Exception {
		long from = from_ > 9999999999L ? from_ / 1000 : from_;
		long to = to_ > 9999999999L ? to_ / 1000 : to_;
		String sym = sym_.convert(PROVIDER_KEY);
		// Yahoo not good for index
		if (sym.startsWith("$")) {
			log.warn(String.format("Symbol requires conversion [%s]", sym));
			return null;
		}
		String requestSym = URLEncoder.encode(sym_.convert(PROVIDER_KEY), "UTF-8");
		String url = String.format(
				"https://query1.finance.yahoo.com/v7/finance/download/%s?period1=%d&period2=%d&interval=%s&events=history&crumb=%s",
				requestSym, from, to, interval_, crumb_);
		String content = getHttp(client_, _ctx, url);
		// log.debug("Content: " + content);
		List<ATPriceHistory> hist = parseHistoryCsv(sym_, from_, content);
		return hist;
	}

	private String getCrumbToken(CloseableHttpClient client_, HttpClientContext ctx_) {
		final String CRUMB_STORE = "CrumbStore";
		String url = String.format("https://finance.yahoo.com/quote/SPY");
		String content = getHttp(client_, ctx_, url);
		int idx1 = content.indexOf(CRUMB_STORE);
		String sub = content.substring(idx1, idx1 + 64).replace(" ", "");
		int idx2 = sub.indexOf("}");
		sub = sub.substring(0, idx2 - 1);
		int idx3 = sub.lastIndexOf("\"");
		String crumb = sub.substring(idx3 + 1);
		crumb = crumb.replace("\\u002F", "/");
		log.debug(String.format("Crumb [%s]", crumb));
		return crumb;
	}

	private String getCrumb(CloseableHttpClient client_, IATSymbol dummy_) {
		String crumb = null;
		for (int i = 0; i < 3; i++) {
			try {
				_ctx = HttpClientContext.create();
				crumb = getCrumbToken(client_, _ctx);

				Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
				Date minDate = DateUtils.addDays(now, -5);
				Map<String, String> testMap = new HashMap<>();
				testMap.put("AAPL", ATFormats.DATE_yyyyMMdd.get().format(minDate));
				List<ATPriceHistory> testHist = requestHistory(client_, crumb, dummy_, "1d", minDate.getTime(), now.getTime());
				if (testHist != null && !testHist.isEmpty()) {
					return crumb;
				}
			} catch (Exception ex) {
				log.error(String.format("Error extracting crumb [%s]", crumb), ex);
			}
		}
		throw new ATException("Failed to extract crumb");
	}

	private List<ATPriceHistory> parseHistoryCsv(IATSymbol sym_, long from_, String csv_) throws IOException {
		List<ATPriceHistory> result = new ArrayList<>();
		if (StringUtils.isEmpty(csv_))
			return result;
		CSVParser records = CSVFormat.DEFAULT.parse(new StringReader(csv_));
		String from = from_ + "";
		for (CSVRecord rec : records) {
			if (rec.size() < 7) {
				if (_isErrorLogged) {
					log.error(String.format("Invalid record [%s - %s]", sym_, rec));
				} else {
					_isErrorLogged = true;
					log.error(String.format("Invalid record [%s - %s]: %s", sym_, rec, csv_));
				}
				continue;
			}
			if (rec.getRecordNumber() == 1)
				continue; // -> Header
			String dateStr = rec.get(0).replaceAll("-", "");
			if (from.compareTo(dateStr) >= 0)
				continue;
			String atId = String.format("%-5s%s", sym_.getId(), dateStr);
			try {
				// Date,Open,High,Low,Close,Adj Close,Volume
				// 2018-07-02,183.820007,187.300003,183.419998,187.179993,186.525818,17731300
				ATPriceHistory pxh = new ATPriceHistory();
				pxh.setSource(SOURCE_KEY);
				pxh.setAtId(atId);
				pxh.setOccId(sym_.getId());
				pxh.setDateStr(dateStr);
				pxh.setOpen(Double.parseDouble(rec.get(1)));
				pxh.setHigh(Double.parseDouble(rec.get(2)));
				pxh.setLow(Double.parseDouble(rec.get(3)));
				// 4: Unadjusted close
				pxh.setClose(Double.parseDouble(rec.get(5)));
				pxh.setVolume(Long.parseLong(rec.get(6)));
				result.add(pxh);
			} catch (Exception ex) {
				log.error(String.format("Error parsing [%s]: %s", atId, rec), ex);
			}
		}
		return result;
	}

	private long getPeriod() {
		long period = 1000;
		if (_config != null) {
			Integer p = _config.getInt("yPeriod");
			if (p != null) {
				period = p;
			}
		}
		return period;
	}

	static private String getHttp(CloseableHttpClient client_, HttpClientContext ctx_, String url_) {
		try {
			HttpGet httpGet = new HttpGet(url_);
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			CloseableHttpResponse response = client_.execute(httpGet, ctx_);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();
			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent(), "UTF-8"));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			return content;
		} catch (Exception ex) {
			log.error(String.format("Error on [%s]", url_), ex);
			return null;
		}
	}
}
