package com.isoplane.at.gatherer;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.time.DateUtils;
import org.bson.Document;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ATAdapterRegistry;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.gatherer.adapter.ATWikipediaAdapter;
import com.isoplane.at.gatherer.model.ATMongoDividend;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;

//@RunWith(SpringJUnit4ClassRunner.class)
public class ATGathererTest {

	static final Logger log = LoggerFactory.getLogger(ATGathererTest.class);
//	private static CompositeConfiguration _config;
	private static ATMongoDao _db;
	private static ATGathererController _ctrl;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		// String[] propertiesPaths =
		// "C:\\Dev\\workspace\\alphatrade\\source.properties,C:\\Dev\\workspace\\alphatrade\\NOCHECKIN.properties,C:\\Dev\\workspace\\alphatrade\\local.properties"
		// .split(",");
		String testArgs = System.getProperty("test_args");
		// String[] propertiesPaths = testArgs.split(",");
		// _config = new CompositeConfiguration();
		// for (String path : propertiesPaths) {
		// log.info(String.format("Reading properties [%s]", path));
		// PropertiesConfiguration config = new Configurations().properties(new File(path));
		// _config.addConfiguration(config);
		// }
		ATConfigUtil.init(testArgs, false);

		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		ATExecutors.init();
		ATAdapterRegistry.init();
		ATMongoTableRegistry.init();

		_db = new ATMongoDao();
		_ctrl = new ATGathererController();
		// _pfM = new ATPortfolioManagerTestWrapper(_config, true);
		// _pfM.initService();
		//
		// _date_20190601 = LocalDate.of(2019, 06, 01);
	}

	@AfterAll
	public static void tearDown() {
		// _db.delete(TEST_TRADES_TABLE, ATPersistenceRegistry.query().empty());
	}

	@BeforeEach
	public void prepare() throws Exception {
	}

	@AfterEach
	public void cleanup() throws Exception {
	}

	@Test
	@Disabled
	public void testWikipedia() throws Exception {
		ATWikipediaAdapter wiki = new ATWikipediaAdapter();
		wiki.updateSymbols(new HashMap<>());
	}

	@Test
	@Disabled
	public void testGetDividends() {
		String[] symbols = { "TAP", "MCD", "ABBV" };
		Map<String, IATDividend> dm = _ctrl.getDividends(Arrays.asList(symbols), true);
		assertNotNull(dm);
		assertEquals(symbols.length, dm.size());
		log.info(String.format("Dividends: %s", dm));
	}

	@Test
	@Disabled
	public void fixDividends() {
		String table = "dividends_bak";
		List<ATMongoDividend> divs = _db.query(table, ATPersistenceRegistry.query().empty(), ATMongoDividend.class);
		List<Document> update = new ArrayList<>();
		divs.forEach(d -> {
			String atId = d.getAtId();
			atId = atId.replace("-", "");
			d.setAtId(atId);
			update.add(new Document(d));
		});
		long result = _db.fastBulkInsert(table, update);
		log.info("hello " + result);
	}

	@Test
	// @Disabled
	public void testStatistics() {
		String minDS8 = ATFormats.DATE_yyyyMMdd.get().format(DateUtils.addYears(new Date(), -2));
		int count = _ctrl.processStatistics("AAPL", minDS8);
		assertEquals(count, 1);
	}

}
