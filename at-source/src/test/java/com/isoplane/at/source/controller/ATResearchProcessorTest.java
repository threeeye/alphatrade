package com.isoplane.at.source.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NavigableSet;
import java.util.TreeSet;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

//@RunWith(SpringJUnit4ClassRunner.class)
public class ATResearchProcessorTest {

	static final Logger log = LoggerFactory.getLogger(ATResearchProcessorTest.class);

	static private Gson _gson;

	@BeforeAll
	public static void setUpBeforeClass() throws Exception {
		_gson = new Gson();
	}

	@AfterAll
	public static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	public void setUp() throws Exception {
	}

	@AfterEach
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws Exception {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
		TreeSet<Integer> setOrig = new TreeSet<>(list);
		NavigableSet<Integer> setRev = setOrig.descendingSet();

		log.info(String.format("Orig     : %s", setOrig));
		log.info(String.format("Orig tail: %s", setOrig.tailSet(5, false)));
		log.info(String.format("Orig hi  : %s", setOrig.higher(5)));
		log.info(String.format("Desc     : %s", setRev));
		log.info(String.format("Desc tail: %s", setRev.tailSet(5, false)));
		log.info(String.format("Desc hi  : %s", setRev.higher(5)));

		List<Integer[]> pairsOrig = detectPairs(setOrig, 1, 3);
		log.info(String.format("Orig pair: %s", _gson.toJson(pairsOrig)));
		List<Integer[]> pairsRev = detectPairs(setRev, 1, 3);
		log.info(String.format("Desc pair: %s", _gson.toJson(pairsRev)));
	}

	private <T> List<T[]> detectPairs(NavigableSet<T> elements_, Integer minSteps_, int maxSteps_) {
		int minSteps = minSteps_ == null ? 1 : Math.max(1, minSteps_);
		List<T[]> list = new ArrayList<>();
		for (int steps = minSteps; steps <= maxSteps_; steps++) {
			NavigableSet<T> elements = elements_;
			topLoop: while (!elements.isEmpty()) {
				T first = elements.first();
				T second = first;
				for (int step = 0; step < steps; step++) {
					if (second == null)
						break;
					second = elements.higher(second);
				}
				elements = elements.tailSet(first, false);
				if (second == null)
					continue topLoop;

				@SuppressWarnings("unchecked")
				T[] array = (T[]) new Object[] { first, second };
				list.add(array);
			}
		}
		return list;
	}

}
