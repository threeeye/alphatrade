package com.isoplane.at.source.controller.process;

import org.apache.http.annotation.Obsolete;

@Obsolete
public class ATEarningsProcess extends ATProcess {

	@Override
	protected int getPriority() {
		return 400;
	}

	@Override
	public boolean equals(Object obj_) {
		return (obj_ instanceof ATEarningsProcess);
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}
	
	@Override
	public String toString() {
		String str = String.format("%s", this.getClass().getSimpleName());
		return str;
	}

	@Override
	protected int compareToInstance(ATProcess process) {
		return 0;
	}

}
