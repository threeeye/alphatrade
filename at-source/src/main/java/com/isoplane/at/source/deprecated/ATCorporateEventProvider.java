package com.isoplane.at.source.deprecated;

import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.store.ATEarning;
import com.isoplane.at.mongodb.IATPersistentLookupStore;

@Deprecated
public class ATCorporateEventProvider implements IATCorporateEventProvider {

	static final Logger log = LoggerFactory.getLogger(ATCorporateEventProvider.class);

	private IATConfiguration _config;
	private IATPersistentLookupStore _dao;
//	private Timer _timer;
	private TreeMap<String, ATEarning> _latestEarningsMap;
	private TreeMap<String, ATDividend> _latestDividendsMap;

	public ATCorporateEventProvider(IATConfiguration sourceConfig_, IATPersistentLookupStore store_) {
		_dao = store_;
		_config = sourceConfig_;
	}

	@Override
	public void initService() {
		updateEarnings();
		updateDividends();
	}

//	private void setupHousekeeping() {
//		try {
//			long period = 6 * 60 * 60 * 1000; // 6h
//			log.info(String.format("Setting up housekeeping with period [%d]", period));
//			_timer = new Timer("EarningsHousekeeping", false);
//			_timer.scheduleAtFixedRate(new TimerTask() {
//
//				@Override
//				public void run() {
//					updateEarnings();
//					updateDividends();
//				}
//			}, 5000, period);
//		} catch (Exception ex) {
//			log.error(String.format("Error initializing [%s]", ATCorporateEventProvider.class.getSimpleName()));
//			throw new ATException(ex);
//		}
//	}

	private void updateEarnings() {
		try {
			String earningsTableId = _config.getString("earningsTableId");
			TreeMap<String, ATEarning> earningsMap = _dao.groupMax(earningsTableId, IATAssetConstants.OCCID, IATAssetConstants.DATE_STR,
					ATEarning.class);
			if (earningsMap == null || earningsMap.isEmpty())
				return;
			_latestEarningsMap = earningsMap;
		} catch (Exception ex) {
			log.error("Error updating earnings", ex);
		}
	}

	private void updateDividends() {
		try {
			String earningsTableId = _config.getString("dividendTableId");
			TreeMap<String, ATDividend> dividendsMap = null;// _dao.groupMax(earningsTableId, IATAssetConstants.OCCID, IATAssetConstants.DIV_EX_DATE_STR_OLD, ATDividend.class);
			if (dividendsMap == null || dividendsMap.isEmpty())
				return;
			_latestDividendsMap = dividendsMap;
		} catch (Exception ex) {
			log.error("Error updating dividends", ex);
		}
	}

	@Override
	public ATEarning getNextEarnings(String sym_) {
		if (_latestEarningsMap == null) {
			updateEarnings();
		}
		if (_latestEarningsMap == null) {
			log.error(String.format("Earnings unavailable"));
			return null;
		}
		ATEarning result = _latestEarningsMap.get(sym_);
		return result;
	}

	@Override
	public ATDividend getNextDividend(String sym_) {
		if (_latestDividendsMap == null) {
			updateDividends();
		}
		if (_latestDividendsMap == null) {
			log.error(String.format("Dividends unavailable"));
			return null;
		}
		ATDividend result = _latestDividendsMap.get(sym_);
		return result;
	}

}
