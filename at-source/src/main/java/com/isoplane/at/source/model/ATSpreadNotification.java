package com.isoplane.at.source.model;

import java.util.Date;
import java.util.Map;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.util.ATFormats;

public class ATSpreadNotification extends ATBaseNotification {

	static public final String META_SPREAD = "spread";

	
	private Double strikeBuy;
	private Double strikeSell;
	private Date expirationDate;
	// private String buyTicker;
	// private String sellTicker;
	private Double apy;
	private Double absolute;
	private Double stockPrice;
	private Double probability;

	@Override
	public String getNotificationKey() {
		String key = String.format("spread %s%s%.2f%.2f", getLabel(), ATFormats.DATE_MMdd.get().format(getExpirationDate()), getStrikeBuy(),
				getStrikeSell());
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATSpreadNotification oldSpread = oldB != null && oldB instanceof ATSpreadNotification ? (ATSpreadNotification) oldB : null;
		// String arrowStr = "✧"; // ▷◇⋄
		// SimpleDateFormat dateFmt = new SimpleDateFormat("MM•dd");
		String dirStr = "✧";
		if (oldSpread != null) {
			double change = (oldSpread.getApy() - this.getApy()) / oldSpread.getApy();
			if (Math.abs(change) < 0.25) {
				return null;
			}
			dirStr = getStockPrice() > oldSpread.getStockPrice() ? "◥" : "◢";
		}
		// "➚" : "➘"; // △▽◥◢➜✧▷◇⋄⋗⋇⚡⛊▷♲♺♻↻⇅⇕⭥⇅
		String title = String.format("%s %s %s %.2f (%.2f%%)", getLabel(), ATFormats.DATE_MMdd.get().format(getExpirationDate()), dirStr,
				getStockPrice(),
				getProbability());
		String body = String.format("%.2f/%.2f ➜ %.2f (%.0f%%)", getStrikeBuy(), getStrikeSell(), getAbsolute(), this.getApy());
		// String body = String.format("%.2f/%.2f ➜ %.2f (%.0f%%) \n· alphatra.de · %tT", getStrikeBuy(), getStrikeSell(), getAbsolute(),
		// this.getApy(), new Date()); // ◇
		String star = isActive() ? "star_" : "";
		String icon = String.format("/globe_%sspread.ico", star);

		return new ATNotificationContent(title, body, icon);
	}

	public Double getStrikeBuy() {
		return strikeBuy;
	}

	public void setStrikeBuy(Double value) {
		this.strikeBuy = value;
	}

	public Double getApy() {
		return apy;
	}

	public void setApy(Double stockPrice) {
		this.apy = stockPrice;
	}

	public Double getAbsolute() {
		return absolute;
	}

	public void setAbsolute(Double margin) {
		this.absolute = margin;
	}

	public Double getStrikeSell() {
		return strikeSell;
	}

	public void setStrikeSell(Double value) {
		this.strikeSell = value;
	}

	@Override
	public String toString() {
		String str = String.format("Strike{%s %.2f (%.2f)}", getLabel(), getApy(), getAbsolute());
		return str;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public Double getProbability() {
		return probability;
	}

	public void setProbability(Double probability) {
		this.probability = probability;
	}

	@Override
	public String getNotificationTag() {
		String dStr = ATFormats.DATE_yyMMdd.get().format(getDate());
		return String.format("strike_%s_%s_%.2f %.2f", getLabel(), dStr, getStrikeBuy(), getStrikeSell());
	}

	@Override
	public String getType() {
		return "spread";
	}

}
