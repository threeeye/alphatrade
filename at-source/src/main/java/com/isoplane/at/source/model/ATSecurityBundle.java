package com.isoplane.at.source.model;

import java.io.Serializable;
import java.util.Collection;

import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATStock;

public class ATSecurityBundle implements Serializable {

	private static final long serialVersionUID = 1L;

	private ATStock stock;
	private Collection<ATOptionSecurity> options;

	public ATStock getStock() {
		return stock;
	}

	public void setStock(ATStock stock) {
		this.stock = stock;
	}

	public Collection<ATOptionSecurity> getOptions() {
		return options;
	}

	public void setOptions(Collection<ATOptionSecurity> options) {
		this.options = options;
	}

}
