package com.isoplane.at.source.adapter;

import com.isoplane.at.source.controller.IATAlertProcessor;

public interface IATAlertAdapter {

	void register(IATAlertProcessor provider);
}
