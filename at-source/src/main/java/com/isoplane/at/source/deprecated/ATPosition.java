package com.isoplane.at.source.deprecated;

import java.util.Date;

import com.isoplane.at.commons.model.ATSecurity;

@Deprecated
public class ATPosition {

	private String id;
	private int index;
	private Date tradeDate;
	private Double tradeTotal;
	private Double count;
	private Double price;
	private int tradeType;
	private String broker;
	private Date dateEarnings;
	private Date dateDividend;
	private Double margin;
	private Double risk;
	private Double value;
	private Double changePct;
	// private String ticker;

	private ATSecurity security;

	public ATSecurity getSecurity() {
		return security;
	}

	public void setSecurity(ATSecurity security) {
		this.security = security;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getTradeDate() {
		return tradeDate;
	}

	public void setTradeDate(Date date) {
		this.tradeDate = date;
	}

	public Double getCount() {
		return count;
	}

	public void setCount(Double count) {
		this.count = count;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public int getTradeType() {
		return tradeType;
	}

	public void setTradeType(int tradeType) {
		this.tradeType = tradeType;
	}

	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public String toString() {
		String str = getSecurity() != null ? getSecurity().toString() : null;
		return str;
	}

	public Date getDateEarnings() {
		return dateEarnings;
	}

	public void setDateEarnings(Date dateEarnings) {
		this.dateEarnings = dateEarnings;
	}

	public Date getDateDividend() {
		return dateDividend;
	}

	public void setDateDividend(Date datePosition) {
		this.dateDividend = datePosition;
	}

	public Double getMargin() {
		return margin;
	}

	public void setMargin(Double margin) {
		this.margin = margin;
	}

	public Double getRisk() {
		return risk;
	}

	public void setRisk(Double risk) {
		this.risk = risk;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Double getChangePct() {
		return changePct;
	}

	public void setChangePct(Double changePct) {
		this.changePct = changePct;
	}

	public Double getTradeTotal() {
		return tradeTotal;
	}

	public void setTradeTotal(Double trateTotal) {
		this.tradeTotal = trateTotal;
	}

	// public String getTicker() {
	// return ticker;
	// }
	// public void setTicker(String ticker) {
	// this.ticker = ticker;
	// }

}
