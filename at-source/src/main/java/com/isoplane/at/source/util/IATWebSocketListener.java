package com.isoplane.at.source.util;

import com.isoplane.at.commons.model.WSMessageWrapper;

public interface IATWebSocketListener {

	void notify(WSMessageWrapper message);
}
