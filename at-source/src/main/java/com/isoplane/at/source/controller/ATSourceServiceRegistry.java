package com.isoplane.at.source.controller;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Timer;
import java.util.TimerTask;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.service.IATUSTreasuryRateProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.source.deprecated.IATPositionProcessor;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATSourceServiceRegistry extends ATServiceRegistry {

	static final Logger log = LoggerFactory.getLogger(ATSourceServiceRegistry.class);

	// static private IATConfiguration _config;
	static private IATAlertProcessor _alertProcessor;
//	static private IATCorporateEventProvider _corporateEventProvider;
	static private IATEquityFoundationProvider _foundationProvider;
	// static private IATNotesProcessor _notesProcessor;
	static private IATNotificationProcessor _notificationProcessor;
	static private IATPositionProcessor _positionProcessor;
	static private IATSecuritiesProvider _securitiesProvider;
	static private IATSettingsProvider _settingsProvider;
//	static private IATSpreadProcessor _spreadProcessor;
	static private IATSymbolsProvider _symbolsProvider;
	// static private IATSymbolProvider _symbolProvider;
	static private IATUSTreasuryRateProvider _usTreasuryRateProvider;

	static private IATOpportunityProcessor _opportunityProcessor;
	// static private IATPortfolioService _portfolioService;

	// static private Set<IATService> _refreshServices = new HashSet<>();

	// static private Set<IATEventSubscriber> _eventSubscribers = new HashSet<>();

	public static void init() {
		ATServiceRegistry.init();
		Configuration config = ATConfigUtil.config();
		try {
			_refreshServices.clear();
			String timeStr = config.getString("serviceRegistry.refreshTime");
			LocalTime time = LocalTime.parse(timeStr);
			long delay = LocalTime.now().until(time, ChronoUnit.MILLIS);
			final long period = 1000L * 60L * 60L * 24L;
			// _eventSubscribers.clear();
			if (delay < 0) {
				delay += period;
			}
			Timer t = new Timer(ATSourceServiceRegistry.class.getSimpleName());
			t.schedule(new TimerTask() {

				@Override
				public void run() {
					log.info(String.format("Resetting services..."));
					for (IATService iatService : _refreshServices) {
						String sname = iatService.getClass().getSimpleName();
						try {
							log.info(String.format("Resetting service [%s]", sname));
							iatService.initService();
						} catch (Exception ex_) {
							log.error(String.format("Error resetting service [%s]", sname), ex_);
						}
					}
				}
			}, delay, period);
			log.info(String.format("Service registry initialized with daily refresh [%s]", timeStr));
		} catch (Exception ex_) {
			log.error(String.format("Failure initializing Service Resgistry"), ex_);
		}
	}

	// public static void subscribeEvents(IATEventSubscriber subscriber_) {
	// if (subscriber_ != null) {
	// _eventSubscribers.add(subscriber_);
	// }
	// }
	//
	// public static void notifyEventSubscribers(IATEvent event_) {
	// for (IATEventSubscriber sub : _eventSubscribers) {
	// try {
	// sub.notify(event_);
	// } catch (Exception ex_) {
	// log.error(String.format("Error notifying [%s]", sub), ex_);
	// }
	// }
	// }

	public static void setEquityFoundationProvider(IATEquityFoundationProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_foundationProvider = provider_;
		log.info(String.format("Registering equity foundation provider [%s]", _foundationProvider.getClass().getSimpleName()));
	}

	public static IATEquityFoundationProvider getEquityFoundationProvider() {
		if (_foundationProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATEquityFoundationProvider.class.getSimpleName()));
		}
		return _foundationProvider;
	}

	@Deprecated
	public static void setSymbolsProvider(IATSymbolsProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_symbolsProvider = provider_;
		log.info(String.format("Registering symbols provider [%s]", _symbolsProvider.getClass().getSimpleName()));
	}

	@Deprecated
	public static IATSymbolsProvider getSymbolsProvider() {
		if (_symbolsProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATSymbolProvider.class.getSimpleName()));
		}
		return _symbolsProvider;
	}

	public static void setNotificationProcessor(IATNotificationProcessor processor_, boolean isRefresh_) {
		initService(processor_, isRefresh_);
		_notificationProcessor = processor_;
		log.info(String.format("Registering notification processor [%s]", _notificationProcessor.getClass().getSimpleName()));
	}

	public static IATNotificationProcessor getNotificationProcessor() {
		if (_notificationProcessor == null) {
			throw new ATException(String.format("Missing [%s]", IATNotificationProcessor.class.getSimpleName()));
		}
		return _notificationProcessor;
	}

	//@Deprecated
	// public static void setPositionProcessor(IATPositionProcessor processor_, boolean isRefresh_) {
	// 	initService(processor_, isRefresh_);
	// 	ATSourceServiceRegistry._positionProcessor = processor_;
	// 	log.info(String.format("Registering position processor [%s]", _positionProcessor.getClass().getSimpleName()));
	// }

	@Deprecated
	public static IATPositionProcessor getPositionProcessor() {
		if (_positionProcessor == null) {
			throw new ATException(String.format("Missing [%s]", IATPositionProcessor.class.getSimpleName()));
		}
		return _positionProcessor;
	}

	public static IATSecuritiesProvider getSecuritiesProvider() {
		if (_securitiesProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATSecuritiesProvider.class.getSimpleName()));
		}
		return _securitiesProvider;
	}

	public static void setSecuritiesProvider(IATSecuritiesProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		ATSourceServiceRegistry._securitiesProvider = provider_;
		log.info(String.format("Registering securities provider [%s]", _securitiesProvider.getClass().getSimpleName()));
	}

	// public static IATSpreadProcessor getSpreadProcessor() {
	// 	if (_spreadProcessor == null) {
	// 		throw new ATException(String.format("Missing [%s]", IATSpreadProcessor.class.getSimpleName()));
	// 	}
	// 	return _spreadProcessor;
	// }

	// public static void setSpreadProcessor(IATSpreadProcessor processor_, boolean isRefresh_) {
	// 	initService(processor_, isRefresh_);
	// 	ATSourceServiceRegistry._spreadProcessor = processor_;
	// 	log.info(String.format("Registering spread processor [%s]", _spreadProcessor.getClass().getSimpleName()));
	// }

	// public static void setCorporateEventProvider(IATCorporateEventProvider provider_, boolean isRefresh_) {
	// 	initService(provider_, isRefresh_);
	// 	_corporateEventProvider = provider_;
	// 	log.info(String.format("Registering corporate event provider [%s]", _corporateEventProvider.getClass().getSimpleName()));
	// }

	// public static IATCorporateEventProvider getCorporateEventProvider() {
	// 	if (_corporateEventProvider == null) {
	// 		throw new ATException(String.format("Missing [%s]", IATCorporateEventProvider.class.getSimpleName()));
	// 	}
	// 	return _corporateEventProvider;
	// }

	public static void setUSTreasuryRateProvider(IATUSTreasuryRateProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_usTreasuryRateProvider = provider_;
		log.info(String.format("Registering US Treasury Rate provider [%s]", _usTreasuryRateProvider.getClass().getSimpleName()));
	}

	public static IATUSTreasuryRateProvider getUSTreasuryRateProvider() {
		if (_usTreasuryRateProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATUSTreasuryRateProvider.class.getSimpleName()));
		}
		return _usTreasuryRateProvider;
	}

	public static void setSettingsProvider(IATSettingsProvider provider_, boolean isRefresh_) {
		initService(provider_, isRefresh_);
		_settingsProvider = provider_;
		log.info(String.format("Registering Settings provider [%s]", _settingsProvider.getClass().getSimpleName()));
	}

	public static IATSettingsProvider getSettingsProvider() {
		if (_settingsProvider == null) {
			throw new ATException(String.format("Missing [%s]", IATSettingsProvider.class.getSimpleName()));
		}
		return _settingsProvider;
	}

	public static void setAlertProcessor(IATAlertProcessor processor_, boolean isRefresh_) {
		initService(processor_, isRefresh_);
		_alertProcessor = processor_;
		log.info(String.format("Registering alert processor [%s]", _alertProcessor.getClass().getSimpleName()));
	}

	public static IATAlertProcessor getAlertProcessor() {
		if (_alertProcessor == null) {
			throw new ATException(String.format("Missing [%s]", IATAlertProcessor.class.getSimpleName()));
		}
		return _alertProcessor;
	}

	public static void setOpportunityProcessor(IATOpportunityProcessor processor_, boolean isRefresh_) {
		initService(processor_, isRefresh_);
		_opportunityProcessor = processor_;
		log.info(String.format("Registering opportunity processor [%s]", _opportunityProcessor.getClass().getSimpleName()));
	}

	public static IATOpportunityProcessor getOpportunityProcessor() {
		if (_opportunityProcessor == null) {
			throw new ATException(String.format("Missing [%s]", IATOpportunityProcessor.class.getSimpleName()));
		}
		return _opportunityProcessor;
	}

	// public static void setUserProvider(IATUserProvider provider_, boolean isRefresh_) {
	// initService(provider_, isRefresh_);
	// _userProvider = provider_;
	// log.info(String.format("Registering user provider [%s]", _userProvider.getClass().getSimpleName()));
	// }
	//
	// public static IATUserProvider getUserProvider() {
	// if (_userProvider == null) {
	// throw new ATException(String.format("Missing [%s]", IATUserProvider.class.getSimpleName()));
	// }
	// return _userProvider;
	// }

	// public static void setNotesProcessor(IATNotesProcessor processor_, boolean isRefresh_) {
	// initService(processor_, isRefresh_);
	// _notesProcessor = processor_;
	// log.info(String.format("Registering notes processor [%s]", _notesProcessor.getClass().getSimpleName()));
	// }
	//
	// public static IATNotesProcessor getNotesProcessor() {
	// if (_notesProcessor == null) {
	// throw new ATException(String.format("Missing [%s]", IATNotesProcessor.class.getSimpleName()));
	// }
	// return _notesProcessor;
	// }

	// public static void setPortfolioService(IATPortfolioService service_, boolean isRefresh_) {
	// initService(service_, isRefresh_);
	// _portfolioService = service_;
	// log.info(String.format("Registering portfolio manager [%s]", _portfolioService.getClass().getSimpleName()));
	// }
	//
	// public static IATPortfolioService getPortfolioService() {
	// if (_portfolioService == null) {
	// throw new ATException(String.format("Missing [%s]", IATPortfolioService.class.getSimpleName()));
	// }
	// return _portfolioService;
	// }

}
