package com.isoplane.at.source.model;

import java.util.Date;

import org.apache.http.annotation.Obsolete;

import com.isoplane.at.commons.model.ATSecEvent;

@Obsolete
public class ATEarningOld extends ATSecEvent implements Comparable<ATEarningOld> {

	private String event;

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	private Date date;

	@Override
	public int compareTo(ATEarningOld other_) {
		Date otherDate = other_ != null ? other_.getDate() : null;
		Date thisDate = getDate();
		if (thisDate == null) {
			return otherDate == null ? 0 : -1;
		}
		int result = thisDate.compareTo(otherDate);
		return result;
	}

	@Override
	public String toString() {
		String str = String.format("ATEarnings{\"%tF\" : %s}", getDate(), getEvent());
		return str;
	}
}
