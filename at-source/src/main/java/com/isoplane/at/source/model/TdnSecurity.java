package com.isoplane.at.source.model;

import java.util.List;

// Note: Not a derivative but 'positions' may contain derivatives.
public class TdnSecurity {

	public String id;
	public String name;
	public String description;
	public Long ts;

	public Double px;
	public Double pxChg;
	public Double pxOpen;
	public Double pxClose;
	public Double pxAsk;
	public Double pxBid;
	public Double pxLoD;
	public Double pxHiD;
	public Double pxAvg;
	// public String pxAvgMode;
	public Double pxHV;
	// public String pxHVMode;
	// public String note;
	public Double mktCap;
	public Double owned;
	public String dateEarn;
	public String dateDiv;
	public Double divAmt;
	public String industry;
	public String sector;

	public List<TdnPosition> positions;
}