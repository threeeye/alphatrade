package com.isoplane.at.source.controller;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOpportunity2;
import com.isoplane.at.commons.model.ATOpportunity2.ATOpportunitySegment;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATModelUtil;

public class ATOpportunityProcessor1 implements IATOpportunityProcessor {

	static final Logger log = LoggerFactory.getLogger(ATOpportunityProcessor1.class);

	static final ZoneId ZONE_ID = ZoneId.systemDefault();

	static final DateTimeFormatter FMT_DS6 = DateTimeFormatter.ofPattern("yyMMdd", Locale.ENGLISH);
	static final DateTimeFormatter FMT_DS10 = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);

	@Override
	public void initService() {
		// TODO Auto-generated method stub

	}

	private TreeMap<Integer, Double> getDividendMap(ATPersistentEquityFoundation foundation_, Double divOverride_) {
		if (divOverride_ != null) {
			return new TreeMap<>(Collections.singletonMap(0, divOverride_));
		}
		if (foundation_ == null)
			return null;
		try {
			Date divTemp = foundation_.getDividendExDate(true);
			TreeMap<Integer, Double> divMap = null;
			boolean isDiv = divTemp != null;
			if (isDiv) {
				Instant divInstant = Instant.ofEpochMilli(divTemp.getTime()).truncatedTo(ChronoUnit.DAYS);
				LocalDate divEx = divInstant.atZone(ZONE_ID).toLocalDate();
				Double div = foundation_.getDividend();
				Integer divFreqYear = foundation_.getDividendFrequency();
				if (divFreqYear == null || divFreqYear == 0) {
					log.warn(String.format("getDividendMap [%s] missing Div Frq. Defaulting to '4'", foundation_.getOccId()));
					divFreqYear = 4;
				}
				Integer divFreqDays = divFreqYear != null ? (12 / divFreqYear) * 30 : null;
				isDiv = div != null && divFreqDays != null;
				if (isDiv) {
					LocalDate now = LocalDate.now();
					divMap = new TreeMap<>();
					int divDays = (int) ChronoUnit.DAYS.between(now, divEx);
					double divCurrent = div;
					for (int i = 0; i < 24; i++) {
						divMap.put(divDays, divCurrent);
						divDays += divFreqDays;
						divCurrent += div;
					}
				}
			}
			return divMap;
		} catch (Exception ex) {
			log.error(String.format("getDividendMap Error [%s]", foundation_.getOccId()), ex);
			return null;
		}
	}

	private StrikeConfig getStrikeConfig(String strategy_, ATUserOpportunityConfig config_, Double pxUl_, ATPersistentEquityFoundation fnd_) {
		if (config_ == null || pxUl_ == null)
			return null;

		// boolean isBelow = ATOpportunity2
		Double strikeMinPct = null;
		Double strikeMaxPct = null;
		TreeMap<Integer, Double> hvMap = null;

		String strikeStrat = config_.getStrikeStrategy();
		if (StringUtils.isBlank(strikeStrat))
			strikeStrat = ATUserOpportunityConfig.STRAT_USER;
		Double strikeMax = config_.getStrikeMax();
		if (strikeMax == null)
			strikeMax = Double.MAX_VALUE;
		Double strikeMin = config_.getStrikeMin();
		if (strikeMin == null)
			strikeMin = 0d;
		switch (strikeStrat) {
		case ATUserOpportunityConfig.STRAT_USER:
			strikeMinPct = ATOpportunity2.isSafeBelowLast(strategy_) ? 100.0 - strikeMax : 100 + strikeMin;
			strikeMaxPct = ATOpportunity2.isSafeBelowLast(strategy_) ? 100.0 - strikeMin : 100 + strikeMax;
			break;
		case ATUserOpportunityConfig.STRAT_ATM:
			strikeMinPct = 95.0;
			strikeMaxPct = 105.0;
			break;
		case ATUserOpportunityConfig.STRAT_ITM:
			strikeMinPct = ATOpportunity2.isSafeBelowLast(strategy_) ? 100.0 : Double.MIN_VALUE;
			strikeMaxPct = ATOpportunity2.isSafeBelowLast(strategy_) ? Double.MAX_VALUE : 100.0;
			break;
		case ATUserOpportunityConfig.STRAT_OTM:
			strikeMinPct = ATOpportunity2.isSafeBelowLast(strategy_) ? Double.MIN_VALUE : 100.0;
			strikeMaxPct = ATOpportunity2.isSafeBelowLast(strategy_) ? 100.0 : Double.MAX_VALUE;
			break;
		case ATUserOpportunityConfig.STRAT_HV:
			hvMap = fnd_ != null ? fnd_.getVolatilityMap() : null;
			if (hvMap == null || hvMap.isEmpty())
				return null;
			for (Entry<Integer, Double> entry : hvMap.entrySet()) {
				entry.setValue(entry.getValue() / ATPriceHistory.ANNUALIZED_FACTOR);
			}
			break;
		case ATUserOpportunityConfig.STRAT_VLT:
			Double vlt = config_.getVolatilityOverride();
			if (vlt == null) {
				log.error(String.format("Missing volatility"));
				return null;
			}
			Double std = pxUl_ * vlt / (100 * ATPriceHistory.ANNUALIZED_FACTOR);
			hvMap = new TreeMap<>(Collections.singletonMap(0, std));
			break;
		default:
			log.error(String.format("getStrikes Unknown Moneyness strategy [%s]", strikeStrat));
			return null;
		}
		if ((strikeMinPct == null || strikeMaxPct == null) && hvMap == null) {
			log.error(String.format("getStrikes Unable to calculate strikes [%.2f:%.2f]", strikeMinPct, strikeMaxPct));
			return null;
		}

		Double strikeMinAbs = strikeMinPct != null ? pxUl_ * strikeMinPct / 100 : null;
		Double strikeMaxAbs = strikeMaxPct != null ? pxUl_ * strikeMaxPct / 100 : null;

		StrikeConfig result = new StrikeConfig(strikeMinAbs, strikeMaxAbs, hvMap);
		return result;
	}

	@Override
	public ATSecurityPack analyzeVCS(ATMarketData ul_, Map<String, ATMarketData> marketMap_, TreeMap<String, TreeSet<String>> callChain_,
			ATUserOpportunityConfig config_) {
		return analyze(ATOpportunity2.VCS, ul_, marketMap_, callChain_, config_);
	}

	@Override
	public ATSecurityPack analyzeVPS(ATMarketData ul_, Map<String, ATMarketData> marketMap_, TreeMap<String, TreeSet<String>> putChain_,
			ATUserOpportunityConfig config_) {
		return analyze(ATOpportunity2.VPS, ul_, marketMap_, putChain_, config_);
	}

	@Override
	public ATSecurityPack analyzeCC(ATMarketData ul_, Map<String, ATMarketData> marketMap_, TreeMap<String, TreeSet<String>> callChain_,
			ATUserOpportunityConfig config_) {
		return analyze(ATOpportunity2.CC, ul_, marketMap_, callChain_, config_);
	}

	private ATSecurityPack analyze(String strategy_, ATMarketData ul_, Map<String, ATMarketData> inMarketMap_,
			TreeMap<String, TreeSet<String>> optionChain_,
			ATUserOpportunityConfig config_) {
		if (ul_ == null || inMarketMap_ == null || inMarketMap_.isEmpty() || optionChain_ == null || optionChain_.isEmpty() || config_ == null)
			return null;

		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;

		try {
			LocalDate now = LocalDate.now();
			Map<String, ATMarketData> outMktMap = new TreeMap<>(Collections.singletonMap(ul_.getOccId(), ul_));

			ATPersistentEquityFoundation foundation = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(ul_.getOccId());

			Double overrideDiv = config_.getDividendOverride();
			TreeMap<Integer, Double> divMap = getDividendMap(foundation, overrideDiv);
			double pxUl = (ul_.getPxAsk() + ul_.getPxBid()) / 2;
			StrikeConfig strikeConfig = getStrikeConfig(strategy_, config_, pxUl, foundation);
			if (strikeConfig == null) {
				return null;
			}

			Integer daysMin = config_.getDaysMin();
			Integer daysMax = config_.getDaysMax();
			Double spreadMin = config_.getSpreadMin();
			if (spreadMin == 0)
				spreadMin = 0d;
			Double spreadMax = config_.getSpreadMax();
			if (spreadMax == null)
				spreadMax = Double.MAX_VALUE;
			Double yieldMinAbs = config_.getYieldMinAbs();
			if (yieldMinAbs == null)
				yieldMinAbs = 0d;
			Double yieldMinPct = config_.getYieldMinPct();
			if (yieldMinPct == null)
				yieldMinPct = 0d;
			Integer dataAge = config_.getDataAge();
			if (dataAge == null)
				dataAge = 4320; // 3 days in h

			List<ATOpportunity2> opportunities = new ArrayList<>();
			for (String expDS10 : optionChain_.keySet()) {
				LocalDate dExp = LocalDate.parse(expDS10, FMT_DS10);
				int expDays = (int) ChronoUnit.DAYS.between(now, dExp);
				if ((daysMin != null && expDays < daysMin) || (daysMax != null && expDays > daysMax))
					continue;
				strikeConfig.updateStrikes(strategy_, pxUl, expDays);
				Map.Entry<Integer, Double> expDivEntry = divMap != null ? divMap.floorEntry(expDays) : null;
				ATOpportunityInternalConfig intConfig = new ATOpportunityInternalConfig(
						ul_.getOccId(), pxUl,
						dataAge, expDays, expDivEntry,
						spreadMin, spreadMax,
						strikeConfig.strikeMin, strikeConfig.strikeMax,
						yieldMinAbs, yieldMinPct);
				Set<String> occIds = optionChain_.get(expDS10);
				switch (strategy_) {
				case ATOpportunity2.CC:
					analyzeCC(occIds, intConfig, inMarketMap_, outMktMap, opportunities);
					break;
				case ATOpportunity2.VCS:
				case ATOpportunity2.VPS:
					analyzeVerticalSpread(strategy_, occIds, intConfig, inMarketMap_, outMktMap, opportunities);
					break;
				default:
					throw new ATException(String.format("analyze Unsupported strategy[%s]", strategy_));
				}
			}

			if (!opportunities.isEmpty()) {
				opportunities.sort(new Comparator<ATOpportunity2>() {
					@Override
					public int compare(ATOpportunity2 a_, ATOpportunity2 b_) {
						return Double.compare(b_.getYieldPct(), a_.getYieldPct());
						// return Double.compare(b_.getYieldAbs(), a_.getYieldAbs());
					}
				});

				Integer oppCount = config_.getOpportunityCount();
				if (oppCount == null)
					oppCount = 5;
				if (opportunities.size() > oppCount) {
					opportunities = opportunities.subList(0, oppCount);
				}
				String symbol = ul_.getOccId();
				ATFundamental fnd = new ATFundamental(foundation, avgMode, hvMode);
				Map<String, ATFundamental> fndMap = new TreeMap<>(Collections.singletonMap(symbol, fnd));
				ATEquityDescription description = new ATEquityDescription(foundation);
				Map<String, ATEquityDescription> dscMap = new TreeMap<>(Collections.singletonMap(symbol, description));
				Map<String, List<ATOpportunity2>> oppMap = new TreeMap<>(Collections.singletonMap(strategy_, opportunities));
				Map<String, Map<String, List<ATOpportunity2>>> symOppMap = new TreeMap<>(Collections.singletonMap(symbol, oppMap));
				ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, outMktMap);
				pack.setOpportunities(symOppMap);
				return pack;
			} else {
				return null;
			}

		} catch (Exception ex) {
			log.error(String.format("analyzeCC Error"), ex);
			return null;
		}
	}

	// public ATSecurityPack analyze(String strategy_, ATMarketData ul_, Map<String, Set<ATMarketData>> optionMap_, ATUserOpportunityConfig config_) {
	// if (ul_ == null || optionMap_ == null || optionMap_.isEmpty() || config_ == null)
	// return null;
	//
	// final String avgMode = IATStatistics.SMA60;
	// final String hvMode = IATStatistics.HV60;
	//
	// try {
	// LocalDate now = LocalDate.now();
	// Map<String, ATMarketData> mktMap = new TreeMap<>(Collections.singletonMap(ul_.getOccId(), ul_));
	//
	// ATPersistentEquityFoundation foundation = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(ul_.getOccId());
	// TreeMap<Integer, Double> divMap = getDividendMap(foundation, config_.overrideDiv);
	// double pxUl = (ul_.getPxAsk() + ul_.getPxBid()) / 2;
	// StrikeConfig strikeConfig = getStrikeConfig(strategy_, config_, pxUl, foundation);
	// if (strikeConfig == null) {
	// return null;
	// }
	//
	// List<ATOpportunity2> opportunities = new ArrayList<>();
	// for (String expDS6 : optionMap_.keySet()) {
	// if (expDS6.length() > 6) {
	// log.error(String.format("analyzeVerticalSpread Error Invalid expDS6 [%s/%s]", ul_.getAtId(), expDS6));
	// continue;
	// }
	// LocalDate dExp = LocalDate.parse(expDS6, FMT_DS6);
	// int expDays = (int) ChronoUnit.DAYS.between(now, dExp);
	// Integer daysMin = config_.getDaysMin();
	// Integer daysMax = config_.getDaysMax();
	// if ((daysMin != null && expDays < daysMin) || (daysMax != null && expDays > daysMax))
	// continue;
	// strikeConfig.updateStrikes(strategy_, pxUl, expDays);
	// Map.Entry<Integer, Double> expDivEntry = divMap != null ? divMap.floorEntry(expDays) : null;
	// ATOpportunityInternalConfig intConfig = new ATOpportunityInternalConfig(
	// ul_.getOccId(), pxUl,
	// config_.dataAge, expDays, expDivEntry,
	// config_.spreadMin, config_.spreadMax,
	// strikeConfig.strikeMin, strikeConfig.strikeMax,
	// config_.yieldMinAbs, config_.yieldMinPct);
	// Set<ATMarketData> options = optionMap_.get(expDS6);
	// switch (strategy_) {
	// case ATOpportunity2.CC:
	// analyzeCC(options, intConfig, mktMap, opportunities);
	// break;
	// case ATOpportunity2.VCS:
	// case ATOpportunity2.VPS:
	// analyzeVerticalSpread(strategy_, options, intConfig, mktMap, opportunities);
	// break;
	// default:
	// throw new ATException(String.format("analyze Unsupported strategy[%s]", strategy_));
	// }
	// }
	//
	// if (!opportunities.isEmpty()) {
	// opportunities.sort(new Comparator<ATOpportunity2>() {
	// @Override
	// public int compare(ATOpportunity2 a_, ATOpportunity2 b_) {
	// return Double.compare(b_.getYieldPct(), a_.getYieldPct());
	// // return Double.compare(b_.getYieldAbs(), a_.getYieldAbs());
	// }
	// });
	//
	// if (opportunities.size() > config_.oppCount) {
	// opportunities = opportunities.subList(0, config_.oppCount);
	// }
	// String symbol = ul_.getOccId();
	// ATFundamental fnd = new ATFundamental(foundation, avgMode, hvMode);
	// Map<String, ATFundamental> fndMap = new TreeMap<>(Collections.singletonMap(symbol, fnd));
	// ATEquityDescription description = new ATEquityDescription(foundation);
	// Map<String, ATEquityDescription> dscMap = new TreeMap<>(Collections.singletonMap(symbol, description));
	// Map<String, List<ATOpportunity2>> oppMap = new TreeMap<>(Collections.singletonMap(strategy_, opportunities));
	// Map<String, Map<String, List<ATOpportunity2>>> symOppMap = new TreeMap<>(Collections.singletonMap(symbol, oppMap));
	// ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);
	// pack.setOpportunities(symOppMap);
	// return pack;
	// } else {
	// return null;
	// }
	//
	// } catch (Exception ex) {
	// log.error(String.format("analyzeCC Error"), ex);
	// return null;
	// }
	// }

	private boolean analyzeCC(Set<String> occIds_, ATOpportunityInternalConfig config_, Map<String, ATMarketData> inMktMap_,
			Map<String, ATMarketData> outMktMap_, List<ATOpportunity2> opportunities_) {
		boolean isOpportunity = false;
		double div = config_.divEntry != null ? config_.divEntry.getValue() : 0;
		Integer divDays = config_.divEntry != null ? config_.divEntry.getKey() : null;
		String expDivStr = null;
		if (divDays != null) {
			expDivStr = LocalDate.now().plusDays(divDays).format(FMT_DS10);
		}
		// int dataMinutes = (int) (60 * Math.abs(config_.dataAge));
		long maxDataAge = DateUtils.addMinutes(new Date(), -config_.dataAge).getTime();
		// = LocalDate.now().minusDays(3).atStartOfDay(ZONE_ID).toEpochSecond();
		// LocalDateTime.now().minusMinutes(dataMinutes).
		for (String occId : occIds_) {
			ATMarketData call = inMktMap_.get(occId);
			if (call == null)
				continue;
			double pxStrike = call.getPxStrike();
			Double pxAsk = call.getPxAsk();
			Double pxBid = call.getPxBid();
			Long tsAsk = call.getTsAsk();
			Long tsBid = call.getTsBid();
			Long ts = tsAsk == null || tsBid == null ? null : Long.min(tsAsk, tsBid);
			if (pxAsk == null || pxAsk == 0 || pxBid == null || pxBid == 0 || pxStrike < config_.strikeMin || pxStrike > config_.strikeMax
					|| ts == null || ts < maxDataAge)
				continue;
			double pxCall = (pxAsk + pxBid) / 2;
			double pxCC = config_.pxUl - pxCall;
			double ccYieldAbs = call.getPxStrike() + div - pxCC;
			if (ccYieldAbs < config_.yieldMinAbs || pxCC >= pxStrike)
				continue;
			double ccYield = 100.0 * ccYieldAbs / pxCC;
			double ccYieldAnnual = 365.0 * ccYield / config_.expDays;
			if (ccYieldAnnual < config_.yieldMinPct)
				continue;
			Double ccYieldAsg = (divDays != null && divDays != 0) ? (365 / divDays) * 100 * (call.getPxStrike() - pxCC) / pxCC : null;

			// NOTE: Rounding reduces network data
			pxCC = Precision.round(pxCC, 2);
			ccYieldAbs = Precision.round(ccYieldAbs, 2);
			ccYieldAnnual = Precision.round(ccYieldAnnual, 2);

			Double pxCallTheo = call.getPxTheo();
			Double pxTheo = pxCallTheo != null ? config_.pxUl - pxCallTheo : null;

			outMktMap_.put(call.getOccId(), call);

			ATOpportunity2 opp = new ATOpportunity2(ATOpportunity2.CC);
			opp.setSymbol(ATModelUtil.getSymbol(occId));
			opp.setDaysExp(config_.expDays);
			opp.setYieldAbs(ccYieldAbs);
			opp.setYieldPct(ccYieldAnnual);
			if (ccYieldAsg != null) {
				ccYieldAsg = Precision.round(ccYieldAsg, 2);
				opp.setYieldAsg(ccYieldAsg);
			}
			opp.setPx(pxCC);
			if (pxTheo != null) {
				pxTheo = Precision.round(pxTheo, 2);
				opp.setPxTheo(pxTheo);
			}
			if (expDivStr != null) {
				div = Precision.round(div, 2);
				opp.setDiv(div);
				opp.setDivExDS10(expDivStr);
			}
			opp.getSegments().add(new ATOpportunitySegment(config_.symbol, 100.0, null));
			opp.getSegments().add(new ATOpportunitySegment(call.getOccId(), -1.0, config_.expDays));
			// opp.getOccIds().add(config_.symbol);
			// opp.getOccIds().add(call.getOccId());
			// opp.getExpDays().add(null);
			// opp.getExpDays().add((int) config_.expDays);
			opportunities_.add(opp);
			isOpportunity = true;
		}
		return isOpportunity;
	}

	// private boolean analyzeCC(Collection<ATMarketData> calls_, ATOpportunityInternalConfig config_, Map<String, ATMarketData> mktMap_,
	// List<ATOpportunity2> opportunities_) {
	// boolean isOpportunity = false;
	// double div = config_.divEntry != null ? config_.divEntry.getValue() : 0;
	// Integer divDays = config_.divEntry != null ? config_.divEntry.getKey() : null;
	// String divExDS10 = null;
	// if (divDays != null) {
	// divExDS10 = LocalDate.now().plusDays(divDays).format(FMT_DS10);
	// }
	// long daysAgo3 = DateUtils.addDays(new Date(), -3).getTime();
	// for (ATMarketData call : calls_) {
	// double pxStrike = call.getPxStrike();
	// Double pxAsk = call.getPxAsk();
	// Double pxBid = call.getPxBid();
	// if (pxAsk == null || pxAsk == 0 || pxBid == null || pxBid == 0 || pxStrike < config_.strikeMin || pxStrike > config_.strikeMax
	// || (daysAgo3 > call.getTsAsk() && daysAgo3 > call.getTsBid() && daysAgo3 > call.getTsLast()))
	// continue;
	//
	// double pxCall = (pxAsk + pxBid) / 2;
	// double pxCC = config_.pxUl - pxCall;
	// double ccYieldAbs = call.getPxStrike() + div - pxCC;
	// if (ccYieldAbs < config_.yieldMinAbs || pxCC >= pxStrike)
	// continue;
	// double ccYield = 100.0 * ccYieldAbs / pxCC;
	// double ccYieldAnnual = 365.0 * ccYield / config_.expDays;
	// if (ccYieldAnnual < config_.yieldMinPct)
	// continue;
	// Double ccYieldAsg = (divDays != null && divDays != 0) ? (365 / divDays) * 100 * (call.getPxStrike() - pxCC) / pxCC : null;
	//
	// // NOTE: Rounding reduces network data
	// pxCC = Precision.round(pxCC, 2);
	// ccYieldAbs = Precision.round(ccYieldAbs, 2);
	// ccYieldAnnual = Precision.round(ccYieldAnnual, 2);
	//
	// Double pxCallTheo = call.getPxTheo();
	// Double pxTheo = pxCallTheo != null ? config_.pxUl - pxCallTheo : null;
	//
	// mktMap_.put(call.getOccId(), call);
	//
	// ATOpportunity2 opp = new ATOpportunity2(ATOpportunity2.CC);
	// opp.setYieldAbs(ccYieldAbs);
	// opp.setYieldPct(ccYieldAnnual);
	// if (ccYieldAsg != null) {
	// ccYieldAsg = Precision.round(ccYieldAsg, 2);
	// opp.setYieldAsg(ccYieldAsg);
	// }
	// opp.setPx(pxCC);
	// if (pxTheo != null) {
	// pxTheo = Precision.round(pxTheo, 2);
	// opp.setPxTheo(pxTheo);
	// }
	// if (divExDS10 != null) {
	// div = Precision.round(div, 2);
	// opp.setDiv(div);
	// opp.setDivExDS10(divExDS10);
	// }
	// opp.getSegments().add(new ATOpportunitySegment(config_.symbol, 100.0, null));
	// opp.getSegments().add(new ATOpportunitySegment(call.getOccId(), -1.0, config_.expDays));
	// // opp.getOccIds().add(config_.symbol);
	// // opp.getOccIds().add(call.getOccId());
	// // opp.getExpDays().add(null);
	// // opp.getExpDays().add((int) config_.expDays);
	// opportunities_.add(opp);
	// isOpportunity = true;
	// }
	// return isOpportunity;
	// }

	private boolean analyzeVerticalSpread(String strategy_, Set<String> occIds_, ATOpportunityInternalConfig config_,
			Map<String, ATMarketData> inMktMap_, Map<String, ATMarketData> outMktMap_, List<ATOpportunity2> opportunities_) {
		boolean isVCS = ATOpportunity2.VCS.equals(strategy_);
		if (!(isVCS || ATOpportunity2.VPS.equals(strategy_))) {
			throw new ATException(String.format("Unsupported strategy [%s]", strategy_));
		}
		boolean isOpportunity = false;
		double div = config_.divEntry != null ? config_.divEntry.getValue() : 0;
		Integer divDays = config_.divEntry != null ? config_.divEntry.getKey() : null;
		String expDivStr = null;
		if (divDays != null) {
			expDivStr = LocalDate.now().plusDays(divDays).format(FMT_DS10);
		}
		// int dataMinutes = (int) (60 * Math.abs(config_.dataAge));
		long maxDataAge = DateUtils.addMinutes(new Date(), -config_.dataAge).getTime();
		// long dataAge = LocalDate.now().minusDays(3).atStartOfDay(ZONE_ID).toEpochSecond() * 1000;
		// NOTE: It 'is' a TreeSet
		TreeSet<String> occIdsA = occIds_ instanceof TreeSet ? (TreeSet<String>) occIds_ : new TreeSet<>(occIds_);
		for (String occIdA : occIdsA) {
			ATMarketData optionA = inMktMap_.get(occIdA);
			if (optionA == null)
				continue;
			double strikeA = optionA.getPxStrike();
			if (strikeA > config_.strikeMax)
				break;
			Double pxAskA = optionA.getPxAsk();
			Double pxBidA = optionA.getPxBid();
			Long tsAskA = optionA.getTsAsk();
			Long tsBidA = optionA.getTsBid();
			Long tsA = tsAskA == null || tsBidA == null ? null : Long.min(tsAskA, tsBidA);
			if (strikeA < config_.strikeMin
					|| pxAskA == null || pxAskA == 0 || pxBidA == null || pxBidA == 0
					|| tsA == null || tsA < maxDataAge)
				continue;
			double pxA = (pxAskA + pxBidA) / 2;
			Double pxATheo = optionA.getPxTheo();
			NavigableSet<String> occIdsB = occIdsA.tailSet(occIdA, false);
			for (String occIdB : occIdsB) {
				ATMarketData optionB = inMktMap_.get(occIdB);
				if (optionB == null)
					continue;
				double strikeB = optionB.getPxStrike();
				double spread = strikeB - strikeA;
				if (strikeB > config_.strikeMax || spread > config_.spreadMax)
					break;
				Double pxAskB = optionB.getPxAsk();
				Double pxBidB = optionB.getPxBid();
				Long tsAskB = optionA.getTsAsk();
				Long tsBidB = optionA.getTsBid();
				Long tsB = tsAskB == null || tsBidB == null ? null : Long.min(tsAskB, tsBidB);
				if (strikeB < config_.strikeMin
						|| pxAskB == null || pxAskB == 0 || pxBidB == null || pxBidB == 0
						|| spread < config_.spreadMin /* NOTE: Not really necessary since strikeB > strikeA */
						|| tsB == null || tsB < maxDataAge)
					continue;
				double pxB = (pxAskB + pxBidB) / 2;
				double px = isVCS ? pxA - pxB : pxB - pxA;
				if (px < config_.yieldMinAbs || px >= spread)
					continue;
				double yield = 100.0 * px / spread;
				double yieldAnnual = 365.0 * yield / config_.expDays;
				if (yieldAnnual < config_.yieldMinPct)
					continue;
				Double yieldAsg = (divDays != null && divDays != 0) ? (365 / divDays) * yield : null;

				px = Precision.round(px, 2);
				yield = Precision.round(yield, 2);
				yieldAnnual = Precision.round(yieldAnnual, 2);

				Double pxBTheo = pxATheo != null ? optionB.getPxTheo() : null;
				Double pxTheo = pxBTheo != null ? isVCS ? pxATheo - pxBTheo : pxBTheo - pxATheo : null;

				outMktMap_.put(optionA.getOccId(), optionA);
				outMktMap_.put(optionB.getOccId(), optionB);

				ATOpportunity2 opp = new ATOpportunity2(strategy_);
				opp.setSymbol(ATModelUtil.getSymbol(occIdB));
				opp.setDaysExp(config_.expDays);
				opp.setYieldAbs(px);
				opp.setYieldPct(yieldAnnual);
				if (yieldAsg != null) {
					yieldAsg = Precision.round(yieldAsg, 2);
					opp.setYieldAsg(yieldAsg);
				}
				opp.setPx(px);
				if (pxTheo != null) {
					pxTheo = Precision.round(pxTheo, 2);
					opp.setPxTheo(pxTheo);
				}
				if (expDivStr != null) {
					div = Precision.round(div, 2);
					opp.setDiv(div);
					opp.setDivExDS10(expDivStr);
				}
				opp.getSegments().add(new ATOpportunitySegment(optionA.getOccId(), isVCS ? -1.0 : 1.0, config_.expDays));
				opp.getSegments().add(new ATOpportunitySegment(optionB.getOccId(), isVCS ? 1.0 : -1.0, config_.expDays));
				// opp.getOccIds().add(optionA.getOccId());
				// opp.getOccIds().add(optionB.getOccId());
				// opp.getExpDays().add((int) config_.expDays);
				// opp.getExpDays().add((int) config_.expDays);
				opportunities_.add(opp);
				isOpportunity = true;
			}
		}
		return isOpportunity;

	}

	// private boolean analyzeVerticalSpread(String strategy_, Collection<ATMarketData> options_, ATOpportunityInternalConfig config_,
	// Map<String, ATMarketData> mktMap_,
	// List<ATOpportunity2> opportunities_) {
	//
	// boolean isVCS = ATOpportunity2.VCS.equals(strategy_);
	// if (!(isVCS || ATOpportunity2.VPS.equals(strategy_))) {
	// throw new ATException(String.format("Unsupported strategy [%s]", strategy_));
	// }
	// boolean isOpportunity = false;
	// double div = config_.divEntry != null ? config_.divEntry.getValue() : 0;
	// Integer divDays = config_.divEntry != null ? config_.divEntry.getKey() : null;
	// String expDivStr = null;
	// if (divDays != null) {
	// expDivStr = LocalDate.now().plusDays(divDays).format(FMT_DS10);
	// }
	// // int dataMinutes = (int) (60 * Math.abs(config_.dataAge));
	// long dataAge = DateUtils.addMinutes(new Date(), -config_.dataAge).getTime();
	// // long dataAge = LocalDate.now().minusDays(3).atStartOfDay(ZONE_ID).toEpochSecond() * 1000;
	// // NOTE: It 'is' a TreeSet
	// TreeSet<ATMarketData> optionsA = options_ instanceof TreeSet ? (TreeSet<ATMarketData>) options_ : new TreeSet<>(options_);
	// for (ATMarketData optionA : optionsA) {
	// double strikeA = optionA.getPxStrike();
	// Double pxAskA = optionA.getPxAsk();
	// Double pxBidA = optionA.getPxBid();
	// Long tsAsk = optionA.getTsAsk();
	// Long tsBid = optionA.getTsBid();
	// Long ts = tsAsk == null || tsBid == null ? null : Long.min(tsAsk, tsBid);
	// if (strikeA < config_.strikeMin || pxAskA == null || pxAskA == 0 || pxBidA == null || pxBidA == 0 || ts < dataAge)
	// continue;
	// if (strikeA > config_.strikeMax)
	// break;
	// double pxA = (pxAskA + pxBidA) / 2;
	// Double pxATheo = optionA.getPxTheo();
	// NavigableSet<ATMarketData> optionsB = optionsA.tailSet(optionA, false);
	// for (ATMarketData optionB : optionsB) {
	// double strikeB = optionB.getPxStrike();
	// double spread = strikeB - strikeA;
	// Double pxAskB = optionB.getPxAsk();
	// Double pxBidB = optionB.getPxBid();
	// if (strikeB < config_.strikeMin
	// || pxAskB == null || pxAskB == 0 || pxBidB == null || pxBidB == 0
	// || spread < config_.spreadMin /* NOTE: Not really necessary since strikeB > strikeA */)
	// continue;
	// if (strikeB > config_.strikeMax || spread > config_.spreadMax)
	// break;
	// double pxB = (pxAskB + pxBidB) / 2;
	// double px = isVCS ? pxA - pxB : pxB - pxA;
	// if (px < config_.yieldMinAbs || px >= spread)
	// continue;
	// double yield = 100.0 * px / spread;
	// double yieldAnnual = 365.0 * yield / config_.expDays;
	// if (yieldAnnual < config_.yieldMinPct)
	// continue;
	// Double yieldAsg = (divDays != null && divDays != 0) ? (365 / divDays) * yield : null;
	//
	// px = Precision.round(px, 2);
	// yield = Precision.round(yield, 2);
	// yieldAnnual = Precision.round(yieldAnnual, 2);
	//
	// Double pxBTheo = pxATheo != null ? optionB.getPxTheo() : null;
	// Double pxTheo = pxBTheo != null ? isVCS ? pxATheo - pxBTheo : pxBTheo - pxATheo : null;
	//
	// mktMap_.put(optionA.getOccId(), optionA);
	// mktMap_.put(optionB.getOccId(), optionB);
	//
	// ATOpportunity2 opp = new ATOpportunity2(strategy_);
	// opp.setYieldAbs(px);
	// opp.setYieldPct(yieldAnnual);
	// if (yieldAsg != null) {
	// yieldAsg = Precision.round(yieldAsg, 2);
	// opp.setYieldAsg(yieldAsg);
	// }
	// opp.setPx(px);
	// if (pxTheo != null) {
	// pxTheo = Precision.round(pxTheo, 2);
	// opp.setPxTheo(pxTheo);
	// }
	// if (expDivStr != null) {
	// div = Precision.round(div, 2);
	// opp.setDiv(div);
	// opp.setDivExDS10(expDivStr);
	// }
	// opp.getSegments().add(new ATOpportunitySegment(optionA.getOccId(), isVCS ? -1.0 : 1.0, config_.expDays));
	// opp.getSegments().add(new ATOpportunitySegment(optionB.getOccId(), isVCS ? 1.0 : -1.0, config_.expDays));
	// // opp.getOccIds().add(optionA.getOccId());
	// // opp.getOccIds().add(optionB.getOccId());
	// // opp.getExpDays().add((int) config_.expDays);
	// // opp.getExpDays().add((int) config_.expDays);
	// opportunities_.add(opp);
	// isOpportunity = true;
	// }
	// }
	// return isOpportunity;
	// }

	// public static class ATUserOpportunityConfig {
	//
	// static public final String STRAT_HV = "hv";
	// static public final String STRAT_USER = "user";
	// static public final String STRAT_ATM = "atm";
	// static public final String STRAT_ITM = "itm";
	// static public final String STRAT_OTM = "otm";
	// static public final String STRAT_VLT = "vlt";
	//
	// public ATUserOpportunityConfig() {
	// }
	//
	// public ATUserOpportunityConfig(Map<String, Object> tmpl_) {
	// }
	//
	// public int daysMin = 0;
	// public int daysMax = Integer.MAX_VALUE;
	//
	// public int dataAge = 4320; // 3 days in h
	// public int oppCount = 5;
	//
	// public Double overrideDiv = null;
	// public Double overrideVlt = null;
	//
	// public double spreadMin = 0.0;
	// public double spreadMax = Double.MAX_VALUE;
	//
	// public double strikeMin = 0.0;
	// public double strikeMax = Double.MAX_VALUE;
	// public String strikeStrat = "user";
	//
	// public double yieldMinAbs = 0.0;
	// public double yieldMinPct = 0.0;
	//
	// public List<String> flags;
	//
	// public void setUserId(String value_) {
	//
	// }
	// }

	public static class ATOpportunityInternalConfig {

		public String symbol;
		public Entry<Integer, Double> divEntry = null;
		public int dataAge = 4320; // 3 days in h
		public int expDays = 0;
		public double pxUl = 0.0;
		public double spreadMin = 0.0;
		public double spreadMax = Double.MAX_VALUE;
		public double strikeMin = 0.0;
		public double strikeMax = Double.MAX_VALUE;
		public double yieldMinAbs = 0.0;
		public double yieldMinPct = 0.0;

		public ATOpportunityInternalConfig(
				String symbol_,
				double pxUl_,
				int dataAge_,
				int expDays_,
				Entry<Integer, Double> expDivEntry,
				double spreadMin_,
				double spreadMax_,
				double strikeMin_,
				double strikeMax_,
				double yieldMinAbs_,
				double yieldMinPct_) {
			this.divEntry = expDivEntry;
			this.dataAge = dataAge_;
			this.expDays = expDays_;
			this.pxUl = pxUl_;
			this.spreadMin = spreadMin_;
			this.spreadMax = spreadMax_;
			this.strikeMax = strikeMax_;
			this.strikeMin = strikeMin_;
			this.symbol = symbol_;
			this.yieldMinAbs = yieldMinAbs_;
			this.yieldMinPct = yieldMinPct_;
		}

	}

	public static class StrikeConfig {

		public Double strikeMin;
		public Double strikeMax;

		public TreeMap<Integer, Double> hvMap;

		public StrikeConfig(Double strikeMin_, Double strikeMax_, TreeMap<Integer, Double> hvMap_) {
			this.strikeMin = strikeMin_;
			this.strikeMax = strikeMax_;
			this.hvMap = hvMap_;
		}

		public void updateStrikes(String strategy_, Double pxUl_, Integer expDays_) {
			if (hvMap == null || hvMap.isEmpty())
				return;
			Entry<Integer, Double> hvDaily = hvMap.floorEntry(expDays_);
			if (hvDaily == null) {
				hvDaily = hvMap.firstEntry();
			}
			if (log.isDebugEnabled())
				log.debug(String.format("Getting HV [%d -> (%d / %.2f)]", expDays_, hvDaily.getKey(), hvDaily.getValue()));
			Double hvExp = hvDaily.getValue() * Math.sqrt(expDays_);
			strikeMin = ATOpportunity2.isSafeBelowLast(strategy_) ? 0 : pxUl_ + hvExp;
			strikeMax = ATOpportunity2.isSafeBelowLast(strategy_) ? pxUl_ = hvExp : Double.MAX_VALUE;
		}

	}

}
