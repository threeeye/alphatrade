package com.isoplane.at.source.controller;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.service.IATService;

public interface IATOpportunityProcessor extends IATService {

	ATSecurityPack analyzeCC(ATMarketData mktUl, Map<String, ATMarketData> marketMap, TreeMap<String, TreeSet<String>> callChain,
			ATUserOpportunityConfig config);

	ATSecurityPack analyzeVCS(ATMarketData mktUl, Map<String, ATMarketData> marketMap, TreeMap<String, TreeSet<String>> callChain,
			ATUserOpportunityConfig config);

	ATSecurityPack analyzeVPS(ATMarketData mktUl, Map<String, ATMarketData> marketMap, TreeMap<String, TreeSet<String>> putChain,
			ATUserOpportunityConfig config);

}
