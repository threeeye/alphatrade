package com.isoplane.at.source.adapter;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATLegacyDividend;
import com.isoplane.at.commons.model.ATStock;

public interface IATDividendAdapter {

	Map<String, TreeSet<ATLegacyDividend>> getDividends(Collection<ATStock> securities);
	
	/**
	 * Used for individual ad-hoc update. May return null if force == false and implementation only supports unspecified
	 * bulk.
	 */
	TreeSet<ATLegacyDividend> getDividend(ATStock security, boolean force);

}
