package com.isoplane.at.source.util;

import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.source.model.ATMongoTask;
import com.isoplane.at.source.model.IATTask;

public class ATSourceMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATSourceMongodbTools.class);

	private Configuration _config;
	private ATMongoDao _dao;
	private String _taskTable;

	public ATSourceMongodbTools(Configuration config_) {
		_config = config_;
		_taskTable = ATMongoTableRegistry.getTaskTable();
	}

	public ATSourceMongodbTools init() {
		_dao = new ATMongoDao(_config);
		// ATExecutors.scheduleAtFixedRate("source.tasks", command_, initialDelay_, period_)
		// _dao.setSkipUpdateTS(true);

		return this;
	}

	public IATTask createTask(String source_, String code_, String data_) {
		ATMongoTask task = new ATMongoTask();
		task.setCode(code_);
		task.setData(data_);
		task.setSource(source_);
		task.setStatus(IATTask.STATUS_NEW);
		task.setAtId(String.format("%s:%s", code_, data_).hashCode() + "");
		return task;
	}

	public List<IATTask> getTasks() {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTask.STATUS, IATTask.STATUS_NEW);
		@SuppressWarnings("unchecked")
		List<IATTask> tasks = (List<IATTask>) (Object) _dao.query(_taskTable, query, ATMongoTask.class);
		return tasks;
	}

	public void saveTasks(IATTask... tasks_) {
		_dao.update(_taskTable, IATLookupMap.AT_ID, tasks_);
	}

	public long deleteTasks(IATWhereQuery query_) {
		long count = _dao.delete(_taskTable, query_);
		return count;
	}

	public List<IATTask> loadTasks(IATWhereQuery query_) {
		List<ATMongoTask> mTasks = _dao.query(_taskTable, query_, ATMongoTask.class);
		@SuppressWarnings("unchecked")
		List<IATTask> tasks = (List<IATTask>) (Object) mTasks;
		return tasks;
	}

	public void save(String table_, IATLookupMap data_) {
		// data_.put(IATMongo.MONGO_ID, data_.getAtId());
		_dao.update(table_, data_);
	}

	public IATLookupMap load(String table_, String id_) {
		IATLookupMap data = _dao.get(table_, id_);
		return data;
	}

}
