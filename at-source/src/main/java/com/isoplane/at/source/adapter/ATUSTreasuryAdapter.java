package com.isoplane.at.source.adapter;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import com.isoplane.at.commons.service.IATUSTreasuryRateProvider;
import com.isoplane.at.source.util.ATHttpUtil;

public class ATUSTreasuryAdapter implements IATUSTreasuryRateProvider {

	static final Logger log = LoggerFactory.getLogger(ATUSTreasuryAdapter.class);

	// alt: http://data.treasury.gov/feed.svc/DailyTreasuryYieldCurveRateData?$filter=month(NEW_DATE) eq 4 and year(NEW_DATE) eq 2018

	Double _riskFreeInterestRate;

	public Double getRiskFreeInterestRate() {
		if (_riskFreeInterestRate == null) {
			log.error(String.format("Risk free rate == NULL"));
		}
		return _riskFreeInterestRate;
	}

	@Override
	public void initService() {
		updateRates();
	}

	private void updateRates() {
		try {
			String url = "https://www.treasury.gov/resource-center/data-chart-center/interest-rates/pages/XmlView.aspx?data=billrates";
			String xml = ATHttpUtil.get(url, null);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
			XPathFactory xpathFactory = XPathFactory.newInstance();
			XPath xpath = xpathFactory.newXPath();
			XPathExpression expr = xpath.compile(
					"/*[local-name()='pre']/*[local-name()='entry'][last()]/*[local-name()='content']/*[local-name()='properties']/*[local-name()='ROUND_B1_YIELD_13WK_2']/text()");
			String rateStr = expr.evaluate(doc);
			Double rate = Double.parseDouble(rateStr);
			_riskFreeInterestRate = rate / 100.0;
			log.info(String.format("3 Months US Treasury Bill Rate (risk-free) [%.5f]", _riskFreeInterestRate));
		} catch (Exception ex) {
			log.error("Error obtaining rate", ex);
		}

	}
}
