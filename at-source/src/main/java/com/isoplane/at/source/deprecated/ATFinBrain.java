package com.isoplane.at.source.deprecated;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.eclipse.jetty.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATLegacyDividend;
import com.isoplane.at.commons.model.ATOpportunity;
import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.model.IATEarning;
import com.isoplane.at.commons.model.IATOptionDescription.Right;
import com.isoplane.at.commons.model.alertcriteria.ATBaseCriteria;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentPosition;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATStandardNormalTable;
import com.isoplane.at.ext.quantlib.ATOptionAnalytics;
import com.isoplane.at.ext.quantlib.ATQuantLib;
import com.isoplane.at.source.controller.ATRequestContext;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;
import com.isoplane.at.source.controller.IATAlertProcessor;
import com.isoplane.at.source.model.ATChangeNotification;
import com.isoplane.at.source.model.ATConfiguration;
import com.isoplane.at.source.model.ATDividendNotification;
import com.isoplane.at.source.model.ATRoll;
import com.isoplane.at.source.model.ATRollNotification;
import com.isoplane.at.source.model.ATStrikeNotification;
import com.isoplane.at.source.model.ATUser;
import com.isoplane.at.source.model.ATYield;
import com.isoplane.at.source.model.IATUserConfiguration;
import com.isoplane.at.source.util.ATDateUtil;

@Deprecated
public class ATFinBrain implements IATFinancialProcessor {

	static final Logger log = LoggerFactory.getLogger(ATFinBrain.class);
	// static private final SimpleDateFormat _yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
	static private final SimpleDateFormat _stdDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	static private Configuration _config;
	private ATConfiguration _atConfig;
	private ATStore _store;
	private Gson _gson;
	// private ATQuantLib _quantLib;

	public ATFinBrain(Configuration config_, ATConfiguration atConfig_, ATStore store_) {
		_config = config_;
		_atConfig = atConfig_;
		_store = store_;
		_gson = new Gson();
		// _quantLib = new ATQuantLib(_atConfig);
	}
	// private long _8weeks_ms = 1000 /* 1 sec */ * 60 /* 1 min */ * 60 /* 1h */ * 24 /* 1d */ * 7 /* 1w */ * 8 /* 8w
	// */;

	@Override
	public List<ATOpportunity> analyze(ATSecurity security_) {
		ATUser user = ATRequestContext.getUser();
		if (security_ == null || !(security_ instanceof ATStock) || user == null || user.getConfig() == null)
			return null;
		try {
			ATStock stock = (ATStock) security_;
			// String[] op = _atConfig.getOptionFlags();
			// List<String> ops = _atConfig.getOptionFlags() == null ? null : Arrays.asList(_atConfig.getOptionFlags());
			List<String> bls = _atConfig.getBlacklistSymbols() == null ? null
					: Arrays.asList(_atConfig.getBlacklistSymbols());
			if (bls != null && bls.contains(stock.getOccId())) {
				return null;
			}
			if (!stock.hasWeeklyOptions() && !_atConfig.getOptionFlagList().contains("M")) {
				return null;
			}
			// long start = System.currentTimeMillis();
			log.debug(String.format("Analyzing [%s]", stock.getOccId()));
			List<ATOpportunity> ccs = analyzeOptions(stock, user.getConfig());
			// log.info(String.format(" -> %d", System.currentTimeMillis() - start));
			return ccs;
		} catch (Exception ex) {
			log.error(String.format("Error analyzing [%s]", security_), ex);
			return null;
		}
	}

	@Deprecated
	@Override
	public Collection<ATBaseNotification> analyzeOptonChain(ATPersistentSecurity... securities_) {
		Collection<ATBaseNotification> result = new ArrayList<>();
		if (securities_ == null || securities_.length <= 1) {
			return result;
		}
		ATPersistentSecurity stock = securities_[0];
		ATPersistentEquityFoundation foundation = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(stock.getOccId());
		if (!validateStock(stock, foundation))
			return result;

		Set<ATPersistentPosition> positions = ATSourceServiceRegistry.getPositionProcessor().getPositions(null);
		Set<String> positionIds = positions.stream().map(p -> p.getOccId()).collect(Collectors.toSet());
		boolean isActive = positionIds.contains(stock.getSymbol());
		String clickAction = String.format("https://tradonado.com/security/%s", stock.getSymbol());

		// if("TSLA".equals(stock.getOccId())) {
		// log.info("tesla");
		// }

		// User defined alerts
		IATAlertProcessor ap = ATSourceServiceRegistry.getAlertProcessor();
		Set<ATBaseNotification> criteriaAlerts = ap.process(stock);
		if (criteriaAlerts != null) {
			result.addAll(criteriaAlerts);
		}

		// Prepare expiration map
		LinkedHashMap<Date, ATPersistentSecurity> dateMap = new LinkedHashMap<>();
		dateMap.put(new Date(0), stock);
		for (int i = 1; i < securities_.length; i++) {
			ATPersistentSecurity newSec = securities_[i];
			if (newSec.getProbability() == null || !stock.getSymbol().equals(newSec.getUnderlyingOccId()))
				continue;
			ATPersistentSecurity oldSec = dateMap.get(newSec.getExpirationDate());
			if (oldSec == null || oldSec.getProbability() < newSec.getProbability()) {
				dateMap.put(newSec.getExpirationDate(), newSec);
			}
		}

		// Dividends
		if (isActive && StringUtil.isNotBlank(foundation.getDividendExDateStr())) {
			Date exDivDate = foundation.getDividendExDate();
			if (exDivDate != null) {
				log.debug(String.format("Notification dividend [%s]: %s", stock.getOccId(), exDivDate));
			}
			long diff = exDivDate.getTime() - System.currentTimeMillis();
			long hours = TimeUnit.DAYS.convert(diff, TimeUnit.HOURS);
			if (diff >= 0 && hours < 48) {
				ATDividendNotification divn = new ATDividendNotification();
				divn.setActive(isActive);
				divn.setClickAction(clickAction);
				divn.setDate(new Date());
				divn.setExDate(exDivDate);
				divn.setLabel(stock.getLabel());
				divn.setSymbol(stock.getOccId());
				result.add(divn);
			}
		}

		// Stock price changes
		Double threshold = isActive ? 0.0175 : 0.15;
		Double referencePx = _atConfig.getMarketStatus() > 0 ? stock.getPriceOpen() : stock.getPriceClose();
		Double currentPx = stock.getPriceLast();
		Double change = (currentPx == null || referencePx == null) ? null : (currentPx - referencePx) / referencePx;
		if (change != null) {
			log.debug(String.format("Notification px change [%s/%b/%d]: %.4f", stock.getOccId(), isActive, _atConfig.getMarketStatus(), change));
		}
		if (change != null && Math.abs(change) >= threshold) {
			ATChangeNotification cn = new ATChangeNotification();
			cn.setSymbol(stock.getOccId());
			cn.setLabel(stock.getLabel());
			cn.setChangePct(100 * change);
			cn.setPeriod("day");
			cn.setDate(new Date());
			cn.setPrice(stock.getPrice());
			cn.setActive(isActive);
			cn.setClickAction(clickAction);
			result.add(cn);
		}

		// Strike warning for active options
		Map<Date, TreeSet<ATPersistentSecurity>> optionMap = new TreeMap<>();
		for (int i = 1; i < securities_.length; i++) {
			ATPersistentSecurity option = securities_[i];
			boolean isOptActive = positionIds.contains(option.getOccId());
			// log.info(String.format("Positions: %s", positionIds));
			if (!isOptActive || !option.isOption())
				continue;
			log.debug(String.format("Analyzing option [%s]", option.getOccId()));
			// log.info(String.format("ISACTIVE %s %b", option.getOccId(), isOptActive));
			try {
				Double lastPx = stock.getPriceLast();
				Double margin = 100.0 * (option.getStrike() - lastPx) / option.getStrike();
				if (Math.abs(margin) > 2.75)
					continue;
				TreeSet<ATPersistentSecurity> options = optionMap.get(option.getExpirationDate());
				if (options == null) {
					options = new TreeSet<>();
					optionMap.put(option.getExpirationDate(), options);
				}
				options.add(option);

				// margin = ATOptionSecurity.Call.equals(option.getOptionRight()) ? margin : -margin;
				// ATStrikeNotification sn = new ATStrikeNotification();
				// sn.setSymbol(stock.getSymbol());
				// sn.setActive(isOptActive);
				// sn.setClickAction(clickAction);
				// sn.setDate(new Date());
				// sn.setLabel(option.getLabel());
				// sn.setStockPrice(lastPx);
				// sn.setMargin(margin);
				// sn.setMeta(String.format("%s%f", option.getOptionRight(), option.getStrike()));
				// result_digest.add(sn);
			} catch (Exception ex) {
				log.error(String.format("Error checking [%s]", option.getOccId()), ex);
			}
		}
		for (TreeSet<ATPersistentSecurity> options : optionMap.values()) {
			if (options.isEmpty())
				continue;
			ATPersistentSecurity option = options.last();
			boolean isOptActive = positionIds.contains(option.getOccId());
			Double lastPx = stock.getPriceLast();
			Double margin = 100.0 * (option.getStrike() - lastPx) / option.getStrike();
			margin = ATOptionSecurity.Call.equals(option.getOptionRight()) ? margin : -margin;
			ATStrikeNotification sn = new ATStrikeNotification();
			sn.setSymbol(stock.getSymbol());
			sn.setActive(isOptActive);
			sn.setClickAction(clickAction);
			sn.setDate(new Date());
			sn.setLabel(option.getLabel());
			sn.setStockPrice(lastPx);
			sn.setMargin(margin);
			sn.setMeta(ATStrikeNotification.META_STRIKE, String.format("%s%f", option.getOptionRight(), option.getStrike()));
			result.add(sn);
		}
		if (!result.isEmpty()) {
			log.info(String.format("Notables  %s", result));
		}
		log.debug(String.format("Notification analysis [%d]", result.size()));
		return result;
	}

	private boolean validateStock(ATPersistentSecurity stock_, ATPersistentEquityFoundation foundation_) {
		String symbol = stock_.getOccId();
		Double stockPx = stock_.getPrice();
		if (stockPx == null || stockPx < 1.0) { // TODO: Parameter
			log.info(String.format("[%-5s] Price is null", symbol));
			return false;
		}
		Set<String> blacklist = ATSourceServiceRegistry.getSymbolsProvider().getBlacklist();
		if (blacklist.contains(symbol)) {
			log.info(String.format("[%-5s] Blacklisted", symbol));
			return false;
		}
		if (foundation_ == null) {
			log.error(String.format("[%-5s] Missing foundation", symbol));
			return false;
		}
		return true;
	}

	@Override
	public Collection<ATBaseNotification> getNoteworthyEvents(Collection<ATSecurity> securities_) {
		if (securities_ == null)
			return null;
		Set<ATBaseNotification> result = new HashSet<>();
		if (true)
			return result;

		Set<ATStock> stocks = new HashSet<>();
		Set<String> done = new HashSet<>();
		Set<ATOptionSecurity> options = new HashSet<>();
		// Note: Alerts now handled in ATAlertProcessor
		// for (ATSecurity sec : securities_) {
		// if (sec instanceof ATStock) {
		// if (sec.getPrice() == null || sec.getPrice() < 1.0)
		// continue;
		// stocks.add((ATStock) sec);
		// } else if (sec instanceof ATOptionSecurity) {
		// options.add((ATOptionSecurity) sec);
		// }
		// Set<ATBaseCriteria> criteria = _store.getAlertCriteria(sec.getOccId());
		// if (criteria == null)
		// continue;
		// criteria = new HashSet<>(criteria); // To prevent concurrent modification
		// for (ATBaseCriteria crit : criteria) {
		// if (done.contains(crit.label()) || !crit.test(sec))
		// continue;
		// String label = crit.label();// String.format("%s %s", sec.getOccId(), crit.shortName());
		// done.add(label);
		// String clickAction = String.format("https://tradonado.com/security/%s", sec.getSymbol());
		// ATAlertNotification alertn = new ATAlertNotification();
		// alertn.setActive(sec.isActive());
		// alertn.setClickAction(clickAction);
		// alertn.setCriteria(crit.getCriteria());
		// alertn.setDate(new Date());
		// alertn.setLabel(label);
		// alertn.setPrice(sec.getPrice());
		// alertn.setSymbol(sec.getSymbol());
		// result_digest.add(alertn);
		// }
		// }
		Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		for (ATStock stock : stocks) {
			String label = stock.getSymbol();
			boolean isActive = false;
			if (stock.isActive()) {
				isActive = true;
			} else if (stock.isOptionActive()) {
				isActive = true;
			}
			// Collection<ATPersistentSecurity> optionChain = ATSourceServiceRegistry.getSecuritiesProvider().getOptionChain(stock.getOccId());
			// // Collection<ATOptionSecurity> optionChain = _store.getOptionChain(stock.getOccId());
			// if (optionChain != null) {
			// for (ATPersistentSecurity opt : optionChain) {
			// if (opt.isActive()) {
			// label = opt.getLabel(); // Just grab the 1st
			// break;
			// }
			// }
			// }
			// }
			String clickAction = String.format(
					"https://snapshot.fidelity.com/fidresearch/snapshot/landing.jhtml#/research?symbol=%s",
					stock.getSymbol());

			// Dividends
			// TreeSet<ATLegacyDividend> dividends = null;
			// if (stock.isActive()) {
			// dividends = _store.getEvent(stock.getOccId(), ATLegacyDividend.class);
			// }
			// if (dividends != null) {
			// for (ATLegacyDividend div : dividends) {
			// if (div.getExDate().before(now))
			// continue;
			// long diff = div.getExDate().getTime() - System.currentTimeMillis();
			// long hours = TimeUnit.DAYS.convert(diff, TimeUnit.HOURS);
			// if (hours >= 0 && hours < 48) {
			// ATDividendNotification divn = new ATDividendNotification();
			// divn.setActive(isActive);
			// divn.setClickAction(clickAction);
			// divn.setDate(new Date());
			// divn.setExDate(div.getExDate());
			// divn.setLabel(label);
			// divn.setSymbol(stock.getOccId());
			// result_digest.add(divn);
			// }
			// }
			// }

			// Price changes
			Double threshold = isActive ? 1.75 : 15;
			Double refPx = _atConfig.getMarketStatus() > 0 ? stock.getPriceOpen() : stock.getPriceClose();
			Double change = (stock.getPriceLast() == null || refPx == null) ? null
					: 100 * (stock.getPriceLast() - refPx) / refPx;
			if (change != null && Math.abs(change) >= threshold) {
				ATChangeNotification cn = new ATChangeNotification();
				cn.setSymbol(stock.getOccId());
				cn.setLabel(label);
				cn.setChangePct(change);
				cn.setPeriod("day");
				cn.setDate(new Date());
				cn.setPrice(stock.getPrice());
				cn.setActive(isActive);
				// TODO: Action should be parameterized
				cn.setClickAction(clickAction);
				result.add(cn);
			}
		}
		// Strike warning
		for (ATOptionSecurity opt : options) {
			try {
				ATStock stock = _store.get(opt.getStockOccId());
				if (stock == null) {
					log.error(String.format("Missing stock [%s / %s]", opt.getStockOccId(), opt.getOccId()));
					continue;
				}
				Double refPx = stock.getPriceLast();// _atConfig.getMarketStatus() > 0 ? stock.getPriceOpen() :
													// stock.getPriceClose();
				Double margin = 100.0 * (opt.getStrike() - refPx) / opt.getStrike();
				if (Math.abs(margin) > 2.75)
					continue;
				margin = ATOptionSecurity.Call.equals(opt.getOptionRight()) ? margin : -margin;
				String clickAction = String.format(
						"https://snapshot.fidelity.com/fidresearch/snapshot/landing.jhtml#/research?symbol=%s",
						stock.getSymbol());

				ATStrikeNotification sn = new ATStrikeNotification();
				sn.setSymbol(opt.getSymbol());
				sn.setActive(opt.isActive());
				sn.setClickAction(clickAction);
				sn.setDate(new Date());
				sn.setLabel(opt.getLabel());
				sn.setStockPrice(refPx);
				sn.setMargin(margin);
				sn.setMeta(ATStrikeNotification.META_STRIKE, String.format("%s%f", opt.getOptionRight(), opt.getStrike()));
				result.add(sn);
			} catch (Exception ex) {
				log.error(String.format("Error checking [%s]", opt.getOccId()), ex);
			}
		}
		if (!result.isEmpty()) {
			log.info(String.format("Notables  %s", result));
		}
		return result;
	}

	@Override
	public Collection<ATBaseNotification> getRollEvents(String symbol_) {
		// if (true)
		// return null;
		ATStock stock = _store.get(symbol_);

		Set<ATBaseNotification> result = new HashSet<>();
		String clickAction = String.format("https://tradonado.com/security/%s", stock.getSymbol());

		if (stock != null && stock.isOptionActive()) {
			Collection<ATOptionSecurity> options = _store.getOptionChain(symbol_);
			if (options == null || options.isEmpty())
				return null;

			IATEarning foundation = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(stock.getOccId());
			Date earningsDate = null;
			if (foundation != null && !StringUtil.isBlank(foundation.getEarningsDateStr())) {
				try {
					earningsDate = ATFormats.DATE_yyyyMMdd.get().parse(foundation.getEarningsDateStr());
				} catch (ParseException ex) {
					log.debug(String.format("Invalid date [%s]", foundation.getEarningsDateStr()), ex);
				}
			}
			//
			// TreeSet<ATEarningOld> earnings = _store.getEvent(symbol_, ATEarningOld.class);
			// ATEarningOld earning = earnings == null || earnings.isEmpty() ? null : earnings.first();
			TreeSet<ATOptionSecurity> owned = new TreeSet<>();

			for (ATOptionSecurity option : options) {
				if (option.isActive())
					owned.add(option);
			}
			for (ATOptionSecurity option : owned) {
				if (option.getPrice() == null || option.getPriceAsk() == null || option.getPriceBid() == null)
					continue;
				for (ATOptionSecurity candidate : options) {
					if (candidate.getPrice() == null || candidate.getPriceAsk() == null
							|| candidate.getPriceBid() == null
							|| !option.getOptionRight().equals(candidate.getOptionRight())
							|| (option.getExpirationDate().compareTo(candidate.getExpirationDate()) >= 0)
							|| (earningsDate != null && earningsDate.compareTo(candidate.getExpirationDate()) <= 0))
						continue;
					double diff = candidate.getPriceBid() - option.getPriceAsk();
					double diffAdj = diff;
					if (ATOptionSecurity.Call.equals(option.getOptionRight())) {
						double delta = candidate.getStrike() - option.getStrike();
						if (delta <= 0)
							continue;
						diffAdj = diff + delta;
					} else if (ATOptionSecurity.Put.equals(option.getOptionRight())) {
						if ((option.getStrike() <= candidate.getStrike())
								|| (candidate.getStrike() > (stock.getPrice() - stock.getPrice() * stock.getPriceHV())))
							continue;
					}
					if (diff <= 0 || diffAdj <= 0)// || option.getStrike() <= candidate.getStrike())
						continue;

					double ratio = diffAdj / stock.getPrice();
					int days = (int) TimeUnit.DAYS.convert(
							candidate.getExpirationDate().getTime() - option.getExpirationDate().getTime(),
							TimeUnit.MILLISECONDS) + 1;
					double apy = ratio * 365 / days;
					if (apy < 0.10)
						continue;
					
					ATRollNotification rn = new ATRollNotification();
					rn.setActive(true);
					rn.setDate(new Date());
					rn.setOwnedTicker(option.getLabel());
					rn.setRollTicker(candidate.getLabel());
					rn.setClickAction(clickAction);
					rn.setLabel("");
					rn.setStockPrice(stock.getPrice());
					rn.setSymbol(option.getSymbol());
					rn.setAbsolute(diff);
					rn.setApy(apy * 100);
					rn.setMeta(ATRollNotification.META_ROLL, candidate.getStrike() > option.getStrike() ? 1 : -1);
					result.add(rn);
					break;
					// try {
					// break;
					// } catch (Exception ex) {
					// log.trace(String.format("%s/%s", option, candidate), ex);
					// }
				}
			}
		}
		return result;
	}

	private List<ATOpportunity> analyzeOptions(ATStock stock_, IATUserConfiguration userConfig_) {
		Collection<ATOptionSecurity> options = stock_ != null ? _store.getOptionChain(stock_.getOccId()) : null;
		if (options == null || stock_.getPriceLast() == null || stock_.getPrice3mAvg() == null) {
			// String jstr = (new Gson()).toJson(stock_);
			log.warn(String.format("Incomplete data on [%-4s] {px: %.2f, px3ma: %.2f}", stock_.getLabel(),
					stock_.getPriceLast(), stock_.getPrice3mAvg()));
			return null;
		}
		String userId = ATRequestContext.getUser().getEmail();
		double upx = stock_.getPriceLast();
		double uvar = stock_.getPrice3mHigh() != null && stock_.getPrice3mLow() != null
				? (100 / 4) * (stock_.getPrice3mHigh() - stock_.getPrice3mLow()) / upx
				: 10;

		double ccMinYieldAbs = 70;
		double ccPxRatio = 0.5;
		double maxBudget = 30000; // To determine max count
		double maxTradeRatio = 4;

		// log.info(String.format("Processing Ideas [%s - %6.2f]", stock_.getOccId(), upx));
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		List<ATOpportunity> opportunities = new ArrayList<>();

		double ccYieldMax = Double.MIN_VALUE;
		double ccSafetyMax = Double.MIN_VALUE;
		double callScoreMin = Integer.MAX_VALUE;
		double callScoreMax = Integer.MIN_VALUE;
		double putScoreMin = Integer.MAX_VALUE;
		double putScoreMax = Integer.MIN_VALUE;

		TreeSet<ATLegacyDividend> allDividends = _store.getEvent(stock_.getOccId(), ATLegacyDividend.class);
		TreeSet<ATLegacyDividend> testDividends = new TreeSet<>();
		if (allDividends != null) {
			for (ATLegacyDividend dividend : allDividends) {
				if (dividend.getExDate().before(today))
					continue;
				testDividends.add(dividend);
			}
		}

		IATEarning earning = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(stock_.getOccId());
		Date earningsDate = earning != null ? earning.getEarningsDate() : null;

		boolean isStkTrigger = false;
		Set<ATBaseCriteria> stkRules = _store.getOpportunityCriteria(stock_.getOccId());
		if (stkRules != null && !stkRules.isEmpty()) {
			for (ATBaseCriteria rule : stkRules) {
				Boolean test = rule.test(stock_);
				if (test == null) { // NULL means abort this security
					return null;
				} else if (test) {
					isStkTrigger = true;
					break;
				}
			}
		}

		boolean isStockActive = stock_.isOwnedBy(userId);
		// log.info(String.format("Ticker ,Bid ,Mid ,Ask ,Yield ,Safety,Days,Alert"));
		// log.info(String.format("Stock [%s - %.2f]", stock_.getLabel(), upx));
		for (ATOptionSecurity opt : options) {
			try {
				opt.setPriceUnderlying(stock_.getPrice());
				int days = (int) TimeUnit.DAYS.convert(opt.getExpirationDate().getTime() - today.getTime(),
						TimeUnit.MILLISECONDS) + 1;
				double minSafetyMargin = Math.max(userConfig_.getBaseSafetyMargin(), (1 + (days / 30)) * uvar);

				if (opt.getPriceAsk() == null || opt.getPriceAsk() == 0 || opt.getPriceBid() == null
						|| opt.getPriceBid() == 0 || days > userConfig_.getMaxOptionDays()
						|| days < userConfig_.getMinOptionDays() || opt.getStrike() == null // || opt.getStrike() > upx
				/* || opt.getOpenInterest() == null || opt.getVolume() == null */) {
					continue;
				}
				if (opt.getPriceAsk() / opt.getPriceBid() > maxTradeRatio)
					continue;

				boolean isOptTrigger = isStkTrigger;
				if (!isOptTrigger) {
					Set<ATBaseCriteria> optRules = _store.getOpportunityCriteria(opt.getOccId());
					if (optRules != null && !optRules.isEmpty()) {
						for (ATBaseCriteria rule : optRules) {
							if (rule.test(opt)) {
								isOptTrigger = true;
								break;
							}
						}
					}
				}

				Map<String, Object> meta = new HashMap<>();

				double div = 0;
				for (ATLegacyDividend dividend : testDividends) {
					if (dividend.getExDate().after(opt.getExpirationDate()))
						break;
					div += dividend.getCashAmount();
					@SuppressWarnings("unchecked")
					Set<String> divList = (Set<String>) meta.get("div");
					if (divList == null) {
						divList = new TreeSet<String>();
						meta.put("div", divList);
					}
					divList.add(String.format("%s (%.2f)", ATFormats.DATE_yyyy_MM_dd.get().format(dividend.getExDate()),
							dividend.getCashAmount()));
				}

				// TreeSet<ATEarningOld> earnings = _store.getEvent(stock_.getOccId(), ATEarningOld.class);
				if (earningsDate != null) {
					Date exDate = DateUtils.ceiling(opt.getExpirationDate(), Calendar.DAY_OF_MONTH);

					// for (ATEarningOld ern : earnings) {
					// Date earnDate = DateUtils.truncate(ern.getDate(), Calendar.DAY_OF_MONTH);
					if (!exDate.after(earningsDate)) {
						@SuppressWarnings("unchecked")
						Set<String> earningsList = (Set<String>) meta.get("earn");
						if (earningsList == null) {
							earningsList = new TreeSet<String>();
							meta.put("earn", earningsList);
						}
						earningsList.add(ATFormats.DATE_MM_dd.get().format(earningsDate));
					}
					// }
				}
				if (!userConfig_.getOptionFlagList().contains(ATOptionSecurity.INCLUDE_EARNINGS) && meta.get("earn") != null)
					continue;
				double marginFactor = meta.containsKey("earn") ? 1.0 : 1.5; // earningsList.isEmpty()
				minSafetyMargin *= marginFactor;
				double optPxMid = (ccPxRatio * opt.getPriceAsk() + opt.getPriceBid()) / (1 + ccPxRatio);
				if (ATRequestContext.getUser().getConfig().getMinPx() > optPxMid) {
					continue;
				}

				ATOpportunity opo = new ATOpportunity();
				opo.setTimestamp(new Date());
				opo.setDate(opt.getExpirationDate());
				opo.setDays(days - 1);
				opo.setOccId(opt.getOccId());
				opo.setLabel(opt.getLabel());
				opo.setSymbol(opt.getSymbol());
				opo.setMeta(_gson.toJson(meta));
				if (opt.isOwnedBy(userId)) {
					opo.setFlags("🌀");
					isStockActive = true;
				}

				String strategy;
				// Earnings substantially increases risk:
				double minApy = userConfig_.getMinApy() * (meta.containsKey("earn") ? 2 : 1);
				double priceAsk;
				double priceMid;
				double priceBid;
				double safetyMargin;
				double yieldAbsolute;
				double yieldAnnualPct;
				if (userConfig_.getOptionFlagList().contains("XXX") && ATOptionSecurity.Call.equals(opt.getOptionRight()) && div > 0) {
					strategy = ATOpportunity.CC;
					priceAsk = upx - opt.getPriceBid();
					priceMid = upx - optPxMid;
					priceBid = upx - opt.getPriceAsk();
					double ccYield = 100 * (div + opt.getStrike() - priceMid) / priceBid; // Strike - cost
					yieldAnnualPct = ccYield * 365 / days;
					yieldAbsolute = div + opt.getStrike() - priceMid;
					double ccYieldAbsTheoMax = ((int) (maxBudget / priceMid)) * yieldAbsolute;
					safetyMargin = 100 * (priceMid - upx) / upx;
					if (!isOptTrigger && (priceAsk > (opt.getStrike() - div) || yieldAnnualPct < minApy
							|| ccYieldAbsTheoMax < ccMinYieldAbs || safetyMargin < minSafetyMargin
							|| opt.getStrike() - div > stock_.getPrice3mAvg()))
						continue;
					if (ccYieldMax < yieldAnnualPct)
						ccYieldMax = yieldAnnualPct;
					if (ccSafetyMax < safetyMargin)
						ccSafetyMax = safetyMargin;
				} else if (userConfig_.getOptionFlagList().contains(ATOptionSecurity.Call) && ATOptionSecurity.Call.equals(opt.getOptionRight())) {
					if (/* opt.getStrike() < stock_.getPrice3mAvg() || */ opt.getStrike() < upx)
						continue;
					strategy = ATOpportunity.CALL;
					priceAsk = opt.getPriceAsk();
					priceMid = optPxMid;
					priceBid = opt.getPriceBid();
					double callYield = 100 * optPxMid / upx;
					yieldAnnualPct = callYield * 365 / days;
					yieldAbsolute = optPxMid;
					double callYieldAbsTheoMax = ((int) (maxBudget / upx)) * optPxMid;
					if (!isOptTrigger && (yieldAnnualPct < minApy || callYieldAbsTheoMax < ccMinYieldAbs))
						continue;
					safetyMargin = 100 * ((opt.getStrike() + optPxMid) - upx) / upx;
					// log.info(String.format("Option [%s]", opt.getLabel()));
					if (!isOptTrigger && (safetyMargin < minSafetyMargin))
						continue;
					double dayScore = (Math.log(days - 0.75)) + 0.375;
					double rangeScore = stock_.hasWeeklyOptions() ? 1.5 : 1.0;
					double callScore = dayScore * rangeScore * (yieldAnnualPct * safetyMargin * 10);
					if (callScoreMin > callScore)
						callScoreMin = callScore;
					if (callScoreMax < callScore)
						callScoreMax = callScore;
					opo.setScoreInternal(callScore);
				} else if (userConfig_.getOptionFlagList().contains(ATOptionSecurity.Put) && ATOptionSecurity.Put.equals(opt.getOptionRight())) {
					if (/* opt.getStrike() > stock_.getPrice3mAvg() || */ opt.getStrike() > upx)
						continue;
					// if("INTC".equals(stock_.getOccId())) {
					// log.info("INTC");
					// }
					strategy = ATOpportunity.PUT;
					priceAsk = opt.getPriceAsk();
					priceMid = optPxMid;
					priceBid = opt.getPriceBid();
					double putYield = 100 * optPxMid / upx;
					yieldAnnualPct = putYield * 365 / days;
					yieldAbsolute = optPxMid;
					double putYieldAbsTheoMax = ((int) (maxBudget / upx)) * optPxMid;
					if (!isOptTrigger && (yieldAnnualPct < minApy || putYieldAbsTheoMax < ccMinYieldAbs))
						continue;
					safetyMargin = 100 * (upx - (opt.getStrike() - optPxMid)) / upx;
					if (!isOptTrigger && (safetyMargin < minSafetyMargin))
						continue;
					double dayScore = (Math.log(days - 0.75)) + 0.375;
					double rangeScore = stock_.hasWeeklyOptions() ? 1.5 : 1.0;
					double putScore = dayScore * rangeScore * (yieldAnnualPct * safetyMargin * 10);
					if (putScoreMin > putScore)
						putScoreMin = putScore;
					if (putScoreMax < putScore)
						putScoreMax = putScore;
					opo.setScoreInternal(putScore);
				} else {
					continue;
				}
				opo.setPriceAsk(round(priceAsk));
				opo.setPriceMid(round(priceMid));
				opo.setPriceBid(round(priceBid));
				opo.setSafetyMgn(round(safetyMargin));
				opo.setYieldAbs(round(yieldAbsolute));
				opo.setYieldPctA(round(yieldAnnualPct));
				opo.setStrategy(strategy);
				opo.setTriggered(isOptTrigger);
				opo.setPeriodicity(stock_.hasWeeklyOptions() ? ATOpportunity.WEEK : ATOpportunity.MONTH);
				String sortId = String.format("%-5s%s%s", stock_.getSymbol(), opt.getOptionRight(), opt.getOccId());
				opo.setSortKey(sortId);
				opportunities.add(opo);
			} catch (Exception ex) {
				log.error(String.format("Error in option [%s]", opt.getOccId()), ex);
			}
		}
		if ("INTC".equals(stock_.getOccId())) {
			log.info(String.format("%s - %d", stock_.getOccId(), opportunities.size()));
		}
		if (opportunities.isEmpty())
			return null;
		// Calculate scores
		double ccScoreMax = Double.MIN_VALUE;
		for (ATOpportunity opo : opportunities) {
			switch (opo.getStrategy()) {
			case (ATOpportunity.CC):
				double scoreInternal = scoreInternal(opo, ccYieldMax, ccSafetyMax);
				double scoreExternal = scoreExternal(opo);
				if (ccScoreMax < scoreInternal)
					ccScoreMax = scoreInternal;
				break;
			case (ATOpportunity.PUT):
				opo.setScoreExternal(opo.getScoreInternal());
				opo.setScoreInternal(putScoreMax != 0 ? 100 * opo.getScoreInternal() / putScoreMax : 0);
				break;
			}
		}

		// https://emojipedia.org/google/
		double pxVar = stock_.getPriceHV() * Math.sqrt(252 / 4) * stock_.getPrice();// (stock_.getPriceStdDev()+stock_.getPriceAtr())/2.0;
		// double pxVar = Math.abs(stock_.getPrice() * stock_.getPriceVolatility() * Math.sqrt(30));
		double varMin = Math.min(stock_.getPriceAtr(), pxVar);
		double pxResist = stock_.getPriceResistance();// stock_.getPriceLast() + pxVar;
		double pxSupport = stock_.getPriceSupport();// stock_.getPriceLast() - pxVar;

		Set<String> flags = new LinkedHashSet<>();
		if (isStockActive) {
			flags.add("🌀");// ⚪☝ 💡💰👍⭐✨⚡⚓☀☑☠💎⏱🔎🚩🏴🏳✍☝;
		}
		if (isStkTrigger) {
			flags.add("⚡");
		}

		// boolean isOpportunity = false;
		// Normalize and sort scores
		Map<Integer, TreeSet<ATOpportunity>> callMap = new HashMap<>();
		Map<Integer, TreeSet<ATOpportunity>> ccMap = new HashMap<>();
		Map<Integer, TreeSet<ATOpportunity>> putMap = new HashMap<>();
		for (ATOpportunity opo : opportunities) {
			switch (opo.getStrategy()) {
			case (ATOpportunity.CALL):
				double cThreshold = Math.min(pxResist - varMin, (pxSupport + pxResist) / 2.0);
				if (stock_.getPriceLast() < cThreshold) {
					// isOpportunity = true;
					flags.add("💰"); // ✨
				}
				addToMap(callMap, opo);
				break;
			case (ATOpportunity.CC):
				double score = 90 * opo.getScoreInternal() / ccScoreMax; // 10% deduct for CC
				opo.setScoreInternal(score);
				addToMap(ccMap, opo);
				break;
			case (ATOpportunity.PUT):
				double pThreshold = Math.min(pxSupport + varMin, (pxSupport + pxResist) / 2.0);
				if (stock_.getPriceLast() < pThreshold) {
					flags.add("💰"); // ✨
				}
				addToMap(putMap, opo);
				break;
			}
		}

		// Generate throttled result_digest list
		List<ATOpportunity> result = new ArrayList<>();

		String industry = StringUtils.isBlank(stock_.getIndustry()) ? stock_.getSector() : stock_.getIndustry();
		industry = StringUtils.isBlank(industry) ? "n/a"
				: industry.length() > 25 ? industry.substring(0, 25) + "…" : industry;
		String label = String.format("%-4s (%tT)", stock_.getLabel(), stock_.getLastUpdate());
		String metaStr = String.format("(%5.2f - %5.2f ⬍ %5.2f)", pxSupport, pxResist, pxVar);
		Map<String, String> meta = new HashMap<>();
		meta.put("range", metaStr);
		ATOpportunity header = new ATOpportunity();
		header.setTimestamp(new Date());
		header.setSymbol(stock_.getSymbol());
		header.setOccId(stock_.getOccId());
		header.setIsActive(isStockActive);
		header.setLabel(label);
		header.setPriceAsk(stock_.getPriceAsk());
		header.setPriceBid(stock_.getPriceBid());
		header.setPriceClose(stock_.getPriceClose());
		header.setPriceMid(stock_.getPriceLast());
		header.setStrategy(ATOpportunity.HEADER);
		header.setScoreInternal(-1.0);
		header.setScoreExternal(-1.0);
		header.setMeta(_gson.toJson(meta));
		// header.setMeta(meta);
		header.setTriggered(isStkTrigger);
		header.setIndustry(industry);
		header.setFlags(String.join("", flags));
		String sortId = String.format("%-5s%s%s", stock_.getSymbol(), " ", stock_.getOccId());
		header.setSortKey(sortId);

		header.setPeriodicity(stock_.hasWeeklyOptions() ? ATOpportunity.WEEK : ATOpportunity.MONTH);
		// header.setPeriodicity(stock_.hasWeeklyOptions() ? "week" : "month");
		result.add(header);
		for (TreeSet<ATOpportunity> set : callMap.values()) {
			Iterator<ATOpportunity> i2 = set.descendingIterator();
			if (i2.hasNext()) {
				ATOpportunity opo = i2.next();
				result.add(opo);
			}
		}
		for (TreeSet<ATOpportunity> set : ccMap.values()) {
			Iterator<ATOpportunity> i2 = set.descendingIterator();
			if (i2.hasNext()) {
				ATOpportunity opo = i2.next();
				result.add(opo);
			}
		}
		for (TreeSet<ATOpportunity> set : putMap.values()) {
			Iterator<ATOpportunity> i2 = set.descendingIterator();
			if (i2.hasNext()) {
				ATOpportunity opo = i2.next();
				result.add(opo);
			}
		}
		if (result.size() == 1) {
			log.info("RESULT SIZE ONE!");
		}
		return result;
	}

	private void addToMap(Map<Integer, TreeSet<ATOpportunity>> map_, ATOpportunity opportunity_) {
		TreeSet<ATOpportunity> set = map_.get(opportunity_.getDays());
		if (set == null) {
			set = new TreeSet<>(new Comparator<ATOpportunity>() {
				@Override
				public int compare(ATOpportunity o1_, ATOpportunity o2_) {
					return Double.compare(o1_.getScoreInternal(), o2_.getScoreInternal());
				}
			});
			map_.put(opportunity_.getDays(), set);
		}
		set.add(opportunity_);

	}

	// TODO: Use geometric mean for scoring based on orthogonal properties https://en.wikipedia.org/wiki/Geometric_mean
	private double scoreInternal(ATOpportunity opo_, Double annualYieldMax_, Double safetyMarginMax_) {
		// Integer daysLeft = opo_.getDays();
		Double yieldA = opo_.getYieldPctA();
		Double smarg = opo_.getSafetyMgn();
		// int days = daysLeft != null && daysLeft > 0 ? daysLeft : 1;
		// double dayScore = (Math.log(days - 0.75) / Math.log(7)) + 1;
		double yield = yieldA != null ? yieldA : 0;
		double yieldScore = yield / annualYieldMax_;
		double margin = smarg != null ? smarg : 0;
		// double score1 = Math.pow(margin, 2.5);
		Double marginScore = Math.pow(margin, 2.5) / Math.pow(safetyMarginMax_, 2.5);
		if (marginScore.isNaN()) {
			log.warn(String.format("Algo error: margin score NaN [%s]", opo_.getOccId()));
			marginScore = 0.0;
		}
		Double score = (yieldScore + marginScore);
		score = score.isNaN() ? 0 : score;
		opo_.setScoreInternal(score);
		return score;
	}

	private double scoreExternal(ATOpportunity opo_) {
		Integer daysLeft = opo_.getDays();
		Double yieldA = opo_.getYieldPctA();
		Double smarg = opo_.getSafetyMgn();
		int days = daysLeft != null && daysLeft > 0 ? daysLeft : 1;
		double dayScore = (Math.log(days - 0.75) / Math.log(7)) + 1;
		double yield = yieldA != null ? yieldA : 0;
		double yieldScore = yield;// / annualYieldMax_;
		double margin = smarg != null ? smarg : 0;
		Double marginScore = Math.pow(margin, 2.5);// / Math.pow(safetyMarginMax_, 2.5);
		if (marginScore.isNaN()) {
			log.warn(String.format("Algo error: margin score NaN [%s]", opo_.getOccId()));
			marginScore = 0.0;
		}
		double score = dayScore * (yieldScore + marginScore);
		opo_.setScoreExternal(score);
		return score;
	}

	private Double round(Double d_) {
		if (d_ == null)
			return null;
		Double d = (double) Math.round(d_ * 1000d) / 1000d;
		return d;
	}

	@Override
	public Map<ATOptionSecurity, Map<String, Set<ATRoll>>> getRolls(ATOptionSecurity option_, ATStock underlying_) {
		Collection<ATOptionSecurity> options = underlying_ != null ? _store.getOptionChain(underlying_.getOccId()) : null;
		if (option_ == null || options == null)
			return null;
		Map<String, Set<ATRoll>> rolls = new HashMap<>();
		double strike = option_.getStrike();
		// Note: Sorted by Date, then Strike
		ATOptionSecurity low = new ATOptionSecurity();
		low.setStrike(Double.MIN_VALUE);
		ATOptionSecurity high = new ATOptionSecurity();
		high.setStrike(Double.MAX_VALUE);
		ATOptionSecurity match = null;
		ATOptionSecurity base = null;
		log.info(String.format("Processing rolls [%s]", option_.getLabel()));
		int currentDays = -1;
		// Set<ATOptionSecurity> rolls = new TreeSet<>();
		for (ATOptionSecurity o : options) {
			int days = (int) TimeUnit.DAYS.convert(
					o.getExpirationDate().getTime() - option_.getExpirationDate().getTime(), TimeUnit.MILLISECONDS);
			// if (days > _atConfig.getMaxOptionDays())
			if (days > 82)
				break;
			if (option_.getOccId().equals(o.getOccId())) {
				base = o;
			}
			if (days <= 0 || o.getStrike() == null || !option_.getOptionRight().equals(o.getOptionRight()))
				continue;

			if (currentDays > 0 && currentDays != days) {
				if (low.getStrike() != Double.MIN_VALUE) {
					addToRolls(rolls, base, low, -1);
					// rolls.add(low);
				}
				if (match != null) {
					addToRolls(rolls, base, match, 0);
					// rolls.add(match);
				}
				if (high.getStrike() != Double.MAX_VALUE) {
					addToRolls(rolls, base, high, 1);
					// rolls.add(high);
				}
				low = new ATOptionSecurity();
				low.setStrike(Double.MIN_VALUE);
				match = null;
				high = new ATOptionSecurity();
				high.setStrike(Double.MAX_VALUE);
			}
			currentDays = days;
			if (o.getStrike() > low.getStrike() && o.getStrike() < strike) {
				low = o;
			} else if (o.getStrike() == strike) {
				match = o;
			} else if (o.getStrike() < high.getStrike() && o.getStrike() > strike) {
				high = o;
			}
		}
		if (low.getStrike() != Double.MIN_VALUE) {
			addToRolls(rolls, base, low, -1);
			// rolls.add(low);
		}
		if (match != null) {
			addToRolls(rolls, base, match, 0);
			// rolls.add(match);
		}
		if (high.getStrike() != Double.MAX_VALUE) {
			addToRolls(rolls, base, high, 1);
			// rolls.add(high);
		}
		// log.info("opt: " + rolls);
		if (base == null || rolls.isEmpty())
			return null;

		Map<ATOptionSecurity, Map<String, Set<ATRoll>>> result = new HashMap<>();
		result.put(base, rolls);
		return result;
	}

	private boolean addToRolls(Map<String, Set<ATRoll>> rolls_, ATOptionSecurity base_, ATOptionSecurity roll_, int offset_) {
		if (rolls_ == null || base_ == null || roll_ == null || base_.getPriceAsk() == null
				|| roll_.getPriceBid() == null)
			return false;

		String expDate = _stdDate.format(roll_.getExpirationDate());
		Set<ATRoll> rolls = rolls_.get(expDate);
		if (rolls == null) {
			rolls = new TreeSet<>();
			rolls_.put(expDate, rolls);
		}

		double ratio = 0.5;
		double ask = (base_.getPriceAsk() + ratio * base_.getPriceBid()) / (1 + ratio);
		double bid = (roll_.getPriceBid() + ratio * roll_.getPriceAsk()) / (1 + ratio);
		double abs = bid - ask;
		double pct = 100 * abs / roll_.getStrike();
		int days = (int) TimeUnit.DAYS.convert(
				roll_.getExpirationDate().getTime() - base_.getExpirationDate().getTime(), TimeUnit.MILLISECONDS) + 1;
		ATYield yield = new ATYield();
		yield.setAbs(abs); // base_.getContractSize() *
		yield.setApy(360 * pct / days);

		ATRoll roll = new ATRoll();
		roll.setOffset(offset_);
		roll.setOption(roll_);
		roll.setYield(yield);
		rolls.add(roll);
		return true;
	}

	// http://www.bearcave.com/software/java/lattice/BinomialLattice.java
	public void priceOptions(ATStock stock_) {
		if (stock_ == null)
			return;

		Date sot = ATDateUtil.getStartOfToday();
		Date end = DateUtils.addMonths(sot, 1);
		Date start = DateUtils.addWeeks(end, -1);
		Collection<ATOptionSecurity> options = _store.getOptionChain(stock_.getOccId());
		Double minStrike = Double.MIN_VALUE;
		Double maxStrike = Double.MAX_VALUE;
		// Get closest option wrap
		// Date date = null;
		for (ATOptionSecurity option : options) {
			if (start.after(option.getExpirationDate()) || end.before(option.getExpirationDate()))
				continue;
			// date = option.getExpirationDate();
			// log.info(String.format("Strike [%.2f]", option.getStrike()));
			if (option.getStrike() > stock_.getPrice() && option.getStrike() < maxStrike)
				maxStrike = option.getStrike();
			if (option.getStrike() < stock_.getPrice() && option.getStrike() > minStrike)
				minStrike = option.getStrike();
		}

		// Double s = stock_.getPriceStdDev() / stock_.getPrice();
		// log.info(String.format("Strikes [%.2f < %.2f < %.2f]", minStrike, stock_.getPrice(), maxStrike));
		// Double u = Math.exp(s * Math.sqrt(1 / 12.0));
		// Double d = Math.exp(-s * Math.sqrt(1 / 12.0));
		//
		// log.info(String.format("Price [%.2f - %.2f / %.2f - %.2f]", stock_.getPrice(), stock_.getPriceStdDev(),
		// stock_.getPrice() - stock_.getPriceStdDev(), stock_.getPrice() + stock_.getPriceStdDev()));
		// // double px = (75.0 * Math.exp(0.060 * 1.0 / 12.0) - 63.0) / (95.0 - 63.0);
		// // e^(rfr/12) 1.0008670423307395604870976295524
		// // 0.21493212943626327058589744220532
		// Double rfr = 1.04 / 100.0;
		// // Double p = (stock_.getPrice() * Math.exp(rfr / 12.0) - minStrike) / (maxStrike - minStrike);
		// Double p = (stock_.getPrice() * Math.exp(rfr / 12.0) - (stock_.getPrice() - stock_.getPriceStdDev()))
		// / (2 * stock_.getPriceStdDev());
		//
		// Double call = p * (maxStrike - minStrike) * Math.exp(rfr / 12.0);
		// log.info(String.format("Call [%s %.2f / %.2f - %.4f]", ATFormats.DATE_yyyy_MM_dd.format(date), minStrike,
		// call, p));
	}

	static public Double calculateProbability(String occId_, Double pxUl_, Double hv_, ATOptionDescription option_, Instant now_) {
		if (pxUl_ == null) {
			log.debug(String.format("Missing underlying Px [%-5s]", occId_));
			return null;
		}
		if (hv_ == null) {
			log.debug(String.format("Missing HV [%-5s]", occId_));
			return null;
		}
		double zFactor = 1.0; // TODO: Consider adjusting for earnings etc
		long days = ChronoUnit.DAYS.between(now_, option_.getExpiration().toInstant()) + 1;
		double stdev = pxUl_ * hv_ * Math.sqrt(days / 365.0);
		double z = Math.abs(pxUl_ - option_.getStrike()) / (stdev * zFactor);
		int pDir = Right.Put == option_.getRight() ? 1 : -1;
		int pItm = option_.getStrike() < pxUl_ ? 1 : -1;
		double p = (pItm * pDir) > 0 ? ATStandardNormalTable.getProbabilityGreaterThan(z)
				: ATStandardNormalTable.getProbabilityLessThan(z);
		return p;
	}

	static public Double calculateMargin(String occId_, Double pxUl_, Double count_, ATOptionDescription option_) {
		if (pxUl_ == null) {
			log.debug(String.format("Missing underlying Px [%-5s]", occId_));
			return null;
		}
		double strike = option_.getStrike();
		double margin = (strike - pxUl_) / strike;
		double sign = Math.signum(count_);
		if (Right.Call == option_.getRight())
			sign = -sign;
		margin = sign * margin;
		return margin;
	}

	static private Double _riskFreeInterest;
	static private int _riskFreeInterestCount;

	static private Double getRiskFreeInterest() {
		_riskFreeInterestCount++;
		if (_riskFreeInterest == null || _riskFreeInterestCount > 100) {
			_riskFreeInterestCount = 0;
			_riskFreeInterest = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
		}
		return _riskFreeInterest;
	}

	static public Double calculateTheoreticalPrice(ATOptionDescription option_, Double volatility_, Double div_, Integer divFreq_, Double pxUl_) {
		if (pxUl_ == null)
			return null;
		// if (fnd != null) {
		// Double iv = fnd.getIV();
		// double volatility = iv != null ? iv : fnd.getHV60();
		// Double div = fnd.getDividend();
		// Integer divFq = fnd.getDividendFrequency();
		double interest = getRiskFreeInterest();
		double yield = div_ == null || divFreq_ == null ? 0.0 : div_ * divFreq_ / pxUl_;
		// ATOptionDescription option = new ATOptionDescription(sec.getSymbol(), sec.getExpirationDate(), strike, right);
		Map<String, ATOptionAnalytics> analyticsMap = ATQuantLib.instance().priceAmericanEquityOption(pxUl_, volatility_, yield, interest,
				option_);
		ATOptionAnalytics analytics = analyticsMap != null ? analyticsMap.get(option_.getOccId()) : null;
		return analytics != null ? analytics.getPrice() : null;

	}

}
