package com.isoplane.at.source;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.time.DateUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.service.IATUSTreasuryRateProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.ext.quantlib.ATOptionAnalytics;
import com.isoplane.at.ext.quantlib.ATQuantLib;
import com.isoplane.at.marketdata.model.ATMongoMarketData;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.model.ATMongoTrade;
import com.isoplane.at.portfolio.model.ATTradeModelTool;
import com.isoplane.at.source.adapter.ATUSTreasuryAdapter;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;

public class ATTestDataManager {

	static final Logger log = LoggerFactory.getLogger(ATTestDataManager.class);

	static private final String TEST = "test";
	static private final String TEST_FLAG = "test_flag";
	static private final String TEST_ID = "test_id";
	static private Random random = new Random(System.currentTimeMillis());
	static private ATTestDataManager instance;

	// private Configuration _config;
	private ATMongoDao _dao;
	private Map<String, ATMongoMarketData> _marketDataMap;
	private Map<String, ATPersistentEquityFoundation> _foundationMap;
	private Map<String, ATMongoTrade> _portfolioMap;
	private Random _random;
	// private ATQuantLib _quantLib;
	private int _notificationCounter;
	// private ATMarketDataManager _marketDataManager;

	private String _foundationTable;
	private String _mktTable;
	private String _tradeTable;

	public static void main(String[] args_) {
		try {
			// String[] propertiesPaths = args_[0].split(",");
			// CompositeConfiguration cconfig = new CompositeConfiguration();
			// for (String path : propertiesPaths) {
			// log.info(String.format("Reading properties [%s]", path));
			// FileBasedConfigurationBuilder<PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<PropertiesConfiguration>(
			// PropertiesConfiguration.class).configure(
			// new Parameters().fileBased()
			// .setListDelimiterHandler(new DefaultListDelimiterHandler(','))
			// .setFile(new File(path)));
			// PropertiesConfiguration config = builder.getConfiguration();
			// // PropertiesConfiguration config = new Configurations().properties(new File(path));
			// cconfig.addConfiguration(config);
			// }
			ATConfigUtil.init(args_[0], false);
			ATExecutors.init();
			ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
			IATUSTreasuryRateProvider ustrp = new ATUSTreasuryAdapter();
			ATSourceServiceRegistry.setUSTreasuryRateProvider(ustrp, true);

			instance = new ATTestDataManager();
			instance.init();

			ATSysUtils.waitForEnter();
			ATExecutors.shutdown();
			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Fatal error in %s", ATTestDataManager.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATTestDataManager() {
		// _config = config_;
		_random = new Random(System.currentTimeMillis());
		_marketDataMap = new HashMap<>();
		_foundationMap = new HashMap<>();
		_portfolioMap = new HashMap<>();
		_dao = new ATMongoDao();

		_foundationTable = ATMongoTableRegistry.getFoundationTable();
		_mktTable = ATMongoTableRegistry.getMarketDataTable();
		_tradeTable = ATMongoTableRegistry.getTradeTable();
		// _quantLib = new ATQuantLib(_config);
		// _marketDataManager = new ATMarketDataManager(config_, false);
	}

	public void init() {
		deleteTestData();
		generateTestFundamentals();
		generateTestMarketData();
		generateTestPortfolio();
		startMarketDataRandomizer();
	}

	public void shutdown() {
		deleteTestData();
	}

	private void deleteTestData() {
		deleteTestPortfolio();
		deleteTestMarketData();
		deleteTestFundamentals();
	}

	private void startMarketDataRandomizer() {
		Configuration config = ATConfigUtil.config();
		final int alertCounter = config.getInt("alert.counter");
		long interval = config.getLong("market.period");
		// Derivatives
		String threadId = "test_marketdata_derivatives";
		ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(threadId);
				List<String> list = new ArrayList<>(_marketDataMap.keySet());
				String id;
				do {
					int index = _random.nextInt(list.size());
					id = list.get(index);
				} while (id.length() < 10);
				ATMongoMarketData data = _marketDataMap.get(id);
				randomizeMarketData(data);
				log.info(String.format("Randomized market data [%s]", data));
				_dao.fastBulkInsert(_mktTable, Arrays.asList(data));
				// _marketDataManager.update(data);
				_notificationCounter++;
				if (_notificationCounter > alertCounter) {
					_notificationCounter = 0;
					ATSysUtils.printNotice();
				}
			}
		}, interval, interval);
		// Equities
		ATExecutors.scheduleAtFixedRate("test_marketdata_equities", new Runnable() {

			@Override
			public void run() {
				List<String> list = new ArrayList<>(_marketDataMap.keySet());
				String id;
				do {
					int index = _random.nextInt(list.size());
					id = list.get(index);
				} while (id.length() > 10);
				ATMongoMarketData data = _marketDataMap.get(id);
				randomizeMarketData(data);
				log.info(String.format("Randomized market data [%s]", data));
				_dao.fastBulkInsert(_mktTable, Arrays.asList(data));
				// _marketDataManager.update(data);
				_notificationCounter++;
				if (_notificationCounter > alertCounter) {
					_notificationCounter = 0;
					ATSysUtils.printNotice();
				}
			}
		}, interval * 2, interval * 2);

	}

	private void generateTestPortfolio() {
		Configuration config = ATConfigUtil.config();
		String testUuid = config.getString("test.uuid");
		int testSize = config.getInt("test.portfolioSize");
		testSize = Math.min(testSize, _marketDataMap.size());
		ArrayList<String> occIds = new ArrayList<>(_marketDataMap.keySet());
		Integer previousId = null;
		do {
			int id = previousId == null || random.nextDouble() > 0.75 ? random.nextInt(occIds.size()) : previousId;
			String occId = occIds.get(id);
			occIds.remove(occId);
			ATMongoMarketData market = _marketDataMap.get(occId);
			ATMongoTrade trade = createTrade(testUuid, _portfolioMap.size(), market);
			_portfolioMap.put(trade.getAtId(), trade);
		} while (_portfolioMap.size() < testSize);

		@SuppressWarnings("unchecked")
		Collection<Document> data = (Collection<Document>) (Object) _portfolioMap.values();
		_dao.fastBulkInsert(_tradeTable, data);
		log.info(String.format("Created [%4d] test positions", _portfolioMap.size()));
	}

	private void deleteTestPortfolio() {
		IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(TEST_FLAG, TEST);
		long deleteCount = _dao.delete(_tradeTable, deleteQuery);
		log.info(String.format("Deleted [%4d] test positions", deleteCount));
	}

	private void generateTestMarketData() {
		Configuration config = ATConfigUtil.config();
		List<Integer> optionDeltas = config.getList(Integer.class, "option.deltas");
		List<String> optionRights = config.getList(String.class, "option.rights");
		List<Double> optionStrikes = config.getList(Double.class, "option.strikes");// Arrays.asList(-5.0, 0.0, 5.0);
		int size = config.getInt("base.size");
		double interest = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
		LocalDate now = LocalDate.now();
		LocalDate localFriday = now.with(TemporalAdjusters.next(DayOfWeek.FRIDAY));
		Date nextfriday = java.sql.Date.valueOf(localFriday);
		for (int i = 1; i <= size; i++) {
			String symbol = String.format("T#%02d", i);
			Double px = i * 10.0;
			ATMongoMarketData underlying = createMarketData(i, symbol, px);
			randomizeMarketData(underlying);
			_marketDataMap.put(symbol, underlying);
			// _marketDataManager.update(underlying);
			// _dao.update(tableId, underlying);
			log.info(String.format("Creating market data [%s]", underlying));
			for (Integer expDelta : optionDeltas) {
				Date expDate = DateUtils.addDays(nextfriday, expDelta * 7);
				for (Double strikeOffset : optionStrikes) {
					double strike = px + strikeOffset;
					for (String right : optionRights) {
						String oSymbol = ATFormats.toOccId(symbol, expDate, right, strike);
						Double oPx = priceOption(symbol, px, oSymbol, interest);
						if (oPx == null) {
							log.info(String.format("Couldn't price [%s]", oSymbol));
							continue;
						}
						ATMongoMarketData option = createMarketData(i, oSymbol, oPx);
						randomizeMarketData(option);
						_marketDataMap.put(oSymbol, option);
						// _marketDataManager.update(option);
						// _dao.update(tableId, option);
						log.info(String.format("Creating market data [%s]", option));
					}
				}
			}
		}
		@SuppressWarnings("unchecked")
		Collection<Document> data = (Collection<Document>) (Object) _marketDataMap.values();
		_dao.fastBulkInsert(_mktTable, data);

		log.info(String.format("Created [%4d] test market data", _marketDataMap.size()));
	}

	private void deleteTestMarketData() {
		IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(TEST_FLAG, TEST);
		long deleteCount = _dao.delete(_mktTable, deleteQuery);
		log.info(String.format("Deleted [%4d] test market data", deleteCount));
	}

	private ATMongoMarketData createMarketData(int index_, String symbol_, double px_) {
		ATMongoMarketData data = new ATMongoMarketData();
		data.put(TEST_ID, index_);
		data.put(TEST_FLAG, TEST);
		data.setMongoId(symbol_);
		data.setAtId(symbol_);
		data.setPxOpen(px_);
		data.setPxClose(px_);
		data.setPxClosePrev(px_);
		data.setPxLast(px_);
		data.setPxAsk(px_);
		data.setPxBid(px_);
		data.setPxHigh(px_);
		data.setPxLow(px_);
		return data;
	}

	private ATMongoTrade createTrade(String uuid_, int index_, ATMongoMarketData data_) {
		Configuration config = ATConfigUtil.config();
		ATMongoTrade trade = new ATMongoTrade();
		// trade.setAtId(value_);
		trade.setOccId(data_.getAtId());
		double pxDelta = random.nextDouble() - 0.5; // Random [-0.5,+0.5]
		double pxTrade = Math.max(0.10, data_.getPxLast() * (1 + pxDelta / 4)); // Random pxLast +- 25%
		trade.setPxTrade(pxTrade);
		double szTrade = random.nextInt(config.getInt("test.maxTradeSize")) * (data_.getAtId().length() < 10 ? 100 : 1);
		szTrade = Math.max(1.0, szTrade) * ((random.nextDouble() - 0.5) > 0 ? 1.0 : -1.0);
		trade.setSzTrade(szTrade);
		trade.setPxTradeTotal(-trade.getSzTrade() * trade.getPxTrade());
		String account = String.format("%s_%d", TEST, random.nextDouble() < 0.5 ? 1 : 2);
		trade.setTradeAccount(account);
		trade.setTradeComment("Test Trade (remove)");
		trade.setTradeUserId(uuid_);
		Date tradeDate = DateUtils.addDays(new Date(), -random.nextInt(config.getInt("test.maxTradeDays")));
		trade.setTsTrade(tradeDate);

		trade.setTradeType(9999);

		String id = ATTradeModelTool.atId(trade);
		id = id.replaceFirst("T", "t");
		trade.setAtId(id);
		trade.put(TEST_ID, index_);
		trade.put(TEST_FLAG, TEST);
		return trade;
	}

	private Double priceOption(String symbolUL_, Double pxUL_, String occId_, Double riskFreeInterest_) {
		try {
			// if("T#01 190419C00005000".equals(occId_)) {
			// log.info("hello");
			// }
			ATPersistentEquityFoundation fnd = _foundationMap.get(symbolUL_);
			if (fnd == null) {
				log.info(String.format("Missing foundation [%s]", symbolUL_));
				return null;
			}
			Double iv = fnd.getIV();
			double volatility = iv != null ? iv : fnd.getHV60();
			Double div = fnd.getDividend();
			Integer divFq = fnd.getDividendFrequency();
			double yield = div == null || divFq == null ? 0.0 : div * divFq / pxUL_;
			ATOptionDescription option = ATOptionDescription.create(occId_);
			Map<String, ATOptionAnalytics> analyticsMap = ATQuantLib.instance().priceAmericanEquityOption(pxUL_, volatility, yield,
					riskFreeInterest_, option);
			ATOptionAnalytics analytics = analyticsMap != null ? analyticsMap.get(option.getOccId()) : null;
			return analytics != null ? analytics.getPrice() : null;
		} catch (Exception ex) {
			log.error(String.format("Error pricing [%s]", occId_), ex);
			return null;
		}
	}

	private void randomizeMarketData(ATMongoMarketData data_) {
		double factor = changeFactor(0.05, false);
		double last = Math.max(0.10, data_.getPxLast() * (1 + factor));
		data_.setPxLast(last); // Note: open is static
		double pxAsk = last * (1 + changeFactor(0.01, false));
		double pxBid = last * (1 + changeFactor(0.01, false));
		// double pxAsk = adjustPrice(data_.getPxLast());
		// double pxBid = adjustPrice(data_.getPxLast());
		data_.setPxAsk(Math.max(pxAsk, pxBid));
		data_.setPxBid(Math.min(pxAsk, pxBid));
		Double high = data_.getPxHigh();
		if (high == null || high < data_.getPxLast()) {
			high = data_.getPxLast();
		}
		data_.setPxHigh(high * (1 + changeFactor(0.01, true)));
		Double low = data_.getPxLow();
		if (low == null || low > data_.getPxLast()) {
			low = data_.getPxLast();
		}
		data_.setPxLow(low * (1 - changeFactor(0.01, true)));
	}

	// private Double adjustPrice(Double px_) {
	// double factor = generateChangeFactor(0.05, false);
	// Double px = Math.max(0.05, px_ * (1 + factor));
	// return px;
	// }

	// rangeFactor limits return value. For rf = 0.2 limits random to 20%.
	private Double changeFactor(double rangeFactor_, boolean positiveOnly_) {
		int flag = positiveOnly_ ? 1 : _random.nextInt(10) < 5 ? -1 : 1;
		double fraction = flag * _random.nextDouble() * rangeFactor_;
		return fraction;
	}

	private void generateTestFundamentals() {
		int size = ATConfigUtil.config().getInt("fnd.size");
		Date now = new Date();
		for (int i = 1; i <= size; i++) {
			String symbol = String.format("T#%02d", i);
			ATPersistentEquityFoundation fnd = new ATPersistentEquityFoundation(symbol);
			fnd.put(IATAssetConstants.DESCRIPTION, String.format("Test Data [%d]", i));
			fnd.put(IATAssetConstants.SECTOR, String.format("Test Sector [%d]", i));
			fnd.put(IATAssetConstants.INDUSTRY, String.format("Test industry [%d]", i));
			double px = i * 10.0;
			fnd.put(IATAssetConstants.PX_LAST, px);
			fnd.put(IATAssetConstants.PX_CLOSE, px);
			fnd.put(IATAssetConstants.PX_OPEN, px);
			fnd.put(IATAssetConstants.MARKET_CAP, i * 1000000.0);
			fnd.put(IATStatistics.SMA60, px);
			fnd.put(IATStatistics.HV60, i * 0.01);
			fnd.put(IATAssetConstants.IV, i * 0.01);
			Date earn = DateUtils.addDays(now, i + 15);
			fnd.put(IATAssetConstants.EARNINGS_DATE_STR_OLD, ATFormats.DATE_yyyyMMdd.get().format(earn));
			Date exdiv = DateUtils.addDays(now, i);
			fnd.put(IATAssetConstants.DIV_EX_DATE_STR_OLD, ATFormats.DATE_yyyyMMdd.get().format(exdiv));
			fnd.put(IATAssetConstants.DIVIDEND, i * 0.25);
			fnd.put(IATAssetConstants.DIV_FREQUENCY, 4);
			fnd.put(IATAssetConstants.MODEL, TEST);
			fnd.put(TEST_FLAG, TEST);
			fnd.put(IATAssetConstants.INDUSTRY, TEST + "_ind_" + i);
			fnd.put(IATAssetConstants.SECTOR, TEST + "_sct_" + i);
			fnd.put(TEST_ID, i);
			log.info(String.format("Creating foundation [%s]", fnd));
			_foundationMap.put(symbol, fnd);
			_dao.update(_foundationTable, fnd);
		}
		log.info(String.format("Created [%4d] test foundations", _foundationMap.size()));
	}

	private void deleteTestFundamentals() {
		IATWhereQuery deleteQuery = ATPersistenceRegistry.query().eq(TEST_FLAG, TEST);
		long deleteCount = _dao.delete(_foundationTable, deleteQuery);
		log.info(String.format("Deleted [%4d] test foundations", deleteCount));
	}

}
