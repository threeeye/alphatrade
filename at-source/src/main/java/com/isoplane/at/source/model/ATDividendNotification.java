package com.isoplane.at.source.model;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.util.ATFormats;

public class ATDividendNotification extends ATBaseNotification implements IATCorporateEventNotification {

	private String symbol;
	private Date exDate;
	
	public ATDividendNotification() {
		super.setSilent(true);
	}

	@Override
	public String getNotificationKey() {
		String key = String.format("exdiv %s", getSymbol());
		return key;
	}

	// Symbols https://www.compart.com/en/unicode/mirrored
	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATDividendNotification oldDiv = oldB != null && oldB instanceof ATChangeNotification
				? (ATDividendNotification) oldB
				: null;

		if (oldDiv != null && DateUtils.isSameDay(oldDiv.getDate(), this.getDate())) {
			return null;
		}
		Date exDate = this.getExDate();
		String title = String.format("%1s ⪾ %2s", this.getSymbol(), // ∂ ⨮  ⪾
				ATFormats.DATE_MM_dd.get().format(exDate));
		String body = String.format("⚠ %s • Ex-Div • %s", this.getLabel().replaceAll(" +", " "), ATFormats.DATE_yyyy_MM_dd.get().format(exDate));
		// String body = String.format("Ex-Div ⚠ %s \n· alphatra.de · %tT", this.getLabel().replaceAll(" +", " "), new Date());
		String iconStar = this.getUserIds().isEmpty() ? "" : "_star";
		String icon = String.format("/globe%s.ico", iconStar);

		return new ATNotificationContent(title, body, icon);
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Date getExDate() {
		return exDate;
	}

	public void setExDate(Date exDate) {
		this.exDate = exDate;
	}

	@Override
	public String getNotificationTag() {
		String dStr = ATFormats.DATE_yyMMdd.get().format(getExDate());
		return String.format("div_%s_%s", getSymbol(), dStr);//.hashCode() + "");
	}

	@Override
	public String getType() {
		return "dividend";
	}

}
