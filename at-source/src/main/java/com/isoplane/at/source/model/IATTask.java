package com.isoplane.at.source.model;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.isoplane.at.commons.store.IATLookupMap;

public interface IATTask extends Map<String, Object> {

	// static final String ID = "__id";
	static final int STATUS_ERROR = -1;
	static final int STATUS_NEW = 0;
	static final int STATUS_COMPLETE = 999;

	static final String STATUS = "status";
	static final String DATA = "data";
	static final String CODE = "code";
	static final String SOURCE = "src";
	static final String LAST = "last";

	String getAtId();

	int getStatus();

	void setStatus(int value);

	String getCode();

	void setCode(String value);

	String getData();

	void setData(String value);

	String getSource();

	void setSource(String value);

	String getLastAccess();

	void setLastAccess(String value);

	String serialize(Gson gson);

	static String serializeTask(Gson gson_, IATTask task_) {
		if (task_ == null)
			return null;
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(IATTask.CODE, task_.getCode());
		map.put(IATTask.DATA, task_.getData());
		map.put(IATTask.SOURCE, task_.getSource());
		map.put(IATTask.STATUS, task_.getStatus());
		String json = gson_.toJson(map);
		return json;
	}

	static String createAtId(IATTask task_) {
		String id = String.format("%s:%s:%d", getCode(task_), getSource(task_), getData(task_).hashCode());
		IATLookupMap.setAtId(task_, id);
		return id;
	}

	static int getStatus(Map<String, Object> map_) {
		Integer value = (Integer) map_.get(STATUS);
		return value == null ? 0 : value;
	}

	static void setStatus(Map<String, Object> map_, Integer value_) {
		if (value_ == null) {
			map_.remove(STATUS);
		} else {
			map_.put(STATUS, value_);
		}
	}

	static String getCode(Map<String, Object> map_) {
		String value = (String) map_.get(CODE);
		return value;
	}

	static void setCode(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(CODE);
		} else {
			map_.put(CODE, value_);
		}
	}

	static String getData(Map<String, Object> map_) {
		String value = (String) map_.get(DATA);
		return value;
	}

	static void setData(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(DATA);
		} else {
			map_.put(DATA, value_);
		}
	}

	static String getSource(Map<String, Object> map_) {
		String value = (String) map_.get(SOURCE);
		return value;
	}

	static void setSource(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(SOURCE);
		} else {
			map_.put(SOURCE, value_);
		}
	}

	static String getLastAccess(Map<String, Object> map_) {
		String value = (String) map_.get(LAST);
		return value;
	}

	static void setLastAccess(Map<String, Object> map_, String value_) {
		if (value_ == null) {
			map_.remove(LAST);
		} else {
			map_.put(LAST, value_);
		}
	}

	static int hashCode(IATTask task_) {
		String id = task_.getAtId();
		return (id != null ? id : "").hashCode();
	}

	static boolean equals(IATTask this_, Object other_) {
		if (other_ instanceof IATTask) {
			return this_.getAtId().equals(((IATTask) other_).getAtId());
		} else
			return false;
	}

}
