package com.isoplane.at.source.model;

import org.apache.commons.lang3.StringUtils;

public class TdnAlertGroup {

	public static class TdnAlert {
		transient public String userId; // Careful - also ignored by Gson
		transient public String occId;
		public Boolean act;
		public String msg;
		public String prp;
		public String opr;
		public String val;

		public boolean cleanup() {
			if (StringUtils.isBlank(userId) || StringUtils.isBlank(occId) || StringUtils.isBlank(prp) || StringUtils.isBlank(opr)
					|| StringUtils.isBlank(val))
				return false;
			prp = prp.trim().toUpperCase();
			opr = opr.trim().toUpperCase();
			return true;
		}

		@Override
		public String toString() {
			String str = String.format("%s|%s|%s|%s|%s", userId, occId, prp, opr, val);
			return str;
		}

	}
}
