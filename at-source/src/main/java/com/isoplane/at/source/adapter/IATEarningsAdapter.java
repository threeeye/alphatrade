package com.isoplane.at.source.adapter;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import org.apache.http.annotation.Obsolete;

import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.source.model.ATEarningOld;

@Obsolete
public interface IATEarningsAdapter {

	/** Used for daily update task */
	Map<String, TreeSet<ATEarningOld>> getEarnings(Collection<ATStock> securities);

	/**
	 * Used for individual ad-hoc update. May return null if force == false and implementation only supports unspecified
	 * bulk.
	 */
	TreeSet<ATEarningOld> getEarnings(ATStock security, boolean force);

}
