package com.isoplane.at.source.model;

import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.IATEquityFoundation;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistentPosition;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;

public class TdnConverter {

	static final Logger log = LoggerFactory.getLogger(TdnConverter.class);

	static public TdnSecurity toTdnSecurity(ATPersistentSecurity sec_, IATEquityFoundation fnd_, Set<ATPersistentPosition> positions_,
			String avgMode_, String hvMode_, String refDS8_) {
		try {
			TdnSecurity tdn = new TdnSecurity();
			tdn.id = sec_.getOccId();
			tdn.description = sec_.getDescription();
			tdn.px = sec_.getPrice();
			tdn.pxOpen = sec_.getPriceOpen();
			tdn.pxClose = sec_.getPriceClose();
			tdn.pxAsk = sec_.getPriceAsk();
			tdn.pxBid = sec_.getPriceBid();
			tdn.pxLoD = sec_.getPriceLow();
			tdn.pxHiD = sec_.getPriceHigh();
			tdn.ts = sec_.getUpdateTime();
			if (fnd_ == null) {
				String name = (String) sec_.get("name");
				if (StringUtils.isNoneBlank(name)) {
					tdn.name = name;
				}
			} else {
				tdn.name = fnd_.getDescription();
				tdn.sector = fnd_.getSector();
				tdn.industry = fnd_.getIndustry();
				Double mcap = fnd_.getMarketCap();
				if (mcap != null) {
					tdn.mktCap = mcap;
				}
				String avgMode = StringUtils.isNotBlank(avgMode_) ? avgMode_ : IATStatistics.SMA60;
				Double avg = fnd_.getAvg(avgMode);
				if (avg != null && !Double.isNaN(avg)) {
					tdn.pxAvg = Precision.round(avg, 2);
				}
				// er.note = fnd.getNote();
				String hvMode = StringUtils.isNotBlank(hvMode_) ? hvMode_ : IATStatistics.HV60;
				Double hv = fnd_.getHV(hvMode);
				if (hv != null && !Double.isNaN(hv)) {
					tdn.pxHV = Precision.round(hv, 2);
				}
				String earningsDS8 = fnd_.getEarningsDS8();
				// Date earnD = fnd_.getEarningsDate();
				// if (earnDate != null && refDate_.before(earnDate)) {
				if (earningsDS8 != null && refDS8_.compareTo(earningsDS8) <= 0) {
					// String earnDateStr = ATFormats.DATE_yyyy_MM_dd.get().format(earnDate);
					String earningsDS10 = ATFormats.ds8Tods10(earningsDS8);
					tdn.dateEarn = earningsDS10;
				}
				// Date exDivDate = fnd_.getDividendExDate();
				String exDividendDS8 = fnd_.getDividendExDS8();
				// if (exDivDate != null && refDate_.before(exDivDate)) {
				if (exDividendDS8 != null && refDS8_.compareTo(exDividendDS8) <= 0) {
					// String exDivStr = ATFormats.DATE_yyyy_MM_dd.get().format(exDivDate);
					String exDividendDS10 = ATFormats.ds8Tods10(exDividendDS8);
					tdn.dateDiv = exDividendDS10;
					Double div = fnd_.getDividend();
					if (div != null) {
						tdn.divAmt = div;
					}
				}
			}
			if (positions_ != null) {
				tdn.positions = new ArrayList<>();
				for (ATPersistentPosition p : positions_) {
					TdnPosition pos = toTdnPosition(p, null, sec_, fnd_);
					tdn.positions.add(pos);
				}
			}
			return tdn;
		} catch (Exception ex) {
			log.error(String.format("Error processing [%s]", sec_.getOccId()), ex);
			return null;
		}
	}

	static public TdnSecurity toTdnSecurity(String occId_, IATSymbol sym_, IATMarketData mkt_, IATEquityFoundation fnd_,
			String avgMode_, String hvMode_, String refDS8_) {
		if (mkt_ == null) {
			log.error(String.format("Missing %s [%s]", IATMarketData.class.getSimpleName(), occId_));
			return null;
		}
		try {
			TdnSecurity tdn = new TdnSecurity();
			tdn.id = occId_;
			tdn.name = sym_.getName();
			tdn.description = sym_.getDescription();
			tdn.sector = sym_.getSector();
			tdn.industry = sym_.getIndustry();
			tdn.px = mkt_.getPxLast();
			tdn.pxOpen = mkt_.getPxOpen();
			tdn.pxClose = mkt_.getPxClose();
			tdn.pxAsk = mkt_.getPxAsk();
			tdn.pxBid = mkt_.getPxBid();
			tdn.pxLoD = mkt_.getPxLow();
			tdn.pxHiD = mkt_.getPxHigh();
			tdn.owned = 0.0;
			tdn.ts = mkt_.getTsLast();
			if (fnd_ != null) {
				Double mcap = fnd_.getMarketCap();
				if (mcap != null) {
					tdn.mktCap = mcap;
				}
				String avgMode = StringUtils.isNotBlank(avgMode_) ? avgMode_ : IATStatistics.SMA60;
				Double avg = fnd_.getAvg(avgMode);
				if (avg != null && !Double.isNaN(avg)) {
					tdn.pxAvg = Precision.round(avg, 2);
				}
				String hvMode = StringUtils.isNotBlank(hvMode_) ? hvMode_ : IATStatistics.HV60;
				Double hv = fnd_.getHV(hvMode);
				if (hv != null && !Double.isNaN(hv)) {
					tdn.pxHV = Precision.round(hv, 2);
				}
				String earningsDS8 = fnd_.getEarningsDS8();
				if (earningsDS8 != null && refDS8_.compareTo(earningsDS8) <= 0) {
					String earningsDS10 = ATFormats.ds8Tods10(earningsDS8);
					tdn.dateEarn = earningsDS10;
				}
				String exDividendDS8 = fnd_.getDividendExDS8();
				if (exDividendDS8 != null && refDS8_.compareTo(exDividendDS8) <= 0) {
					String exDividendDS10 = ATFormats.ds8Tods10(exDividendDS8);
					tdn.dateDiv = exDividendDS10;
					Double div = fnd_.getDividend();
					if (div != null) {
						tdn.divAmt = div;
					}
				}
			}
			return tdn;
		} catch (Exception ex) {
			log.error(String.format("Error processing [%s]", occId_), ex);
			return null;
		}
	}

	static public TdnPosition toTdnPosition(ATPersistentPosition pos_, ATPersistentSecurity sec_, ATPersistentSecurity ul_,
			IATEquityFoundation fnd_) {
		TdnPosition pos = new TdnPosition();
		pos.tid = pos_.getOccId();
		pos.label = pos_.getLabel();
		pos.acct = pos_.getBroker();
		pos.pxTrd = pos_.getTradePrice();
		pos.dateTrd = pos_.getTradeDateStr();
		pos.count = pos_.getCount();
		if (pos_.isOption()) {
			ATPersistentSecurity sec = sec_ != null ? sec_ : ATSourceServiceRegistry.getSecuritiesProvider().get(pos_.getOccId());
			if (sec != null) {
				pos.pxLast = sec.getPrice();
			}
			if (ul_ != null) {
				pos.pxUL = ul_.getPrice();
			}
		} else if (sec_ != null) {
			pos.pxLast = sec_.getPrice(); // TODO: Keep?s
		} else if (ul_ != null && pos_.getOccId().equals(ul_.getOccId())) {
			pos.pxLast = ul_.getPrice();
		}
		if (fnd_ != null) {
			pos.dateEarn = fnd_.getEarningsDS8();
		}
		return pos;
	}

	static public TdnOHLCV toTdnOHLCV(ATPriceHistory history_, boolean isCloseOnly_, boolean isVolume_) {
		if (history_ == null)
			return null;
		TdnOHLCV ohlcv = new TdnOHLCV();
		StringBuilder d = new StringBuilder(history_.getDateStr());
		d.insert(6, '-');
		d.insert(4, '-');
		ohlcv.d = d.toString();
		ohlcv.c = history_.getClose();
		if (!isCloseOnly_) {
			ohlcv.o = history_.getOpen();
			ohlcv.h = history_.getHigh();
			ohlcv.l = history_.getLow();
		}
		if (isVolume_) {
			ohlcv.v = history_.getVolume();
		}
		return ohlcv;
	}
}
