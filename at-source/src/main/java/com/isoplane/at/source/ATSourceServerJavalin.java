package com.isoplane.at.source;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.delete;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.deser.std.JdkDeserializers;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.isoplane.at.adapter.ATAdapterRegistry;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.config.ATPersistentConfiguration;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOpportunityClip;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeTraceGroup;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserNote;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.ATUserSetting;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.serializers.ATDateSerializer;
import com.isoplane.at.commons.model.serializers.ATDoubleSerializer;
import com.isoplane.at.commons.model.serializers.ATSecurityDeserializer;
import com.isoplane.at.commons.model.serializers.ATSecuritySerializer;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATMarketStatusService;
import com.isoplane.at.commons.service.IATPortfolioService;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATUSTreasuryRateProvider;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFileAppender;
import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.IATJsonSerializationExclude;
import com.isoplane.at.ext.jsensors.ATSystemMonitor;
import com.isoplane.at.fundamentals.ATFundamentalsManager;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.alertrule.ATKafkaAlertRuleProducer;
import com.isoplane.at.kafka.fundamentals.ATKafkaFundamentalsConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusConsumer;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationProducer;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;
import com.isoplane.at.marketdata.ATMarketDataManager;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.ATMongoStatusService;
import com.isoplane.at.mongodb.IATPersistentLookupStore;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.source.adapter.ATUSTreasuryAdapter;
import com.isoplane.at.source.controller.ATAlertProcessor;
import com.isoplane.at.source.controller.ATEquityFoundationProvider;
import com.isoplane.at.source.controller.ATKafkaUserSessionController;
import com.isoplane.at.source.controller.ATOpportunityProcessor1;
import com.isoplane.at.source.controller.ATRequestContext;
import com.isoplane.at.source.controller.ATSecuritiesProvider;
import com.isoplane.at.source.controller.ATSettingsProvider;
import com.isoplane.at.source.controller.ATSourceController;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;
import com.isoplane.at.source.controller.IATAlertProcessor;
import com.isoplane.at.source.controller.IATOpportunityProcessor;
import com.isoplane.at.source.controller.IATSecuritiesProvider;
import com.isoplane.at.source.controller.IATSettingsProvider;
import com.isoplane.at.source.model.ATConfiguration;
import com.isoplane.at.source.util.ATWebSocketServiceJavalin;
import com.isoplane.at.symbols.ATSymbolManager;
import com.isoplane.at.users.ATUserManager;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.Javalin;

public class ATSourceServerJavalin implements IATSystemStatusProvider {

	static {
		String logPath = System.getProperty("logPath");
		ATFileAppender.overrideFile(logPath);
	}
	static final Logger log = LoggerFactory.getLogger(ATSourceServerJavalin.class);

	static private String ID;
	static private ATSourceServerJavalin _instance;

	private Gson _gson;
	@SuppressWarnings("unused")
	private Gson _debugGson;
	private ATConfiguration _atConfig;
	private ATSourceController _ctrl;
	// private Configuration _config;
	private boolean _isRunning;
	private Map<String, Long> _callMonitorMap = new TreeMap<>();

	private Javalin _server;
	int _port = 8082;

	private ATKafkaSystemStatusProducer _statusProducer;

	public static void main(String[] args_) {

		JdkDeserializers tmp = new com.fasterxml.jackson.databind.deser.std.JdkDeserializers();
		tmp.getClass();

		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);

			// Configuration config = ATConfigUtil.init(args_[0], true);

			ATExecutors.init();
			// ATEhCache.init(config);
			ATAdapterRegistry.init();
			ATMongoTableRegistry.init();
			if (!config.getBoolean("sysmon", false)) {
				log.info(String.format("System monitor disabled"));
			} else {
				ATSystemMonitor.init();
			}
			ATWebSocketServiceJavalin.configure(config);
			ATKafkaRegistry.register("source",
					ATKafkaAlertRuleProducer.class,
					ATKafkaFundamentalsConsumer.class,
					ATKafkaMarketDataConsumer.class,
					ATKafkaMarketDataPackConsumer.class,
					ATKafkaMarketStatusConsumer.class,
					ATKafkaPushNotificationProducer.class,
					ATKafkaSseMessageProducer.class,
					ATKafkaSystemStatusProducer.class,
					ATKafkaUserSessionConsumer.class,
					ATKafkaUserSessionProducer.class);

			// Service Registry
			ATSourceServiceRegistry.init();
			IATUserProvider userSvc = new ATUserManager(null, true);
			ATServiceRegistry.setUserService(userSvc, true);
			IATMarketStatusService statusSvc = new ATMongoStatusService();
			ATServiceRegistry.setStatusService(statusSvc, false);
			// IATWebSocketService webSocket = new ATWebSocketService(cconfig);
			// ATServiceRegistry.setWebSocketService(webSocket, true);
			IATPortfolioService portfolio = new ATPortfolioManager(true);
			ATServiceRegistry.setPortfolioService(portfolio, false);
			IATMarketDataService marketData = new ATMarketDataManager(true);
			ATServiceRegistry.setMarketDataService(marketData, false);
			IATSymbolProvider symbolSvc = new ATSymbolManager(true);
			ATSourceServiceRegistry.setSymbolProvider(symbolSvc, false);
			IATFundamentalsProvider fndSvc = new ATFundamentalsManager();
			ATServiceRegistry.setFundamentalsProvider(fndSvc, false);
			IATOpportunityProcessor oppSvc = new ATOpportunityProcessor1();
			ATSourceServiceRegistry.setOpportunityProcessor(oppSvc, false);

			ATKafkaUserSessionController.init(portfolio);

			// String propertiesPath = args_[0];

			_instance = new ATSourceServerJavalin();
			_instance.init(config);
			// _instance.setupWebServer();
			_instance.serve();

			// String tempId = String.format("sm_%d", System.currentTimeMillis());
			// ATTestKafkaUserSessionProducer prd = new ATTestKafkaUserSessionProducer(tempId, cconfig);
			// ATKafkaUserSessionProducerTestDriver driver = new ATKafkaUserSessionProducerTestDriver(prd);
			// ATExecutors.scheduleAtFixedRate(String.format("%s_test", prd.TOPIC), driver, 1000, 1000);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATSourceServerJavalin.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private void init(Configuration config_) {
		try {
			// _config = config_;
			Configuration config = ATConfigUtil.config();
			final String ipStr = ATSysUtils.getIpAddress("n/a");
			ATSysLogger.setLoggerClass(String.format("SourceServer.[%s]", ipStr));

			ID = String.format("%s:%s:%s", ATSourceServerJavalin.class.getSimpleName(), ipStr, _port);

			ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);

			IATSettingsProvider sp = new ATSettingsProvider();
			ATSourceServiceRegistry.setSettingsProvider(sp, false);

			_port = config.getInt("web.port");

			// log.info(String.format("Reading configuration: %s", configPath_));
			// Configuration config = new Configurations().properties(new File(configPath_));
			// ATWebSocketService.configure(config_);

			_atConfig = new ATConfiguration(config);
			// ATFirebaseAdapter firebaseAdapter = new ATFirebaseAdapter(config, _atConfig);
			// ATSourceServiceRegistry.setNotificationProcessor(firebaseAdapter, true);
			// firebaseAdapter.initService();

			IATPersistentLookupStore dao = new ATMongoDao();
			String[] sourceConfig = _atConfig.getString("source.config").split("/");
			IATConfiguration atconfig = ATPersistentConfiguration.getConfiguration(dao, sourceConfig[0],
					sourceConfig[1]);

			IATUSTreasuryRateProvider ustrp = new ATUSTreasuryAdapter();
			ATSourceServiceRegistry.setUSTreasuryRateProvider(ustrp, true);

			ATEquityFoundationProvider eqfp = new ATEquityFoundationProvider(dao);
			ATSourceServiceRegistry.setEquityFoundationProvider(eqfp, true);

			/*
			 * NOTE: Probably not required for current config (2020-02-10)
			 * 
			 * ATSymbolsProvider symp = new ATSymbolsProvider(atconfig, dao);
			 * ATSourceServiceRegistry.setSymbolsProvider(symp, true);
			 * ATPositionProcessor posp = new ATPositionProcessor(config_, atconfig, dao);
			 * ATSourceServiceRegistry.setPositionProcessor(posp, true);
			 */

			// IATCorporateEventProvider ceProvider = new ATCorporateEventProvider(atconfig, dao);
			// ATSourceServiceRegistry.setCorporateEventProvider(ceProvider, true);

			IATSecuritiesProvider secProvider = new ATSecuritiesProvider(atconfig, dao);
			ATSourceServiceRegistry.setSecuritiesProvider(secProvider, true);
			// IATSpreadProcessor spreadProcessor = new ATSpreadProcessor(atconfig);
			// ATSourceServiceRegistry.setSpreadProcessor(spreadProcessor, true);

			_gson = new GsonBuilder().registerTypeAdapter(ATSecurity.class, new ATSecurityDeserializer())
					.addSerializationExclusionStrategy(new ExclusionStrategy() {
						@Override
						public boolean shouldSkipField(FieldAttributes f) {
							return f.getAnnotation(IATJsonSerializationExclude.class) != null;
						}

						@Override
						public boolean shouldSkipClass(Class<?> clazz) {
							return false;
						}
					})
					.registerTypeAdapter(ATSecurity.class, new ATSecuritySerializer())
					.registerTypeAdapter(Date.class, new ATDateSerializer())
					.registerTypeAdapter(Double.class, new ATDoubleSerializer())
					.create();

			IATAlertProcessor alertProcessor = new ATAlertProcessor();
			ATSourceServiceRegistry.setAlertProcessor(alertProcessor, false);

			ATSystemStatusManager.register(this);

			_ctrl = new ATSourceController(config_);
			_ctrl.init();

			_isRunning = true;

			ATKafkaRegistry.start();
			_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);
			String pre = this.getClass().getSimpleName();
			ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

			log.info(String.format("%s:%d", ipStr, _atConfig.getWebPort()));
			ATSysLogger.info(String.format("SourceServer [%s] started", ipStr));
		} catch (Exception ex) {
			log.error("Error initializing", ex);
		}
	}

	private void serve() {
		Configuration config = ATConfigUtil.config();

		// JavalinJson.setFromJsonMapper(_gson::fromJson);
		// JavalinJson.setToJsonMapper(_gson::toJson);

		_server = Javalin.create(config_ -> {
			// config_.j
			config_.enableCorsForAllOrigins();
			boolean isWebDebugLog = config.getBoolean("web.debug.logging", false);
			if (isWebDebugLog) {
				config_.enableDevLogging();
			}
		}).start(_port);

		// io.javalin.websocket.WsConfig wsc;
		_server.ws("/socket/v1", ws_ -> {
			ATWebSocketServiceJavalin.instance().init(ws_);
		});

		_server.before(ctx_ -> {
			String path = ctx_.path();//.contextPath();
			Long count = this._callMonitorMap.get(path);
			this._callMonitorMap.put(path, count != null ? count + 1 : 1);
		});

		_server.routes(() -> {
			path("/api/v2ep", () -> {
				// get("/callcmd", ctx_ -> {
				get("/call/{cmd}", ctx_ -> {
					// String command = ctx_.pathParam(":cmd").toLowerCase();
					String command = ctx_.pathParam("cmd").toLowerCase();
					Object response = null;
					switch (command) {
						case "dividend":
							String divSymStr = ctx_.queryParam("symbols");// request_.queryParams("symbols");
							String[] divSyms = divSymStr.replace(" ", "").split(",");
							response = _ctrl.getDividends(divSyms);
							break;
						// @NOTE: Gatherer
						// case "option_exp":
						// 	String oeSymStr = ctx_.queryParam("symbols");// request_.queryParams("symbols");
						// 	String[] oeSyms = oeSymStr.replace(" ", "").split(",");
						// 	response = _ctrl.getOptionExpirations(oeSyms);
						// 	break;
						default:
							String msg = String.format("Unsupported command [%s]", command);
							throw new ATException(msg);
					}
					String jsonResponse = _gson.toJson(response);
					ctx_.result(jsonResponse);
				});
			});

			path("/api/v2", () -> {
				// Configuration config = ATConfigUtil.config();
				get("/callback", ctx_ -> {
					try {
						if (log.isDebugEnabled()) {
							StringBuilder sb = new StringBuilder();
							String qs = ctx_.queryString();
							qs = qs != null ? URLDecoder.decode(qs, StandardCharsets.UTF_8.toString()) : null;
							sb.append(String.format("Callback query: %s", qs)).append(System.lineSeparator());

							String query = _gson.toJson(qs);
							String body = ctx_.body();
							body = StringUtils.isBlank(body) ? "" : body;
							sb.append(String.format("Callback body: %s", body)).append(System.lineSeparator());

							Map<String, String> headers = ctx_.headerMap();
							log.debug(String.format("Callback query  : %s", query));
							if (headers != null) {
								int i = 0;
								for (String header : headers.keySet()) {
									String h = String.format("Callback header[%d]: %s - %s", i++, header,
											ctx_.header(header));
									log.debug(h);
									sb.append(h).append(System.lineSeparator());
								}
							}

							SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmssSSS");
							String path = config.getString("callback.save.path");
							File f = new File(path, String.format("callback-%s.txt", df.format(new Date())));
							log.info(String.format("Saving callback content [%s]: %s", f.getAbsolutePath(), body));
							BufferedWriter writer = new BufferedWriter(new FileWriter(f.getAbsolutePath(), true));
							writer.write(sb.toString());
							writer.close();
						}
						ctx_.result("OK");
					} catch (Exception ex) {
						log.error("", ex);
						ctx_.result("Error");
					}
				});

				// post("/tasktask", ctx_ -> {
				post("/task/{task}", ctx_ -> {
					// ATWebResponseWrapper response = new ATWebResponseWrapper();
					// try {
					// String taskType = ctx_.pathParam(":task").toLowerCase();
					// String uid = ctx_.header("uid");
					// String body = ctx_.body();
					// Object result_digest = _ctrl.registerTask(uid, taskType, body);
					// response.data = result_digest;
					// } catch (Exception ex) {
					// log.error(String.format("task Error"), ex);
					// response.error = ex.toString();// .getMessage();
					// }
					// String jsonResponse = _gson.toJson(response);
					// ctx_.result(jsonResponse);
				});

				get("/security-pack/{type}", ctx_ -> {
					try {
						String type = ctx_.pathParam("type").toLowerCase();
						String uid = ctx_.header("uid");
						ATWebResponseWrapper response = new ATWebResponseWrapper();
						String symbol = ctx_.queryParam("symbol");
						ATSecurityPack result = _ctrl.getSecuritPack(uid, Arrays.asList(type), symbol);
						response.data = result;
						String jsonResponse = _gson.toJson(response);
						ctx_.result(jsonResponse);
					} finally {
						ATRequestContext.clear();
					}
				});

				// post("/commandcmd", ctx_ -> {
				post("/command/{cmd}", ctx_ -> {
					// String command = ctx_.pathParam(":cmd").toLowerCase();
					String command = ctx_.pathParam("cmd").toLowerCase();
					String uid = ctx_.header("uid");
					String body = ctx_.body();
					Map<String, List<String>> queryParams = ctx_.queryParamMap();

					ATWebResponseWrapper response = new ATWebResponseWrapper();

					switch (command) {
						case "archive": {
							@SuppressWarnings("unchecked")
							HashMap<String, Object> bodyMap = _gson.fromJson(body, HashMap.class);
							var id = bodyMap.get("id");
							var type = bodyMap.get("type");
							var result = _ctrl.archive(uid, type.toString(), id, null);
							response.data = result;
							break;
						}
						case "dynamic_symbols":
							@SuppressWarnings("unchecked")
							HashMap<String, String> bodyMap = _gson.fromJson(body, HashMap.class);
							String typeStr = bodyMap != null ? bodyMap.get("type") : null;
							String symbolStr = bodyMap != null ? bodyMap.get("symbols") : null;
							Set<String> dynamicSymbols = StringUtils.isBlank(symbolStr) ? null
									: new TreeSet<>(Arrays.asList(symbolStr.split(",")));
							_ctrl.updateDynamicSymbols(uid, typeStr, dynamicSymbols);
							response.data = true;
							break;
						case "research":
							@SuppressWarnings("unchecked")
							HashMap<String, Object> eqtTmpl = _gson.fromJson(body, HashMap.class);
							ATUserEquityModel eqtModel = new ATUserEquityModel(eqtTmpl);
							eqtModel.setUserId(uid);
							ATSecurityPack result = _ctrl.getResearch(eqtModel);
							response.data = result;
							break;
						case "equity_model":
							if (queryParams != null && !queryParams.isEmpty()) {
								List<String> delStrs = queryParams.get("delete");
								boolean isDelete = delStrs != null && "true".equals(delStrs.get(0));
								if (isDelete) {
									List<String> idStrs = queryParams.get(IATLookupMap.AT_ID);
									String atid = idStrs != null ? idStrs.get(0) : null;
									Boolean success = _ctrl.deleteUserEquityModel(uid, atid);
									response.data = success;
								}
							} else {
								@SuppressWarnings("unchecked")
								HashMap<String, Object> eqtmTmpl = _gson.fromJson(body, HashMap.class);
								ATUserEquityModel eqtmInModel = new ATUserEquityModel(eqtmTmpl);
								eqtmInModel.setUserId(uid);
								ATUserEquityModel outModel = _ctrl.saveUserEquityModel(eqtmInModel);
								if (outModel != null) {
									response.data = outModel;
								} else {
									response.error = String.format("Error saving");
								}
							}
							break;
						case "modeling_clip":
							@SuppressWarnings("unchecked")
							HashMap<String, Object>[] mclipTmpl = _gson.fromJson(body, HashMap[].class);
							Set<ATOpportunityClip> inClips = ATOpportunityClip.fromJsonMaps(uid, mclipTmpl);
							List<ATOpportunityClip> outClips = _ctrl.updateUserOpportunityClips(uid, inClips);
							response.data = outClips;
							break;
						case "opportunities":
							List<String> symbols = ctx_.queryParams("symbols");
							@SuppressWarnings("unchecked")
							HashMap<String, Object> oppCfgTmpl = _gson.fromJson(body, HashMap.class);
							ATUserOpportunityConfig oppConfig = new ATUserOpportunityConfig(oppCfgTmpl);
							ATSecurityPack oppPack = _ctrl.getOpportunities(symbols, oppConfig);
							response.data = oppPack;
							break;
						case "opportunity_config":
							if (queryParams != null && !queryParams.isEmpty()) {
								List<String> delStrs = queryParams.get("delete");
								boolean isDelete = delStrs != null && "true".equals(delStrs.get(0));
								if (isDelete) {
									List<String> idStrs = queryParams.get(IATLookupMap.AT_ID);
									String atid = idStrs != null ? idStrs.get(0) : null;
									Boolean success = _ctrl.deleteUserOpportunityConfig(uid, atid);
									response.data = success;
								}
							} else {
								@SuppressWarnings("unchecked")
								HashMap<String, Object> eqtmTmpl = _gson.fromJson(body, HashMap.class);
								ATUserOpportunityConfig optInConfig = new ATUserOpportunityConfig(eqtmTmpl);
								optInConfig.setUserId(uid);
								ATUserOpportunityConfig outConfig = _ctrl.saveUserOpportunityConfig(optInConfig);
								if (outConfig != null) {
									response.data = outConfig;
								} else {
									response.error = String.format("Error saving [%s]", command);
								}
							}
							break;
						case "performance":
							Map<String, Object> performanceMap = _ctrl.assessPerformance(uid);
							String performanceResult = _gson.toJson(performanceMap);
							response.data = performanceResult;
							break;
						// case "sync":
						// _ctrl.syncSheets(uid);
						// response.data = true;
						// break;
						case "tasks":
							log.error(String.format("tasks DEPRECATED"));
							response.error = "tasks Deprecated";
							// ATTask[] tasks = _gson.fromJson(body, ATTask[].class);
							// String taskCode = ctx_.queryParam("code");
							// boolean taskResult = _ctrl.registerTasks(uid, taskCode, tasks);
							// response.data = taskResult;
							break;
						case "test_notification":
							_ctrl.testNotification(uid);
							response.data = true;
							break;
						case "trade_trace": {
							var tradeTrace = ATTradeTraceGroup.fromJson(uid, _gson, body);
							var replaceId = ctx_.queryParam("replaceId");
							var success = _ctrl.saveTradeTraceGroup(tradeTrace, replaceId);
							response.data = success;
							break;
						}
						case "user_import":
							if (!isAdmin(uid)) {
								if (!isAdmin(uid)) {
									throw new ATException(String.format("UNAUTHORIZED [%s/%s]", uid, command));
								}
							}
							String importUserId = ctx_.queryParam("userId");
							String userData = body;
							JsonObject json = _gson.fromJson(userData, JsonObject.class);
							Map<String, Object> importResult = _ctrl.importUserData(json, importUserId);
							response.data = importResult;
							break;
						case "user_setting":
							String settingData = body;
							ATUserSetting setting = _gson.fromJson(settingData, ATUserSetting.class);
							setting.setUserId(uid);
							boolean settingResult = _ctrl.saveUserSetting(setting);
							response.data = settingResult;
							break;
						case "watchlist":
							if (queryParams != null && !queryParams.isEmpty()) {
								List<String> delStrs = queryParams.get("delete");
								boolean isDelete = delStrs != null && "true".equals(delStrs.get(0));
								if (isDelete) {
									List<String> idStrs = queryParams.get(IATLookupMap.AT_ID);
									String atid = idStrs != null ? idStrs.get(0) : null;
									Boolean success = _ctrl.deleteWatchlist(uid, atid);
									response.data = success;
								}
							} else {
								@SuppressWarnings("unchecked")
								HashMap<String, Object> wlTmpl = _gson.fromJson(body, HashMap.class);
								ATWatchlist wlInModel = new ATWatchlist(wlTmpl);
								wlInModel.setUserId(uid);
								ATWatchlist outModel = _ctrl.saveWatchlist(wlInModel);
								if (outModel != null) {
									response.data = outModel;
								} else {
									response.error = String.format("Error saving");
								}
							}
							break;
						default:
							String msg = String.format("Unsupported command [%s]", command);
							log.info(msg);
							response.data = false;
							response.error = msg;
							break;
					}
					String jsonResponse = _gson.toJson(response);
					ctx_.result(jsonResponse);
				});

				// get("/commandcmd", ctx_ -> {
				get("/command/{cmd}", ctx_ -> {
					try {
						// String command = ctx_.pathParam(":cmd").toLowerCase();
						String command = ctx_.pathParam("cmd").toLowerCase();
						String uid = ctx_.header("uid");
						ATWebResponseWrapper response = new ATWebResponseWrapper();
						switch (command) {
							case "brokers":
								List<ATBroker> brokers = ATSourceServiceRegistry.getUserService().getUserBrokers(uid);
								response.data = brokers;
								break;
							case "dashboard":
								ATSecurityPack dashPack = _ctrl.getDashboard(uid);
								response.data = dashPack;
								break;
							case "digest": {
								String symbol = ctx_.queryParam("symbol");
								ATSecurityPack digestPack = _ctrl.getDigest(uid, symbol);
								response.data = digestPack;
								break;
							}
							case "equity_model":
								List<ATUserEquityModel> outModel = _ctrl.getUserEquityModels(uid);
								response.data = outModel;
								break;
							case "expirations":
								Set<String> expirations = _ctrl.getExpirations();
								response.data = expirations;
								break;
							case "lookup":
								String symbolLu = ctx_.queryParam("symbol");
								List<IATSymbol> lookups = ATSourceServiceRegistry.getSymbolProvider()
										.lookupSymbol(symbolLu);
								List<ATEquityDescription> dscs = lookups.stream().limit(10)
										.map(l -> new ATEquityDescription(l.getId(), l.getName(), l.getDescription(),
												l.getSector(), l.getIndustry()))
										.collect(Collectors.toList());
								response.data = dscs;
								break;
							case "modeling_clip":
								String symbolMc = ctx_.queryParam("symbol");
								List<ATOpportunityClip> outClips = _ctrl.getUserOpportunityClips(uid, symbolMc);
								response.data = outClips;
								break;
							case "opportunity_config":
								List<ATUserOpportunityConfig> outConfigs = _ctrl.getUserOpportunityConfigs(uid);
								response.data = outConfigs;
								break;
							case "option_expiration":
								String symbolOe = ctx_.queryParam("symbol");
								ATOptionExpiration exps = _ctrl.getOptionExpirations(symbolOe);
								response.data = exps;
								break;
							case "portfolio": {
								String forceStr = ctx_.queryParam("force");
								boolean force = StringUtils.isBlank(forceStr) ? false : Boolean.parseBoolean(forceStr);
								String symbolPf = ctx_.queryParam("symbol");
								ATSecurityPack portfolioPack = _ctrl.getPortfolio(uid, symbolPf, force);
								response.data = portfolioPack;
								break;
							}
							case "sec_category":
								Map<String, Set<String>> cats = _ctrl.getSecurityCategories();
								response.data = cats;
								break;
							case "security": {
								String symbolSec = ctx_.queryParam("symbol");
								List<String> flagsSec = ctx_.queryParams("flags");
								// Boolean isDynamic = Boolean.valueOf(ctx_.queryParam("dynamic", "false"));
								String[] flagSecArray = flagsSec != null ? flagsSec.toArray(new String[0]) : null;
								String periodStr = ctx_.queryParam("period");
								Period period = StringUtils.isBlank(periodStr) ? null : Period.parse(periodStr);
								ATSecurityPack pack = _ctrl.getSecurity(uid, symbolSec, period, flagSecArray);
								response.data = pack;
								response.error = pack == null ? String.format("%s not found", symbolSec) : null;
								break;
							}
							case "security-pack": {
								List<String> cmds = ctx_.queryParams("cmd");
								// Boolean isDynamic = Boolean.valueOf(ctx_.queryParam("dynamic", "false"));
								// String[] flagSecArray = flagsSec != null ? flagsSec.toArray(new String[0]) : null;
								String symbol = ctx_.queryParam("symbol");
								ATSecurityPack result = _ctrl.getSecuritPack(uid, cmds, symbol);
								response.data = result;
								break;
							}
							case "tasks":
								// List<String> tasks = _ctrl.getTasks(uid);
								// response.data = tasks;
								break;
							case "trade-ledger": {
								String symbol = ctx_.queryParam("symbol");
								ATSecurityPack result = _ctrl.getTradeLedger(uid, symbol);
								response.data = result;
								break;
							}
							case "trades":
								String activeStr = ctx_.queryParam("active");
								String symbolTrd = ctx_.queryParam("symbol");
								boolean isActive = StringUtils.isBlank(activeStr) ? true : Boolean.valueOf(activeStr);
								ATSecurityPack result = _ctrl.getTrades(uid, isActive, symbolTrd);
								response.data = result;
								break;
							case "users":
								if (!isAdmin(uid)) {
									throw new ATException(String.format("UNAUTHORIZED [%s/%s]", uid, command));
								}
								List<ATUser> users = ATSourceServiceRegistry.getUserService().getUsers();
								response.data = users;
								break;
							case "user_export":
								if (!isAdmin(uid)) {
									throw new ATException(String.format("UNAUTHORIZED [%s/%s]", uid, command));
								}
								String exportUserId = ctx_.queryParam("userId");
								Map<String, Object> data = _ctrl.exportUserData(exportUserId);
								response.data = data;
								break;
							case "user_setting":
								String settingId = ctx_.queryParam("settingId");
								ATUserSetting setting = _ctrl.getUserSetting(uid, settingId);
								response.data = setting;
								break;
							case "watchlist":
								List<ATWatchlist> wlOutModel = _ctrl.getWatchlists(uid);
								response.data = wlOutModel;
								break;
							default:
								String msg = String.format("Unsupported command [%s]", command);
								log.error(msg);
								throw new ATException(msg);
						}
						String jsonResponse = _gson.toJson(response);
						ctx_.result(jsonResponse);
						// return jsonResponse;
					} finally {
						ATRequestContext.clear();
					}
				});
				delete("/command/{cmd}", ctx_ -> {
					try {
						String command = ctx_.pathParam("cmd").toLowerCase();
						String uid = ctx_.header("uid");
						ATWebResponseWrapper response = new ATWebResponseWrapper();
						switch (command) {
							case "trade_trace":
								String idStr = ctx_.queryParam("id");
								Long id = Long.parseLong(idStr);
								var result = _ctrl.deleteTradeTrace(uid, id);
								response.data = result;
								break;
							default:
								String msg = String.format("Unsupported delete command [%s]", command);
								log.error(msg);
								throw new ATException(msg);
						}
						String jsonResponse = _gson.toJson(response);
						ctx_.result(jsonResponse);
					} finally {
						ATRequestContext.clear();
					}
				});

				// get("/spreads", ctx_ -> {
				// String uid = ctx_.header("uid");
				// try {
				// TdnOptionRersearchConfig spreadConfig = new TdnOptionRersearchConfig();
				// populateObjectFromQueryParams(request, spreadConfig);
				// JsonObject result_digest = ATSourceServiceRegistry.getSpreadProcessor().getNewSpreads(uid, spreadConfig);
				// JsonObject resultWrapper = new JsonObject();
				// resultWrapper.add("data", result_digest);
				// String resultStr = _gson.toJson(resultWrapper);
				// return resultStr;
				// } finally {
				// // ATRequestContext.clear();
				// }
				// });
				//
				// final String TRADES = "/trades";
				// post("/tradesforce", ctx_ -> {
				post("/trades/{force}", ctx_ -> {
					// log.debug(String.format("Request [%s]", TRADES));
					// String forceStr = ctx_.pathParam(":force");//.toUpperCase();
					String forceStr = ctx_.pathParam("force");// .toUpperCase();
					boolean isForce = Boolean.parseBoolean(forceStr);
					String uid = ctx_.header("uid");
					String bodyStr = ctx_.body();
					try {
						ATTrade[] trades = _gson.fromJson(bodyStr, ATTrade[].class);
						for (ATTrade trade : trades) {
							// Make sure types are correct (often deserialized to Double)
							Date tradeDate = trade.getTsTrade();
							trade.setTsTrade(tradeDate);
							Integer tradeType = trade.getTradeType();
							trade.setTradeType(tradeType);
						}
						boolean result = _ctrl.saveTrades(uid, trades, isForce);
						ctx_.result(String.format("{\"success\":%b}", result));
						// return String.format("{\"success\":%b}", result_digest);
					} catch (Exception ex) {
						log.error(String.format("Error in json: %s", bodyStr), ex);
						String resultStr = String.format("{\"success\":%b, \"error\":\"%s\"}", false, ex.getMessage());
						ctx_.result(resultStr);
					} finally {
						// ATRequestContext.clear();
					}
				});

				get("/assets", ctx_ -> {
					String uid = ctx_.header("uid");
					try {
						List<ATUserAsset> result = _ctrl.getAssets(uid);
						ATWebResponseWrapper response = new ATWebResponseWrapper();
						response.data = result;// .getTrades();
						String jsonResponse = _gson.toJson(response);
						ctx_.result(jsonResponse);
					} catch (Exception ex) {
						log.error(String.format("Failed [%s] /assets", uid), ex);
						throw new ATException(ex);
					} finally {
						// ATRequestContext.clear();
					}
				});

				// get("/securityoccId", ctx_ -> {
				get("/security/{occId}", ctx_ -> {
					// String occId = ctx_.pathParam(":occId").toUpperCase();
					String occId = ctx_.pathParam("occId").toUpperCase();
					List<String> flags = ctx_.queryParams("flags");
					String[] flagArray = flags != null ? flags.toArray(new String[0]) : null;
					String periodStr = ctx_.queryParam("period");
					Period period = StringUtils.isBlank(periodStr) ? null : Period.parse(periodStr);
					String uid = ctx_.header("uid");
					ATSecurityPack pack = _ctrl.getSecurity(uid, occId, period, flagArray);
					ATWebResponseWrapper response = new ATWebResponseWrapper();
					response.data = pack;
					response.error = pack == null ? String.format("%s not found", occId) : null;
					String jsonResponse = _gson.toJson(response);
					ctx_.result(jsonResponse);
					// return jsonResponse;
				});

				post("/settings/*", ctx_ -> {
					String uid = ctx_.header("uid");
					String bodyStr = ctx_.body();
					String path = ctx_.path();
					String cmd = path.substring(path.lastIndexOf("settings/") + "settings/".length());
					// String cmd = ctx_.splat()[0];
					try {
						boolean result = false;
						JsonObject body = _gson.fromJson(bodyStr, JsonObject.class);
						switch (cmd) {
							case "notification_subscription":
								String token = body.get("token").getAsString();
								String mode = body.get("mode").getAsString();
								result = _ctrl.subscribeUserNotifications(uid, token, mode);
								break;
						}
						ctx_.result(String.format("{\"data\":%b}", result));
					} catch (Exception ex) {
						log.error(String.format("Error in json: %s", bodyStr), ex);
						ctx_.result(String.format("{\"error\":%s}", ex.getMessage()));
					} finally {
						ATRequestContext.clear();
					}
				});

				path("user_alerts", () -> {
					// get(":symbol", ctx_ -> {
					get("{symbol}", ctx_ -> {
						String uid = ctx_.header("uid");
						// String symbol = ctx_.pathParam(":symbol").toUpperCase();
						String symbol = ctx_.pathParam("symbol").toUpperCase();
						if ("*".equals(symbol))
							symbol = null;
						ATWebResponseWrapper response = new ATWebResponseWrapper();
						List<ATUserAlertRulePack> packs = ATSourceServiceRegistry.getUserService().getUserAlerts(uid,
								symbol);
						if (packs != null && !packs.isEmpty()) {
							response.data = packs;
						}
						String result = _gson.toJson(response);
						ctx_.result(result);
					});
					post(ctx_ -> {
						String uid = ctx_.header("uid");
						String bodyStr = ctx_.body();
						try {
							ATUserAlertRulePack alertPack = _gson.fromJson(bodyStr, ATUserAlertRulePack.class);
							if (StringUtils.isBlank(alertPack.occId))
								ctx_.result(String.format("{\"success\":%b}", false));
							if (!uid.equals(alertPack.usrId)) {
								throw new ATException(String.format("User Id mismatch [%s/%s]", uid, alertPack.usrId));
							}
							if (alertPack.alerts != null) {
								for (ATAlertRule alert : alertPack.alerts) {
									alert.setOccId(alertPack.occId);
								}
							}
							ATUserAlertRulePack resultPack = _ctrl.saveUserAlerts(uid, alertPack);
							String result = _gson.toJson(resultPack);
							ctx_.result(String.format("{\"data\":%s}", result));
						} catch (Exception ex) {
							log.error(String.format("Error saving alerts: %s", bodyStr), ex);
							ctx_.result(String.format("{\"error\":%s}", ex.getMessage()));
						} finally {
							ATRequestContext.clear();
						}
					});
				});

				// get("/user_notessymbol", ctx_ -> {
				get("/user_notes/{symbol}", ctx_ -> {
					String uid = ctx_.header("uid");
					// String symbol = ctx_.pathParam(":symbol").toUpperCase(); // request_.params(":symbol");
					String symbol = ctx_.pathParam("symbol").toUpperCase(); // request_.params(":symbol");
					if ("*".equals(symbol))
						symbol = null;
					List<ATUserNote> notes = ATSourceServiceRegistry.getUserService().getUserNotes(uid, symbol);
					ATUserNote note = notes != null && !notes.isEmpty() ? notes.get(0) : new ATUserNote();
					String result = _gson.toJson(note);
					ctx_.result(String.format("{\"data\":%s}", result));
				});

				post("/user_notes", ctx_ -> {
					String uid = ctx_.header("uid");
					String bodyStr = ctx_.body();
					try {
						ATUserNote userNote = _gson.fromJson(bodyStr, ATUserNote.class);
						boolean saveResult = ATSourceServiceRegistry.getUserService().saveUserNote(uid, userNote);
						String result = saveResult ? bodyStr : String.format("{\"error\":%s}", "Saving failed");
						ctx_.result(String.format("{\"data\":%s}", result));
					} catch (Exception ex) {
						log.error(String.format("Error saving notes: %s", bodyStr), ex);
						ctx_.result(String.format("{\"error\":%s}", ex.getMessage()));
					} finally {
						ATRequestContext.clear();
					}
				});

				get("/ext_data", ctx_ -> {
					Map<String, Map<String, Object>> resultMap = new HashMap<>();
					var symbolStr = ctx_.queryParam("symbols").toUpperCase();
					if (!StringUtils.isBlank(symbolStr)) {
						IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
						//	String[] occIds = symbolStr.replace(" ", "").split(",");
						Set<String> occIds = Arrays.stream(symbolStr.split(",")).map(String::trim)
								.collect(Collectors.toSet());
						for (String occId : occIds) {
							ATMarketData mkt = mktSvc.getMarketData(occId);
							resultMap.put(occId, mkt);
						}
					}
					String result = _gson.toJson(resultMap);
					ctx_.result(result);
				});
			});

		});

	}

	private boolean isAdmin(String userId_) {
		ATUser user = ATServiceRegistry.getUserService().getUser(userId_);
		return user == null ? false : user.getRoles().contains("admin");
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> statusMap = new TreeMap<>();
		for (String path : this._callMonitorMap.keySet()) {
			String key = String.format("path.%s", path);
			String value = String.format("%d", this._callMonitorMap.get(path));
			statusMap.put(key, value);
		}
		return statusMap;
	}

	public static class ATWebResponseWrapper {
		public String type;
		public Object data;
		public Object error;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (_isRunning) {
					long statusPeriod = ATConfigUtil.config().getLong("status.period", 5000);
					Map<String, String> status = ATSystemStatusManager.getStatus();
					_statusProducer.send(ID, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (_isRunning) {
					run();
				}
			}
		}
	}

}
