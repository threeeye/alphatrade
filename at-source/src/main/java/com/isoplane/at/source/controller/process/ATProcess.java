package com.isoplane.at.source.controller.process;

public abstract class ATProcess implements Comparable<ATProcess> {

	private int cycleCount;

	abstract protected int getPriority();

	abstract public String toString();

	abstract protected int compareToInstance(ATProcess process);

	@Override
	public int compareTo(ATProcess other_) {
		if (other_ == null)
			return -1;
		int result = Integer.compare(this.getPriority(), other_.getPriority());
		if (result == 0) {
			return this.compareToInstance(other_);
		}
		return result;
	}

	public int getCycleCount() {
		return cycleCount;
	}

	public void setCycleCount(int cycleCount) {
		this.cycleCount = cycleCount;
	}

}
