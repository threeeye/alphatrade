package com.isoplane.at.source.deprecated;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATOpportunity;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.source.model.ATRoll;

public interface IATFinancialProcessor {

	List<ATOpportunity> analyze(ATSecurity security);

	Map<ATOptionSecurity, Map<String, Set<ATRoll>>> getRolls(ATOptionSecurity option, ATStock underlying);

	Collection<ATBaseNotification> getNoteworthyEvents(Collection<ATSecurity> securities);
	
	void priceOptions(ATStock stock);

	Collection<ATBaseNotification> getRollEvents(String symbol);

	Collection<ATBaseNotification> analyzeOptonChain(ATPersistentSecurity... securities);

}
