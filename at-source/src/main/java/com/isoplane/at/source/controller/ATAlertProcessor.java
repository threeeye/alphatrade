package com.isoplane.at.source.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATAlertRule;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.alertcriteria.ATBaseCriteria;
import com.isoplane.at.commons.model.alertcriteria.ATCriteriaUtil;
import com.isoplane.at.commons.model.alertcriteria.ATDateCriteria;
import com.isoplane.at.commons.model.alertcriteria.ATPriceCriteria;
import com.isoplane.at.commons.service.IATUserAlertListener;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.source.model.ATAlertNotification;
import com.isoplane.at.source.model.ATDividendNotification;
import com.isoplane.at.source.model.ATEarningsNotification;

public class ATAlertProcessor implements IATAlertProcessor, IATUserAlertListener {

	static final Logger log = LoggerFactory.getLogger(ATAlertProcessor.class);

	// private Configuration _config;
	private Map<String, Set<ATBaseCriteria>> _criteriaMap = new HashMap<>();
	private Map<String, Long> _alertThrottle;
	// private Map<String, Long> _earningsThrottle;
	// private Map<String, Long> _dividendThrottle;

	public ATAlertProcessor() {
		// _config = config_;
		// alertSource_.register(this);
		_alertThrottle = new HashMap<>();
		// _earningsThrottle = new HashMap<>();
		// _dividendThrottle = new HashMap<>();
	}

	// @Deprecated
	// @Override
	// public void update(String userId_, Collection<TdnAlert> alerts_) {
	// // Remove old criteria
	// for (String occId : _criteriaMap.keySet()) {
	// Set<ATBaseCriteria> updateCriteria = _criteriaMap.get(occId);
	// updateCriteria.removeIf(c -> c.getUserId().equals(userId_));
	// }
	// // TODO: Sort for and remove equivalent criteria
	// Set<ATBaseCriteria> newCriteria = ATCriteriaUtil.createCriteria(userId_, alerts_.toArray(new TdnAlert[0]));
	// for (ATBaseCriteria criteria : newCriteria) {
	// Set<ATBaseCriteria> updateCriteria = _criteriaMap.get(criteria.getOccId());
	// if (updateCriteria == null) {
	// updateCriteria = new HashSet<>();
	// _criteriaMap.put(criteria.getOccId(), updateCriteria);
	// }
	// updateCriteria.add(criteria);
	// }
	// log.info(String.format("Added new alerts [%s/%d]", userId_, newCriteria.size()));
	// }

	private void updateUserAlerts(String userId_, String occId_, ATAlertRule... alerts_) {
		// Remove old criteria
		Set<ATBaseCriteria> updateCriteria = _criteriaMap.get(occId_);
		if (updateCriteria != null) {
			updateCriteria.removeIf(c -> c.getUserId().equals(userId_));
		}
		if (alerts_ == null || alerts_.length == 0)
			return;
		if (updateCriteria == null) {
			updateCriteria = new HashSet<>();
			_criteriaMap.put(occId_, updateCriteria);
		}
		// TODO: Sort for and remove equivalent criteria
		Set<ATBaseCriteria> newCriteria = ATCriteriaUtil.createCriteria(userId_, alerts_);
		for (ATBaseCriteria criteria : newCriteria) {
			updateCriteria.add(criteria);
		}
		log.info(String.format("Updated [%d / %s] alerts: %s", newCriteria.size(), occId_, userId_));
	}

	@Override
	public void initService() {
		IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		userSvc.register(this);
		List<ATUserAlertRulePack> alertPacks = userSvc.getUserAlerts(null, null);
		if (alertPacks == null || alertPacks.isEmpty())
			return;
		Map<String, List<ATUserAlertRulePack>> symbolAlertMap = alertPacks.stream().collect(Collectors.groupingBy(p -> p.occId));
		for (String symbol : symbolAlertMap.keySet()) {
			List<ATUserAlertRulePack> userAlertPacks = symbolAlertMap.get(symbol);
			// Map<String, List<ATAlertRule>> symbolMap = userAlerts.stream().collect(Collectors.groupingBy(ATAlertRule::getOccId));
			for (ATUserAlertRulePack pack : userAlertPacks) {
				ATAlertRule[] symbolAlerts = pack.alerts;
				if (symbolAlerts != null && symbolAlerts.length > 0) {
					updateUserAlerts(pack.usrId, pack.occId, symbolAlerts);
				}
			}
		}
	}

	@Override
	public Set<ATBaseNotification> process(ATMarketData data_, Set<String> ownerIds_) {
		String occId = data_.getAtId();
		if (occId == null)
			return null;
		Configuration config = ATConfigUtil.config();
		boolean isOption = occId.length() > 9;

		Set<ATBaseNotification> result = new HashSet<>();

		// User defined alerts
		if (false) {
			Set<ATBaseCriteria> criteria = _criteriaMap.get(occId);
			if (criteria != null) {
				long alertThrottle = config.getLong("alerts.alertThrottle");
				Long alertTS = this._alertThrottle.get(occId);
				if (alertTS == null || System.currentTimeMillis() - alertTS > alertThrottle) {
					String ul = isOption ? ATModelUtil.getUnderlyingOccId(occId) : occId;
					this._alertThrottle.put(ul, System.currentTimeMillis());
					// if ("WBA".equals(data_.getOccId())) {
					// int ii = 0;
					// }
					if (isOption) {
						log.info(String.format("isOption [%b]", isOption));
					}
					if ("PFE".equals(ul)) {
						log.info(String.format("%s", ul));
					}
					IATEquityFoundationProvider eqtSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
					ATPersistentEquityFoundation eqf = eqtSvc.getFoundation(ul);
					for (ATBaseCriteria crit : criteria) {
						// crit.getUserId()
						if (crit.test(data_, eqf)) {
							ATBaseNotification alert = toNotification(crit, data_, eqf);
							// String tag = alert.getNotificationTag();
							// String key = alert.getNotificationKey();
							if (alert != null) {
								result.add(alert);
							}
						}
					}
				}
			}
		}

		// ATSourceServiceRegistry.getNotificationProcessor().push(result_digest);
		return result;
	}

	@Override
	public Set<ATBaseNotification> processEconomicEvents(String occId_, Set<String> ownerIds_) {
		if (StringUtils.isBlank(occId_) || occId_.length() > 10 || ownerIds_ == null || ownerIds_.isEmpty())
			return null;
		ATPersistentEquityFoundation fnd = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(occId_);
		if (fnd == null) {
			log.debug(String.format("processEconomicEvents missing Foundation [%s]", occId_));
			return null;
		}

		Set<ATBaseNotification> result = new HashSet<>();

		// Dividends
		Date exDivDate = fnd.getDividendExDate();
		if (exDivDate != null) {
			long diff = exDivDate.getTime() - System.currentTimeMillis();
			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			if (days >= 0 && days <= 7) {
				ATDividendNotification divn = new ATDividendNotification();
				divn.setDate(new Date());
				divn.setExDate(exDivDate);
				divn.setLabel(fnd.getLabel());
				divn.setSymbol(occId_);
				divn.addUserIds(ownerIds_);
				result.add(divn);
			}
		}

		// Earnings
		Date earningsDate = fnd.getEarningsDate();
		if (earningsDate != null) {
			long diff = earningsDate.getTime() - System.currentTimeMillis();
			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			if (days >= 0 && days <= 7) {
				ATEarningsNotification earnn = new ATEarningsNotification();
				earnn.setDate(new Date());
				earnn.setEarningsDate(earningsDate);
				earnn.setLabel(fnd.getLabel());
				earnn.setSymbol(occId_);
				earnn.addUserIds(ownerIds_);
				result.add(earnn);
			}
		}

		return result;
	}
	

	private ATBaseNotification toNotification(ATBaseCriteria criteria_, IATMarketData mkt_, IATStatistics stats_) {
		Double price = mkt_.getPxLast();
		String symbol = mkt_.getAtId();
		if (symbol.length() > 6) {
			symbol = symbol.substring(0, 6).trim();
		}
		String clickAction = String.format("https://tradonado.com/security/%s", criteria_.getOccId());
		ATAlertNotification alert = new ATAlertNotification();
		alert.setActive(true); // User defined alerts should be active
		alert.setClickAction(clickAction);
		alert.setCriteria(criteria_.getCriteria());
		alert.setDate(new Date());
		alert.setLabel(criteria_.label(mkt_, stats_));
		alert.setPrice(price);
		alert.setSymbol(symbol);
		alert.addUserId(criteria_.getUserId());
		if (criteria_ instanceof ATPriceCriteria) {
			alert.setAlertType("px");
		} else if (criteria_ instanceof ATDateCriteria) {
			alert.setAlertType("date");
		}
		return alert;
	}

	@Override
	public void notify(ATUserAlertRulePack pack_) {
		if (pack_ == null)
			return;
		updateUserAlerts(pack_.usrId, pack_.occId, pack_.alerts);
	}
	
	@Override
	@Deprecated
	public Set<ATBaseNotification> process(ATPersistentSecurity... secs_) {
		
		return null;

		// if (secs_ == null)
		// 	return null;

		// IATEquityFoundationProvider foundations = ATSourceServiceRegistry.getEquityFoundationProvider();
		// IATPositionProcessor positions = ATSourceServiceRegistry.getPositionProcessor();

		// Set<ATBaseNotification> result = new HashSet<>();
		// for (ATPersistentSecurity sec : secs_) {
		// 	Set<String> userIds = positions.getUserIds(sec.getOccId());

		// 	// User defined alerts
		// 	Set<ATBaseCriteria> criteria = _criteriaMap.get(sec.getOccId());
		// 	if (criteria != null) {
		// 		for (ATBaseCriteria crit : criteria) {
		// 			if (crit.test(sec)) {
		// 				ATBaseNotification alert = toNotification(crit, sec);
		// 				if (alert != null) {
		// 					result.add(alert);
		// 				}
		// 			}
		// 		}
		// 	}

		// 	if (!sec.isOption()) {
		// 		ATPersistentEquityFoundation fnd = foundations.getFoundation(sec.getOccId());

		// 		// Dividends
		// 		Date exDivDate = fnd != null ? fnd.getDividendExDate() : null;
		// 		if (exDivDate != null) {
		// 			long diff = exDivDate.getTime() - System.currentTimeMillis();
		// 			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		// 			if (days >= 0 && days <= 7) {
		// 				ATDividendNotification divn = new ATDividendNotification();
		// 				divn.setDate(new Date());
		// 				divn.setExDate(exDivDate);
		// 				divn.setLabel(sec.getLabel());
		// 				divn.setSymbol(sec.getOccId());
		// 				divn.addUserIds(userIds);
		// 				result.add(divn);
		// 			}
		// 		}

		// 		// Earnings
		// 		Date earningsDate = fnd != null ? fnd.getEarningsDate() : null;
		// 		if (earningsDate != null) {
		// 			long diff = earningsDate.getTime() - System.currentTimeMillis();
		// 			long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
		// 			if (days >= 0 && days <= 7) {
		// 				ATEarningsNotification earnn = new ATEarningsNotification();
		// 				earnn.setDate(new Date());
		// 				earnn.setEarningsDate(earningsDate);
		// 				earnn.setLabel(sec.getLabel());
		// 				earnn.setSymbol(sec.getOccId());
		// 				earnn.addUserIds(userIds);
		// 				result.add(earnn);
		// 			}
		// 		}

		// 	}

		// }
		// return result;
	}

	@Deprecated
	private ATBaseNotification toNotification(ATBaseCriteria criteria_, ATPersistentSecurity sec_) {
		String clickAction = String.format("https://tradonado.com/security/%s", criteria_.getOccId());
		ATAlertNotification alert = new ATAlertNotification();
		alert.setActive(true); // User defined alerts should be active
		alert.setClickAction(clickAction);
		alert.setCriteria(criteria_.getCriteria());
		alert.setDate(new Date());
		alert.setLabel(criteria_.label(sec_));
		alert.setPrice(sec_.getPrice());
		alert.setSymbol(sec_.getSymbol());
		alert.addUserId(criteria_.getUserId());
		if (criteria_ instanceof ATPriceCriteria) {
			alert.setAlertType("px");
		} else if (criteria_ instanceof ATDateCriteria) {
			alert.setAlertType("date");
		}
		return alert;
	}


	// @Override
	// public void notify(IATMarketData data_) {
	// process(data_);
	// }

}
