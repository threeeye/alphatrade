package com.isoplane.at.source.controller;

import java.util.Collection;
import java.util.Set;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.service.IATService;

public interface IATNotificationProcessor extends IATService {

	Set<String> push(Collection<ATBaseNotification> notifications);

	void reset();

	boolean subscribeNotifications(String uid, String token, String mode);
}
