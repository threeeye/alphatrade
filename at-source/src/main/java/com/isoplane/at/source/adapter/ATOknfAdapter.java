package com.isoplane.at.source.adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.source.model.ATConfiguration;

public class ATOknfAdapter implements IATSP500Adapter {

	static final Logger log = LoggerFactory.getLogger(ATOknfAdapter.class);

	private ATConfiguration _config;
	private long _lastProcessTime = 0;
	private long _processPeriod = 1000;
	private Gson _gson = new Gson();

	public ATOknfAdapter(ATConfiguration config_) {
		_config = config_;
	}

	@Override
	public Set<ATStock> getSP500() {
		try {
			// New ?
			// https://pkgstore.datahub.io/core/s-and-p-500-companies/constituents_json/data/b0c0dbabbc66fa902dd40a9e5596263e/constituents_json.json
			// String url = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents-financials.json";
			String path = String.format("%s/%s.sp500", _config.getString("data.path"), ATOknfAdapter.class.getSimpleName());
			String url = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents.json";
			String json = downloadHttp(url);
			if (StringUtils.isBlank(json)) {
				json = new Scanner(new File(path)).useDelimiter("\\A").next();
			} else {
				try (PrintWriter out = new PrintWriter(path)) {
					out.println(json);
				}
			}
			Set<ATStock> result = parseConstituents(json);
			log.info(String.format("SP 500 [%,d]", result.size()));
			return result;
		} catch (Exception ex) {
			log.error("Error getting SP500", ex);
			return null;
		}
	}

	@Deprecated
	@Override
	public boolean processSP500(List<String> constituents_) {
		if (constituents_ == null)
			return false;
		try {
			// String url = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents-financials.json";
			String url = "http://data.okfn.org/data/core/s-and-p-500-companies/r/constituents.json";
			String json = downloadHttp(url);
			Set<ATStock> result = parseConstituents(json);
			if (result == null || result.isEmpty()) {
				return false;
			} else {
				constituents_.clear();
				constituents_.addAll(result.stream().map(c -> c.getSymbol()).collect(Collectors.toList()));
				return true;
			}
		} catch (Exception ex) {
			log.error("Error getting SP500", ex);
			return false;
		}
	}

	private Set<ATStock> parseConstituents(String json_) {
		Set<ATStock> result = new TreeSet<>();
		try {
			if (StringUtils.isBlank(json_))
				return result;
			OkfnConstituent[] cnsts = _gson.fromJson(json_, OkfnConstituent[].class);
			for (OkfnConstituent okfnConstituent : cnsts) {
				ATStock stock = new ATStock();
				stock.setOccId(okfnConstituent.Symbol);
				stock.setSymbol(okfnConstituent.Symbol);
				stock.setLabel(okfnConstituent.Symbol);
				stock.setDescription(okfnConstituent.Name);
				stock.setSector(okfnConstituent.Sector);
				result.add(stock);
			}
		} catch (Exception ex) {
			log.error(String.format("Error parsing: %s", json_), ex);
		}
		return result;
	}

	// TODO: Move this to HttpUtil
	private String downloadHttp(String url_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			long gap = System.currentTimeMillis() - _lastProcessTime;
			if (gap < _processPeriod) {
				Thread.sleep(_processPeriod - gap);
			}

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url_);
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			response = httpclient.execute(httpGet);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(),
			// url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			_lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (Exception ex) {
			log.error(String.format("Error on [%s]", url_), ex);
			return null;
		} finally {
			if (response != null)
				response.close();
		}
	}

	static private class OkfnConstituent {
		public String Name;
		public String Symbol;
		public String Sector;
		@SuppressWarnings("unused")
		public Double Price;
		@SerializedName("Dividend Yield")
		public Double DividendYield;
		@SerializedName("Price/Earnings")
		public Double PriceEarnings;
		@SerializedName("Earnings/Share")
		public Double EarningsShare;
		@SerializedName("Book Value")
		public Double BookValue;
		@SerializedName("52 week low")
		public Double Price52Low;
		@SerializedName("52 week high")
		public Double Price52High;
		@SerializedName("Market Cap")
		public Double MarketCap;
		@SuppressWarnings("unused")
		public Double EBITDA;
		@SerializedName("Price/Sales")
		public Double PriceSales;
		@SerializedName("Price/Book")
		public Double PriceBook;
		@SerializedName("SEC Filings")
		public String SECFilings;
	}

}
