package com.isoplane.at.source.model;

import java.util.HashMap;

import com.isoplane.at.source.controller.process.ATOptionsProcess;

public class ATSystemStatus extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public static final String PricingProcessedCount = "PricingProcessedCount";
	public static final String PricingThroughput = "PricingThroughput";
	public static final String StartupTime = "StartupTime";
	public static final String OptionChainThroughput = "OptionChainThroughput";

	private static final ATSystemStatus _instance = new ATSystemStatus();
	
	public static final ATSystemStatus Current() {
		_instance.put(OptionChainThroughput, ATOptionsProcess.counter().getThroughput());
		 return _instance;
	}
	
	static {
		_instance.put(PricingProcessedCount, 0L);
		_instance.put(PricingThroughput, 0.0);
		_instance.put(StartupTime, System.currentTimeMillis());
	}
}
