package com.isoplane.at.source.controller.process;

import com.isoplane.at.source.util.ATCounter;

public class ATOptionsProcess extends ATProcess {

	static private ATCounter _counter = new ATCounter();

	static public ATCounter counter() {
		return _counter;
	}

	private String _occId;

	public ATOptionsProcess(String occId_) {
		_occId = occId_;
	}

	public String getSecurityId() {
		return _occId;
	}

	@Override
	protected int getPriority() {
		return 1000;
	}

	@Override
	public boolean equals(Object obj_) {
		return (obj_ != null && obj_ instanceof String && getSecurityId().equals((String) obj_));
	}

	@Override
	public int hashCode() {
		return getSecurityId().hashCode();
	}

	@Override
	public String toString() {
		String str = String.format("%s[%-4s]", this.getClass().getSimpleName(), getSecurityId());
		return str;
	}

	@Override
	protected int compareToInstance(ATProcess other_) {
		if (other_ instanceof ATOptionsProcess) {
			return this.getSecurityId().compareTo(((ATOptionsProcess) other_).getSecurityId());
		} else {
			return 0;
		}
	}

}
