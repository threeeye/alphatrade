package com.isoplane.at.source.model;

import java.util.Collection;

public interface IATUserConfiguration {

	double getBaseSafetyMargin();

	double getMinApy();
	
	double getMinPx();

	int getMinOptionDays();

	int getMaxOptionDays();

	String[] getExtraSymbols();

	String[] getIdeaTriggers();

	String[] getAlertTriggers();

	String[] getBlacklistSymbols();
	
	String[] getSpreadSymbols();

	Collection<String> getOptionFlagList();

	boolean isNotifications();

}