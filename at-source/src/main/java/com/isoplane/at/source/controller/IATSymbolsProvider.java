package com.isoplane.at.source.controller;

import java.util.Set;

import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentSymbols;

public interface IATSymbolsProvider extends IATService {

	ATPersistentSymbols getSymbols();
	
	Set<String> getBlacklist();
}
