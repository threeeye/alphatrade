package com.isoplane.at.source.model;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.util.ATFormats;

public class ATPriceHistoryOld implements Comparable<ATPriceHistoryOld> {

	private String occId;
	private Date date;
	private Double open;
	private Double close;
	private Double low;
	private Double high;
	private Double low1Q;
	private Double high1Q;
	private Double open1Q;
	private Double close1Q;
	private Double avg1Q;
	private Double low1Y;
	private Double high1Y;
	private Double open1Y;

	public Double getLow1Q() {
		return low1Q;
	}

	public void setLow1Q(Double low1q) {
		low1Q = low1q;
	}

	public Double getHigh1Q() {
		return high1Q;
	}

	public void setHigh1Q(Double high1q) {
		high1Q = high1q;
	}

	public Double getOpen1Q() {
		return open1Q;
	}

	public void setOpen1Q(Double open1q) {
		open1Q = open1q;
	}

	public Double getClose1Q() {
		return close1Q;
	}

	public void setClose1Q(Double close1q) {
		close1Q = close1q;
	}

	public Double getLow1Y() {
		return low1Y;
	}

	public void setLow1Y(Double low1y) {
		low1Y = low1y;
	}

	public Double getHigh1Y() {
		return high1Y;
	}

	public void setHigh1Y(Double high1y) {
		high1Y = high1y;
	}

	public Double getOpen1Y() {
		return open1Y;
	}

	public void setOpen1Y(Double open1y) {
		open1Y = open1y;
	}

	public Double getClose1Y() {
		return close1Y;
	}

	public void setClose1Y(Double close1y) {
		close1Y = close1y;
	}

	private Double close1Y;
	private Double avg1Y;

	public Double getAvg1Q() {
		return avg1Q;
	}

	public void setAvg1Q(Double avg1q) {
		avg1Q = avg1q;
	}

	public Double getAvg1Y() {
		return avg1Y;
	}

	public void setAvg1Y(Double avg1y) {
		avg1Y = avg1y;
	}

	private Integer volume;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getOpen() {
		return open;
	}

	public void setOpen(Double open) {
		this.open = open;
	}

	public Double getClose() {
		return close;
	}

	public void setClose(Double close) {
		this.close = close;
	}

	public Double getLow() {
		return low;
	}

	public void setLow(Double low) {
		this.low = low;
	}

	public Double getHigh() {
		return high;
	}

	public void setHigh(Double high) {
		this.high = high;
	}

	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	public String getOccId() {
		return occId;
	}

	public void setOccId(String occId) {
		this.occId = occId;
	}

	@Override
	public int compareTo(ATPriceHistoryOld other_) {
		if (other_ == null)
			return 1;
		Date otherDate = other_.getDate();
		Date thisDate = getDate();
		int result;
		if (thisDate == null) {
			result = otherDate == null ? 0 : -1;
		} else {
			result = thisDate.compareTo(otherDate);
		}
		if (result != 0)
			return result;
		String thisSym = getOccId();
		String otherSym = other_.getOccId();
		if (StringUtils.isBlank(thisSym)) {
			result = StringUtils.isBlank(otherSym) ? 0 : -1;
		} else {
			result = thisSym.compareTo(otherSym);
		}
		return result;
	}

	@Override
	public String toString() {
		String str = String.format("%s %s %.2f", getOccId(), ATFormats.DATE_yyyy_MM_dd.get().format(getDate()), getClose());
		return str;
	}

}
