package com.isoplane.at.source.util;

import com.isoplane.at.commons.util.IATEvent;

public class ATAuthenticationEvent implements IATEvent {

	public ATAuthenticationEvent() {
	}

	public ATAuthenticationEvent(String uid_, boolean isActive_) {
		this.uid = uid_;
		this.isActive = isActive_;
	}

	private String uid;
	private boolean isActive;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isSignin) {
		this.isActive = isSignin;
	}
}
