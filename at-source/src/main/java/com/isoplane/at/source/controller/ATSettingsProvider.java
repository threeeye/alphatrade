package com.isoplane.at.source.controller;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.config.IATConfigurationListener;

public class ATSettingsProvider extends HashMap<String, Object> implements IATSettingsProvider {

	private static final long serialVersionUID = 1L;
	static final Logger log = LoggerFactory.getLogger(ATSettingsProvider.class);

	static int _unusedCount = 0;
	static final Set<String> _unusedSet = new HashSet<>();

	@Override
	public Object put(String key_, Object value_) {
		if (StringUtils.isBlank(key_)) {
			log.error("NULL key");
			return null;
		}
		_unusedSet.add(key_);
		Object oldValue = super.get(key_);
		if (oldValue == null) {
			if (value_ == null)
				return null;
		} else if (oldValue.equals(value_)) {
			return oldValue;
		}
		super.put(key_, value_);
		log.debug(String.format("Settings [%10s]: %s -> %s", key_, oldValue, value_));
		return oldValue;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map_) {
		if (map_ == null) {
			log.error("NULL map");
			return;
		}
		map_.entrySet().forEach(e_ -> {
			this.put(e_.getKey(), e_.getValue());
		});
	}

	@Override
	public Boolean getBoolean(String key_) {
		Boolean value = (Boolean) getValue(key_);
		return value;
	}

	@Override
	public boolean getBoolean(String key_, boolean default_) {
		Boolean value = (Boolean) getValue(key_, default_);
		return value;
	}

	@Override
	public Double getDouble(String key_) {
		Double value = (Double) getValue(key_);
		return value;
	}

	@Override
	public double getDouble(String key_, double default_) {
		Double value = (Double) getValue(key_, default_);
		return value;
	}

	@Override
	public Integer getInt(String key_) {
		Integer value = (Integer) getValue(key_);
		return value;
	}

	@Override
	public int getInt(String key_, int default_) {
		Integer value = (Integer) getValue(key_, default_);
		return value;
	}

	@Override
	public Long getLong(String key_) {
		Long value = (Long) getValue(key_);
		return value;
	}

	@Override
	public long getLong(String key_, long default_) {
		Long value = (Long) getValue(key_, default_);
		return value;
	}

	@Override
	public String getString(String key_) {
		String value = (String) getValue(key_);
		return value;
	}

	@Override
	public String getString(String key_, String default_) {
		String value = (String) getValue(key_, default_);
		return value;
	}

	private Object getValue(String key_) {
		if (StringUtils.isBlank(key_)) {
			log.error("NULL key");
			return null;
		}
		_unusedSet.remove(key_);
		if (_unusedCount++ > 100) {
			_unusedCount = 0;
			if (!_unusedSet.isEmpty()) {
				log.info(String.format("Unused settings %s", _unusedSet));
			}
		}
		Object value = super.get(key_);
		if (value == null) {
			log.debug(String.format("Not found [%s]", key_));
		}
		return value;
	}

	private Object getValue(String key_, Object fallback_) {
		Object value = this.get(key_);
		if (value == null) {
			log.debug(String.format("Using fall-back [%s/%s]", key_, fallback_));
			return fallback_;
		} else {
			return value;
		}
	}

	@Override
	public void initService() {

	}

	@Override
	public void registerListener(IATConfigurationListener listener) {

	}

}
