package com.isoplane.at.source.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.source.controller.ATRequestContext;

public class ATConfiguration implements IATUserConfiguration {

	static final Logger log = LoggerFactory.getLogger(ATConfiguration.class);

	public static final String USER_TYPE = "user";
	public static final String SYSTEM_TYPE = "system";

	private String type;
	private double riskFreeInterest;
	private double priceStatAmplitude;
	private double priceStatSlope;
	private long processPeriod;
	private long pricingPeriod;
	private int maxOptionDays; // Property only for json decoding. Stored in underlying map
	private int minOptionDays; // Property only for json decoding. Stored in underlying map
	private double baseSafetyMargin; // Property only for json decoding. Stored in underlying map
	private double boundaryMargin; // Property only for json decoding. Stored in underlying map
	private int boundaryRange; // Property only for json decoding. Stored in underlying map
	private double minApy; // Property only for json decoding. Stored in underlying map
	private double minPx; // Property only for json decoding. Stored in underlying map
	private double timeDecay; // Property only for json decoding; Stored in map
	private int saveBulkSize;
	private boolean isNotifications; // Property only for json decoding; Stored in map
	private String[] extraSymbols;
	private String[] ideaTriggers;
	private String[] blacklistSymbols;
	private String[] alertTriggers;
	private String[] optionFlags;
	private String[] spreadSymbols;
	private Configuration _config;
	private int _status;

	transient private List<String> _optionFlagList;

	public ATConfiguration(Configuration config_) {
		_config = config_;
	}

	public ATConfigurationChange update(ATConfiguration config_) {
		ATUser user = ATRequestContext.getUser();
		ATConfigurationChange result = new ATConfigurationChange();
		// boolean result_digest = false;
		if (config_ == null)
			return result;
		if (getRiskFreeInterest() != config_.riskFreeInterest) {
			setRiskFreeInterest(config_.riskFreeInterest);
			log.info(String.format("[%s] -> Risk Free Int [%.2f]", user.getEmail(), getRiskFreeInterest()));
			result.isChanged = true;
		}
		if (getPriceStatAmplitude() != config_.priceStatAmplitude) {
			setPriceStatAmplitude(config_.priceStatAmplitude);
			log.info(String.format("[%s] -> Price Stat Amp [%.2f]", user.getEmail(), getPriceStatAmplitude()));
			result.isChanged = true;
		}
		if (getPriceStatSlope() != config_.priceStatSlope) {
			setPriceStatSlope(config_.priceStatSlope);
			log.info(String.format("[%s] -> Price Stat Slope [%.2f]", user.getEmail(), getPriceStatSlope()));
			result.isChanged = true;
		}
		if (getProcessPeriod() != config_.processPeriod) {
			setProcessPeriod(config_.processPeriod);
			log.info(String.format("[%s] -> Processing period [%d]", user.getEmail(), getProcessPeriod()));
			result.isChanged = true;
		}
		if (getPricingPeriod() != config_.pricingPeriod) {
			setPricingPeriod(config_.pricingPeriod);
			log.info(String.format("[%s] -> Pricing period [%d]", user.getEmail(), getPricingPeriod()));
			result.isChanged = true;
		}
		if (getMaxOptionDays() != config_.maxOptionDays) {
			setMaxOptionDays(config_.maxOptionDays);
			log.info(String.format("[%s] -> Max Option Days [%d]", user.getEmail(), getMaxOptionDays()));
			result.isChanged = true;
		}
		if (getMinOptionDays() != config_.minOptionDays) {
			setMinOptionDays(config_.minOptionDays);
			log.info(String.format("[%s] -> Min Option Days [%d]", user.getEmail(), getMinOptionDays()));
			result.isChanged = true;
		}
		if (getBaseSafetyMargin() != config_.baseSafetyMargin) {
			setBaseSafetyMargin(config_.baseSafetyMargin);
			log.info(String.format("[%s] -> Safety Margin [%.2f]", user.getEmail(), getBaseSafetyMargin()));
			result.isChanged = true;
		}
		if (getBoundaryMargin() != config_.boundaryMargin) {
			setBoundaryMargin(config_.boundaryMargin);
			log.info(String.format("[%s] -> Boundary Margin [%.2f]", user.getEmail(), getBoundaryMargin()));
			result.isChanged = true;
			result.isBoundaryMargin = true;
		}
		if (getBoundaryRange() != config_.boundaryRange) {
			setBoundaryRange(config_.boundaryRange);
			log.info(String.format("[%s] -> Boundary Range [%d]", user.getEmail(), getBoundaryRange()));
			result.isChanged = true;
			result.isBoundaryRange = true;
		}
		if (getMinApy() != config_.minApy) {
			setMinApy(config_.minApy);
			log.info(String.format("[%s] -> Minimum APY [%.2f]", user.getEmail(), getMinApy()));
			result.isChanged = true;
		}
		if (getMinPx() != config_.minPx) {
			setMinPx(config_.minPx);
			log.info(String.format("[%s] -> Minimum Px [%.2f]", user.getEmail(), getMinPx()));
			result.isChanged = true;
		}
		if (getTimeDecay() != config_.timeDecay) {
			setTimeDecay(config_.timeDecay);
			log.info(String.format("[%s] -> Time decay [%.2f]", user.getEmail(), getTimeDecay()));
			result.isChanged = true;
		}
		if (getSaveBulkSize() != config_.saveBulkSize) {
			setSaveBulkSize(config_.saveBulkSize);
			log.info(String.format("[%s] -> Save bulk size [%d]", user.getEmail(), getSaveBulkSize()));
			result.isChanged = true;
		}
		if (isNotifications() != config_.isNotifications) {
			setNotifications(config_.isNotifications);
			log.info(String.format("[%s] -> Notifications [%b]", user.getEmail(), isNotifications()));
			result.isChanged = true;
		}
		if (!Arrays.equals(getExtraSymbols(), config_.getExtraSymbols())) {
			setExtraSymbols(config_.getExtraSymbols());
			log.info(String.format("[%s] -> Extra symbols [%s]", user.getEmail(), String.join(", ", getExtraSymbols())));
			result.isChanged = true;
		}
		if (!Arrays.equals(getOptionFlags(), config_.getOptionFlags())) {
			setOptionFlags(config_.getOptionFlags());
			log.info(String.format("[%s] -> Option flags [%s]", user.getEmail(), String.join(", ", getOptionFlags())));
			result.isChanged = true;
		}
		if (!Arrays.equals(getIdeaTriggers(), config_.getIdeaTriggers())) {
			setIdeaTriggers(config_.getIdeaTriggers());
			log.info(String.format("[%s] -> Idea triggers [%s]", user.getEmail(), String.join(", ", getIdeaTriggers())));
			result.isChanged = true;
			result.isIdeaTriggers = true;
		}
		if (!Arrays.equals(getBlacklistSymbols(), config_.getBlacklistSymbols())) {
			setBlacklistSymbols(config_.getBlacklistSymbols());
			log.info(String.format("[%s] -> Blacklist [%s]", user.getEmail(), String.join(", ", getBlacklistSymbols())));
			result.isChanged = true;
		}
		if (!Arrays.equals(getAlertTriggers(), config_.getAlertTriggers())) {
			setAlertTriggers(config_.getAlertTriggers());
			log.info(String.format("[%s] -> Alert triggers [%s]", user.getEmail(), String.join(", ", getAlertTriggers())));
			result.isChanged = true;
			result.isAlertTriggers = true;
		}
		if (!Arrays.equals(getSpreadSymbols(), config_.getSpreadSymbols())) {
			setSpreadSymbols(config_.getSpreadSymbols());
			log.info(String.format("[%s] -> Spread symbols [%s]", user.getEmail(), String.join(", ", getSpreadSymbols())));
			result.isChanged = true;
		}
		return result;
	}

	public int getWebPort() {
		return _config.getInt("web.port");
	}

	public int getWebPoolSizeMin() {
		return _config.getInt("web.pool.min");
	}

	public int getWebPoolSizeMax() {
		return _config.getInt("web.pool.max");
	}

	public int getWebPoolIdldeTimeout() {
		return _config.getInt("web.pool.idleto");
	}
	
	public int getWebFormSizeMax() {
		return _config.getInt("web.formsize.max");
	}

	public String getExportPath() {
		return _config.getString("export.path");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getBaseSafetyMargin()
	 */
	@Override
	public double getBaseSafetyMargin() {
		return _config == null ? baseSafetyMargin : _config.getDouble("safety.margin");
	}

	public void setBaseSafetyMargin(double baseSafetyMargin) {
		_config.setProperty("safety.margin", baseSafetyMargin);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getMinApy()
	 */
	@Override
	public double getMinApy() {
		return _config == null ? minApy : _config.getDouble("min.apy");
	}

	public void setMinApy(double minApy) {
		_config.setProperty("min.apy", minApy);
	}

	@Override
	public double getMinPx() {
		return _config == null ? minPx : _config.getDouble("min.px");
	}

	public void setMinPx(double minPx) {
		_config.setProperty("min.px", minPx);
	}

	public long getProcessPeriod() {
		return _config.getLong("period.process");
	}

	public void setProcessPeriod(long processPeriod) {
		_config.setProperty("period.process", processPeriod);
	}

	public long getPricingPeriod() {
		return _config.getLong("period.pricing");
	}

	public void setPricingPeriod(long pricingPeriod) {
		_config.setProperty("period.pricing", pricingPeriod);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getMinOptionDays()
	 */
	@Override
	public int getMinOptionDays() {
		return _config == null ? minOptionDays : _config.getInt("option.days.min");
	}

	public void setMinOptionDays(int minOptionDays) {
		_config.setProperty("option.days.min", minOptionDays);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getMaxOptionDays()
	 */
	@Override
	public int getMaxOptionDays() {
		return _config == null ? maxOptionDays : _config.getInt("option.days.max");
	}

	public void setMaxOptionDays(int maxOptionDays) {
		_config.setProperty("option.days.max", maxOptionDays);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getExtraSymbols()
	 */
	@Override
	public String[] getExtraSymbols() {
		return extraSymbols;
	}

	public void setExtraSymbols(String[] extraSymbols) {
		this.extraSymbols = extraSymbols;
	}

	public String getString(String key_) {
		return _config.getString(key_);
	}

	public void setMarketStatus(int status_) {
		_status = status_;
	}

	public int getMarketStatus() {
		return _status;
	}

	public double getPriceStatSlope() {
		return _config.getDouble("stat.price.slope");
	}

	public void setPriceStatSlope(double priceStatSlope) {
		_config.setProperty("stat.price.slope", priceStatSlope);
	}

	public double getPriceStatAmplitude() {
		return _config.getDouble("stat.price.amp");
	}

	public void setPriceStatAmplitude(double priceStatAmplitude) {
		_config.setProperty("stat.price.amp", priceStatAmplitude);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getIdeaTriggers()
	 */
	@Override
	public String[] getIdeaTriggers() {
		return ideaTriggers;
	}

	public void setIdeaTriggers(String[] fixedOpportunities) {
		this.ideaTriggers = fixedOpportunities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getAlertTriggers()
	 */
	@Override
	public String[] getAlertTriggers() {
		return alertTriggers;
	}

	public void setAlertTriggers(String[] priceTriggers) {
		this.alertTriggers = priceTriggers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getBlacklistSymbols()
	 */
	@Override
	public String[] getBlacklistSymbols() {
		return blacklistSymbols;
	}

	public void setBlacklistSymbols(String[] blacklistSymbols_) {
		this.blacklistSymbols = blacklistSymbols_;
	}

	public int getSaveBulkSize() {
		return _config.getInt("save.bulk.size");
	}

	public void setSaveBulkSize(int saveBulkSize) {
		_config.setProperty("save.bulk.size", saveBulkSize);
	}

	public double getBoundaryMargin() {
		return _config.getDouble("boundary.margin");
	}

	public void setBoundaryMargin(double boundaryMargin) {
		_config.setProperty("boundary.margin", boundaryMargin);
	}

	public int getBoundaryRange() {
		return _config.getInt("boundary.range");

	}

	public void setBoundaryRange(int boundaryRange) {
		_config.setProperty("boundary.range", boundaryRange);
	}

	public double getRiskFreeInterest() {
		return _config.getDouble("interest.riskfree");
	}

	public void setRiskFreeInterest(double riskFreeInterest) {
		_config.setProperty("interest.riskfree", riskFreeInterest);
	}

	public double getTimeDecay() {
		return _config.getDouble("time.decay");
	}

	public void setTimeDecay(double timeDecay) {
		_config.setProperty("time.decay", timeDecay);
	}

	public String[] getOptionFlags() {
		return optionFlags;
	}

	public void setOptionFlags(String[] optionFlags_) {
		this.optionFlags = optionFlags_;
		if (optionFlags != null) {
			_optionFlagList = Arrays.asList(optionFlags);
			_optionFlagList.replaceAll(String::toUpperCase);
		}
	}

	@Override
	public boolean isNotifications() {
		return _config == null ? isNotifications : _config.getBoolean("notifications");
	}

	public void setNotifications(boolean isNotifications) {
		_config.setProperty("notifications", isNotifications);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.isoplane.at.source.model.IATUserConfiguration#getOptionFlagList()
	 */
	@Override
	public List<String> getOptionFlagList() {
		if (optionFlags != null) {
			_optionFlagList = Arrays.asList(optionFlags);
			_optionFlagList.replaceAll(String::toUpperCase);
		} else {
			_optionFlagList = new ArrayList<>();
		}
		return _optionFlagList;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String[] getSpreadSymbols() {
		return spreadSymbols;
	}

	public void setSpreadSymbols(String[] spreadSymbols) {
		this.spreadSymbols = spreadSymbols;
	}

	public static class ATConfigurationChange {

		private boolean isChanged;
		private boolean isBoundaryMargin;
		private boolean isBoundaryRange;
		private boolean isIdeaTriggers;
		private boolean isMaxOptionDays;
		private boolean isPriceStatAmplitude;
		private boolean isPriceStatSlope;
		private boolean isExtraSymbols;
		private boolean isBlacklist;

		public boolean isMaxOptionDays() {
			return isMaxOptionDays;
		}

		public boolean isPriceStatAmplitude() {
			return isPriceStatAmplitude;
		}

		public boolean isPriceStatSlope() {
			return isPriceStatSlope;
		}

		public boolean isExtraSymbols() {
			return isExtraSymbols;
		}

		public boolean isBlacklist() {
			return isBlacklist;
		}

		public boolean isIdeaTriggers() {
			return isIdeaTriggers;
		}

		public boolean isAlertTriggers() {
			return isAlertTriggers;
		}

		private boolean isAlertTriggers;

		public boolean isChanged() {
			return isChanged;
		}

		public boolean isBoundaryMargin() {
			return isBoundaryMargin;
		}

		public boolean isBoundaryRange() {
			return isBoundaryRange;
		}

	}

}
