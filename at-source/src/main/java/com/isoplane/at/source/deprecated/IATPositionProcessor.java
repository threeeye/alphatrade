package com.isoplane.at.source.deprecated;

import java.util.Set;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATExposure;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentPosition;

public interface IATPositionProcessor extends IATService {

	TreeSet<ATPersistentPosition> getPositions(String userId);

	void updatePositions(ATPosition... positions);

	Set<ATExposure> getExposure();

	String getNewPositions(String userId);

	Set<String> getUserIds(String occId);

}
