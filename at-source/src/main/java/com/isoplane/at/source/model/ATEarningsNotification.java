package com.isoplane.at.source.model;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.util.ATFormats;

public class ATEarningsNotification extends ATBaseNotification implements IATCorporateEventNotification {

//	private String symbol;
	private Date earnDate;

	public ATEarningsNotification() {
		super.setSilent(true);
	}

	@Override
	public String getNotificationKey() {
		String key = String.format("earn %s", getSymbol());
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATEarningsNotification oldEarn = oldB != null && oldB instanceof ATChangeNotification
				? (ATEarningsNotification) oldB
				: null;

		if (oldEarn != null && DateUtils.isSameDay(oldEarn.getDate(), this.getDate())) {
			return null;
		}
		Date earnDate = this.getEarningsDate();
		String title = String.format("%1s ⋲ %2s", this.getSymbol(), // ⋹ •
				ATFormats.DATE_MM_dd.get().format(earnDate));
		String body = String.format("⚠ %s • Earnings • %s", this.getLabel().replaceAll(" +", " "), ATFormats.DATE_yyyy_MM_dd.get().format(earnDate));
		String iconStar = this.getUserIds().isEmpty() ? "" : "_star";
		String icon = String.format("/globe%s.ico", iconStar);

		return new ATNotificationContent(title, body, icon);
	}

//	public String getSymbol() {
//		return symbol;
//	}

	public Date getEarningsDate() {
		return earnDate;
	}

	public void setEarningsDate(Date earnDate) {
		this.earnDate = earnDate;
	}

	@Override
	public String getNotificationTag() {
		String dStr = ATFormats.DATE_yyMMdd.get().format(getEarningsDate());
		return String.format("earn_%s_%s", getSymbol(), dStr);// getEarningsDate().hashCode());
	}

	@Override
	public String getType() {
		return "earnings";
	}

}
