package com.isoplane.at.source.deprecated;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.math.MBretBlackScholesFormula;
import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATOpportunity;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATVerticalSpread;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentPosition;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATStandardNormalTable;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;
import com.isoplane.at.source.controller.IATEquityFoundationProvider;
import com.isoplane.at.source.controller.IATSecuritiesProvider;
import com.isoplane.at.source.controller.IATSymbolsProvider;
import com.isoplane.at.source.model.ATSpreadNotification;
import com.isoplane.at.source.model.TdnEquityResearchResponse;
import com.isoplane.at.source.model.TdnOptionRersearchConfig;
import com.isoplane.at.source.model.TdnSecurity;

@Deprecated
// Icons: https://apps.timwhitlock.info/emoji/tables/unicode
public class ATSpreadProcessor implements IATSpreadProcessor {

	static final Logger log = LoggerFactory.getLogger(ATSpreadProcessor.class);

	private IATConfiguration _config;
	private Gson _gson;
	private IATSecuritiesProvider _securitiesProvider;
	private IATEquityFoundationProvider _foundationProvider;
	private IATPositionProcessor _positionProcessor;
	private IATSymbolsProvider _symbolsProvider;

	public ATSpreadProcessor(IATConfiguration config_) {
		_gson = new GsonBuilder().serializeNulls().create();
		_config = config_;
	}

	@Override
	public void initService() {
		_securitiesProvider = ATSourceServiceRegistry.getSecuritiesProvider();
		if (_securitiesProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATSecuritiesProvider.class.getSimpleName()));
		}
		_foundationProvider = ATSourceServiceRegistry.getEquityFoundationProvider();
		if (_foundationProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATEquityFoundationProvider.class.getSimpleName()));
		}
		_positionProcessor = ATSourceServiceRegistry.getPositionProcessor();
		if (_positionProcessor == null) {
			throw new ATException(String.format("Initialization error [%s]", IATPositionProcessor.class.getSimpleName()));
		}
		_symbolsProvider = ATSourceServiceRegistry.getSymbolsProvider();
		if (_symbolsProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATSymbolsProvider.class.getSimpleName()));
		}
	}

	@Override
	public TreeSet<ATOpportunity> getSpreads() {
		Map<String, ATPersistentEquityFoundation> foundations = _foundationProvider.getFoundationMap();

		// long maxAge = _config.getInt("spreadRecordAge") * 60L * 1000L; // In minutes -> ms
		// String apyMode = _config.getString("spreadApyMode");
		// String avgMode = _config.getString("spreadAvgMode");

		TdnOptionRersearchConfig config = new TdnOptionRersearchConfig();
		config.eqtPxMin = _config.getDouble("spreadUlMin");
		config.skipEarn = false;
		config.recMaxAge = _config.getInt("spreadRecordAge") * 60L * 1000L;
		config.spreadApyMode = _config.getString("spreadApyMode");
		config.eqtAvgMode = _config.getString("spreadAvgMode");
		config.earningsRiskFactor = _config.getDouble("spreadEarningsZFactor");
		config.stratApyMin = _config.getDouble("spreadApyMin") / 100.0;
		config.stratMaxDays = _config.getInt("spreadCutoffDays");
		config.stratDateMaxCount = 3;
		config.stratPxMin = _config.getDouble("spreadPxMin");
		config.stratMarginMin = _config.getDouble("spreadSafetyMargin") / 100.0;
		config.spreadWidthMaxCount = _config.getInt("spreadMaxCount");
		config.spreadWidthCountRiskFactor = _config.getDouble("spreadCountFactor", 1.0);
		config.spreadWidthMaxPx = _config.getDouble("spreadMaxWidth");

		// double apyMin = _config.getDouble("spreadApyMin") / 100.0;
		// String apyMode = _config.getString("spreadApyMode", "bid");
		// String avgMode = _config.getString("spreadAvgMode", "sma60");
		// int cutoffDays = _config.getInt("spreadCutoffDays");
		// double earningsZFactor = _config.getDouble("spreadEarningsZFactor");
		// double pxMin = isNotification_ ? nCfg[0] : _config.getDouble("spreadPxMin");
		// double safetyMgn = (isNotification_ ? nCfg[1] : _config.getDouble("spreadSafetyMargin")) / 100.0;
		// int spreadMaxCount = isNotification_ ? (int) nCfg[2] : _config.getInt("spreadMaxCount");
		// double spreadCountFactor = _config.getDouble("spreadCountFactor", 1.0);
		// double spreadMaxWidth = isNotification_ ? nCfg[3] : _config.getDouble("spreadMaxWidth");

		log.info(String.format("Generating spread opportunities [avg: %s; apy: %s]", config.eqtAvgMode, config.spreadApyMode));

		Set<ATPersistentPosition> positions = _positionProcessor.getPositions(null);
		Set<String> stockPositionIds = positions.stream().map(p -> p.isOption() ? p.getUnderlyingOccId() : p.getOccId()).collect(Collectors.toSet());

		TreeSet<ATOpportunity> result = new TreeSet<>();
		Set<String> stockIds = _securitiesProvider.getStockIds();
		for (String stockOccId : stockIds) {
			try {
				// double ulMin = _config.getDouble("spreadUlMin");
				ATPersistentSecurity stock = _securitiesProvider.get(stockOccId);
				ATPersistentEquityFoundation foundation = foundations.get(stockOccId);
				if (!validateStock(stock, foundation, config.eqtPxMin))
					continue;
				List<ATPersistentSecurity> options = getOptionChain(stockOccId, config.recMaxAge);
				TreeSet<ATVerticalSpread> spreads = analyzeSpreads(stock, options, foundation, stockPositionIds, config, false);
				// log.info(String.format("Spreads [%5d, %-5s]", spreads.size(), stockOccId));
				TreeSet<ATOpportunity> stockResult = constructOpportunities(stock, spreads, foundation);
				if (!stockResult.isEmpty() && stockPositionIds.contains(stockOccId)) {
					stockResult.first().setIsActive(true);
					// stockResult.first().setFlags("✅");
				}
				result.addAll(stockResult);
				log.debug(String.format("Spreads [%-5s %5d]", stock.getLabel(), stockResult.size()));
			} catch (Exception ex) {
				log.error(String.format("Error processing [%s]", stockOccId), ex);
			}

		}
		return result;
	}

	@Override
	public JsonObject getNewSpreads(String userId_, TdnOptionRersearchConfig config_) {
		log.debug("getNewSpreads");
		Map<String, ATPersistentEquityFoundation> foundations = _foundationProvider.getFoundationMap();
		if (config_ != null) {
			config_.cleanup();
		}

		// Set<String> ulSymbols = new HashSet<>();
		// if (StringUtils.isNotBlank(config_.eqtSymbols)) {
		// for (String sym : config_.eqtSymbols.split(",")) {
		// sym = sym.trim();
		// if (StringUtils.isNotBlank(sym)) {
		// ulSymbols.add(sym);
		// }
		// }
		// }
		// Set<String> stockIds1 = _securitiesProvider.getStockIds();
		// Set<String> stockIds2 = stockIds1.stream().filter(sym -> (ulSymbols.isEmpty() ? true :
		// ulSymbols.contains(sym))).collect(Collectors.toSet());
		// Set<String> stockIds = new TreeSet<>(stockIds2);

		Set<ATPersistentPosition> positions = _positionProcessor.getPositions(userId_);
		Set<String> positionIds = new HashSet<>();
		for (ATPersistentPosition pos : positions) {
			positionIds.add(pos.getOccId());
			String ulId = pos.getUnderlyingOccId();
			if (StringUtils.isNotBlank(ulId)) {
				positionIds.add(ulId);
			}
		}

		TdnEquityResearchResponse equities = _securitiesProvider.getSecurities(userId_, true, config_);
		TreeMap<String, TdnSecurity> equityMap = new TreeMap<>();
		for (TdnSecurity equity : equities.equities) {
			equityMap.put(equity.id, equity);
		}
		log.info(String.format("Generating spread opportunities [avg: %s; apy: %s] - %d", config_.eqtAvgMode, config_.spreadApyMode,
				equityMap.size()));

		long throttle = 0;
		long maxThrottle = 1000;

		JsonObject root = new JsonObject();
		JsonArray dataArray = new JsonArray();
		root.add("strategies", dataArray);
		root.addProperty("ulSize", equityMap.size());

		Date now = new Date();
		SimpleDateFormat yyymmddFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (String stockOccId : equityMap.keySet()) {
			try {
				ATPersistentSecurity stock = _securitiesProvider.get(stockOccId);
				ATPersistentEquityFoundation foundation = foundations.get(stockOccId);
				if (!validateStock(stock, foundation, config_.eqtPxMin))
					continue;
				List<ATPersistentSecurity> options = getOptionChain(stockOccId, config_.recMaxAge);
				TreeSet<ATVerticalSpread> spreads = analyzeSpreads(stock, options, foundation, positionIds, config_, false);
				if (spreads == null || spreads.isEmpty())
					continue;
				Double px = stock.getPrice();
				if (px == null)
					continue;
				double dailyDelta = (px - stock.getPriceClose()) / stock.getPriceClose();

				// TdnSecurity ul2 = new TdnSecurity();
				// ul2.id = stock.getOccId();
				// ul2.name = foundation.getDescription();
				// ul2.px = px;
				// ul2.pxClose=stock.getPriceClose();
				// ul2.add("pxChange", new JsonPrimitive(dailyDelta));
				// ul2.add("pxAsk", new JsonPrimitive(stock.getPriceAsk()));
				// ul2.add("pxBid", new JsonPrimitive(stock.getPriceBid()));
				// ul2.add("pxLoD", new JsonPrimitive(stock.getPriceLow()));
				// ul2.add("pxHiD", new JsonPrimitive(stock.getPriceHigh()));
				// ul2.add("pxAvg", new JsonPrimitive(foundation.getAvg(config_.eqtAvgMode)));
				// ul2.add("pxAvgMode", new JsonPrimitive(config_.eqtAvgMode));

				JsonObject ul = new JsonObject();
				ul.add("id", new JsonPrimitive(stock.getOccId()));
				ul.add("name", foundation.getDescription() != null ? new JsonPrimitive(foundation.getDescription()) : null);
				ul.add("px", new JsonPrimitive(px));
				ul.add("pxClose", new JsonPrimitive(stock.getPriceClose()));
				ul.add("pxChange", new JsonPrimitive(dailyDelta));
				ul.add("pxAsk", new JsonPrimitive(stock.getPriceAsk()));
				ul.add("pxBid", new JsonPrimitive(stock.getPriceBid()));
				ul.add("pxLoD", new JsonPrimitive(stock.getPriceLow()));
				ul.add("pxHiD", new JsonPrimitive(stock.getPriceHigh()));
				ul.add("pxAvg", new JsonPrimitive(foundation.getAvg(config_.eqtAvgMode)));
				ul.add("pxAvgMode", new JsonPrimitive(config_.eqtAvgMode));
				Double hv = foundation.getHV(config_.eqtHVMode);
				if (hv != null) {
					ul.add("pxHV", new JsonPrimitive(hv));
				}
				ul.add("pxHVMode", new JsonPrimitive(config_.eqtHVMode));
				ul.add("ts", new JsonPrimitive(stock.getUpdateTime()));
				Date earnDate = foundation.getEarningsDate();
				if (earnDate != null && now.before(earnDate)) {
					String earnDateStr = ATFormats.DATE_yyyy_MM_dd.get().format(earnDate);
					ul.add("dateEarn", new JsonPrimitive(earnDateStr));
				}
				Date exDivDate = foundation.getDividendExDate();
				// exDivDate = DateUtils.addDays(now, 5);
				if (exDivDate != null && now.before(exDivDate)) {
					String exDivStr = ATFormats.DATE_yyyy_MM_dd.get().format(exDivDate);
					ul.add("dateDiv", new JsonPrimitive(exDivStr));
					Double div = foundation.getDividend();
					// div = 4.5;
					if (div != null) {
						ul.add("divAmt", new JsonPrimitive(div));
					}
				}
				// String note = foundation.getNote();
				// if (StringUtils.isNotBlank(note)) {
				// ul.add("note", new JsonPrimitive(note));
				// }
				if (positionIds.contains(stock.getOccId())) {
					ul.add("owned", new JsonPrimitive(true));
				}
				int ulCredits = 0;
				if (stock.getPrice() < foundation.getSma60())
					ulCredits += 1;
				JsonArray strategyArray = new JsonArray();
				// ul.add("strategies", strategyArray);
				for (ATVerticalSpread spread : spreads) {
					if (throttle++ > maxThrottle)
						break;
					int stratCredits = ulCredits;
					double strikeWidth = spread.getStrikeSell() - spread.getStrikeBuy();
					if (strikeWidth <= 2.5)
						stratCredits += 1;
					if (spread.getSafetyMargin() > 10)
						stratCredits += 1;
					JsonObject strategy = new JsonObject();
					strategyArray.add(strategy);
					strategy.add("type", new JsonPrimitive("sprd-v-bull-put"));
					strategy.add("yldAbs", new JsonPrimitive(spread.getPriceSpread()));
					strategy.add("yldPctA", new JsonPrimitive(spread.getApy()));
					strategy.add("score", new JsonPrimitive(spread.getScore()));
					strategy.add("prb", new JsonPrimitive(spread.getProbability()));
					strategy.add("mrg", new JsonPrimitive(spread.getSafetyMargin()));
					strategy.add("strkWidth", new JsonPrimitive(strikeWidth));
					strategy.add("credits", new JsonPrimitive(stratCredits));
					JsonArray optionArray = new JsonArray();
					strategy.add("options", optionArray);
					if (strategyArray.size() == 0)
						continue;
					JsonObject o1 = new JsonObject();
					JsonObject o2 = new JsonObject();
					optionArray.add(o1);
					optionArray.add(o2);
					String expiryStr = yyymmddFormat.format(spread.getExpirationDate());
					o1.add("exp", new JsonPrimitive(expiryStr));
					o2.add("exp", new JsonPrimitive(expiryStr));
					o1.add("strike", new JsonPrimitive(spread.getStrikeBuy()));
					o2.add("strike", new JsonPrimitive(spread.getStrikeSell()));
					o1.add("count", new JsonPrimitive(-1));
					o2.add("count", new JsonPrimitive(+1));
					o1.add("px", spread.getPxBuyLast() != null ? new JsonPrimitive(spread.getPxBuyLast()) : null);
					o2.add("px", spread.getPxSellLast() != null ? new JsonPrimitive(spread.getPxSellLast()) : null);
					o1.add("pxAsk", new JsonPrimitive(spread.getPxBuyAsk()));
					o2.add("pxAsk", new JsonPrimitive(spread.getPxSellAsk()));
					o1.add("pxBid", new JsonPrimitive(spread.getPxBuyBid()));
					o2.add("pxBid", new JsonPrimitive(spread.getPxSellBid()));
					o1.add("pxLoD", spread.getPxBuyLoD() != null ? new JsonPrimitive(spread.getPxBuyLoD()) : null);
					o2.add("pxLoD", spread.getPxSellLoD() != null ? new JsonPrimitive(spread.getPxSellLoD()) : null);
					o1.add("pxHiD", spread.getPxBuyHiD() != null ? new JsonPrimitive(spread.getPxBuyHiD()) : null);
					o2.add("pxHiD", spread.getPxSellHiD() != null ? new JsonPrimitive(spread.getPxSellHiD()) : null);
					if (positionIds.contains(spread.getOccIdBuy())) {
						o1.add("owned", new JsonPrimitive(true));
					}
					if (positionIds.contains(spread.getOccIdSell())) {
						o2.add("owned", new JsonPrimitive(true));
					}
				}
				JsonObject idea = new JsonObject();
				idea.add("ul", ul);
				idea.add("strategies", strategyArray);
				dataArray.add(idea);

			} catch (Exception ex) {
				log.error(String.format("Error processing [%s]", stockOccId), ex);
			}
		}
		return root;
	}

	@Override
	public Collection<ATBaseNotification> getSpreadEvents(ATPersistentSecurity... secs_) {
		Collection<ATBaseNotification> result = new ArrayList<>();
		if (secs_ == null || secs_.length == 0) {
			return result;
		}

		double[] nCfg = null;
		String ncfgStr = _config.getString("spreadNotificationCfg");
		String[] ncfgArray = ncfgStr.split(",");
		nCfg = new double[ncfgArray.length];
		for (int i = 0; i < nCfg.length; i++) {
			// 0: spreadPxMin; 1: spreadSafetyMargin; 2: spreadMaxCount; 3: spreadMaxWidth
			nCfg[i] = Double.parseDouble(ncfgArray[i]);
		}

		TdnOptionRersearchConfig config = new TdnOptionRersearchConfig();
		config.eqtPxMin = _config.getDouble("spreadUlMin");
		config.recMaxAge = _config.getInt("spreadRecordAge") * 60L * 1000L;
		config.spreadApyMode = _config.getString("spreadApyMode");
		config.eqtAvgMode = _config.getString("spreadAvgMode");
		config.stratApyMin = _config.getDouble("spreadApyMin") / 100.0;
		config.stratMaxDays = _config.getInt("spreadCutoffDays");
		config.earningsRiskFactor = _config.getDouble("spreadEarningsZFactor");
		config.stratPxMin = nCfg[0];
		config.stratMarginMin = nCfg[1];
		config.spreadWidthMaxCount = (int) nCfg[2];
		config.spreadWidthCountRiskFactor = _config.getDouble("spreadCountFactor", 1.0);
		config.spreadWidthMaxPx = nCfg[3];

		// double apyMin = _config.getDouble("spreadApyMin") / 100.0;
		// String apyMode = _config.getString("spreadApyMode", "bid");
		// String avgMode = _config.getString("spreadAvgMode", "sma60");
		// int cutoffDays = _config.getInt("spreadCutoffDays");
		// double earningsZFactor = _config.getDouble("spreadEarningsZFactor");
		// double pxMin = isNotification_ ? nCfg[0] : _config.getDouble("spreadPxMin");
		// double safetyMgn = (isNotification_ ? nCfg[1] : _config.getDouble("spreadSafetyMargin")) / 100.0;
		// int spreadMaxCount = isNotification_ ? (int) nCfg[2] : _config.getInt("spreadMaxCount");
		// double spreadCountFactor = _config.getDouble("spreadCountFactor", 1.0);
		// double spreadMaxWidth = isNotification_ ? nCfg[3] : _config.getDouble("spreadMaxWidth");

		ATPersistentSecurity stock = secs_[0];
		double ulMin = _config.getDouble("spreadUlMin");
		ATPersistentEquityFoundation foundation = _foundationProvider.getFoundation(stock.getOccId());
		if (!validateStock(stock, foundation, ulMin))
			return result;

		List<ATPersistentSecurity> options;
		if (secs_.length == 1) {
			int maxAge = _config.getInt("spreadRecordAge") * 60 * 1000; // In minutes -> ms
			options = getOptionChain(stock.getOccId(), maxAge);
		} else {
			options = new ArrayList<>();
			for (int i = 0; i < secs_.length; i++) {
				ATPersistentSecurity sec = secs_[i];
				if (sec.isOption()) {
					options.add(sec);
				}
			}
		}

		Set<ATPersistentPosition> positions = _positionProcessor.getPositions(null); // TODO: Move this outside
		Set<String> positionIds = positions.stream().map(p -> p.getOccId()).collect(Collectors.toSet());

		TreeSet<ATVerticalSpread> spreads = analyzeSpreads(stock, options, foundation, positionIds, config, true);
		if (secs_.length > 0) { // TODO: What is this? Debug? (was 50)
			result = constructEvents(stock, spreads);
		}
		// result_digest = getSpreads(stock, options, foundation, apyMode);
		return result;
	}

	private Collection<ATBaseNotification> constructEvents(ATPersistentSecurity stock_, TreeSet<ATVerticalSpread> spreads_) {
		Set<ATPersistentPosition> positions = _positionProcessor.getPositions(null);
		Set<String> positionIds = positions.stream().map(p -> p.getOccId()).collect(Collectors.toSet());

		List<ATBaseNotification> result = new ArrayList<>();
		for (ATVerticalSpread spread : spreads_) {
			String clickAction = String.format("https://tradonado.com/security/%s", stock_.getSymbol());
			ATSpreadNotification event = new ATSpreadNotification();
			// setPrice
			event.setAbsolute(spread.getPriceSpread());
			event.setActive(positionIds.contains(stock_.getSymbol()));
			event.setApy(spread.getApy() * 100);
			// setSpreadBase
			event.setClickAction(clickAction);
			event.setDate(new Date());
			event.setExpirationDate(spread.getExpirationDate());
			event.setLabel(stock_.getSymbol());
			// event.setLabel(String.format("%s %s", stock_.getSymbol(), ATFormats.DATE_yyMMdd.format(spread.getExpirationDate())));
			event.setMeta(ATSpreadNotification.META_SPREAD, 1);
			event.setProbability(spread.getProbability());
			event.setStockPrice(stock_.getPrice());
			event.setStrikeBuy(spread.getStrikeBuy());
			event.setStrikeSell(spread.getStrikeSell());
			event.setSymbol(stock_.getSymbol());
			result.add(event);
			break;
		}
		// analyzeSpreads(stock_, options_, foundation_, isNotification_)
		return result;
	}

	private TreeSet<ATOpportunity> constructOpportunities(ATPersistentSecurity stock_, TreeSet<ATVerticalSpread> spreads_,
			ATPersistentEquityFoundation foundation_) {
		TreeSet<ATOpportunity> result = new TreeSet<>();
		String symbol = stock_.getOccId();
		try {
			// TreeSet<ATVerticalSpread> spreads = analyzeSpreads(stock_, options_, foundation_);
			if (spreads_ == null || spreads_.isEmpty())
				return result;
			String avgMode = _config.getString("spreadAvgMode", "sma60");
			Double pxAvg = foundation_.getAvg(avgMode);
			Double spx = stock_.getPrice();
			Double hv = getHV(foundation_, stock_);
			Double ifRate = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
			ATOpportunity header = new ATOpportunity();
			header.setDate(new Date());
			header.setLabel(stock_.getLabel());
			header.setOccId(symbol);
			header.setPriceAsk(stock_.getPriceAsk());
			header.setPriceAvg(pxAvg);
			header.setPriceBid(stock_.getPriceBid());
			header.setPriceClose(stock_.getPriceClose());
			header.setPriceMid(spx);
			header.setStrategy(ATOpportunity.HEADER);
			header.setScoreExternal(-1.0);
			header.setScoreInternal(-1.0);
			header.setSymbol(symbol);
			header.setTimestamp(new Date(stock_.getUpdateTime()));
			header.setSortKey(String.format("%s", symbol));
			// String note = foundation_.getNote();
			// if (!StringUtils.isBlank(note)) {
			// Map<String, Object> meta = new HashMap<>();
			// meta.put(IATAssetConstants.NOTE, note);
			// header.setMeta(_gson.toJson(meta));
			// }
			result.add(header);
			for (ATVerticalSpread spread : spreads_) {
				try {
					ATOpportunity opo = new ATOpportunity();
					opo.setDate(spread.getExpirationDate());
					long diff = spread.getExpirationDate().getTime() - System.currentTimeMillis();
					int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
					opo.setDays(days);
					// opo.setFlags(flags);
					opo.setLabel(spread.getOccIdStock());
					// opo.setOccId(spread.getOccId());
					opo.setPriceMid(spread.getPriceSpread());
					opo.setSafetyMgn(spread.getSafetyMargin());
					// opo.setScoreExternal(score);
					opo.setScoreInternal(spread.getScore());
					opo.setStrategy(ATOpportunity.SPREAD);
					opo.setStrikeA(spread.getStrikeBuy());
					opo.setStrikeB(spread.getStrikeSell());
					// opo.setSymbol(spread.getOccIdStock());
					opo.setTimestamp(new Date());
					opo.setYieldAbs(spread.getPriceSpread());
					opo.setYieldPctA(spread.getApy());
					opo.setProbability(spread.getProbability());
					opo.setFlags(spread.getFlags());
					opo.setSortKey(
							String.format("%s%s%12.2f%10.2f", symbol, ATFormats.DATE_yyyyMMdd.get().format(spread.getExpirationDate()),
									1000000000.0 - spread.getStrikeSell(), spread.getStrikeBuy()));
					String theoStr = "";
					Double iv = foundation_.getIV();
					if (iv == null || iv == 0) {
						log.debug(String.format("Missing IV [%s]", foundation_.getSymbol()));
						iv = foundation_.getHV("hv10");
					}
					if (spx != null && iv != null && ifRate != null) {
						double yearFract = days / 365.0;
						double pxTheoBuy = MBretBlackScholesFormula.calculate(false, spx, spread.getStrikeBuy(), ifRate, yearFract, iv);
						double pxTheoSell = MBretBlackScholesFormula.calculate(false, spx, spread.getStrikeSell(), ifRate, yearFract, iv);
						theoStr = String.format("%.2f - %.2f", pxTheoSell, pxTheoBuy);
					}

					// ADBE 180511P00200000, FFIV 180511P00133000
					if ("ADBE 180511P00200000".equals(spread.getOccIdSell())) {
						String msg = String.format("%s, %.2f, %.2f, %.4f, %.4f, %.4f", spread.getOccIdSell(), spx, spread.getStrikeSell(), ifRate,
								days / 365.0, iv);
						log.info(msg);
					}
					Map<String, Object> meta = spread.getMeta();
					meta.put("theo", theoStr);
					String metaStr = _gson.toJson(meta);
					opo.setMeta(metaStr);
					result.add(opo);
				} catch (Exception ex) {
					log.error(String.format("Error generating spread [%s - %s]: %s", spread.getOccIdBuy(), spread.getOccIdSell(), ex.getMessage()));
				}
			}
		} catch (Exception ex) {
			log.error(String.format("Error processing [%s]", symbol), ex);
		}
		return result;
	}

	private TreeSet<ATVerticalSpread> analyzeSpreads(ATPersistentSecurity stock_, Collection<ATPersistentSecurity> options_,
			ATPersistentEquityFoundation foundation_, Set<String> positionIds_, TdnOptionRersearchConfig config_, boolean isNotification_) {
		TreeSet<ATVerticalSpread> result = new TreeSet<>();
		Double sprice = stock_.getPrice();
		if (options_ == null || options_.isEmpty() || sprice == null)
			return result;
		Double hv = getHV(foundation_, stock_);
		if (hv == null) {
			log.debug(String.format("Failed to get stdev for [%-5s]", stock_.getOccId()));
			return result;
		}

		// double[] nCfg = null;
		// if (isNotification_) {
		// String ncfgStr = _config.getString("spreadNotificationCfg");
		// String[] ncfgArray = ncfgStr.split(",");
		// nCfg = new double[ncfgArray.length];
		// for (int i = 0; i < nCfg.length; i++) {
		// // 0: spreadPxMin; 1: spreadSafetyMargin; 2: spreadMaxCount; 3: spreadMaxWidth
		// nCfg[i] = Double.parseDouble(ncfgArray[i]);
		// }
		// }

		Map<Date, Map<String, TreeSet<ATPersistentSecurity>>> dateMap = new TreeMap<>();
		// double apyMin = _config.getDouble("spreadApyMin") / 100.0;
		// String apyMode = _config.getString("spreadApyMode", "bid");
		// // String avgMode = _config.getString("spreadAvgMode", "sma60");
		// int cutoffDays = _config.getInt("spreadCutoffDays");
		// double earningsZFactor = _config.getDouble("spreadEarningsZFactor");
		// double pxMin = isNotification_ ? nCfg[0] : _config.getDouble("spreadPxMin");
		// double safetyMgn = (isNotification_ ? nCfg[1] : _config.getDouble("spreadSafetyMargin")) / 100.0;
		// int spreadMaxCount = isNotification_ ? (int) nCfg[2] : _config.getInt("spreadMaxCount");
		// double spreadCountFactor = _config.getDouble("spreadCountFactor", 1.0);
		// double spreadMaxWidth = isNotification_ ? nCfg[3] : _config.getDouble("spreadMaxWidth");
		for (ATPersistentSecurity o : options_) {
			try {
				if (o.getPriceAsk() == null || o.getPriceBid() == null)
					continue;
				Map<String, TreeSet<ATPersistentSecurity>> options = dateMap.get(o.getExpirationDate());
				if (options == null) {
					options = new HashMap<>();
					options.put(ATOptionSecurity.Call, new TreeSet<>());
					options.put(ATOptionSecurity.Put, new TreeSet<>());
					dateMap.put(o.getExpirationDate(), options);
				}
				options.get(o.getOptionRight()).add(o);
			} catch (Exception ex) {
				// Object ob = o.getExpirationDate();
				log.error(String.format("Error assigning option [%s]: %s", o, o.getExpirationDate()), ex);
			}
		}

		Instant now = Instant.now().truncatedTo(ChronoUnit.DAYS);
		Double stdev = null;
		Date earningsDate = foundation_ != null ? foundation_.getEarningsDate() : null;
		for (Date date : dateMap.keySet()) {
			long days = ChronoUnit.DAYS.between(now, date.toInstant()) + 1;
			if (days > config_.stratMaxDays)
				continue;
			if (stdev == null) {
				stdev = sprice * hv * Math.sqrt(days / 365.0);
			}
			TreeSet<ATVerticalSpread> dateSet = new TreeSet<>();
			Map<String, TreeSet<ATPersistentSecurity>> optionMap = dateMap.get(date);
			for (String right : optionMap.keySet()) {
				if (ATOptionSecurity.Call.equals(right))
					continue;
				NavigableSet<ATPersistentSecurity> options = optionMap.get(right).descendingSet();
				Iterator<ATPersistentSecurity> oi = options.iterator();
				while (oi.hasNext()) {
					ATPersistentSecurity sellOption = oi.next();
					double margin = (sprice - sellOption.getStrike()) / sprice;
					NavigableSet<ATPersistentSecurity> tail = options.tailSet(sellOption, false);
					if (tail.size() == 0 || margin < config_.stratMarginMin)
						continue;
					double adjustingSavetyMgn = config_.stratMarginMin;
					// int spreadCount = 0;
					for (ATPersistentSecurity buyOption : tail) {
						// spreadCount++;
						adjustingSavetyMgn *= (1 + config_.spreadWidthCountRiskFactor);
						if ((isNotification_ && (sellOption.getPriceBid() - buyOption.getPriceAsk() < config_.stratPxMin))
								|| margin < adjustingSavetyMgn)
							continue;
						// log.info(String.format("%3d %s - %s, %10.2f", spreadCount, buyOption, sellOption, sellOption.getStrike() -
						// buyOption.getStrike()));
						double zFactor = 1.0;
						double strikeSpread = sellOption.getStrike() - buyOption.getStrike();
						double pxAskDelta = sellOption.getPriceAsk() - buyOption.getPriceAsk();
						double pxBidDelta = sellOption.getPriceBid() - buyOption.getPriceBid();
						double price;
						switch (config_.spreadApyMode) {
						case "ask":
							price = pxAskDelta;
							break;
						case "bid":
							price = pxBidDelta;
							break;
						case "mkt":
							price = sellOption.getPriceBid() - buyOption.getPriceAsk();
							break;
						default: // -> avg/mid
							price = (pxAskDelta + pxBidDelta) / 2.0;
							break;
						}
						double apy = (price / strikeSpread) * (360.0 / days);
						if (price < config_.stratPxMin || apy < config_.stratApyMin || strikeSpread > config_.spreadWidthMaxPx
								|| (buyOption.getPriceBid() == 0 && sellOption.getPriceBid() == 0))
							continue;
						double score1 = sellOption.getPriceBid() / buyOption.getPriceAsk();
						double score2 = pxBidDelta != 0 ? pxAskDelta / pxBidDelta : pxAskDelta / 0.0001;
						double score = (8 * score1 + 2 * score2) / 10.0;
						if (score == Double.NaN || score == Double.NEGATIVE_INFINITY || score == Double.POSITIVE_INFINITY) {
							log.info(String.format("Invalid score [%f]", score));
						}
						if (sellOption.getPriceBid() == 0 || buyOption.getPriceBid() == 0 || buyOption.getPriceBid() == null)
							continue;
						Map<String, Object> meta = new HashMap<>();
						List<String> flags = new ArrayList<>();
						if (positionIds_ != null && (positionIds_.contains(sellOption.getOccId()) || positionIds_.contains(buyOption.getOccId()))) {
							flags.add("✅");
						}
						// ⚠: Earnings
						// 👍: All Buy > Sell & Bid > Ask
						// 👎: Sell Ask > Buy Ask
						// 🍀: Strike < SMA 250
						// 💎: Strike < Min 60
						if (earningsDate != null && !earningsDate.after(date)) {
							if (config_.skipEarn != null && config_.skipEarn)
								continue;
							meta.put("earn", ATFormats.DATE_MM_dd.get().format(earningsDate));
							flags.add("⚠");
							zFactor = 1.0 + config_.earningsRiskFactor;
						}
						if (sellOption.getStrike() < foundation_.getSma254()) {
							flags.add("🍀");
						}
						if ((sellOption.getPriceBid() > buyOption.getPriceBid()) && (sellOption.getPriceAsk() > buyOption.getPriceAsk())
								&& (sellOption.getPriceBid() > buyOption.getPriceAsk())) {
							// All buy prices above sell prices and bid above ask
							flags.add("👍");
						} else if (sellOption.getPriceAsk() < buyOption.getPriceAsk()) {
							flags.add("👎");
						}
						if (sellOption.getStrike() < foundation_.getMin60()) {
							flags.add("💎");
						}
						String range = String.format("%.2f•%.2f - %.2f•%.2f", sellOption.getPriceBid(), sellOption.getPriceAsk(),
								buyOption.getPriceBid(), buyOption.getPriceAsk());
						meta.put("range", range);
						ATVerticalSpread spread = new ATVerticalSpread();
						spread.setApy(apy);
						spread.setExpirationDate(sellOption.getExpirationDate());
						spread.setFlags(String.join("", flags));
						spread.setOccIdBuy(buyOption.getOccId());
						spread.setOccIdSell(sellOption.getOccId());
						spread.setOccIdStock(stock_.getOccId());
						spread.setPriceSpread(price);
						spread.setPriceStock(stock_.getPrice());
						spread.setRight(right);
						spread.setSafetyMargin(margin);
						spread.setScore(score);
						spread.setStrikeBuy(buyOption.getStrike());
						spread.setStrikeSell(sellOption.getStrike());
						spread.setMeta(meta);
						spread.setPxBuyAsk(buyOption.getPriceAsk());
						spread.setPxBuyBid(buyOption.getPriceBid());
						Double buyPxLo = buyOption.getPriceLow();
						if (buyPxLo == null) {
							log.debug(String.format("px Buy Lo is null [%s]", buyOption.getOccId()));
						}
						spread.setPxBuyLoD(buyPxLo);
						Double buyPxHi = buyOption.getPriceHigh();
						if (buyPxHi == null) {
							log.debug(String.format("px Buy Hi is null [%s]", buyOption.getOccId()));
						}
						spread.setPxBuyHiD(buyPxHi);
						Double buyPxLast = buyOption.getPriceLast();
						if (buyPxLast == null) {
							log.debug(String.format("px Buy Last is null [%s]", buyOption.getOccId()));
						}
						spread.setPxBuyLast(buyPxLast);
						spread.setPxSellAsk(sellOption.getPriceAsk());
						spread.setPxSellBid(sellOption.getPriceBid());
						Double sellPxLo = sellOption.getPriceLow();
						if (sellPxLo == null) {
							log.debug(String.format("px Sell Lo is null [%s]", sellOption.getOccId()));
						}
						spread.setPxSellLoD(sellPxLo);
						Double sellPxHi = sellOption.getPriceHigh();
						if (sellPxHi == null) {
							log.debug(String.format("px Sell Hi is null [%s]", sellOption.getOccId()));
						}
						spread.setPxSellHiD(sellPxHi);
						Double sellPxLast = sellOption.getPriceLast();
						if (sellPxLast == null) {
							log.debug(String.format("px Sell Last is null [%s]", sellOption.getOccId()));
						}
						spread.setPxSellLast(sellPxLast);
						// spread.setMeta(_gson.toJson(meta));
						if (stdev != null && stdev != 0) {
							double z = (stock_.getPrice() - sellOption.getStrike()) / (stdev * zFactor);
							double p = ATStandardNormalTable.getProbabilityGreaterThan(z);
							if (p < config_.stratMarginMin)
								continue;
							spread.setProbability(p);
						}
						dateSet.add(spread);

						// log.info(String.format("Spread: %-6s%s %s [%7.2f,%7.2f - %5.2f] %6.2f - %6.2f", anchor.getStockOccId(),
						// ATFormats.DATE_yyyyMMdd.format(anchor.getExpirationDate()), right, o.getStrike(), anchor.getStrike(), strikeSpread,
						// price, apy * 100.0));
					}
					// log.info(String.format("Options [%s - %s]", anchor, tail));
				}
				// log.info(String.format("Options [%s - %s]", date, options));
			}
			if (dateSet.isEmpty())
				continue;
			TreeSet<ATVerticalSpread> sorted = new TreeSet<>(new Comparator<ATVerticalSpread>() {

				@Override
				public int compare(ATVerticalSpread a_, ATVerticalSpread b_) {
					int comp = Double.compare(b_.getApy(), a_.getApy());
					return comp;
				}
			});
			sorted.addAll(dateSet);
			for (int i = 0; i < config_.stratDateMaxCount; i++) {
				ATVerticalSpread first = sorted.pollFirst();
				if (first != null) {
					result.add(first);
				}
			}
		}
		return result;
	}

	private List<ATPersistentSecurity> getOptionChain(String symbol_, long maxAge_) {
		long timecut = System.currentTimeMillis() - maxAge_;
		Collection<ATPersistentSecurity> optionChain = _securitiesProvider.getOptionChain(symbol_);
		List<ATPersistentSecurity> options = optionChain.stream().filter(o -> o.getUpdateTime() > timecut).collect(Collectors.toList());
		return options;
	}

	private boolean validateStock(ATPersistentSecurity stock_, ATPersistentEquityFoundation foundation_, double ulMin_) {
		String symbol = stock_.getOccId();
		Double stockPx = stock_.getPrice();
		if (stockPx == null) {
			log.error(String.format("[%-5s] Price is null", symbol));
			return false;
		}
		if (stockPx < ulMin_)
			return false;
		Set<String> blacklist = _symbolsProvider.getBlacklist();
		if (blacklist.contains(symbol)) {
			log.info(String.format("[%-5s] Blacklisted", symbol));
			return false;
		}
		if (foundation_ == null) {
			log.error(String.format("[%-5s] Missing foundation", symbol));
			return false;
		}
		return true;
	}

	private Double getHV(IATStatistics foundation_, ATPersistentSecurity stock_) {
		String hvMode = _config.getString("spreadHvMode");
		Double hv = foundation_.getHV(hvMode);
		if (hv == null) {
			hv = stock_.getHV();
		}
		return hv;
	}

}
