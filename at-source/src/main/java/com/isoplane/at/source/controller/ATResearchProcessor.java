package com.isoplane.at.source.controller;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableSet;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOpportunity2;
import com.isoplane.at.commons.model.ATOpportunity2.ATOpportunitySegment;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserEquityModel.ATModelFilter;
import com.isoplane.at.commons.model.ATUserEquityModel.EATPriceReference;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.source.util.ATModelTranslator;

public class ATResearchProcessor {

	static final Logger log = LoggerFactory.getLogger(ATResearchProcessor.class);

	static private ATResearchProcessor _instance;

	static public ATResearchProcessor getInstance() {
		if (_instance == null) {
			_instance = new ATResearchProcessor();
		}
		return _instance;
	}

	protected ATResearchProcessor() {
	}

	public ATSecurityPack getResearch(ATUserEquityModel model_) {
		String mode = model_.getMode();
		ATSecurityPack eqtR = getEquityResearch(model_);
		if (eqtR == null || eqtR.getMarketMap().isEmpty() || ATUserEquityModel.MODE_EQUITY.equals(mode)) {
			return eqtR;
		}
		ATSecurityPack optR = getOptionResearch(model_, eqtR);
		return optR;
	}

	private ATSecurityPack getOptionResearch(ATUserEquityModel model_, ATSecurityPack eqtR_) {
		ArrayList<ATModelFilter> filters = model_.getOptionFilters();
		if (filters == null || filters.isEmpty())
			return null;

		EATPriceReference pxRef = EATPriceReference.px_strk;
		List<String> strategies = Arrays.asList(ATModelFilter.STRATEGY_OPT_CC, ATModelFilter.STRATEGY_OPT_SVCS, ATModelFilter.STRATEGY_OPT_SVPS);
		for (ATModelFilter filter : filters) {
			switch (filter.getId()) {
			case ATModelFilter.STRATEGY_OPT:
				@SuppressWarnings("unchecked")
				List<String> strs = (List<String>) filter.getValue();
				if (strs != null && !strs.isEmpty()) {
					strategies = strs;
				}
				break;
			case ATModelFilter.PX_REFERENCE:
				String ref = (String) filter.getValue();
				if (!StringUtils.isBlank(ref)) {
					pxRef = EATPriceReference.valueOf(ref);
				}
				break;
			}
		}

		// String nowDS6 = ATFormats.DATE_yyMMdd.get().format(new Date());
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		Map<String, ATFundamental> fndMap = eqtR_.getFundamentals();
		Map<String, ATEquityDescription> dscMap = eqtR_.getDescriptions();
		Map<String, ATMarketData> mktMap = eqtR_.getMarketMap();
		Map<String, ATMarketData> filterMktMap = new HashMap<>();
		TreeMap<String, TreeMap<String, TreeSet<String>>> filterCallChain = new TreeMap<>();
		TreeMap<String, TreeMap<String, TreeSet<String>>> filterPutChain = new TreeMap<>();
		TreeMap<String, Map<String, List<ATOpportunity2>>> opportunityMap = new TreeMap<>();
		Set<String> retainedSymbols = new HashSet<>();
		Iterator<String> iSym = mktMap.keySet().iterator();
		while (iSym.hasNext()) {
			int optionCount = 0;
			String symbol = iSym.next();
			ATMarketData mktUl = mktMap.get(symbol);
			Double pxUl = mktUl.getPxMid();
			if (pxUl == null) {
				log.debug(String.format("getOptionResearch: Missing pxUL [%s]", symbol));
				continue;
			}
			ATSecurityPack optionPack = mktSvc.getOptionChain(symbol, null);
			Map<String, ATMarketData> optionMktMap = optionPack.getMarketMap();
			if (optionMktMap == null || optionMktMap.isEmpty()) {
				IATWhereQuery oQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID_UNDERLYING, symbol);
				optionMktMap = mktSvc.getMarketData(oQuery);
				if (optionMktMap == null || optionMktMap.isEmpty())
					continue;
			}
			ATFundamental fnd = fndMap.get(symbol);
			Map<String, List<ATOpportunity2>> symOpportunityMap = new TreeMap<>();
			if (strategies.contains(ATModelFilter.STRATEGY_OPT_CC) || strategies.contains(ATModelFilter.STRATEGY_OPT_SVCS)) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> callChain = optionPack.getCallChain();
				TreeMap<String, TreeSet<String>> symCallChain = callChain != null ? callChain.get(symbol) : null;
				filterOptionAge(filters, symCallChain);
				if (symCallChain != null && !symCallChain.isEmpty()) {
					Set<String> retainedCallIds = new HashSet<>();
					for (String strategy : strategies) {
						switch (strategy) {
						case ATModelFilter.STRATEGY_OPT_CC:
							List<ATOpportunity2> ccOpps = selectCC(filters, pxRef, fnd, mktUl, optionMktMap, symCallChain);
							if (ccOpps != null && !ccOpps.isEmpty()) {
								symOpportunityMap.put(ATOpportunity2.CC, ccOpps);
								Set<String> retained = ccOpps.stream().map(o -> o.getSegments()).flatMap(List::stream).map(s -> s.occId)
										.collect(Collectors.toSet());
								retainedSymbols.add(symbol);
								retainedCallIds.addAll(retained);
							}
							break;
						case ATModelFilter.STRATEGY_OPT_SVCS:
							List<ATOpportunity2> svcsOpps = selectSVCS(filters, pxRef, fnd, mktUl, optionMktMap, symCallChain);
							if (svcsOpps != null && !svcsOpps.isEmpty()) {
								symOpportunityMap.put(ATOpportunity2.VCS, svcsOpps);
								Set<String> retained = svcsOpps.stream().map(o -> o.getSegments()).flatMap(List::stream).map(s -> s.occId)
										.collect(Collectors.toSet());
								retainedSymbols.add(symbol);
								retainedCallIds.addAll(retained);
								optionCount += retained.size();
							}
							break;
						}
					}
					prepareFilterOptions(optionMktMap, filterMktMap, filterCallChain, symbol, symCallChain, retainedCallIds);
				}
			}
			if (strategies.contains(ATModelFilter.STRATEGY_OPT_SVPS)) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> putChain = optionPack.getPutChain();
				TreeMap<String, TreeSet<String>> symPutChain = putChain != null ? putChain.get(symbol) : null;
				filterOptionAge(filters, symPutChain);
				if (symPutChain != null && !symPutChain.isEmpty()) {
					Set<String> retainedPutIds = new HashSet<>();
					for (String strategy : strategies) {
						switch (strategy) {
						case ATModelFilter.STRATEGY_OPT_SVPS:
							List<ATOpportunity2> svcsOpps = selectSVPS(filters, pxRef, fnd, mktUl, optionMktMap, symPutChain);
							if (svcsOpps != null && !svcsOpps.isEmpty()) {
								symOpportunityMap.put(ATOpportunity2.VPS, svcsOpps);
								Set<String> retained = svcsOpps.stream().map(o -> o.getSegments()).flatMap(List::stream).map(s -> s.occId)
										.collect(Collectors.toSet());
								retainedSymbols.add(symbol);
								retainedPutIds.addAll(retained);
								optionCount += retained.size();
							}
							break;
						}
					}
					prepareFilterOptions(optionMktMap, filterMktMap, filterPutChain, symbol, symPutChain, retainedPutIds);
				}
			}

			if (optionCount == 0) {
				iSym.remove();
				if (fndMap != null) {
					fndMap.remove(symbol);
				}
				if (dscMap != null) {
					dscMap.remove(symbol);
				}
			} else {

			}
			if (!symOpportunityMap.isEmpty()) {
				opportunityMap.put(symbol, symOpportunityMap);
			}
		}
		if (filterCallChain.isEmpty() && filterPutChain.isEmpty())
			return null;
		mktMap.putAll(filterMktMap);
		fndMap.keySet().retainAll(retainedSymbols);
		dscMap.keySet().retainAll(retainedSymbols);
		if (!filterCallChain.isEmpty()) {
			eqtR_.setCallChain(filterCallChain);
		}
		if (!filterPutChain.isEmpty()) {
			eqtR_.setPutChain(filterPutChain);
		}
		if (!opportunityMap.isEmpty()) {
			eqtR_.setOpportunities(opportunityMap);
		}
		return eqtR_;
	}

	private void prepareFilterOptions(Map<String, ATMarketData> sourceMktMap_, Map<String, ATMarketData> targetMktMap_,
			TreeMap<String, TreeMap<String, TreeSet<String>>> targetChain_, String symbol_, TreeMap<String, TreeSet<String>> symChain_,
			Set<String> retainedIds_) {
		Iterator<Entry<String, TreeSet<String>>> iExp = symChain_.entrySet().iterator();
		while (iExp.hasNext()) {
			Entry<String, TreeSet<String>> entry = iExp.next();
			Set<String> occIds = entry.getValue();
			occIds.retainAll(retainedIds_);
			if (occIds.isEmpty()) {
				iExp.remove();
			}
		}
		if (!symChain_.isEmpty()) {
			targetChain_.put(symbol_, symChain_);
		}
		for (String occId : retainedIds_) {
			ATMarketData mktO = sourceMktMap_.get(occId);
			if (mktO != null) {
				targetMktMap_.put(occId, mktO);
			}
		}
	}

	private List<ATOpportunity2> selectSVPS(List<ATModelFilter> filters_, EATPriceReference pxRef_, ATFundamental fnd_, ATMarketData mktUl_,
			Map<String, ATMarketData> mktMap_, TreeMap<String, TreeSet<String>> putMap_) {
		List<ATOpportunity2> opportunities_1 = selectVerticalSpread(ATModelFilter.STRATEGY_OPT_SVPS, filters_, pxRef_, fnd_, mktUl_, mktMap_,
				putMap_);
		return opportunities_1;
	}

	private List<ATOpportunity2> selectVerticalSpread(String optStrat_, List<ATModelFilter> filters_, EATPriceReference pxRef_, ATFundamental fnd_,
			ATMarketData mktUl_, Map<String, ATMarketData> mktMap_, TreeMap<String, TreeSet<String>> expMap_) {
		if (mktUl_ == null || mktMap_ == null || mktMap_.isEmpty() || expMap_ == null || expMap_.isEmpty())
			return null;
		List<ATOpportunity2> opportunities = new ArrayList<>();
		double apyMin = 0;
		double apyMax = Double.MAX_VALUE;
		// boolean isApy = false;
		double spreadPxMin = 0;
		double spreadPxMax = Double.MAX_VALUE;
		boolean isSpreadPx = false;
		int stepsMin = 1;
		int stepsMax = 1;
		String yldStrat = ATModelFilter.STRATEGY_YLD_H2L;
		for (ATModelFilter filter : filters_) {
			String filterId = filter.getId();
			Object oVal = filter.getValue();
			Object oMin = filter.getMin();
			Object oMax = filter.getMax();
			switch (filterId) {
			case ATModelFilter.STRATEGY_YLD:
				if (oVal != null) {
					yldStrat = (String) oVal;
				}
				break;
			case ATModelFilter.SPREAD_PX:
				if (oMin != null) {
					spreadPxMin = (Double) oMin;
					isSpreadPx = true;
				}
				if (oMax != null) {
					spreadPxMax = (Double) oMax;
					isSpreadPx = true;
				}
				break;
			case ATModelFilter.SPREAD_STEPS:
				if (oMin != null) {
					stepsMin = Math.max(1, (Integer) oMin);
				}
				if (oMax != null) {
					stepsMax = (Integer) oMax;
				}
				break;
			case ATModelFilter.YIELD_APY:
				if (oMin != null) {
					apyMin = (Double) oMin;
					// isApy = true;
				}
				if (oMax != null) {
					apyMax = (Double) oMax;
					// isApy = true;
				}
				break;
			}
		}

		for (Entry<String, TreeSet<String>> entry : expMap_.entrySet()) {
			String expDS10 = entry.getKey();
			LocalDate ld = LocalDate.parse(expDS10);
			LocalDate now = LocalDate.now();
			int daysExp = (int) ChronoUnit.DAYS.between(now, ld) + 1;
			@SuppressWarnings("unchecked")
			NavigableSet<String> optionIds = (NavigableSet<String>) entry.getValue().clone();
			// optionIds.retainAll(retainedIds_);
			if ((ATModelFilter.STRATEGY_OPT_SVPS.equals(optStrat_) && ATModelFilter.STRATEGY_YLD_H2L.equals(yldStrat))
					|| (ATModelFilter.STRATEGY_OPT_SVCS.equals(optStrat_) && ATModelFilter.STRATEGY_YLD_L2H.equals(yldStrat))) {
				optionIds = optionIds.descendingSet();
			}
			List<ATOpportunity2> expOpportunities = new ArrayList<>();
			stepLoop: for (int steps = stepsMin; steps <= stepsMax; steps++) {
				NavigableSet<String> ids = optionIds;
				idLoop: while (!ids.isEmpty()) {
					String first = ids.first();
					String second = first;
					for (int step = 0; step < steps; step++) {
						if (second == null)
							break;
						second = ids.higher(second);
					}
					ids = ids.tailSet(first, false);
					if (second == null)
						continue idLoop;
					// if (ATModelFilter.STRATEGY_OPT_SVCS.equals(type_)) {
					// String tmp = first;
					// first = second;
					// second = tmp;
					// }
					ATMarketData mkt1 = mktMap_.get(first);
					ATMarketData mkt2 = mktMap_.get(second);
					if (mkt1 == null || mkt2 == null)
						continue idLoop;
					Double px1 = mkt1.getPxMid();
					Double px2 = mkt2.getPxMid();
					Double strike1 = mkt1.getPxStrike();
					Double strike2 = mkt2.getPxStrike();
					Double px = px1 != null && px2 != null ? Precision.round(px1 - px2, 4) : null;

					List<ATMarketData> optList = Arrays.asList(mkt1, mkt2);

					if (!filterOptionPrice(optStrat_, filters_, pxRef_, fnd_, mktUl_, optList, px))
						continue idLoop;

					int factor = ATModelFilter.STRATEGY_OPT_SVCS.equals(optStrat_) ? -1 : 1;
					Double spread = strike1 != null && strike2 != null ? (factor * strike1) - (factor * strike2) : null;
					Double pxRisk = px != null && spread != null ? spread - px : null;
					if (pxRisk == null || pxRisk == 0 || (isSpreadPx && (spread < spreadPxMin || spread > spreadPxMax)))
						continue idLoop;
					double yld = px / pxRisk;
					double apy = 365 * yld / daysExp;
					if (apy < apyMin || apy > apyMax) {
						if (ATModelFilter.STRATEGY_YLD_DEEP.equals(yldStrat)) {
							continue idLoop;
						} else {
							continue stepLoop;
						}
					}

					apy = Precision.round(apy, 2);
					// yieldAbs = Precision.round(yieldAbs, 2);
					yld = Precision.round(yld, 2);
					ATOpportunity2 opp = new ATOpportunity2(optStrat_);
					opp.setDaysExp(daysExp);
					opp.setSymbol(mktUl_.getOccId());
					opp.setYieldAbs(px);
					opp.setYieldApy(apy);
					opp.setYieldPct(yld);
					// if (ccYieldAsg != null) {
					// ccYieldAsg = Precision.round(ccYieldAsg, 2);
					// opp.setYieldAsg(ccYieldAsg);
					// }
					opp.setPx(px);
					// if (pxTheo != null) {
					// pxTheo = Precision.round(pxTheo, 2);
					// opp.setPxTheo(pxTheo);
					// }
					// if (expDivStr != null) {
					// div = Precision.round(div, 2);
					// opp.setDiv(div);
					// opp.setDivExDS10(expDivStr);
					// }
					opp.getSegments().add(new ATOpportunitySegment(first, -1.0, daysExp));
					opp.getSegments().add(new ATOpportunitySegment(second, 1.0, daysExp));
					expOpportunities.add(opp);
					if (!ATModelFilter.STRATEGY_YLD_DEEP.equals(yldStrat))
						break stepLoop;
				}
			}
			if (expOpportunities != null && !expOpportunities.isEmpty()) {
				Optional<ATOpportunity2> maxOppt = expOpportunities.stream().max(new Comparator<ATOpportunity2>() {
					@Override
					public int compare(ATOpportunity2 o1, ATOpportunity2 o2) {
						double yld1 = o1.getYieldPct();
						double yld2 = o2.getYieldPct();
						return Double.compare(yld1, yld2);
					}
				});
				if (maxOppt.isPresent()) {
					opportunities.add(maxOppt.get());
				}
			}
		}
		return opportunities;
	}

	private List<ATOpportunity2> selectSVCS(List<ATModelFilter> filters_, EATPriceReference pxRef_, ATFundamental fnd_, ATMarketData mktUl_,
			Map<String, ATMarketData> optionMktMap_,
			TreeMap<String, TreeSet<String>> callMap_) {
		List<ATOpportunity2> opportunities = selectVerticalSpread(ATModelFilter.STRATEGY_OPT_SVCS, filters_, pxRef_, fnd_, mktUl_, optionMktMap_,
				callMap_);
		return opportunities;
	}

	private boolean filterOptionPrice(String strat_, List<ATModelFilter> filters_, EATPriceReference pxRef_, ATFundamental fnd_, ATMarketData mktUl_,
			List<ATMarketData> mktOpts_, Double pxO_) {
		Double pxUl = mktUl_.getPxMid();
		Double pxBase = ATModelFilter.STRATEGY_OPT_CC.equals(strat_)
				? pxUl
				: mktOpts_.stream().mapToDouble(o_ -> o_.getPxStrike()).max().getAsDouble();
		for (ATModelFilter pxFilter : filters_) {
			Double fMax = pxFilter.getMax();
			Double fMin = pxFilter.getMin();
			Double pxFnd = null;
			Double pxMax = null;
			Double pxMin = null;
			String filterId = pxFilter.getId();
			switch (filterId) {
			case ATModelFilter.HV_60:
			case ATModelFilter.HV_125:
			case ATModelFilter.HV_254:
				Double hv = fnd_ != null ? fnd_.getHv(filterId) : null;
				if (hv == null || pxUl == null)
					return false;
				Double pxHvMax = pxUl * (1 + hv);
				Double pxHvMin = pxUl * (1 - hv);
				pxMax = fMax != null ? pxHvMax * (100 + fMax) / 100 : null;
				pxMin = fMin != null ? pxHvMin * (100 + fMin) / 100 : null;
				break;
			case ATModelFilter.PX:
				pxMax = fMax;
				pxMin = fMin;
				break;
			case ATModelFilter.PX_CHANGE:
				if (pxUl == null)
					return false;
				pxMax = fMax != null ? pxUl * (100 + fMax) / 100 : null;
				pxMin = fMin != null ? pxUl * (100 + fMin) / 100 : null;
				break;
			case ATModelFilter.PX_MAX_60:
			case ATModelFilter.PX_MAX_125:
			case ATModelFilter.PX_MAX_254:
				pxFnd = fnd_ != null ? fnd_.getMax(filterId) : null;
				if (pxFnd == null)
					return false;
				pxMax = fMax != null ? pxFnd * (100 + fMax) / 100 : null;
				pxMin = fMin != null ? pxFnd * (100 + fMin) / 100 : null;
				break;
			case ATModelFilter.PX_MIN_60:
			case ATModelFilter.PX_MIN_125:
			case ATModelFilter.PX_MIN_254:
				pxFnd = fnd_ != null ? fnd_.getMin(filterId) : null;
				if (pxFnd == null)
					return false;
				pxMax = fMax != null ? pxFnd * (100 + fMax) / 100 : null;
				pxMin = fMin != null ? pxFnd * (100 + fMin) / 100 : null;
				break;
			case ATModelFilter.SMA_60:
			case ATModelFilter.SMA_125:
			case ATModelFilter.SMA_254:
				pxFnd = fnd_ != null ? fnd_.getAvg(filterId) : null;
				if (pxFnd == null)
					return false;
				pxMax = fMax != null ? pxFnd * (100 + fMax) / 100 : null;
				pxMin = fMin != null ? pxFnd * (100 + fMin) / 100 : null;
				break;
			}
			if (EATPriceReference.px_strk.equals(pxRef_)) {
				if (mktOpts_ == null || mktOpts_.isEmpty())
					return false;
				for (ATMarketData mktO : mktOpts_) {
					Double strk = mktO.getPxStrike();
					if (strk == null || (pxMax != null && strk > pxMax) || (pxMin != null && strk < pxMin))
						return false;
				}
			} else {
				Double pxEff = pxBase - pxO_;
				if ((pxMax != null && pxEff > pxMax) || (pxMin != null && pxEff < pxMin))
					return false;
			}
		}
		return true;
	}

	private List<ATOpportunity2> selectCC(List<ATModelFilter> filters_, EATPriceReference pxRef_, ATFundamental fnd_, ATMarketData mktUl_,
			Map<String, ATMarketData> optionMktMap_, TreeMap<String, TreeSet<String>> callMap_) {
		if (mktUl_ == null || optionMktMap_ == null || optionMktMap_.isEmpty() || callMap_ == null || callMap_.isEmpty())
			return null;
		List<ATOpportunity2> opportunities = new ArrayList<>();
		for (Entry<String, TreeSet<String>> callEntry : callMap_.entrySet()) {
			String expDS10 = callEntry.getKey();
			LocalDate ld = LocalDate.parse(expDS10);
			LocalDate now = LocalDate.now();
			int daysExp = (int) ChronoUnit.DAYS.between(now, ld) + 1;
			TreeSet<String> calls = callEntry.getValue();
			if (calls.isEmpty())
				continue;
			@SuppressWarnings("unchecked")
			NavigableSet<String> sortedCalls = (NavigableSet<String>) calls.clone();
			idLoop: while (!sortedCalls.isEmpty()) {
				String occIdCall = sortedCalls.pollLast();
				ATMarketData mktCall = optionMktMap_.get(occIdCall);
				Double pxUl = mktUl_.getPxMid();
				Double pxCall = mktCall.getPxMid();
				Double pxCC = pxUl - pxCall;
				Double yieldAbs = mktCall.getPxStrike() - pxCC;
				Double yieldPct = yieldAbs / pxCC;
				Double apy = 365 * yieldPct / daysExp;
				List<ATMarketData> optionList = Arrays.asList(mktCall);
				if (!filterOptionPrice(ATModelFilter.STRATEGY_OPT_CC, filters_, pxRef_, fnd_, mktUl_, optionList, pxCall))
					continue idLoop;
				boolean isYieldFilter = false;
				for (ATModelFilter filter : filters_) {
					Double fMin = filter.getMin();
					Double fMax = filter.getMax();
					switch (filter.getId()) {
					case ATModelFilter.YIELD_APY:
						isYieldFilter = true;
						if ((fMin != null && fMin > apy * 100) || (fMax != null && fMax < apy * 100))
							continue idLoop;
						break;
					}
				}
				if (!isYieldFilter && (apy <= 0))
					continue idLoop;

				apy = Precision.round(apy, 2);
				yieldAbs = Precision.round(yieldAbs, 2);
				yieldPct = Precision.round(yieldPct, 2);
				ATOpportunity2 opp = new ATOpportunity2(ATOpportunity2.CC);
				opp.setDaysExp(daysExp);
				opp.setSymbol(mktUl_.getOccId());
				opp.setYieldAbs(yieldAbs);
				opp.setYieldApy(apy);
				opp.setYieldPct(yieldPct);
				// if (ccYieldAsg != null) {
				// ccYieldAsg = Precision.round(ccYieldAsg, 2);
				// opp.setYieldAsg(ccYieldAsg);
				// }
				opp.setPx(pxCC);
				// if (pxTheo != null) {
				// pxTheo = Precision.round(pxTheo, 2);
				// opp.setPxTheo(pxTheo);
				// }
				// if (expDivStr != null) {
				// div = Precision.round(div, 2);
				// opp.setDiv(div);
				// opp.setDivExDS10(expDivStr);
				// }
				opp.getSegments().add(new ATOpportunitySegment(mktUl_.getOccId(), 100.0, null));
				opp.getSegments().add(new ATOpportunitySegment(occIdCall, -1.0, daysExp));
				opportunities.add(opp);
				break idLoop;
			}
		}
		return opportunities;
	}

	private boolean filterOptionAge(List<ATModelFilter> filters_, Map<String, TreeSet<String>> expOptionIdMap_) {
		if (filters_ == null || filters_.isEmpty() || expOptionIdMap_ == null || expOptionIdMap_.isEmpty())
			return false;

		Optional<ATModelFilter> ageFilterOptional = filters_.stream().filter(f_ -> ATModelFilter.EXP_DAYS.equals(f_.getId())).findAny();
		if (!ageFilterOptional.isPresent())
			return false;
		ATModelFilter ageFilter = ageFilterOptional.get();
		Double min = ageFilter.getMin();
		Double max = ageFilter.getMax();
		final int daysMin = min != null ? min.intValue() : 0;
		final int daysMax = max != null ? max.intValue() : Integer.MAX_VALUE;
		LocalDate now = LocalDate.now();

		Iterator<String> iDs10 = expOptionIdMap_.keySet().iterator();
		while (iDs10.hasNext()) {
			String expDS10 = iDs10.next();
			LocalDate expD = LocalDate.parse(expDS10);
			int days = (int) ChronoUnit.DAYS.between(now, expD);
			if (days < daysMin || days > daysMax) {
				iDs10.remove();
				;
			}
		}
		return true;
	}

	private ATSecurityPack getEquityResearch(ATUserEquityModel model_) {
		// final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;

		String mLabel = model_ != null ? model_.getLabel() : null;
		ArrayList<ATModelFilter> filters = model_ == null ? null : model_.getFilters();
		if (filters == null || filters.isEmpty()) {
			log.info(String.format("Missing filters", mLabel));
			return null;
		}

		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();

		Map<String, ATEquityDescription> dscMap = new TreeMap<>();
		Map<String, ATFundamental> fndMap = new TreeMap<>();
		Map<String, ATMarketData> mktMap = new TreeMap<>();

		Map<String, ATMarketData> mktFilterMap = null;
		IATWhereQuery mktQuery = ATModelTranslator.modelToMarketQuery(model_);
		if (mktQuery != null) {
			mktFilterMap = mktSvc.getMarketData(mktQuery);
		}
		Map<String, ATPersistentEquityFoundation> allEqtMap = fndSvcOld.getFoundationMap();
		// IATWhereQuery fndQuery = ATModelTranslator.modelToFundamentalQuery(model_);
		// Map<String, ATPersistentEquityFoundation> fndFilterMap = null;
		// if (fndQuery != null) {
		Map<String, ATFundamental> allFndMap = new HashMap<>(fndSvc.getFundamentals());
		Set<String> unionSet = new HashSet<>(allFndMap.keySet());
		Set<String> rejectedSet = new HashSet<>();
		for (ATModelFilter filter : filters) {
			String filterId = filter.getId();
			String asset = filter.getAsset();
			if (ATModelFilter.ASSET_OPTION.equals(asset))
				continue;
			String fType = filter.getType();
			boolean isPct = ATModelFilter.TYPE_PERCENTILE.equals(fType);
			if (isPct) {
				Object pctValue = filter.getValue();
				if (pctValue == null || !(pctValue instanceof Double) || ((Double) pctValue) == 0)
					continue;
				Double pctLimit = (Double) pctValue;
				switch (filterId) {
				case ATModelFilter.PE_GROWTH:
					Function<ATFundamental, Double> fValuePEG = (f) -> f.getPEGrowthRatio();
					limitValue(fValuePEG, allFndMap, pctLimit / 100.0, unionSet, filterId);
					break;
				case ATModelFilter.REV_GROWTH_3Y:
					Function<ATFundamental, Double> fValueRG3Y = (f) -> f.getRevenueGrowthAvg3Y();
					limitValue(fValueRG3Y, allFndMap, pctLimit / 100.0, unionSet, filterId);
					break;
				case ATModelFilter.REV_MLT:
					Function<ATFundamental, Double> fValueRM = (f) -> f.getRevenueMultiple();
					limitValue(fValueRM, allFndMap, pctLimit / 100.0, unionSet, filterId);
					break;
				case ATModelFilter.TRAILING_PE:
					Function<ATFundamental, Double> fValueTPE = (f) -> f.getTrailingPE();
					limitValue(fValueTPE, allFndMap, pctLimit / 100.0, unionSet, filterId);
					break;
				default:
					log.error(String.format("Unsupported pct filter [%s]", filterId));
					break;
				}
			} else {
				final Double fMin = filter.getMin();
				final Double fMax = filter.getMax();
				final Object fValue = filter.getValue();
				// Collection<String> wlSyms = null;
				symbolLoop: for (String occId : allFndMap.keySet()) {
					ATMarketData mkt = mktFilterMap != null ? mktFilterMap.get(occId) : mktSvc.getMarketData(occId);
					if (mktFilterMap != null && !mktFilterMap.containsKey(occId) || rejectedSet.contains(occId) || mkt == null) {
						continue;
					}
					ATPersistentEquityFoundation eqf = allEqtMap.get(occId);
					ATEquityDescription description = eqf != null ? new ATEquityDescription(eqf) : null;
					ATFundamental fnd = allFndMap.get(occId);
					Function<ATFundamental, Double> fndFnc = null;
					switch (filterId) {
					case ATModelFilter.DIVIDEND_YIELD:
						Double divYld = null;
						Double div = fnd.getDividend();
						Double px = mkt.getPxMid();
						if (div != null && px != null && px != 0) {
							Integer divFrq = fnd.getDividendFrequency();
							divYld = ((divFrq != null ? divFrq : 4) * div) / px;
						}
						if (!passMinMax(divYld, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.HV:
						Map<String, Double> hvMap = fnd.getHvMap();
						Double hv = hvMap != null ? hvMap.get(hvMode) : null;
						if (!passMinMax(hv, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.IV:
						Double iv = fnd.getIV();
						if (!passMinMax(iv, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.INDUSTRY:
						if (description != null && !fValue.equals(description.getIndustry())) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.MCAP:
						Double mcap = fnd.getMarketCap();
						if (!passMinMax(mcap, fMin, fMax, 1 / 1000000000d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.PE_GROWTH:
						Double peg = fnd.getPEGrowthRatio();
						if (!passMinMax(peg, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.REV_GROWTH_3Y:
						Double rev3y = fnd.getRevenueGrowthAvg3Y();
						if (!passMinMax(rev3y, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.REV_MLT:
						Double rm = fnd.getRevenueMultiple();
						if (!passMinMax(rm, fMin, fMax, 1d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.TRAILING_PE:
						Double tpe = fnd.getTrailingPE();
						if (!passMinMax(tpe, fMin, fMax, 100d)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.SECTOR:
						if (description != null && !fValue.equals(description.getSector())) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.SYMBOLS:
						@SuppressWarnings("unchecked")
						Collection<String> syms = (Set<String>) fValue;
						if (!syms.contains(occId)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					case ATModelFilter.WATCHLIST:
						@SuppressWarnings("unchecked")
						Collection<String> wlSyms = (Collection<String>) filter.get("wlSyms");
						if (wlSyms == null) {
							IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
							List<ATWatchlist> wls = usrSvc.getWatchlists(model_.getUserId());
							if (wls == null || wls.isEmpty())
								break symbolLoop;
							String wlId = (String) fValue;
							wls.removeIf(wl_ -> !wlId.equals(wl_.getId()));
							if (wls.isEmpty())
								break symbolLoop;
							wlSyms = wls.get(0).getSymbols();
							filter.put("wlSyms", wlSyms);
						}
						if (!wlSyms.contains(occId)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					// NOTE: Statistical filters
					case ATModelFilter.PX_MAX_254:
						if (fndFnc == null)
							fndFnc = (fnd_) -> fnd_.getMax(filterId);
					case ATModelFilter.SMA_60:
					case ATModelFilter.SMA_125:
					case ATModelFilter.SMA_254:
						if (fndFnc == null)
							fndFnc = (fnd_) -> fnd_.getAvg(filterId);
						Double min = fMin != null ? 100.0 + fMin : null;
						Double max = fMax != null ? 100.0 + fMax : null;
						if (!passStatFilter(fndFnc, mkt, fnd, min, max)) {
							rejectedSet.add(occId);
							continue;
						}
						break;
					// NOTE: Market properties (handled in mkt query above)
					case ATModelFilter.PX:
					case ATModelFilter.PX_CHANGE:
						break;
					default:
						log.error(String.format("Unsupported abs filter [%s]", filterId));
						break;
					}

					mktMap.put(occId, mkt);
					fndMap.put(occId, fnd);
					dscMap.put(occId, description);
				}
				int sizeStart = allFndMap.size();
				allFndMap.keySet().removeAll(rejectedSet);
				log.debug(String.format("getEquities abs Filter[%s]: %d -> %d", filterId, sizeStart, allFndMap.size()));

			}
		}
		Set<String> remaining = unionSet;// allFndMap.keySet();
		mktMap.keySet().removeAll(rejectedSet);
		mktMap.keySet().retainAll(remaining);
		fndMap.keySet().removeAll(rejectedSet);
		fndMap.keySet().retainAll(remaining);
		dscMap.keySet().removeAll(rejectedSet);
		dscMap.keySet().retainAll(remaining);

		ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);
		return pack;
	}

	private boolean passStatFilter(Function<ATFundamental, Double> fnc_, ATMarketData mkt_, ATFundamental fnd_,
			Double minFactor_, Double maxFactor_) {
		if (mkt_ == null || fnd_ == null)
			return false;
		Double pxMid = mkt_.getPxMid();
		Double pxRef = fnc_.apply(fnd_);
		Double pxAbsMin = minFactor_ != null && pxRef != null ? minFactor_ * pxRef : null;
		Double pxAbsMax = maxFactor_ != null && pxRef != null ? maxFactor_ * pxRef : null;
		boolean result = passMinMax(pxMid, pxAbsMin, pxAbsMax, 100d);
		return result;
	}

	private boolean passMinMax(Double value_, Double min_, Double max_, Double factor_) {
		Double value = value_ == null ? null : (factor_ != null ? value_ * factor_ : value_);
		if (value == null || (min_ != null && value < min_) || (max_ != null && value > max_)) {
			return false;
		} else {
			return true;
		}
	}

	private Map<String, ATFundamental> limitValue(Function<ATFundamental, Double> fnc_, Map<String, ATFundamental> map_, Double limit_,
			Set<String> unionSet_, String filterId_) {
		Integer sizeStartAll = map_ != null ? map_.size() : null;
		Integer sizeStartUnion = unionSet_.size();
		if (fnc_ == null || map_ == null || limit_ == null) {
			log.debug(String.format("limitValue pct Filter[%s]: %d / %d", filterId_, sizeStartAll, sizeStartUnion));
			return map_;
		}

		final int sign = (int) Math.signum(limit_);
		final int size = (int) Math.ceil(Math.abs(limit_) * map_.size());

		Comparator<Entry<String, ATFundamental>> cmp = new Comparator<Entry<String, ATFundamental>>() {

			@Override
			public int compare(Entry<String, ATFundamental> a_, Entry<String, ATFundamental> b_) {
				Double a = fnc_.apply(a_.getValue());
				Double b = fnc_.apply(b_.getValue());
				int compare = sign * Double.compare(a, b);
				return compare;
			}
		};

		Map<String, ATFundamental> notNullMap = map_.entrySet().stream().filter(e -> fnc_.apply(e.getValue()) != null)
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
		int notNullCount = notNullMap.size();
		Map<String, ATFundamental> result = notNullMap.entrySet().stream()
				// .filter(e -> fnc_.apply(e.getValue()) != null)
				.sorted(cmp).limit(size)
				.collect(Collectors.toMap(map -> map.getKey(), map -> map.getValue()));
		if (result != null) {
			unionSet_.retainAll(result.keySet());
		}
		log.debug(String.format("limitValue pct Filter[%s]: %d(%d) -> %d / %d -> %d", filterId_, sizeStartAll, notNullCount, result.size(),
				sizeStartUnion, unionSet_.size()));
		return result;
	}

}
