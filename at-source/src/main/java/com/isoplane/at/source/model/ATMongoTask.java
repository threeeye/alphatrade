package com.isoplane.at.source.model;

import java.util.Map;

import org.bson.Document;

import com.google.gson.Gson;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoTask extends Document implements IATTask, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	public ATMongoTask() {
	}

	public ATMongoTask(IATTask tmpl_) {
		setAtId(tmpl_.getAtId());
		setCode(tmpl_.getCode());
		setData(tmpl_.getData());
		setSource(tmpl_.getSource());
		setStatus(tmpl_.getStatus());
		setLastAccess(tmpl_.getLastAccess());
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public void setAtId(String value_) {
		IATLookupMap.setAtId(this, value_);
	}

	@Override
	public String getAtId() {
		return IATLookupMap.getAtId(this);
	}

	@Override
	public int getStatus() {
		return IATTask.getStatus(this);
	}

	@Override
	public void setStatus(int value_) {
		IATTask.setStatus(this, value_);
	}

	@Override
	public String getCode() {
		return IATTask.getCode(this);
	}

	@Override
	public void setCode(String value_) {
		IATTask.setCode(this, value_);
	}

	@Override
	public String getData() {
		return IATTask.getData(this);
	}

	@Override
	public void setData(String value_) {
		IATTask.setData(this, value_);
	}

	@Override
	public String getSource() {
		return IATTask.getSource(this);
	}

	@Override
	public void setSource(String value_) {
		IATTask.setSource(this, value_);
	}

	@Override
	public String getLastAccess() {
		return IATTask.getLastAccess(this);
	}

	@Override
	public void setLastAccess(String value_) {
		IATTask.setLastAccess(this, value_);
	}

	@Override
	public int hashCode() {
		return IATTask.hashCode(this);
	}

	@Override
	public boolean equals(Object other_) {
		return IATTask.equals(this, other_);
	}

	@Override
	public String serialize(Gson gson_) {
		return IATTask.serializeTask(gson_, this);
	}

}
