package com.isoplane.at.source.model.enhanced;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;
import java.util.Spliterator;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ATTreeSet<T> extends TreeSet<T> {

	private static final long serialVersionUID = 1L;
	private boolean _disableWarnings = false;
	private Set<IATChangeListener> _listeners = new HashSet<>();

	public void disableWarnings(boolean flag_) {
		_disableWarnings = flag_;
	}

	public void changeListener(IATChangeListener listener_, boolean add_) {
		if (listener_ == null)
			return;
		if (add_)
			_listeners.add(listener_);
		else
			_listeners.remove(listener_);
	}

	@Override
	public Stream<T> parallelStream() {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.parallelStream(): No events on change!");
		return super.parallelStream();
	}

	@Override
	public boolean removeIf(Predicate<? super T> arg0) {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.removeIf(): No events on change!");
		return super.removeIf(arg0);
	}

	@Override
	public Stream<T> stream() {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.stream(): No events on change!");
		return super.stream();
	}

	@Override
	public boolean add(T arg0) {
		boolean result = super.add(arg0);
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
		return result;
	}

	@Override
	public boolean addAll(Collection<? extends T> arg0) {
		boolean result = super.addAll(arg0);
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
		return result;
	}

	@Override
	public void clear() {
		super.clear();
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
	}

	@Override
	public Iterator<T> descendingIterator() {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.descendingIterator(): No events on change!");
		return super.descendingIterator();
	}

	@Override
	public NavigableSet<T> descendingSet() {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.descendingSet(): No events on change!");
		return super.descendingSet();
	}

	@Override
	public boolean remove(Object arg0) {
		boolean result = super.remove(arg0);
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
		return result;
	}

	@Override
	public Spliterator<T> spliterator() {
		if (!_disableWarnings)
			System.out.println("ATTreeSet.spliterator(): No events on change!");
		return super.spliterator();
	}

	@Override
	public boolean removeAll(Collection<?> arg0) {
		boolean result = super.removeAll(arg0);
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
		return result;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		boolean result = super.retainAll(c);
		for (IATChangeListener l : _listeners) {
			try {
				l.changed();
			} catch (Throwable t) {
			}
		}
		return result;
	}

}
