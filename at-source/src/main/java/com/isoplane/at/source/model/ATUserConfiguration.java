package com.isoplane.at.source.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class ATUserConfiguration implements IATUserConfiguration {

	static final Logger log = LoggerFactory.getLogger(ATUserConfiguration.class);

	private double _baseSafetyMargin;
	private double _minApy;
	private double _minPx;
	private int _minOptionDays;
	private int _maxOptionDays;
	private boolean _isNotifications;
	private String[] extraSymbols;
	private String[] ideaTriggers;
	private String[] blacklistSymbols;
	private String[] alertTriggers;
	private String[] spreadSymbols;
	private Set<String> _optionFlags;

	@Override
	public double getBaseSafetyMargin() {
		return _baseSafetyMargin;
	}

	public void setBaseSafetyMargin(double baseSafetyMargin_) {
		this._baseSafetyMargin = baseSafetyMargin_;
	}

	@Override
	public double getMinApy() {
		return _minApy;
	}

	public void setMinApy(double minApy_) {
		this._minApy = minApy_;
	}

	@Override
	public double getMinPx() {
		return _minPx;
	}

	public void setMinPx(double minPx_) {
		this._minPx = minPx_;
	}

	@Override
	public int getMinOptionDays() {
		return _minOptionDays;
	}

	public void setMinOptionDays(int minOptionDays_) {
		this._minOptionDays = minOptionDays_;
	}

	@Override
	public int getMaxOptionDays() {
		return _maxOptionDays;
	}

	public void setMaxOptionDays(int maxOptionDays_) {
		this._maxOptionDays = maxOptionDays_;
	}

	@Override
	public boolean isNotifications() {
		return _isNotifications;
	}

	public void setNotifications(boolean isNotifications_) {
		this._isNotifications = isNotifications_;
	}

	@Override
	public String[] getExtraSymbols() {
		return extraSymbols;
	}

	public void setExtraSymbols(String[] extraSymbols) {
		this.extraSymbols = extraSymbols;
	}

	@Override
	public String[] getIdeaTriggers() {
		return ideaTriggers;
	}

	public void setIdeaTriggers(String[] fixedOpportunities) {
		this.ideaTriggers = fixedOpportunities;
	}

	@Override
	public String[] getAlertTriggers() {
		return alertTriggers;
	}

	public void setAlertTriggers(String[] priceTriggers) {
		this.alertTriggers = priceTriggers;
	}

	@Override
	public String[] getBlacklistSymbols() {
		return blacklistSymbols;
	}

	public void setBlacklistSymbols(String[] blacklistSymbols_) {
		this.blacklistSymbols = blacklistSymbols_;
	}

	@Override
	public Collection<String> getOptionFlagList() {
		return _optionFlags;
	}

	@Override
	public String[] getSpreadSymbols() {
		return spreadSymbols;
	}

	public void setSpreadSymbols(String[] spreadSymbols) {
		this.spreadSymbols = spreadSymbols;
	}

	public void setOptionFlagList(Collection<String> optionFlags_) {
		if (optionFlags_ != null) {
			this._optionFlags = new HashSet<>(optionFlags_);
		}
	}

	public boolean update(IATUserConfiguration config_, String key_) {
		boolean isChanged = false;
		if (getMaxOptionDays() != config_.getMaxOptionDays()) {
			setMaxOptionDays(config_.getMaxOptionDays());
			log.info(String.format("-> [%s] Max Option Days [%d]", key_, config_.getMaxOptionDays()));
			isChanged = true;
		}
		if (getMinOptionDays() != config_.getMinOptionDays()) {
			setMinOptionDays(config_.getMinOptionDays());
			log.info(String.format("-> [%s] Min Option Days [%d]", key_, config_.getMinOptionDays()));
			isChanged = true;
		}
		if (getBaseSafetyMargin() != config_.getBaseSafetyMargin()) {
			setBaseSafetyMargin(config_.getBaseSafetyMargin());
			log.info(String.format("-> [%s] Safety Margin [%.2f]", key_, config_.getBaseSafetyMargin()));
			isChanged = true;
		}
		if (getMinApy() != config_.getMinApy()) {
			setMinApy(config_.getMinApy());
			log.info(String.format("-> [%s] Minimum APY [%.2f]", key_, config_.getMinApy()));
			isChanged = true;
		}
		if (getMinPx() != config_.getMinPx()) {
			setMinPx(config_.getMinPx());
			log.info(String.format("-> [%s] Minimum Px [%.2f]", key_, config_.getMinPx()));
			isChanged = true;
		}
		if (isNotifications() != config_.isNotifications()) {
			setNotifications(config_.isNotifications());
			log.info(String.format("-> [%s] Notifications [%b]", key_, config_.isNotifications()));
			isChanged = true;
		}
		if (!Arrays.equals(getExtraSymbols(), config_.getExtraSymbols())) {
			setExtraSymbols(config_.getExtraSymbols());
			log.info(String.format("-> [%s] Extra symbols [%s]", key_, String.join(", ", config_.getExtraSymbols())));
			isChanged = true;
		}
		if (getOptionFlagList() == null) {
			if (config_.getOptionFlagList() != null) {
				setOptionFlagList(config_.getOptionFlagList());
				log.info(String.format("-> [%s] Option flags [%s]", key_, String.join(", ", config_.getOptionFlagList())));
				isChanged = true;
			}
		} else {
			if (config_.getOptionFlagList() == null) {
				getOptionFlagList().clear();
				log.info(String.format("-> [%s] Option flags [%s]", key_, String.join(", ", config_.getOptionFlagList())));
				isChanged = true;
			} else if (!CollectionUtils.containsAll(config_.getOptionFlagList(), getOptionFlagList())
					|| !CollectionUtils.containsAll(getOptionFlagList(), config_.getOptionFlagList())) {
				setOptionFlagList(config_.getOptionFlagList());
				log.info(String.format("-> [%s] Option flags [%s]", key_, String.join(", ", config_.getOptionFlagList())));
				isChanged = true;
			}
		}
		if (!Arrays.equals(getIdeaTriggers(), config_.getIdeaTriggers())) {
			setIdeaTriggers(config_.getIdeaTriggers());
			log.info(String.format("-> [%s] Idea triggers [%s]", key_, String.join(", ", config_.getIdeaTriggers())));
			isChanged = true;
			// result_digest.isIdeaTriggers = true;
		}
		if (!Arrays.equals(getBlacklistSymbols(), config_.getBlacklistSymbols())) {
			setBlacklistSymbols(config_.getBlacklistSymbols());
			log.info(String.format("-> [%s] Blacklist [%s]", key_, String.join(", ", config_.getBlacklistSymbols())));
			isChanged = true;
		}
		if (!Arrays.equals(getAlertTriggers(), config_.getAlertTriggers())) {
			setAlertTriggers(config_.getAlertTriggers());
			log.info(String.format("-> [%s] Alert triggers [%s]", key_, String.join(", ", config_.getAlertTriggers())));
			isChanged = true;
			// result_digest.isAlertTriggers = true;
		}
		return isChanged;

	}

}
