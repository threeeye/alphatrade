package com.isoplane.at.source.model;

import java.util.Map;

import com.isoplane.at.commons.model.ATBaseNotification;

public class ATStrikeNotification extends ATBaseNotification {

	static public final String META_STRIKE = "strike";

	
	private String symbol;
	private Double stockPrice;
	private Double margin;
	// private Date exDate;

	@Override
	public String getNotificationKey() {
		String key = String.format("strike %s%s", getSymbol(), getMeta(META_STRIKE));
		return key;
	}
 
	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATStrikeNotification oldStrike = oldB != null && oldB instanceof ATStrikeNotification
				? (ATStrikeNotification) oldB
				: null;
		String arrowStr = "✧"; // ▷◇⋄
		if (oldStrike != null) {
			double change = (oldStrike.getMargin() - this.getMargin()) / oldStrike.getMargin();
			if (Math.abs(change) > 0.1) {
				arrowStr = oldStrike.getStockPrice() < this.getStockPrice() ? "➚" : "➘"; // △▽◥◢
			} else {
				return null;
			}
		}

		String title = String.format("%1s %s  %.2f", this.getLabel().replaceAll(" +", " "), arrowStr, this.getStockPrice()); // ⚡
		String body = String.format("⚡  margin %+5.2f%%", this.getMargin());
		// String body = String.format("⚡ margin %+5.2f%% \n· alphatra.de · %tT", this.getMargin(), new Date());
		String icon = margin < 1 ? String.format("/globe_star_flash_red.ico")
				: String.format("/globe_star_flash_yellow.ico");

		return new ATNotificationContent(title, body, icon);
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	public Double getMargin() {
		return margin;
	}

	public void setMargin(Double margin) {
		this.margin = margin;
	}

	@Override
	public String toString() {
		String str = String.format("Strike{%s %.2f (%.2f)}", getSymbol(), getStockPrice(), getMargin());
		return str;
	}

	@Override
	public String getNotificationTag() {
		return String.format("strike_%s", getSymbol());
	}

	@Override
	public String getType() {
		return "strike";
	}

}
