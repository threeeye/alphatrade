package com.isoplane.at.source.controller.process;

public interface IATProcessCallback<T> {

	void process(T param_);
}
