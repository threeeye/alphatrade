package com.isoplane.at.source.controller;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.store.ATPersistentSymbols;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATPersistentLookupStore;

public class ATSymbolsProvider implements IATSymbolsProvider {

	static final Logger log = LoggerFactory.getLogger(ATSymbolsProvider.class);

	private IATConfiguration _config;
	private IATPersistentLookupStore _dao;
	// private Map<String, ATPersistentEquityFoundation> _fMap;
	private long _lastRunTime;
	private ATPersistentSymbols _symbols;

	public ATSymbolsProvider(IATConfiguration config_, IATPersistentLookupStore dao_) {
		_config = config_;
		_dao = dao_;
	}

	@Override
	public void initService() {
		init();
	}

	private void init() {
		updateSymbols();
		log.info(String.format("Initialized symbols"));
	}

	@Override
	public ATPersistentSymbols getSymbols() {
		checkUpdate();
		return _symbols;
	}

	@Override
	public Set<String> getBlacklist() {
		ATPersistentSymbols symbols = getSymbols();
		Set<String> result = symbols != null ? symbols.getBlacklist() : new HashSet<>();
		return result;
	}

	private void updateSymbols() {
		try {
			String[] symbolsTableId = _config.getString("symbolsTableId").split("/");
			IATLookupMap map = _dao.get(symbolsTableId[0], symbolsTableId[1]);
			ATPersistentSymbols symbols = new ATPersistentSymbols(map);
			_symbols = symbols;
		} catch (Exception ex) {
			log.error("Error updating symbols", ex);
		}
	}

	synchronized private void checkUpdate() {
		long symbolsPeriod = _config.getLong("symbolsPeriod", 30000);
		if (_symbols != null && (System.currentTimeMillis() - symbolsPeriod < _lastRunTime))
			return;
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				updateSymbols();
			}
		});
		t.start();
	}

}
