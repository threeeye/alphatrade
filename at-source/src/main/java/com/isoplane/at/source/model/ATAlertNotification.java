package com.isoplane.at.source.model;

import java.util.Map;

import com.isoplane.at.commons.model.ATBaseNotification;

// Note: Stupid name, I know...
public class ATAlertNotification extends ATBaseNotification {

//	private String symbol;
	private Double price;
	private String criteria;
	private String alertType;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

//	public String getSymbol() {
//		return symbol;
//	}

//	public void setSymbol(String symbol) {
//		super.setSymbol(symbol);
//	//	this.symbol = symbol;
//		this.setMeta("symbol", symbol);
//	}

	@Override
	public String toString() {
		String str = String.format("%s (%.2f)", getLabel(), getPrice());
		return str;
	}

	@Override
	public String getNotificationKey() {
		String key = String.format("alert %s", getLabel());
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		if (oldB != null && (System.currentTimeMillis() - oldB._timestamp < timeout_) && oldB.getLabel().equals(this.getLabel())) {
			return null;
		}

		super._timestamp = System.currentTimeMillis();
		String title = "date".equals(this.getAlertType()) ? String.format("%s (%.2f) %s", this.getSymbol(), this.getPrice(), this.getCriteria()) 
				: String.format("%s %.2f %s", this.getSymbol(), this.getPrice(), this.getCriteria());
		String body = String.format("💥  %s", this.getLabel().replaceAll(" +", " "));
		// String body = String.format("✨ %s \n· alphatra.de · %tT", this.getLabel().replaceAll(" +", " "), new Date());
		String iconStar = this.isActive() ? "_star" : "";
		String icon = String.format("/globe_price%s.ico", iconStar);

		return new ATNotificationContent(title, body, icon);
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	@Override
	public String getNotificationTag() {
		return String.format("alert_%s_%s", getSymbol(), getCriteria());//.hashCode() + "";
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	@Override
	public String getType() {
		return "alert";
	}
}
