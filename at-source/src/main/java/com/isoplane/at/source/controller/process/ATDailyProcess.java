package com.isoplane.at.source.controller.process;

public class ATDailyProcess extends ATProcess {

	@Override
	protected int getPriority() {
		return 200;
	}

	@Override
	public boolean equals(Object obj_) {
		return (obj_ instanceof ATDailyProcess);
	}

	@Override
	public int hashCode() {
		return this.getClass().hashCode();
	}

	@Override
	public String toString() {
		String str = String.format("%s", this.getClass().getSimpleName());
		return str;
	}

	@Override
	protected int compareToInstance(ATProcess process) {
		return 0;
	}

}
