package com.isoplane.at.source.model;

public class TdnSecurityResponse {

	public String avgMode;
	public String hvMode;
	public TdnSecurity security;

	public TdnOHLCV[] history;
	public Double[] avg;
	public Double[] hv;
}
