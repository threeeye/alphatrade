package com.isoplane.at.source.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.service.IATPortfolioService;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;

public class ATKafkaUserSessionController implements IATProtoUserSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUserSessionController.class);

	static private ATKafkaUserSessionController _instance;

	private IATPortfolioService _prtSvc;

	private ATKafkaUserSessionProducer _kafkaUserSessionProducer;

	private Map<String, Set<String>> _userPortfolioMap;
	private Map<String, Set<String>> _userTemporarySymbolMap;

	static public ATKafkaUserSessionController init(IATPortfolioService prtSvc_) {
		ATKafkaUserSessionController instance = instance();
		instance._prtSvc = prtSvc_;
		return instance;
	}

	static public ATKafkaUserSessionController instance() {
		if (_instance == null) {
			_instance = new ATKafkaUserSessionController();
			_instance.init();
		}
		return _instance;
	}

	private ATKafkaUserSessionController() {
	}

	private void init() {
		_userPortfolioMap = new HashMap<>();
		_userTemporarySymbolMap = new HashMap<>();
		// _prtSvc = ATServiceRegistry.getPortfolioService();
		_kafkaUserSessionProducer = ATKafkaRegistry.get(ATKafkaUserSessionProducer.class);
		ATKafkaUserSessionConsumer kafkaUserSessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		kafkaUserSessionConsumer.subscribe(this);
	}

	public void processDynamicSymbols(String userId_, String type_, boolean isReplace_, String... symbols_) {
		if (StringUtils.isBlank(userId_) || symbols_ == null)
			return;
		List<String> symbols = Arrays.asList(symbols_);
		Set<String> userSymbols = isReplace_ ? null : this._userTemporarySymbolMap.get(userId_);
		if (userSymbols == null) {
			userSymbols = new TreeSet<>();
			this._userTemporarySymbolMap.put(userId_, userSymbols);
		} else if (userSymbols.containsAll(symbols)) {
			log.debug(String.format("addDynamicSymbols No change"));
			return;
		}
		Configuration config = ATConfigUtil.config();
		String topic = config.getString("kafka.user_session.topic.positions");

		userSymbols.addAll(symbols);
		@SuppressWarnings("unchecked")
		Map<String, String> sessionData = (Map<String, String>) (Object) ArrayUtils.toMap(new String[][] {
				{ ATSession.DATA_SYMBOLS_TYPE, type_ },
				{ ATSession.DATA_SYMBOLS_TEMP, String.join(",", userSymbols) }
		});
		// Map<String, String> sessionData = Collections.singletonMap(ATSession.DATA_SYMBOLS_TEMP, String.join(",", userSymbols));

		ATSession session = new ATSession();
		session.setUserId(userId_);
		session.setToken(IATSession.TOKEN_ALL);
		session.setData(sessionData);
		this._kafkaUserSessionProducer.send(topic, EATChangeOperation.UPDATE, session);
	}

	@Override
	public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
		Configuration config = ATConfigUtil.config();
		String topic = config.getString("kafka.user_session.topic.positions");
		String userId = session_.getUserId();
		Map<String, String> sessionData = session_.getData();
		switch (action_) {
		case ADD:
		case CREATE:
			Set<String> userSymbols = loadSessionPortfolio(userId);
			if (userSymbols != null) {
				if (sessionData == null) {
					sessionData = new HashMap<>();
					session_.setData(sessionData);
				}
				this._userPortfolioMap.put(userId, userSymbols);
				sessionData.put(ATSession.DATA_SYMBOLS_PORT, String.join(",", userSymbols));
				this._kafkaUserSessionProducer.send(topic, EATChangeOperation.CREATE, session_);
			}
			break;
		case UPDATE:
			this._kafkaUserSessionProducer.send(topic, action_, session_);
			break;
		case DELETE:
			// if (sessionData != null) {
			// sessionData.remove(ATSession.DATA_SYMBOLS_PORT);
			// sessionData.remove(ATSession.DATA_SYMBOLS_TEMP);
			// }
			session_.setData(null);
			this._userPortfolioMap.remove(userId);
			this._userTemporarySymbolMap.remove(userId);
			this._kafkaUserSessionProducer.send(topic, EATChangeOperation.DELETE, session_);
			break;
		default:
			log.warn(String.format("notifyUserSession Unsupported action [%s]", action_));
			break;
		}
	}

	private Set<String> loadSessionPortfolio(String userId_) {
		if (this._prtSvc == null) {
			log.warn(String.format("Portfolo service not initialized"));
			return null;
		}
		Set<String> occIds = _prtSvc.getActiveOccIds(userId_, true);
		if (occIds == null || occIds.isEmpty())
			return null;
		Set<String> symbols = occIds.stream()
				.map(o -> o.length() > 10 ? o.substring(0, 6).trim() : o).collect(Collectors.toCollection(TreeSet::new));
		Set<String> userPortfolio = _userPortfolioMap.get(userId_);
		if (userPortfolio == null) {
			userPortfolio = new HashSet<>();
			_userPortfolioMap.put(userId_, userPortfolio);
		}
		userPortfolio.addAll(symbols);
		return occIds;
	}

}
