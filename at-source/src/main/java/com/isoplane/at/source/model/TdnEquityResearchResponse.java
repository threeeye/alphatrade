package com.isoplane.at.source.model;

import com.isoplane.at.commons.model.ATSecurityResearchConfig;
import com.isoplane.at.commons.model.ATStrategy;

public class TdnEquityResearchResponse {

	public String type;
//	public String avgMode;
//	public String hvMode;
	public ATSecurityResearchConfig config;
	public TdnSecurity[] equities;
	public ATStrategy[] strategies;

	static public TdnEquityResearchResponse nullResponse() {
		TdnEquityResearchResponse response = new TdnEquityResearchResponse();
	//	response.equities = new TdnSecurity[0];
		return response;
	}

}