package com.isoplane.at.source.deprecated;

import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATEarning;

public interface IATCorporateEventProvider extends IATService {

	ATEarning getNextEarnings(String sym);

	IATDividend getNextDividend(String sym);
}
