package com.isoplane.at.source.adapter;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATStock;

public interface IATOptionsAdapter {

	/**
	 * Gets requested options from data provider
	 * 
	 * @param securities
	 *            Optionable securities to query
	 * @return Map of option collections keyed by security
	 */
	Map<String, TreeSet<ATOptionSecurity>> getOptions(Collection<ATStock> securities);

	/**
	 * Gets requested options from data provider
	 * 
	 * @param security
	 *            Optionable security to query
	 */
	TreeSet<ATOptionSecurity> getOptions(ATStock security);
}
