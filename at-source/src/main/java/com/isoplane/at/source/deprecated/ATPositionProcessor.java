package com.isoplane.at.source.deprecated;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.model.ATExposure;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentPosition;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.mongodb.IATPersistentLookupStore;
import com.isoplane.at.source.controller.ATRequestContext;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;
import com.isoplane.at.source.controller.IATEquityFoundationProvider;
import com.isoplane.at.source.controller.IATSecuritiesProvider;
import com.isoplane.at.source.model.ATUser;

import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPositionProcessor implements IATPositionProcessor {

	static final Logger log = LoggerFactory.getLogger(ATPositionProcessor.class);

	private IATConfiguration _atConfig;
	private IATPersistentLookupStore _dao;
	private long _throughputCount;
	private long _throughputTime;
	private IATSecuritiesProvider _securitiesProvider;
	private IATEquityFoundationProvider _foundationProvider;
	// private ATQuantLib _quantLib;

	private Map<String /* symbol */, TreeSet<ATPersistentPosition>> _optionPositionMap;
	private Set<ATPersistentPosition> _equityPositions;

	// private long _lastRunTime;

	@Deprecated
	public ATPositionProcessor(IATConfiguration atConfig_, IATPersistentLookupStore dao_) {
		_atConfig = atConfig_;
		_dao = dao_;
		_optionPositionMap = new TreeMap<>();
		_equityPositions = new TreeSet<>();
		// _quantLib = new ATQuantLib(_config);
	}

	@Override
	public void initService() {
		_securitiesProvider = ATSourceServiceRegistry.getSecuritiesProvider();
		if (_securitiesProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATSecuritiesProvider.class.getSimpleName()));
		}
		_foundationProvider = ATSourceServiceRegistry.getEquityFoundationProvider();
		if (_foundationProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATEquityFoundationProvider.class.getSimpleName()));
		}
		init();
	}

	private void init() {
		_equityPositions.clear();
		_optionPositionMap.clear();
		String positionTableId = _atConfig.getString("positionTableId");
		// ATPersistenceRegistry.query().eq(IATAssetConstants.OWNER_ID, value);
		List<ATPersistentPosition> positions = _dao.query(positionTableId, ATPersistenceRegistry.query().empty(), ATPersistentPosition.class);
		for (ATPersistentPosition pos : positions) {
			if (!pos.isOption()) {
				_equityPositions.add(pos);
			} else {
				String symbol = pos.getUnderlyingOccId();
				ATPersistentSecurity sec = _securitiesProvider.get(pos.getOccId());
				if (sec == null) {
					log.error(String.format("Missing security for position [%s]", pos.getOccId()));
				} else {
					pos.setExpirationDate(sec.getExpirationDate());
				}
				TreeSet<ATPersistentPosition> options = _optionPositionMap.get(symbol);
				if (options == null) {
					options = new TreeSet<>();
					_optionPositionMap.put(symbol, options);
				}
				options.add(pos);
			}
		}
		// _quantLib.init();
		log.info(String.format("Initialized [%d] positions", positions.size()));
	}

	@Override
	public void updatePositions(ATPosition... positions_) {
		if (positions_ == null || positions_.length == 0)
			return;
		log.info(String.format("[%d] positions", positions_.length));

		ATUser user = ATRequestContext.getUser();
		Instant now = Instant.now().truncatedTo(ChronoUnit.DAYS);
		Map<String, ATPersistentPosition> posMap = new HashMap<>();
		for (ATPosition p : positions_) {
			try {
				ATSecurity sec = p.getSecurity();
				if (sec == null)
					continue;
				String atId = String.format("%s %s %s", ATFormats.DATE_yyyy_MM_dd.get().format(p.getTradeDate()),
						ATFormats.TIME_HH_mm_ss.get().format(p.getTradeDate()), sec.getOccId());
				// if ("MCD 180323C00162500".equals(sec.getOccId())) {
				// log.info("hello");
				// }

				ATPersistentPosition pos = posMap.get(atId);
				if (pos != null) {
					double count = pos.getCount() + p.getCount();
					double price = (pos.getCount() * pos.getTradePrice() + p.getCount() * p.getPrice()) / count;
					pos.setCount(count);
					pos.setTradePrice(price);
					if (user != null) {
						pos.setOwnerId(user.getUid());
					}
				} else {
					pos = new ATPersistentPosition();
					pos.setAtId(atId);
					if (user != null) {
						pos.setOwnerId(user.getUid());
					}
					pos.setOccId(sec.getOccId());
					pos.setBroker(p.getBroker());
					pos.setChanged(true);
					pos.setCount(p.getCount());
					pos.setTradeDate(p.getTradeDate());
					pos.setTradePrice(p.getPrice());
					pos.setUnderlyingOccId(sec.getSymbol());
					pos.setTradeTotal(p.getTradeTotal());
					if (sec instanceof ATOptionSecurity) {
						ATOptionSecurity opt = (ATOptionSecurity) sec;
						pos.setExpirationDate(opt.getExpirationDate());
						pos.setOptionRight(opt.getOptionRight());
						pos.setStrike(opt.getStrike());
					} else {
						pos.setType(IATAssetConstants.STOCK_CODE);
					}
					posMap.put(atId, pos);

					ATPersistentEquityFoundation foundation = _foundationProvider.getFoundation(sec.getSymbol());
					if (foundation != null) {
						Date earningsDate = foundation.getEarningsDate();
						if (earningsDate != null && now.isBefore(earningsDate.toInstant())) {
							p.setDateEarnings(earningsDate);
						}
						Date dividendDate = foundation.getDividendExDate();
						if (dividendDate != null && now.isBefore(dividendDate.toInstant())) {
							p.setDateDividend(dividendDate);
						}
					} else {
						log.error(String.format("No foundation for [%s]", sec.getOccId()));
					}

					ATPersistentSecurity psec = _securitiesProvider.get(sec.getOccId());
					if (psec == null) {
						log.error(String.format("No data for [%-5s]", sec.getOccId()));
						continue;
					} else if (psec.getPrice() == null) {
						log.error(String.format("Missing price for [%-5s]", sec.getOccId()));
						continue;
					}
					sec.setPrice(psec.getPrice());
					double currentValue = psec.isOption() ? 100 * p.getCount() * sec.getPrice() : p.getCount() * sec.getPrice();
					p.setValue(currentValue);
					Double posCount = pos.getCount();
					Double pxPos = pos.getTradePrice();
					Double pxSec = psec.getPrice();
					double changePct = (posCount < 1 ? -100 : 100) * (pxSec - pxPos) / pxPos;
					if (Double.isFinite(changePct)) {
						p.setChangePct(changePct);
					} else {
						log.warn(String.format("Pricing error [%s]", pos.getOccId()));
					}

					if (psec.isOption()) {
						Double risk = psec.getProbability();
						if (risk != null) {
							p.setRisk(risk * 100.0);
						}
						ATOptionSecurity opt = (ATOptionSecurity) sec;
						opt.setExpirationDate(psec.getExpirationDate());
						ATPersistentSecurity usec = _securitiesProvider.get(opt.getSymbol().trim());
						if (usec != null) {
							Double upx = usec.getPrice();
							opt.setPriceUnderlying(upx);
							double strike = opt.getStrike();
							double count = p.getCount();
							double sign = count == 0 ? 0 : count / Math.abs(count);
							double margin = 100.0 * (strike - upx) / strike;
							if (ATOptionSecurity.Call.equals(opt.getOptionRight())) {
								p.setMargin(-sign * margin);
							} else {
								p.setMargin(sign * margin);
							}

							// if (hv != null) {
							// long days = ChronoUnit.DAYS.between(now, opt.getExpirationDate().toInstant()) + 1;
							// Double stdev = upx * hv * Math.sqrt(days / 365.0);
							// double zFactor = 1.0;
							// double strike = opt.getStrike();
							// double diff = upx > strike ? upx - strike : strike - upx;
							// double z = (upx - strike) / (stdev * zFactor);
							// boolean twist = ATOptionSecurity.Put.equals(opt.getOptionRight()) ? upx > strike : upx < strike;
							// double risk = twist ? ATStandardNormalTable.getProbabilityGreaterThan(z)
							// : ATStandardNormalTable.getProbabilityLessThan(z);
							// }
						} else {
							log.error(String.format("No underlying for [%s]", sec.getOccId()));
						}
					}
				}
				// if (pos.isOption()) {
				// ATOptionSecurity option = (ATOptionSecurity) p.getSecurity();
				// double strike = option.getStrike();
				// Double price = option.getPriceUnderlying();
				// if (price != null) {
				// double count = p.getCount();
				// double sign = count == 0 ? 0 : count / Math.abs(count);
				// double margin = 100.0 * (strike - price) / strike;
				// if (ATOptionSecurity.Call.equals(option.getOptionRight())) {
				// p.setMargin(-sign * margin);
				// } else {
				// p.setMargin(sign * margin);
				// }
				// } else {
				// log.warn(String.format("Underlying NULL price [%s]", option));
				// }
				// }
			} catch (Exception ex_) {
				log.error(String.format("Error in [%s]", p.getId()), ex_);
			}
		}
		updateAndPersist(posMap.values());
	}

	public Set<String> getUserIds(String occId_) {
		Set<String> result = new HashSet<>();
		if (StringUtils.isBlank(occId_))
			return result;
		if (occId_.length() > 10) {
			for (TreeSet<ATPersistentPosition> opts : _optionPositionMap.values()) {
				for (ATPersistentPosition opt : opts) {
					if (occId_.equals(opt.getOccId())) {
						result.add(opt.getOwnerId());
					}
				}
			}
		} else {
			for (ATPersistentPosition eqt : _equityPositions) {
				if (occId_.equals(eqt.getOccId())) {
					result.add(eqt.getOwnerId());
				}
			}
		}
		return result;
	}

	@Override
	public String getNewPositions(String userId_) {
		throw new NotImplementedException(userId_);
		
//		if (StringUtils.isBlank(userId_)) {
//			throw new ATException("Missing user ID");
//		}
//		Gson gson = new Gson();
//		JsonObject root = new JsonObject();
//		JsonArray posArray = new JsonArray();
//		root.add("positions", posArray);
//		JsonArray secArray = new JsonArray();
//		root.add("securities", secArray);
//
//		double interest = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
//
//		// String securities
//		Map<String, ATPersistentEquityFoundation> foundations = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundationMap();
//
//		Map<String, ATPersistentSecurity> securities = new HashMap<>();
//
//		Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
//		String nowDS8 = ATFormats.DATE_yyyyMMdd.get().format(now);
//		TreeSet<ATPersistentPosition> posns = getPositions(userId_);
//		for (ATPersistentPosition pos : posns) {
//			ATPersistentSecurity sec = securities.get(pos.getOccId());
//			if (sec == null) {
//				sec = _securitiesProvider.get(pos.getOccId());
//				if (sec == null) {
//					log.error(String.format("Security not found [%s]", pos.getOccId()));
//					continue;
//				}
//			}
//			IATEquityFoundation fnd = foundations.get(sec.getSymbol());
//
//			JsonObject jp = new JsonObject();
//			jp.addProperty("aid", pos.getOccId());
//			jp.addProperty("label", pos.getLabel());
//
//			jp.addProperty("count", pos.getCount());
//			jp.addProperty("acct", pos.getBroker());
//			jp.addProperty("dateTrd", pos.getTradeDateStr());
//			jp.addProperty("pxTrd", pos.getTradePrice());
//			jp.addProperty("pxClose", sec.getPriceClose());
//			jp.addProperty("pxLast", sec.getPrice());
//			jp.addProperty("pxTotal", pos.getTradeTotal());
//			if (fnd != null) {
//				Date earn = fnd.getEarningsDate();
//				if (earn != null && now.before(earn)) {
//					jp.addProperty("dateEarn", ATFormats.DATE_yyyy_MM_dd.get().format(fnd.getEarningsDate()));
//				}
//			}
//			if (sec.isOption()) {
//				ATPersistentSecurity ul = securities.get(pos.getUnderlyingOccId());
//				if (ul == null) {
//					ul = _securitiesProvider.get(sec.getUnderlyingOccId());
//					securities.put(ul.getOccId(), ul);
//					TdnSecurity tdn = TdnConverter.toTdnSecurity(ul, fnd, null, null, null, nowDS8);
//					JsonElement tdnJson = gson.toJsonTree(tdn);
//					secArray.add(tdnJson);
//				}
//				String right = sec.getOptionRight();
//				boolean isCall = "C".equals(right);
//				Double ulPx = ul.getPrice();
//				Double strike = sec.getStrike();
//				// TdnSecurity tdn = TdnConverter.toTdnSecurity(ul, fnd, null, null, null, now);
//				// JsonElement tdnJson = gson.toJsonTree(tdn);
//				// jp.add("sec", tdnJson);
//				// jp.addProperty("sec", gson.toJson(tdn));
//
//				jp.addProperty("ulId", sec.getUnderlyingOccId());
//				jp.addProperty("pxUL", ulPx);
//				jp.addProperty("optRight", right);
//				jp.addProperty("dateExp", ATFormats.DATE_yyyy_MM_dd.get().format(sec.getExpirationDate()));
//				long diffInMillies = sec.getExpirationDate().getTime() - now.getTime();
//				long days = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
//				jp.addProperty("expDays", days);
//				jp.addProperty("strk", strike);
//				if (fnd != null) {
//					Double iv = fnd.getIV();
//					double volatility = iv != null ? iv : fnd.getHV60();
//					Double div = fnd.getDividend();
//					Integer divFq = fnd.getDividendFrequency();
//					double yield = div == null || divFq == null ? 0.0 : div * divFq / ulPx;
//					ATOptionDescription option = new ATOptionDescription(sec.getSymbol(), sec.getExpirationDate(), strike, right);
//					Map<String, ATOptionAnalytics> analyticsMap = ATQuantLib.instance(_config).priceAmericanEquityOption(ulPx, volatility, yield,
//							interest, option);
//					ATOptionAnalytics analytics = analyticsMap != null ? analyticsMap.get(option.getOccId()) : null;
//					if (analytics != null) {
//						jp.addProperty("pxTheo", analytics.getPrice());
//					}
//				}
//				double mrg = (strike - ulPx) / strike;
//				double isOwned = pos.getCount() > 0 ? 1 : -1;
//				double isInTheMoney = isCall ? (mrg < 0 ? 1 : -1) : (mrg > 0 ? 1 : -1);
//				mrg = Precision.round(Math.abs(mrg) * isInTheMoney * isOwned, 4);
//				jp.addProperty("strkMrg", mrg);
//				Double risk = sec.getProbability();
//				if (risk != null) {
//					if (isOwned > 0) {
//						// risk = 1.0 - risk;
//					}
//					if (risk > 1) {
//						risk = 1.0;
//					}
//					risk = Precision.round(risk, 4);
//					jp.addProperty("risk", risk);
//				}
//			} else {
//				if (!securities.containsKey(sec.getOccId())) {
//					securities.put(sec.getOccId(), sec);
//					TdnSecurity tdn = TdnConverter.toTdnSecurity(sec, fnd, null, null, null, now);
//					JsonElement tdnJson = gson.toJsonTree(tdn);
//					secArray.add(tdnJson);
//				}
//				// TdnSecurity tdn = TdnConverter.toTdnSecurity(sec, fnd, null, null, null, now);
//				// JsonElement tdnJson = gson.toJsonTree(tdn);
//				// jp.add("sec", tdnJson);
//			}
//			posArray.add(jp);
//		}
//		String result_digest = gson.toJson(root);
//		return result_digest;
	}

	@Override
	public TreeSet<ATPersistentPosition> getPositions(String userId_) {
		TreeSet<ATPersistentPosition> result = new TreeSet<>();
		if (StringUtils.isBlank(userId_)) {
			log.warn("MISSING POSITION USER ID (Defaulting to all)");
		}
		Set<ATPersistentPosition> equities = StringUtils.isBlank(userId_) ? _equityPositions
				: _equityPositions.stream().filter(p -> userId_.equals(p.getOwnerId())).collect(Collectors.toSet());
		result.addAll(equities);
		Map<String, TreeSet<ATPersistentPosition>> optionsMap = _optionPositionMap;
		for (Set<ATPersistentPosition> os : optionsMap.values()) {
			if (StringUtils.isNotBlank(userId_)) {
				os = os.stream().filter(p -> userId_.equals(p.getOwnerId())).collect(Collectors.toSet());
			}
			result.addAll(os);
		}
		return result;
	}

	private boolean updateAndPersist(Collection<ATPersistentPosition> positions_) {
		if (positions_ == null || positions_.isEmpty())
			return false;
		Set<String> positionIds = new TreeSet<>();
		Set<ATPersistentPosition> eSet = new TreeSet<>();
		Map<String, TreeSet<ATPersistentPosition>> pMap = new TreeMap<>();
		Set<IATLookupMap> bulkUpdateSet = new HashSet<>();
		for (ATPersistentPosition pos : positions_) {
			if (pos.isOption()) {
				String symbol = pos.getSymbol();
				TreeSet<ATPersistentPosition> ps = pMap.get(symbol);
				if (ps == null) {
					ps = new TreeSet<>();
					pMap.put(symbol, ps);
				}
				ps.add(pos);
			} else {
				eSet.add(pos);
			}
			if (pos.isChanged()) {
				pos.setUpdateTime(System.currentTimeMillis());
				bulkUpdateSet.add(pos);
			}
			positionIds.add(pos.getOccId());
		}
		_equityPositions = eSet;
		_optionPositionMap = pMap;

		// String occIds = String.join(",", positionIds);
		// String[] positionSymbolDb = _atConfig.getString("positionSymbols").split("/");
		// ATPersistentLookupBase base = new ATPersistentLookupBase();
		// base.setAtId(positionSymbolDb[1]);
		// base.put(positionSymbolDb[2], occIds);
		// _dao.update(positionSymbolDb[0], base);

		if (!bulkUpdateSet.isEmpty()) {
			Thread daoThread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						long startTime = System.currentTimeMillis();
						String positionTableId = _atConfig.getString("positionTableId");
						long count = _dao.delete(positionTableId, ATPersistenceRegistry.query().empty());
						log.debug(String.format("Removed [%d] positions", count));
						boolean success = _dao.update(positionTableId, bulkUpdateSet.toArray(new IATLookupMap[0]));
						if (!success)
							return;
						_throughputCount += bulkUpdateSet.size();
						_throughputTime += System.currentTimeMillis() - startTime;
						if (_throughputTime > 1000) {
							double tp = _throughputCount * 1.0 / (_throughputTime / 1000.0);
							_throughputCount = 0;
							_throughputTime = 0;
							log.info(String.format("DAO throughput [%.2f/s]", tp));
						}
					} catch (Exception ex) {
						log.error(String.format("Error persisting"), ex);
					}
				}
			}, "PositionDao");
			daoThread.start();
		}
		return true;
	}

	@Override
	public Set<ATExposure> getExposure() {
		Map<String, ATExposure> resultMap = new HashMap<>();
		double equityTotal = 0;
		double callTotal = 0;
		double putTotal = 0;
		// 1. Equity
		if (_equityPositions != null && !_equityPositions.isEmpty()) {
			for (ATPersistentPosition pos : _equityPositions) {
				try {
					String occId = pos.getOccId();
					ATPersistentSecurity sec = _securitiesProvider.get(occId);
					if (sec == null) {
						log.warn(String.format("No security [%s]", occId));
						continue;
					} else if (sec.getPrice() == null) {
						log.warn(String.format("Missing price [%s]", occId));
						continue;
					}
					double value = pos.getCount() * sec.getPrice();
					equityTotal += value;
					String key = occId;// String.format("%s [S]", occId);
					ATExposure exp = resultMap.get(key);
					if (exp == null) {
						exp = new ATExposure(key);
						exp.setEquityAmount(value);
						resultMap.put(key, exp);
					} else {
						exp.setEquityAmount(exp.getEquityAmount() + value);
					}
				} catch (Exception ex_) {
					log.error(String.format("Error processing exposure [%s]", pos.getOccId()), ex_);
				}
			}
		} else {
			log.debug("No equity exposure");
		}
		// 2. Options
		if (_optionPositionMap != null && !_optionPositionMap.isEmpty()) {
			Map<String, TreeSet<ATPersistentPosition>> pMap = _optionPositionMap;
			TreeSet<String> symbols = new TreeSet<>(pMap.keySet());
			for (String sym : symbols) {
				try {
					Map<String, Double> metaMap = new HashMap<>();
					TreeSet<ATPersistentPosition> pList = pMap.get(sym);
					double callSum = 0;
					double putSum = 0;
					for (ATPersistentPosition p : pList) {
						try {
							if (ATOptionSecurity.Call.equals(p.getOptionRight())) {
								callSum += 100 * p.getCount() * p.getStrike();
							} else if (ATOptionSecurity.Put.equals(p.getOptionRight())) {
								putSum += 100 * p.getCount() * p.getStrike();
							}
							Date expDate = p.getExpirationDate();
							String dateStr = ATFormats.DATE_MMdd.get().format(expDate);
							double optCount = Math.abs(p.getCount());
							if (metaMap.containsKey(dateStr)) {
								optCount += metaMap.get(dateStr);
							}
							metaMap.put(dateStr, optCount);
						} catch (Exception ex) {
							log.error(String.format("Error getting exposure [%s]", p.getOccId()), ex);
						}
					}
					ATExposure exp = resultMap.get(sym);
					if (exp == null) {
						exp = new ATExposure(sym);
						resultMap.put(sym, exp);
					}
					exp.setCallAmount(callSum);
					exp.setPutAmount(putSum);
					double eqAmount = exp.getEquityAmount() != null ? exp.getEquityAmount() : 0;
					exp.setTotalAmount(eqAmount + callSum + putSum);

					Set<String> metaSet = new TreeSet<>();
					for (Entry<String, Double> e : metaMap.entrySet()) {
						metaSet.add(String.format("%s(%2.0f)", e.getKey(), e.getValue()));
					}
					String metaStr = String.join(", ", metaSet);
					exp.setMeta(metaStr);
					callTotal += callSum;
					putTotal += putSum;
					log.debug(String.format("Option exposure: %-5s: %9.2f", sym, callSum + putSum));
				} catch (Exception ex) {
					log.error(String.format("Error getting exposure [%s]", sym), ex);
				}
			}
		} else {
			log.debug("No option exposure");
		}
		Set<ATExposure> result = new TreeSet<>(resultMap.values());
		ATExposure texp = new ATExposure("Ƭotal");
		texp.setEquityAmount(equityTotal);
		texp.setCallAmount(callTotal);
		texp.setPutAmount(putTotal);
		texp.setTotalAmount(equityTotal + callTotal + putTotal);
		result.add(texp);
		return result;
	}

}
