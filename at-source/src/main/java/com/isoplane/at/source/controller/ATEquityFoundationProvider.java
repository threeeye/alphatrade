package com.isoplane.at.source.controller;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.IATEvent;
import com.isoplane.at.commons.util.IATEventSubscriber;
import com.isoplane.at.mongodb.IATPersistentLookupStore;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.source.util.ATAuthenticationEvent;

public class ATEquityFoundationProvider implements IATEquityFoundationProvider, IATEventSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATEquityFoundationProvider.class);

	// private Configuration _config;
	// private IATConfiguration _atConfig;
	private IATPersistentLookupStore _dao;
	private Map<String, ATPersistentEquityFoundation> _fMap;
	private long _lastUpdate = 0;
	// private long _lastRunTime;
	// private Map<String, Map<String, String>> _notes;
	// private Map<String, ATPersistentNote> _notesMap;

	public ATEquityFoundationProvider(IATPersistentLookupStore dao_) {
		// _config = config_;
		// _atConfig = atConfig_;
		_dao = dao_;
		// _notes = new HashMap<>();
		ATSourceServiceRegistry.subscribeEvents(this);
	}

	@Override
	public void initService() {
		init();
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		long foundationPeriod = config.getLong("period.foundation");// _atConfig.getLong("foundationPeriod");
		String threadId = String.format("%s.foundation", this.getClass().getSimpleName());
		ATExecutors.scheduleAtFixedRate(threadId, () -> update(), 0L, foundationPeriod);
	}

	private void update() {
		this._lastUpdate = System.currentTimeMillis();
		String foundationTableId = ATMongoTableRegistry.getFoundationTable();
		// _lastRunTime = System.currentTimeMillis();
		// String foundationTableId = _atConfig.getString("foundationTableId");
		List<ATPersistentEquityFoundation> foundation = _dao.query(foundationTableId, null, ATPersistentEquityFoundation.class);
		Map<String, ATPersistentEquityFoundation> newMap = new TreeMap<>();
		for (ATPersistentEquityFoundation f : foundation) {
			newMap.put(f.getOccId(), f);
		}
		_fMap = Collections.unmodifiableMap(newMap);
		Duration d = Duration.of(System.currentTimeMillis() - this._lastUpdate, ChronoUnit.MILLIS);
		log.info(String.format("Initialized [%d] equities [%s]", _fMap.size(), d));
	}

	@Override
	public Map<String, ATPersistentEquityFoundation> getFoundationMap() {
		return _fMap;
	}

	@Override
	public ATPersistentEquityFoundation getFoundation(String occId_) {
		ATPersistentEquityFoundation fnd = _fMap.get(occId_);
		return fnd;
	}

	@Override
	public void notify(IATEvent event_) {
		if (event_ instanceof ATAuthenticationEvent) {
			ATAuthenticationEvent event = (ATAuthenticationEvent) event_;
			if (StringUtils.isBlank(event.getUid()))
				return;
			if (event.isActive()) {
				// loadNotes(event.getUid());
			} else {
				// _notes.remove(event.getUid());
				// log.debug(String.format("Cleared notes for [%s]", event.getUid()));
			}
		}
	}

}
