package com.isoplane.at.source.model;

import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.ATException;

@Deprecated
public class ATUser {

	static final public String PERMISSION_IDEAS = "ideas";
	static final public String PERMISSION_NOTIFICATIONS = "notifications";
	static final public String PERMISSION_CONFIGURATION = "configuration";
	static final public String PERMISSION_PORTFOLIO = "portfolio";
	static final public String PERMISSION_ALERTS = "alerts";

	private String email;
	private String sheetId;
	private String uid;
	private Set<String> permissions;
	private Set<String> notificationTokens;

	transient private ATUserConfiguration _config;

	public ATUser(String email_, IATUserConfiguration config_) {
		if (StringUtils.isBlank(email_))
			throw new ATException("Missing email");
		setEmail(email_);
		_config = new ATUserConfiguration();
		_config.update(_config, email_);
	}

	public boolean hasPermission(String permission_) {
		return permissions != null && permissions.contains(permission_);
	}

	public String getSheetId() {
		return sheetId;
	}

	public void setSheetId(String sheetId) {
		this.sheetId = sheetId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Set<String> permissions) {
		this.permissions = permissions;
	}

	public Set<String> getNotificationTokens() {
		return notificationTokens;
	}

	public void setNotificationTokens(Set<String> notificationTokens) {
		this.notificationTokens = notificationTokens;
	}

	public IATUserConfiguration getConfig() {
		return _config;
	}

	public boolean updateConfig(IATUserConfiguration config_) {
		return _config.update(config_, getEmail());
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

}
