package com.isoplane.at.source.model;

// Note: May be derivative
public class TdnPosition {

	public String tid;
	public String acct;
	public Double pxTrd;
	public Double pxLast;
	public Double pxUL;
	public String dateTrd;
	public String dateEarn;
	public Double count;
	public String label;
}
