package com.isoplane.at.source.util;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserEquityModel.ATModelFilter;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;

public class ATModelTranslator {
	static final Logger log = LoggerFactory.getLogger(ATModelTranslator.class);

	static public IATWhereQuery modelToMarketQuery(ATUserEquityModel model_) {
		ArrayList<ATModelFilter> filters = model_ != null ? model_.getFilters() : null;
		if (filters == null || filters.isEmpty())
			return null;

		boolean isInitialized = false;
		IATWhereQuery query = ATPersistenceRegistry.query().isNull(IATAssetConstants.OCCID_UNDERLYING);
		for (ATModelFilter filter : filters) {
			String asset = filter.getAsset();
			if (ATModelFilter.ASSET_OPTION.equals(asset))
				continue;
			Double min = filter.getMin();
			Double max = filter.getMax();
			// Object value = filter.getValue();
			switch (filter.getId()) {
			case ATModelFilter.PX:
				if (min != null) {
					query = query.gte(IATAssetConstants.PX_LAST, min);
					isInitialized = true;
				}
				if (max != null) {
					query = query.lte(IATAssetConstants.PX_LAST, max);
					isInitialized = true;
				}
				break;
			case ATModelFilter.PX_CHANGE:
				if (min != null) {
					// min = min - 100.0;
					query = query.gte(IATAssetConstants.PX_CHANGE, min);
					isInitialized = true;
				}
				if (max != null) {
					// max = (max - 100.0);
					query = query.lte(IATAssetConstants.PX_CHANGE, max);
					isInitialized = true;
				}
				break;
			// Fundamental properties (ignored)
			case ATModelFilter.INDUSTRY:
			case ATModelFilter.MCAP:
			case ATModelFilter.PE_GROWTH:
			case ATModelFilter.PX_MAX_254:
			case ATModelFilter.REV_GROWTH_3Y:
			case ATModelFilter.REV_MLT:
			case ATModelFilter.SECTOR:
			case ATModelFilter.SMA_60:
			case ATModelFilter.SMA_125:
			case ATModelFilter.SMA_254:
			case ATModelFilter.SYMBOLS:
			case ATModelFilter.TRAILING_PE:
			case ATModelFilter.WATCHLIST:
				break;
			default:
				log.error(String.format("Unsupported filter [%s]", filter.getId()));
				break;
			}
		}
		return isInitialized ? query : null;
	}

	static public IATWhereQuery modelToFundamentalQuery(ATUserEquityModel model_) {
		ArrayList<ATModelFilter> filters = model_ != null ? model_.getFilters() : null;
		if (filters == null || filters.isEmpty())
			return null;

		IATWhereQuery query = ATPersistenceRegistry.query();
		for (ATModelFilter filter : filters) {
			Double min = filter.getMin();
			Double max = filter.getMax();
			Object value = filter.getValue();
			switch (filter.getId()) {
			case ATModelFilter.SECTOR:
				query = query.eq(IATAssetConstants.SECTOR, value);
				break;
			case ATModelFilter.INDUSTRY:
				query = query.eq(IATAssetConstants.INDUSTRY, value);
				break;
			case ATModelFilter.MCAP:
				if (min != null) {
					query = query.gte(IATAssetConstants.MARKET_CAP, min);
				}
				if (max != null) {
					query = query.lte(IATAssetConstants.MARKET_CAP, max);
				}
				break;
			// Market properties (ignored)
			case ATModelFilter.PX:
			case ATModelFilter.PX_CHANGE:
				break;
			case ATModelFilter.SYMBOLS:
				// TODO: Check why no filter
				break;
			default:
				log.error(String.format("Unsupported filter [%s]", filter.getId()));
				break;
			}

		}
		return query;
	}

}
