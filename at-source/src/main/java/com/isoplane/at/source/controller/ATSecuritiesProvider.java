package com.isoplane.at.source.controller;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Year;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.config.IATConfiguration;
import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.ATResponsePack.ATStrategyPack;
import com.isoplane.at.commons.model.ATSecurityResearchConfig;
import com.isoplane.at.commons.model.ATStrategy;
import com.isoplane.at.commons.model.ATStrategy.ATStrategyGroup;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATUserNote;
import com.isoplane.at.commons.model.ATVerticalSpread;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATEquityFoundation;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATOptionDescription;
import com.isoplane.at.commons.model.IATSecurityResearchConfig;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentPosition;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATMathUtil;
import com.isoplane.at.commons.util.ATStandardNormalTable;
import com.isoplane.at.mongodb.IATPersistentLookupStore;
import com.isoplane.at.source.model.TdnConverter;
import com.isoplane.at.source.model.TdnEquityResearchConfig;
import com.isoplane.at.source.model.TdnEquityResearchResponse;
import com.isoplane.at.source.model.TdnOHLCV;
import com.isoplane.at.source.model.TdnSecurity;
import com.isoplane.at.source.model.TdnSecurityResponse;
import com.isoplane.at.source.model.TdnSymbolLookup;

public class ATSecuritiesProvider implements IATSecuritiesProvider {

	static final Logger log = LoggerFactory.getLogger(ATSecuritiesProvider.class);

	private IATConfiguration _config;
	private IATPersistentLookupStore _dao;
	private IATEquityFoundationProvider _foundationProvider;
	private ConcurrentSkipListMap<String, ATPersistentSecurity> _stockMap;
	private ConcurrentSkipListMap<String, ATPersistentSecurity> _optionMap;
	private ConcurrentSkipListMap<String, ATPersistentSecurity> _indexMap;

	public ATSecuritiesProvider(IATConfiguration config_, IATPersistentLookupStore dao_) {
		_config = config_;
		_dao = dao_;
		_optionMap = new ConcurrentSkipListMap<>();
		_stockMap = new ConcurrentSkipListMap<>();
		_indexMap = new ConcurrentSkipListMap<>();
	}

	@Override
	public void initService() {
		_foundationProvider = ATSourceServiceRegistry.getEquityFoundationProvider();
		if (_foundationProvider == null) {
			throw new ATException(String.format("Initialization error [%s]", IATEquityFoundationProvider.class.getSimpleName()));
		}
		// init();
	}

	// private void init() {
	// log.info(String.format("Initializing..."));
	// _stockMap.clear();
	// _optionMap.clear();
	// if (_config.getBoolean("isInitializeOnStart")) {
	//
	// String securityTableId = _config.getString("securityTableId");
	// IATWhereQuery query = ATPersistenceRegistry.query().eq(ATPersistentSecurity.TYPE, 1);
	// List<ATPersistentSecurity> stocks = _dao.query(securityTableId, query, ATPersistentSecurity.class);
	// if (stocks != null) {
	// for (ATPersistentSecurity stock : stocks) {
	// if (stock.getOccId().startsWith("$")) {
	// _indexMap.put(stock.getOccId(), stock);
	// } else {
	// _stockMap.put(stock.getOccId(), stock);
	// }
	// }
	// }
	//
	// String indexTableId = _config.getString("indexTableId");
	// IATWhereQuery idxQuery = ATPersistenceRegistry.query().eq("type", "index");
	// List<ATPersistentSecurity> indices = _dao.query(indexTableId, idxQuery, ATPersistentSecurity.class);
	// if (indices != null) {
	// for (ATPersistentSecurity idx : indices) {
	// ATPersistentSecurity index = _indexMap.get(idx.getOccId());
	// if (index != null) {
	// index.put(IATAssetConstants.NAME, idx.get(IATAssetConstants.NAME));
	// index.put(IATAssetConstants.DESCRIPTION, idx.get(IATAssetConstants.DESCRIPTION));
	// } else {
	// _indexMap.put(idx.getOccId(), idx);
	// }
	// }
	// }
	//
	// log.info(String.format("Loaded [%3d] stocks. Loading options...", stocks.size()));
	// int ocount = 0;
	// Map<String, Collection<ATPersistentSecurity>> optionMap = new HashMap<>();
	// for (ATPersistentSecurity s : stocks) {
	// ocount++;
	// String todayStr = ATFormats.DATE_yyyyMMdd.get().format(new Date());
	// IATWhereQuery query2 = new ATMongoWhereQuery().eq(ATPersistentSecurity.OCCID_UNDERLYING, s.getSymbol()).and()
	// .eq(ATPersistentSecurity.TYPE, ATPersistentSecurity.OPTION_PUT_CODE).and()
	// .gte(ATPersistentSecurity.EXPIRATION_DATE_STR_OLD, todayStr);
	// List<ATPersistentSecurity> options = _dao.query(securityTableId, query2, ATPersistentSecurity.class);
	// if (options != null) {
	// log.info(String.format("[%3d/%3d %-5s] Loading [%3d] options", ocount, stocks.size(), s.getSymbol(), options.size()));
	// optionMap.put(s.getOccId(), options);
	// for (ATPersistentSecurity o : options) {
	// _optionMap.put(o.getOccId(), o);
	// }
	// }
	// }
	// processOptions(optionMap);
	// }
	// }

	@Override
	public Set<String> getStockIds() {
		Set<String> result = new HashSet<>(_stockMap.keySet());
		return result;
	}

	@Override
	public ATPersistentSecurity get(String key_) {
		if (StringUtils.isBlank(key_))
			return null;
		ATPersistentSecurity sec;
		if (key_.length() <= 13) {
			sec = _stockMap.get(key_);
		} else {
			sec = _optionMap.get(key_);
		}
		if (sec == null) {
			String securityTableId = _config.getString("securityTableId");
			IATWhereQuery query = ATPersistenceRegistry.query().eq(ATPersistentSecurity.AT_ID, key_);
			List<ATPersistentSecurity> secs = _dao.query(securityTableId, query, ATPersistentSecurity.class);
			if (secs != null && secs.size() == 1) {
				log.info(String.format("Loading [%s]", key_));
				sec = secs.get(0);
				if (sec.isOption()) {
					_optionMap.put(key_, sec);
				} else {
					_stockMap.put(key_, sec);
				}
			} else {
				log.warn(String.format("Missing [%s]", key_));
			}
		}
		return sec;
	}

	public List<ATPersistentSecurity> get(String atid_, boolean includeOptions_) {
		if (StringUtils.isBlank(atid_))
			return null;
		List<ATPersistentSecurity> result = new ArrayList<>();
		String securityTableId = _config.getString("securityTableId");

		if (atid_.length() <= 10) {
			ATPersistentSecurity sec = _stockMap.get(atid_);
			if (sec == null) {
				IATWhereQuery query = ATPersistenceRegistry.query().eq(ATPersistentSecurity.AT_ID, atid_);
				List<ATPersistentSecurity> secs = _dao.query(securityTableId, query, ATPersistentSecurity.class);
				if (secs != null && !secs.isEmpty()) {
					result.addAll(secs);
				}
			}
			if (!includeOptions_)
				return result;

			List<ATPersistentSecurity> options = _optionMap.values().stream().filter(o -> atid_.equals(o.getUnderlyingOccId()))
					.collect(Collectors.toList());
			if (options != null && !options.isEmpty()) {
				result.addAll(options);
				return result;
			}
			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID_UNDERLYING, atid_);
			options = _dao.query(securityTableId, query, ATPersistentSecurity.class);
			if (options != null) {
				result.addAll(options);
			}
			return result;
		} else {
			ATPersistentSecurity sec = _optionMap.get(atid_);
			if (sec == null) {
				IATWhereQuery query = ATPersistenceRegistry.query().eq(ATPersistentSecurity.AT_ID, atid_);
				List<ATPersistentSecurity> secs = _dao.query(securityTableId, query, ATPersistentSecurity.class);
				if (secs != null) {
					result.addAll(secs);
				}
			}
			return result;
		}

	}

	@Override
	public Collection<ATPersistentSecurity> getOptionChain(String key_) {
		if (key_ == null)
			return null;
		String fromKey = String.format("%-6s", key_);// + "000000";
		// String toKey = key_.replace(" ", "0");// "999999";
		ConcurrentNavigableMap<String, ATPersistentSecurity> subMap = _optionMap.subMap(fromKey + "0", fromKey + "9");
		return subMap == null ? new TreeSet<>() : subMap.values();
	}

	@Override
	public int put(ATPersistentSecurity... secs_) {
		if (secs_ == null || secs_.length == 0)
			return 0;
		int count = 0;
		Set<String> stockIds = new HashSet<>();
		for (ATPersistentSecurity s : secs_) {
			if (s.isOption()) {
				ATPersistentSecurity old = _optionMap.put(s.getOccId(), s);
				if (old == null) {
					count++;
				}
			} else if (s.isIndex()) {
				ATPersistentSecurity old = _indexMap.put(s.getOccId(), s);
				if (old == null) {
					count++;
				} else {
					String name = (String) old.get(IATAssetConstants.NAME);
					s.put(IATAssetConstants.NAME, name);
					String dscr = (String) old.get(IATAssetConstants.DESCRIPTION);
					s.put(IATAssetConstants.DESCRIPTION, dscr);
				}
			} else {
				stockIds.add(s.getOccId());
				ATPersistentSecurity old = _stockMap.put(s.getOccId(), s);
				if (old == null) {
					count++;
				}
			}
		}
		Map<String, Collection<ATPersistentSecurity>> optionMap = new HashMap<>();
		for (String occId : stockIds) {
			Collection<ATPersistentSecurity> options = getOptionChain(occId);
			if (options == null || options.isEmpty())
				continue;
			optionMap.put(occId, options);
		}
		processOptions(optionMap);
		log.debug(String.format("Added [%d] securities", count));
		return count;
	}

	private void processOptions(Map<String, Collection<ATPersistentSecurity>> optionMap_) {
		for (Entry<String, Collection<ATPersistentSecurity>> om : optionMap_.entrySet()) {
			String sym = om.getKey();
			ATPersistentSecurity stock = _stockMap.get(sym);
			if (stock == null || stock.getPrice() == null) {
				log.error(String.format("No priced stock [%s]", sym));
				continue;
			}
			ATPersistentEquityFoundation foundation = _foundationProvider.getFoundation(sym);
			if (foundation == null) {
				log.error(String.format("No foundation [%s]", sym));
				continue;
			}
			String hvMode = _config.getString("spreadHvMode");
			Double hv = foundation.getHV(hvMode);
			if (hv == null) {
				hv = stock.getHV();
			}
			if (hv == null) {
				log.debug(String.format("Failed to get stdev for [%-5s]", sym));
				continue;
			}
			double zFactor = 1.0; // TODO: Consider adjusting for earnings etc
			double sprice = stock.getPrice();
			Instant now = Instant.now().truncatedTo(ChronoUnit.DAYS);
			for (ATPersistentSecurity option : om.getValue()) {
				// if("MCD 180323C00162500".equals(option.getOccId())) {
				// log.info("hello opt");
				// }
				long days = ChronoUnit.DAYS.between(now, option.getExpirationDate().toInstant()) + 1;
				double stdev = sprice * hv * Math.sqrt(days / 365.0);
				double z = Math.abs(sprice - option.getStrike()) / (stdev * zFactor);
				// double p = ATOptionSecurity.Put.equals(option.getOptionRight()) ? ATStandardNormalTable.getProbabilityGreaterThan(z)
				// : ATStandardNormalTable.getProbabilityLessThan(z);
				int pDir = "P".equals(option.getOptionRight()) ? 1 : -1;
				int pItm = option.getStrike() < sprice ? 1 : -1;
				double p = (pItm * pDir) > 0 ? ATStandardNormalTable.getProbabilityGreaterThan(z)
						: ATStandardNormalTable.getProbabilityLessThan(z);
				option.setProbability(p);
			}

		}
	}

	@Override
	public TdnEquityResearchResponse getSecurities(String uid_, ATSecurityResearchConfig config_) {
		if (config_ == null)
			return null;
		config_.cleanup();
		boolean isUnderlyingOnly = IATSecurityResearchConfig.TYPE_SECURITY.equals(config_.getType());
		ATSecurityResearchConfig equityConfig = isUnderlyingOnly ? config_ : config_.getUnderlyingConfig();
		List<TdnSecurity> securities = getEquities(uid_, equityConfig);
		TdnEquityResearchResponse result = new TdnEquityResearchResponse();
		result.type = config_.getType();
		result.config = config_;
		if (isUnderlyingOnly) {
			result.equities = securities.toArray(new TdnSecurity[0]);
		} else if (securities != null && !securities.isEmpty()) {
			// Set<String> symbols = securities.stream().map(s -> s.id).collect(Collectors.toCollection(TreeSet::new));
			Map<String, List<ATStrategy>> strategyMap = getOptionStrategies(uid_, securities, config_);
			if (strategyMap != null && !strategyMap.isEmpty()) {
				List<ATStrategy> strategies = new ArrayList<>();
				strategyMap.values().forEach(v -> strategies.addAll(v));
				// result_digest.strategies = strategyMap.values().toArray(new ATStrategy[0]);
				result.strategies = strategies.toArray(new ATStrategy[0]);
			}
		}
		return result;
	}

	private Map<String, List<ATStrategy>> getOptionStrategies(String uid_, List<TdnSecurity> equities_, ATSecurityResearchConfig config_) {
		try {
			Map<String, ATEquityDescription> dscMap = new TreeMap<>();
			Map<String, ATFundamental> fndMap = new TreeMap<>();
			Map<String, ATMarketData> mktMap = new TreeMap<>();
			List<ATStrategyGroup> stratGroups = new ArrayList<>();

			IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
			IATEquityFoundationProvider fndSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
			// Map<String, ATMarketData> sMktData = mktSvc.getMarketData(true);

			Integer daysMax = config_.getStrategyDaysMax();
			String maxDate = daysMax != null ? ATFormats.DATE_yyMMdd.get().format(DateUtils.addDays(new Date(), daysMax)) : null;
			String[] maxDates = { maxDate };
			final Long maxAge = config_.getEquityMaxAge() != null ? System.currentTimeMillis() - config_.getEquityMaxAge() : null;
			final Boolean isSkipEarnings = config_.isSkipEarnings() != null ? config_.isSkipEarnings() : false;

			Set<String> userOccIds = ATSourceServiceRegistry.getPortfolioService().getActiveOccIds(uid_, true);

			Map<String, List<ATStrategy>> result = new TreeMap<String, List<ATStrategy>>();
			int count = 0;
			for (TdnSecurity equity : equities_) {
				String symbol = equity.id;
				ATMarketData ulMkt = mktSvc.getMarketData(symbol);
				ATPersistentEquityFoundation ulFnd = fndSvc.getFoundation(symbol);
				if (ulMkt == null || ulFnd == null) {
					log.warn(String.format("Missing foundation[%b] or market data[%b] for [%d]", ulFnd != null, ulMkt != null, symbol));
					continue;
				}
				if (!isSkipEarnings) {
					String earningsDS8 = ulFnd.getEarningsDS8();
					if (earningsDS8 != null) {
						String earningsDS6 = earningsDS8.substring(2);
						if (maxDate == null || maxDate.compareTo(earningsDS6) > 0) {
							maxDates[0] = earningsDS6;
						}
					}
				}
				log.debug(String.format("[%3d/%d] Processing [%s]", ++count, equities_.size(), symbol));
				// Map<String, ATMarketData> oMktData = mktSvc.getMarketData(false);
				IATWhereQuery oQuery = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID_UNDERLYING, symbol);
				Map<String, ATMarketData> oMktData = mktSvc.getMarketData(oQuery);
				List<IATMarketData> mktOptions = oMktData.values().stream()
						.filter(m -> {
							String underlying = m.getUnderlying();
							if (underlying == null || !symbol.equals(m.getUnderlying()))
								return false;
							if (maxAge != null) {
								Long tsLast = m.getTsLast();
								if (tsLast == null || maxAge > m.getTsLast())
									return false;
							}
							if (maxDates[0] != null) {
								String expDS6 = IATMarketData.getExpDS6(m);
								if (expDS6 == null || expDS6.compareTo(maxDates[0]) >= 0)
									return false;
							}
							return true;
						})
						.sorted((a, b) -> a.getAtId().compareTo(b.getAtId()))
						.collect(Collectors.toList());
				if (mktOptions == null || mktOptions.isEmpty()) {
					log.warn(String.format("Missing options data [%s]", symbol));
					continue;
				}
				List<ATStrategy> symbolStrategies = analyzeOptions(ulFnd, ulMkt, mktOptions, config_);
				if (symbolStrategies != null && !symbolStrategies.isEmpty()) {
					double owned = 0;
					if (userOccIds.contains(symbol)) {
						owned += 1.0;
					}
					if (userOccIds.stream().anyMatch(i -> i.length() > 6 && i.startsWith(symbol))) {
						owned += 0.5;
					}
					for (ATStrategy strategy : symbolStrategies) {
						strategy.owned = owned;
					}
					result.put(symbol, symbolStrategies);

					ATFundamental fnd = new ATFundamental(ulFnd, config_.getAvgMode(), config_.getHVMode());
					// ATMarketData mkt = new ATMarketData(ulMkt);
					ATEquityDescription dsc = new ATEquityDescription(ulFnd);
					mktMap.put(symbol, ulMkt);
					fndMap.put(symbol, fnd);
					dscMap.put(symbol, dsc);

					ATStrategyGroup stratGroup = new ATStrategyGroup();
					stratGroup.symbol = symbol;
					stratGroup.strategies = symbolStrategies.toArray(new ATStrategy[0]);
					stratGroups.add(stratGroup);
				}
				log.debug(String.format("Options [%s/%3d]", symbol, mktOptions.size()));
			}
			ATStrategyPack pack = new ATStrategyPack(stratGroups, dscMap, fndMap, mktMap);

			if (false) {

				// @Override
				// public JsonObject getOptionSpreads(String userId_, Set<String> symbols_, TdnOptionRersearchConfig config_) {
				log.debug("getNewSpreads");
				Map<String, ATPersistentEquityFoundation> foundations = _foundationProvider.getFoundationMap();
				if (config_ != null) {
					config_.cleanup();
				}

				log.info(String.format("Generating spread opportunities [avg: %s; apy: %s] - %d", config_.getAvgMode(), config_.getSpreadApyMode(),
						equities_.size()));

				long throttle = 0;
				long maxThrottle = 1000;

				JsonObject root = new JsonObject();
				JsonArray dataArray = new JsonArray();
				root.add("strategies", dataArray);
				root.addProperty("ulSize", equities_.size());

				// IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();

				Date now = new Date();
				SimpleDateFormat yyymmddFormat = new SimpleDateFormat("yyyy-MM-dd");
				for (TdnSecurity eqyity : equities_) {
					String symbol = eqyity.id;
					try {
						ATPersistentSecurity stock = null;// -- _securitiesProvider.get(symbol);
						ATPersistentEquityFoundation foundation = foundations.get(symbol);
						// -- if (!validateStock(stock, foundation, config_.getPxMin()))
						// continue;
						// -- List<ATPersistentSecurity> options = getOptionChain(symbol, config_.getEquityMaxAge());
						TreeSet<ATVerticalSpread> spreads = null;// -- T analyzeSpreads(stock, options, foundation, symbols_, config_, false);
						if (spreads == null || spreads.isEmpty())
							continue;
						Double px = stock.getPrice();
						if (px == null)
							continue;
						double dailyDelta = (px - stock.getPriceClose()) / stock.getPriceClose();

						// TdnSecurity ul2 = new TdnSecurity();
						// ul2.id = stock.getOccId();
						// ul2.name = foundation.getDescription();
						// ul2.px = px;
						// ul2.pxClose=stock.getPriceClose();
						// ul2.add("pxChange", new JsonPrimitive(dailyDelta));
						// ul2.add("pxAsk", new JsonPrimitive(stock.getPriceAsk()));
						// ul2.add("pxBid", new JsonPrimitive(stock.getPriceBid()));
						// ul2.add("pxLoD", new JsonPrimitive(stock.getPriceLow()));
						// ul2.add("pxHiD", new JsonPrimitive(stock.getPriceHigh()));
						// ul2.add("pxAvg", new JsonPrimitive(foundation.getAvg(config_.eqtAvgMode)));
						// ul2.add("pxAvgMode", new JsonPrimitive(config_.eqtAvgMode));

						JsonObject ul = new JsonObject();
						ul.add("id", new JsonPrimitive(stock.getOccId()));
						ul.add("name", foundation.getDescription() != null ? new JsonPrimitive(foundation.getDescription()) : null);
						ul.add("px", new JsonPrimitive(px));
						ul.add("pxClose", new JsonPrimitive(stock.getPriceClose()));
						ul.add("pxChange", new JsonPrimitive(dailyDelta));
						ul.add("pxAsk", new JsonPrimitive(stock.getPriceAsk()));
						ul.add("pxBid", new JsonPrimitive(stock.getPriceBid()));
						ul.add("pxLoD", new JsonPrimitive(stock.getPriceLow()));
						ul.add("pxHiD", new JsonPrimitive(stock.getPriceHigh()));
						ul.add("pxAvg", new JsonPrimitive(foundation.getAvg(config_.getAvgMode())));
						ul.add("pxAvgMode", new JsonPrimitive(config_.getAvgMode()));
						Double hv = foundation.getHV(config_.getHVMode());
						if (hv != null) {
							ul.add("pxHV", new JsonPrimitive(hv));
						}
						ul.add("pxHVMode", new JsonPrimitive(config_.getHVMode()));
						ul.add("ts", new JsonPrimitive(stock.getUpdateTime()));
						Date earnDate = foundation.getEarningsDate();
						if (earnDate != null && now.before(earnDate)) {
							String earnDateStr = ATFormats.DATE_yyyy_MM_dd.get().format(earnDate);
							ul.add("dateEarn", new JsonPrimitive(earnDateStr));
						}
						Date exDivDate = foundation.getDividendExDate();
						// exDivDate = DateUtils.addDays(now, 5);
						if (exDivDate != null && now.before(exDivDate)) {
							String exDivStr = ATFormats.DATE_yyyy_MM_dd.get().format(exDivDate);
							ul.add("dateDiv", new JsonPrimitive(exDivStr));
							Double div = foundation.getDividend();
							// div = 4.5;
							if (div != null) {
								ul.add("divAmt", new JsonPrimitive(div));
							}
						}
						// String note = foundation.getNote();
						// if (StringUtils.isNotBlank(note)) {
						// ul.add("note", new JsonPrimitive(note));
						// }
						if (userOccIds.contains(stock.getOccId())) {
							ul.add("owned", new JsonPrimitive(true));
						}
						int ulCredits = 0;
						if (stock.getPrice() < foundation.getSma60())
							ulCredits += 1;
						JsonArray strategyArray = new JsonArray();
						// ul.add("strategies", strategyArray);
						for (ATVerticalSpread spread : spreads) {
							if (throttle++ > maxThrottle)
								break;
							int stratCredits = ulCredits;
							double strikeWidth = spread.getStrikeSell() - spread.getStrikeBuy();
							if (strikeWidth <= 2.5)
								stratCredits += 1;
							if (spread.getSafetyMargin() > 10)
								stratCredits += 1;
							JsonObject strategy = new JsonObject();
							strategyArray.add(strategy);
							strategy.add("type", new JsonPrimitive("sprd-v-bull-put"));
							strategy.add("yldAbs", new JsonPrimitive(spread.getPriceSpread()));
							strategy.add("yldPctA", new JsonPrimitive(spread.getApy()));
							strategy.add("score", new JsonPrimitive(spread.getScore()));
							strategy.add("prb", new JsonPrimitive(spread.getProbability()));
							strategy.add("mrg", new JsonPrimitive(spread.getSafetyMargin()));
							strategy.add("strkWidth", new JsonPrimitive(strikeWidth));
							strategy.add("credits", new JsonPrimitive(stratCredits));
							JsonArray optionArray = new JsonArray();
							strategy.add("options", optionArray);
							if (strategyArray.size() == 0)
								continue;
							JsonObject o1 = new JsonObject();
							JsonObject o2 = new JsonObject();
							optionArray.add(o1);
							optionArray.add(o2);
							String expiryStr = yyymmddFormat.format(spread.getExpirationDate());
							o1.add("exp", new JsonPrimitive(expiryStr));
							o2.add("exp", new JsonPrimitive(expiryStr));
							o1.add("strike", new JsonPrimitive(spread.getStrikeBuy()));
							o2.add("strike", new JsonPrimitive(spread.getStrikeSell()));
							o1.add("count", new JsonPrimitive(-1));
							o2.add("count", new JsonPrimitive(+1));
							o1.add("px", spread.getPxBuyLast() != null ? new JsonPrimitive(spread.getPxBuyLast()) : null);
							o2.add("px", spread.getPxSellLast() != null ? new JsonPrimitive(spread.getPxSellLast()) : null);
							o1.add("pxAsk", new JsonPrimitive(spread.getPxBuyAsk()));
							o2.add("pxAsk", new JsonPrimitive(spread.getPxSellAsk()));
							o1.add("pxBid", new JsonPrimitive(spread.getPxBuyBid()));
							o2.add("pxBid", new JsonPrimitive(spread.getPxSellBid()));
							o1.add("pxLoD", spread.getPxBuyLoD() != null ? new JsonPrimitive(spread.getPxBuyLoD()) : null);
							o2.add("pxLoD", spread.getPxSellLoD() != null ? new JsonPrimitive(spread.getPxSellLoD()) : null);
							o1.add("pxHiD", spread.getPxBuyHiD() != null ? new JsonPrimitive(spread.getPxBuyHiD()) : null);
							o2.add("pxHiD", spread.getPxSellHiD() != null ? new JsonPrimitive(spread.getPxSellHiD()) : null);
							if (userOccIds.contains(spread.getOccIdBuy())) {
								o1.add("owned", new JsonPrimitive(true));
							}
							if (userOccIds.contains(spread.getOccIdSell())) {
								o2.add("owned", new JsonPrimitive(true));
							}
						}
						JsonObject idea = new JsonObject();
						idea.add("ul", ul);
						idea.add("strategies", strategyArray);
						dataArray.add(idea);

					} catch (Exception ex) {
						log.error(String.format("Error processing [%s]", symbol), ex);
					}
				}
			}
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error"), ex);
			return null;
		}
	}

	private List<ATStrategy> analyzeOptions(ATPersistentEquityFoundation fnd_, IATMarketData ul_, List<IATMarketData> options_,
			ATSecurityResearchConfig config_) {
		if (fnd_ == null || ul_ == null || options_ == null || options_.isEmpty() || config_ == null)
			return null;
		Double avgMarginMax = config_.getAvgMarginMax();
		Double avgMarginMin = config_.getAvgMarginMin();
		String avgMode = config_.getAvgMode();
		Double avg = null;
		if (avgMode != null && (avgMarginMax != null || avgMarginMin != null)) {
			avg = fnd_.getAvg(avgMode);
			if (avg == null || avg == 0) {
				log.warn(String.format("Missing average [%s/%s]", fnd_.getOccId(), avgMode));
				return null;
			}
		}
		int century = Year.now().getValue() / 100;
		Map<String, OptionStrategyToken> optionMap = new HashMap<>();
		for (IATMarketData mkt : options_) {
			IATOptionDescription option = ATOptionDescription.create(mkt.getAtId());
			Double margin = null;
			if (avg != null) {
				margin = Math.abs((avg - option.getStrike()) / avg);
				if (avgMarginMax < margin)
					continue;
				if (avgMarginMin > margin)
					continue;
			}
			LocalDate expDate = LocalDate.parse(ATFormats.ds6Tods10(option.getExpirationDS6(), century));
			Long daysToExp = ChronoUnit.DAYS.between(LocalDate.now(), expDate);
			ATStrategy strat = new ATStrategy();
			strat.symbol = fnd_.getOccId();
			strat.earningsDS8 = fnd_.getEarningsDS8();
			strat.margin = margin;
			strat.daysToExp = daysToExp;
			OptionStrategyToken ph = new OptionStrategyToken(mkt, option, strat);
			optionMap.put(option.getOccId(), ph);
		}
		List<ATStrategy> result = analyzeStrategies(optionMap.values(), config_);
		// = new ArrayList<>();
		// for (String occId : optionMap.keySet()) {
		// OptionStrategyToken ph = optionMap.get(occId);
		// result_digest.add(ph.strat);
		// }
		return result;
	}

	List<ATStrategy> analyzeStrategies(Collection<OptionStrategyToken> tokens_, ATSecurityResearchConfig config_) {
		List<ATStrategy> result = new ArrayList<>();
		for (OptionStrategyToken token : tokens_) {
			Double minAPY = config_.getStrategyApyMin();
			if (minAPY != null) {
				Double px = IATMarketData.getPx(token.mkt, IATAssetConstants.MS_1D);
				Double apy = ATMathUtil.calcApy(token.option.getStrike(), px, token.strat.daysToExp);
				if (apy != null && apy >= minAPY) {
					ATStrategy strat = token.strat.clone();
					strat.type = IATOptionDescription.Right.Put == token.option.getRight() ? ATStrategy.TYPE_PUT : ATStrategy.TYPE_CALL;
					strat.apy = apy;
					strat.px = px;
					result.add(strat);
				}
			}
		}
		return result;
	}

	private List<TdnSecurity> getEquities(String uid_, ATSecurityResearchConfig config_) {
		// 1. Collect OCC ids
		Set<String> occIds = new TreeSet<>();
		Map<String, ATSymbol> symbolMap = ATSourceServiceRegistry.getSymbolProvider().getSymbols();
		occIds.addAll(symbolMap.keySet());
		Set<String> userOccIds = ATSourceServiceRegistry.getPortfolioService().getActiveOccIds(uid_, true);
		Set<String> userSymbols = userOccIds.stream().map(i -> i.length() > 6 ? i.substring(0, 6).trim() : i).collect(Collectors.toSet());
		if (config_.isOwned()) {
			occIds.retainAll(userSymbols);
		}
		IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		if (!occIds.isEmpty() && config_.hasAlerts()) {
			List<ATUserAlertRulePack> alerts = userSvc.getUserAlerts(uid_, null);
			Set<String> alertSymbols = alerts.stream().map(a -> a.occId).collect(Collectors.toSet());
			occIds.retainAll(alertSymbols);

		}
		if (!occIds.isEmpty() && config_.hasNotes()) {
			List<ATUserNote> notes = userSvc.getUserNotes(uid_, null);
			Set<String> noteSymbols = notes != null ? notes.stream().map(n -> n.getOccId()).collect(Collectors.toSet()) : null;
			occIds.retainAll(noteSymbols);
		}
		if (occIds.isEmpty())
			return null;

		// 2. Generate data
		String avgMode = config_.getAvgMode();

		Double pxMax = config_.getPxMax();
		Double pxMaxChg = config_.getPxMaxChange();
		Double pxMin = config_.getPxMin();
		Double pxMinChg = config_.getPxMinChange();
		Double avgMrgMax = config_.getAvgMarginMax();
		Double avgMrgMin = config_.getAvgMarginMin();
		boolean isReqPx = pxMax != null || pxMin != null || pxMaxChg != null || pxMinChg != null || avgMrgMax != null || avgMrgMin != null;

		String hvMode = config_.getHVMode();
		Double hvMax = config_.getHVMax();
		Double hvMin = config_.getHVMin();
		boolean isReqHV = hvMode != null && (hvMax != null || hvMin != null);

		List<String> configSymbols = config_.getSymbols();
		if (configSymbols != null && configSymbols.isEmpty())
			configSymbols = null;

		Long maxAge = config_.getEquityMaxAge();
		Long tsMin = maxAge != null ? System.currentTimeMillis() - maxAge : null;

		String nowDS8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		IATEquityFoundationProvider fndSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		List<TdnSecurity> results = new ArrayList<>();
		for (String occId : occIds) {
			try {
				if (configSymbols != null && !configSymbols.contains(occId))
					continue;
				IATEquityFoundation fnd = fndSvc.getFoundation(occId);
				// ATFundamental fd = new ATFundamental(ef, avgMode, hvMode);
				// ATEquityDescription dsc = new ATEquityDescription(ef);

				IATMarketData mkt = mktSvc.getMarketData(occId);
				Double pxMid = IATMarketData.getPxMid(mkt);
				if (pxMid == null && isReqPx)
					continue;
				if ((pxMax != null && pxMid > pxMax) || (pxMin != null && pxMid < pxMin))
					continue;

				Long tsLast = mkt != null ? mkt.getTsLast() : null;
				if (tsMin != null && (tsMin == null || tsLast < tsMin))
					continue;

				IATSymbol sym = symbolMap.get(occId);
				TdnSecurity sec = TdnConverter.toTdnSecurity(occId, sym, mkt, fnd, avgMode, hvMode, nowDS8);
				if (sec == null)
					continue;
				if (userOccIds.contains(occId)) {
					sec.owned += 1.0;
				}
				if (userOccIds.stream().anyMatch(i -> i.length() > 6 && i.startsWith(occId))) {
					sec.owned += 0.5;
				}
				if (sec.px != null && sec.px != 0) {
					if (sec.pxClose != null) {
						sec.pxChg = (sec.px - sec.pxClose) / sec.px;
						if (pxMaxChg != null && sec.pxChg > pxMaxChg)
							continue;
						if (pxMinChg != null && sec.pxChg < pxMinChg)
							continue;
					}
					if (sec.pxAvg != null) {
						double pxAvgChg = (sec.px - sec.pxAvg) / sec.px;
						if (avgMrgMax != null && pxAvgChg > avgMrgMax)
							continue;
						if (avgMrgMin != null && pxAvgChg < avgMrgMin)
							continue;
					}
				}
				if (sec.pxHV != null && isReqHV) {
					if (hvMax != null && sec.pxHV > hvMax)
						continue;
					if (hvMin != null & sec.pxHV < hvMin)
						continue;
				}

				// if (results.size() > 25)
				// break;

				results.add(sec);

			} catch (Exception ex) {
				log.error(String.format("Error processing [%s]", occId), ex);
			}
		}
		log.info(String.format("Security research [%d/%s]", results.size(), uid_));
		return results;
	}

	@Override
	public TdnEquityResearchResponse getSecurities(String uid_, boolean underlyingOnly_, TdnEquityResearchConfig config_) {

		Collection<ATPersistentSecurity> securities = _stockMap.values();
		if (!underlyingOnly_) {
			securities.addAll(_optionMap.values());
		}
		log.debug(String.format("Securities count [%d]", securities.size()));
		// Set<String> symbols = null;
		if (config_ != null) {
			config_.cleanup();
		}

		// 1. Collect OCC Ids
		Map<String, ATSymbol> symbolMap = ATSourceServiceRegistry.getSymbolProvider().getSymbols();
		Set<String> occIds = new TreeSet<>();
		if (config_.eqtIsOwned) {
			Set<String> userOccIds = ATSourceServiceRegistry.getPortfolioService().getActiveOccIds(uid_, true);
			Set<String> userSymbols = userOccIds.stream().map(i -> i.length() > 6 ? i.substring(0, 6).trim() : i).collect(Collectors.toSet());
			occIds.addAll(userSymbols);
			if (!underlyingOnly_) {
				occIds.addAll(userOccIds);
			}
		} else {
			occIds.addAll(symbolMap.keySet());
		}
		IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		if (config_.eqtHasAlerts) {
			List<ATUserAlertRulePack> alerts = userSvc.getUserAlerts(uid_, null);
			Set<String> alertOccIds = alerts.stream().map(a -> a.occId).collect(Collectors.toSet());
			occIds.retainAll(alertOccIds);
		}
		if (config_.eqtHasNotes) {
			List<ATUserNote> notes = userSvc.getUserNotes(uid_, null);
			Set<String> noteOccIds = notes != null ? notes.stream().map(n -> n.getOccId()).collect(Collectors.toSet()) : null;
			occIds.retainAll(noteOccIds);
		}
		if (occIds.isEmpty() && false)
			return null;

		// 2. Generate data
		String avgMode = config_.eqtAvgMode;
		String hvMode = config_.eqtHVMode;
		String nowDS8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		IATEquityFoundationProvider fndSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		List<TdnSecurity> results = new ArrayList<>();
		for (String occId : occIds) {
			try {
				ATPersistentEquityFoundation ef = fndSvc.getFoundation(occId);
				IATMarketData mkt = mktSvc.getMarketData(occId);
				IATSymbol sym = symbolMap.get(occId);
				TdnSecurity sec = TdnConverter.toTdnSecurity(occId, sym, mkt, ef, avgMode, hvMode, nowDS8);
				if (sec != null) {
					results.add(sec);
					if (results.size() > 25)
						break;
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing [%s]", occId), ex);
			}
		}
		log.info(String.format("Security research [%d/%s]", results.size(), uid_));

		// Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		// TreeSet<ATPersistentPosition> allPositions = ATSourceServiceRegistry.getPositionProcessor().getPositions(uid_);
		// HashMap<String, Set<ATPersistentPosition>> posMap = new HashMap<>();
		// for (ATPersistentPosition pos : allPositions) {
		// String ulId = underlyingOnly_ ? (pos.isOption() ? pos.getUnderlyingOccId() : pos.getOccId()) : pos.getOccId();
		// Set<ATPersistentPosition> ulPos = posMap.get(ulId);
		// if (ulPos == null) {
		// ulPos = new HashSet<>();
		// posMap.put(ulId, ulPos);
		// }
		// ulPos.add(pos);
		// }
		// Map<String, String> notesMap = ATSourceServiceRegistry.getNotesProcessor().getUserNotes(uid_);

		// List<TdnSecurity> resultEquities = new ArrayList<>();
		//
		// for (ATPersistentSecurity sec : securities) {
		// // if ("INTC".equals(stock.getOccId())) {
		// // log.debug("HELLO!");
		// // }
		// try {
		// String occId = sec.getOccId();
		// // Test Owned
		// Set<ATPersistentPosition> positions = posMap.get(sec.getOccId());
		// if (config_.eqtIsOwned && positions == null)
		// continue;
		// // Test price
		// if (sec.getPrice() == null || config_.eqtPxMin > sec.getPrice() || config_.eqtPxMax < sec.getPrice())
		// continue;
		// // Test symbol
		// if (config_.symbols != null && !config_.symbols.contains(sec.getOccId()))
		// continue;
		// ATPersistentEquityFoundation fnd = _foundationProvider.getFoundation(occId);
		// if (fnd == null) {
		// log.error(String.format("No Foundation for [%s]", occId));
		// continue;
		// }
		// // Test Notes
		// if (config_.eqtHasNotes && !notesMap.containsKey(occId))
		// continue;
		// Double pxCh = sec.getPriceClose() != null ? (sec.getPrice() - sec.getPriceClose()) / sec.getPriceClose() : null;
		// if (pxCh != null && config_.eqtPxChgMinD != null && config_.eqtPxChgMaxD != null && config_.eqtPxChgMinD > config_.eqtPxChgMaxD) {
		// // If cfgMin > cfgMax effectively make it an asymmetric 'abs'
		// if (pxCh < config_.eqtPxChgMinD && pxCh > config_.eqtPxChgMaxD)
		// continue;
		// } else {
		// if (config_.eqtPxChgMaxD != null && (pxCh == null || pxCh > config_.eqtPxChgMaxD))
		// continue;
		// if (config_.eqtPxChgMinD != null && (pxCh == null || pxCh < config_.eqtPxChgMinD))
		// continue;
		// }
		// // Test Avg Margins
		// Double avg = config_.eqtAvgMode != null ? fnd.getAvg(config_.eqtAvgMode) : null;
		// if (avg != null && avg != 0) {
		// Double avgMrg = 100.0 * (sec.getPrice() - avg) / avg;
		// if (avgMrg < config_.eqtAvgMin || avgMrg > config_.eqtAvgMax)
		// continue;
		// }
		// Double pxHV = config_.eqtHVMode != null ? fnd.getHV(config_.eqtHVMode) : null;
		// if (pxHV != null && !Double.isNaN(pxHV)) {
		// Double hv = 100.0 * pxHV;
		// if (hv < config_.eqtHVMin || hv > config_.eqtHVMax)
		// continue;
		// }
		//
		// TdnSecurity er = TdnConverter.toTdnSecurity(sec, fnd, positions, config_.eqtAvgMode, config_.eqtHVMode, nowDS8);
		// resultEquities.add(er);
		// } catch (Exception ex) {
		// log.error(String.format("Error analyzing [%s]", sec.getOccId()), ex);
		// }
		// }

		TdnEquityResearchResponse result = new TdnEquityResearchResponse();
		result.equities = results.toArray(new TdnSecurity[0]);
		// result_digest.equities = resultEquities.toArray(new TdnSecurity[0]);

		return result;// root; //new Gson().toJson(root);
	}

	@Override
	public Set<TdnSymbolLookup> lookup(String symbol_) {
		Set<TdnSymbolLookup> result = new TreeSet<>();
		if (StringUtils.isBlank(symbol_))
			return result;

		String symbol = symbol_.toUpperCase();
		for (String sym : _stockMap.keySet()) {
			ATPersistentEquityFoundation fnd = _foundationProvider.getFoundation(sym);
			String name = fnd != null ? fnd.getDescription() : null;
			if (sym.contains(symbol) || (name != null && name.toUpperCase().contains(symbol))) {
				TdnSymbolLookup lookup = new TdnSymbolLookup();
				lookup.symbol = sym;
				result.add(lookup);
			}
		}
		for (String sym : _indexMap.keySet()) {
			ATPersistentSecurity idx = _indexMap.get(sym);
			String name = idx.getDescription();
			if (sym.contains(symbol) || (name != null && name.toUpperCase().contains(symbol))) {
				TdnSymbolLookup lookup = new TdnSymbolLookup();
				lookup.symbol = sym;
				result.add(lookup);
			}
		}

		return result;
	}

	@Override
	@Deprecated
	public TdnSecurityResponse getDetails(String occId_, List<String> flags_, String uid_) {
		ATPersistentSecurity psec;
		ATPersistentEquityFoundation fnd = null;
		// if ("SUN".equals(occId_)) {
		// log.debug(occId_);
		// }
		Set<ATPersistentPosition> positions = null;
		if (occId_.startsWith("$")) {
			psec = _indexMap.get(occId_);
		} else {
			psec = _stockMap.get(occId_);
			fnd = _foundationProvider.getFoundation(occId_);
			if (flags_ != null && flags_.contains("pos")) {
				if (StringUtils.isBlank(occId_) || psec == null) {
					log.error(String.format("Missing security [%s]", occId_));
					return null;
				} else if (fnd == null) {
					log.error(String.format("Missing fundamentals [%s]", occId_));
				}
				TreeSet<ATPersistentPosition> allPositions = ATSourceServiceRegistry.getPositionProcessor().getPositions(uid_);
				positions = new TreeSet<>();
				for (ATPersistentPosition pos : allPositions) {
					if (occId_.equals(pos.getOccId()) || occId_.equals(pos.getUnderlyingOccId())) {
						positions.add(pos);
					}
				}
			}
		}
		TdnSecurityResponse response = new TdnSecurityResponse();
		if (psec == null) {
			log.error(String.format("Security not found [%s]", occId_));
			return response;
		}

		response.hvMode = IATStatistics.HV60;
		response.avgMode = IATStatistics.SMA60;

		boolean isHistory = flags_ != null && (flags_.contains("ohlc") || flags_.contains("close"));
		if (isHistory) {
			boolean isAvg = flags_.contains("avg");
			boolean isHV = flags_.contains("hv");
			boolean isCloseOnly = flags_.contains("close") && !flags_.contains("ohlc");
			boolean isVolume = flags_.contains("vol");

			String historyTableId = _config.getString("historyTableId");
			Date startDate = DateUtils.addYears(new Date(), -2);
			String startDateStr = ATFormats.DATE_yyyyMMdd.get().format(startDate);
			IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, occId_).and().gte(IATAssetConstants.DATE_STR,
					startDateStr);
			List<ATPriceHistory> history = _dao.query(historyTableId, query, ATPriceHistory.class);
			List<Double> avg = new ArrayList<>();
			List<Double> hv = new ArrayList<>();
			if (history != null) {
				List<TdnOHLCV> histList = new ArrayList<>();
				for (ATPriceHistory hist : history) {
					TdnOHLCV ohlcv = TdnConverter.toTdnOHLCV(hist, isCloseOnly, isVolume);
					if (ohlcv != null) {
						histList.add(ohlcv);
					}
					if (isAvg) {
						avg.add(hist.getAvg(response.avgMode));
					}
					if (isHV) {
						hv.add(hist.getHV(response.hvMode));
					}
				}
				response.history = histList.toArray(new TdnOHLCV[0]);
				response.avg = avg.toArray(new Double[0]);
				response.hv = hv.toArray(new Double[0]);
			}
		}
		try {
			String nowDS8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
			TdnSecurity sec = TdnConverter.toTdnSecurity(psec, fnd, positions, TdnEquityResearchConfig.getDefaultAvgMode(),
					TdnEquityResearchConfig.getDefaultHVMode(), nowDS8);
			response.security = sec;
		} catch (Exception ex) {
			log.error(String.format("Error processing [%s]", occId_), ex);
		}
		return response;
	}

	// ------------------

	final class OptionStrategyToken {
		public OptionStrategyToken(IATMarketData mkt_, IATOptionDescription option_, ATStrategy strat_) {
			mkt = mkt_;
			option = option_;
			strat = strat_;
		}

		IATMarketData mkt;
		IATOptionDescription option;
		ATStrategy strat;
	}

}
