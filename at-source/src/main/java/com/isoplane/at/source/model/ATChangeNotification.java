package com.isoplane.at.source.model;

import java.util.Map;

import org.apache.commons.lang3.time.DateUtils;

import com.isoplane.at.commons.model.ATBaseNotification;

public class ATChangeNotification extends ATBaseNotification {

	private String symbol;
	private Double changePct;
	private String period;
	private Double price;

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public Double getChangePct() {
		return changePct;
	}

	public void setChangePct(Double changePct) {
		this.changePct = changePct;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	@Override
	public String toString() {
		// String str = String.format("%s", getSymbol());
		String str = String.format("%s %+6.2f%% (%.2f)", getSymbol(), getChangePct(), getPrice());
		return str;
	}

	@Override
	public String getNotificationKey() {
		String key = String.format("pxch %s", getSymbol());
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATChangeNotification oldCh = oldB != null && oldB instanceof ATChangeNotification ? (ATChangeNotification) oldB
				: null;

		// http://csbruce.com/software/utf-8.html
		String arrowStr = "✧"; // ▷◇⋄
		if (oldCh != null && DateUtils.isSameDay(oldCh.getDate(), this.getDate())) {
			double change = (oldCh.getChangePct() - this.getChangePct()) / oldCh.getChangePct();
			double threshold = this.isActive() ? 0.15 : 0.5;
			if (Math.abs(change) > threshold) {
				arrowStr = oldCh.getPrice() < this.getPrice() ? "➚" : "➘"; // △▽◥◢
			} else {
				return null;
			}
		}

		String title = String.format("%1s %2$s %3$.2f %4$+7.2f%%", this.getSymbol(), arrowStr, this.getPrice(),
				this.getChangePct()); // Price ⚠💰
		String body = String.format("⇅  %s", this.getLabel().replaceAll(" +", " "));
//		String body = String.format("⇅  %s \n· alphatra.de · %tT", this.getLabel().replaceAll(" +", " "), new Date());
		String iconStar = this.isActive() ? "_star" : "";
		String iconArrow = this.getChangePct() == 0 ? "" : (this.getChangePct() < 0 ? "_down" : "_up");
		String icon = String.format("/globe%s%s.ico", iconStar, iconArrow);

		return new ATNotificationContent(title, body, icon);
	}

	@Override
	public String getNotificationTag() {
		return String.format("change_%s", getSymbol());
	}

	@Override
	public String getType() {
		return "change";
	}
}
