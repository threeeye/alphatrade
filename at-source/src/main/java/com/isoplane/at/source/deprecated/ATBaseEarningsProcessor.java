package com.isoplane.at.source.deprecated;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import org.apache.http.annotation.Obsolete;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.source.adapter.IATEarningsAdapter;
import com.isoplane.at.source.model.ATEarningOld;

@Obsolete
public class ATBaseEarningsProcessor {

	static final Logger log = LoggerFactory.getLogger(ATBaseEarningsProcessor.class);

	private ATStore _store;
	private IATEarningsAdapter _adapter;

	public ATBaseEarningsProcessor(ATStore store_, IATEarningsAdapter adapter_) {
		_store = store_;
		_adapter = adapter_;
	}

	// Daily update
	public boolean updateEarnings(Collection<ATStock> securities_) {
		try {
			Map<String, TreeSet<ATEarningOld>> newEarningsMap = _adapter.getEarnings(securities_);
			if (newEarningsMap == null)
				return false;
			int delCount = _store.deleteFuture(ATEarningOld.class, newEarningsMap.keySet());
			log.debug(String.format("Deleted [%d] future earnings", delCount));
			for (String occId : newEarningsMap.keySet()) {
				TreeSet<ATEarningOld> earnings = newEarningsMap.get(occId);
				for (ATEarningOld earning : earnings) {
					_store.put(earning);
				}
			}
			_store.refreshEvents();
			return true;
		} catch (Exception ex) {
			log.error(String.format("Error updating earings"), ex);
			return false;
		}
	}

}
