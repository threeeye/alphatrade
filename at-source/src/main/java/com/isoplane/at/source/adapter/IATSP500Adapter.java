package com.isoplane.at.source.adapter;

import java.util.List;
import java.util.Set;

import com.isoplane.at.commons.model.ATStock;

public interface IATSP500Adapter {

	Set<ATStock> getSP500();

	boolean processSP500(List<String> constituents);
}
