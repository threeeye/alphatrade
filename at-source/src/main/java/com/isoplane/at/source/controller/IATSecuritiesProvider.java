package com.isoplane.at.source.controller;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.isoplane.at.commons.model.ATSecurityResearchConfig;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.source.model.TdnEquityResearchConfig;
import com.isoplane.at.source.model.TdnEquityResearchResponse;
import com.isoplane.at.source.model.TdnSecurityResponse;
import com.isoplane.at.source.model.TdnSymbolLookup;

public interface IATSecuritiesProvider extends IATService {

	int put(ATPersistentSecurity... securities);

	Collection<ATPersistentSecurity> getOptionChain(String occId);

	ATPersistentSecurity get(String occId);

	List<ATPersistentSecurity> get(String atid, boolean includeOptions);

	Set<String> getStockIds();

	TdnEquityResearchResponse getSecurities(String uid, boolean underlyingOnly, TdnEquityResearchConfig cfg);

	TdnSecurityResponse getDetails(String occId, List<String> flags, String uid);

	Set<TdnSymbolLookup> lookup(String symbol);

	TdnEquityResearchResponse getSecurities(String userId, ATSecurityResearchConfig config);

}
