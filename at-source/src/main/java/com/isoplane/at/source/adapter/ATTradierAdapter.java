package com.isoplane.at.source.adapter;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.ATLegacyDividend;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.model.IATQuote;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.source.model.ATConfiguration;
import com.isoplane.at.source.model.ATEarningOld;
import com.isoplane.at.source.model.ATPriceHistoryOld;
import com.isoplane.at.source.model.ATSystemStatus;

public class ATTradierAdapter implements IATEarningsAdapter, IATDividendAdapter, IATOptionsAdapter, IATPricingAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierAdapter.class);

	static final List<Integer> _validEarnings = Arrays.asList(7, 8, 9, 10);

	private String _urlBase = "https://api.tradier.com";
	private long _lastProcessTime = 0;
	private long _processPeriod = 500;
	static private Gson _gson = new Gson();
	private int _maxSize = 100;

	private ATConfiguration _config;

	public ATTradierAdapter(ATConfiguration config_) {
		_config = config_;
	}

	private void getMarketStatus() {
		try {
			throttle();
			String jsonStr = getHttp(_urlBase + "/v1/markets/clock");
			JsonObject root = _gson.fromJson(jsonStr, JsonObject.class).getAsJsonObject();
			JsonObject clock = root.getAsJsonObject("clock");
			switch (clock.get("state").getAsString()) {
			case "closed":
				_config.setMarketStatus(666);
				break;
			case "open":
				_config.setMarketStatus(0);
				break;
			case "postmarket":
				_config.setMarketStatus(1);
				break;
			case "premarket":
				_config.setMarketStatus(-1);
				break;
			}
		} catch (Exception ex) {
			log.error(String.format("Error getting market status"), ex);
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
	}

	public List<IATQuote> getPricing(Collection<? extends ATSecurity> securities_) {
		getMarketStatus();
		try {
			if (securities_ == null || securities_.isEmpty())
				return null;
			List<IATQuote> result = new ArrayList<>();
			int maxSize = 250;
			Set<String> occIds = new TreeSet<>();
			for (ATSecurity sec : securities_) {
				occIds.add(sec.getOccId().replace(" ", "")); // Note: No spaces for Tradier request
			}
			List<List<String>> partitions = Lists.partition(new ArrayList<>(occIds), maxSize);
			for (List<String> list : partitions) {
				throttle();
				String ids = String.join(",", list);
				log.debug(String.format("Pricing   [%s]", ids));
				String url = _urlBase + "/v1/markets/quotes?symbols=" + ids;
				String jsonStr = getHttp(url);
				// log.info(String.format("Data [%s]: %s", ids, jsonStr));
				List<IATQuote> pricing = parsePricing(jsonStr);
				if (pricing != null) {
					result.addAll(pricing);
				} else {
					log.error(String.format("Error pricing [%s]", ids));
				}
			}
			// TODO: make this ATCounter
			long start = (Long) ATSystemStatus.Current().get(ATSystemStatus.StartupTime);
			long count = (Long) ATSystemStatus.Current().get(ATSystemStatus.PricingProcessedCount) + securities_.size();
			ATSystemStatus.Current().put(ATSystemStatus.PricingProcessedCount, count);
			ATSystemStatus.Current().put(ATSystemStatus.PricingThroughput,
					count * 1000.0 / (System.currentTimeMillis() - start));
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error pricing [%s]", securities_), ex);
			return null;
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
	}

	private List<IATQuote> parsePricing(String json_) {
		try {
			if (StringUtils.isBlank(json_))
				return null;
			JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
			JsonElement qe = root.get("quotes");
			if (!qe.isJsonObject()) {
				log.error(String.format("Pricing data error: %s", json_.trim()));
				return null;
			}
			List<IATQuote> result = new ArrayList<>();
			JsonObject quotes = qe.getAsJsonObject();
			JsonElement quote = quotes.get("quote");
			if (quote != null) {
				int marketStatus = _config.getMarketStatus();
				if (quote.isJsonArray()) {
					JsonArray qArray = quote.getAsJsonArray();
					for (JsonElement jsonElement : qArray) {
						if (jsonElement.isJsonObject()) {
							IATQuote q = ATTradierQuote.toQuote(jsonElement, marketStatus);
							if (q != null) {
								result.add(q);
							}
						}
					}
				} else if (quote.isJsonObject()) {
					IATQuote q = ATTradierQuote.toQuote(quote, marketStatus);
					if (q != null) {
						result.add(q);
					}
				}
			} else {
				log.error("No quotes");
				return null;
			}
			JsonElement unmatched = quotes.get("unmatched_symbols");
			if (unmatched != null) {
				log.warn("Unmatched symbols: " + unmatched);
			}
			return result;
		} catch (Exception exi) {
			if (json_.toLowerCase().contains("quota")) {
				log.error(String.format("Error pricing: %s", json_));
			} else {
				log.error(String.format("Error pricing: %s", json_), exi);
			}
			return null;
		}
	}

	@Override
	public TreeSet<ATOptionSecurity> getOptions(ATStock security_) {
		if (security_ == null || StringUtils.isBlank(security_.getSymbol())) {
			return null;
		} else {
			Map<String, TreeSet<ATOptionSecurity>> options = getOptions(Arrays.asList(security_));
			return options.get(security_.getSymbol());
		}
	}

	@Override
	public Map<String, TreeSet<ATOptionSecurity>> getOptions(Collection<ATStock> securities_) {
		Map<String, TreeSet<ATOptionSecurity>> result = new HashMap<String, TreeSet<ATOptionSecurity>>();
		try {
			if (securities_ == null || securities_.isEmpty())
				return result;
			for (ATStock stock : securities_) {
				try {
					log.info(String.format("Refreshing options [%s]", stock.getOccId()));
					throttle();
					TreeSet<ATOptionSecurity> options = new TreeSet<>();
					String urlExp = String.format("%s/v1/markets/options/expirations?symbol=%s", _urlBase,
							stock.getOccId());
					String expJson = getHttp(urlExp);
					List<String> dateStrs = extractDates(expJson);
					if (dateStrs == null || dateStrs.isEmpty()) {
						log.debug(String.format("Options   [%s - %s]", stock.getOccId(),
								expJson != null ? expJson.trim() : null));
						continue;
					}
					for (String dateStr : dateStrs) {
						throttle();
						Date expDate = ATFormats.DATE_yyyy_MM_dd.get().parse(dateStr);
						int days = (int) TimeUnit.DAYS.convert(expDate.getTime() - System.currentTimeMillis(),
								TimeUnit.MILLISECONDS);
						if (days > _config.getMaxOptionDays())
							break;
						String urlChain = String.format("%s/v1/markets/options/chains?symbol=%s&expiration=%s",
								_urlBase, stock.getOccId(), dateStr);
						String chainJson = getHttp(urlChain);
						TreeSet<ATOptionSecurity> dateOptions = populateOptions(chainJson, expDate);
						if (dateOptions != null) {
							options.addAll(dateOptions);
						}
					}
					log.debug(String.format("Options   [%-4s(%4d)]", stock.getOccId(), options.size()));
					if (!options.isEmpty()) {
						result.put(stock.getOccId(), options);
					}
				} catch (Exception ex) {
					log.error(String.format("Error in option chain [%s]", stock), ex);
				} finally {
					_lastProcessTime = System.currentTimeMillis();
				}
			}
		} catch (Exception ex) {
			log.error(String.format("Error in options [%s]", securities_), ex);
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
		return result;
	}

	@Override
	public TreeSet<ATLegacyDividend> getDividend(ATStock security_, boolean force_) {
		if (security_ == null || StringUtils.isBlank(security_.getSymbol())) {
			return null;
		} else {
			Map<String, TreeSet<ATLegacyDividend>> dividends = getDividends(Arrays.asList(security_));
			return dividends.get(security_.getSymbol());
		}
	}

	@Override
	synchronized public Map<String, TreeSet<ATLegacyDividend>> getDividends(Collection<ATStock> securities_) {
		Map<String, TreeSet<ATLegacyDividend>> result = new HashMap<>();
		try {
			if (securities_ == null || securities_.isEmpty())
				return result;
			Set<String> occIds = new TreeSet<>();
			for (ATSecurity sec : securities_) {
				occIds.add(sec.getOccId());
			}
			int count = 0;
			List<List<String>> partitions = Lists.partition(new ArrayList<>(occIds), _maxSize);
			for (List<String> list : partitions) {
				throttle();
				String ids = String.join(",", list);
				if (StringUtils.isBlank(ids))
					continue;
				log.info(String.format("Dividends [%s]", ids));
				String url = _urlBase + "/beta/markets/fundamentals/dividends?symbols=" + ids;
				String jsonStr = getHttp(url);
				Map<String, TreeSet<ATLegacyDividend>> dividendMap = parseDividends(jsonStr, list);
				result.putAll(dividendMap);
				count += dividendMap.size();
				// count += processDividends(securities_, jsonStr);
			}
			log.info(String.format("Retrieved [%d] dividends", count));
		} catch (Exception ex) {
			throw new RuntimeException(String.format("Error in dividends [%s]", securities_), ex);
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
		return result;
	}

	@Override
	public TreeSet<ATEarningOld> getEarnings(ATStock security_, boolean force_) {
		if (security_ == null || StringUtils.isBlank(security_.getSymbol())) {
			return null;
		} else {
			Map<String, TreeSet<ATEarningOld>> earnings = getEarnings(Arrays.asList(security_));
			return earnings.get(security_.getSymbol());
		}
	}

	@Override
	synchronized public Map<String, TreeSet<ATEarningOld>> getEarnings(Collection<ATStock> securities_) {
		Map<String, TreeSet<ATEarningOld>> result = new HashMap<String, TreeSet<ATEarningOld>>();
		try {
			if (securities_ == null || securities_.isEmpty())
				return result;
			Set<String> occIds = new TreeSet<>();
			for (ATSecurity sec : securities_) {
				occIds.add(sec.getOccId());
			}
			int count = 0;
			List<List<String>> partitions = Lists.partition(new ArrayList<>(occIds), _maxSize);
			for (List<String> list : partitions) {
				String ids = getJoinedSymbols(securities_);
				if (StringUtils.isBlank(ids))
					continue;
				throttle();
				log.info(String.format("Earnings  [%s]", ids));
				String url = _urlBase + "/beta/markets/fundamentals/calendars?symbols=" + ids;
				String jsonStr = getHttp(url);
				Map<String, TreeSet<ATEarningOld>> earningsMap = parseEarnings(jsonStr, list);
				result.putAll(earningsMap);
				count += earningsMap.size();
			}
			log.info(String.format("Retrieved [%d] earnings", count));
		} catch (Exception ex) {
			log.error(String.format("Error in earnings [%s]", securities_), ex);
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
		return result;
	}

	// @Override
	synchronized public boolean processPriceStats(Collection<ATStock> securities_) {
		try {
			String ids = getJoinedSymbols(securities_);
			if (StringUtils.isBlank(ids))
				return false;
			log.info(String.format("Px Stats  [%s]", ids));
			String url = _urlBase + "/beta/markets/fundamentals/statistics?symbols=" + ids;
			throttle();
			String jsonStr = getHttp(url);
			processPriceStats(securities_, jsonStr);
			return true;
		} catch (Exception ex) {
			throw new RuntimeException(String.format("Error in price stats [%s]", securities_), ex);
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
	}

	public TreeSet<ATPriceHistoryOld> getPriceHistory(String ticker_, Date start_, Date end_) {
		try {
			if (StringUtils.isBlank(ticker_))
				return null;
			log.info(String.format("Px Hist   [%s]", ticker_));
			Calendar cal = Calendar.getInstance();
			String startStr;
			String endStr;
			if (end_ == null) {
				endStr = ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime());
			} else {
				endStr = ATFormats.DATE_yyyy_MM_dd.get().format(end_);
			}
			if (start_ == null) {
				cal.add(Calendar.YEAR, -5);
				startStr = ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime());
			} else {
				startStr = ATFormats.DATE_yyyy_MM_dd.get().format(start_);
			}
			// endStr = "2013-04-21";
			String url = String.format("%s/v1/markets/history?symbol=%s&start=%s&end=%s&interval=daily", _urlBase,
					ticker_, startStr, endStr);
			throttle();
			String jsonStr = getHttp(url);
			TreeSet<ATPriceHistoryOld> result = getPriceHistory(ticker_, jsonStr);
			if (result == null) {
				log.warn(String.format("No px history for [%s: %s - %s]", ticker_, startStr, endStr));
			}
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error in price history [%s]", ticker_), ex);
			return null;
		} finally {
			_lastProcessTime = System.currentTimeMillis();
		}
	}

	private TreeSet<ATPriceHistoryOld> getPriceHistory(String ticker_, String json_) {
		// log.info(String.format("###### Px history [%s]: %s", ticker_, json_));
		TreeSet<ATPriceHistoryOld> result = new TreeSet<>();
		try {
			if (StringUtils.isNotBlank(json_)) {
				JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
				JsonElement history = root.get("history");
				if (history.isJsonNull()) {
					return null;
				}
				Calendar cal = Calendar.getInstance();
				DateUtils.truncate(cal, Calendar.DAY_OF_MONTH);
				cal.add(Calendar.DAY_OF_YEAR, 1);
				JsonElement days = history.getAsJsonObject().get("day");
				if (days.isJsonArray()) {
					for (JsonElement day : days.getAsJsonArray()) {
						ATPriceHistoryOld pxh = parsePriceHistory(ticker_, day);
						if (pxh == null || pxh.getDate() == null || pxh.getDate().after(cal.getTime()))
							continue;
						result.add(pxh);
					}
				} else {
					ATPriceHistoryOld pxh = parsePriceHistory(ticker_, days);
					if (pxh != null && pxh.getDate() != null && pxh.getDate().before(cal.getTime())) {
						result.add(pxh);
					}
				}
			}
		} catch (Exception ex) {
			log.error(String.format("Error in px history [%s]", json_), ex); // 2023-01-20
			return null;
		}
		return result;
	}

	private ATPriceHistoryOld parsePriceHistory(String ticker_, JsonElement json_) {
		if (StringUtils.isBlank(ticker_) || json_ == null)
			return null;
		ATTradierPricingHistory temp = _gson.fromJson(json_, ATTradierPricingHistory.class);
		if (temp != null) {
			try {
				ATPriceHistoryOld pxh = new ATPriceHistoryOld();
				pxh.setOccId(ticker_);
				pxh.setDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.date));
				pxh.setOpen(temp.open);
				pxh.setClose(temp.close);
				pxh.setLow(temp.low);
				pxh.setHigh(temp.high);
				pxh.setVolume(temp.volume);
				return pxh;
			} catch (Exception ex) {
				log.warn(String.format("Couldn't parse [%s]", temp), ex);
			}
		}
		return null;
	}

	private String getJoinedSymbols(Collection<ATStock> securities_) {
		if (securities_ == null || securities_.isEmpty())
			return null;
		Set<String> symbols = new TreeSet<>();
		for (ATSecurity sec : securities_) {
			symbols.add(sec.getSymbol());
		}
		String ids = String.join(",", symbols);
		return ids;
	}

	synchronized private void throttle() throws InterruptedException {
		long duration = System.currentTimeMillis() - _lastProcessTime;
		if (_processPeriod > duration) {
			Thread.sleep(_processPeriod - duration);
		}
	}

	private List<String> extractDates(String json_) {
		try {
			List<String> dates = new ArrayList<>();
			if (StringUtils.isNotBlank(json_)) {
				JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
				JsonElement ee = root.get("expirations");
				if (ee.isJsonNull() || !ee.isJsonObject()) {
					return dates;
				}
				JsonElement jdates = ee.getAsJsonObject().get("date");
				if (jdates.isJsonArray()) {
					JsonArray dateJson = jdates.getAsJsonArray();
					for (JsonElement dj : dateJson) {
						dates.add(dj.getAsString());
					}
				} else {
					dates.add(jdates.getAsString());
				}
			}
			return dates;
		} catch (Exception ex) {
			log.error(String.format("Error in dates [%s]", json_), ex);
			return null;
		}
	}

	private TreeSet<ATOptionSecurity> populateOptions(String json_, Date expDate_) {
		TreeSet<ATOptionSecurity> optionChain = new TreeSet<>();
		try {
			if (StringUtils.isNotBlank(json_)) {
				JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
				JsonElement e = root.get("options");
				if (e.isJsonNull())
					return null;
				JsonObject joptions = e.getAsJsonObject();
				JsonArray joptionArray = joptions.getAsJsonArray("option");
				for (JsonElement joption : joptionArray) {
					ATOptionSecurity option = new ATOptionSecurity();
					option.setExpirationDate(expDate_);
					if (populatePricing(option, joption)) {
						option.setChanged(true);
						option.setStockOccId(option.getSymbol());
						optionChain.add(option);
					}
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(String.format("Error in option chain [%s]", json_), ex);
		}
		return optionChain;
	}

	private static class ATTradierQuote implements IATQuote {

		private ATTradierSecurity _sec;
		private int _marketStatus;
		private String _occId;
		private String _symbol;
		// private String _label;
		private boolean _isOption;

		private static IATQuote toQuote(JsonElement json_, int marketStatus_) {
			try {
				ATTradierSecurity tempSec = _gson.fromJson(json_, ATTradierSecurity.class);
				ATTradierQuote quote = new ATTradierQuote();
				quote._sec = tempSec;
				quote._marketStatus = marketStatus_;

				// if (tempSec.symbol.equals("DD") || tempSec.symbol.startsWith("DOW") || "HPE".equals(tempSec.symbol)) {
				// log.info(String.format("DD/DOW/HPE: %s", json_.toString()));
				// }
				ATSecurity proto = ATModelUtil.toSecurity(tempSec.symbol);
				quote._isOption = proto instanceof ATOptionSecurity && StringUtils.isNotBlank(tempSec.underlying)
						&& tempSec.underlying.equals(tempSec.root_symbol) && tempSec.contract_size == 100;
				quote._occId = proto.getOccId();
				quote._symbol = proto.getSymbol();
				if (!quote._isOption && quote.getOccId().length() > 8) {
					log.info(String.format("Not an option [%s]", quote.getOccId()));
				}
				return quote;
			} catch (Exception ex) {
				log.error("Error generating quote", ex);
				return null;
			}
		}

		@Override
		public String getOccId() {
			return _occId;
		}

		@Override
		public String getSymbol() {
			return _symbol;
		}

		@Override
		public String getDescription() {
			return _sec.description;
		}

		@Override
		public Double getPrice() {
			Double result = null;
			if (_sec == null)
				return result;
			if (_marketStatus == 0 || _sec.ask == null || _sec.bid == null) {
				result = _sec.last;
			} else {
				result = (_sec.bid + _sec.ask) / 2.0;
			}
			return result;
		}

		@Override
		public Double getPriceAsk() {
			return _sec.ask;
		}

		@Override
		public Double getPriceBid() {
			return _sec.bid;
		}

		@Override
		public Double getPriceHigh() {
			return _sec.high;
		}

		@Override
		public Double getPriceLow() {
			return _sec.low;
		}

		@Override
		public Double getPriceLast() {
			return _sec.last;
		}

		@Override
		public Double getPriceOpen() {
			return _sec.open;
		}

		@Override
		public Double getPriceClose() {
			return (_marketStatus == 1 || _marketStatus == 666 ? _sec.close : _sec.prevclose);
		}

		@Override
		public Integer getVolume() {
			return _sec.volume;
		}

		@Override
		public Integer getSizeAsk() {
			return _sec.asksize;
		}

		@Override
		public Integer getSizeBid() {
			return _sec.bidsize;
		}

		@Override
		public Date getDateAsk() {
			return _sec.ask_date != null ? new Date(_sec.ask_date) : null;
		}

		@Override
		public Date getDateBid() {
			return _sec.bid_date != null ? new Date(_sec.bid_date) : null;
		}

		@Override
		public Date getDateLast() {
			return _sec.trade_date != null ? new Date(_sec.trade_date) : null;
		}

		@Override
		public boolean isOption() {
			return _isOption;
		}

		@Override
		public Double getOptionStrike() {
			return _sec.strike;
		}

		@Override
		public String getOptionExpirationType() {
			switch (_sec.expiration_type) {
			case "weeklys":
				return ATOptionSecurity.Weekly;
			case "standard":
				return ATOptionSecurity.Monthly;
			default:
				return null;
			}
		}

		@Override
		public Integer getOptionContractSize() {
			return _sec.contract_size;
		}

		@Override
		public String getOptionRight() {
			switch (_sec.option_type) {
			case "call":
				return ATOptionSecurity.Call;
			case "put":
				return ATOptionSecurity.Put;
			default:
				return null;
			}
		}

	}

	private class ATTradierSecurity {
		public String symbol;
		public Double ask;
		public Double bid;
		public Double close;
		public Double prevclose;
		public Double high;
		public Double last;
		public Double low;
		public Double open;
		public Integer volume;
		public Integer asksize;
		public Integer bidsize;
		public Long ask_date;
		public Long bid_date;
		public Long trade_date;
		public String description;

		// Options
		public Double strike;
		// public String expirationDate; => Given. No need to parse.
		public String expiration_type;
		public Integer contract_size;
		public String option_type;

		public String underlying;
		public String root_symbol;
	}

	private class ATTradierPricingHistory {
		// public String symbol;
		public String date;
		public Double open;
		public Double close;
		public Double low;
		public Double high;
		public Integer volume;
	}

	private boolean populatePricing(ATSecurity security_, JsonElement json_) {
		if (json_ == null)
			return false;
		// List<String> cl = Arrays.asList("FUSVX", "ICTG", "LLTC", "MPG", "MRGE", "SCHS");
		try {
			int status = _config.getMarketStatus();
			ATTradierSecurity temp = _gson.fromJson(json_, ATTradierSecurity.class);
			// if (cl.contains(temp.symbol)) {
			// log.info(temp.symbol);
			// }
			ATSecurity proto = ATModelUtil.toSecurity(temp.symbol);
			security_.setOccId(proto.getOccId());
			security_.setSymbol(proto.getSymbol());
			security_.setLabel(proto.getLabel());
			if (status == 0 || temp.ask == null || temp.bid == null) {
				security_.setPrice(temp.last);
			} else {
				security_.setPrice((temp.bid + temp.ask) / 2.0);
			}
			security_.setPriceAsk(temp.ask);
			security_.setPriceBid(temp.bid);
			security_.setPriceClose(status == 1 || status == 666 ? temp.close : temp.prevclose);
			security_.setPriceHigh(temp.high);
			security_.setPriceLast(temp.last);
			security_.setPriceLow(temp.low);
			security_.setPriceOpen(temp.open);
			security_.setVolume(temp.volume);
			security_.setSizeAsk(temp.asksize);
			security_.setSizeBid(temp.bidsize);
			security_.setDateAsk(temp.ask_date != null ? new Date(temp.ask_date) : null);
			security_.setDateBid(temp.bid_date != null ? new Date(temp.bid_date) : null);
			security_.setDateLast(temp.trade_date != null ? new Date(temp.trade_date) : null);
			// security_.setDescription(temp.description); TODO: Disabled due to Tradier bug

			if (security_ instanceof ATOptionSecurity) {
				if (StringUtils.isBlank(temp.underlying) || !temp.underlying.equals(temp.root_symbol)
						|| temp.contract_size != 100)
					return false;
				ATOptionSecurity option = (ATOptionSecurity) security_;
				option.setStrike(temp.strike);
				option.setContractSize(temp.contract_size);
				if ("weeklys".equals(temp.expiration_type)) {
					option.setExpirationType(ATOptionSecurity.Weekly);
				} else if ("standard".equals(temp.expiration_type)) {
					option.setExpirationType(ATOptionSecurity.Monthly);
				}
				if ("call".equals(temp.option_type)) {
					option.setOptionRight(ATOptionSecurity.Call);
				} else if ("put".equals(temp.option_type)) {
					option.setOptionRight(ATOptionSecurity.Put);
				}
				// expirationDate: Done previously
			}
			return true;
		} catch (Exception ex) {
			log.error(String.format("Error parsing [%s]", security_.getOccId()), ex);
			return false;
		}
	}

	private Map<String, TreeSet<ATLegacyDividend>> parseDividends(String json_, List<String> symbols_) {
		Map<String, TreeSet<ATLegacyDividend>> result = new HashMap<>();
		JsonArray jrootArray = _gson.fromJson(json_, JsonElement.class).getAsJsonArray();
		for (JsonElement jrootElement : jrootArray) {
			JsonObject data = jrootElement.getAsJsonObject();
			String symbol = data.get("request").getAsString();
			JsonElement jresults = data.get("results");
			if (!symbols_.contains(symbol) || jresults == null || !jresults.isJsonArray())
				continue;
			TreeSet<ATLegacyDividend> dividends = result.get(symbol);
			if (dividends == null) {
				dividends = new TreeSet<>();
				result.put(symbol, dividends);
			}
			JsonArray jresultsArray = jresults.getAsJsonArray();
			for (JsonElement jresult : jresultsArray) {
				JsonElement jtables = jresult.getAsJsonObject().get("tables");
				if (jtables != null) {
					JsonElement jcash_dividends = jtables.getAsJsonObject().get("cash_dividends");
					if (jcash_dividends.isJsonArray()) {
						for (JsonElement jdividend : jcash_dividends.getAsJsonArray()) {
							try {
								ATTradierCashDividend temp = _gson.fromJson(jdividend, ATTradierCashDividend.class);
								ATLegacyDividend div = new ATLegacyDividend();
								div.setCashAmount(temp.cash_amount);
								div.setDeclarationDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.declaration_date));
								div.setExDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.ex_date));
								div.setPayDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.pay_date));
								div.setRecordDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.record_date));
								dividends.add(div);
							} catch (Exception ex) {
								log.error(String.format("Error parsing dividend [%s]", jdividend), ex);
							}
						}
					}
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	private class ATTradierCashDividend {
		public String share_class_id;
		public String dividend_type;
		public String ex_date;
		public Double cash_amount;
		public String currency_i_d;
		public String declaration_date;
		public Integer frequency;
		public String pay_date;
		public String record_date;
	}

	private Map<String, TreeSet<ATEarningOld>> parseEarnings(String json_, List<String> symbols_) {
		Map<String, TreeSet<ATEarningOld>> result = new HashMap<>();
		JsonArray jrootArray = _gson.fromJson(json_, JsonElement.class).getAsJsonArray();
		for (JsonElement jrootElement : jrootArray) {
			JsonObject data = jrootElement.getAsJsonObject();
			String symbol = data.get("request").getAsString(); // Contains symbol
			JsonElement jresults = data.get("results");
			if (!symbols_.contains(symbol) || jresults == null || !jresults.isJsonArray())
				continue;
			TreeSet<ATEarningOld> earnings = result.get(symbol);
			if (earnings == null) {
				earnings = new TreeSet<>();
				result.put(symbol, earnings);
			}
			JsonArray jresultsArray = jresults.getAsJsonArray();
			for (JsonElement jresult : jresultsArray) {
				JsonElement jtables = jresult.getAsJsonObject().get("tables");
				if (jtables != null) {
					JsonElement jcorporate_calendars = jtables.getAsJsonObject().get("corporate_calendars");
					if (jcorporate_calendars.isJsonArray()) {
						for (JsonElement jcalendar : jcorporate_calendars.getAsJsonArray()) {
							try {
								ATTradierCashEarnings temp = _gson.fromJson(jcalendar, ATTradierCashEarnings.class);
								if (!_validEarnings.contains(temp.event_type))
									continue;
								ATEarningOld earning = new ATEarningOld();
								earning.setDate(ATFormats.DATE_yyyy_MM_dd.get().parse(temp.begin_date_time));
								earning.setEvent(temp.event);
								earnings.add(earning);
							} catch (Exception ex) {
								log.error(String.format("Error parsing earnings [%s]", jcalendar), ex);
							}
						}
					}
				}
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	private class ATTradierCashEarnings {
		public String company_id;
		public String begin_date_time;
		public String end_date_time;
		public Integer event_type;
		public String event;
		public String time_zone;
	}

	private void processPriceStats(Collection<ATStock> securities_, String json_) {
		JsonArray rootArray = _gson.fromJson(json_, JsonElement.class).getAsJsonArray();
		for (JsonElement jsonElement : rootArray) {
			JsonObject data = jsonElement.getAsJsonObject();
			String id = data.get("request").getAsString();
			List<ATStock> securities = new ArrayList<>(securities_);
			Iterator<ATStock> i = securities.iterator();
			while (i.hasNext()) {
				ATStock stock = i.next();
				if (id.equals(stock.getOccId())) {
					JsonElement quote = data.get("results");
					if (quote == null || quote.isJsonNull() || !quote.isJsonArray())
						continue;
					JsonArray rArray = quote.getAsJsonArray();
					for (JsonElement jsonElement2 : rArray) {
						JsonElement results = jsonElement2.getAsJsonObject().get("tables");
						if (results != null) {
							JsonElement priceStats = results.getAsJsonObject().get("price_statistics");
							if (priceStats.isJsonObject()) {
								populatePriceStats(stock, priceStats.getAsJsonObject());
							}
						}
					}
					i.remove();
					break;
				}
			}
		}
	}

	private void populatePriceStats(ATStock security_, JsonObject priceStats_) {
		if (priceStats_ == null)
			return;
		ATTradierPriceStats ps3m = new ATTradierPriceStats();
		ATTradierPriceStats ps1y = new ATTradierPriceStats();
		ATTradierPriceStats temp;
		try {
			temp = _gson.fromJson(priceStats_.get("period_90d"), ATTradierPriceStats.class);
			ps3m.merge(temp);
			temp = _gson.fromJson(priceStats_.get("period_3m"), ATTradierPriceStats.class);
			ps3m.merge(temp);
			temp = _gson.fromJson(priceStats_.get("period_1y"), ATTradierPriceStats.class);
			ps1y.merge(temp);
		} catch (Exception ex) {
			log.error(String.format("Error parsing [%s]", priceStats_), ex);
		}
		security_.setPrice1yAvg(ps1y.moving_average_price);
		security_.setPrice1yHigh(ps1y.high_price);
		security_.setPrice1yLow(ps1y.low_price);
		security_.setPrice3mAvg(ps3m.moving_average_price);
		security_.setPrice3mHigh(ps3m.high_price);
		security_.setPrice3mLow(ps3m.low_price);
	}

	// @SuppressWarnings("unused")
	private class ATTradierPriceStats {
		// public String period;
		public Double moving_average_price;
		public Double high_price;
		public Double low_price;

		public void merge(ATTradierPriceStats other_) {
			if (other_ == null)
				return;
			if (this.moving_average_price == null) {
				this.moving_average_price = other_.moving_average_price;
			}
			if (this.high_price == null) {
				this.high_price = other_.high_price;
			}
			if (this.low_price == null) {
				this.low_price = other_.low_price;
			}
		}
	}

	private String getHttp(String url_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			long gap = System.currentTimeMillis() - _lastProcessTime;
			if (gap < _processPeriod) {
				Thread.sleep(_processPeriod - gap);
			}

			CloseableHttpClient httpclient = HttpClients.createDefault();
			String token = _config.getString("tradier.token");
			HttpGet httpGet = new HttpGet(url_);
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Authorization", token);
			httpGet.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			response = httpclient.execute(httpGet);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(),
			// url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			_lastProcessTime = System.currentTimeMillis();
			return content;
		} finally {
			if (response != null)
				response.close();
		}
	}

}
