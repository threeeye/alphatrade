package com.isoplane.at.source.model;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class TdnEquityResearchConfig {

	public Double eqtPxMin;
	public Double eqtPxMax;

	public Double eqtPxChgMinD;
	public Double eqtPxChgMaxD;

	public String eqtIndustry;
	public String eqtSector;
	public String eqtSymbols;

	public String eqtAvgMode;
	public Double eqtAvgMin;
	public Double eqtAvgMax;

	public String eqtHVMode;
	public Double eqtHVMin;
	public Double eqtHVMax;

	public Long eqtRecMaxAge;
	public Boolean eqtIsOwned;
	public Boolean eqtHasNotes;
	public Boolean eqtHasAlerts;

	transient public Set<String> symbols;

	static public String getDefaultAvgMode() {
		return "sma60";
	}

	static public String getDefaultHVMode() {
		return "hv60";
	}

	public void cleanup() {
		if (eqtPxMin == null)
			eqtPxMin = -Double.MAX_VALUE;
		if (eqtPxMax == null)
			eqtPxMax = Double.MAX_VALUE;
		if (eqtPxChgMinD == null)
			eqtPxChgMinD = -Double.MAX_VALUE;
		if (eqtPxChgMaxD == null)
			eqtPxChgMaxD = Double.MAX_VALUE;

		if (StringUtils.isBlank(eqtAvgMode))
			eqtAvgMode = getDefaultAvgMode();
		if (eqtAvgMin == null)
			eqtAvgMin = -Double.MAX_VALUE;
		if (eqtAvgMax == null)
			eqtAvgMax = Double.MAX_VALUE;
		if (StringUtils.isBlank(eqtHVMode))
			eqtHVMode = getDefaultHVMode();
		if (eqtHVMin == null)
			eqtHVMin = -Double.MAX_VALUE;
		if (eqtHVMax == null)
			eqtHVMax = Double.MAX_VALUE;

		if (eqtIsOwned == null)
			eqtIsOwned = false;
		if (eqtHasNotes == null)
			eqtHasNotes = false;
		if (eqtHasAlerts == null)
			eqtHasAlerts = false;

		if (StringUtils.isNotBlank(eqtSymbols)) {
			symbols = new HashSet<>();
			String[] syms = eqtSymbols.split(",");
			for (String sym : syms) {
				symbols.add(sym.trim().toUpperCase());
			}
		}
	}

}
