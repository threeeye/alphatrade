package com.isoplane.at.source.controller;

import com.isoplane.at.source.model.ATUser;

public class ATRequestContext {

	static private ThreadLocal<ATUser> _user = new ThreadLocal<>();

	public static void setUser(ATUser user_) {
		_user.set(user_);
	}
	
	public static ATUser getUser() {
		return _user.get();
	}
	
	public static void clear() {
		_user.remove();
	}
}
