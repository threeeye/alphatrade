package com.isoplane.at.source.util;

public class ATCounter {

//	static final Logger log = LoggerFactory.getLogger(ATCounter.class);

	// private String _name;
	// private long _threshold;
	private long _count;
	private long _timer;

	public ATCounter(/*String name_, long threshold_*/) {
//		_name = name_;
//		_threshold = threshold_;
		_count = 0;
		_timer = System.currentTimeMillis();
	}

	public void increment() {
		_count++;
//		if (_count++ > _threshold) {
//			long count = _count;
//			_count = 0;
//			double seconds = (System.currentTimeMillis() - _timer) / 1000.0;
//			_timer = System.currentTimeMillis();
//			double ops = seconds != 0 ? count / seconds : 0;
//			log.info(String.format("Throughput [%s - %.2f/sec]", _name, ops));
//		}
	}

	public double getThroughput() {
		double seconds = (System.currentTimeMillis() - _timer) / 1000.0;
		double ops = seconds != 0 ? _count / seconds : 0;
		return ops;
	}
}
