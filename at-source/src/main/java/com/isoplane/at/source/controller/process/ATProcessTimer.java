package com.isoplane.at.source.controller.process;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.time.DateUtils;

public class ATProcessTimer extends ConcurrentHashMap<Class<?>, Long> {

	private static final long serialVersionUID = 1L;

	private Set<Class<?>> _dailyTimers = Collections.synchronizedSet(new HashSet<>());

	public void addDailyProcess(Class<? extends ATProcess> class_) {
		_dailyTimers.add(class_);
	}

	public void updateTimestamp(Class<?> class_) {
		this.put(class_, System.currentTimeMillis());
	}

	public boolean isTodayComplete(Class<?> class_) {
		Long last = this.get(class_);
		if (last == null)
			return false;
		Calendar lastDailyDate = Calendar.getInstance();
		lastDailyDate.setTimeInMillis(last);
		return DateUtils.isSameDay(lastDailyDate.getTime(), new Date());
	}

	public boolean throttle(Class<?> class_, long delta_) {
		Long last = this.get(class_);
		if (last == null)
			return false;
		return System.currentTimeMillis() - last < delta_;
	}

	public void reset() {
		this.clear();
	}

	public void reset(Class<?> class_) {
		this.remove(class_);
	}

}
