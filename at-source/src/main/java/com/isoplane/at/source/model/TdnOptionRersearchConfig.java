package com.isoplane.at.source.model;

import org.apache.commons.lang3.StringUtils;

public class TdnOptionRersearchConfig extends TdnEquityResearchConfig {

	// Underlying
	// public String ulSymbols;
	// public Double ulPxMin;
	public Boolean skipEarn;

	// Other
	// public String hvMode;
	// public String avgMode;
	public Long recMaxAge;
	public Double earningsRiskFactor;

	// Strategy
	public Integer stratMaxDays;
	public Double stratApyMin;
	public Double stratPxMin;
	public Integer stratDateMaxCount;
	public Double stratRiskMax;

	// Options
	public Double stratMarginMin;
	public Double spreadWidthMaxPx;
	public Integer spreadWidthMaxCount;
	public Double spreadWidthCountRiskFactor;
	public String spreadApyMode;

	public void cleanup() {
		super.cleanup();
		if (stratMarginMin == null)
			stratMarginMin = -Double.MAX_VALUE;
		if (stratApyMin == null)
			stratApyMin = 0.0;
		if (stratPxMin == null)
			stratPxMin = 0.0;
		if (spreadWidthMaxCount == null)
			spreadWidthMaxCount = Integer.MAX_VALUE;
		if (spreadWidthMaxPx == null)
			spreadWidthMaxPx = Double.MAX_VALUE;
		if (stratDateMaxCount == null)
			stratDateMaxCount = Integer.MAX_VALUE;
		if (spreadWidthCountRiskFactor == null)
			spreadWidthCountRiskFactor = 0.0;
		if (stratMaxDays == null)
			stratMaxDays = Integer.MAX_VALUE;
		if (earningsRiskFactor == null)
			earningsRiskFactor = 0.0;
		if (recMaxAge == null)
			recMaxAge = 30 * 24 * 60 * 60 * 1000L;
		if (StringUtils.isBlank(spreadApyMode))
			spreadApyMode = "mid";
	}

}
