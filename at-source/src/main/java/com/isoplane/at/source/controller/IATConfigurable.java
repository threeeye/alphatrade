package com.isoplane.at.source.controller;

import com.isoplane.at.source.model.IATUserConfiguration;

public interface IATConfigurable {

	void updateConfiguration(IATUserConfiguration config);
}
