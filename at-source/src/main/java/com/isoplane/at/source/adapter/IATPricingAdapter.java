package com.isoplane.at.source.adapter;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.IATQuote;
import com.isoplane.at.source.model.ATPriceHistoryOld;

public interface IATPricingAdapter {

	List<IATQuote> getPricing(Collection<? extends ATSecurity> securities);

	TreeSet<ATPriceHistoryOld> getPriceHistory(String ticker, Date start, Date end);

}
