package com.isoplane.at.source.adapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.annotation.Obsolete;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.source.model.ATConfiguration;
import com.isoplane.at.source.model.ATEarningOld;

@Obsolete
public class ATStockRoverAdapter implements IATEarningsAdapter {

	static final Logger log = LoggerFactory.getLogger(ATStockRoverAdapter.class);

//	static private final SimpleDateFormat _yyyy_MM_dd = new SimpleDateFormat("yyyy-MM-dd");
// private final SimpleDateFormat _stdDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	static private final String EARNINGS_URL = "https://www.stockrover.com/markets/central/get_earnings_calendar_data";

	private Gson _gson = new Gson();
	private ATConfiguration _config;

	public ATStockRoverAdapter(ATConfiguration config_) {
		_config = config_;
	}

	@Override
	public TreeSet<ATEarningOld> getEarnings(ATStock security_, boolean force_) {
		// Implementation only supports unspecified bulk. If the daily update didn't contain the security it is unlikely
		// the new update will.
		if (!force_ || security_ == null || StringUtils.isBlank(security_.getSymbol())) {
			return null;
		} else {
			Map<String, TreeSet<ATEarningOld>> earnings = getEarnings(null);
			return earnings.get(security_.getSymbol());
		}
	}

	// Used for daily call to update and persist latest earnings
	@Override
	public Map<String, TreeSet<ATEarningOld>> getEarnings(Collection<ATStock> securities_) {
		// Note: Implementation ignores input and retrieves all earnings with one call
		try {
			String path = String.format("%s/%s.earnings", _config.getString("data.path"),
					ATStockRoverAdapter.class.getSimpleName());
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.add(Calendar.DAY_OF_MONTH, -1);
			while (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
				cal.add(Calendar.DAY_OF_MONTH, -1);
			}
			String startStr = String.format("%sT00:00:00",  ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime()));
			for (int i = 1; i < 19; i++) {
				cal.add(Calendar.WEEK_OF_YEAR, 1);
			}
			cal.add(Calendar.DAY_OF_MONTH, -1);
			String endStr = String.format("%sT23:59:59",  ATFormats.DATE_yyyy_MM_dd.get().format(cal.getTime()));

			log.info(String.format("Updating earnings [%s - %s]", startStr, endStr));

			Map<String, String> form = new HashMap<>();
			form.put("start_date", startStr);
			form.put("end_date", endStr);
			form.put("min_date", startStr);
			form.put("max_date", endStr);
			form.put("auth_window", "false");
			form.put("full_data_request", "0");
			form.put("ticker_list_request", "0");
			form.put("ticker_day_request", "0");
			form.put("tickers", "0");
			String json = postHttp(EARNINGS_URL, form);
			if (StringUtils.isBlank(json)) {
				json = new Scanner(new File(path)).useDelimiter("\\A").next();
			} else {
				try (PrintWriter out = new PrintWriter(path)) {
					out.println(json);
				}
			}
			Map<String, TreeSet<ATEarningOld>> earningsMap = parseEarnings(json);
			// updateDatabase(earningsMap);
			return earningsMap;
		} catch (Exception ex) {
			log.error(String.format("Error updating earings"), ex);
			return null;
		}
	}

	private Map<String, TreeSet<ATEarningOld>> parseEarnings(String json_) {
		Map<String, TreeSet<ATEarningOld>> earningsMap = new HashMap<>();
		JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
		JsonArray array = root.getAsJsonArray("earnings_calendar_next_quarter");
		int count = 0;
		for (JsonElement jsonElement : array) {
			JsonArray earnArray = jsonElement.getAsJsonArray();
			String ticker = earnArray.get(0).getAsString();
			TreeSet<ATEarningOld> earnings = earningsMap.get(ticker);
			if (earnings == null) {
				earnings = new TreeSet<>();
				earningsMap.put(ticker, earnings);
			}
			String dateStr = earnArray.get(3).getAsString();
			try {
				Date date = ATFormats.DATE_full.get().parse(dateStr);
				ATEarningOld earn = new ATEarningOld();
				earn.setOccId(ticker);
				earn.setDate(date);
				earn.setChanged(true);
				earnings.add(earn);
				count++;
				// log.info(String.format("##### %d - Ticker: %s - %s", i++, ticker, _yyyy_MM_dd.format(date)));
			} catch (Exception ex) {
				log.error(String.format("Error parsing date [%s - %s]", ticker, dateStr));
			}
		}
		log.info(String.format("Retrieved [%d] earnings", count));
		return earningsMap;
	}

	private String postHttp(String url_, Map<String, String> form_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			// long gap = System.currentTimeMillis() - _lastProcessTime;
			// if (gap < _processPeriod) {
			// Thread.sleep(_processPeriod - gap);
			// }

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url_);
			httpPost.setHeader("Accept", "application/json");
			// httpGet.setHeader("Authorization", _tradierToken);
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			for (Entry<String, String> nameValuePair : form_.entrySet()) {
				params.add(new BasicNameValuePair(nameValuePair.getKey(), nameValuePair.getValue()));
			}
			httpPost.setEntity(new UrlEncodedFormEntity(params));

			response = httpclient.execute(httpPost);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(),
			// url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} finally {
			if (response != null)
				response.close();
		}
	}

}
