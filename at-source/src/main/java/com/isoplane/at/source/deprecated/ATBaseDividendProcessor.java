package com.isoplane.at.source.deprecated;

import java.util.Collection;
import java.util.Map;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATLegacyDividend;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.source.adapter.IATDividendAdapter;

public class ATBaseDividendProcessor {

	static final Logger log = LoggerFactory.getLogger(ATBaseDividendProcessor.class);

	private ATStore _store;
	private IATDividendAdapter _adapter;
	// private IATDividendDb _db;
	// private Map<String, TreeSet<ATLegacyDividend>> _dividendMap;

	public ATBaseDividendProcessor(ATStore store_, IATDividendAdapter adapter_) {
		_store = store_;
		_adapter = adapter_;
		// _db = db_;
		// init();
	}

	// private void init() {
	// log.info(String.format("Initializing..."));
	// Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
	// _dividendMap = _db.load(null);
	//
	// for (TreeSet<ATLegacyDividend> divs : _dividendMap.values()) {
	// for (ATLegacyDividend div : divs) {
	// div.setChanged(true);
	// _store.put(div);
	// }
	// }
	//
	// int count1 = _dividendMap.values().stream().mapToInt(Set::size).sum();
	// _dividendMap.values().stream().iterator().forEachRemaining(s -> s.removeIf(d -> d.getExDate().before(today)));
	// int count2 = _dividendMap.values().stream().mapToInt(Set::size).sum();
	// log.info(String.format("Loaded [%,d / %,d] dividends", count2, count1));
	// }

	// public void initialize(Collection<ATStock> securities_) {
	// for (ATStock stock : securities_) {
	// TreeSet<ATLegacyDividend> dividends = _dividendMap.get(stock.getSymbol());
	// if (dividends == null || dividends.isEmpty())
	// continue;
	// stock.getDividends().clear();
	// stock.getDividends().addAll(dividends);
	// }
	// }

	// public boolean processDividends(Collection<ATStock> securities_) {
	// if (securities_ == null || securities_.isEmpty())
	// return false;
	// for (ATStock stock : securities_) {
	// TreeSet<ATLegacyDividend> dividends = _dividendMap.get(stock.getSymbol());
	// if (dividends == null) {
	// dividends = _adapter.getDividend(stock, false);
	// if (dividends != null) {
	// Map<String, TreeSet<ATLegacyDividend>> newDividendMap = new HashMap<>();
	// newDividendMap.put(stock.getOccId(), dividends);
	// if (updateDividends(newDividendMap, false)) {
	// _dividendMap.put(stock.getOccId(), dividends);
	// log.info(String.format("Added new dividend [%s]", stock.getOccId()));
	// } else {
	// continue;
	// }
	// }
	// }
	// if (dividends != null) {
	// stock.getDividends().clear();
	// stock.getDividends().addAll(dividends);
	// }
	// }
	// return true;
	// }

	// Daily update
	public boolean updateDividends(Collection<ATStock> securities_) {
		try {
			Map<String, TreeSet<ATLegacyDividend>> dividendMap = _adapter.getDividends(securities_);
			// boolean result_digest = updateDividends(newDividendMap, true);
			if (dividendMap == null)
				return false;

			for (String occId : dividendMap.keySet()) {
				// stockId = stock.getOccId();
				TreeSet<ATLegacyDividend> dividends = dividendMap.get(occId);
				for (ATLegacyDividend div : dividends) {
					_store.put(div);
				}
				// stock.getEarnings().clear();
				// stock.getEarnings().addAll(earnings);
			}
			// }
			return true;

		} catch (Exception ex) {
			log.error(String.format("Error updating dividends"), ex);
			return false;
		}
	}

	// private boolean updateDividends(Map<String, TreeSet<ATLegacyDividend>> dividendMap_, boolean mergeOld_) {
	// if (dividendMap_ == null || dividendMap_.isEmpty())
	// return false;
	// int count = 0;
	// Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
	// Set<String> dbKeys = mergeOld_ ? null : dividendMap_.keySet();
	// Map<String, TreeSet<ATLegacyDividend>> oldDividendMap = _db.load(dbKeys);
	// for (String key : dividendMap_.keySet()) {
	// TreeSet<ATLegacyDividend> newDividends = dividendMap_.get(key);
	// TreeSet<ATLegacyDividend> oldDividends = oldDividendMap.get(key);
	// // 1. Merge latest dividends
	// if (oldDividends != null) {
	// newDividends.addAll(oldDividends);
	// }
	// // 2. Store dividends
	// Map<String, TreeSet<ATLegacyDividend>> storageMap = new HashMap<>();
	// storageMap.put(key, newDividends);
	// count += _db.store(storageMap);
	// }
	// // 3. Merge all dividends
	// if (mergeOld_) {
	// for (String key : oldDividendMap.keySet()) {
	// if (!dividendMap_.containsKey(key)) {
	// dividendMap_.put(key, oldDividendMap.get(key));
	// }
	// }
	// }
	// // 4. Trim dividends (for runtime remove dividends before today)
	// for (String key : dividendMap_.keySet()) {
	// TreeSet<ATLegacyDividend> newDividends = dividendMap_.get(key);
	// Iterator<ATLegacyDividend> i = newDividends.iterator();
	// Date previousDate = new Date(0);
	// while (i.hasNext()) {
	// Date date = i.next().getExDate();
	// if (date.before(today)) {
	// i.remove();
	// }
	// // TODO: 4. Warn if dividends too close (may be data corruption)
	// int days = (int) TimeUnit.DAYS.convert(date.getTime() - previousDate.getTime(), TimeUnit.MILLISECONDS);
	// if (days < 7) {
	// log.warn(String.format("[%s] dividends too narrow [%s - %s]", key,
	// ATFormats.DATE_yyyy_MM_dd.format(previousDate), ATFormats.DATE_yyyy_MM_dd.format(date)));
	// }
	// }
	// }
	// log.info(String.format("Stored [%d] dividends", count++));
	// return true;
	// }

}
