package com.isoplane.at.source.deprecated;

import java.util.Set;

import com.isoplane.at.source.model.ATUser;

@Deprecated
public interface IATUserService {

//	Set<ATUser> getUsers();

	ATUser getUser(String email);

//	void update(ATUser user);
}
