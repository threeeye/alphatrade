package com.isoplane.at.source.controller;

import java.util.Map;

import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;

public interface IATEquityFoundationProvider extends IATService {

	Map<String, ATPersistentEquityFoundation> getFoundationMap();

	ATPersistentEquityFoundation getFoundation(String occId);

}
