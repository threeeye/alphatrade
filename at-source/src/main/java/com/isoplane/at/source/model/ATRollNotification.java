package com.isoplane.at.source.model;

import java.util.Map;

import com.isoplane.at.commons.model.ATBaseNotification;

public class ATRollNotification extends ATBaseNotification {

	static public final String META_ROLL = "roll";

	private String ownedTicker;
	private String rollTicker;
	private Double apy;
	private Double absolute;
	private Double stockPrice;

	@Override
	public String getNotificationKey() {
		String key = String.format("roll %s%s", getOwnedTicker(), getRollTicker());
		return key;
	}

	@Override
	public ATNotificationContent getNotificationContent(Map<String, ATBaseNotification> notificationMap_, long timeout_) {
		ATBaseNotification oldB = notificationMap_ != null ? notificationMap_.get(this.getNotificationKey()) : null;
		ATRollNotification oldRoll = oldB != null && oldB instanceof ATRollNotification
				? (ATRollNotification) oldB
				: null;
		// String arrowStr = "✧"; // ▷◇⋄
		if (oldRoll != null) {
			double change = (oldRoll.getApy() - this.getApy()) / oldRoll.getApy();
			if (Math.abs(change) < 0.25) {
				return null;
				// arrowStr = oldRoll.getApy() < this.getApy() ? "➚" : "➘"; // △▽◥◢
				// } else {
				// return null;
			}
		}
		// "➚" : "➘"; // △▽◥◢➜✧▷◇⋄⋗⋇⚡⛊▷♲♺♻↻
		// String[] rollTokens = this.getRollTicker().replaceAll(" +", " ").split(" ");
		String ownedTicker = this.getOwnedTicker().replaceAll(" +", " ");
		String rollTicker = this.getRollTicker().substring(this.getRollTicker().indexOf(' ')).replaceAll(" +", " ");

		String dirStr = (Integer) this.getMeta(META_ROLL) > 0 ? "◥" : "◢";
		String title = String.format("%s %s %.2f", ownedTicker, dirStr, this.getStockPrice());
		String body = String.format("♺  %s / %.2f ➜ %.2f%%", rollTicker, this.getAbsolute(), this.getApy());
		// String body = String.format("♺ %s / %.2f ➜ %.2f%% \n· alphatra.de · %tT", rollTicker, this.getAbsolute(),
		// this.getApy(), new Date());
		String icon = "/globe_roll.ico";

		return new ATNotificationContent(title, body, icon);
	}

	public String getOwnedTicker() {
		return ownedTicker;
	}

	public void setOwnedTicker(String symbol) {
		this.ownedTicker = symbol;
	}

	public Double getApy() {
		return apy;
	}

	public void setApy(Double stockPrice) {
		this.apy = stockPrice;
	}

	public Double getAbsolute() {
		return absolute;
	}

	public void setAbsolute(Double margin) {
		this.absolute = margin;
	}

	public String getRollTicker() {
		return rollTicker;
	}

	public void setRollTicker(String rollTicker) {
		this.rollTicker = rollTicker;
	}

	@Override
	public String toString() {
		String str = String.format("Strike{%s %.2f (%.2f)}", getOwnedTicker(), getApy(), getAbsolute());
		return str;
	}

	public Double getStockPrice() {
		return stockPrice;
	}

	public void setStockPrice(Double stockPrice) {
		this.stockPrice = stockPrice;
	}

	@Override
	public String getNotificationTag() {
		return String.format("roll_%s_%s", getOwnedTicker(), getRollTicker());
	}

	@Override
	public String getType() {
		return "roll";
	}

}
