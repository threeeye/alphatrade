package com.isoplane.at.source.model;

import com.isoplane.at.commons.model.ATOptionSecurity;

public class ATRoll implements Comparable<ATRoll> {

	private ATOptionSecurity option;
	private int offset;
	private ATYield yield;

	public ATOptionSecurity getOption() {
		return option;
	}

	public void setOption(ATOptionSecurity option) {
		this.option = option;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public ATYield getYield() {
		return yield;
	}

	public void setYield(ATYield yield) {
		this.yield = yield;
	}

	@Override
	public int compareTo(ATRoll other_) {
		if (other_ == null)
			return -1;
		if (getOption() == null)
			return other_.getOption() == null ? Integer.compare(getOffset(), other_.getOffset()) : 1;
			
		int result = getOption().compareTo(other_.getOption());
		return result != 0 ? result : Integer.compare(getOffset(), other_.getOffset());
	}
}
