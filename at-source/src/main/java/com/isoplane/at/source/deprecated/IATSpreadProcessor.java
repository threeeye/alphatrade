package com.isoplane.at.source.deprecated;

import java.util.Collection;
import java.util.TreeSet;

import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATOpportunity;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.source.model.TdnOptionRersearchConfig;

public interface IATSpreadProcessor extends IATService {

	/**
	 * @param occIds
	 *            - Stocks to add. Those previously added will be skipped.
	 * @return List of added Stock ids.
	 */
	// TreeSet<String> add(String... occIds);

	TreeSet<ATOpportunity> getSpreads();
	
	JsonObject getNewSpreads(String userId, TdnOptionRersearchConfig spreadConfig);

	Collection<ATBaseNotification> getSpreadEvents(ATPersistentSecurity... secs);

	// void quit();
}
