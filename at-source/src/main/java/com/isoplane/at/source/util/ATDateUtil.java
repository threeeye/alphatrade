package com.isoplane.at.source.util;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

public class ATDateUtil {

	static public Date getStartOfToday() {
		Date sot = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		return sot;
	}
}
