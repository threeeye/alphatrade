package com.isoplane.at.source.controller;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.google.common.primitives.Ints;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.protobuf.Any;
import com.isoplane.at.adapter.alpaca.ATAlpacaAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATEquityDescription;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATHistoryData;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATOpportunity2;
import com.isoplane.at.commons.model.ATOpportunityClip;
import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATOptionExpiration.ATOptionExpirationDate;
import com.isoplane.at.commons.model.ATPerformance;
import com.isoplane.at.commons.model.ATPortfolio;
import com.isoplane.at.commons.model.ATPosition;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.ATTestNotification;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.ATTradeTraceGroup;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.ATUserAsset;
import com.isoplane.at.commons.model.ATUserEquityModel;
import com.isoplane.at.commons.model.ATUserNote;
import com.isoplane.at.commons.model.ATUserOpportunityConfig;
import com.isoplane.at.commons.model.ATUserSetting;
import com.isoplane.at.commons.model.ATWatchlist;
import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATMarketData.OptionRight;
import com.isoplane.at.commons.model.IATOptionDescription;
import com.isoplane.at.commons.model.IATPortfolio;
import com.isoplane.at.commons.model.IATStatistics;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.WSMessageWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoAlertRuleWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoPushNotificationWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoSecurityPackWrapper;
import com.isoplane.at.commons.model.protobuf.AlertRuleProtos.AlertRule;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.model.protobuf.SecurityPackProtos.SecurityPack;
import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.commons.model.serializers.ATDateSerializer;
import com.isoplane.at.commons.model.serializers.ATDoubleSerializer;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATFundamentalsProvider;
import com.isoplane.at.commons.service.IATMarketDataService;
import com.isoplane.at.commons.service.IATMarketDataSubscriber;
import com.isoplane.at.commons.service.IATPortfolioService;
import com.isoplane.at.commons.service.IATSessionListener;
import com.isoplane.at.commons.service.IATSymbolListener;
import com.isoplane.at.commons.service.IATTradeListener;
import com.isoplane.at.commons.service.IATUSTreasuryRateProvider;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.service.IATWebSocketService;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATMathUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATWSMessageQueue;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.ext.quantlib.ATOptionAnalytics;
import com.isoplane.at.ext.quantlib.ATQuantLib;
//import com.isoplane.at.gatherer.ATGathererController;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.alertrule.ATKafkaAlertRuleProducer;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationProducer;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageProducer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;
//import com.isoplane.at.source.controller.ATTaskManager.ATTask;
import com.isoplane.at.source.util.ATHttpUtil;
import com.isoplane.at.source.util.ATWebSocketServiceJavalin;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATSourceController
		implements IATTradeListener, IATMarketDataSubscriber, IATSessionListener, IATSymbolListener {

	static final Logger log = LoggerFactory.getLogger(ATSourceController.class);

	static public final String SOURCE_KEY = "SRC";

	static private Instant NOW;
	// private Configuration _config;
	private String _todayDS8;
	private boolean _isRunning;
	//	private ATGathererController _gatherer;
	// private ATTaskManager _taskSvc;
	private Gson _gson;

	private DelayQueue<DelayedTradeToken> _tradeTokenQueue;

	private ATKafkaSseMessageProducer _kafkaSseProducer;

	private ATKafkaPushNotificationProducer _kafkaPushNotificationProducer;

	private ATKafkaUserSessionProducer _kafkaUserSessionProducer;
	private ATKafkaAlertRuleProducer _kafkaAlertRuleProducer;

	private ATKafkaUserSessionController _userSessionCtrl;

	private ATAlpacaAdapter _alpacaAdp; // Used to get UL time/sales px when persisting trades.

	public ATSourceController(Configuration config_) {
		// this._config = config_;
		_isRunning = true;
		_tradeTokenQueue = new DelayQueue<>();
		//	_gatherer = new ATGathererController();
		// _taskSvc = new ATTaskManager(config_);

		//	ATSourceServiceRegistry.getFundamentalsProvider().
	}

	public void init() {
		_gson = new GsonBuilder()
				.registerTypeAdapter(Date.class, new ATDateSerializer())
				.registerTypeAdapter(Double.class, new ATDoubleSerializer())
				.create();

		_userSessionCtrl = ATKafkaUserSessionController.instance();
		ATSourceServiceRegistry.getPortfolioService().register(this);
		ATSourceServiceRegistry.getMarketDataService().subscribe(this);
		ATSourceServiceRegistry.getUserService().register(this);
		ATSourceServiceRegistry.getSymbolProvider().register(this);
		NOW = Instant.now().truncatedTo(ChronoUnit.DAYS);
		ATExecutors.submit(String.format("%s.wsMessageQueue", this.getClass().getSimpleName()),
				() -> processWSMessageQueue());
		ATExecutors.submit(String.format("%s.tradeTokenQueue", this.getClass().getSimpleName()),
				() -> processTradeQueue());
		ATExecutors.scheduleAtFixedRate(String.format("%s.movers", this.getClass().getSimpleName()),
				() -> pushUpdates(), 30000, 30000);

		// ATKafkaUserSessionConsumer kafkaUserSessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		// kafkaUserSessionConsumer.subscribe(this);
		_alpacaAdp = ATAlpacaAdapter.getInstance();

		_kafkaAlertRuleProducer = ATKafkaRegistry.get(ATKafkaAlertRuleProducer.class);
		_kafkaSseProducer = ATKafkaRegistry.get(ATKafkaSseMessageProducer.class);
		_kafkaUserSessionProducer = ATKafkaRegistry.get(ATKafkaUserSessionProducer.class);
		_kafkaPushNotificationProducer = ATKafkaRegistry.get(ATKafkaPushNotificationProducer.class);
		// _taskSvc.init();
	}

	// public void syncSheets(String userId_) {
	// ATSourceServiceRegistry.getPortfolioService().verifySheets(userId_);
	// }

	// public Map<String, Object> registerTask(String uid_, String taskType_, String json_) {
	// // Map<String, Object> result_digest = _taskSvc.registerTask(uid_, taskType_, json_);
	// return null;
	// }

	// public boolean registerTasks(String source_, String code_, Object tasks_) {
	// return false;
	// // return _taskSvc.registerTasks(source_, code_, tasks_);
	// }

	// public List<String> getTasks(String userId_) {
	// return null;
	// // List<IATTask> tasks = _taskSvc.getTasks(userId_);
	// // if (tasks == null || tasks.isEmpty())
	// // return null;
	// // List<String> serlz = tasks.stream().map(t -> t.serialize(_gson)).collect(Collectors.toList());
	// // return serlz;
	// }

	public Collection<ATPerformance> getPerformance(String userId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
		List<IATTrade> trades = ATSourceServiceRegistry.getPortfolioService().getTrades(query);
		log.debug(String.format("getPerformance loaded [%d] trades", trades.size()));

		// 1. Group trades by underlying and groupId
		Map<String, List<IATTrade>> byGroupMap = new HashMap<>();
		Map<String, List<IATTrade>> byUnderyingMap = new TreeMap<>();
		for (IATTrade trade : trades) {
			String occId = trade.getOccId();
			String symbol = occId.length() < 6 ? occId : occId.substring(0, 6).trim();
			List<IATTrade> byUnderlyingList = byUnderyingMap.get(symbol);
			if (byUnderlyingList == null) {
				byUnderlyingList = new ArrayList<>();
				byUnderyingMap.put(symbol, byUnderlyingList);
			}
			byUnderlyingList.add(trade);
			String groupId = trade.getTradeGroup();
			if (groupId != null) {
				List<IATTrade> byGroupList = byGroupMap.get(groupId);
				if (byGroupList == null) {
					byGroupList = new ArrayList<>();
					byGroupMap.put(groupId, byGroupList);
				}
				byGroupList.add(trade);
			}
		}

		// 2a.
		for (List<IATTrade> tradeGroup : byGroupMap.values()) {
			Map<Integer, Double[]> typeMap = new HashMap<>();
			for (IATTrade trade : tradeGroup) {
				trade.getTradeType();
				Double[] obj = typeMap.get(trade.getTradeType());
				Double sz = trade.getSzTrade();
				Double px = trade.getPxTradeTotal();
				if (obj == null) {
					obj = new Double[] { (sz == null ? 0 : sz), (px == null ? 0 : px) };
					typeMap.put(trade.getTradeType(), obj);
				} else {
					obj[0] = obj[0] + (sz == null ? 0 : sz);
					obj[1] = obj[1] + (px == null ? 0 : px);
				}
			}
		}

		// 3. Merge and sort trades into categories and unique occIds
		final double pseudoInfinity = 5000000000.0;
		final String DEBIT = "_tempDebit";
		final String OPTION = "_tempOption";
		final Date now = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		IATMarketDataService mdSvc = ATSourceServiceRegistry.getMarketDataService();
		List<ATPerformance> performances = new ArrayList<>();
		for (Entry<String, List<IATTrade>> entry : byUnderyingMap.entrySet()) {
			double totalCash = 0;
			String ulOccId = entry.getKey();
			ATPerformance perf = new ATPerformance();
			perf.setOccId(entry.getKey());
			IATMarketData market = mdSvc.getMarketData(ulOccId);
			if (market != null) {
				perf.setPxLast(market.getPxLast());
			}
			Set<ATPerformance> longCalls = new TreeSet<>();
			Set<ATPerformance> longPuts = new TreeSet<>();
			ATPerformance underlying = null;
			Set<ATPerformance> shortCalls = new TreeSet<>();
			Set<ATPerformance> shortPuts = new TreeSet<>();
			Map<String, ATPerformance> detailMap = new TreeMap<>();
			for (IATTrade trade : entry.getValue()) {
				try {
					String detailOccId = trade.getOccId();
					Double tradeSz = trade.getSzTrade();
					Double tradePxTotal = trade.getPxTradeTotal();
					if (tradePxTotal != null)
						totalCash += tradePxTotal;
					ATOptionDescription option = ATOptionDescription.create(detailOccId);
					if (tradeSz == null || option != null && option.getExpiration().before(now))
						continue;
					ATPerformance detail = detailMap.get(detailOccId);
					if (detail == null) {
						detail = new ATPerformance();
						detail.setOccId(detailOccId);
						detail.setValueTotal(0.0);
						// detail.setRiskTotal(0.0);
						detail.setPxTradeTotal(0.0);
						detail.setPxTrade(0.0);
						detail.setSzTrade(0.0);
						IATMarketData detailMarket = mdSvc.getMarketData(detailOccId);
						if (detailMarket != null) {
							detail.setPxLast(detailMarket.getPxLast());
						}
						detailMap.put(detailOccId, detail);
						if (option == null) {
							underlying = detail;
						} else {
							detail.put(OPTION, option);
							if (tradeSz > 0) {
								if (IATOptionDescription.Right.Call.equals(option.getRight()))
									longCalls.add(detail);
								else
									longPuts.add(detail);
							} else {
								if (IATOptionDescription.Right.Call.equals(option.getRight()))
									shortCalls.add(detail);
								else
									shortPuts.add(detail);
							}
						}
					}
					// NOTE: Order of following is important
					Double tradePx = trade.getPxTrade();
					double detailSz = detail.getSzTrade();
					detail.setSzTrade(detail.getSzTrade() + tradeSz);
					if (tradePx != null) {
						detail.setPxTrade(
								((detailSz * detail.getPxTrade()) + (tradeSz * tradePx)) / (detailSz + tradeSz));
					}
					if (tradePxTotal != null)
						detail.setPxTradeTotal(detail.getPxTradeTotal() + tradePxTotal);
				} catch (Exception ex) {
					log.error(String.format("Error processing [%s]", trade), ex);
				}
			}

			// 3. Process categories
			Map<String, ATPerformance> perfMap = new TreeMap<>();
			double underlyingCount = underlying != null && underlying.getSzTrade() != null ? underlying.getSzTrade()
					: 0;
			if (underlyingCount != 0) {
				double underlyingPxLast = underlying.getPxLast() != null ? underlying.getPxLast() : 0;
				double underlyingValue = underlyingCount * underlyingPxLast;
				underlying.setValueTotal(underlyingValue);
				underlying.put(DEBIT, underlyingCount);
				perfMap.put(underlying.getHashStr(false), underlying);
			} else {
				underlying = null;
			}
			for (ATPerformance longCall : longCalls) {
				Double longCallCount = longCall.getSzTrade();
				double longCallPxLast = longCall.getPxLast() != null ? longCall.getPxLast() : 0;
				if (longCallCount == null || longCallCount == 0)
					continue;
				double longCallValue = 100 * longCallCount * longCallPxLast;
				longCall.setValueTotal(longCallValue);
				longCall.put(DEBIT, longCallCount);
			}
			for (ATPerformance longPut : longPuts) {
				Double longPutCount = longPut.getSzTrade();
				double longPutPxLast = longPut.getPxLast() != null ? longPut.getPxLast() : 0;
				if (longPutCount == null || longPutCount == 0)
					continue;
				double longPutValue = 100 * longPutCount * longPutPxLast;
				longPut.setValueTotal(longPutValue);
				longPut.put(DEBIT, longPutCount);
			}
			for (ATPerformance shortCall : shortCalls) {
				Double shortCallCount = shortCall.getSzTrade();
				double shortCallPxLast = shortCall.getPxLast() != null ? shortCall.getPxLast() : 0;
				if (shortCallCount == null || shortCallCount == 0)
					continue;
				double shortCallValue = 100 * shortCallCount * shortCallPxLast;
				shortCall.setValueTotal(shortCallValue);
				// Look for cover in underlying
				double longUnderlyingDebit = underlying != null ? (Double) underlying.get(DEBIT) : 0;
				if (longUnderlyingDebit > 0) {
					double uncoveredCount = Math.max(0, -100 * shortCallCount - longUnderlyingDebit) / 100;
					double coveredCount = -shortCallCount - uncoveredCount;
					underlying.put(DEBIT, Math.max(0, longUnderlyingDebit - (100 * coveredCount)));
					if (uncoveredCount == 0 /* covered */) {
						perfMap.put(shortCall.getHashStr(false), shortCall);
						continue;
					} else {
						shortCallCount += coveredCount;
						ATPerformance coveredShortCall = new ATPerformance();
						coveredShortCall.putAll(shortCall);
						coveredShortCall.setSzTrade(coveredCount);
						perfMap.put(coveredShortCall.getHashStr(false), coveredShortCall);
						shortCall.setSzTrade(shortCallCount);
					}
				}
				Iterator<ATPerformance> iLongCall = longCalls.iterator();
				while (iLongCall.hasNext()) {
					ATPerformance longCall = iLongCall.next();
					double longCallCount = longCall.getSzTrade();
					ATOptionDescription shortCallOption = (ATOptionDescription) shortCall.get(OPTION);
					ATOptionDescription longCallOption = (ATOptionDescription) longCall.get(OPTION);
					if (longCallCount <= 0 || longCallOption.getExpiration().before(shortCallOption.getExpiration()))
						continue;
					double uncoveredCount = Math.max(0, -shortCallCount - longCallCount);
					double coveredCount = -shortCallCount - uncoveredCount;
					if (coveredCount > 0) {
						longCallCount -= coveredCount;
						if (longCallCount <= 0) {
							iLongCall.remove();
						} else {
							longCall.setSzTrade(longCallCount - coveredCount);
						}
						double longCallRisk = -100 * coveredCount * longCallOption.getStrike();
						ATPerformance coveringLongCall = new ATPerformance();
						coveringLongCall.putAll(longCall);
						coveringLongCall.setSzTrade(coveredCount);
						coveringLongCall.setCallRisk(longCallRisk);
						ATPerformance previousLongCall = perfMap.get(coveringLongCall.getHashStr(false));
						if (previousLongCall != null) {
							previousLongCall.setSzTrade(previousLongCall.getSzTrade() + coveredCount);
							previousLongCall.setCallRisk(previousLongCall.getCallRisk() + longCallRisk);
						} else {
							perfMap.put(coveringLongCall.getHashStr(false), coveringLongCall);
						}

						double shortCallRisk = 100 * coveredCount * shortCallOption.getStrike();
						ATPerformance coveredShortCall = new ATPerformance();
						coveredShortCall.putAll(shortCall);
						coveredShortCall.setSzTrade(-coveredCount);
						coveredShortCall.setCallRisk(shortCallRisk);
						ATPerformance previousShortCall = perfMap.get(coveredShortCall.getHashStr(false));
						if (previousShortCall != null) {
							previousShortCall.setSzTrade(previousShortCall.getSzTrade() - coveredCount);
							previousShortCall.setCallRisk(previousShortCall.getCallRisk() + shortCallRisk);
						} else {
							perfMap.put(coveredShortCall.getHashStr(false), coveredShortCall);
						}
						shortCallCount += coveredCount;
						shortCall.setSzTrade(shortCallCount);
						if (shortCallCount >= 0)
							break;
					}
				}
				if (shortCallCount < 0) {
					shortCall.setCallRisk(pseudoInfinity);
					perfMap.put(shortCall.getHashStr(false), shortCall);
				}
			}
			for (ATPerformance shortPut : shortPuts) {
				Double shortPutCount = shortPut.getSzTrade();
				double shortPutPxLast = shortPut.getPxLast() != null ? shortPut.getPxLast() : 0;
				if (shortPutCount == null || shortPutCount == 0)
					continue;
				double shortPutValue = 100 * shortPutCount * shortPutPxLast;
				shortPut.setValueTotal(shortPutValue);
				// Look for cover in underlying
				double underlyingDebit = underlying != null ? (Double) underlying.get(DEBIT) : 0;
				if (underlyingDebit < 0) {
					double uncoveredCount = Math.max(0, -100 * shortPutCount + underlyingDebit) / 100;
					double coveredCount = -shortPutCount - uncoveredCount;
					underlying.put(DEBIT, underlyingDebit + (100 * coveredCount));
					if (uncoveredCount == 0 /* covered */) {
						perfMap.put(shortPut.getHashStr(false), shortPut);
						continue;
					} else {
						shortPutCount += coveredCount;
						ATPerformance coveredShortPut = new ATPerformance();
						coveredShortPut.putAll(shortPut);
						coveredShortPut.setSzTrade(coveredCount);
						perfMap.put(coveredShortPut.getHashStr(false), coveredShortPut);
						shortPut.setSzTrade(shortPutCount);
					}
				}
				Iterator<ATPerformance> iLongPut = longPuts.iterator();
				while (iLongPut.hasNext()) {
					ATPerformance longPut = iLongPut.next();
					double longPutCount = longPut.getSzTrade();
					ATOptionDescription shortPutOption = (ATOptionDescription) shortPut.get(OPTION);
					ATOptionDescription longPutOption = (ATOptionDescription) longPut.get(OPTION);
					if (longPutCount <= 0 || longPutOption.getExpiration().before(shortPutOption.getExpiration()))
						continue;
					double uncoveredCount = Math.max(0, -shortPutCount - longPutCount);
					double coveredCount = -shortPutCount - uncoveredCount;
					if (coveredCount > 0) {
						longPutCount -= coveredCount;
						if (longPutCount <= 0) {
							iLongPut.remove();
						} else {
							longPut.setSzTrade(longPutCount - coveredCount);
						}
						double longPutRisk = 100 * coveredCount * longPutOption.getStrike();
						ATPerformance coveringLongPut = new ATPerformance();
						coveringLongPut.putAll(longPut);
						coveringLongPut.setSzTrade(coveredCount);
						coveringLongPut.setPutRisk(longPutRisk);
						ATPerformance previousLongPut = perfMap.get(coveringLongPut.getHashStr(false));
						if (previousLongPut != null) {
							previousLongPut.setSzTrade(previousLongPut.getSzTrade() + coveredCount);
							previousLongPut.setPutRisk(previousLongPut.getPutRisk() + longPutRisk);
						} else {
							perfMap.put(coveringLongPut.getHashStr(false), coveringLongPut);
						}
						double shortPutRisk = -100 * coveredCount * shortPutOption.getStrike();
						ATPerformance coveredShortPut = new ATPerformance();
						coveredShortPut.putAll(shortPut);
						coveredShortPut.setSzTrade(-coveredCount);
						coveredShortPut.setPutRisk(shortPutRisk);
						ATPerformance previousShortPut = perfMap.get(coveredShortPut.getHashStr(false));
						if (previousShortPut != null) {
							previousShortPut.setSzTrade(previousShortPut.getSzTrade() - coveredCount);
							previousShortPut.setPutRisk(previousShortPut.getPutRisk() + shortPutRisk);
						} else {
							perfMap.put(coveredShortPut.getHashStr(false), coveredShortPut);
						}
						shortPutCount += coveredCount;
						shortPut.setSzTrade(shortPutCount);
						if (shortPutCount >= 0)
							break;
					}
				}
				if (shortPutCount < 0) {
					shortPut.setPutRisk(pseudoInfinity);
					perfMap.put(shortPut.getHashStr(false), shortPut);
				}
			}
			for (ATPerformance longCall : longCalls) {
				Double longCallCount = longCall.getSzTrade();
				if (longCallCount != null && longCallCount != 0)
					perfMap.put(longCall.getHashStr(false), longCall);
			}
			for (ATPerformance longPut : longPuts) {
				Double longPutCount = longPut.getSzTrade();
				if (longPutCount != null && longPutCount != 0)
					perfMap.put(longPut.getHashStr(false), longPut);
			}

			// 4. Collect
			double totalValue = 0;
			double totalCallRisk = 0;
			double totalPutRisk = 0;
			Set<ATPerformance> details = new TreeSet<>(new ATPerformance.ATPerformanceComparator());
			for (ATPerformance detail : perfMap.values()) {
				if (detail.getSzTrade() == 0)
					continue;
				Double value = detail.getValueTotal();
				if (value != null && value != 0) {
					totalValue += detail.getValueTotal();
				} else {
					detail.remove(ATPerformance.VALUE_TOTAL);
				}
				Object callRisk = detail.getCallRisk();
				if (callRisk != null)
					totalCallRisk += (Double) callRisk;
				Object putRisk = detail.getPutRisk();
				if (putRisk != null)
					totalPutRisk += (Double) putRisk;
				detail.remove(DEBIT);
				detail.remove(OPTION);
				details.add(detail);
			}
			double totalUlRisk = 0;
			double totalRisk = totalUlRisk + totalCallRisk + totalPutRisk;
			perf.setDetails(details);
			if (totalValue != 0)
				perf.setValueTotal(totalValue);
			if (totalCash > 0)
				perf.setPxTradeTotal(totalCash);
			if (underlyingCount > 0) {
				perf.setSzTrade(underlyingCount);
				perf.setPxTrade(totalCash / underlyingCount);
			}
			if (totalRisk > 0)
				perf.setRiskTotal(totalRisk);
			if (totalCallRisk != 0)
				perf.setCallRisk(totalCallRisk);
			if (totalPutRisk != 0)
				perf.setPutRisk(totalPutRisk);
			performances.add(perf);
		}
		return performances;
	}

	public void testNotification(String userId_) {
		log.warn(String.format("testNotification Not implemented"));
		ATBaseNotification note = new ATTestNotification();
		note.addUserId(userId_);
		PushNotification push = ATProtoPushNotificationWrapper.toNotification(note, new Date());
		_kafkaPushNotificationProducer.send(push);
		// ATSourceServiceRegistry.getNotificationProcessor().push(Arrays.asList(note));
	}

	public Set<String> getExpirations() {
		Set<String> expirations = ATSourceServiceRegistry.getSymbolProvider().getExpirations();
		String dateStr = ATFormats.DATE_yyyy_MM_dd.get().format(new Date());
		expirations = expirations.stream().filter(e -> e.compareTo(dateStr) >= 0)
				.collect(Collectors.toCollection(TreeSet::new));

		return expirations;
	}

	public ATSecurityPack getSecurity(String userId_, String symbol_, Period period_, String... flags_) {
		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		String symbol = ATModelUtil.getSymbol(symbol_);

		// if (isDynamic_) {
		// _userSessionCtrl.processDynamicSymbols(userId_, false, symbol);
		// }

		IATMarketData symMkt = mktSvc.getMarketData(symbol);
		if (symMkt == null) {
			Map<String, ATMarketData> ulMap = mktSvc.getOptionUnderlying(symbol);
			symMkt = ulMap != null ? ulMap.get(symbol) : null;
		}
		if (symMkt == null)
			return null;
		ATMarketData mktUl = new ATMarketData(symMkt);

		ATPersistentEquityFoundation pef = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(symbol);
		if (pef == null) {
			log.warn(String.format("getSecurity Missing fundamentals [%s]", symbol_));
		}
		ATFundamental fnd = pef != null ? new ATFundamental(pef, avgMode, hvMode) : null;
		ATEquityDescription dsc = pef != null ? new ATEquityDescription(pef) : null;

		Map<String, ATEquityDescription> dscMap = Collections.singletonMap(symbol, dsc);
		Map<String, ATFundamental> fndMap = fnd != null ? Collections.singletonMap(symbol, fnd) : null;
		Map<String, ATMarketData> mktMap = new TreeMap<>();
		mktMap.put(symbol_, mktUl);
		// Collections.singletonMap(occId_, mkt);
		ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);

		List<String> flags = flags_ == null ? null : Arrays.asList(flags_);
		boolean isHistory = period_ != null && flags != null && (flags.contains("ohlc") || flags.contains("close"));
		if (isHistory) {
			Map<String, List<ATHistoryData>> hstMap = getHistoryMap(userId_, symbol_, period_, flags);
			pack.setHistory(hstMap);
		}

		boolean isOptionCalls = flags_ != null && flags.contains("call");
		boolean isOptionPuts = flags_ != null && flags.contains("put");
		if (isOptionCalls || isOptionPuts) {
			OptionRight right = isOptionCalls ? (isOptionPuts ? null : OptionRight.CALL) : OptionRight.PUT;
			ATSecurityPack optionPack = mktSvc.getOptionChain(symbol, right);
			if (optionPack != null) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> callChain = optionPack.getCallChain();
				if (callChain != null) {
					pack.setCallChain(callChain);
				}
				TreeMap<String, TreeMap<String, TreeSet<String>>> putChain = optionPack.getPutChain();
				if (putChain != null) {
					// Map<String, Map<String, Set<ATMarketData>>> putMap = Collections.singletonMap(symbol, puts);
					pack.setPutChain(putChain);
				}
			}
			Map<String, ATMarketData> optionMktMap = optionPack.getMarketMap();
			if (optionMktMap != null) {
				// optionMktMap.put(o/, value)
				mktMap.putAll(optionMktMap);

			}
		}

		boolean isOptionExp = flags_ != null && flags.contains("opt_exp");
		if (isOptionExp) {
			Map<String, ATOptionExpiration> oMap = ATSourceServiceRegistry.getSymbolProvider()
					.getOptionExpirations(Arrays.asList(symbol));
			ATOptionExpiration exp = oMap != null ? oMap.get(symbol) : null;
			Map<String, List<ATOptionExpirationDate>> expMap = exp != null
					? Collections.singletonMap(symbol, exp.getExpirations())
					: null;
			if (expMap != null) {
				pack.setOptionExpirations(expMap);
			}
		}
		return pack;
	}

	public ATOptionExpiration getOptionExpirations(String symbol_) {
		Map<String, ATOptionExpiration> oMap = ATSourceServiceRegistry.getSymbolProvider()
				.getOptionExpirations(Arrays.asList(symbol_));
		ATOptionExpiration exp = oMap.get(symbol_);
		return exp;
	}

	private Map<String, List<ATHistoryData>> getHistoryMap(String userId_, String symbol_, Period period_,
			List<String> flags_) {
		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		Period period = period_ != null ? period_ : Period.ofYears(1);
		LocalDate startDate = (LocalDate) period.subtractFrom(LocalDate.now());
		String startDateStr = ATFormats.formatYYYYMMDD(startDate);
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.OCCID, symbol_).and().gte(
				IATAssetConstants.DATE_STR,
				startDateStr);
		Map<String, List<ATHistoryData>> historyMap = mktSvc.getHistory(query, flags_, avgMode, hvMode);
		return historyMap;
	}

	public ATSecurityPack getPortfolio(String userId_, String symbol_, boolean force_) {
		log.debug(String.format("getPortfolio [%s]", userId_));
		long start = System.currentTimeMillis();
		final String traceTimeFmt = "getPortfolio [%s] - %-6s: %s";
		List<IATPortfolio> portfolio = ATSourceServiceRegistry.getPortfolioService().getPortfolio(userId_, symbol_);
		if (portfolio == null || portfolio.isEmpty())
			return null;

		final String avgMode = IATStatistics.SMA60;
		final String hvMode = IATStatistics.HV60;
		_todayDS8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());

		log.debug(String.format(traceTimeFmt, userId_, "pfo",
				DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
		start = System.currentTimeMillis();

		Set<String> symbols = new TreeSet<>();
		Map<String, List<ATPortfolio>> prtMap = new HashMap<>();
		Map<String, ATFundamental> fndMap = new HashMap<>();
		Map<String, ATEquityDescription> dscMap = new HashMap<>();
		Map<String, ATMarketData> mktMap = new HashMap<>();
		IATEquityFoundationProvider fndSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATUSTreasuryRateProvider usTrSvc = ATSourceServiceRegistry.getUSTreasuryRateProvider();
		Double interest = usTrSvc.getRiskFreeInterestRate();
		if (interest == null) {
			log.error(String.format("Treasury interest rate unavailable!"));
		}

		for (IATPortfolio ipf : portfolio) {
			String occId = ipf.getOccId();
			ATPortfolio pf = new ATPortfolio(ipf);
			List<ATPortfolio> pfs = prtMap.get(occId);
			if (pfs == null) {
				pfs = new ArrayList<>();
				prtMap.put(occId, pfs);
			}
			pfs.add(pf);
			ATMarketData mkt = mktSvc.getMarketData(occId);
			if (mkt != null) {
				mktMap.put(occId, mkt);
			}
			String sym = ipf.getUnderlying();
			symbols.add(sym);
			if (!mktMap.containsKey(sym)) {
				mkt = mktSvc.getMarketData(sym);
				if (mkt != null) {
					mktMap.put(sym, mkt);
				}
			}
		}
		log.debug(String.format(traceTimeFmt, userId_, "mkt",
				DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
		start = System.currentTimeMillis();

		// Populate Symbol Foundation and Market
		for (String symbol : symbols) {
			// Foundation
			ATPersistentEquityFoundation foundation = fndSvc.getFoundation(symbol);
			if (foundation == null) {
				log.warn(String.format("Missing foundation [%s]", symbol));
				continue;
			}
			if (!fndMap.containsKey(symbol)) {
				ATFundamental fnd = new ATFundamental(foundation, avgMode, hvMode);
				fnd.setModeAvg(avgMode);
				fnd.setModeHV(hvMode);
				fnd.setPxAvg(foundation.getAvg(avgMode));
				fnd.setPxHV(foundation.getHV(hvMode));
				String divExDS8 = fnd.getDividendExDS8();
				if (divExDS8 != null && _todayDS8.compareTo(divExDS8) > 0) {
					fnd.setDividendExDS8(null);
					fnd.setDividend(null);
					fnd.setDividendFrequency(null);
				}
				String earnDS8 = fnd.getEarningsDS8();
				if (earnDS8 != null && _todayDS8.compareTo(earnDS8) > 0) {
					fnd.setEarningsDS8(null);
				}
				fndMap.put(symbol, fnd);

				Map<String, ATMarketData> symMktMap = portfolio.stream()
						.filter(p -> p.getUnderlying().equals(symbol))
						.map(p -> p.getOccId()).distinct()
						.map(o -> mktMap.get(o)).filter(m -> m != null)
						.collect(Collectors.toMap(ATMarketData::getOccId, Function.identity()));
				ATMarketData mktUl = mktSvc.getMarketData(symbol);
				populateStatistics(mktUl, fnd, interest, symMktMap);
			}

			// Description
			if (!dscMap.containsKey(symbol)) {
				ATEquityDescription description = new ATEquityDescription(foundation);
				dscMap.put(symbol, description);
			}
		}
		log.debug(String.format(traceTimeFmt, userId_, "fnd",
				DurationFormatUtils.formatDurationHMS(System.currentTimeMillis() - start)));
		start = System.currentTimeMillis();

		// ATMarketData pick = mktMap.get("AAL 210521C00008000");
		// if (pick != null) {
		// int ii = 0;
		// }
		ATSecurityPack result = new ATSecurityPack(dscMap, fndMap, mktMap, prtMap);
		return result;
	}

	public List<ATUserAsset> getAssets(String userId_) {
		List<ATUserAsset> assets = ATSourceServiceRegistry.getPortfolioService().getAssets(userId_);
		return assets;
	}

	public ATSecurityPack getDigest(String userId_, String symbol_) {
		IATPortfolioService pfSvc = ATSourceServiceRegistry.getPortfolioService();
		List<ATTradeDigest> digests = pfSvc.getTradeDigest(userId_, symbol_);
		List<ATTradeGroup> groups = pfSvc.getTradeGroups(userId_, symbol_);
		if (digests == null || digests.isEmpty() || groups == null || groups.isEmpty())
			return null;

		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();

		Map<String, List<ATTradeDigest>> digestMap = digests.stream()
				.collect(Collectors.groupingBy(ATTradeDigest::getUnderlying));
		Map<String, List<ATTradeGroup>> groupMap = groups.stream()
				.collect(Collectors.groupingBy(ATTradeGroup::getUnderlying));
		Map<String, ATFundamental> fndMap = new HashMap<>();
		Map<String, ATEquityDescription> dscMap = new HashMap<>();
		Map<String, ATMarketData> mktMap = new HashMap<>();

		for (Entry<String, List<ATTradeDigest>> entry : digestMap.entrySet()) {
			String symbol = entry.getKey();
			ATMarketData mktUl = mktSvc.getMarketData(symbol);
			if (mktUl != null) {
				mktMap.put(symbol, mktUl);
			}
			Map<String, ATFundamental> fnd = fndSvc.getFundamentals(symbol);
			if (fnd != null) {
				fndMap.putAll(fnd);
			}
			ATPersistentEquityFoundation fndOld = fndSvcOld.getFoundation(symbol);
			if (fndOld != null) {
				ATEquityDescription description = new ATEquityDescription(fndOld);
				dscMap.put(symbol, description);
			}
			for (ATTradeDigest digest : entry.getValue()) {
				String occId = digest.getOccId();
				if (mktMap.containsKey(occId))
					continue;
				ATMarketData mkt = mktSvc.getMarketData(occId);
				if (mkt != null) {
					mktMap.put(occId, mkt);
				}
			}
		}
		ATSecurityPack result = new ATSecurityPack(dscMap, fndMap, mktMap);
		result.setDigest(digestMap);
		result.setStrategy(groupMap);
		return result;
	}

	public ATSecurityPack getTrades(String userId_, boolean isActive_, String symbol_) {
		// final String avgMode = IATStatistics.SMA60;
		/// final String hvMode = IATStatistics.HV60;
		if (StringUtils.isBlank(userId_))
			return null;

		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();

		_todayDS8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		log.debug(String.format("subscribe [%s]", userId_));
		IATWhereQuery query = ATPersistenceRegistry.query();
		if (isActive_) {
			LocalDate today = LocalDate.now().minusWeeks(1);
			Long nowL = ATFormats.dateToNumber(today);
			query = query.lt(IATAssetConstants.SIZE_ACTIVE, 0).or().gt(IATAssetConstants.SIZE_ACTIVE, 0).or()
					.gte(IATAssetConstants.TS_TRADE, nowL);
		}
		query = query.and().eq(IATAssetConstants.TRADE_USER, userId_);
		if (StringUtils.isNotBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		List<IATTrade> dbTrades = ATSourceServiceRegistry.getPortfolioService().getTrades(query);

		Map<String, ATFundamental> fndMap = new HashMap<>();
		Map<String, ATEquityDescription> dscMap = new HashMap<>();
		Map<String, ATMarketData> mktMap = new HashMap<>();
		// Trades
		Map<String, List<ATTrade>> tradeMap = new TreeMap<>();
		for (IATTrade trade : dbTrades) {
			String occId = trade.getOccId();
			ATMarketData mkt = mktSvc.getMarketData(occId);
			if (mkt != null) {
				mktMap.put(occId, mkt);
			}
			String symbol = ATFormats.toSymbol(occId);
			List<ATTrade> trades = tradeMap.get(symbol);
			if (trades == null) {
				trades = new ArrayList<>();
				tradeMap.put(symbol, trades);
			}
			trades.add(new ATTrade(trade));
			if (!mktMap.containsKey(symbol)) {
				ATMarketData mktUl = mktSvc.getMarketData(symbol);
				if (mktUl != null) {
					mktMap.put(symbol, mktUl);
				}
			}
			if (!fndMap.containsKey(symbol)) {
				Map<String, ATFundamental> fnd = fndSvc.getFundamentals(symbol);
				if (fnd != null) {
					fndMap.putAll(fnd);
				}
				ATPersistentEquityFoundation fndOld = fndSvcOld.getFoundation(symbol);
				if (fndOld != null) {
					ATEquityDescription description = new ATEquityDescription(fndOld);
					dscMap.put(symbol, description);
				}
			}
		}
		ATSecurityPack result = new ATSecurityPack(dscMap, fndMap, mktMap);
		result.setTrades(tradeMap);
		return result;
	}

	public boolean saveTrades(String uid_, ATTrade[] trades_, boolean isForce_) {
		// NOTE: Assumption all trades share same Underlying
		List<ATTrade> trades = Arrays.asList(trades_);
		Collections.sort(trades);
		Double pxUl = null;
		for (ATTrade trade : trades) {
			String occId = trade.getOccId();
			if (occId.length() < 10) {
				pxUl = trade.getPxTrade();
				continue;
			}
			if (pxUl == null) {
				String symbol = ATModelUtil.getSymbol(trade.getOccId());
				ATTrade ult = _alpacaAdp.getTrade(symbol, trade.getTsTrade());
				if (ult == null) {
					log.warn(String.format("Failed to get UL market data [%s]", symbol));
				}
				pxUl = ult != null ? ult.getPxTrade() : -1;
			}
			if (pxUl != null && pxUl > 0) {
				trade.setPxUl(pxUl);
			} else {
				log.debug(String.format("Unable to retrieve last pxUl for [%s / %s]", occId,
						ATFormats.DATE_TIME_mid.get().format(trade.getTsTrade())));
			}
		}
		boolean result = ATSourceServiceRegistry.getPortfolioService().saveTrades(uid_, trades_);
		return result;
	}

	public boolean saveTradeTraceGroup(ATTradeTraceGroup trace_, String replaceId_) {
		var prtSvc = ATSourceServiceRegistry.getPortfolioService();
		var replaceId = replaceId_ != null ? NumberUtils.toLong(replaceId_) : null;
		var savedCount = prtSvc.saveTradeTrace(trace_, replaceId);
		return savedCount == 1;
	}

	public boolean deleteTradeTrace(String uid_, Long id) {
		var prtSvc = ATSourceServiceRegistry.getPortfolioService();
		var result = prtSvc.deleteTradeTrace(uid_, id);
		return result;
	}

	public ATUserAlertRulePack saveUserAlerts(String userId_, ATUserAlertRulePack pack_) {
		IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		boolean isSaved = userSvc.saveUserAlerts(userId_, pack_);
		if (!isSaved)
			return null;
		AlertRule rule = ATProtoAlertRuleWrapper.toAlertRule(pack_);
		_kafkaAlertRuleProducer.send(rule);
		List<ATUserAlertRulePack> packs = userSvc.getUserAlerts(userId_, pack_.occId);
		ATUserAlertRulePack pack = packs != null && packs.size() == 1 ? packs.get(0) : null;
		return pack;
		// List<ATAlertRule> result_digest = alertMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		// return result_digest;
	}

	private void populateStatistics(ATMarketData mktUl_, ATFundamental fnd_, Double interest_,
			Map<String, ATMarketData> mktMap_) {
		if (fnd_ == null || mktMap_ == null)
			return;
		try {
			Map<String, ATOptionDescription> optionMap = mktMap_.keySet().stream()
					.filter(k -> k.length() > 6).map(occId -> ATOptionDescription.create(occId))
					.collect(Collectors.toMap(ATOptionDescription::getOccId, Function.identity()));
			if (optionMap.isEmpty())
				return;
			String symbol = fnd_.getOccId();
			// ATMarketData mktUl = mktMap_.get(symbol);
			Double pxUl = mktUl_ != null ? mktUl_.getPxLast() : null;
			if (pxUl == null) {
				log.error(String.format("Missing market data for [%s]", symbol));
				return;
			}
			Double iv = fnd_.getIV();
			Double hv = fnd_.getPxHV();
			if (hv == null) {
				return;
			}
			double interest = interest_ != null ? interest_ : 0;
			double volatility = iv != null ? iv : fnd_.getPxHV();
			Double div = fnd_.getDividend();
			Integer divFq = fnd_.getDividendFrequency();
			double yield = div == null || divFq == null ? 0.0 : div * divFq / pxUl;
			Double pxHv = fnd_.getPxHV();
			Map<String, ATOptionAnalytics> analyticsMap = ATQuantLib.instance().priceAmericanEquityOption(pxUl,
					volatility, yield,
					interest, optionMap.values().toArray(new ATOptionDescription[0]));
			for (String occId : analyticsMap.keySet()) {
				ATMarketData mkt = mktMap_.get(occId);
				if (mkt != null) {
					ATOptionAnalytics stat = analyticsMap.get(occId);
					mkt.setPxTheo(stat.getPrice());

					ATOptionDescription option = optionMap.get(occId);
					Double risk = ATMathUtil.calculateProbability(occId, pxUl, pxHv, option, NOW);
					mkt.setRisk(risk);
				}
			}
		} catch (Exception ex) {
			log.error(String.format("Failed running statistics [%s]", fnd_.getOccId()), ex);
		}
	}

	public Map<String, Object> assessPerformance(String userId_) {
		Map<String, Map<String, Double>> assetMap = ATSourceServiceRegistry.getPortfolioService()
				.assessPerformance(userId_);
		Map<String, Map<String, List<Double>>> resultMap = new TreeMap<>();
		IATSecuritiesProvider secSvc = ATSourceServiceRegistry.getSecuritiesProvider();
		Double grandTotal = 0.0;
		Set<String> errorSymbols = new TreeSet<>();
		for (Entry<String, Map<String, Double>> asset : assetMap.entrySet()) {
			String symbol = asset.getKey();
			if (log.isTraceEnabled())
				log.trace(String.format("%-6s%s", symbol, "--------------------"));
			Map<String, List<Double>> assetPerformance = resultMap.get(symbol);
			if (assetPerformance == null) {
				assetPerformance = new TreeMap<>();
				resultMap.put(symbol, assetPerformance);
			}
			Map<String, Double> positions = asset.getValue();
			Double total = 0.0;
			ATPersistentSecurity underlying = secSvc.get(symbol);
			Double last = underlying != null ? underlying.getPrice() : null;
			for (Entry<String, Double> position : positions.entrySet()) {
				String occId = position.getKey();
				Double count = position.getValue();
				String label;
				Double price;
				boolean isCash = false;
				if (IATPortfolioService.CASH_KEY.equals(occId)) {
					label = IATPortfolioService.CASH_KEY;
					price = 1.0;
					isCash = true;
				} else if ("@AGG".equals(symbol) || "@DATE".equals(symbol)) {
					label = occId;
					price = 1.0;
					isCash = true;
				} else {
					label = ATFormats.toLabel(occId);
					ATPersistentSecurity sec = secSvc.get(occId);
					price = sec != null ? sec.getPrice() : null;
				}
				Double risk = ATModelUtil.getStrike(occId);
				if (price != null) {
					Double value = price * count;
					if (occId.length() > 6) {
						value *= 100;
						if (risk != null) {
							risk *= (100.0 * count);
						}
					} else if (!isCash) {
						risk = value;
					} else {
						risk = last;
					}
					assetPerformance.put(label,
							risk == null ? Arrays.asList(value)
									: isCash ? Arrays.asList(value, risk) : Arrays.asList(value, risk, count));
					total += value;
					if (log.isTraceEnabled())
						log.trace(String.format("      %-18s : %10.2f", label, value));
				} else {
					assetPerformance.put(String.format("%s *", label),
							risk == null ? Arrays.asList(0.0) : Arrays.asList(0.0, risk, count));
					if (log.isTraceEnabled())
						log.trace(String.format("      %-18s : [%10.2f]", label, count));
				}
			}
			grandTotal += total;
			if (log.isTraceEnabled())
				log.trace(String.format("      %-18s | %10.2f", "", total));
		}
		log.debug(String.format("%-6s%s %10.2f", "......", "...............TOTAL", grandTotal));
		log.debug(String.format("Errors %s", errorSymbols));

		Map<String, Object> result = new HashMap<>();
		result.put("performance", resultMap);
		result.put("errors", errorSymbols);
		return result;
	}

	@Override
	public void notify(EATChangeOperation operation_, IATTrade trade_) {
		log.debug(String.format("Trade event [%s]: %s", operation_, trade_));

		String userId = trade_.getTradeUserId();
		String symbol = trade_.getOccId();
		if (symbol == null) {
			log.error(String.format("Missing symbol: %s", trade_));
		}
		if (symbol.length() > 6) {
			symbol = symbol.substring(0, 6).trim();
		}
		long delay = ATConfigUtil.config().getLong("portfolio.broadcast.delay");
		DelayedTradeToken token = new DelayedTradeToken(userId, symbol, delay);
		if (!_tradeTokenQueue.contains(token)) {
			// NOTE: Monitored by 'processTradeQueue'
			_tradeTokenQueue.offer(token);
		}
	}

	@Override
	public void notify(IATMarketData mkt_) {
		String occId = mkt_.getAtId();
		if (occId != null && occId.length() < 10) {
			runEconomicEventNotifications(mkt_.getAtId());
		}
	}

	Map<String, Long> _economicEventThrottleMap = new HashMap<>();

	private void runEconomicEventNotifications(String symbol_) {
		Configuration config = ATConfigUtil.config();
		long eventPeriod = config.getLong("alerts.economic.throttle");
		Long ts = this._economicEventThrottleMap.get(symbol_);
		if (ts != null && System.currentTimeMillis() < ts)
			return;
		this._economicEventThrottleMap.put(symbol_, System.currentTimeMillis() + eventPeriod);

		// if(true) return;
		// final String avgMode = IATStatistics.SMA60;
		// final String hvMode = IATStatistics.HV60;
		// ATMarketData mkt = mkt_ instanceof ATMarketData ? (ATMarketData) mkt_ : new ATMarketData(mkt_);
		// String occId = mkt.getOccId();
		// if ("CSCO".equals(occId)) {
		// int i = 0;
		// }
		// Set<String> userIds = new HashSet<>();// ATSourceServiceRegistry.getPortfolioService().getOwnerIds(occId);
		//
		// Set<String> taskOwners = _taskSvc.getOwnerIds(occId);
		// if (taskOwners != null & !taskOwners.isEmpty()) {
		// if (userIds == null) {
		// userIds = taskOwners;
		// } else {
		// userIds.addAll(taskOwners);
		// }
		// }

		IATPortfolioService prtSvc = ATSourceServiceRegistry.getPortfolioService();
		// prtSvc.getOwnerIds(symbol_);
		// IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		Set<String> userIds = prtSvc.getAffiliatedUserIds(symbol_);
		// userSvc.getUsers().stream().map(u -> u.getAtId()).collect(Collectors.toSet());
		// Set<String> activeUsers = userSvc.getActiveUserIds();
		if (userIds == null || userIds.isEmpty())
			// || activeUsers == null || activeUsers.isEmpty())
			return;

		// final Set<String> finalUserIds = userIds;
		// Callable<Long> call = new Callable<Long>() {
		//
		// @Override
		// public Long call() throws Exception {
		// long start = System.currentTimeMillis();
		// Set<String> threadUids = new HashSet<String>(finalUserIds);
		// threadUids.retainAll(activeUsers);
		// if (!threadUids.isEmpty()) {
		// WSMessageWrapper message = new WSMessageWrapper();
		// message.setSource("source");
		// message.setType("mkt");
		// message.setFunction("mkt");
		// // ATMarketData mkt = new ATMarketData(data_);
		// if (occId.length() > 6) {
		// IATMarketData iulMkt = ATSourceServiceRegistry.getMarketDataService().getMarketData(mkt.getUnderlying());
		// if (iulMkt != null) {
		// ATMarketData ulMkt = new ATMarketData(iulMkt);
		// double interest = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
		// ATPersistentEquityFoundation ef = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(mkt.getUnderlying());
		// ATFundamental fnd = new ATFundamental(ef, avgMode, hvMode);
		// fnd.setPxHV(ef.getHV60());
		// Map<String, ATMarketData> map = ImmutableMap.of(ulMkt.getOccId(), ulMkt, occId, mkt);
		// populateStatistics(fnd, interest, map);
		// }
		// }
		// message.setData(Arrays.asList(mkt));
		// //IATWebSocketService wsSvc = ATSourceServiceRegistry.getWebSocketService();
		// ATWebSocketServiceJavalin wsSvc = ATWebSocketServiceJavalin.instance();
		// String userStr = String.join(",", threadUids);
		// message.setTarget(userStr);
		// wsSvc.send(message);
		// }
		//
		// // Criteria & Alert Notifications
		// Set<ATBaseNotification> notifications = ATSourceServiceRegistry.getAlertProcessor().process(mkt, threadUids);
		// Set<String> failedTokens = ATSourceServiceRegistry.getNotificationProcessor().push(notifications);
		// if (failedTokens != null) {
		// userSvc.removeMessageTokens(failedTokens);
		// }
		// return System.currentTimeMillis() - start;
		// }
		// };
		// Future<Long> fut = ATExecutors.submit(String.format("ATSourceController.notify[mkt]"), call);
		// try {
		// Long duration = fut.get();
		// Object p = duration;
		// } catch (InterruptedException | ExecutionException ex) {
		// log.error("Error", ex);
		// }

		// NOTE: Check statistics and projections!

		// userIds.retainAll(activeUsers);
		// if (!userIds.isEmpty()) {
		// WSMessageWrapper message = new WSMessageWrapper();
		// message.setSource("source");
		// message.setType("mkt");
		// message.setFunction("mkt");
		// // ATMarketData mkt = new ATMarketData(data_);
		// if (occId.length() > 6) {
		// IATMarketData iulMkt = ATSourceServiceRegistry.getMarketDataService().getMarketData(mkt.getUnderlying());
		// if (iulMkt != null) {
		// ATMarketData ulMkt = new ATMarketData(iulMkt);
		// double interest = ATSourceServiceRegistry.getUSTreasuryRateProvider().getRiskFreeInterestRate();
		// ATPersistentEquityFoundation ef = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundation(mkt.getUnderlying());
		// ATFundamental fnd = new ATFundamental(ef, avgMode, hvMode);
		// fnd.setPxHV(ef.getHV60());
		// Map<String, ATMarketData> map = ImmutableMap.of(ulMkt.getOccId(), ulMkt, occId, mkt);
		// if (false)
		// populateStatistics(fnd, interest, map);
		// }
		// }
		// message.setData(Arrays.asList(mkt));
		//
		// IATWebSocketService wsSvc = ATWebSocketServiceJavalin.instance();// : ATSourceServiceRegistry.getWebSocketService();
		// String userStr = String.join(",", userIds);
		// message.setTarget(userStr);
		// wsSvc.send(message);
		// }

		// Criteria & Alert Notifications
		Set<ATBaseNotification> notifications = ATSourceServiceRegistry.getAlertProcessor()
				.processEconomicEvents(symbol_, userIds);
		if (notifications != null && !notifications.isEmpty()) {
			for (ATBaseNotification notification : notifications) {
				PushNotification push = ATProtoPushNotificationWrapper.toNotification(notification, new Date());
				_kafkaPushNotificationProducer.send(push);
			}
			// Set<String> failedTokens = ATSourceServiceRegistry.getNotificationProcessor().push(notifications);
			// if (failedTokens != null) {
			// userSvc.removeMessageTokens(failedTokens);
			// }
		}
	}

	@Override
	public void notify(EATMarketStatus status_) {
		log.info(String.format("Market status [%s]", status_));
	}

	@Override
	public void notify(EATChangeOperation operation_, ATSession session_) {
		log.info(String.format("Session event [%s]: %s", operation_, session_));
	}

	@Override
	public void notify(EATChangeOperation ignore1, ATSymbol ignore2) {
	}

	@Override
	public void notifyDynamicSymbols(Map<String, ATSymbol> dynamicSymbols_, Map<String, Set<String>> ignore) {
		if (dynamicSymbols_ == null || dynamicSymbols_.isEmpty())
			return;
		String[] symbols = dynamicSymbols_.keySet().toArray(new String[0]);
		//	getDividends(symbols);
	}

	private void pushUpdates() {
		try {
			Set<String> uids = ATServiceRegistry.getUserService().getActiveUserIds();
			if (uids == null || uids.isEmpty())
				return;
			pushMarketUpdates();
			pushMovers();
			pushTrends();
		} catch (Exception ex) {
			log.error("pushUpdates Error", ex);
		}
	}

	private ATSecurityPack getTrends() {
		try {
			String json = ATHttpUtil.get("http://192.168.8.99:5000/api/v1/command/symbol_trend", null);
			if (StringUtils.isBlank(json))
				return null;
			JsonObject jo = (new Gson()).fromJson(json, JsonObject.class);
			JsonElement je = jo.get("data");
			if (je == null || !je.isJsonArray())
				return null;
			IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
			IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
			IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();

			Map<String, ATMarketData> mktMap = new HashMap<>();
			Map<String, ATEquityDescription> dscMap = new TreeMap<>();
			Map<String, ATFundamental> fndMap = new TreeMap<>();

			for (JsonElement element : je.getAsJsonArray()) {
				String symbol = element.getAsString();
				IATMarketData imkt = mktSvc.getMarketData(symbol);
				Double pxLast = imkt != null ? imkt.getPxLast() : null;
				if (pxLast == null || pxLast < 5)
					continue;
				mktMap.put(symbol, new ATMarketData(imkt));
				Map<String, ATFundamental> fnd = fndSvc.getFundamentals(symbol);
				if (fnd != null) {
					fndMap.putAll(fnd);
				}
				ATPersistentEquityFoundation fndOld = fndSvcOld.getFoundation(symbol);
				if (fndOld != null) {
					ATEquityDescription description = new ATEquityDescription(fndOld);
					dscMap.put(symbol, description);
				}
			}
			if (mktMap.isEmpty())
				return null;
			log.debug(String.format("Broadcasting trend pack"));
			ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);
			return pack;
		} catch (Exception ex_) {
			log.error(String.format("getTrends Error"), ex_);
			return null;
		}
	}

	private void pushTrends() {
		try {
			ATSecurityPack pack = getTrends();
			Map<String, ATMarketData> trends = pack != null ? pack.getMarketMap() : null;
			if (trends == null || trends.isEmpty())
				return;

			sendSecurityPackSseMessage(ATProtoSecurityPackWrapper.TYPE_TREND_PACK, pack);
			// sendMarketDataSseMessage(ATProtoMarketDataWrapper.TYPE_TRENDS, trends.values());
			String[] occIds = trends.keySet().toArray(new String[trends.size()]);
			_userSessionCtrl.processDynamicSymbols(IATUser.USER_TRENDS, ATProtoMarketDataWrapper.TYPE_TRENDS, true,
					occIds);
		} catch (Exception ex_) {
			log.error(String.format("pushTrends Error"), ex_);
		}
	}

	private void sendMarketDataSseMessage(String type_, Collection<ATMarketData> mkt_) {
		if (StringUtils.isBlank(type_) || mkt_ == null || mkt_.isEmpty())
			return;
		MarketDataPack.Builder packBuilder = MarketDataPack.newBuilder();
		packBuilder.setType(type_);
		packBuilder.setSource(SOURCE_KEY);
		for (ATMarketData mkt : mkt_) {
			packBuilder.addMarketData(ATProtoMarketDataWrapper.toMarketData(mkt));
		}
		MarketDataPack trendPack = packBuilder.build();

		SseMessage.Builder builder = SseMessage.newBuilder();
		Any anyTrendPack = Any.pack(trendPack);
		builder.setData(anyTrendPack);
		builder.setType("mktPack");
		builder.addTargets(WSMessageWrapper.TARGET_ALL);
		SseMessage msg = builder.build();
		_kafkaSseProducer.send(msg);
	}

	private void sendSecurityPackSseMessage(String type_, ATSecurityPack pack_) {
		if (StringUtils.isBlank(type_) || pack_ == null)
			return;
		SecurityPack pack = ATProtoSecurityPackWrapper.toSecurityPack(pack_);

		SseMessage.Builder builder = SseMessage.newBuilder();
		Any anySecurityPack = Any.pack(pack);
		builder.setData(anySecurityPack);
		builder.setType(type_);
		builder.addTargets(WSMessageWrapper.TARGET_ALL);
		SseMessage msg = builder.build();
		_kafkaSseProducer.send(msg);
	}

	// NOTE: Index and Sector updates
	private void pushMarketUpdates() {
		try {
			Configuration config = ATConfigUtil.config();
			String indexStr = config.getString("source.index");
			Set<String> ids = StringUtils.isBlank(indexStr)
					? new TreeSet<>()
					: new TreeSet<>(Arrays.asList(indexStr.trim().split("\\s*,\\s*")));
			String sectorStr = config.getString("source.sector");
			if (!StringUtils.isBlank(sectorStr)) {
				ids.addAll(Arrays.asList(sectorStr.trim().split("\\s*,\\s*")));
			}

			IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
			List<ATMarketData> mktList = new ArrayList<>();
			for (String id : ids) {
				IATMarketData imkt = mktSvc.getMarketData(id);
				if (imkt == null)
					continue;
				mktList.add(new ATMarketData(imkt));
			}
			if (mktList.isEmpty())
				return;

			sendMarketDataSseMessage(ATProtoMarketDataWrapper.TYPE_MARKET_DATA, mktList);
		} catch (Exception ex_) {
			log.error(String.format("pushMarketUpdates Error"), ex_);
		}
	}

	private ATSecurityPack getMovers() {
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();
		List<ATMarketData> moverList = mktSvc.getMovers();
		int limit = Math.min(10, moverList.size());

		Map<String, ATMarketData> mktMap = new HashMap<>();
		Map<String, ATEquityDescription> dscMap = new TreeMap<>();
		Map<String, ATFundamental> fndMap = new TreeMap<>();

		for (int i = 0; i < limit; i++) {
			Map<String, Object> map = moverList.get(i);
			ATMarketData mkt = new ATMarketData(map);
			String symbol = mkt.getAtId();
			mktMap.put(symbol, mkt);
			Map<String, ATFundamental> fnd = fndSvc.getFundamentals(symbol);
			if (fnd != null) {
				fndMap.putAll(fnd);
			}
			ATPersistentEquityFoundation fndOld = fndSvcOld.getFoundation(symbol);
			if (fndOld != null) {
				ATEquityDescription description = new ATEquityDescription(fndOld);
				dscMap.put(symbol, description);
			}
		}
		ATSecurityPack pack = new ATSecurityPack(dscMap, fndMap, mktMap);
		return pack;
	}

	private void pushMovers() {
		try {
			ATSecurityPack pack = getMovers();
			Map<String, ATMarketData> movers = pack != null ? pack.getMarketMap() : null;
			if (movers == null || movers.isEmpty())
				return;

			sendSecurityPackSseMessage(ATProtoSecurityPackWrapper.TYPE_MOVER_PACK, pack);
			// sendMarketDataSseMessage(ATProtoMarketDataWrapper.TYPE_MOVERS, movers.values());

			String[] occIds = movers.keySet().toArray(new String[movers.size()]);
			_userSessionCtrl.processDynamicSymbols(IATUser.USER_MOVERS, ATProtoMarketDataWrapper.TYPE_MOVERS, true,
					occIds);
		} catch (Exception ex_) {
			log.error(String.format("pushMovers Error"), ex_);
		}
	}

	private void processWSMessageQueue() {
		IATWebSocketService wsSvc = ATWebSocketServiceJavalin.instance();
		while (_isRunning) {
			try {
				WSMessageWrapper msg = ATWSMessageQueue.poll(1000);
				if (msg == null)
					continue;
				wsSvc.send(msg);
			} catch (Exception ex) {
				log.error(String.format("processWSMessageQueue Error", ex));
			}
		}
	}

	private void processTradeQueue() {
		Configuration config = ATConfigUtil.config();
		long delay = config.getLong("portfolio.broadcast.delay") * 10;
		IATEquityFoundationProvider fndSvc = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATPortfolioService pfSvc = ATSourceServiceRegistry.getPortfolioService();

		boolean isJavalin = config.getBoolean("web.isJavalin");
		IATWebSocketService wsSvc = isJavalin ? ATWebSocketServiceJavalin.instance()
				: ATSourceServiceRegistry.getWebSocketService();

		IATUSTreasuryRateProvider usTrSvc = ATSourceServiceRegistry.getUSTreasuryRateProvider();
		String avgMode = IATStatistics.SMA60;
		String hvMode = IATStatistics.HV60;
		WSMessageWrapper message1 = null;
		while (_isRunning) {
			try {
				DelayedTradeToken token = _tradeTokenQueue.poll(delay, TimeUnit.MILLISECONDS);
				if (token == null) {
					log.debug(String.format("Waiting for trade tokens..."));
					continue;
				}
				NOW = Instant.now().truncatedTo(ChronoUnit.DAYS);
				double interest = usTrSvc.getRiskFreeInterestRate();
				String symbol = token._symbol;

				ATPersistentEquityFoundation fnd = fndSvc.getFoundation(symbol);
				Map<String, ATFundamental> fndMap = fnd == null ? null
						: Collections.singletonMap(symbol, new ATFundamental(fnd, avgMode, hvMode));

				ATMarketData symMkt = mktSvc.getMarketData(symbol);
				Map<String, ATMarketData> mktMap = symMkt == null ? new HashMap<>()
						: Collections.singletonMap(symbol, symMkt);

				IATWhereQuery query = ATPersistenceRegistry.query()
						.eq(IATAssetConstants.TRADE_USER, token._userId)
						.eq(IATTrade.UNDERLYING, token._symbol);
				List<ATTrade> trades = pfSvc.getTrades(query).stream().map(t -> ATTrade.create(t))
						.filter(t -> t != null)
						.collect(Collectors.toList());
				// Set<String> tradeIds = new TreeSet<>();
				Map<String, List<ATTrade>> trdMap = Collections.singletonMap(symbol, trades);
				for (IATTrade trade : trades) {
					String dOccId = trade.getOccId();
					Double sz = trade.getSzTradeActive();
					if (!mktMap.containsKey(dOccId) && sz != null && sz != 0) {
						ATMarketData mkt = mktSvc.getMarketData(dOccId);
						if (mkt != null) {
							mktMap.put(dOccId, mkt);
						}
					}
					// tradeIds.add(dOccId);
				}
				if (fndMap != null) {
					populateStatistics(symMkt, fndMap.get(symbol), interest, mktMap);
				}

				ATSecurityPack pack = new ATSecurityPack(null, fndMap, mktMap);

				pack.setTrades(trdMap);
				// pack.setTradesOld(Arrays.asList(tradeGroup));
				log.debug(String.format("Broadcasting trade pack [%s/%s]", token._userId, token._symbol));
				message1 = new WSMessageWrapper();
				message1.setSource("source");
				message1.setType("trd");
				message1.setFunction(String.format("trd.%s", EATChangeOperation.SWAP));
				message1.setData(pack);
				message1.setTarget(token._userId);
				wsSvc.send(message1);

				// WSMessageWrapper message2 = new WSMessageWrapper();
				// message2.setSource("source");
				// message2.setType("trd");
				// message2.setFunction(String.format("trd.%s", EATChangeOperation.SWAP));
				// message2.setData(tradeIds);
				// message2.setTarget(token._userId);
				// wsSvc.send(message2);

			} catch (Exception ex) {
				log.error(String.format("Error processing trade update: %s", ATFormats.serialize(message1)), ex);
			}
		}
	}

	private static class DelayedTradeToken implements Delayed {

		private long _triggerTime;
		private String _userId;
		private String _symbol;

		public DelayedTradeToken(String userId_, String symbol_, long delay_) {
			_triggerTime = System.currentTimeMillis() + delay_;
			_userId = userId_;
			_symbol = symbol_;
		}

		@Override
		public int compareTo(Delayed other_) {
			if (other_ == null)
				return -1;
			long diff = this.getDelay(TimeUnit.MILLISECONDS) - other_.getDelay(TimeUnit.MILLISECONDS);
			return (int) Math.signum(Ints.saturatedCast(diff));
		}

		public long getDelay(TimeUnit delay_) {
			long diff = _triggerTime - System.currentTimeMillis();
			return delay_.convert(diff, TimeUnit.MILLISECONDS);
		}

		@Override
		public boolean equals(Object other_) {
			if (other_ == null)
				return false;

			if (!(other_ instanceof DelayedTradeToken))
				return false;
			DelayedTradeToken other = (DelayedTradeToken) other_;
			int comp = this._symbol.compareTo(other._symbol);
			if (comp == 0)
				comp = this._userId.compareTo(other._userId);
			return comp == 0;
		}

	}

	// MBean

	// private Map<String, String> getMonitoringStatus() {
	// Map<String, String> statusMap = new HashMap<>();
	// statusMap.put("tradeTokenQueueSize", (_tradeTokenQueue != null ? _tradeTokenQueue.size() : -1) + "");
	// return statusMap;
	// }

	public boolean subscribeUserNotifications(String uid_, String token_, String mode_) {
		Map<String, String> data = new HashMap<>();
		data.put("mode", mode_);
		data.put("token", token_);

		ATSession session = new ATSession();
		session.setToken(ATSession.TOKEN_ALL);
		session.setUserId(uid_);
		session.setData(data);

		Configuration config = ATConfigUtil.config();
		String topic = config.getString("kafka.user_session.topic.push_notification");
		_kafkaUserSessionProducer.send(topic, EATChangeOperation.UPDATE, session);

		return true;

		// boolean result_digest = ATSourceServiceRegistry.getNotificationProcessor().subscribeNotifications(uid_, token_, mode_);
		// if (result_digest) {
		// IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		// ATUser user = userSvc.getUser(uid_);
		// if (user != null) {
		// if (user.updateMessageToken(mode_, token_)) {
		// result_digest = userSvc.updateUser(user);
		// return result_digest;
		// }
		// }
		// }
		// return false;
	}

	// @NOTE: Gatherer
	public Map<String, IATDividend> getDividends(String[] symbols_) {
		if (symbols_ == null || symbols_.length == 0)
			return null;
		IATFundamentalsProvider fndSvc = ATSourceServiceRegistry.getFundamentalsProvider();
		Map<String, IATDividend> divMap = fndSvc.getDividends(symbols_);
		//	Map<String, IATDividend> divMap = _gatherer.getDividends(Arrays.asList(symbols_), true);
		return divMap;
	}

	// @NOTE: Gatherer
	// 	public Map<String, IATOptionExpiration> getOptionExpirations(String[] symbols_) {
	// 	Map<String, IATOptionExpiration> expSet = _gatherer.getOptionExpirations(true, symbols_);
	// 	return expSet;
	// }

	public Map<String, Set<String>> getSecurityCategories() {
		Map<String, Set<String>> cats = ATSourceServiceRegistry.getSymbolProvider().getCategories();
		return cats;
	}

	public ATUserEquityModel saveUserEquityModel(ATUserEquityModel model_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		ATUserEquityModel outModel = usrSvc.saveUserEquityModel(model_);
		return outModel;
	}

	public List<ATUserEquityModel> getUserEquityModels(String uid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		List<ATUserEquityModel> models = usrSvc.getUserEquityModels(uid_);
		return models;
	}

	public Boolean deleteUserEquityModel(String uid_, String atid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		Boolean success = usrSvc.deleteUserEquityModel(uid_, atid_);
		return success;
	}

	public ATUserOpportunityConfig saveUserOpportunityConfig(ATUserOpportunityConfig config_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		ATUserOpportunityConfig outConfig = usrSvc.saveUserOpportunityConfig(config_);
		return outConfig;
	}

	public List<ATUserOpportunityConfig> getUserOpportunityConfigs(String uid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		List<ATUserOpportunityConfig> configs = usrSvc.getUserOpportunityConfigs(uid_);
		return configs;
	}

	public Boolean deleteUserOpportunityConfig(String uid_, String atid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		Boolean success = usrSvc.deleteUserOpportunityConfig(uid_, atid_);
		return success;
	}

	public List<ATOpportunityClip> updateUserOpportunityClips(String uid_, Set<ATOpportunityClip> clips_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		List<ATOpportunityClip> outClips = usrSvc.updateOpportunityClips(uid_, clips_);
		return outClips;
	}

	public List<ATOpportunityClip> getUserOpportunityClips(String uid_, String symbol_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		List<ATOpportunityClip> configs = usrSvc.getOpportunityClips(uid_, symbol_);
		return configs;
	}

	public Boolean deleteWatchlist(String uid_, String atid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		Boolean success = usrSvc.deleteWatchlist(uid_, atid_);
		return success;
	}

	public ATWatchlist saveWatchlist(ATWatchlist data_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		ATWatchlist outModel = usrSvc.saveWatchlist(data_);
		return outModel;
	}

	public List<ATWatchlist> getWatchlists(String uid_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		List<ATWatchlist> data = usrSvc.getWatchlists(uid_);
		return data;
	}

	public ATUserSetting getUserSetting(String userId_, String settingId_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		ATUserSetting setting = usrSvc.getUserSetting(userId_, settingId_);
		return setting;
	}

	public boolean saveUserSetting(ATUserSetting setting_) {
		IATUserProvider usrSvc = ATSourceServiceRegistry.getUserService();
		boolean result = usrSvc.saveUserSetting(setting_);
		return result;
	}

	public ATSecurityPack getDashboard(String uid_) {
		Configuration config = ATConfigUtil.config();
		String indiceStr = config.getString("dash.indices");
		String[] indices = StringUtils.isBlank(indiceStr) ? new String[0] : indiceStr.split("\\s*,\\s*");
		String sectorStr = config.getString("dash.sectors");
		String[] sectors = StringUtils.isBlank(sectorStr) ? new String[0] : sectorStr.split("\\s*,\\s*");
		HashMap<String, String[]> metaMap = new HashMap<>();
		metaMap.put("dash.indices", indices);
		metaMap.put("dash.sectors", sectors);
		ATSecurityPack pack = new ATSecurityPack(null);
		pack.setMeta(metaMap);
		for (String sym : indices) {
			log.debug(String.format("Merging [%s]", sym));
			ATSecurityPack newPack = getSecurity(uid_, sym, null, (String) null);
			pack.merge(newPack);
		}
		for (String sym : sectors) {
			log.debug(String.format("Merging [%s]", sym));
			ATSecurityPack newPack = getSecurity(uid_, sym, null, (String) null);
			pack.merge(newPack);
		}
		ATSecurityPack moverPack = getMovers();
		if (moverPack != null && moverPack.getMarketMap() != null) {
			pack.merge(moverPack);
			metaMap.put("dash.movers", moverPack.getMarketMap().keySet().toArray(new String[0]));
		}
		ATSecurityPack trendPack = getTrends();
		if (trendPack != null && trendPack.getMarketMap() != null) {
			pack.merge(trendPack);
			metaMap.put("dash.trends", trendPack.getMarketMap().keySet().toArray(new String[0]));
		}
		return pack;
	}

	public void logUserActivity(String ip_, String userId_, String activity_, String data_) {
		ATSourceServiceRegistry.getUserService().logUserActivity(ip_, userId_, activity_, data_);
	}

	public Map<String, Object> exportUserData(String userId_) {
		Map<String, Object> map = new TreeMap<>();

		IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
		ATUser user = userSvc.getUser(userId_);
		if (user != null) {
			map.put("user", user);
		}

		List<ATBroker> brokers = userSvc.getUserBrokers(userId_);
		if (brokers != null && !brokers.isEmpty()) {
			ArrayList<ATBroker> brokerList = new ArrayList<>();
			for (ATBroker broker : brokers) {
				brokerList.add(broker);
			}
			map.put("brokers", brokerList.toArray());
		}

		List<ATUserNote> notes = userSvc.getUserNotes(userId_, null);
		if (notes != null && !notes.isEmpty()) {
			ArrayList<ATUserNote> noteList = new ArrayList<>();
			for (ATUserNote note : notes) {
				noteList.add(note);
			}
			map.put("notes", noteList.toArray());
		}

		List<ATUserAlertRulePack> alertPacks = userSvc.getUserAlerts(userId_, null);
		if (alertPacks != null && alertPacks.size() > 0) {
			map.put("alerts", alertPacks);
		}

		List<ATWatchlist> watchLists = userSvc.getWatchlists(userId_);
		if (watchLists != null) {
			map.put("watchlists", watchLists);
		}

		List<ATUserEquityModel> models = userSvc.getUserEquityModels(userId_);
		if (models != null && !models.isEmpty()) {
			ArrayList<ATUserEquityModel> modelList = new ArrayList<>();
			for (ATUserEquityModel model : models) {
				modelList.add(model);
			}
			map.put("modelsEqt", modelList.toArray());
		}

		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
		List<IATTrade> dbTrades = ATSourceServiceRegistry.getPortfolioService().getTrades(query);
		if (dbTrades != null && !dbTrades.isEmpty()) {
			ArrayList<ATTrade> tradeList = new ArrayList<>();
			for (IATTrade itrade : dbTrades) {
				ATTrade trade = itrade instanceof ATTrade ? (ATTrade) itrade : new ATTrade(itrade);
				tradeList.add(trade);
			}
			map.put("trades", tradeList.toArray());
		}
		map.put("timestamp", new Date());
		return map;
	}

	public Map<String, Object> importUserData(JsonObject json_, String userId_) {
		Map<String, Object> result = new HashMap<>();
		try {
			IATUserProvider userSvc = ATSourceServiceRegistry.getUserService();
			ATUser user = userSvc.getUser(userId_);
			JsonObject jdata = json_.getAsJsonObject("data");
			if (user == null || jdata == null) {
				String msg = String.format("importUserData error [%s]: Missing user or data", userId_);
				log.error(msg);
				throw new ATException(msg);
			}
			log.info(String.format("Importing user data [%s]", user.getEmail()));

			ATBroker[] brokers = null;
			JsonElement jbrokers = jdata.get("brokers");
			if (jbrokers != null && jbrokers.isJsonArray()) {
				brokers = _gson.fromJson(jbrokers, ATBroker[].class);
			}
			ATUserNote[] notes = null;
			JsonElement jnotes = jdata.get("notes");
			if (jnotes != null && jnotes.isJsonArray()) {
				notes = _gson.fromJson(jnotes, ATUserNote[].class);
			}
			ATUserAlertRulePack[] alertPacks = null;
			JsonElement jalerts = jdata.get("alerts");
			if (jalerts != null && jalerts.isJsonArray()) {
				alertPacks = _gson.fromJson(jalerts, ATUserAlertRulePack[].class);
			}
			ATUserEquityModel[] models = null;
			JsonElement jmodels = jdata.get("modelsEqt");
			if (jmodels != null && jmodels.isJsonArray()) {
				models = _gson.fromJson(jmodels, ATUserEquityModel[].class);
			}
			Map<String, Object> userResultMap = ATSourceServiceRegistry.getUserService().importUserData(userId_,
					brokers, notes, alertPacks, models,
					true);
			result.putAll(userResultMap);

			JsonElement jtrades = jdata.get("trades");
			if (jtrades != null && jtrades.isJsonArray()) {
				ATTrade[] trades = _gson.fromJson(jtrades.getAsJsonArray(), ATTrade[].class);
				Map<String, Object> tradeResultMap = ATSourceServiceRegistry.getPortfolioService().importTrades(userId_,
						trades, true);
				result.putAll(tradeResultMap);
			}
		} catch (Exception ex_) {
			log.error(String.format("importUserData error", ex_));
			throw new ATException("importUserData error", ex_);
		}
		return result;
	}

	public void updateDynamicSymbols(String userId_, String type_, Set<String> dynamicSymbols_) {
		String[] symbols = dynamicSymbols_ != null ? dynamicSymbols_.toArray(new String[dynamicSymbols_.size()])
				: new String[0];
		_userSessionCtrl.processDynamicSymbols(userId_, type_, true, symbols);
	}

	public ATSecurityPack getOpportunities(List<String> symbols_, ATUserOpportunityConfig config_) {
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		if (symbols_ == null || symbols_.isEmpty()) {
			log.debug(String.format(" Symbols empty. Using Movers."));
			mktSvc = ATSourceServiceRegistry.getMarketDataService();
			List<ATMarketData> moverList = mktSvc.getMovers();
			if (moverList != null) {
				symbols_ = moverList.stream().map(m -> m.getOccId()).collect(Collectors.toList());
			}
		}
		IATOpportunityProcessor oppSvc = ATSourceServiceRegistry.getOpportunityProcessor();

		String nowDS6 = ATFormats.DATE_yyMMdd.get().format(new Date());

		ATSecurityPack resultPack = new ATSecurityPack(null);
		for (String symbol : symbols_) {
			ATMarketData mktUl = mktSvc.getMarketData(symbol);
			if (mktUl == null)
				continue;
			log.debug(String.format("getOpportunities [%s]", symbol));
			ATSecurityPack oPack = mktSvc.getOptionChain(symbol, null);
			if (oPack == null || oPack.getMarketMap() == null || oPack.getMarketMap().isEmpty()) {
				IATWhereQuery query = ATPersistenceRegistry.query()
						.startsWith(IATAssetConstants.OCCID_UNDERLYING, symbol).and()
						.gte(IATAssetConstants.EXPIRATION_DATE_STR, nowDS6);
				Map<String, ATMarketData> options = mktSvc.getMarketData(query);
				if (options != null && !options.isEmpty()) {
					TreeMap<String, TreeSet<String>> callChain = new TreeMap<>();
					TreeMap<String, TreeSet<String>> putChain = new TreeMap<>();
					TreeMap<String, ATMarketData> mktMap = new TreeMap<>();
					// oPack.put(IATAssetConstants.CALL, callMap);
					// oPack..put(IATAssetConstants.PUT, putMap);
					SimpleDateFormat fmtDS10 = ATFormats.DATE_yyyy_MM_dd.get();
					for (Entry<String, ATMarketData> entry : options.entrySet()) {
						String occId = entry.getKey();
						if (occId.length() < 10)
							continue;
						ATMarketData mkt = entry.getValue();
						Date dExp = mkt.getDateExp();// ATModelUtil.getExpirationDS6(occId);
						String expDS10 = fmtDS10.format(dExp);
						OptionRight right = ATModelUtil.getOptionRight(occId);
						Map<String, TreeSet<String>> rightMap = OptionRight.CALL == right ? callChain : putChain;

						TreeSet<String> chainSegment = rightMap.get(expDS10);
						if (chainSegment == null) {
							chainSegment = new TreeSet<>();
							rightMap.put(expDS10, chainSegment);
						}
						chainSegment.add(occId);
						mktMap.put(occId, mkt);
					}
					if (!mktMap.isEmpty()) {
						oPack = new ATSecurityPack(null, null, mktMap);
						if (!callChain.isEmpty()) {
							oPack.setCallChain(new TreeMap<>(Collections.singletonMap(symbol, callChain)));
						}
						if (!putChain.isEmpty()) {
							oPack.setPutChain(new TreeMap<>(Collections.singletonMap(symbol, putChain)));
						}
					}
				}
			}

			List<String> flags = config_.getFlags();
			if (flags != null && flags.contains(ATOpportunity2.CC)) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> callChain = oPack.getCallChain();
				if (callChain != null && callChain.containsKey(symbol)) {
					ATSecurityPack ccPack = oppSvc.analyzeCC(mktUl, oPack.getMarketMap(), callChain.get(symbol),
							config_);
					if (ccPack != null) {
						log.debug(String.format("CC [%d]", ccPack.getOpportunities().size()));
						resultPack.merge(ccPack);
					}
				}
			}
			if (flags != null && flags.contains(ATOpportunity2.VCS)) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> callChain = oPack.getCallChain();
				if (callChain != null && !callChain.isEmpty()) {
					ATSecurityPack vcsPack = oppSvc.analyzeVCS(mktUl, oPack.getMarketMap(), callChain.get(symbol),
							config_);
					if (vcsPack != null) {
						log.debug(String.format("VCS [%d]", vcsPack.getOpportunities().size()));
						resultPack.merge(vcsPack);
					}
				}
			}
			if (flags != null && flags.contains(ATOpportunity2.VPS)) {
				TreeMap<String, TreeMap<String, TreeSet<String>>> putChain = oPack.getPutChain();
				if (putChain != null && !putChain.isEmpty()) {
					ATSecurityPack vpsPack = oppSvc.analyzeVPS(mktUl, oPack.getMarketMap(), putChain.get(symbol),
							config_);
					if (vpsPack != null) {
						log.debug(String.format("VPS [%d]", vpsPack.getOpportunities().size()));
						resultPack.merge(vpsPack);
					}
				}
			}

			Map<String, Map<String, List<ATOpportunity2>>> opportunitiesMap = resultPack.getOpportunities();
			if (opportunitiesMap != null) {
				Map<String, List<ATOpportunity2>> symbolOpportunitiesMap = opportunitiesMap.get(symbol);
				if (symbolOpportunitiesMap != null) {
					List<ATOpportunity2> opportunities = symbolOpportunitiesMap.values().stream()
							.flatMap(Collection::stream)
							.collect(Collectors.toList());
					if (opportunities != null) {
						opportunities.sort(new Comparator<ATOpportunity2>() {
							@Override
							public int compare(ATOpportunity2 a_, ATOpportunity2 b_) {
								return Double.compare(b_.getYieldAbs(), a_.getYieldAbs());
							}
						});
						Integer oppCount = config_.getOpportunityCount();
						if (oppCount == null)
							oppCount = 5;
						if (opportunities.size() > oppCount) {
							opportunities = opportunities.subList(0, oppCount);
						}
						Set<Integer> hashSet = opportunities.stream().map(o -> o.hashCode())
								.collect(Collectors.toSet());
						for (List<ATOpportunity2> strategyOpportunities : symbolOpportunitiesMap.values()) {
							strategyOpportunities.removeIf(o -> !hashSet.contains(o.hashCode()));
						}
					}
				}
			}
		}

		return resultPack;
	}

	public ATSecurityPack getResearch(ATUserEquityModel researchModel_) {
		ATResearchProcessor researchPrc = ATResearchProcessor.getInstance();
		ATSecurityPack result = researchPrc.getResearch(researchModel_);
		return result;
	}

	public ATSecurityPack getTradeLedger(String userId_, String symbol_) {
		IATPortfolioService prtSvc = ATServiceRegistry.getPortfolioService();
		List<ATTrade> trades = prtSvc.getTradeLedger(userId_, symbol_);
		if (trades == null || trades.isEmpty())
			return null;
		Map<String, List<ATTrade>> ledgerMap = trades.stream()
				.collect(Collectors.groupingBy(t_ -> IATTrade.getUnderlyingSymbol(t_)));
		Set<String> occIds = ledgerMap.values().stream().flatMap(List::stream).map(ATTrade::getOccId)
				.collect(Collectors.toSet());
		ATSecurityPack pack = prepareSecurityPack(userId_, null, occIds);
		pack.setTradeLedger(ledgerMap);
		return pack;
	}

	public ATSecurityPack getSecuritPack(String userId_, List<String> commands_, String symbol_) {
		if (commands_ == null || commands_.isEmpty())
			return null;
		ATSecurityPack pack = null;
		for (String cmd : commands_) {
			switch (cmd) {
				case "positions": {
					IATPortfolioService pfSvc = ATSourceServiceRegistry.getPortfolioService();
					var positions = pfSvc.getPositions(userId_, symbol_);
					var occIds = positions.stream().map(p_ -> p_.getOccId()).collect(Collectors.toSet());
					pack = prepareSecurityPack(userId_, pack, occIds);
					pack.setPositions(positions);
					break;
				}
				case "trade-digest": {
					IATPortfolioService pfSvc = ATSourceServiceRegistry.getPortfolioService();
					List<ATTradeDigest> digests = pfSvc.getTradeDigest(userId_, symbol_);
					List<ATTradeGroup> groups = pfSvc.getTradeGroups(userId_, symbol_);
					Set<String> occIds = new HashSet<>();
					if (symbol_ != null) {
						occIds.add(symbol_);
					}
					Map<String, List<ATTradeDigest>> digestMap = null;
					Map<String, List<ATTradeGroup>> groupMap = null;
					if (digests != null && !digests.isEmpty() && groups != null && !groups.isEmpty()) {
						digestMap = digests.stream().collect(Collectors.groupingBy(ATTradeDigest::getUnderlying));
						groupMap = groups.stream().collect(Collectors.groupingBy(ATTradeGroup::getUnderlying));
						Set<String> occIdsNew = digestMap.values().stream().flatMap(List::stream)
								.map(ATTradeDigest::getOccId)
								.collect(Collectors.toCollection(HashSet::new));
						occIds.addAll(occIdsNew);
						occIds.addAll(digestMap.keySet());
					}
					pack = prepareSecurityPack(userId_, pack, occIds);
					if (digestMap != null) {
						pack.setDigest(digestMap);
					}
					if (groupMap != null) {
						pack.setStrategy(groupMap);
					}
					break;
				}
				case "trade-ledger": {
					IATPortfolioService prtSvc = ATServiceRegistry.getPortfolioService();
					List<ATTrade> trades = prtSvc.getTradeLedger(userId_, symbol_);
					if (trades == null || trades.isEmpty())
						continue;
					Map<String, List<ATTrade>> ledgerMap = trades.stream()
							.collect(Collectors.groupingBy(t_ -> IATTrade.getUnderlyingSymbol(t_)));
					Set<String> occIds = ledgerMap.values().stream().flatMap(List::stream).map(ATTrade::getOccId)
							.collect(Collectors.toCollection(HashSet::new));
					occIds.addAll(ledgerMap.keySet());
					pack = prepareSecurityPack(userId_, pack, occIds);
					pack.setTradeLedger(ledgerMap);
					break;
				}
				case "trade_trace": {
					IATPortfolioService prtSvc = ATServiceRegistry.getPortfolioService();
					var occIds = new TreeSet<String>();
					if (symbol_ != null) {
						occIds.add(symbol_);
					}
					Map<String, List<ATTradeTraceGroup>> traceMap = null;
					List<ATTradeTraceGroup> traces = prtSvc.getTradeTrace(userId_, symbol_);
					if (traces != null && !traces.isEmpty()) {
						traceMap = traces.stream()
								.collect(Collectors.groupingBy(t_ -> t_.getSymbols().get(0)));

						var symbols = traces.stream().map(t_ -> t_.getSymbols()).flatMap(List::stream)
								.collect(Collectors.toSet());
						occIds.addAll(symbols);
					}
					Map<String, List<ATTrade>> ledgerMap = null;
					List<String> symbols = occIds.stream().filter(s_ -> s_.length() < 10)
							.collect(Collectors.toCollection(ArrayList::new));
					List<ATTrade> trades = !symbols.isEmpty()
							? prtSvc.getTradeLedger(userId_, symbols.toArray(new String[0]))
							: null;
					if (trades != null && !trades.isEmpty()) {
						ledgerMap = trades.stream()
								.collect(Collectors.groupingBy(t_ -> IATTrade.getUnderlyingSymbol(t_)));
						occIds.addAll(ledgerMap.keySet());

						Set<String> tradeOccIds = ledgerMap.values().stream().flatMap(List::stream)
								.map(ATTrade::getOccId)
								.collect(Collectors.toCollection(HashSet::new));
						occIds.addAll(tradeOccIds);
					}
					if ((traceMap == null || traceMap.isEmpty()) && (ledgerMap == null || ledgerMap.isEmpty()))
						continue;
					pack = prepareSecurityPack(userId_, pack, occIds);
					if (traceMap != null) {
						pack.setTradeTrace(traceMap);
					}
					if (ledgerMap != null) {
						pack.setTradeLedger(ledgerMap);
					}
					break;
				}
				default:
					log.error(String.format("getSecuritPack Unsupported command [%s]", cmd));
					//	throw new ATException(String.format("Unsupported security pack type [%s]", cmd));
			}
		}
		return pack;
	}

	private ATSecurityPack prepareSecurityPack(String userId_, ATSecurityPack pack_, Collection<String> occIds_) {
		if (occIds_ == null || occIds_.isEmpty())
			return null;
		IATMarketDataService mktSvc = ATSourceServiceRegistry.getMarketDataService();
		IATFundamentalsProvider fndSvc = ATServiceRegistry.getFundamentalsProvider();
		IATEquityFoundationProvider fndSvcOld = ATSourceServiceRegistry.getEquityFoundationProvider();
		IATPortfolioService pfSvc = ATSourceServiceRegistry.getPortfolioService();

		Map<String, Double> cumMap = pack_ != null ? pack_.getCumulative() : new HashMap<>();
		Map<String, ATFundamental> fndMap = pack_ != null ? pack_.getFundamentals() : new HashMap<>();
		Map<String, ATEquityDescription> dscMap = pack_ != null ? pack_.getDescriptions() : new HashMap<>();
		Map<String, ATMarketData> mktMap = pack_ != null ? pack_.getMarketMap() : new HashMap<>();

		for (String occId : occIds_) {
			boolean isSymbol = occId.length() < 10;
			if (isSymbol) {
				if (!cumMap.containsKey(occId)) {
					ATTrade cumTrade = pfSvc.getTradeCumulative(userId_, occId);
					Double pxCum = cumTrade != null ? cumTrade.getPxNet() : null;
					if (pxCum != null) {
						cumMap.put(occId, pxCum);
					}
				}
				if (!fndMap.containsKey(occId)) {
					Map<String, ATFundamental> fnd = fndSvc.getFundamentals(occId);
					if (fnd != null) {
						fndMap.putAll(fnd);
					}
					ATPersistentEquityFoundation fndOld = fndSvcOld.getFoundation(occId);
					if (fndOld != null) {
						ATEquityDescription description = new ATEquityDescription(fndOld);
						dscMap.put(occId, description);
					}
				}
			}
			if (!mktMap.containsKey(occId)) {
				ATMarketData mkt = mktSvc.getMarketData(occId);
				if (mkt != null) {
					mktMap.put(occId, mkt);
				}
			}
		}
		ATSecurityPack result = pack_ != null ? pack_ : new ATSecurityPack(dscMap, fndMap, mktMap);
		result.setCumulative(cumMap);
		return result;
	}

	public boolean archive(String userId_, String type_, Object id_, HashMap<String, String> params_) {
		switch (type_) {
			case "trade_trace": {
				IATPortfolioService prtSvc = ATServiceRegistry.getPortfolioService();
				var result = prtSvc.archiveTradeTrace(userId_, id_);
				return result;
			}
			default:
				log.error(String.format("archive: Unsupported type [%s]", type_));
				return false;
		}
	}

}
