package com.isoplane.at.source.util;

import java.util.Date;

import com.isoplane.at.source.model.ATChangeNotification;

public class ATTestUtil {

	static private int _changeNotificationState = 0;

	static public ATChangeNotification getChangeNotification() {
		ATChangeNotification n = new ATChangeNotification();
		n.setActive(true);
		n.setClickAction("http://www.google.com");
		n.setDate(new Date());
		n.setLabel("Test Notification");
		n.setPeriod("day");
		switch (_changeNotificationState) {
		case 0:
			n.setChangePct(50.0);
			n.setPrice(12.345);
			n.setSymbol("TEST");
			_changeNotificationState++;
			break;
		case 1:
			n.setChangePct(25.0);
			n.setPrice(1.2345);
			n.setSymbol("TEST");
			_changeNotificationState++;
			break;
		case 2:
			n.setChangePct(75.0);
			n.setPrice(123.45);
			n.setSymbol("TEST");
			_changeNotificationState = 0;
			break;
		}
		return n;
	}
}
