package com.isoplane.at.source.model;

public class ATYield implements Comparable<ATYield> {

	private Double apy;
	private Double abs;

	public Double getApy() {
		return apy;
	}

	public void setApy(Double apy) {
		this.apy = apy;
	}

	public Double getAbs() {
		return abs;
	}

	public void setAbs(Double abs) {
		this.abs = abs;
	}

	@Override
	public int compareTo(ATYield other_) {
		if (other_ == null)
			return -1;
		if (getApy() == null)
			return other_.getApy() == null ? 0 : 1;
		
		return Double.compare(getApy(), other_.getApy());
	}
}
