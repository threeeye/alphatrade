package com.isoplane.at.source.controller;

import java.util.Set;

import com.isoplane.at.commons.model.ATBaseNotification;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.service.IATService;
import com.isoplane.at.commons.store.ATPersistentSecurity;

public interface IATAlertProcessor extends IATService {

	// void update(String userId, Collection<TdnAlert> alerts);

	Set<ATBaseNotification> process(ATPersistentSecurity... secs);

	Set<ATBaseNotification> process(ATMarketData marketData, Set<String> ownerIds);

	Set<ATBaseNotification> processEconomicEvents(String occId, Set<String> ownerIds);

}
