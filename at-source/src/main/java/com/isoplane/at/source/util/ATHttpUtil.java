package com.isoplane.at.source.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class ATHttpUtil {

	static public String get(String url_, Map<String, String> headers_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url_);
		//	httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			if (headers_ != null) {
				for (Entry<String, String> h : headers_.entrySet()) {
					httpGet.setHeader(h.getKey(), h.getValue());
				}
			}
			response = httpclient.execute(httpGet);

			HttpEntity entity = response.getEntity();
			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			return content;
		} finally {
			if (response != null)
				response.close();
		}
	}

	static public String postForm(String url_, Map<String, String> headers_, Map<String, String> form_)
			throws Exception {

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		for (Entry<String, String> nameValuePair : form_.entrySet()) {
			params.add(new BasicNameValuePair(nameValuePair.getKey(), nameValuePair.getValue()));
		}
		HttpEntity data = new UrlEncodedFormEntity(params);

		String result = postForm(url_, headers_, data);
		return result;
	}

	static public String postJson(String url_, Map<String, String> headers_, String json_) throws Exception {
		HttpEntity data = new StringEntity(json_, "UTF-8");
		Map<String, String> headers = headers_ != null ? headers_ : new HashMap<>();
		headers.put("Content-Type", "application/json; charset=utf-8");

		String result = postForm(url_, headers_, data);
		return result;
	}

	static private String postForm(String url_, Map<String, String> headers_, HttpEntity data_) throws Exception {
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url_);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");
			for (Entry<String, String> h : headers_.entrySet()) {
				httpPost.setHeader(h.getKey(), h.getValue());
			}
			httpPost.setEntity(data_);
			response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			return content;
		} finally {
			if (response != null)
				response.close();
		}
	}

}
