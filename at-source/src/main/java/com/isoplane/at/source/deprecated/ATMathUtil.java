package com.isoplane.at.source.deprecated;

import java.util.Arrays;
import java.util.Date;
import java.util.TreeSet;

import com.isoplane.at.source.model.ATPriceHistoryOld;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Deprecated
public class ATMathUtil {

	static final Logger log = LoggerFactory.getLogger(ATMathUtil.class);

	public static Double exponentialMovingAverage(int length_, Double... d_) {
		if (d_ == null || d_.length == 0 || d_.length < length_)
			return null;
		// double sum = 0;
		// for (Double d : d_) {
		// if (d != null) {
		// sum += d;
		// }
		// }
		// double savg = sum / d_.length;
		double k = 2.0 / (length_ + 1.0);
		double eavg = d_[d_.length - length_]; // Seed
		for (int i = d_.length + 1 - length_; i < d_.length; i++) {
			eavg = (d_[i] * k) + (eavg * (1 - k));
		}
		return eavg;
	}

	public static ATPriceHistoryOld getPriceStats(TreeSet<ATPriceHistoryOld> px_) {
		if (px_ == null || px_.isEmpty())
			return null;
		ATPriceHistoryOld first = px_.first();
		try {
			Date quarter = DateUtils.addMonths(new Date(), -3);
			Date year = DateUtils.addYears(new Date(), -1);
			Date yearPlus = DateUtils.addWeeks(year, 1);// Tolerance 1 week...
			String symbol = first.getOccId();
			if (yearPlus.before(first.getDate())) {
				log.warn(String.format("Less than one year of data [%s]", first.getOccId()));
			}
			if (quarter.before(first.getDate())) {
				log.warn(String.format("Less than one quarter of data [%s]", first.getOccId()));
			}

			ATPriceHistoryOld result = new ATPriceHistoryOld();
			double yK = 2.0 / (252 + 1.0);
			double qK = 2.0 / (252 / 4.0 + 1.0);
			double yLow = Double.MAX_VALUE;
			double yHigh = Double.MIN_VALUE;
			double qLow = Double.MAX_VALUE;
			double qHigh = Double.MIN_VALUE;
			Double yAvg = null;
			Double qAvg = null;
			for (ATPriceHistoryOld atPriceHistory : px_) {
				if (!symbol.equals(atPriceHistory.getOccId())) {
					log.error(String.format("Symbol mismatch [%s - %s]", symbol, atPriceHistory.getOccId()));
					return null;
				}
				if (year.before(atPriceHistory.getDate())) {
					if (result.getOpen1Y() == null) {
						result.setOpen1Q(atPriceHistory.getOpen());
					}
					// log.info(String.format("### PXH: %s - %s", sec.getSymbol(), atPriceHistory.getDate()));
					double close = atPriceHistory.getClose();
					yAvg = yAvg == null ? close : (close * yK) + (yAvg * (1 - yK));
					yLow = yLow > close ? close : yLow;
					yHigh = yHigh < close ? close : yHigh;
					if (quarter.before(atPriceHistory.getDate())) {
						if (result.getOpen1Q() == null) {
							result.setOpen1Q(atPriceHistory.getOpen());
						}
						qAvg = qAvg == null ? close : (close * qK) + (qAvg * (1 - qK));
						qLow = qLow > close ? close : qLow;
						qHigh = qHigh < close ? close : qHigh;
					}
				}
			}
			// result_digest.setOpen1Q(first);
			ATPriceHistoryOld last = px_.last();
			result.setOpen(first.getOpen());
			result.setClose(last.getClose());
			result.setLow1Q(qLow);
			result.setHigh1Q(qHigh);
			result.setAvg1Q(qAvg);// (qSum / mCount);
			result.setClose1Q(last.getClose());
			result.setLow1Y(yLow);
			result.setHigh1Y(yHigh);
			result.setAvg1Y(yAvg);// (ySum / yCount);
			result.setClose1Y(last.getClose());
			// TODO: Warn when px seem out of date
			return result;
		} catch (Exception ex) {
			log.error(String.format("Error in price stats [%s]", first.getOccId()), ex);
			return null;
		}
	}


	public static void getVariance(TreeSet<ATDateDouble> data_, double timeDecay_) {
		if (data_ == null || data_.isEmpty())
			return;

		Double previousValue = null;
		int dayCount = 0;
		double logSum = 0.0;
		double weightedLogSum = 0.0;
		for (ATDateDouble data : data_) {
			if (previousValue != null && previousValue != 0.0) {
				double logVal = Math.log(data.getValue() / previousValue);
				logSum += logVal;
				weightedLogSum = timeDecay_ * weightedLogSum + logVal;
				dayCount++;
			}
			previousValue = data.getValue();
		}

		Double weightedLogAvg = weightedLogSum / (dayCount * timeDecay_);

		double logVolatility = logSum / (dayCount - 1); 
	}

	static public class Statistics {
		private double[] _data;
		private int _size;
		private Double _mean;
		private Double _variance;
		private Double _stdDev;
		private Double _median;

		public Statistics(double[] data_) {
			if (data_ == null || data_.length == 0)
				return;
			_data = data_;
			_size = data_.length;

			double sum = 0.0;
			for (double a : data_)
				sum += a;
			_mean = sum / _size;

			double temp = 0;
			for (double a : _data)
				temp += (a - _mean) * (a - _mean);
			_variance = temp / _size;

			_stdDev = Math.sqrt(_variance);

			Arrays.sort(_data);
			_median = _data.length % 2 == 0 ? (_data[(_data.length / 2) - 1] + _data[_data.length / 2]) / 2.0
					: _data[_data.length / 2];
		}

		public double getMean() {
			return _mean;
		}

		public double getVariance() {
			return _variance;
		}

		public double getStdDev() {
			return _stdDev;
		}

		public double median() {
			return _median;
		}
	}

	static public class ATDateDouble implements Comparable<ATDateDouble> {

		private Date date;
		private Double value;

		public ATDateDouble(Date date_, Double value_) {
			super();
			if (date_ == null)
				throw new NullPointerException("date");
			if (value_ == null)
				throw new NullPointerException("value");
			this.date = date_;
			this.value = value_;
		}

		public Date getDate() {
			return date;
		}

		public Double getValue() {
			return value;
		}

		@Override
		public int compareTo(ATDateDouble arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

	}

}
