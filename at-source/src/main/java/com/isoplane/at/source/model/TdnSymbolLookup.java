package com.isoplane.at.source.model;

public class TdnSymbolLookup implements Comparable<TdnSymbolLookup> {

	@Override
	public int compareTo(TdnSymbolLookup other_) {
		if (other_ == null || other_.symbol == null) {
			return this.symbol == null ? 0 : -1;
		}
		if (this.symbol == null)
			return 1;
		return this.symbol.compareTo(other_.symbol);
	}

	public String name;
	public String symbol;
	public String type;

}
