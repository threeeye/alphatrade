package com.isoplane.at.source.deprecated;

import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATLegacyDividend;
import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecEvent;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.commons.model.alertcriteria.ATBaseCriteria;
import com.isoplane.at.commons.store.ATPersistentEquityFoundation;
import com.isoplane.at.source.controller.ATSourceServiceRegistry;
import com.isoplane.at.source.model.ATConfiguration;
import com.isoplane.at.source.model.ATUser;

public class ATStore {

	static final Logger log = LoggerFactory.getLogger(ATStore.class);

	private ATConfiguration _config;
//	private IATSecuritiesDb _secDb;
//	private IATSecEventsDb _eventDb;
	private IATUserService _userSvc;
	private ConcurrentSkipListMap<String, ATStock> _stockMap;
	private ConcurrentSkipListMap<String, ATOptionSecurity> _optionMap;
	private ConcurrentSkipListMap<String, TreeSet<ATLegacyDividend>> _dividendMap;
	private ConcurrentSkipListMap<String, Set<ATBaseCriteria>> _alertCriteriaMap;
	private ConcurrentSkipListMap<String, Set<ATBaseCriteria>> _opportunityCriteriaMap;
	private Set<ATSecurity> _secStorageSet;
	private Set<ATSecEvent> _eventStorageSet;

	public ATStore(ATConfiguration config_,  IATUserService userSvc_) {
		_config = config_;
	//	_secDb = secDb_;
	//	_eventDb = sevDb_;
		_userSvc = userSvc_;
		_optionMap = new ConcurrentSkipListMap<>();
		_stockMap = new ConcurrentSkipListMap<>();
		_dividendMap = new ConcurrentSkipListMap<>();
		_alertCriteriaMap = new ConcurrentSkipListMap<>();
		_opportunityCriteriaMap = new ConcurrentSkipListMap<>();
		_secStorageSet = new HashSet<>();
		_eventStorageSet = new HashSet<>();
	}

	public void init() {
		log.info(String.format("Initializing..."));
		if (true)
			return;

		Map<String, ATPersistentEquityFoundation> foundations = ATSourceServiceRegistry.getEquityFoundationProvider().getFoundationMap();

		List<ATSecurity> securities = null;// _secDb.load(null, true);
		int optCount = 0;
		int stockCount = 0;
		if (securities != null) {
			for (ATSecurity sec : securities) {
				if (sec instanceof ATStock) {
					if (!foundations.keySet().contains(sec.getOccId()))
						continue;
					_stockMap.put(sec.getOccId(), (ATStock) sec);
					stockCount++;
				} else if (sec instanceof ATOptionSecurity) {
					if (!foundations.keySet().contains(((ATOptionSecurity) sec).getStockOccId()))
						continue;
					_optionMap.put(sec.getOccId(), (ATOptionSecurity) sec);
					optCount++;
				}
			}
		}
		log.info(String.format("Loaded [%,7d] stocks", stockCount));
		log.info(String.format("Loaded [%,7d] options", optCount));
		refreshEvents();
		// List<ATSecEvent> events = _eventDb.load(null, true);
		// int divCount = 0;
		// int ernCount = 0;
		// if (events != null) {
		// for (ATSecEvent event : events) {
		// String occId = event.getOccId();
		// if (event instanceof ATLegacyDividend) {
		// TreeSet<ATLegacyDividend> dividends = _dividendMap.get(occId);
		// if (dividends == null) {
		// dividends = new TreeSet<>();
		// _dividendMap.put(occId, dividends);
		// }
		// dividends.add((ATLegacyDividend) event);
		// divCount++;
		// } else if (event instanceof ATEarning) {
		// TreeSet<ATEarning> earnings = _earningsMap.get(occId);
		// if (earnings == null) {
		// earnings = new TreeSet<>();
		// _earningsMap.put(occId, earnings);
		// }
		// earnings.add((ATEarning) event);
		// ernCount++;
		// }
		// }
		// }
		// log.info(String.format("Loaded [%,7d] dividends", divCount));
		// log.info(String.format("Loaded [%,7d] earnings", ernCount));
	}

	public void flush() {
		log.info("Flushing...");
		Set<ATSecurity> secStore = _secStorageSet;
		_secStorageSet = new HashSet<>();
	//	_secDb.store(secStore);

		Set<ATSecEvent> eventStore = _eventStorageSet;
		_eventStorageSet = new HashSet<>();
//		_eventDb.store(eventStore);
	}

	public void validate() {
	//	_eventDb.validate();
	}

	public Set<String> getStockIds() {
		Set<String> result = new HashSet<>(_stockMap.keySet());
		return result;
	}

	public void exportSecurities() {
		try {
			SimpleDateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
			String fileName = String.format("AT_export_%s.csv", dateFmt.format(new Date()));
			Path path = Paths.get(_config.getExportPath(), fileName);
			if (!Files.exists(path)) {
				Files.createFile(path);
			}
			BufferedWriter writer = Files.newBufferedWriter(path);
			for (String stockId : _stockMap.keySet()) {
				ATStock stock = _stockMap.get(stockId);
				String sPxO = stock.getPriceOpen() != null ? String.format("%.2f", stock.getPriceOpen()) : "";
				String sPxH = stock.getPriceOpen() != null ? String.format("%.2f", stock.getPriceHigh()) : "";
				String sPxL = stock.getPriceOpen() != null ? String.format("%.2f", stock.getPriceLow()) : "";
				String sPxC = stock.getPriceOpen() != null ? String.format("%.2f", stock.getPriceClose()) : "";
				String sRow = String.format("%s,%s,%s,%s,%s\n", stock.getOccId(), sPxO, sPxH, sPxL, sPxC);
				writer.write(sRow);
				Collection<ATOptionSecurity> options = getOptionChain(stockId);
				for (ATOptionSecurity option : options) {
					String oPxO = option.getPriceOpen() != null ? String.format("%.2f", option.getPriceOpen()) : "";
					String oPxH = option.getPriceOpen() != null ? String.format("%.2f", option.getPriceHigh()) : "";
					String oPxL = option.getPriceOpen() != null ? String.format("%.2f", option.getPriceLow()) : "";
					String oPxC = option.getPriceOpen() != null ? String.format("%.2f", option.getPriceClose()) : "";
					String oRow = String.format("%s,%s,%s,%s,%s\n", option.getLabel(), oPxO, oPxH, oPxL, oPxC);
					writer.write(oRow);
				}
			}
			writer.close();
			log.info("Export done");
		} catch (Exception ex) {
			log.error("Error exporting securities", ex);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends ATSecurity> T get(String key_) {
		if (StringUtils.isBlank(key_))
			return null;
		T sec;
		if (key_.length() <= 13) {
			sec = (T) _stockMap.get(key_);
		} else {
			sec = (T) _optionMap.get(key_);
		}
		return sec;
	}

	@SuppressWarnings("unchecked")
	public <T extends ATSecEvent> TreeSet<T> getEvent(String key_, Class<T> clazz_) {
		if (StringUtils.isBlank(key_))
			return null;
		TreeSet<T> sec;
		if (ATLegacyDividend.class.equals(clazz_)) {
			sec = (TreeSet<T>) _dividendMap.get(key_);
			// } else if (ATEarningOld.class.equals(clazz_)) {
			// sec = (TreeSet<T>) _earningsMap.get(key_);
		} else {
			return null;
		}
		return sec;
	}

	public Collection<ATOptionSecurity> getOptionChain(String key_) {
		if (key_ == null)
			return null;
		String fromKey = key_ + " ";// + "000000";
		String toKey = key_ + "ZZZZ";// "999999";
		ConcurrentNavigableMap<String, ATOptionSecurity> subMap = _optionMap.subMap(fromKey, toKey);
		return subMap == null ? new TreeSet<>() : subMap.values();
	}

	public void put(ATSecurity sec_) {
		if (sec_ == null || sec_.getOccId() == null || !sec_.isChanged())
			return;
		sec_.setChanged(false);
		if (sec_ instanceof ATStock) {
			_stockMap.put(sec_.getOccId(), (ATStock) sec_);
		} else if (sec_ instanceof ATOptionSecurity) {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			ATOptionSecurity option = (ATOptionSecurity) sec_;
			if (today.compareTo(option.getExpirationDate()) <= 0) {
				_optionMap.put(option.getOccId(), option);
			}
		} else {
			return;
		}
		queueForPersistence(sec_);
	}

	public boolean remove(ATSecurity sec_) {
		if (sec_ == null || sec_.getOccId() == null)
			return false;
		if (sec_ instanceof ATStock) {
			return _stockMap.remove(sec_.getOccId()) != null;
		} else if (sec_ instanceof ATOptionSecurity) {
			// Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			ATOptionSecurity option = (ATOptionSecurity) sec_;
			// if (today.compareTo(option.getExpirationDate()) <= 0) {
			return _optionMap.remove(option.getOccId()) != null;
			// }
		} else {
			return false;
		}
		// TODO: Persist
		// queueForPersistence(sec_);
	}

	public void put(ATSecEvent event_) {
		if (event_ == null || event_.getOccId() == null || !event_.isChanged())
			return;
		event_.setChanged(false);
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		String occId = event_.getOccId();
		if (event_ instanceof ATLegacyDividend) {
			ATLegacyDividend div = (ATLegacyDividend) event_;
			if (today.compareTo(div.getExDate()) <= 0) {
				TreeSet<ATLegacyDividend> dividends = _dividendMap.get(occId);
				if (dividends == null) {
					dividends = new TreeSet<>();
					_dividendMap.put(occId, dividends);
				}
				dividends.add(div);
			}
			// } else if (event_ instanceof ATEarningOld) {
			// ATEarningOld ern = (ATEarningOld) event_;
			// if (today.compareTo(ern.getDate()) <= 0) {
			// TreeSet<ATEarningOld> earnings = _earningsMap.get(occId);
			// if (earnings == null) {
			// earnings = new TreeSet<>();
			// _earningsMap.put(occId, earnings);
			// }
			// earnings.add(ern);
			// }
		} else {
			return;
		}
		queueForPersistence(event_);
	}

	public int deleteFuture(Class<? extends ATSecEvent> eventType_, Collection<String> refIds_) {
		return 0;// _eventDb.deleteFuture(eventType_, refIds_);
	}

	public void deactivate() {
		for (ATSecurity atSecurity : _stockMap.values()) {
			atSecurity.getUserIds().clear();
		}
		for (ATSecurity atSecurity : _optionMap.values()) {
			atSecurity.getUserIds().clear();
		}
	}

	public void updateAlertCriteria(Set<ATBaseCriteria> crits_) {
		Set<String> crits = updateCriteria(crits_, _alertCriteriaMap);
		if (crits != null) {
			log.debug(String.format("Alerts %s", String.join(",", crits)));
		}
	}

	public void updateOpportunitiesCriteria(Set<ATBaseCriteria> crits_) {
		Set<String> crits = updateCriteria(crits_, _opportunityCriteriaMap);
		if (crits != null) {
			log.debug(String.format("Ideas   %s", String.join(",", crits)));
		}
	}

	public Set<ATBaseCriteria> getAlertCriteria(String occId_) {
		if (StringUtils.isBlank(occId_))
			return null;
		return _alertCriteriaMap.get(occId_);
	}

	public Set<ATBaseCriteria> getOpportunityCriteria(String occId_) {
		if (StringUtils.isBlank(occId_))
			return null;
		return _opportunityCriteriaMap.get(occId_);
	}

	synchronized private Set<String> updateCriteria(Set<ATBaseCriteria> crits_,
			ConcurrentSkipListMap<String, Set<ATBaseCriteria>> map_) {
		if (crits_ == null || crits_.isEmpty()) {
			Set<String> result = map_.isEmpty() ? null : new HashSet<>();
			map_.clear();
			return result;
		}

		int oldHash = map_.values().stream().map(c -> c.toString().hashCode()).mapToInt(Integer::intValue).sum();
		map_.clear();
		Set<String> result = new TreeSet<>();
		for (ATBaseCriteria crit : crits_) {

			if (crit == null)
				continue;
			Set<ATBaseCriteria> crits = map_.get(crit.getOccId());
			if (crits == null) {
				crits = new HashSet<>();
				map_.put(crit.getOccId(), crits);
			}
			if (crits.add(crit)) {
				result.add(crit.toString());
			}
		}
		int newHash = map_.values().stream().map(c -> c.toString().hashCode()).mapToInt(Integer::intValue).sum();
		return oldHash != newHash ? result : null;
	}

	private void queueForPersistence(ATSecurity sec_) {
		try {
			_secStorageSet.add(sec_);
			if (_secStorageSet.size() > _config.getSaveBulkSize()) {
				Set<ATSecurity> toStore = _secStorageSet;
				_secStorageSet = new HashSet<>();
				log.debug(String.format("Storing   [%s(%d)]", sec_.getClass().getSimpleName(), toStore.size()));
				(new Thread() {
					public void run() {
			//			_secDb.store(toStore);
						toStore.clear();
					}
				}).start();
			}
		} catch (Exception ex) {
			log.error(String.format("Unable to queue [%s]", sec_.getOccId()), ex);
		}
	}

	private void queueForPersistence(ATSecEvent event_) {
		try {
			_eventStorageSet.add(event_);
			if (_eventStorageSet.size() > _config.getSaveBulkSize()) {
				Set<ATSecEvent> toStore = _eventStorageSet;
				_eventStorageSet = new HashSet<>();
				log.debug(String.format("Storing   [%s(%d)]", event_.getClass().getSimpleName(), toStore.size()));
				(new Thread() {
					public void run() {
				//		_eventDb.store(toStore);
						toStore.clear();
					}
				}).start();
			}
		} catch (Exception ex) {
			log.error(String.format("Unable to queue [%s %s]", event_.getOccId()), ex);
		}
	}

	public void refreshEvents() {
		flush();
		ConcurrentSkipListMap<String, TreeSet<ATLegacyDividend>> dividendMap = new ConcurrentSkipListMap<>();
		// ConcurrentSkipListMap<String, TreeSet<ATEarningOld>> earningsMap = new ConcurrentSkipListMap<>();
		List<ATSecEvent> events = null;// _eventDb.load(null, true);
		int divCount = 0;
		int ernCount = 0;
		if (events != null) {
			for (ATSecEvent event : events) {
				String occId = event.getOccId();
				if (event instanceof ATLegacyDividend) {
					TreeSet<ATLegacyDividend> dividends = dividendMap.get(occId);
					if (dividends == null) {
						dividends = new TreeSet<>();
						dividendMap.put(occId, dividends);
					}
					dividends.add((ATLegacyDividend) event);
					divCount++;
					// } else if (event instanceof ATEarningOld) {
					// TreeSet<ATEarningOld> earnings = earningsMap.get(occId);
					// if (earnings == null) {
					// earnings = new TreeSet<>();
					// earningsMap.put(occId, earnings);
					// }
					// earnings.add((ATEarningOld) event);
					// ernCount++;
				}
			}
			_dividendMap = dividendMap;
			// _earningsMap = earningsMap;
		}
		log.info(String.format("Loaded [%,7d] dividends", divCount));
		// log.info(String.format("Loaded [%,7d] earnings", ernCount));
	}

}
