package com.isoplane.at.source.model.enhanced;

public interface IATChangeListener {

	void changed();
}
