package com.isoplane.at.instrumentation;

import java.lang.instrument.Instrumentation;

public class ATInstrumentation {

	private static volatile Instrumentation globalInstr;

	public static void premain(String args, Instrumentation inst) {
		globalInstr = inst;
	}

	public static long getObjectSize(Object obj) {
		if (globalInstr == null)
			throw new IllegalStateException("Instrumentation agent not initialized");
		return globalInstr.getObjectSize(obj);
	}
	
}