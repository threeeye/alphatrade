package com.isoplane.at.svc.crypto;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

import com.isoplane.at.adapter.nomics.ATNomicsWebApiAdapter;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKafkaCryptoSource implements IATSystemStatusProvider {

    static final Logger log = LoggerFactory.getLogger(ATKafkaCryptoSource.class);

    static private String ID;
    static private ATKafkaCryptoSource _instance;

    private final CountDownLatch _serviceLatch;

    private Map<String, Long> _errorMap = new TreeMap<>();
    private boolean _isRunning;
    // private PriorityBlockingQueue<SymbolItem> _symbolQueue;
    private ATKafkaSystemStatusProducer _statusProducer;
    private ATKafkaMarketDataPackProducer _marketDataPackProducer;
    private ATNomicsWebApiAdapter _nomicsAdapter;
    private MarketWorker _marketWorker;

    static public void main(String[] args_) {
        try {
            if (args_ == null || args_.length < 1) {
                throw new Exception(
                        String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
            }
            String root = System.getProperty("alphatradePath");
            Configuration config = ATConfigUtil.init(root, args_[0], true);

            final String ipStr = ATSysUtils.getIpAddress("n/a");
            // ATSysLogger.setLoggerClass(String.format("TradierAgent.[%s]", ipStr));

            ID = String.format("%s:%s", ATKafkaCryptoSource.class.getSimpleName(), ipStr);

            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    log.info(String.format("Executing ShutdownHook"));
                    ATExecutors.shutdown();
                    if (_instance != null) {
                        _instance.shutdown();
                    }
                }
            }));

            ATExecutors.init();
            ATKafkaRegistry.register("cry_src",
                    // ATKafkaMarketDataProducer.class,
                    ATKafkaMarketDataPackProducer.class, ATKafkaSystemStatusProducer.class);

            _instance = new ATKafkaCryptoSource();
            _instance.init();

            String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
            log.info(msg);
            // ATSysLogger.info(String.format("%s [%s] started",
            // _instance.getClass().getSimpleName(), ipStr));

            _instance.start();

            if (config.getBoolean("runAsService", true)) {
                log.info(String.format("Running as service"));
                _instance._serviceLatch.await();
            } else {
                ATSysUtils.waitForEnter();
            }
            System.exit(0);

        } catch (Exception ex) {
            log.error(String.format("Failed to start %s", ATKafkaCryptoSource.class.getSimpleName()), ex);
            System.exit(1);
        }
    }

    public ATKafkaCryptoSource() {
        _serviceLatch = new CountDownLatch(2);
    }

    private void init() {
        this._nomicsAdapter = ATNomicsWebApiAdapter.getInstance();
        ATSystemStatusManager.register(this);
    }

    private void start() {
        this._isRunning = true;

        _statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);
        _marketDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);

        _marketWorker = new MarketWorker();
        ATExecutors.submit(MarketWorker.class.getSimpleName(), _marketWorker);

        String pre = this.getClass().getSimpleName();
        ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

        ATKafkaRegistry.start();
    }

    protected void shutdown() {
    }

    private void sendMarketData(Map<String, MarketData> data_) {
        if (data_ == null || data_.isEmpty())
            return;
        MarketDataPack.Builder builder = MarketDataPack.newBuilder();
        builder.addAllMarketData(data_.values());
        builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
        builder.setSubType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA_PACK);
        builder.setSource(ATNomicsWebApiAdapter.SOURCE_KEY);
        builder.addAllSymbols(data_.keySet());
        builder.setSeqLength(data_.size());
        builder.setSeq((int) this._marketWorker._cycleCount);
        MarketDataPack pack = builder.build();
        ATProtoMarketDataPackWrapper wrapper = new ATProtoMarketDataPackWrapper(pack);
        if (log.isDebugEnabled()) {
            log.debug(String.format("sendMarketData [%02d/%d]", this._marketWorker._cycleCount + 1, data_.size()));
        }
        this._marketDataPackProducer.send(wrapper);
    }

    @Override
    public Map<String, String> getSystemStatus() {
        Configuration config = ATConfigUtil.config();
        Map<String, String> status = new TreeMap<>();

        status.put("nomics.symbols", String.format("%s", config.getString("nomics.symbols")));
        status.put("nomics.pricing.count", String.format("%d", this._nomicsAdapter.pricingCount));
        status.put("nomics.cycle.count", String.format("%d", this._marketWorker._cycleCount));
        status.put("nomics.cycle.duration", String.format("%s", this._marketWorker._cycleDuration));

        for (Entry<String, Long> entry : _errorMap.entrySet()) {
            status.put(String.format("errors.%s", entry.getKey()), String.format("%d", entry.getValue()));
            // status.put("count.errors.ids", String.format("%s", _errorMap.keySet()));
        }

        return status;
    }

    @Override
    public boolean isRunning() {
        return this._isRunning;
    }

    public class MarketWorker implements Runnable {

        private long _cycleCount = 0;
        private Duration _cycleDuration = Duration.ZERO;

        @Override
        public void run() {

            while (_isRunning) {
                try {
                    Configuration config = ATConfigUtil.config();
                    long start = System.currentTimeMillis();

                    String symStr = config.getString("nomics.symbols");
                    if (!StringUtils.isBlank(symStr)) {
                        Set<String> symbols = Arrays.stream(symStr.split(",")).map(String::trim)
                                .collect(Collectors.toSet());

                        if (log.isDebugEnabled()) {
                            log.debug(String.format("Processing: %s", symStr));
                        }
                        Map<String, MarketData> data = ATKafkaCryptoSource.this._nomicsAdapter
                                .getProtoMarketData(symbols);
                        if (data != null && !data.isEmpty()) {
                            ATKafkaCryptoSource.this.sendMarketData(data);
                        }
                        if (data == null || data.isEmpty()
                                || (symbols.removeAll(data.keySet()) && !symbols.isEmpty())) {
                            // log.warn(String.format("Processing error: Missing symbols %s", symbols));
                            for (String id : symbols) {
                                Long count = _errorMap.get(id);
                                Long newCount = count != null ? count + 1 : 1;
                                _errorMap.put(id, newCount);
                                log.warn(String.format("Failed [%s/%d]", id, newCount));
                            }
                        }
                    } else {
                        log.warn(String.format("Processing error: No symbols defined"));
                        Thread.sleep(ATKafkaCryptoSource.this._nomicsAdapter.getThrottleTime());
                    }
                    long durationMs = System.currentTimeMillis() - start;
                    _cycleDuration = Duration.ofMillis(durationMs);

                    // final String pollKey = "nomics.queue.poll.period";
                    // long pollPeriod = config.getLong(pollKey, 5000);

                    _cycleCount++;
                    // long delta = pollPeriod - durationMs;
                    // if (delta > 0) {
                    // Thread.sleep(delta);
                    // }
                } catch (Exception ex) {
                    log.error(String.format("%s Error"), this.getClass().getSimpleName(), ex);
                }
            }
        }

    }

    private class StatusRunner implements Runnable {

        @Override
        public void run() {
            // NOTE: Dual nested to (a) reduce try/cath overhead (b) avoid nested iteration
            while (_isRunning) {
                try {
                    while (_isRunning) {
                        long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
                        Map<String, String> status = ATSystemStatusManager.getStatus();
                        _statusProducer.send(ID, status);
                        Thread.sleep(statusPeriod);
                    }
                } catch (Exception ex) {
                    log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
                }
            }
            log.info(String.format("%s Stopped", this.getClass().getSimpleName()));
        }
    }

    // public static class SymbolItem implements Comparable<SymbolItem> {
    //     public SymbolItem(String symbol_, int priority_) {
    //         symbol = symbol_;
    //         priority = priority_;
    //     }

    //     public String symbol;
    //     public int priority;

    //     @Override
    //     public String toString() {
    //         return this.symbol;
    //     }

    //     @Override
    //     public int compareTo(SymbolItem other_) {
    //         if (other_ == null) {
    //             return -1;
    //         }
    //         return other_.symbol.compareTo(this.symbol);
    //     }
    // }

}
