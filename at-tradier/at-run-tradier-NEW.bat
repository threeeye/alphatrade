title AlphaTrade - Tradier Data Agent NEW
SET dir=C:/Dev/workspace/alphatrade/at-tradier

C:\Dev\ProcessMonitor.exe -P C:\Dev\Java\jre-9.0.1\bin\java -A "-Dlogback.configurationFile=/%dir%/tradier-logback.xml -jar %dir%/jar/at-tradier-NEW.jar %dir%/../NOCHECKIN-local.properties config/tradier" -T 60000 -K "OutOfMemoryError,UnknownHostException,Excessive timeouts,Error scheduling output" -D 5000 -L C:\data\logs\PM-tradier.log -S logs3.papertrailapp.com:50247:AT

EXIT
