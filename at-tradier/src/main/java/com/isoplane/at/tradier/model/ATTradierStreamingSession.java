package com.isoplane.at.tradier.model;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ATTradierStreamingSession {

	public ATTradierStream stream;
	public Long timeStamp;
	public String streamId;

	public Map<String, String> buildParams(Collection<String> symbols_, String... filter_) {
		String symStr = String.join(",", symbols_);
		Map<String, String> params = new HashMap<>();
		params.put("sessionid", stream.sessionid);
		params.put("symbols", symStr);
		params.put("linebreak", "true");
		if (filter_.length > 0) {
			String filters = String.join(",", Arrays.asList(filter_));
			params.put("filter", filters); // quote, summary, trade
		}
		return params;
	}

	public String getUrl() {
		return stream.url;
	}

	public static class ATTradierStream {
		public String sessionid;
		public String url;
	}
}
