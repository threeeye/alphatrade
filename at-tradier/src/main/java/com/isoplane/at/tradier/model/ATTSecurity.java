package com.isoplane.at.tradier.model;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.time.DateUtils;

import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.util.ATFormats;

public class ATTSecurity extends ATPersistentSecurity {

	private static final long serialVersionUID = 1L;

	public static final String MIN_TICK = "mintk";
	transient private long _optionUpdate; // Used for stock type to schedule nightly option chain refresh
	transient private Set<String> expirations;
	transient private Set<String> pricedExpirations;
	transient private Set<Double> strikes;
	// transient private boolean isFoundation;

	public ATTSecurity() {
	}

	public ATTSecurity(String occId_) {
		super(occId_);
		if (isOption()) {
			setOptionUpdate(0);
		}
	}

	public long getOptionUpdate() {
		return _optionUpdate;
	}

	public void setOptionUpdate(long value_) {
		this._optionUpdate = value_;
	}

	public Set<String> getExpirations() {
		if (expirations == null) {
			expirations = new TreeSet<>();
		}
		return expirations;
	}

	public void setExpirations(Set<String> expirations_) {
		this.expirations = expirations_;
	}

	public Double getMinTick() {
		return (Double) get(MIN_TICK);
	}

	public void setMinTick(Double minTick) {
		put(MIN_TICK, minTick);
	}

	@Override
	public void setExpirationDate(Date date_) {
		if (date_ != null) {
			String dstr = ATFormats.DATE_yyyyMMdd.get().format(date_);
			super.setExpirationDateStr(dstr);
		}
		super.setExpirationDate(date_);
	}

	// public boolean isFoundation() {
	// return isFoundation;
	// }
	//
	// public void setFoundation(boolean isFoundation_) {
	// this.isFoundation = isFoundation_;
	// }

	public Set<Double> getStrikes() {
		if (strikes == null) {
			strikes = new TreeSet<>();
		}
		return strikes;
	}

	public void setStrikes(Set<Double> strikes_) {
		this.strikes = strikes_;
	}

	public Set<String> getPricedExpirations() {
		if (pricedExpirations == null) {
			pricedExpirations = new TreeSet<>();
		}
		return pricedExpirations;
	}

	public void setPricedExpirations(Set<String> pricedExpirations_) {
		this.pricedExpirations = pricedExpirations_;
	}

	public Set<String> getActiveExpirations(int cutoffDays_) {
		if (getExpirations().isEmpty())
			return null;
		Date cutoffDate = DateUtils.addDays(new Date(), cutoffDays_);
		String cutoffDateStr = ATFormats.DATE_yyyy_MM_dd.get().format(cutoffDate);
		Set<String> expDates = getExpirations().stream().filter(d -> d.compareTo(cutoffDateStr) <= 0).collect(Collectors.toSet());
		return expDates;
	}

	public Set<String> getUnpricedExpirations(int cutoffDays_) {
		Set<String> expDates = getActiveExpirations(cutoffDays_);
		if (expDates == null)
			return null;
		Set<String> pricedDates = getPricedExpirations();
		expDates.removeAll(pricedDates);
		return expDates;
	}

}
