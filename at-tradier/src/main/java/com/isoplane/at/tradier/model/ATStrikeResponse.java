package com.isoplane.at.tradier.model;

import java.util.List;

public class ATStrikeResponse {
	
	public ATInternalStrikes strikes;

	public static class ATInternalStrikes {
		public List<Double> strike;
	}
}
