package com.isoplane.at.tradier.util;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.isoplane.at.commons.service.IATMarketDataSubscriber;
import com.isoplane.at.tradier.model.ATTradierStreamingMarketData;
import com.isoplane.at.tradier.model.IATHttpStringListener;

@Deprecated
public class ATTradierStreamProcessor implements IATHttpStringListener {

	private final Logger log;// = LoggerFactory.getLogger(ATTradierStreamProcessor.class);

	static private Gson _gson;

	private Configuration _config;
	private long _count;
	private long _errorCount;
	private Long _maxCount;
	private Long _errorMax;
	private boolean _isTerminated = false;
	private Map<String, ATTradierStreamingMarketData> _throttleMap;
	private IATMarketDataSubscriber _listener;
	private String _id;
	private Long _timeout;

	public ATTradierStreamProcessor(Configuration config_, IATMarketDataSubscriber listener_, Long timeout_, String name_) {
		this(config_, listener_, timeout_, name_, null);
	}

	public ATTradierStreamProcessor(Configuration config_, IATMarketDataSubscriber listener_, Long timeout_, String streamId_, Long maxCount_) {
		log = LoggerFactory.getLogger(String.format("%s.%s", getClass().getSimpleName(), streamId_));
		_config = config_;
		_id = streamId_;
		_maxCount = maxCount_;
		_listener = listener_;
		_timeout = timeout_;
		_errorMax = _config.getLong("tradier.stream.errors.max");
		_throttleMap = new TreeMap<>();
		_gson = new GsonBuilder()
				.registerTypeAdapter(Double.class, new GsonDoubleTypeAdapter())
				.registerTypeAdapter(Integer.class, new GsonIntegerTypeAdapter())
				.registerTypeAdapter(Long.class, new GsonLongTypeAdapter()).create();
	}

	@Override
	public String getId() {
		return _id;
	}

	@Override
	public Long getTimeout() {
		return _timeout == null ? -1 : _timeout;
	}

	@Override
	public Long reset() {
		log.debug("reset");
		Long count = _count;
		_count = 0;
		_errorCount = 0;
		_isTerminated = false;
		return count;
	}

	Double googlPxLst = 0.0;

	@Override
	public int notify(String str_) {
		try {
			_count++;
			ATTradierStreamingMarketData response = _gson.fromJson(str_, ATTradierStreamingMarketData.class);
			if (response != null && response.getSymbol() != null) {
//				if ("tradex".equals(response.type)) {
//					response.type = "trade";
//				}
//				if ("GOOGL".equals(response.getSymbol()) && "trade".equals(response.type)) {
//					log.info(response.type);
//				}
				ATTradierStreamingMarketData previous = _throttleMap.get(response.getSymbol());
				if (previous != null) {
					boolean proceed = true;
					long throttleMin = _config.getLong("tradier.stream.throttle.min");
					Long timestamp = previous.timestamps.get(response.type);
					if (timestamp == null)
						timestamp = 0L;
					long delta = timestamp != null ? System.currentTimeMillis() - timestamp : throttleMin + 10;
					if (!"trade".equals(response.type) && !"tradex".equals(response.type)) {
						if (delta < throttleMin) {
							if (log.isDebugEnabled())
								log.debug(String.format("Throttling [%-6s.%s]", response.getSymbol(), response.type));
							proceed = false;
							// return 0;
						}
						long throttleMax = _config.getLong("tradier.stream.throttle.max");
						if (delta < throttleMax) {
							Double ratio = _config.getDouble("tradier.stream.minratio");
							if (!response.isSignificantChange(ratio, previous)) {
								if (log.isTraceEnabled())
									log.trace(String.format("Ingoring change [%-6s,%5.4f]: %s", response.getSymbol(), ratio, str_));
								proceed = false;
								// return 0;
							}
						}
					} else if (response.last == null || response.last.isNaN() || response.last == previous.last) {
						if (log.isTraceEnabled())
							log.trace(String.format("Ingoring last [%-6s,%5.4f]: %s", response.getSymbol(), response.last, str_));
						proceed = false;
						// return 0;
					}
					previous.merge(response);
					if (!proceed)
						return 0;
					response = previous;
				} else {
					if (response.open != null && response.open.isNaN())
						response.open = null;
					if (response.close != null && response.close.isNaN())
						response.close = null;
					if (response.prevClose != null && response.prevClose.isNaN())
						response.prevClose = null;
					if (response.high != null && response.high.isNaN())
						response.high = null;
					if (response.low != null && response.low.isNaN())
						response.low = null;
					if (response.last != null && response.last.isNaN())
						response.last = null;
				}
//				if ("GOOGL".equals(response.getSymbol()) && "trade".equals(response.type)) {
//					log.info(response.type);
//				}
				response.timestamps.put(response.type, System.currentTimeMillis());
				_throttleMap.put(response.getSymbol(), response);
				if (_listener != null) {
					_listener.notify(response);
				}
			}
			if (log.isDebugEnabled())
				log.debug(String.format("%4d: %s", _count, str_));

			if (_maxCount != null && _count >= _maxCount) {
				log.debug(String.format("Terminating after [%d]", _count));
				terminate();
				return -2;
			} else {
				return 0;
			}
		} catch (NumberFormatException ex) {
			_errorCount++;
			log.error(String.format("Deserialization error [%d]: %s - %s", _errorCount, ex.getMessage(), str_));
		} catch (Exception ex) {
			_errorCount++;
			log.error(String.format("Streaming error [%d]: %s", _errorCount, str_), ex);
		}
		// NOTE: Only recycle if no maxCount
		return (_maxCount == null && _errorCount > _errorMax) ? -5 : 0;
	}

	@Override
	public boolean isTerminated() {
		return _isTerminated;
	}

	@Override
	public Long terminate() {
		log.debug("terminate");
		_isTerminated = true;
		return _count;
	}

	public static class GsonIntegerTypeAdapter extends TypeAdapter<Number> {

		@Override
		public void write(JsonWriter out, Number value)
				throws IOException {
			out.value(value);
		}

		@Override
		public Number read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}
			try {
				String result = in.nextString();
				if (result.length() == 0) {
					return null;
				}
				return Integer.parseInt(result);
			} catch (NumberFormatException e) {
				throw new JsonSyntaxException(e);
			}
		}
	}

	public static class GsonLongTypeAdapter extends TypeAdapter<Number> {

		@Override
		public void write(JsonWriter out, Number value)
				throws IOException {
			out.value(value);
		}

		@Override
		public Number read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}
			try {
				String result = in.nextString();
				if (result.length() == 0) {
					return null;
				}
				return Long.parseLong(result);
			} catch (NumberFormatException e) {
				throw new JsonSyntaxException(e);
			}
		}
	}

	public static class GsonDoubleTypeAdapter extends TypeAdapter<Number> {

		@Override
		public void write(JsonWriter out, Number value)
				throws IOException {
			out.value(value);
		}

		@Override
		public Number read(JsonReader in) throws IOException {
			if (in.peek() == JsonToken.NULL) {
				in.nextNull();
				return null;
			}
			try {
				String result = in.nextString();
				if (result.length() == 0) {
					return null;
				}
				return Double.parseDouble(result);
			} catch (NumberFormatException e) {
				throw new JsonSyntaxException(e);
			}
		}
	}

}
