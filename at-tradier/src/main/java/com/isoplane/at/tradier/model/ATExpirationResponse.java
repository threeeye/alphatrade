package com.isoplane.at.tradier.model;

import java.util.List;

public class ATExpirationResponse {

	public ATInternalExpirations expirations;
	
	public static class ATInternalExpirations {
		public List<String> date;
	}
}
