package com.isoplane.at.tradier.model;

public interface IATHttpStringListener {

	int notify(String str);

	/**
	 * Resets listener state
	 * 
	 * @return Event count since last reset
	 */
	Long reset();

	/**
	 * Terminates listener
	 * 
	 * @return Event count since last reset
	 */
	Long terminate();
	
	boolean isTerminated();
	
	String getId();
	
	Long getTimeout();
}
