package com.isoplane.at.tradier.model;

import com.isoplane.at.commons.model.IATMarketData;

public interface IATTradierMarketData extends IATMarketData {

	void setAtId(String value);
	
	String getSymbol();
}
