package com.isoplane.at.tradier.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.BiMap;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData.Builder;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.tradier.model.ATTradierStreamingSession;

public class ATTradierProtoConnector {

	static final Logger log = LoggerFactory.getLogger(ATTradierProtoConnector.class);
	static final Gson _gson = new Gson();

	static public final String SOURCE_KEY = "TRD";

	private PoolingHttpClientConnectionManager _httpConManager;
	private CloseableHttpAsyncClient _asyncHttpclient;
	private String _tradierToken;
	private int _streamingSessionTimeout;
	private long _streamingRequestTimeout;
	private long _errorMax;
	// private int _streamingStreamTimeout;
	private String _urlBase;
	private Configuration _config;
	private boolean _isRunning;
	private Map<String, ATTradierStreamingSession> _sessionMap;
	private EATMarketStatus _mktStatus;
	private Set<IATProtoMarketSubscriber> _subscribers;

	private Map<String, MarketData> _cache;

	public ATTradierProtoConnector(Configuration config_) {
		_config = config_;

		_mktStatus = EATMarketStatus.OPEN;
		_urlBase = _config.getString("tradier.baseUrl");
		_tradierToken = _config.getString("tradier.token");
		_streamingSessionTimeout = _config.getInt("tradier.stream.ssn.timeout");
		_errorMax = _config.getLong("tradier.stream.errors.max");
		_streamingRequestTimeout = _config.getLong("tradier.stream.position.timeout", -1);

		// _streamingStreamTimeout = _config.getInt("tradier.stream.str.timeout");
		_sessionMap = new HashMap<>();
		_subscribers = new HashSet<>();
		_cache = new ConcurrentHashMap<>();
		_httpConManager = new PoolingHttpClientConnectionManager();
	}

	public MarketData getMarketData(String occId_) {
		return this._cache.get(occId_);
	}

	public void initAsyncClient() {
		RequestConfig requestConfig = RequestConfig.custom()
				.setSocketTimeout(3000)
				.setConnectTimeout(3000).build();
		_asyncHttpclient = HttpAsyncClients.custom()
				.setDefaultRequestConfig(requestConfig)
				.setMaxConnPerRoute(1000)
				.setMaxConnTotal(1000)
				.build();
		_isRunning = true;
	}

	public void shutdown() {
		log.debug(String.format("Shutting down"));
		try {
			_isRunning = false;
			if (_httpConManager != null)
				_httpConManager.close();
			if (_asyncHttpclient != null)
				_asyncHttpclient.close();
		} catch (IOException ex) {
			log.error(String.format("Error shutting down", ex));
		}
	}

	public void subscribe(IATProtoMarketSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	private String getHttp(String url_, String tradierToken_) {
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(_httpConManager).build();
			HttpGet httpGet = new HttpGet(url_);
			httpGet.setHeader("Accept", "application/json");
			if (StringUtils.isNotBlank(tradierToken_)) {
				httpGet.setHeader("Authorization", tradierToken_);
			}
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			response = httpclient.execute(httpGet);
			try {
				@SuppressWarnings("unused")
				Header h1 = response.getFirstHeader("X-Ratelimit-Allowed");
				@SuppressWarnings("unused")
				Header h2 = response.getFirstHeader("X-Ratelimit-Used");
				Header h3 = response.getFirstHeader("X-Ratelimit-Available");
				@SuppressWarnings("unused")
				Header h4 = response.getFirstHeader("X-Ratelimit-Expiry");
				if (h3 != null) {
					String h3v = h3.getValue();
					int available = Integer.parseInt(h3v);
					if (available < 10) {
						log.warn(String.format("Header limit approaching [%d]", available));
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing headers"), ex);
			}

			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (Exception ex) {
			log.error(String.format("Error requesting: %s", url_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

	private String postHttp(String url_, String tradierToken_, Map<String, String> params_) {
		CloseableHttpResponse response = null;
		try {
			CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(_httpConManager).build();
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url_);
			if (params_ != null) {
				ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
				for (Entry<String, String> e : params_.entrySet()) {
					postParameters.add(new BasicNameValuePair(e.getKey(), e.getValue()));
				}
				httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
			}
			httpPost.setHeader("Accept", "application/json");
			if (StringUtils.isNotBlank(tradierToken_)) {
				httpPost.setHeader("Authorization", tradierToken_);
			}
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			response = httpclient.execute(httpPost);
			try {
				@SuppressWarnings("unused")
				Header h1 = response.getFirstHeader("X-Ratelimit-Allowed");
				@SuppressWarnings("unused")
				Header h2 = response.getFirstHeader("X-Ratelimit-Used");
				Header h3 = response.getFirstHeader("X-Ratelimit-Available");
				@SuppressWarnings("unused")
				Header h4 = response.getFirstHeader("X-Ratelimit-Expiry");
				if (h3 != null) {
					String h3v = h3.getValue();
					int available = Integer.parseInt(h3v);
					if (available < 10) {
						log.warn(String.format("Header limit approaching [%d]", available));
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing headers: %s", Arrays.asList(response.getAllHeaders())), ex);
			}

			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (ConnectException cex) {
			String msg = String.format("Error connecting: %s", url_);
			log.error(msg);
			ATSysLogger.error(msg);
			return null;
		} catch (Exception ex) {
			String msg = String.format("Error requesting: %s", url_);
			log.error(msg, ex);
			ATSysLogger.error(msg);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

	private int streamHttp(String url_, String token_, Map<String, String> params_, BiMap<String, String> reverseSymbolMap_) throws Exception {
		int result = -50;
		String watchThreadId = String.format("%s.idleStreamWatch", this.getClass().getSimpleName());
		CloseableHttpClient httpclient = HttpClients.custom().setConnectionManager(_httpConManager).build();
		HttpPost httpPost = new HttpPost(url_);
		if (params_ != null) {
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			for (Entry<String, String> e : params_.entrySet()) {
				postParameters.add(new BasicNameValuePair(e.getKey(), e.getValue()));
			}
			httpPost.setEntity(new UrlEncodedFormEntity(postParameters, "UTF-8"));
		}
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Connection", "Keep-Alive");
		if (StringUtils.isNotBlank(token_)) {
			httpPost.setHeader("Authorization", token_);
		}
		ScheduledFuture<?> future = null;
		try (CloseableHttpResponse response = httpclient.execute(httpPost);) {
			if (this._streamingRequestTimeout > 0) {
				future = ATExecutors.schedule(watchThreadId, new Runnable() {

					@Override
					public void run() {
						Thread.currentThread().setName(watchThreadId);
						try {
							if (!httpPost.isAborted()) {
								log.debug(String.format("Terminating timed-out stream [%d]", ATTradierProtoConnector.this._streamingRequestTimeout));
								httpPost.abort();
								response.close(); // NOTE: Results in scanner reading null below
							}
						} catch (Exception ex) {
							log.error(String.format("Error closing stream"), ex);
						}
					}
				}, this._streamingRequestTimeout);
			}
			HttpEntity entity = response.getEntity();
			InputStream inStream = entity.getContent();
			InputStream errorStream = entity.getContent();
			try (Scanner scanner = new Scanner(inStream).useDelimiter("\n")) {
				int code = -1;
				int errorCount = 0;
				do {
					String str = scanner.hasNext() ? scanner.next() : null;
					if (str == null) {
						// listener_.terminate();
						if (log.isDebugEnabled()) {
							log.debug(String.format("Stream closed [%s]", url_));
						}
						code = -1;
					} else {
						if (!str.startsWith("{")) {
							try (Scanner errorScanner = new Scanner(errorStream)) {
								log.warn(String.format("Invalid json: %s", errorScanner.next()));
							}
							errorCount++;
							code = errorCount > _errorMax ? -5 : 0;
						} else {
							if (this._subscribers != null && !this._subscribers.isEmpty()) {
								MarketData mkt = streamToMarketData(str, reverseSymbolMap_);
								if (mkt != null) {
									Set<ATProtoMarketDataWrapper> protos = Collections.singleton(new ATProtoMarketDataWrapper(mkt));
									for (IATProtoMarketSubscriber subscriber : _subscribers) {
										subscriber.notifyMarketData(protos);
									}
								}
							}
							code = 0;
						}
						// code = listener_.notify(str);

						// if (str.indexOf("\"type\":\"quote\"") < 0
						// && str.indexOf("\"type\":\"trade\"") < 0
						// && str.indexOf("\"type\":\"summary\"") < 0) {
						// int xxx = 0;
						// }
					}
				} while (code >= 0);
				if (code == -5) {
					try (Scanner errorScanner = new Scanner(errorStream)) {
						if (errorScanner.hasNext())
							log.warn(String.format("Streaming error trigger: %s", errorScanner.next()));
					}
				}
				result = code;
				httpPost.abort(); // NOTE: Critically important line, or Close gets stuck!
			} catch (Exception ex) {
				String detailStr = "";
				try (Scanner errorScanner = new Scanner(entity.getContent())) {
					if (errorScanner.hasNext())
						detailStr = errorScanner.next();
				}
				log.error(String.format("Streaming error [%s]: %s", url_, detailStr), ex);
				result = -100;
			}
		} catch (Exception exx) {
			log.error(String.format("Error initiating stream [%s]", url_), exx);
			result = -10;
		} finally {
			if (future != null && !future.isCancelled())
				future.cancel(false);
		}
		return result;
	}

	public void streamSymbols(String streamId_, Collection<String> symbols_, String params_, BiMap<String, String> reverseSymbolMap_) {
		if (symbols_ == null || symbols_.isEmpty()) {
			log.info(String.format("No symbols [%s]", streamId_));
			int pause = _config.getInt("tradier.pause.marketClosed");
			try {
				Thread.sleep(pause);
			} catch (InterruptedException e) {
				log.error(String.format("Sleep interrupted [%s]", streamId_));
			}
			return;
		}
		int code = -10;
		try {
			int pause = _config.getInt("tradier.stream.break.open");
			do {
				ATTradierStreamingSession session = getStreamingSession(streamId_);
				if (session != null) {
					Map<String, String> params = session.buildParams(symbols_, params_); // quote, trade, summary, timesale, tradex

					code = streamHttp(session.getUrl(), _tradierToken, params, reverseSymbolMap_);
					if (code >= 0 && _isRunning) {
						log.info(String.format("Restarting stream [%s]...", streamId_));
						Thread.sleep(pause);
					}
					// listener_.reset();
				} else {
					log.error(String.format("Error creating streaming session [%s]", streamId_));
					_isRunning = false;
					code = -100;
				}
			} while (code >= 0 && _isRunning);
			String reason = code == -2 ? "Max count reached" : code + "";
			log.debug(String.format("Terminated streaming session [%s]: %s", streamId_, reason));
		} catch (Exception ex) {
			log.error(String.format("Error streaming [%s]", streamId_), ex);
		}
	}

	private ATTradierStreamingSession getStreamingSession(String streamId_) {
		ATTradierStreamingSession oldSession = _sessionMap.get(streamId_);
		if (oldSession != null && (System.currentTimeMillis() - oldSession.timeStamp < _streamingSessionTimeout)) {
			log.debug(String.format("Recycling streaming session [%s]", streamId_));
			return oldSession;
		}
		try {
			String url = _urlBase + "/v1/markets/events/session";
			String json = postHttp(url, _tradierToken, null);
			if (StringUtils.isNotBlank(json)) {
				ATTradierStreamingSession session = _gson.fromJson(json, ATTradierStreamingSession.class);
				session.streamId = streamId_;
				session.timeStamp = System.currentTimeMillis();
				log.debug(String.format("Received streaming endpoint %s", json));
				_sessionMap.put(streamId_, session);
				return session;
			}
		} catch (Exception ex) {
			log.error(String.format("Streaming connection interrupted..."), ex);
			// log.error(String.format("Streaming connection interrupted [restarting=%b/%d]", __isRunning, _restartCount), ex);
		}
		return null;
	}

	// Pre-market trading is 4:00-9:30. After-hours trading is 16:00-20:00.
	public EATMarketStatus getMarketStatus() {
		try {
			String jsonStr = getHttp(_urlBase + "/v1/markets/clock", _tradierToken);
			JsonObject root = _gson.fromJson(jsonStr, JsonObject.class).getAsJsonObject();
			JsonObject clock = root.getAsJsonObject("clock");
			String status = clock.get("state").getAsString();
			log.debug(String.format("%s[%s]", "updateMarketStatus", status));
			switch (status) {
			case "closed":
				_mktStatus = EATMarketStatus.CLOSED;
				break;
			case "open":
				_mktStatus = EATMarketStatus.OPEN;
				break;
			case "postmarket":
				_mktStatus = EATMarketStatus.POST;
				break;
			case "premarket":
				_mktStatus = EATMarketStatus.PRE;
				break;
			default:
				_mktStatus = EATMarketStatus.CLOSED;
				break;
			}
			return _mktStatus;
		} catch (Exception ex) {
			log.error("Error updating market status", ex);
			return null;
		}
	}

	public Collection<String> requestQuotes(Collection<String> ids_, BiMap<String, String> reverseSymbolMap_) {
		if (ids_ == null || ids_.isEmpty())
			return null;
		String jsonStr = null;
		try {
			String url = _urlBase + "/v1/markets/quotes";
			String trdIds = String.join(",", ids_);
			Map<String, String> params = new HashMap<>();
			params.put("symbols", trdIds);// .replaceAll("\\.", "/")); // TODO: Prob don't need replace now
			jsonStr = postHttp(url, _tradierToken, params);

			List<ATProtoMarketDataPackWrapper> protoWrappers = requestToMarketData(jsonStr, reverseSymbolMap_);
			if (protoWrappers == null || protoWrappers.isEmpty()) {
				return ids_;
			}
			BiMap<String, String> symbolMap = reverseSymbolMap_.inverse();
			Set<String> unmatched = new HashSet<>(ids_);
			for (ATProtoMarketDataPackWrapper wrapper : protoWrappers) {
				if (this._subscribers != null) {
					for (IATProtoMarketSubscriber subscriber : this._subscribers) {
						subscriber.notifyMarketDataPack(wrapper);
					}
				}
				String occId = wrapper.proto().getSymbols(0);
				String atId = convertSymbol(occId, symbolMap);
				unmatched.remove(atId);
			}
			// if (this._subscribers != null) {
			// for (IATProtoMarketSubscriber subscriber : this._subscribers) {
			// subscriber.notifyMarketDataPack(protoWrapper);
			// }
			// }
			return unmatched;
		} catch (Exception ex) {
			log.error(String.format("requestQuotes Error: %s", jsonStr), ex);
			return null;
		}
	}

	private String convertSymbol(String trdSym_, Map<String, String> reverseSymbolMap_) {
		String atSym = reverseSymbolMap_.get(trdSym_);
		if (atSym == null) {
			atSym = trdSym_;
		}
		return atSym;
	}

	private List<ATProtoMarketDataPackWrapper> requestToMarketData(String json_, Map<String, String> reverseSymbolMap_) {
		try {
			// Set<String> symbols = new TreeSet<>();
			Map<String, List<MarketData>> protoMap = new HashMap<>();
			// List<MarketData> protoMkts = new ArrayList<>();
			// List<ATProtoMarketDataWrapper> result_digest = new ArrayList<>();
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode quotesNode = rootNode.get("quotes");
			JsonNode quoteNode = quotesNode.get("quote");
			Iterator<JsonNode> iQuote = quoteNode.elements();
			while (iQuote.hasNext()) {
				JsonNode mktNode = iQuote.next();
				Builder mktBld = MarketData.newBuilder();

				String occId;
				String trdSymbol = mktNode.get("symbol").asText();
				if (trdSymbol.length() > 10) {
					String trdUnderlying = mktNode.get("underlying").asText();
					String post = trdSymbol.substring(trdUnderlying.length());
					String underlying = convertSymbol(trdUnderlying, reverseSymbolMap_);
					occId = String.format("%-6s%s", underlying, post);
					mktBld.setIdUl(underlying);
				} else {
					occId = convertSymbol(trdSymbol, reverseSymbolMap_);
				}
				mktBld.setId(occId);
				String symbol = ATModelUtil.getSymbol(occId);

				// >> PX
				Double pxAsk = getDouble(mktNode, "ask");
				if (pxAsk != null) {
					mktBld.setPxAsk(pxAsk);
				}
				Double pxBid = getDouble(mktNode, "bid");
				if (pxBid != null) {
					mktBld.setPxBid(pxBid);
				}
				Double pxClose = getDouble(mktNode, "close");
				if (pxClose != null) {
					mktBld.setPxClose(pxClose);
				}
				Double pxClosePre = getDouble(mktNode, "prevclose");
				if (pxClosePre != null) {
					mktBld.setPxClosePre(pxClosePre);
				}
				Double pxChange = getDouble(mktNode, "change_percentage");
				if (pxChange != null) {
					mktBld.setPxDayCh(pxChange);
				}
				Double pxDayHi = getDouble(mktNode, "high");
				if (pxDayHi != null) {
					mktBld.setPxDayHi(pxDayHi);
				}
				Double pxDayLo = getDouble(mktNode, "low");
				if (pxDayLo != null) {
					mktBld.setPxDayLo(pxDayLo);
				}
				Double pxLast = getDouble(mktNode, "last");
				if (pxLast != null) {
					mktBld.setPxLast(pxLast);
				}
				Double pxOpen = getDouble(mktNode, "open");
				if (pxOpen != null) {
					mktBld.setPxOpen(pxOpen);
				}
				// Double pxTheo = mktNode.getPxTheo();
				// if (pxTheo != null) {
				// mktBld.setPxTheo(pxTheo);
				// }
				// << PX

				// >> SZ
				Integer szAsk = getInteger(mktNode, "asksize");
				if (szAsk != null) {
					mktBld.setSzAsk(szAsk);
				}
				Integer szBid = getInteger(mktNode, "bidsize");
				if (szBid != null) {
					mktBld.setSzBid(szBid);
				}
				Integer szLast = getInteger(mktNode, "last_volume");
				if (szLast != null) {
					mktBld.setSzLast(szLast);
				}
				// << SZ

				// >> TS
				Long tsAsk = getLong(mktNode, "ask_date");
				if (tsAsk != null) {
					mktBld.setTsAsk(tsAsk);
				}
				Long tsBid = getLong(mktNode, "bid_date");
				if (tsBid != null) {
					mktBld.setTsBid(tsBid);
				}
				Long tsLast = getLong(mktNode, "trade_date");
				if (tsLast != null) {
					mktBld.setTsLast(tsLast);
				}
				// << TS

				// >> OTHER
				Long oi = getLong(mktNode, "open_interest");
				if (oi != null) {
					mktBld.setOpenInt(oi);
				}
				Long volDay = getLong(mktNode, "volume");
				if (volDay != null) {
					mktBld.setVolDay(volDay);
				}
				mktBld.setSrc(SOURCE_KEY);
				// << OTHER

				MarketData mkt = mktBld.build();
				MarketData oldMkt = _cache.get(occId);
				if (oldMkt != null) {
					boolean isAskChanged = oldMkt.hasTsAsk() && oldMkt.getTsAsk() != tsAsk;
					boolean isBidChanged = oldMkt.hasTsBid() && oldMkt.getTsBid() != tsBid;
					boolean isLastChanged = oldMkt.hasTsLast() && oldMkt.getTsLast() != tsLast;
					boolean isOiChanged = oldMkt.hasOpenInt() && oldMkt.getOpenInt() != oi;
					boolean isVolDayChanged = oldMkt.hasVolDay() && oldMkt.getVolDay() != volDay;
					boolean isChanged = isAskChanged || isBidChanged || isLastChanged || isOiChanged || isVolDayChanged;
					if (!isChanged) {
						continue;
					}
				}
				// if (oldMkt == null || !oldMkt.equals(mkt)) {
				_cache.put(occId, mkt);
				List<MarketData> protoMktList = protoMap.get(symbol);
				if (protoMktList == null) {
					protoMktList = new ArrayList<>();
					protoMap.put(symbol, protoMktList);
				}
				protoMktList.add(mkt);
				// protoMkts.add(mkt);
				// symbols.add(ATModelUtil.getSymbol(occId));
				// result_digest.add(new ATProtoMarketDataWrapper(mkt));
				// }
			}

			List<ATProtoMarketDataPackWrapper> result = new ArrayList<>();
			for (Entry<String, List<MarketData>> entry : protoMap.entrySet()) {
				MarketDataPack.Builder builder = MarketDataPack.newBuilder();
				builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
				builder.setSubType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
				builder.setSource(SOURCE_KEY);
				builder.addSymbols(entry.getKey());
				builder.addAllMarketData(entry.getValue());
				MarketDataPack mktPack = builder.build();
				ATProtoMarketDataPackWrapper mktWrapper = new ATProtoMarketDataPackWrapper(mktPack);
				result.add(mktWrapper);
			}

			// MarketDataPack.Builder builder = MarketDataPack.newBuilder();
			// builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
			// builder.setSubType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
			// builder.setSource(SOURCE_KEY);
			// builder.addAllMarketData(protoMkts);
			// builder.addAllSymbols(symbols);
			// return new ATProtoMarketDataPackWrapper(builder.build());

			return result;
		} catch (Exception ex) {
			String msg = log.isDebugEnabled() ? String.format("requestToMarketData Error: %s", json_) : "requestToMarketData Error";
			log.error(msg, ex);
			return null;
		}
	}

	/*
	 * {"type":"quote","symbol":"FLIR201218C00045000","bid":0.6,"bidsz":10,"bidexch":"B","biddate":"1599837754000","ask":0.75,"asksz":76,"askexch":"M"
	 * ,"askdate":"1599837757000"}
	 * 
	 * {"type":"trade","symbol":"ICPT210319C00060000","exch":"X","price":"5.0","size":"1","cvol":"0","date":"1599665376729","last":"5.0"}
	 * 
	 * {"type":"summary","symbol":"TSLA201016P00250000","open":"5.3","high":"6.3","low":"5.22","prevClose":"6.1","openInterest":"2553"}
	 */
	private MarketData streamToMarketData(String json_, BiMap<String, String> reverseSymbolMap_) {
		try {
			JsonNode mktNode = new ObjectMapper().readTree(json_);
			Builder mktBld = MarketData.newBuilder();

			String occId;
			String trdSymbol = mktNode.get("symbol").asText();
			if (trdSymbol.length() > 10) {
				int i = 0;
				for (; i < trdSymbol.length(); i++) {
					char c = trdSymbol.charAt(i);
					if (Character.isDigit(c))
						break;
				}
				String trdUnderlying = trdSymbol.substring(0, i);
				String underlying = convertSymbol(trdUnderlying, reverseSymbolMap_);
				String post = trdSymbol.substring(i);
				occId = String.format("%-6s%s", underlying, post);
				mktBld.setIdUl(underlying);
			} else {
				occId = convertSymbol(trdSymbol, reverseSymbolMap_);
			}
			mktBld.setId(occId);

			MarketData oldMkt = this._cache.get(occId);

			String type = mktNode.get("type").asText();
			switch (type) {
			case "quote":
				// >> TS
				Long tsAsk = getLong(mktNode, "askdate");
				Long tsBid = getLong(mktNode, "biddate");
				if (oldMkt != null) {
					boolean isChanged = (oldMkt.hasTsAsk() && oldMkt.getTsAsk() != tsAsk)
							|| (oldMkt.hasTsBid() && oldMkt.getTsBid() != tsBid);
					if (!isChanged) {
						return null;
					}
				}
				if (tsAsk != null) {
					mktBld.setTsAsk(tsAsk);
				}
				if (tsBid != null) {
					mktBld.setTsBid(tsBid);
				}
				// << TS
				// >> PX
				Double pxAsk = getDouble(mktNode, "ask");
				if (pxAsk != null) {
					mktBld.setPxAsk(pxAsk);
				}
				Double pxBid = getDouble(mktNode, "bid");
				if (pxBid != null) {
					mktBld.setPxBid(pxBid);
				}
				// << PX
				// >> SZ
				Integer szAsk = getInteger(mktNode, "asksz");
				if (szAsk != null) {
					mktBld.setSzAsk(szAsk);
				}
				Integer szBid = getInteger(mktNode, "bidsz");
				if (szBid != null) {
					mktBld.setSzBid(szBid);
				}
				// << SZ
				break;
			case "summary":
				// >> PX
				Double pxClose = getDouble(mktNode, "close");
				if (pxClose != null) {
					mktBld.setPxClose(pxClose);
				}
				Double pxClosePre = getDouble(mktNode, "prevClose");
				if (pxClosePre != null) {
					mktBld.setPxClosePre(pxClosePre);
				}
				Double pxDayHi = getDouble(mktNode, "high");
				if (pxDayHi != null) {
					mktBld.setPxDayHi(pxDayHi);
				}
				Double pxDayLo = getDouble(mktNode, "low");
				if (pxDayLo != null) {
					mktBld.setPxDayLo(pxDayLo);
				}
				Double pxOpen = getDouble(mktNode, "open");
				if (pxOpen != null) {
					mktBld.setPxOpen(pxOpen);
				}
				// << PX
				// >> OTHER
				Long oi = getLong(mktNode, "openInterest");
				if (oi != null) {
					mktBld.setOpenInt(oi);
				}
				// << OTHER
				break;
			case "trade":
			case "tradex":
				// >> TS
				Long tsLast = getLong(mktNode, "date");
				if (tsLast != null) {
					boolean isChanged = oldMkt != null && oldMkt.hasTsLast() && oldMkt.getTsLast() != tsLast;
					if (!isChanged) {
						return null;
					}
					mktBld.setTsLast(tsLast);
				}
				// << TS
				// >> PX
				Double pxLast = getDouble(mktNode, "last");
				if (pxLast != null) {
					mktBld.setPxLast(pxLast);
				}
				// << PX
				// >> SZ
				Integer szLast = getInteger(mktNode, "size");
				if (szLast != null) {
					mktBld.setSzLast(szLast);
				}
				// << SZ
				// >> OTHER
				Long cumulativeVolume = getLong(mktNode, "cvol");
				if (cumulativeVolume != null) {
					mktBld.setVolDay(cumulativeVolume);
				}
				// << OTHER
				break;
			default:
				log.warn(String.format("Unsupported data type [%s]", type));
				break;
			}
			// >> OTHER
			mktBld.setSrc(SOURCE_KEY);
			// << OTHER

			MarketData mkt = mktBld.build();
			return mkt;
		} catch (Exception ex) {
			String msg = log.isDebugEnabled() ? String.format("streamToMarketData Error: %s", json_) : "streamToMarketData Error";
			log.error(msg, ex);
			return null;
		}
	}

	private Double getDouble(JsonNode node_, String prop_) {
		JsonNode node = node_.get(prop_);
		return node == null ? null : node.asDouble();
	}

	private Integer getInteger(JsonNode node_, String prop_) {
		JsonNode node = node_.get(prop_);
		return node == null ? null : node.asInt();
	}

	private Long getLong(JsonNode node_, String prop_) {
		JsonNode node = node_.get(prop_);
		return node == null ? null : node.asLong();
	}

}
