package com.isoplane.at.tradier.model;

import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATMarketData;

public class ATTradierStreamingMarketData implements IATMarketData, IATTradierMarketData {

	// private static final long serialVersionUID = 1L;

	transient public Map<String, Long> timestamps = new HashMap<>();
	// transient public Long timestamp;
	// NOTE: Internally uses 'symbol' to compare tradier streamed data
	transient public String atId;
	public String type;

	private String symbol;

	// trade
	public Double last;
	public Long size;
	public Long cvol;
	public Long date;

	// quote
	public Double bid;
	public Long bidsz;
	public Long biddate;
	public Double ask;
	public Long asksz;
	public Long askdate;

	// summary
	public Double open;
	public Double high;
	public Double low;
	public Double prevClose;
	public Double close;

	public Long openInterest;

	private Double strike;
	private String right;
	private Double px_change;

	// NOTE: ignored
	// public String exch;
	// public String askexch;
	// public String bidexch;
	// public Double price;

	@Override
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public boolean isSignificantChange(Double ratio_, ATTradierStreamingMarketData previous_) {
		if (type == null)
			return false;
		switch (type) {
		case "quote":
			return isSignificantAskBidPriceChange(ratio_, previous_);
		case "trade":
			return isSignificantTradePriceChange(ratio_, previous_);
		case "summary":
			return isSignificantHighLowOpenClosePriceChange(ratio_, previous_);
		default:
			return true;
		}
	}

	private boolean isSignificantTradePriceChange(Double ratio_, ATTradierStreamingMarketData previous_) {
		if (last == null)
			return false;
		if (previous_ == null || previous_.last == null)
			return true;
		Double diff = Math.abs((last - previous_.last) / last);
		return diff >= ratio_;
	}

	private boolean isSignificantAskBidPriceChange(Double ratio_, ATTradierStreamingMarketData previous_) {
		if ((ask == null) || (bid == null))
			return false;
		if ((previous_ == null) || (previous_.ask == null) || (previous_.bid == null))
			return true;
		Double diff1 = Math.abs((ask - previous_.ask) / ask);
		if (diff1 >= ratio_)
			return true;
		Double diff2 = Math.abs((bid - previous_.bid) / bid);
		return diff2 >= ratio_;
	}

	private boolean isSignificantHighLowOpenClosePriceChange(Double ratio_, ATTradierStreamingMarketData previous_) {
		if (previous_ == null)
			return true;
		if ((high != null) && (low != null)) {
			if ((previous_.high == null) || (previous_.low == null))
				return true;
			Double diff1 = Math.abs((high - previous_.high) / high);
			if (diff1 >= ratio_)
				return true;
			Double diff2 = Math.abs((low - previous_.low) / low);
			if (diff2 >= ratio_)
				return true;
		}
		if ((open != null && (close != null))) {
			if ((previous_.open == null) || (previous_.close == null))
				return true;
			Double diff1 = Math.abs((open - previous_.open) / open);
			if (diff1 >= ratio_)
				return true;
			Double diff2 = Math.abs((close - previous_.close) / close);
			if (diff2 >= ratio_)
				return true;
		}
		return false;

	}

	public void merge(ATTradierStreamingMarketData other_) {
		if (other_ == null || other_.type == null)
			return;
		if (other_.atId != null)
			atId = other_.atId;
		// if (other_.date == null)
		// other_.date = 0L;
		type = other_.type;
		switch (other_.type) {
		case "quote":
			boolean isNewAsk = askdate == null || (other_.askdate != null && other_.askdate > askdate);
			boolean isNewBid = biddate == null || (other_.biddate != null && other_.biddate > biddate);
			if (isNewAsk && other_.ask != null && !other_.ask.isNaN())
				ask = other_.ask;
			if (isNewAsk && other_.asksz != null && other_.asksz != 0)
				asksz = other_.asksz;
			// if (other_.askexch != null)
			// askexch = other_.askexch;
			if (isNewAsk)
				askdate = other_.askdate;
			if (isNewBid && other_.bid != null && !other_.bid.isNaN())
				bid = other_.bid;
			if (isNewBid && other_.bidsz != null && other_.bidsz != 0)
				bidsz = other_.bidsz;
			// if (other_.bidexch != null)
			// bidexch = other_.bidexch;
			if (isNewBid)
				biddate = other_.biddate;
			// break;
		case "trade":
		case "tradex":
			// if (other_.exch != null)
			// exch = other_.exch;
			// if (other_.price != null)
			// price = other_.price;
			boolean isNewLast = date == null || (other_.date != null && other_.date > date);
			if (isNewLast && other_.last != null && !other_.last.isNaN())
				last = other_.last;
			if (isNewLast && other_.size != null && other_.size != 0)
				size = other_.size;
			if (isNewLast && other_.cvol != null && other_.cvol != 0)
				cvol = other_.cvol;
			if (isNewLast && other_.date != null && other_.date != 0)
				date = other_.date;
			if (isNewLast) {
				px_change = IATMarketData.calculatePxChange(this);
			}
			break;
		case "summary":
			if (other_.open != null && !other_.open.isNaN())
				open = other_.open;
			if (other_.high != null && !other_.high.isNaN())
				high = other_.high;
			if (other_.low != null && !other_.low.isNaN())
				low = other_.low;
			if (other_.prevClose != null && !other_.prevClose.isNaN())
				prevClose = other_.prevClose;
			if (other_.close != null && !other_.close.isNaN())
				close = other_.close;
			break;
		default:
			return;
		}
	}
	
	public void calculatePxChange() {
		px_change = IATMarketData.calculatePxChange(this);
	}

	@Override
	public String getAtId() {
		return atId;
	}

	@Override
	public void setAtId(String value) {
		this.atId = value;
	}

	@Override
	public Double getPxLast() {
		return last;
	}

	@Override
	public Integer getSzLast() {
		return size!=null?size.intValue():null;
	}

	@Override
	public Long getTsLast() {
		return date;
	}

	@Override
	public Double getPxAsk() {
		return ask;
	}

	@Override
	public Long getTsAsk() {
		return askdate;
	}

	@Override
	public Integer getSzAsk() {
		return asksz!=null?asksz.intValue():null;
	}

	@Override
	public Double getPxBid() {
		return bid;
	}

	@Override
	public Long getTsBid() {
		return biddate;
	}

	@Override
	public Integer getSzBid() {
		return bidsz!=null?bidsz.intValue():null;
	}

	@Override
	public Long getDailyVol() {
		return cvol;
	}

	@Override
	public Double getPxOpen() {
		return open;
	}

	@Override
	public Double getPxClose() {
		return close;
	}

	@Override
	public Double getPxClosePrev() {
		return prevClose;
	}

	@Override
	public Double getPxHigh() {
		return high;
	}

	@Override
	public Double getPxLow() {
		return low;
	}

	@Override
	public Long getOpenInterest() {
		return openInterest;
	}

	@Override
	public Double getPxStrike() {
		return strike;
	}

	@Override
	public Double getPxChange() {
		return px_change;
	}

	@Override
	public String getOptionRight() {
		return right;
	}
	@Override
	public <T> T getMeta(String key_) {
		return null;
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> result = new HashMap<>();
		IATMarketData.setDailyVol(result, getDailyVol());
		IATMarketData.setPxAsk(result, getPxAsk());
		IATMarketData.setPxBid(result, getPxBid());
		IATMarketData.setPxLast(result, getPxLast());
		IATAsset.setSzAsk(result, getSzAsk());
		IATAsset.setSzBid(result, getSzBid());
		IATAsset.setSzLast(result, getSzLast());
		IATMarketData.setTsAsk(result, getTsAsk());
		IATMarketData.setTsBid(result, getTsBid());
		IATMarketData.setTsLast(result, getTsLast());
		IATMarketData.setPxOpen(result, getPxOpen());
		IATMarketData.setPxClose(result, getPxClose());
		IATMarketData.setPxClosePrev(result, getPxClosePrev());
		IATMarketData.setPxHigh(result, getPxHigh());
		IATMarketData.setPxLow(result, getPxLow());
		IATMarketData.setOpenInterest(result, getOpenInterest());
		IATAsset.setPxStrike(result, getPxStrike());
		IATMarketData.setOptionRight(result, getOptionRight());
		IATMarketData.setPxChange(result, getPxChange());
		return result;
	}

	@Override
	public String getUnderlying() {
		// TODO Auto-generated method stub
		return null;
	}

}
