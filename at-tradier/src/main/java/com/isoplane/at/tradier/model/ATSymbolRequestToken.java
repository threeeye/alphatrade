package com.isoplane.at.tradier.model;

import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATPriorityQueue.IATQueueable;

public class ATSymbolRequestToken implements IATQueueable {

	public String tradierId;
	public String underlyingOccId;
	public Double strike;
	public Integer days2Exp;
	public Integer priority;
	public Boolean isPut;
	public Long lastProcessedTime = -1L;

	@Override
	public String toString() {
		return tradierId;
	}

	@Override
	public int hashCode() {
		return tradierId == null ? 0 : tradierId.hashCode();
	}

	@Override
	public boolean equals(Object other_) {
		if (other_ instanceof ATSymbolRequestToken) {
			return this.hashCode() == other_.hashCode();
		}
		return false;
	}

	@Override
	public Long getLastProcessedTime() {
		return lastProcessedTime;
	}

	@Deprecated
	public int getPriority(IATMarketData market_) {
		int timeFactor = days2Exp == null ? 5 : days2Exp > 180 ? 1 : days2Exp > 90 ? 2 : days2Exp > 30 ? 3 : days2Exp > 14 ? 4 : 5;
		int priceFactor = 1;
		if (market_ != null && strike != null) {
			Double ulPxLast = market_.getPxLast();
			if (ulPxLast != null) {
				boolean isOutside = (strike < 0.5 * ulPxLast || strike > 1.5 * ulPxLast);
				if (isPut) {
					priceFactor = isOutside ? -1 : strike > ulPxLast ? 2 : 4;
				} else {
					priceFactor = isOutside ? -1 : 3;// strike < pxUl_ ? 2 : 4;
				}
			}
		}
		// (-> bulksize=200)
		// 1,6,9 -> 575/390/200
		// 1,7,12 -> 750/430/180 ???
		// 1,7,11 -> 610/350/120
		// (-> bulksize=250)
		// 1,7,10 -> 485/280/100
		// (-> bulksize=300)
		// 1,6,9 -> 360/240/80
		// 1,7,9 -> 380/220/90
		// 1,6,8 -> 340/230/85
		// 1,6,7 -> 310/210/95
		int weightedFactor = (3 * timeFactor + 2 * priceFactor) / 5;
		if (weightedFactor >= 4)
			return 7;
		else if (weightedFactor > 2)
			return 6;
		else
			return 1;
	}

	public int getPriority(MarketData market_) {
		int priceFactor = 1;
		if (market_ != null && strike != null && market_.hasPxLast()) {
			Double ulPxLast = market_.getPxLast();
			if (ulPxLast != null) {
				boolean isOutside = (strike < 0.5 * ulPxLast || strike > 1.5 * ulPxLast);
				if (isPut) {
					priceFactor = isOutside ? -1 : strike > ulPxLast ? 2 : 4;
				} else {
					priceFactor = isOutside ? -1 : 3;// strike < pxUl_ ? 2 : 4;
				}
			}
		}
		int timeFactor = days2Exp == null ? 5 : days2Exp > 180 ? 1 : days2Exp > 90 ? 2 : days2Exp > 30 ? 3 : days2Exp > 14 ? 4 : 5;
		// (-> bulksize=200)
		// 1,6,9 -> 575/390/200
		// 1,7,12 -> 750/430/180 ???
		// 1,7,11 -> 610/350/120
		// (-> bulksize=250)
		// 1,7,10 -> 485/280/100
		// (-> bulksize=300)
		// 1,6,9 -> 360/240/80
		// 1,7,9 -> 380/220/90
		// 1,6,8 -> 340/230/85
		// 1,6,7 -> 310/210/95
		int weightedFactor = (3 * timeFactor + 2 * priceFactor) / 5;
		if (weightedFactor >= 4)
			return 7;
		else if (weightedFactor > 2)
			return 6;
		else
			return 1;
	}

}
