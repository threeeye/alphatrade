package com.isoplane.at.tradier.model;

import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATMarketData;

public class ATTradierRequestMarketData implements IATMarketData, IATTradierMarketData {

	// private static final long serialVersionUID = 1L;

	transient public Map<String, Long> timestamps = new HashMap<>();
	// transient public Long timestamp;
	// NOTE: Internally uses 'symbol' to compare tradier streamed data
	transient public String atId;
	public String type;

	private String symbol;

	// trade
	public Double last;
	public Long last_volume;
	public Long volume;
	public Long trade_date;

	// quote
	public Double bid;
	public Long bidsize;
	public Long bid_date;
	public Double ask;
	public Long asksize;
	public Long ask_date;

	// summary
	public Double open;
	public Double high;
	public Double low;
	public Double prevclose;
	public Double close;

	public Long openInterest;

	private Double strike;
	private String right;
	private Double px_change;

	// NOTE: ignored
	// public String exch;
	// public String askexch;
	// public String bidexch;
	// public Double price;

	@Override
	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public boolean isSignificantChange(Double ratio_, ATTradierRequestMarketData previous_) {
		if (type == null)
			return false;
		switch (type) {
		case "quote":
			return isSignificantAskBidPriceChange(ratio_, previous_);
		case "trade":
			return isSignificantTradePriceChange(ratio_, previous_);
		case "summary":
			return isSignificantHighLowOpenClosePriceChange(ratio_, previous_);
		default:
			return true;
		}
	}

	private boolean isSignificantTradePriceChange(Double ratio_, ATTradierRequestMarketData previous_) {
		if (last == null)
			return false;
		if (previous_ == null || previous_.last == null)
			return true;
		Double diff = Math.abs((last - previous_.last) / last);
		return diff >= ratio_;
	}

	private boolean isSignificantAskBidPriceChange(Double ratio_, ATTradierRequestMarketData previous_) {
		if ((ask == null) || (bid == null))
			return false;
		if ((previous_ == null) || (previous_.ask == null) || (previous_.bid == null))
			return true;
		Double diff1 = Math.abs((ask - previous_.ask) / ask);
		if (diff1 >= ratio_)
			return true;
		Double diff2 = Math.abs((bid - previous_.bid) / bid);
		return diff2 >= ratio_;
	}

	private boolean isSignificantHighLowOpenClosePriceChange(Double ratio_, ATTradierRequestMarketData previous_) {
		if (previous_ == null)
			return true;
		if ((high != null) && (low != null)) {
			if ((previous_.high == null) || (previous_.low == null))
				return true;
			Double diff1 = Math.abs((high - previous_.high) / high);
			if (diff1 >= ratio_)
				return true;
			Double diff2 = Math.abs((low - previous_.low) / low);
			if (diff2 >= ratio_)
				return true;
		}
		if ((open != null && (close != null))) {
			if ((previous_.open == null) || (previous_.close == null))
				return true;
			Double diff1 = Math.abs((open - previous_.open) / open);
			if (diff1 >= ratio_)
				return true;
			Double diff2 = Math.abs((close - previous_.close) / close);
			if (diff2 >= ratio_)
				return true;
		}
		return false;

	}

	public void merge(ATTradierRequestMarketData other_) {
		if (other_ == null || other_.type == null)
			return;
		if (other_.atId != null)
			atId = other_.atId;
		// if (other_.date == null)
		// other_.date = 0L;
		type = other_.type;
		switch (other_.type) {
		case "quote":
			boolean isNewAsk = ask_date == null || (other_.ask_date != null && other_.ask_date > ask_date);
			boolean isNewBid = bid_date == null || (other_.bid_date != null && other_.bid_date > bid_date);
			if (isNewAsk && other_.ask != null && !other_.ask.isNaN())
				ask = other_.ask;
			if (isNewAsk && other_.asksize != null && other_.asksize != 0)
				asksize = other_.asksize;
			// if (other_.askexch != null)
			// askexch = other_.askexch;
			if (isNewAsk)
				ask_date = other_.ask_date;
			if (isNewBid && other_.bid != null && !other_.bid.isNaN())
				bid = other_.bid;
			if (isNewBid && other_.bidsize != null && other_.bidsize != 0)
				bidsize = other_.bidsize;
			// if (other_.bidexch != null)
			// bidexch = other_.bidexch;
			if (isNewBid)
				bid_date = other_.bid_date;
			// break;
		case "trade":
		case "tradex":
			// if (other_.exch != null)
			// exch = other_.exch;
			// if (other_.price != null)
			// price = other_.price;
			boolean isNewLast = trade_date == null || (other_.trade_date != null && other_.trade_date > trade_date);
			if (isNewLast && other_.last != null && !other_.last.isNaN())
				last = other_.last;
			if (isNewLast && other_.last_volume != null && other_.last_volume != 0)
				last_volume = other_.last_volume;
			if (isNewLast && other_.volume != null && other_.volume != 0)
				volume = other_.volume;
			if (isNewLast && other_.trade_date != null && other_.trade_date != 0)
				trade_date = other_.trade_date;
			if (isNewLast) {
				px_change = IATMarketData.calculatePxChange(this);
			}
			break;
		case "summary":
			if (other_.open != null && !other_.open.isNaN())
				open = other_.open;
			if (other_.high != null && !other_.high.isNaN())
				high = other_.high;
			if (other_.low != null && !other_.low.isNaN())
				low = other_.low;
			if (other_.prevclose != null && !other_.prevclose.isNaN())
				prevclose = other_.prevclose;
			if (other_.close != null && !other_.close.isNaN())
				close = other_.close;
			break;
		default:
			return;
		}
	}

	public void calculatePxChange() {
		px_change = IATMarketData.calculatePxChange(this);
	}

	@Override
	public String getAtId() {
		return atId;
	}

	@Override
	public void setAtId(String value) {
		this.atId = value;
		// if (value != null && value.length() > 10) {
		// right = value.substring(12, 13);
		// String s = value.substring(13);
		// Integer is = Integer.parseInt(s);
		// strike = is/1000.0;
		// }
	}

	@Override
	public Double getPxLast() {
		return last;
	}

	@Override
	public Integer getSzLast() {
		return last_volume != null ? last_volume.intValue() : null;
	}

	@Override
	public Long getTsLast() {
		return trade_date;
	}

	@Override
	public Double getPxAsk() {
		return ask;
	}

	@Override
	public Long getTsAsk() {
		return ask_date;
	}

	@Override
	public Integer getSzAsk() {
		return asksize != null ? asksize.intValue() : null;
	}

	@Override
	public Double getPxBid() {
		return bid;
	}

	@Override
	public Long getTsBid() {
		return bid_date;
	}

	@Override
	public Integer getSzBid() {
		return bidsize!=null?bidsize.intValue():null;
	}

	@Override
	public Long getDailyVol() {
		return volume;
	}

	@Override
	public Double getPxOpen() {
		return open;
	}

	@Override
	public Double getPxClose() {
		return close;
	}

	@Override
	public Double getPxClosePrev() {
		return prevclose;
	}

	@Override
	public Double getPxHigh() {
		return high;
	}

	@Override
	public Double getPxLow() {
		return low;
	}

	@Override
	public Long getOpenInterest() {
		return openInterest;
	}

	@Override
	public Double getPxStrike() {
		return strike;
	}

	@Override
	public Double getPxChange() {
		return px_change;
	}

	@Override
	public String getOptionRight() {
		return right;
	}

	@Override
	public <T> T getMeta(String key_) {
		return null;
	}

	@Override
	public Map<String, Object> getAsMap() {
		Map<String, Object> result = new HashMap<>();
		IATMarketData.setDailyVol(result, getDailyVol());
		IATMarketData.setPxAsk(result, getPxAsk());
		IATMarketData.setPxBid(result, getPxBid());
		IATMarketData.setPxLast(result, getPxLast());
		IATAsset.setSzAsk(result, getSzAsk());
		IATAsset.setSzBid(result, getSzBid());
		IATAsset.setSzLast(result, getSzLast());
		IATMarketData.setTsAsk(result, getTsAsk());
		IATMarketData.setTsBid(result, getTsBid());
		IATMarketData.setTsLast(result, getTsLast());
		IATMarketData.setPxOpen(result, getPxOpen());
		IATMarketData.setPxClose(result, getPxClose());
		IATMarketData.setPxClosePrev(result, getPxClosePrev());
		IATMarketData.setPxHigh(result, getPxHigh());
		IATMarketData.setPxLow(result, getPxLow());
		IATMarketData.setOpenInterest(result, getOpenInterest());
		IATAsset.setPxStrike(result, getPxStrike());
		IATMarketData.setOptionRight(result, getOptionRight());
		IATMarketData.setPxChange(result, getPxChange());
		return result;
	}

	@Override
	public String getUnderlying() {
		// TODO Auto-generated method stub
		return null;
	}

}
