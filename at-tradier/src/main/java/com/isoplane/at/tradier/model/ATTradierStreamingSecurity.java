package com.isoplane.at.tradier.model;

import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.TreeMap;

import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATFormats;

public class ATTradierStreamingSecurity {

	public String ticker;
	public String json;
	public Map<String, String> optionMap;

//	public ATTradierStreamingSecurity(String ticker_) {
//		ticker = ticker_;
//		optionMap = new TreeMap<>();
//	}

	public ATTradierStreamingSecurity(IATSymbol symbol_) {
		ticker = symbol_.convert("tradier");
		optionMap = new TreeMap<>();
		if (symbol_.getOptions() == null)
			return;
		SimpleDateFormat dateFormat = ATFormats.DATE_yyMMdd.get();
		symbol_.getOptions().forEach((date_, chain_) -> {
			if (chain_.getStrikes() == null)
				return;
			String dateStr = dateFormat.format(date_);
			chain_.getStrikes().forEach(strike_ -> {
				long strikeL = (long) (strike_ * 1000);
				String symbolPre = String.format("%s%s%%s%08d", ticker, dateStr, strikeL);
				String symC = String.format(symbolPre, "C");
				String symP = String.format(symbolPre, "P");
				optionMap.put(symC, null);
				optionMap.put(symP, null);
			});
		});
	}

}
