package com.isoplane.at.tradier;

import java.net.InetAddress;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.apache.commons.collections.buffer.CircularFifoBuffer;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATOptionChain;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATMarketDataSubscriber;
import com.isoplane.at.commons.service.IATMarketStatusService;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSymbolListener;
import com.isoplane.at.commons.service.IATTotalPositionListener;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATPriorityQueue;
import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.marketdata.ATMarketDataManager;
import com.isoplane.at.mongodb.ATMongoStatusService;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.symbols.ATSymbolManager;
import com.isoplane.at.tradier.model.ATSymbolRequestToken;
import com.isoplane.at.tradier.model.IATTradierMarketData;
import com.isoplane.at.tradier.util.ATTradierConnector;
import com.isoplane.at.tradier.util.ATTradierRequestProcessor;
import com.isoplane.at.tradier.util.ATTradierStreamProcessor;
import com.isoplane.at.tradier.util.ATTradierUtil;

public class ATTradierManager
		implements IATSymbolListener, IATTotalPositionListener, IATMarketDataSubscriber, IATProtoMarketSubscriber {

	// static final String INSTANCE_NAME = ATTradierManager.class.getSimpleName();
	static final Logger log = LoggerFactory.getLogger(ATTradierManager.class);

	private static ATTradierManager _instance;

	static private boolean IS_KAFKA = true;
	private ATKafkaMarketDataProducer _kafkaMarketDataProducer;
	private ATKafkaMarketDataPackProducer _kafkaMarketDataPackProducer;
	// private ATKafkaUserSessionMonitor _kafkaConsumer;

	private static boolean _isStreamPositions;
	private static boolean _isStreamSymbols;
	private static boolean _isRequestSymbol;
	// private ATTradierConnector _connector;
	private ATTradierConnector _connector;
	// private Configuration _config;
	private boolean _isRunning;
	private CircularFifoBuffer _tradierCircle;
	private Set<String> _tradierPositions;
	private ATPriorityQueue<ATSymbolRequestToken> _standardSymbolQueue;
	private ATPriorityQueue<ATSymbolRequestToken> _dynamicSymbolQueue;
	private int _dynQuotaCount = 0;
	private ATMarketDataManager _marketDataManager;
	private ATSymbolManager _symbolsManager;
	private ATPortfolioManager _portfolioManager;
	private final ReentrantLock _symbolLock = new ReentrantLock();
	private final CountDownLatch _serviceLatch;// = new CountDownLatch(2);
	private Map<String, String> _reverseSymbolMap;
	private long _periodicCycle = 0;
	private long _periodicReportTimestamp = System.currentTimeMillis();
	private long _periodicMarketDataCount = 0;
	private long _periodicMarketDataRate = 0;
	private long _monitorReportTimestamp = System.currentTimeMillis();
	private long _monitorMarketDataCount = 0;
	private EATMarketStatus _marketStatus;// = EATMarketStatus.OPEN;
	// private long _throughputLogInterval;
	private ATSymbolRefresher _symbolRefresher;

	static public void main(String[] args_) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			
//			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
//			Configuration config = ATConfigUtil.init(configPath, true);

			InetAddress ip = InetAddress.getLocalHost();
			final String ipStr = ip.getHostAddress();
			ATSysLogger.setLoggerClass(String.format("TradierAgent.[%s]", ipStr));

			// String[] propertiesPaths = args_[0].split(",");
			// log.info(String.format("Reading properties: %s", Arrays.asList(propertiesPaths)));
			// CompositeConfiguration cconfig = new CompositeConfiguration();
			// for (String path : propertiesPaths) {
			// log.info(String.format("Reading properties [%s]", path));
			// PropertiesConfiguration config = new Configurations().properties(new File(path));
			// cconfig.addConfigurationFirst(config);
			// }
			// Configuration config = ATConfigUtil.init(args_[0], true);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					_instance.shutdown();
					ATExecutors.shutdown();
				}
			}));

			ATExecutors.init();
			// ATEhCache.init(config);
			ATMongoTableRegistry.init();
			if (IS_KAFKA) {
				ATKafkaRegistry.register("trd_mgr",
						ATKafkaMarketDataProducer.class,
						ATKafkaMarketDataPackProducer.class);
			}

			IATMarketStatusService statusSvc = new ATMongoStatusService();
			ATServiceRegistry.setStatusService(statusSvc, false);

			_isStreamPositions = config.getBoolean("tradier.stream.position", false);
			_isStreamSymbols = config.getBoolean("tradier.stream.symbol", false);
			_isRequestSymbol = config.getBoolean("tradier.request.symbol", false);

			_instance = new ATTradierManager();
			_instance.init();

			String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
			log.info(msg);
			ATSysLogger.info(String.format("%s [%s] started", _instance.getClass().getSimpleName(), ipStr));

			_instance.run();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", _instance.getClass().getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATTradierManager() {
		// _config = config_;
		// Configuration config = ATConfigUtil.config();
		int latchCount = (_isStreamPositions ? 1 : 0) + (_isStreamSymbols ? 1 : 0) + (_isRequestSymbol ? 1 : 0);
		_serviceLatch = new CountDownLatch(latchCount);
		// _throughputLogInterval = _config.getLong("tradier.stream.interval.report", 60000);
		_reverseSymbolMap = new HashMap<>();
		_standardSymbolQueue = new ATPriorityQueue<>();
		_dynamicSymbolQueue = new ATPriorityQueue<>();
		_symbolRefresher = new ATSymbolRefresher();
		_marketDataManager = new ATMarketDataManager(false);
		_symbolsManager = new ATSymbolManager(true);
		_symbolsManager.register(this);
		_portfolioManager = new ATPortfolioManager(true);
		_portfolioManager.register(this);
	}

	private void init() {
		try {
			Configuration config = ATConfigUtil.config();
			_connector = new ATTradierConnector(config);
			_connector.initAsyncClient();
			_connector.subscribe(this);
			_marketDataManager.initService();
			_marketStatus = _marketDataManager.getMarketStatus();
			_symbolsManager.initService();

			if (IS_KAFKA) {
				ATKafkaRegistry.start();
				_kafkaMarketDataProducer = ATKafkaRegistry.get(ATKafkaMarketDataProducer.class);
				_kafkaMarketDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);
				// _kafkaConsumer = ATKafkaRegistry.get(ATKafkaUserSessionMonitor.class);
			}

			reloadSymbols();
			LocalTime refreshTime = LocalTime.parse(config.getString("tradier.refreshTime"), DateTimeFormatter.ofPattern("HH:mm:ss"));
			long dur = ChronoUnit.MILLIS.between(LocalTime.now(), refreshTime);
			if (Math.signum(dur) < 0) {
				dur += (1000 * 60 * 60 * 24);
			}
			ATExecutors.scheduleAtFixedRate("symbol_reload", () -> reloadSymbols(), dur, 1000 * 60 * 60 * 24);

			_portfolioManager.initService();
			_tradierPositions = new TreeSet<>();
			_portfolioManager.getActiveOccIds(null, true).forEach(id_ -> {
				notify(EATChangeOperation.ADD, id_);
				String underlyingId = ATModelUtil.getUnderlyingOccId(id_);
				if (underlyingId != null) {
					notify(EATChangeOperation.ADD, underlyingId);
				}
			});
			_symbolsManager.getSymbols().forEach((id_, s_) -> {
				notify(EATChangeOperation.ADD, id_);
			});
			_periodicCycle = config.getLong("tradier.stream.interval.report");
			ATExecutors.scheduleAtFixedRate("throughput", new Runnable() {

				@Override
				public void run() {
					long deltaTs = System.currentTimeMillis() - _periodicReportTimestamp;
					_periodicMarketDataRate = (long) Math.ceil(_periodicMarketDataCount * 1000.0 / deltaTs);
					_periodicReportTimestamp = System.currentTimeMillis();
					_periodicMarketDataCount = 0;
					log.info(String.format("Effective throughput (1min/%s) [%d/s]", _marketStatus, _periodicMarketDataRate));
				}
			}, _periodicCycle, _periodicCycle);
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	private void run() {
		_isRunning = true;

		String threadId = String.format("%s.marketStatus", getClass().getSimpleName());
		ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {
			@Override
			public void run() {
				Thread.currentThread().setName(threadId);
				updateMarketStatus();
			}
		}, 1000, 60000);
		Configuration config = ATConfigUtil.config();
		log.info(String.format("Position streaming [%b]", _isStreamPositions));
		if (_isStreamPositions) {
			String positionDataTypes = config.getString("tradier.stream.position.data");
			Long positionStreamTimeout = config.getLong("tradier.stream.position.timeout", -1);
			ATTradierStreamProcessor positionListener = new ATTradierStreamProcessor(config, this, positionStreamTimeout, "positions");
			streamPositions(_tradierPositions, positionListener, positionDataTypes);
		}

		log.info(String.format("Symbol   streaming [%b]", _isStreamSymbols));
		if (_isStreamSymbols) {
			String symbolDataTypes = config.getString("tradier.stream.symbol.data");
			Long symbolStreamTimeout = config.getLong("tradier.stream.symbol.timeout", -1);
			long symbolBulkThreshold = config.getLong("tradier.stream.symbol.threshold");
			ATTradierStreamProcessor symbolsListener = new ATTradierStreamProcessor(config, this, symbolStreamTimeout, "symbol",
					symbolBulkThreshold);
			streamSymbols(symbolsListener, symbolDataTypes);
		}

		log.info(String.format("Symbol   quotes    [%b]", _isRequestSymbol));
		if (_isRequestSymbol) {
			ATTradierRequestProcessor requestListener = new ATTradierRequestProcessor(config);
			requestSymbols(requestListener);
		}
	}

	private Future<?> requestSymbols(ATTradierRequestProcessor listener_) {
		Set<String> requestedSymbols = new HashSet<>();
		Future<?> future = ATExecutors.schedule("tradier-md", new Runnable() {

			@Override
			public void run() {
				while (_isRunning) {
					try {
						Configuration config = ATConfigUtil.config();
						int maxQuotaCount = config.getInt("tradier.dynamic.quota");
						ATPriorityQueue<ATSymbolRequestToken> whichQueue = _standardSymbolQueue;
						if (_dynamicSymbolQueue.size() > 0 && _dynQuotaCount++ > maxQuotaCount) {
							whichQueue = _dynamicSymbolQueue;
							_dynQuotaCount = 0;
						}
						ATPriorityQueue<ATSymbolRequestToken> queue = whichQueue;
						boolean ignoreMarketHours = config.getBoolean("tradier.ignore.hours");
						if (!ignoreMarketHours && EATMarketStatus.OPEN != _marketStatus) {
							long snooze = config.getInt("tradier.pause.marketClosed");
							log.info(String.format("Market [%s]. Snoozing [%ds]...", _marketStatus, snooze / 1000));
							Thread.sleep(snooze);
							continue;
						}
						_symbolLock.lock();
						long reqPeriod = config.getLong("tradier.request.period");
						long end = System.currentTimeMillis() + reqPeriod;
						int bulkSize = config.getInt("tradier.request.bulksize");
						Collection<ATSymbolRequestToken> tokens = queue.poll(bulkSize);
						if (tokens == null || tokens.isEmpty()) {
							log.warn(String.format("No symbols to quote. Throttling..."));
							// _symbolLock.unlock();
							Thread.sleep(5000);
							continue;
						}
						// log.debug(String.format("Symbols: %s", tokens.size()));
						Set<String> tradierIds = new HashSet<>();
						Set<String> underlyingIds = new TreeSet<>();
						tokens.forEach(t_ -> {
							tradierIds.add(t_.tradierId);
							underlyingIds.add(t_.underlyingOccId);
						});
						if (log.isDebugEnabled()) {
							requestedSymbols.addAll(tradierIds);
							log.debug(String.format("Requesting [%d/%d] %s", underlyingIds.size(), requestedSymbols.size(),
									log.isTraceEnabled() ? underlyingIds : ""));
						}
						Collection<String> unmatched = _connector.requestQuotes(tradierIds, ATTradierManager.this);
						if (unmatched != null && !unmatched.isEmpty()) {
							log.info(String.format("Removing unmatched: %s", unmatched));
						}
						tokens.forEach(t_ -> {
							if (unmatched != null && unmatched.contains(t_.tradierId)) {
								requestedSymbols.remove(t_.tradierId);
								return;
							}
							t_.lastProcessedTime = System.currentTimeMillis();
							IATMarketData md = _marketDataManager.getMarketData(t_.underlyingOccId);
							t_.priority = t_.getPriority(md);
							queue.add(t_, t_.priority);
						});
						long remaining = end - System.currentTimeMillis();
						if (remaining > 0) {
							Thread.sleep(remaining);
						}
					} catch (Exception ex) {
						log.error(String.format("Request error"), ex);
					} finally {
						_symbolLock.unlock();
					}
				}
				_serviceLatch.countDown();
			}
		}, 5000);
		return future;
	}

	private Future<?> streamPositions(Collection<String> positions_, ATTradierStreamProcessor listener_, String params_) {
		String threadId = String.format("%s.stream.positions", getClass().getSimpleName());
		Future<?> future = ATExecutors.submit(threadId, new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(threadId);
				do {
					try {
						Configuration config = ATConfigUtil.config();
						_connector.streamSymbols(threadId, positions_, listener_, params_);
						long pause = config.getInt("tradier.stream.break.open");
						Thread.sleep(pause);
					} catch (Exception ex) {
						log.error(String.format("Error processing stream [%s]", threadId), ex);
					}
				} while (_isRunning);
				_serviceLatch.countDown();
			}
		});
		log.debug(String.format("Initialized stream [%s]", threadId));
		return future;
	}

	private Future<?> streamSymbols(ATTradierStreamProcessor listener_, String params_) {
		String threadId = String.format("%s.stream.symbols", this.getClass().getSimpleName());
		Future<?> future = ATExecutors.submit(threadId, new Runnable() {

			@Override
			public void run() {
				Thread.currentThread().setName(threadId);
				do {
					try {
						Configuration config = ATConfigUtil.config();
						boolean ignoreMarketHours = config.getBoolean("tradier.ignore.hours");
						if (EATMarketStatus.CLOSED == _marketStatus && !ignoreMarketHours) {
							long snooze = config.getInt("tradier.pause.marketClosed");
							log.info(String.format("Market closed. Snoozing [%ds]...", snooze / 1000));
							Thread.sleep(snooze);
							continue;
						}
						int bulkSize = config.getInt("tradier.stream.symbol.bulkSize");
						Set<String> symbolSet = new TreeSet<>();
						_symbolLock.lock();
						try {
							for (int i = 0; i < bulkSize; i++) {
								String symbol = (String) _tradierCircle.remove();
								_tradierCircle.add(symbol);
								if (EATMarketStatus.OPEN == _marketStatus || ignoreMarketHours) {
									symbolSet.add(symbol);
								} else if (symbol.length() < 9) {
									symbolSet.add(symbol);
								}
							}
						} finally {
							_symbolLock.unlock();
						}
						_connector.streamSymbols(threadId, symbolSet, listener_, params_);
						long pause = config.getInt("tradier.stream.break.open");
						if (EATMarketStatus.OPEN != _marketStatus)
							pause *= 100;
						Thread.sleep(pause);
					} catch (Exception ex) {
						try {
							long pause = 30000;
							log.error(String.format("Error streaming [%s]. Retrying in [%ds]...", threadId, pause / 1000), ex);
							Thread.sleep(pause);
						} catch (InterruptedException exx) {
							log.error(String.format("Error restarting stream [%s]. Terminating", threadId), exx);
							_isRunning = false;
						}
					}
				} while (_isRunning);
				_serviceLatch.countDown();
			}
		});
		return future;
	}

	private void reloadSymbols() {
		Configuration config = ATConfigUtil.config();
		int maxDays = config.getInt("tradier.stream.maxdays");
		Date maxDate = DateUtils.addDays(new Date(), maxDays);
		Set<String> tradierSymbols = new TreeSet<>();
		Map<String, String> reverseSymbMap = new HashMap<>();
		Map<String, ATSymbol> symbolMap = _symbolsManager.getSymbols();
		List<ATSymbolRequestToken> symbolList = new ArrayList<>();
		for (ATSymbol symbol : symbolMap.values()) {
			String occId = symbol.getId();
			log.debug(String.format("Loading and preparing [%s]", occId));
			String tradierId = symbol.convert(IATSymbol.TRADIER);
			if (!occId.equals(tradierId)) {
				reverseSymbMap.put(tradierId, occId);
			}
			IATMarketData marketData = _marketDataManager.getMarketData(occId);
			tradierSymbols.add(tradierId);
			if (!_isStreamSymbols) {
				ATSymbolRequestToken token = new ATSymbolRequestToken();
				token.tradierId = tradierId;
				token.underlyingOccId = occId;
				token.priority = 2;
				symbolList.add(token);
			}
			Map<Date, ATOptionChain> expirationMap = symbol.getOptions();
			if (expirationMap == null)
				continue;
			for (Entry<Date, ATOptionChain> entry : expirationMap.entrySet()) {
				Date expDate = entry.getKey();
				if (expDate.after(maxDate))
					continue;
				Set<Double> strikes = entry.getValue().getStrikes();
				if (strikes == null)
					continue;
				int days2Exp = (int) TimeUnit.DAYS.convert(expDate.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS) + 1;
				for (Double strike : strikes) {
					String tradierOptionC = ATTradierUtil.toTradierOptionSymbol(tradierId, expDate, "C", strike);
					String tradierOptionP = ATTradierUtil.toTradierOptionSymbol(tradierId, expDate, "P", strike);
					tradierSymbols.add(tradierOptionC);
					tradierSymbols.add(tradierOptionP);

					ATSymbolRequestToken cToken = new ATSymbolRequestToken();
					cToken.days2Exp = days2Exp;
					cToken.strike = strike;
					cToken.tradierId = tradierOptionC;
					cToken.underlyingOccId = occId;
					cToken.isPut = false;
					cToken.priority = cToken.getPriority(marketData);
					ATSymbolRequestToken pToken = new ATSymbolRequestToken();
					pToken.days2Exp = days2Exp;
					pToken.strike = strike;
					pToken.tradierId = tradierOptionP;
					pToken.underlyingOccId = occId;
					pToken.isPut = true;
					pToken.priority = pToken.getPriority(marketData);
					symbolList.add(cToken);
					symbolList.add(pToken);
				}
			}
		}
		_reverseSymbolMap = reverseSymbMap;
		log.debug(String.format("Custom [%s] symbols [%d]", IATSymbol.TRADIER, reverseSymbMap.size()));
		_symbolLock.lock();
		log.info(String.format("Locking symbols for refresh..."));
		Map<Integer, List<ATSymbolRequestToken>> tokenMap = symbolList.stream().collect(Collectors.groupingBy(s -> s.priority));
		_standardSymbolQueue.clear();
		for (Entry<Integer, List<ATSymbolRequestToken>> entry : tokenMap.entrySet()) {
			_standardSymbolQueue.addAll(entry.getValue(), entry.getKey());
		}
		log.info(String.format("Loaded [%d] symbols: %s", symbolList.size(), _standardSymbolQueue.getStatus()));
		try {
			if (_isStreamSymbols) {
				Map<String, Object> status = _standardSymbolQueue.getStatus();
				log.info(String.format("Loaded [%d:%d] symbols", tradierSymbols.size(), status.get("total")));
				_tradierCircle = new CircularFifoBuffer(tradierSymbols);
				log.info(String.format("Loaded [%d] tickers for pricing", _tradierCircle.size()));
			}
		} finally {
			_symbolLock.unlock();
			log.info(String.format("Symbols unlocked"));
		}
	}

	// Pre-market trading is 4:00-9:30. After-hours trading is 16:00-20:00.
	private void updateMarketStatus() {
		EATMarketStatus status = _connector.getMarketStatus();
		if (_marketStatus == null || !_marketStatus.equals(status)) {
			notify(status);
		}
	}

	@Override
	public void notify(EATChangeOperation operation_, ATSymbol symbol_) {
		// log.info(String.format("Reloading [%s]", symbol_));
		_symbolRefresher.schedule();
		// reloadSymbols(); // Note: Expensive but should rarely occur (actually occurs nightly...)
	}

	@Override
	public void notifyDynamicSymbols(Map<String, ATSymbol> symbolMap_, Map<String, Set<String>> ignore) {
		if (symbolMap_ == null || symbolMap_.isEmpty())
			return;
		Map<String, String> reverseSymbMap = new HashMap<>();
		List<ATSymbolRequestToken> symbolList = new ArrayList<>();
		for (ATSymbol symbol : symbolMap_.values()) {
			String occId = symbol.getId();
			log.debug(String.format("Loading and preparing [%s]", occId));
			String tradierId = symbol.convert(IATSymbol.TRADIER);
			if (!occId.equals(tradierId)) {
				reverseSymbMap.put(tradierId, occId);
			}
			ATSymbolRequestToken token = new ATSymbolRequestToken();
			token.tradierId = tradierId;
			token.underlyingOccId = occId;
			token.priority = 2;
			symbolList.add(token);
			// IATMarketData marketData = _marketDataManager.getMarketData(occId);
			// tradierSymbols.add(tradierId);
			Map<Date, ATOptionChain> expirationMap = symbol.getOptions();
			if (expirationMap == null)
				continue;
			IATMarketData marketData = _marketDataManager.getMarketData(occId);
			for (Entry<Date, ATOptionChain> entry : expirationMap.entrySet()) {
				Date expDate = entry.getKey();
				Set<Double> strikes = entry.getValue().getStrikes();
				if (strikes == null)
					continue;
				int days2Exp = (int) TimeUnit.DAYS.convert(expDate.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS) + 1;
				for (Double strike : strikes) {
					String tradierOptionC = ATTradierUtil.toTradierOptionSymbol(tradierId, expDate, "C", strike);
					String tradierOptionP = ATTradierUtil.toTradierOptionSymbol(tradierId, expDate, "P", strike);
					// tradierSymbols.add(tradierOptionC);
					// tradierSymbols.add(tradierOptionP);

					ATSymbolRequestToken cToken = new ATSymbolRequestToken();
					cToken.days2Exp = days2Exp;
					cToken.strike = strike;
					cToken.tradierId = tradierOptionC;
					cToken.underlyingOccId = occId;
					cToken.isPut = false;
					cToken.priority = cToken.getPriority(marketData);
					ATSymbolRequestToken pToken = new ATSymbolRequestToken();
					pToken.days2Exp = days2Exp;
					pToken.strike = strike;
					pToken.tradierId = tradierOptionP;
					pToken.underlyingOccId = occId;
					pToken.isPut = true;
					pToken.priority = pToken.getPriority(marketData);
					symbolList.add(cToken);
					symbolList.add(pToken);
				}
			}
		}
		_reverseSymbolMap.putAll(reverseSymbMap);

		log.debug(String.format("Dynamic custom [%s] symbols [%d]", IATSymbol.TRADIER, reverseSymbMap.size()));
		_symbolLock.lock();
		log.info(String.format("Locking for dynamic symbols..."));
		Map<Integer, List<ATSymbolRequestToken>> tokenMap = symbolList.stream().collect(Collectors.groupingBy(s -> s.priority));
		_dynamicSymbolQueue.clear();
		for (Entry<Integer, List<ATSymbolRequestToken>> entry : tokenMap.entrySet()) {
			_dynamicSymbolQueue.addAll(entry.getValue(), entry.getKey());
		}
		log.info(String.format("Loaded [%d] dynamic symbols: %s", symbolList.size(), _dynamicSymbolQueue.getStatus()));
		_symbolLock.unlock();
		log.info(String.format("Dynamic symbols unlocked"));
	}

	@Override
	public void notify(EATChangeOperation operation_, String occId_) {
		String tradierId = ATTradierUtil.toTradierSymbol(occId_, _symbolsManager);
		if (tradierId == null) {
			log.warn(String.format("Unavailable symbol [%s/%s]", occId_, IATSymbol.TRADIER));
			return;
		}
		switch (operation_) {
		case ADD:
			_tradierPositions.add(tradierId);
			break;
		case DELETE:
			if (occId_.length() > 6) { // We only remove options
				_tradierPositions.remove(tradierId);
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper wrapper_) {
		_kafkaMarketDataPackProducer.send(wrapper_);
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
		_kafkaMarketDataProducer.send(wrappers_);
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper wrapper_) {
		// TODO Auto-generated method stub
	}

	@Override
	public void notify(IATMarketData data_) {
		_periodicMarketDataCount++;
		_monitorMarketDataCount++;
		IATTradierMarketData data = (IATTradierMarketData) data_;
		if (data.getAtId() == null) {
			String tradierId = data.getSymbol();
			String atId = ATTradierUtil.tradier2OccId(tradierId, _reverseSymbolMap);
			data.setAtId(atId);
			// if ("NDAQ".equals(atId) || "GOOGL".equals(atId) || "GE".equals(atId) || "AAPL".equals(atId)) {
			// log.debug(atId);
			// }
		}
		if (log.isTraceEnabled())
			log.trace(String.format("Market: %s", data_.getAtId()));
		_marketDataManager.update(data);
		if (IS_KAFKA) {
			_kafkaMarketDataProducer.send(data);
		}
	}

	@Override
	public void notify(EATMarketStatus status_) {
		if (status_ == null)
			return;
		EATMarketStatus oldStatus = _marketStatus;
		_marketStatus = status_;
		_marketDataManager.update(_marketStatus);
		if (log.isDebugEnabled())
			log.debug(String.format("Market status changed [%s -> %s]", oldStatus, _marketStatus));
	}

	public void shutdown() {
		log.debug(String.format("Shutting down"));
		if (_symbolsManager != null)
			_symbolsManager.shutdown();
		if (_connector != null)
			_connector.shutdown();
	}

	public class ATSymbolRefresher {

		private ScheduledFuture<?> _existingFuture;

		public void schedule() {
			if (_existingFuture != null) {
				_existingFuture.cancel(false);
				_existingFuture = null;
			}
			String threadId = "refresh_symbols(update)";
			ScheduledFuture<?> newFuture = ATExecutors.schedule(threadId, new Runnable() {

				@Override
				public void run() {
					Thread.currentThread().setName(threadId);
					log.info(String.format("ATSymbolRefresher.run()"));
					reloadSymbols();
					_existingFuture = null;
				}
			}, 2000);
			_existingFuture = newFuture;
		}

	}

	private Long getThoughputRate() {
		long deltaTs = System.currentTimeMillis() - _monitorReportTimestamp;
		long ratio = (long) Math.ceil(_monitorMarketDataCount * 1000.0 / deltaTs);
		return ratio;
	}

	private String getThroughputDuration() {
		Duration duration = Duration.ofMillis(System.currentTimeMillis() - _monitorReportTimestamp);
		return duration.toString();
	}

	private String getThoughputStart() {
		Date date = new Date(_monitorReportTimestamp);
		String str = String.format("%s %s", ATFormats.DATE_yyyy_MM_dd.get().format(date), ATFormats.TIME_HH_mm_ss.get().format(date));
		return str;
	}

	public Map<String, String> getMonitoringStatus() {
		Configuration config = ATConfigUtil.config();
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("throughputDuration", getThroughputDuration());
		statusMap.put("throughputStart", getThoughputStart());
		statusMap.put("throughputRate", getThoughputRate() + "");
		statusMap.put("throughputCount", _monitorMarketDataCount + "");
		statusMap.put(String.format("throughputRate(%ds)", (long) (_periodicCycle / 1000)), _periodicMarketDataRate + "");
		statusMap.put("marketStatus", _marketStatus.name());
		statusMap.put("request.symbols", _isRequestSymbol + "");
		statusMap.put("request.bulkSize", config.getInt("tradier.request.bulksize") + "");
		statusMap.put("request.period", config.getInt("tradier.request.period") + "");
		statusMap.put("stream.symbols", _isStreamSymbols + "");
		statusMap.put("stream.positions", _isStreamPositions + "");
		statusMap.put("positions.size", _tradierPositions.size() + "");
		for (Entry<String, Object> entry : _standardSymbolQueue.getStatus().entrySet()) {
			statusMap.put(String.format("queue.std.%s", entry.getKey()), entry.getValue() + "");
		}
		for (Entry<String, Object> entry : _dynamicSymbolQueue.getStatus().entrySet()) {
			statusMap.put(String.format("queue.dyn.%s", entry.getKey()), entry.getValue() + "");
		}
		return statusMap;
	}

}
