package com.isoplane.at.tradier;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.kafka.test.ATKafkaSimpleMessageConsumer;
import com.isoplane.at.kafka.test.ATKafkaSimpleMessageProducer;

public class ATTradierKafkaTest {

	static final Logger log = LoggerFactory.getLogger(ATTradierKafkaTest.class);

	private static CompositeConfiguration _config;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String testArgs = System.getProperty("test_args");
		String[] propertiesPaths = testArgs.split(",");
		_config = new CompositeConfiguration();
		for (String path : propertiesPaths) {
			log.info(String.format("Reading properties [%s]", path));
			PropertiesConfiguration config = new Configurations().properties(new File(path));
			_config.addConfiguration(config);
		}
	}

	@AfterAll
	public static void tearDown() {
		// _db.delete(TEST_TRADES_TABLE, ATPersistenceRegistry.query().empty());
	}

	@BeforeEach
	public void prepare() throws Exception {
	}

	@AfterEach
	public void cleanup() throws Exception {
	}

	// @Disabled
	@Test
	public void testSimpleMessage() throws InterruptedException {
		ATKafkaSimpleMessageProducer producer = new ATKafkaSimpleMessageProducer("test_producer", _config);
		ATKafkaSimpleMessageConsumer consumer = new ATKafkaSimpleMessageConsumer("test_consumer", _config);
		new Thread(consumer).start();

		final int COUNT = 30;
		for (int i = 0; i < COUNT; i++) {
			producer.send(String.format("Test %d", i));
			Thread.sleep(10);
		}

		Thread.sleep(1000);
		producer.stop();
		consumer.stop();
		int count = consumer.getCount();
		assertEquals(count, COUNT);
	}

	@Disabled
	@Test
	public void testMarketData() throws InterruptedException {
		ATKafkaRegistry.register("tradier_test", ATKafkaMarketDataProducer.class, ATKafkaMarketDataConsumer.class);
		ATKafkaRegistry.start();
		ATKafkaMarketDataProducer producer = ATKafkaRegistry.get(ATKafkaMarketDataProducer.class);
		ATKafkaMarketDataConsumer consumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);

		// ATTestKafkaUserSessionProducer producer = new ATTestKafkaUserSessionProducer("test_mkt_producer", _config);
		// ATKafkaUserSessionMonitor consumer = new ATKafkaUserSessionMonitor("test_mkt_consumer", _config);
		// new Thread(consumer).start();

		final int COUNT = 30;
		for (int i = 0; i < COUNT; i++) {
			ATMarketData mkt = new ATMarketData();
			mkt.setOccId(String.format("MKT_%d", i));
			producer.send(mkt);
			Thread.sleep(10);
		}

		producer.stop();
		consumer.stop();
		Thread.sleep(1000);
		// int count = consumer.getCount();
		// assertEquals(count, COUNT);

	}

}
