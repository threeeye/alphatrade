title AlphaTrade - Tradier Data Agent
SET dir=C:/Dev/workspace/alphatrade/at-tradier

C:\Dev\ProcessMonitor.exe -P C:\Dev\Java\jdk1.8.0_152\jre\bin\java -A "-Dlogback.configurationFile=/%dir%/tradier-logback.xml -jar %dir%/jar/at-tradier.jar %dir%/../NOCHECKIN-local.properties config/tradier" -T 60000 -K "OutOfMemoryError,UnknownHostException,Excessive timeouts,Error scheduling output" -D 5000 -L C:\data\logs\PM-tradier.log -S logs3.papertrailapp.com:50247:AT

EXIT
