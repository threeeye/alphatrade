package com.isoplane.at.symbols;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.isoplane.at.adapter.IATRussellAdapter;
import com.isoplane.at.adapter.kibot.ATKibotAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.symbols.util.ATSymbolMogodbTools;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class ATSymbolManagerTest {

	static final Logger log = LoggerFactory.getLogger(ATSymbolManagerTest.class);

	static private String _testTableId;
	static private ATMongoDao _db;
	static private ATSymbolManagerTestWrapper _symM;
	static LocalDate _date_20190601;

	static final String TEST_USER = "TEST_USER";

	@BeforeAll
	public static void setup() {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, true);

		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		ATExecutors.init();

		_db = new ATMongoDao();
		_symM = new ATSymbolManagerTestWrapper(false);
		_symM.initService();

		_date_20190601 = LocalDate.of(2019, 06, 01);
	}

	@AfterAll
	public static void tearDown() {
		if (_testTableId != null) {
			_db.delete(_testTableId, ATPersistenceRegistry.query().empty());
		}
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
		// _db.delete(_testTableId, ATPersistenceRegistry.query().empty());
	}

	@Test
	@Disabled
	public void test() {
	}

	@Disabled
	@Test
	public void testInsert() {
		String userId = "TEST";
		List<String> occIds = Arrays.asList("AAL", "AAL   200327C00021500", "AAL   200327C00022000");
		_symM.addDynamicSymbols(userId, occIds);

		int i = 1;
		int j = i;
		// assertTrue(resultData.size() == 1);
	}

	@Test
	public void testKibotRussell() {
		IATRussellAdapter r = new TestKibotAdapter();
		Map<String, ATSymbol> symbolMap = _symM.getSymbols("sp500");
		assertNotNull(symbolMap);
		assertTrue(!symbolMap.isEmpty());
	}

	@Test
	public void testRead() {
		_symM.getMongoDb().init();
	}

	public static class ATSymbolManagerTestWrapper extends ATSymbolManager {

		public ATSymbolManagerTestWrapper(boolean isStreaming_) {
			super(isStreaming_);
		}

		public ATSymbolMogodbTools getMongoDb() {
			return _mongodb;
		}

	}

	public static class TestKibotAdapter extends ATKibotAdapter {

		public Set<ATSymbol> parseSymbols(String tsv_) {
			return super.parseSymbols(tsv_);
		}
	}

}
