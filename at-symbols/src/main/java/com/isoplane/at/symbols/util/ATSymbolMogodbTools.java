package com.isoplane.at.symbols.util;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.or;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATOptionChain;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATOptionExpiration.ATOptionExpirationDate;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.symbols.model.ATMongoDynamicSymbol;
import com.isoplane.at.symbols.model.ATMongoSymbol;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATSymbolMogodbTools {

	static final Logger log = LoggerFactory.getLogger(ATSymbolMogodbTools.class);
	static final Long THROTTLE_DELAY = 1000L;
	static final Gson _gson = new Gson();

	static final SimpleDateFormat _df = ATFormats.DATE_yyyy_MM_dd.get();

	// private Configuration _config;
	boolean _isStreaming;
	private Map<String, ATSymbol> _symbolMap;
	private Map<String, ATSymbol> _readonlySymbolMap;
	private Set<String> _expirationStrings;
	private Set<String> _readonlyExpirationStrings;

	private ATSymbolDao _dao;
	//	private IATSymbolListener _listener;
	private String _standardSymbolTable;
	private String _dynamicSymbolTable;
	private String _optionExpTable;
	private String _symbolConverterTable;

	public ATSymbolMogodbTools(boolean isStreaming_ /*, IATSymbolListener listener_*/) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		// _config = config_;
		_isStreaming = isStreaming_;
		//	_listener = listener_;
		_symbolMap = new TreeMap<>();
		_readonlySymbolMap = Collections.unmodifiableMap(_symbolMap);
		_expirationStrings = new TreeSet<>();
		_readonlyExpirationStrings = Collections.unmodifiableSet(_expirationStrings);

		_standardSymbolTable = ATMongoTableRegistry.getSymbolTable();
		_symbolConverterTable = ATMongoTableRegistry.getSymbolConverterTable();
		_dynamicSymbolTable = ATMongoTableRegistry.getDynamicSymbolTable();
		_optionExpTable = ATMongoTableRegistry.getOptionExpiryTable();
	}

	public ATSymbolMogodbTools init() {
		_dao = new ATSymbolDao();

		readSymbolsSync();
		// ATExecutors.scheduleAtFixedRate("", () -> readSymbolsSync(), 0, ATMathUtil.DAY_IN_MS);

		if (_isStreaming) {
			//			monitorStandardSymbols();
			//			monitorOptionChain();
			//			monitorDynamicSymbols();
			log.error(String.format("#################################"));
			log.error(String.format("#                               #"));
			log.error(String.format("# REMOVE MONGO REACTIVE DRIVER! #"));
			log.error(String.format("#                               #"));
			log.error(String.format("#################################"));
		}
		return this;
	}

	public Map<String, ATSymbol> getSymbols(boolean includeIncative_, String... groups_) {
		log.debug(String.format("getSymbols Ingored [includeIncative:%b/%s]", includeIncative_, groups_));
		if (groups_ == null || groups_.length == 0) {
			Map<String, ATSymbol> map = includeIncative_
					? _readonlySymbolMap
					: _symbolMap.entrySet().stream().collect(
							Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, TreeMap::new));
			return map;
		} else {
			var map = loadSymbols(groups_);
			return map;
		}
	}

	public long addDynamicSymbols(String uid_, List<String> occIds_) {
		Map<String, ATMongoDynamicSymbol> dynSymbolMap = new HashMap<>();
		for (String occId : occIds_) {
			String symbol = ATModelUtil.getSymbol(occId);
			ATMongoDynamicSymbol dsym = dynSymbolMap.get(symbol);
			if (dsym == null) {
				dsym = new ATMongoDynamicSymbol(symbol, new ArrayList<>());
				dsym.setUserId(uid_);
				dynSymbolMap.put(symbol, dsym);
			}
			if (!dsym.containsKey(occId)) {
				dsym.getOccIds().add(occId);
			}
		}
		@SuppressWarnings("unchecked")
		Collection<Document> data = (Collection<Document>) (Object) dynSymbolMap.values();
		long count = _dao.fastBulkInsert(_dynamicSymbolTable, data);
		return count;
		// ATMongoDynamicSymbol[] data = dynSymbolMap.values().toArray(new ATMongoDynamicSymbol[0]);
		// _dao.i
	}

	public ATDynamicSymbolPack getDynamicSymbols() {
		List<ATMongoDynamicSymbol> dynSymbols = _dao.query(_dynamicSymbolTable, ATPersistenceRegistry.query().empty(),
				ATMongoDynamicSymbol.class);
		if (dynSymbols == null || dynSymbols.isEmpty())
			return null;
		int count = 0;
		Map<String, Set<String>> userSymbolMap = new HashMap<>();
		Map<String, ATSymbol> symbolMap = new TreeMap<>();
		for (ATMongoDynamicSymbol dynSymbol : dynSymbols) {
			List<String> occIds = dynSymbol.getOccIds();
			if (occIds == null || occIds.isEmpty())
				continue;
			String underlyingOccId = dynSymbol.getAtId();
			ATSymbol symbol = symbolMap.get(underlyingOccId);
			if (symbol == null) {
				ATSymbol sym = _symbolMap.get(underlyingOccId);
				symbol = sym == null ? new ATSymbol(underlyingOccId) : sym;
				symbol.setOptions(new TreeMap<>());
				symbolMap.put(underlyingOccId, symbol);
			}
			String userId = dynSymbol.getUserId();
			Set<String> userSymbols = userSymbolMap.get(userId);
			if (userSymbols == null) {
				userSymbols = new TreeSet<>();
				userSymbolMap.put(userId, userSymbols);
			}
			userSymbols.add(underlyingOccId);
			for (String occId : occIds) {
				count++;
				if (occId.length() <= 6)
					continue; // NOTE: Base symbol covered above
				Date dexp = ATModelUtil.getExpiration(occId);
				double strike = ATModelUtil.getStrike(occId);
				ATOptionChain chain = symbol.getOptions().get(dexp);
				if (chain == null) {
					chain = new ATOptionChain(underlyingOccId, dexp, new TreeSet<>());
					symbol.getOptions().put(dexp, chain);
				}
				chain.getStrikes().add(strike);
			}
		}
		log.debug(String.format("Loaded [%d] dynamic symbols", count));
		ATDynamicSymbolPack pack = new ATDynamicSymbolPack();
		pack.dynamicSymbolMap = symbolMap;
		pack.userSymbolMap = userSymbolMap;
		return pack;
	}

	public Map<String, ATOptionExpiration> loadOptionExpirations(IATWhereQuery query_) {
		IATWhereQuery query = query_ != null ? query_ : ATPersistenceRegistry.query().empty();
		List<ATPersistentLookupBase> oeList = _dao.query(_optionExpTable, query, ATPersistentLookupBase.class);
		Map<String, ATOptionExpiration> resultMap = new TreeMap<>();
		if (oeList != null) {
			for (ATPersistentLookupBase base : oeList) {
				ATOptionExpiration oe = new ATOptionExpiration();
				oe.setOccId((String) base.get(IATMongo.MONGO_ID));
				@SuppressWarnings("unchecked")
				List<Document> oeDocs = (List<Document>) (Object) IATOptionExpiration.getExpirations(base);
				List<ATOptionExpirationDate> expDates = oeDocs.stream()
						.map(oed -> new ATOptionExpirationDate(IATOptionExpiration.getExpDS6(oed),
								IATOptionExpiration.getStrikes(oed)))
						.collect(Collectors.toList());
				oe.setExpirations(expDates);
				resultMap.put(oe.getOccId(), oe);
			}
		}
		return resultMap;
	}

	public Map<String, String> getSymbolConversions(String srcId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.SOURCE, srcId_);
		List<ATPersistentLookupBase> dbList = _dao.query(_symbolConverterTable, query, ATPersistentLookupBase.class);
		if (dbList == null || dbList.isEmpty())
			return null;
		Map<String, String> result = new TreeMap<>();
		for (ATPersistentLookupBase entry : dbList) {
			String atId = (String) entry.get(IATLookupMap.AT_ID);
			String conv = (String) entry.get("conv");
			result.put(atId, conv);
		}
		return result;
	}

	public boolean updateOptionExpirations(Collection<IATOptionExpiration> exps_) {
		if (exps_ == null || exps_.isEmpty())
			return false;
		int count = 0;
		for (IATOptionExpiration exp : exps_) {
			String occId = exp.getOccId();
			Map<String, Object> map = exp.getAsMap();
			map.put(IATMongo.MONGO_ID, occId);
			map.remove(IATAssetConstants.OCCID);
			boolean success = _dao.updateValues(_optionExpTable, occId, map, true);
			if (success)
				count++;
		}
		return count == exps_.size();
	}

	public long deleteOptionExpirations(IATWhereQuery query_) {
		if (query_ == null)
			return -1;
		long count = _dao.delete(_optionExpTable, query_);
		return count;
	}

	// private void mergeSymbols() {
	// Map<String, ATMongoSymbol> symbolMap = new TreeMap<>();
	// symbolMap.putAll(_positionSymbolMap);
	// symbolMap.putAll(_baseSymbolMap);
	// _symbolMap = symbolMap;
	// notifyListener(_symbolMap);
	// }

	private void readSymbolsSync() {
		List<ATMongoSymbol> symbols = _dao.query(_standardSymbolTable, ATPersistenceRegistry.query().empty(),
				ATMongoSymbol.class);
		Map<String, ATSymbol> symbolMap = new TreeMap<>();
		Set<String> expirationStrings = new TreeSet<>();
		Map<String, Set<ATOptionChain>> chains = readOptionChainsSync(null);
		for (ATMongoSymbol msym : symbols) {
			if (msym.isInactive() || (msym.getReferences() != null && msym.getReferences().size() == 1
					&& msym.getReferences().contains(IATAssetConstants.IDX_R3000)))
				continue;
			// if ("BRK.B".equals(msym.getAtId())) {
			// log.info(msym.getAtId());
			// }
			ATSymbol symbol = new ATSymbol((IATSymbol) msym);
			Set<ATOptionChain> options = chains.get(symbol.getId());
			if (options != null) {
				Map<Date, ATOptionChain> chain = new TreeMap<>();
				for (ATOptionChain option : options) {
					Date dExp = option.getExpiration();
					chain.put(dExp, option);
					String expStr = _df.format(dExp);
					expirationStrings.add(expStr);
				}
				symbol.setOptions(chain);
			}
			symbolMap.put(symbol.getId(), symbol);
		}
		_symbolMap = symbolMap;
		_readonlySymbolMap = Collections.unmodifiableMap(_symbolMap);
		_expirationStrings.clear();
		_expirationStrings.addAll(expirationStrings);
		log.info(String.format("Loaded [%d] symbols", symbolMap.size()));
	}

	private Map<String, ATSymbol> loadSymbols(String... groups_) {
		Map<String, ATSymbol> symbolMap = new TreeMap<>();
		var docs = _dao.queryReferences(_standardSymbolTable, groups_);
		for (Document doc : docs) {
			ATSymbol symbol = new ATSymbol(new ATMongoSymbol(doc));
			symbolMap.put(symbol.getId(), symbol);
		}
		return symbolMap;
	}

	// private void monitorStandardSymbols() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify standard symbol [%s]: %s", operation_, data_));
	// 			}
	// 			switch (operation_) {
	// 			case "DELETE":
	// 				BsonDocument doc = (BsonDocument) data_;
	// 				String delMongoId = doc.getString(IATMongo.MONGO_ID).getValue();
	// 				String delId = ATMongoSymbol.fromMongoId(delMongoId);
	// 				if (!StringUtils.isBlank(delId)) {
	// 					ATSymbol delSymbol = new ATSymbol();
	// 					delSymbol.setId(delId);
	// 					_symbolMap.remove(delId);
	// 					notifyStandardSymbolListener(EATChangeOperation.DELETE, delSymbol);
	// 				} else {
	// 					log.error(String.format("monitorStandardSymbols Not found [%s]", delMongoId));
	// 				}
	// 				break;
	// 			case "ADD":
	// 			case "INSERT":
	// 			case "UPDATE":
	// 				String json = _gson.toJson(data_);
	// 				ATSymbol newSymbol = new ATSymbol(_gson.fromJson(json, ATMongoSymbol.class));
	// 				ATSymbol oldSymbol = _symbolMap.put(newSymbol.getId(), newSymbol);
	// 				if (oldSymbol != null) {
	// 					newSymbol.setOptions(oldSymbol.getOptions());
	// 				} else {
	// 					Map<String, Set<ATOptionChain>> chainMap = readOptionChainsSync(newSymbol.getId());
	// 					if (chainMap != null && !chainMap.isEmpty()) {
	// 						Map<Date, ATOptionChain> chain = new TreeMap<>();
	// 						for (ATOptionChain option : chainMap.get(newSymbol.getId())) {
	// 							chain.put(option.getExpiration(), option);
	// 						}
	// 						newSymbol.setOptions(chain);
	// 					}
	// 				}
	// 				_symbolMap.put(newSymbol.getId(), newSymbol);
	// 				EATChangeOperation op = ATMongoTools.toOperation(operation_);
	// 				notifyStandardSymbolListener(op, newSymbol);
	// 				break;
	// 			default:
	// 				log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 				break;
	// 			}
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _standardSymbolTable;// _config.getString(tableKey);
	// 		}
	// 	});
	// }

	// private void monitorDynamicSymbols() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("Notify dynamic symbol [%s]: %s", operation_, data_));
	// 			}
	// 			notifyDynamicSymbolListener();
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _dynamicSymbolTable;// _config.getString(tableKey);
	// 		}
	// 	});
	// }

	public Set<String> getExpirations() {
		return _readonlyExpirationStrings;
	}

	private Map<String, Set<ATOptionChain>> readOptionChainsSync(String atId_) {
		IATWhereQuery query = StringUtils.isBlank(atId_) ? null
				: ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, atId_);
		Map<String, ATOptionExpiration> expMap = loadOptionExpirations(query);
		if (expMap != null) {
			Map<String, Set<ATOptionChain>> result = new TreeMap<>();
			for (Entry<String, ATOptionExpiration> entry : expMap.entrySet()) {
				ATOptionExpiration optionExp = entry.getValue();
				Set<ATOptionChain> chains = optionExpirationToChain(optionExp);
				result.put(entry.getKey(), chains);
			}
			log.debug(String.format("readOptionChainsSync[%d]", result.size()));
			return result;
		} else {
			log.warn(String.format("No option chain [%s]", atId_));
			return null;
		}
	}

	private Set<ATOptionChain> optionExpirationToChain(IATOptionExpiration exp_) {
		if (exp_ == null)
			return null;
		SimpleDateFormat df = ATFormats.DATE_yyMMdd.get();
		String occId = exp_.getOccId();
		try {
			Set<ATOptionChain> chains = new TreeSet<>();
			for (ATOptionExpirationDate exp : exp_.getExpirations()) {
				String expDS6 = exp.getExpDS6();
				Date dExp = df.parse(expDS6);
				Set<Double> strikes = new TreeSet<>(exp.getStrikes());
				ATOptionChain chain = new ATOptionChain(occId, dExp, strikes);
				chains.add(chain);
			}
			return chains;
		} catch (Exception ex) {
			log.error(String.format("optionExpirationToChain[%s] Error", occId), ex);
			return null;
		}
	}

	// private void monitorOptionChain() {
	// 	ATMongoReactiveDao daoReact = new ATMongoReactiveDao();
	// 	daoReact.subscribe(new IATMongoListener() {

	// 		@Override
	// 		public void notify(Object data_, String operation_, UpdateDescription update_) {
	// 			if (log.isDebugEnabled()) {
	// 				log.debug(String.format("monitorOptionChain [%s]: %s", operation_, data_));
	// 			}
	// 			switch (operation_) {
	// 				case "DELETE":
	// 					BsonDocument bdoc = (BsonDocument) data_;
	// 					String delMongoId = bdoc.getString(IATMongo.MONGO_ID).getValue();
	// 					String symbolId = ATMongoSymbol.fromMongoId(delMongoId);
	// 					ATSymbol symbol = _symbolMap.get(symbolId);
	// 					if (symbol == null) {
	// 						log.error(String.format("Option chain symbol not found [%s / %s]", delMongoId, symbolId));
	// 						return;
	// 					}
	// 					symbol.setOptions(null);
	// 					notifyStandardSymbolListener(EATChangeOperation.UPDATE, symbol);
	// 					break;
	// 				case "ADD":
	// 				case "INSERT":
	// 				case "UPDATE":
	// 					Document doc = (Document) data_;
	// 					String occId = doc.getString(IATMongo.MONGO_ID);
	// 					ATSymbol chainSymbol = _symbolMap.get(occId);
	// 					if (chainSymbol == null) {
	// 						log.error(String.format("monitorOptionChain Symbol not found [%s]", occId));
	// 						return;
	// 					}
	// 					List<ATOptionExpirationDate> dexps = IATOptionExpiration.getExpirations(doc);
	// 					ATOptionExpiration exp = new ATOptionExpiration();
	// 					exp.setOccId(occId);
	// 					exp.setExpirations(dexps);
	// 					Set<ATOptionChain> chains = optionExpirationToChain(exp);
	// 					if (chains == null) {
	// 						log.error(String.format("monitorOptionChain Chain is null [%s]", occId));
	// 						return;
	// 					}
	// 					Map<Date, ATOptionChain> chainMap = chains.stream()
	// 							.collect(Collectors.toMap(ATOptionChain::getExpiration, c -> c));
	// 					chainSymbol.setOptions(chainMap);
	// 					notifyStandardSymbolListener(EATChangeOperation.UPDATE, chainSymbol);
	// 					break;
	// 				default:
	// 					log.error(String.format("Unsupported operation [%s]: %s", operation_, _gson.toJson(data_)));
	// 					return;
	// 			}
	// 		}

	// 		@Override
	// 		public String getCollection() {
	// 			return _optionExpTable;
	// 		}
	// 	});
	// }

	/**
	 * Updates given symbols in db.
	 * 
	 * @param symbols_
	 *            - Updated symbols
	 * @param reference_
	 *            - Symbol reference (for targeted additions and deletes)
	 */
	public void update(Collection<ATSymbol> symbols_, String reference_) {
		if ((symbols_ == null || symbols_.isEmpty()) && StringUtils.isBlank(reference_)) {
			log.warn(String.format("No update symbols [%s]", reference_));
			return;
		}
		try {
			Set<String> newIds = new HashSet<>();

			//// // 1. Add & Modify (only when 'symbols' are set)
			int updateCount = 0;
			if (symbols_ != null) {
				for (ATSymbol sym : symbols_) {
					ATSymbol oldSym = _symbolMap.get(sym.getId());
					if (oldSym != null && oldSym.equals(sym))
						continue;
					String id = sym.getId();
					String newDesc = sym.getDescription();
					String newName = sym.getName();
					String newModel = sym.getModel();
					String newSector = sym.getSector();
					String newIndustry = sym.getIndustry();
					String newSource = sym.getSource();
					Set<String> newReferences = sym.getReferences();
					if (newReferences == null || newReferences.isEmpty())
						log.error(String.format("Missing references [%s]", id));
					Map<String, String> newConverters = sym.getConverters();
					// List<String> options = sym.getOptions();

					// NOTE: Only populate changed properties for faster insert
					ATMongoSymbol newSym = new ATMongoSymbol();
					newSym.setId(id);

					if (oldSym != null) {
						String oldDesc = oldSym.getDescription();
						String oldName = oldSym.getName();
						String oldModel = oldSym.getModel();
						String oldSector = oldSym.getSector();
						String oldIndustry = oldSym.getIndustry();
						String oldSource = oldSym.getSource();
						// Boolean oldInactive = oldSym.isInactive();
						Set<String> oldRefs = oldSym.getReferences();

						String desc = StringUtils.isNotBlank(newDesc) && !newDesc.equals(oldDesc) ? newDesc : oldDesc;
						newSym.setDescription(desc);
						String name = StringUtils.isNotBlank(newName) && !newName.equals(oldName) ? newName : oldName;
						newSym.setName(name);
						String model = StringUtils.isNotBlank(newModel) && !newModel.equals(oldModel) ? newModel
								: oldModel;
						newSym.setModel(model);
						String sector = StringUtils.isNotBlank(newSector) && !newSector.equals(oldSector) ? newSector
								: oldSector;
						newSym.setSector(sector);
						String industry = StringUtils.isNotBlank(newIndustry) && !newIndustry.equals(oldIndustry)
								? newIndustry
								: oldIndustry;
						newSym.setIndustry(industry);
						String source = StringUtils.isNotBlank(newSource) && !newSource.equals(oldSource) ? newSource
								: oldSource;
						newSym.setSource(source);
						// newSym.setInactive(oldInactive);
						if (oldRefs != null) {
							Set<String> newRefs = newReferences != null ? newReferences : new HashSet<>();
							oldRefs.addAll(newRefs);
							newSym.setReferences(oldRefs);
						} else {
							newSym.setReferences(newReferences);
						}
						// Converters unlikely to be programmatically changed
						if (newConverters != null && !newConverters.isEmpty()) {
							Map<String, String> oldConv = oldSym.getConverters();
							int oldSize = oldConv.size();
							oldConv.putAll(newConverters);
							if (oldSize != oldConv.size()) {
								newSym.setConverters(oldConv);
							}
						}
					} else {
						if (newDesc != null)
							newSym.setDescription(newDesc);
						if (newName != null)
							newSym.setName(newName);
						if (newModel != null)
							newSym.setModel(newModel);
						if (newSector != null)
							newSym.setSector(newSector);
						if (newIndustry != null)
							newSym.setIndustry(newIndustry);
						if (newSource != null)
							newSym.setSource(newSource);
						if (newReferences != null && !newReferences.isEmpty())
							newSym.setReferences(newReferences);
						if (newConverters != null && !newConverters.isEmpty())
							newSym.setConverters(newConverters);
						// NOTE: New symbols considered active
					}

					if (_dao.update(_standardSymbolTable, newSym))
						updateCount++;
					_symbolMap.put(newSym.getId(), new ATSymbol(newSym));
					newIds.add(newSym.getId());
				}
			}
			// 2. Remove (only when 'reference' is set)
			int deleteCount = 0;
			if (StringUtils.isNotBlank(reference_)) {
				Set<String> oldIds = new HashSet<>();
				for (ATSymbol sym : _symbolMap.values()) {
					Set<String> oldRefs = sym.getReferences();
					if (oldRefs != null && !oldRefs.contains(reference_))
						oldIds.add(sym.getId());
				}
				oldIds.removeAll(newIds);
				for (String oldId : oldIds) {
					ATSymbol oldSym = _symbolMap.get(oldId);
					Set<String> oldRefs = oldSym.getReferences();
					oldRefs.remove(reference_);
					if (oldRefs.isEmpty()) {
						IATWhereQuery delQuery = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, oldId);
						_dao.delete(_standardSymbolTable, delQuery);
						// deleteOptionChain(oldId);
						deleteOptionExpirations(oldId);
						deleteCount++;
					} else {
						ATMongoSymbol deadSym = new ATMongoSymbol();
						deadSym.setId(oldSym.getId());
						deadSym.setReferences(oldRefs);
						_dao.update(_standardSymbolTable, deadSym);
					}
				}
			}
			log.info(String.format("Symbols [%s: updated: %d; removed: %d]", reference_, updateCount, deleteCount));
		} catch (Exception ex) {
			log.error(String.format("Failed update [%s]", reference_), ex);
		}
	}

	// private void notifyStandardSymbolListener(EATChangeOperation operation_, ATSymbol symbol_) {
	// 	if (!_isStreaming || _listener == null)
	// 		return;
	// 	try {
	// 		_listener.notify(operation_, symbol_);
	// 	} catch (Exception ex) {
	// 		log.error("notifyStandardSymbolListener Error", ex);
	// 	}
	// }

	// private void notifyDynamicSymbolListener() {
	// 	if (!_isStreaming || _listener == null)
	// 		return;
	// 	try {
	// 		_listener.notifyDynamicSymbols(null, null);
	// 	} catch (Exception ex) {
	// 		log.error("notifyDynamicSymbolListener Error", ex);
	// 	}
	// }

	public void shutdown() {
		log.debug(String.format("Shutting down"));
	}

	private long deleteOptionExpirations(String symbol_) {
		IATWhereQuery delQuery = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, symbol_);
		long result = _dao.delete(_optionExpTable, delQuery);
		return result;
	}

	public static class ATMongoOptionChain extends ATPersistentLookupBase implements IATMongo {

		private static final long serialVersionUID = 1L;

		static final String CHAIN = "chain";

		public String getMongoId() {
			return (String) super.get(MONGO_ID);
		}

		public void setMongoId(String value_) {
			super.put(MONGO_ID, value_);
		}

		public String getId() {
			return super.getAtId();
		}

		public void setId(String value_) {
			super.setAtId(value_);
			this.setMongoId(ATMongoSymbol.toMongoId(value_));
		}

		public Map<String, Set<Double>> getChain() {
			@SuppressWarnings("unchecked")
			Map<String, Set<Double>> chain = (Map<String, Set<Double>>) super.get(CHAIN);
			return chain;
		}

		public void setChain(Map<String, Set<Double>> chain) {
			super.put(CHAIN, chain);
		}

	}

	public static class ATMongoOptionData extends Document {

		private static final long serialVersionUID = 1L;

		static final String EXPIRATION = "exp";
		static final String STRIKE = "strike";

		public ATMongoOptionData() {
		}

		public ATMongoOptionData(Date date_, Double strike_) {
			setExpiration(date_);
			setStrike(strike_);
		}

		public Double getStrike() {
			return getStrike(this);
		}

		static public Double getStrike(Document doc_) {
			return doc_.getDouble(STRIKE);
		}

		public void setStrike(Double strike) {
			super.put(STRIKE, strike);
		}

		public Date getExpiration() throws ParseException {
			return getExpiration(this);
		}

		static public Date getExpiration(Document doc_) throws ParseException {
			String expStr = doc_.getString(EXPIRATION);
			Date expiration = ATFormats.DATE_yyyy_MM_dd.get().parse(expStr);
			return expiration;
		}

		public void setExpiration(Date expiration) {
			String expStr = ATFormats.DATE_yyyy_MM_dd.get().format(expiration);
			super.put(EXPIRATION, expStr);
		}

	}

	public static class ATDynamicSymbolPack {
		public Map<String, ATSymbol> dynamicSymbolMap;
		public Map<String, Set<String>> userSymbolMap;
	}

	public static class ATSymbolDao extends ATMongoDao {
		public FindIterable<Document> queryReferences(String table_, String... groups_) {
			MongoCollection<Document> coll = super._db.getCollection(table_);
			if (groups_ == null || groups_.length == 0) {
				FindIterable<Document> find = coll.find();
				return find;
			} else {
				List<Bson> findRefs = Arrays.asList(groups_).stream().map(g -> eq(ATMongoSymbol.REFERENCES, g))
						.collect(Collectors.toList());
				FindIterable<Document> find = coll.find(or(findRefs));
				return find;
			}
		}
	}

}
