package com.isoplane.at.symbols.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.isoplane.at.commons.model.ATOptionChain;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.mongodb.IATMongo;

public class ATMongoSymbol extends Document implements IATSymbol, IATMongo, IATLookupMap {

	private static final long serialVersionUID = 1L;

	static final public String CONVERTERS = "conv";
	static final public String REFERENCES = "ref";
	static final public String OPTIONS = "options";

	transient private Map<Date, ATOptionChain> options;

	public ATMongoSymbol() {
		super();
	}

	public ATMongoSymbol(Document doc_) {
		super(doc_);
	}

	static public String toMongoId(String id_) {
		String mongoId = String.format("S_%s", id_);
		return mongoId;
	}

	/**
	 * Converts MongoDB key to AT id
	 */
	static public String fromMongoId(String mongoId_) {
		if (mongoId_ == null || mongoId_.length() < 3)
			return null;
		String id = mongoId_.substring(2);
		return id;
	}

	public String getMongoId() {
		return super.getString(MONGO_ID);
	}

	public void setMongoId(String value_) {
		super.put(MONGO_ID, value_);
	}

	@Override
	public String getId() {
		return this.getAtId();
	}

	public void setId(String value_) {
		this.setAtId(value_);
		this.setMongoId(toMongoId(value_));
	}

	@Override
	public String getName() {
		return super.getString(IATAssetConstants.NAME);
	}

	public void setName(String value_) {
		super.put(IATAssetConstants.NAME, value_);
	}

	@Override
	public String getDescription() {
		return super.getString(IATAssetConstants.DESCRIPTION);
	}

	public void setDescription(String value_) {
		super.put(IATAssetConstants.DESCRIPTION, value_);
	}

	@Override
	public String getSector() {
		return super.getString(IATAssetConstants.SECTOR);
	}

	public void setSector(String value_) {
		super.put(IATAssetConstants.SECTOR, value_);
	}

	@Override
	public String getIndustry() {
		return super.getString(IATAssetConstants.INDUSTRY);
	}

	public void setIndustry(String value_) {
		super.put(IATAssetConstants.INDUSTRY, value_);
	}

	@Override
	public String getModel() {
		return super.getString(IATAssetConstants.MODEL);
	}

	public void setModel(String value_) {
		super.put(IATAssetConstants.MODEL, value_);
	}

	@Override
	public Map<Date, ATOptionChain> getOptions() {
		return options;
	}

	public void setOptions(Map<Date, ATOptionChain> value_) {
		options = value_;
	}

	@Override
	public String getSource() {
		return super.getString(IATAssetConstants.SOURCE);
	}

	public void setSource(String value_) {
		super.put(IATAssetConstants.SOURCE, value_);
	}

	public Boolean isInactive() {
		Boolean inactive = super.getBoolean(IATAssetConstants.INACTIVE);
		return inactive != null && inactive;
	}

	public void setInactive(Boolean value_) {
		if (value_) {
			super.put(IATAssetConstants.INACTIVE, value_);
		} else {
			super.remove(IATAssetConstants.INACTIVE);
		}
	}

	/**
	 * Expensive operation! Technically retrieves List and constructs TreeSet.
	 */
	@Override
	public Set<String> getReferences() {
		List<String> refs = super.getList(REFERENCES, String.class);
		return refs != null ? new TreeSet<>(refs) : null;
	}

	public void setReferences(Set<String> value_) {
		List<String> refs = value_ != null ? new ArrayList<>(value_) : null;
		super.put(REFERENCES, refs);
	}

	@Override
	public Map<String, String> getConverters() {
		@SuppressWarnings("unchecked")
		Map<String, String> map = super.get(CONVERTERS, Map.class);
		return map;
	}

	public void setConverters(Map<String, String> value_) {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Document conv = new Document((Map) value_);
		super.put(CONVERTERS, conv);
	}

	@Override
	public String convert(String provider_) {
		return IATSymbol.convert(this, provider_);
	}

	/** Returns true if not present and added */
	public boolean addReference(String value_) {
		if (StringUtils.isBlank(value_))
			return false;
		Set<String> refs = getReferences();
		if (refs == null) {
			refs = new TreeSet<>(Arrays.asList(value_));
			setReferences(refs);
			return true;
		} else {
			return refs.add(value_);
		}
	}

	@Override
	public int hashCode() {
		String hashStr = String.format("%s%s%s%s%s", getId(), getDescription(), getModel(), getConverters(),
				getReferences());
		return hashStr.hashCode();
	}

	@Override
	public boolean equals(Object obj_) {
		if (obj_ == null || !(obj_ instanceof ATMongoSymbol))
			return false;
		ATMongoSymbol other = (ATMongoSymbol) obj_;
		return this.hashCode() == other.hashCode();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> value_) {
		super.putAll(value_);
	}

	@Override
	public void setUpdateTime(long value_) {
		super.put(UPDATE_TIME, value_);
	}

	@Override
	public Long getUpdateTime() {
		return super.getLong(UPDATE_TIME);
	}

	// @Override
	// public void setInsertTime(long value_) {
	// super.put(INSERT_TIME, value_);
	// }

	@Override
	public Long getInsertTime() {
		return super.getLong(INSERT_TIME);
	}

	@Override
	public void setAtId(String value_) {
		super.put(AT_ID, value_);
	}

	@Override
	public String getAtId() {
		return super.getString(AT_ID);
	}

	@Override
	public int compareTo(IATSymbol other_) {
		return IATSymbol.compareTo(this, other_);
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public Boolean isSP500() {
		var refs = this.getReferences();
		return refs != null && refs.contains(IATAssetConstants.SP500);
	}

	@Override
	public Boolean isR3000() {
		var refs = this.getReferences();
		return refs != null && refs.contains(IATAssetConstants.RUSSELL3000);
	}

}
