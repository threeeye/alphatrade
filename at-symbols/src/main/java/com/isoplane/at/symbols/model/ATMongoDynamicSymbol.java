package com.isoplane.at.symbols.model;

import java.util.List;
import java.util.Map;

import org.bson.Document;

import com.isoplane.at.commons.store.IATLookupMap;

public class ATMongoDynamicSymbol extends Document implements IATLookupMap {

	private static final long serialVersionUID = 1L;

	public final static String ID = "_id";
	public final static String USER_ID = "usr";
	public final static String OCC_IDS = "occIds";

	public ATMongoDynamicSymbol() {
	}

	public ATMongoDynamicSymbol(String occId_, List<String> occIds_) {
		setAtId(occId_);
		setOccIds(occIds_);
	}

	@Override
	public String getAtId() {
		return super.getString(ID);
	}

	@Override
	public void setAtId(String value_) {
		super.put(ID, value_);
	}

	public String getUserId() {
		return super.getString(USER_ID);
	}

	public void setUserId(String value_) {
		super.put(USER_ID, value_);
	}

	public List<String> getOccIds() {
		List<String> data = super.getList(OCC_IDS, String.class);
		return data;
	}

	public void setOccIds(List<String> value_) {
		super.put(OCC_IDS, value_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> map_) {
		super.putAll(map_);
	}

	@Override
	public void setUpdateTime(long value_) {
		IATLookupMap.setUpdateTime(this, value_);
	}

	@Override
	public Long getUpdateTime() {
		return IATLookupMap.getUpdateTime(this);
	}

	@Override
	public Long getInsertTime() {
		return IATLookupMap.getInsertTime(this);
	}

}
