package com.isoplane.at.symbols;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.IATSymbolListener;
import com.isoplane.at.commons.service.IATSymbolProvider;

public class ATFakeSymbolManager implements IATSymbolProvider {

	static final Logger log = LoggerFactory.getLogger(ATFakeSymbolManager.class);

	private Set<String> symbols;

	public ATFakeSymbolManager(String... symbols_) {
		this.symbols = new TreeSet<>(Arrays.asList(symbols_));
	}

	@Override
	public void initService() {
		log.info(String.format("initService"));
	}

	@Override
	public void register(IATSymbolListener listener) {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<IATSymbol> lookupSymbol(String symbol) {
		log.info(String.format("lookupSymbol: %s", symbol));
		ATSymbol sym = new ATSymbol(symbol);
		return Arrays.asList(sym);
	}

	@Override
	public Map<String, ATSymbol> getSymbols(String... groups) {
		log.info(String.format("getSymbols"));
		if (this.symbols == null)
			return null;
		Map<String, ATSymbol> symMap = this.symbols.stream().collect(Collectors.toMap(s -> s, s -> new ATSymbol(s)));
		return symMap;
	}

	@Override
	public Map<String, String> getSymbolConversions(String srcId) {
		log.info(String.format("getSymbolConversions: %s", srcId));
		return null;
	}

	@Override
	public Map<String, ATOptionExpiration> getOptionExpirations(Collection<String> symbols) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Set<String> getExpirations() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<String, Set<String>> getCategories() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addDynamicSymbols(String userId, List<String> occIds) {
		log.info(String.format("addDynamicSymbols: %s, %s", userId, occIds));
		if (occIds == null)
			return;
		this.symbols.addAll(occIds);
	}

}
