package com.isoplane.at.symbols;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import com.isoplane.at.commons.model.ATOptionChain;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.IATSymbolListener;
import com.isoplane.at.commons.service.IATSymbolProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.symbols.util.ATSymbolMogodbTools;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATSymbolManager implements IATSymbolProvider /*, IATSymbolListener*/ {

	static final Logger log = LoggerFactory.getLogger(ATSymbolManager.class);

	// private Configuration _config;
	protected ATSymbolMogodbTools _mongodb;
	// private Map<String, IATSymbol> _symbolMap;
	private Set<IATSymbolListener> _listeners;
	private boolean _isInitialized = false;
//	private boolean _processNotifications = false;
	private boolean _isStreaming = false;
//	private BlockingQueue<ATSymbolNotification> _queue;
//	private ATSymbolNotifier _notifier;
//	private Long _queueInterval;
//	private Timer _dynTimer = new Timer();

	public static void main(String[] args_) {
		try {
			ATConfigUtil.init(args_[0], false);
			ATExecutors.init();
			ATSymbolManager instance = new ATSymbolManager(false);
			instance.initService();

			ATSysUtils.waitForEnter();
			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATSymbolManager.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATSymbolManager(boolean isStreaming_) {
	//	Configuration config = ATConfigUtil.config();
		_isStreaming = isStreaming_;
		if (_isStreaming) {
			// int queueSize = config.getInt("symbols.queueSize");
			// _queueInterval = config.getLong("symbols.queueInterval");
			// log.info(String.format("Initializing queue [%d]", queueSize));
			// _queue = new ArrayBlockingQueue<>(queueSize);
			// String threadId = ATSymbolNotifier.class.getSimpleName();
			// _notifier = new ATSymbolNotifier(threadId);
			// ATExecutors.schedule(_notifier.getId(), _notifier, 0);
		}
	}

	@Override
	public void initService() {
		log.info("Initializing...");

		initSymbols();
//		_processNotifications = true;
		_isInitialized = true;
	}

	@Override
	public void register(IATSymbolListener listener_) {
		if (_listeners == null) {
			_listeners = new HashSet<>();
		}
		_listeners.add(listener_);
	}

	@Override
	public Map<String, ATSymbol> getSymbols(String... groups_) {
		if (!_isInitialized) {
			log.warn(String.format("[%s] not initialized.", this.getClass().getSimpleName()));
			initService();
		}
		return _mongodb.getSymbols(false, groups_);
	}

	public ATSymbol getSymbol(String occId_, boolean constructIfMissing_) {
		if (StringUtils.isBlank(occId_))
			return null;
		String occId = occId_.trim();
		Map<String, ATSymbol> sMap = _mongodb.getSymbols(true);
		ATSymbol symbol = sMap.get(occId);
		if (symbol == null) {
			log.warn(String.format("Missing symbol [%s]. Defaulting [%b]", occId, constructIfMissing_));
			if (constructIfMissing_) {
				String symStr = ATModelUtil.getSymbol(occId);
				symbol = new ATSymbol(symStr);
			}
		}
		return symbol;
	}

	@Override
	public Map<String, String> getSymbolConversions(String srcId_) {
		return _mongodb.getSymbolConversions(srcId_);
	}

	@Override
	public Map<String, Set<String>> getCategories() {
		Map<String, ATSymbol> syms = _mongodb.getSymbols(false);
		Map<String, Set<String>> rmap = new TreeMap<>();
		for (IATSymbol sym : syms.values()) {
			String s = sym.getSector();
			if (StringUtils.isBlank(s))
				continue;
			String i = sym.getIndustry();
			if (StringUtils.isBlank(i))
				continue;
			Set<String> sector = rmap.get(s);
			if (sector == null) {
				sector = new TreeSet<>();
				rmap.put(s, sector);
			}
			sector.add(i);
		}
		return rmap;
	}

	@Override
	public Set<String> getExpirations() {
		Set<String> expirations = _mongodb.getExpirations();
		return expirations;
	}

	@Override
	public List<IATSymbol> lookupSymbol(String symbol_) {
		List<IATSymbol> result = new ArrayList<>();
		if (StringUtils.isBlank(symbol_))
			return result;

		String symbol = symbol_.trim().toUpperCase();
		Map<String, ATSymbol> sMap = _mongodb.getSymbols(true);
		for (Entry<String, ATSymbol> entry : sMap.entrySet()) {
			String key = entry.getKey();
			String name = entry.getValue().getName();
			if (key.contains(symbol) || (name != null && name.toUpperCase().contains(symbol))) {
				result.add(entry.getValue());
			}
		}
		return result;
	}

	@Override
	public void addDynamicSymbols(String userId_, List<String> occIds_) {
		long count = _mongodb.addDynamicSymbols(userId_, occIds_);
		log.debug(String.format("Inserted [%d] dynamic symbols", count));
	}

	private Map<String, ATSymbol> initSymbols() {
		_mongodb = new ATSymbolMogodbTools(_isStreaming/*, this*/);
		_mongodb.init();

		Map<String, ATSymbol> msym = _mongodb.getSymbols(true);
		int count = 0;
		for (ATSymbol sym : msym.values()) {
			count++;
			Map<Date, ATOptionChain> options = sym.getOptions();
			if (options != null) {
				for (ATOptionChain expirations : options.values()) {
					if (expirations != null) {
						Set<Double> strikes = expirations.getStrikes();
						if (strikes != null) {
							count += strikes.size();
						}
					}
				}
			}
		}
		log.info(String.format("Initialized symbols [%d | %d]", msym.size(), count));
		return msym;
	}

	// @Override
	// public void notify(EATChangeOperation operation_, ATSymbol symbol_) {
	// 	if (!_processNotifications || !_isStreaming || _listeners == null)
	// 		return;
	// 	try {
	// 		Configuration config = ATConfigUtil.config();
	// 		ATSymbolNotification data = new ATSymbolNotification(operation_, symbol_);
	// 		if (!_queue.offer(data, _queueInterval, TimeUnit.MILLISECONDS)) {
	// 			_queueInterval = config.getLong("symbol.queueInterval");
	// 			log.error(String.format("Queue[sym] full [%d/%d]", _queue.size(), _queue.remainingCapacity()));
	// 		}
	// 	} catch (Exception ex) {
	// 		log.error(String.format("Queue[sym] processing error"), ex);
	// 	}
	// }

	// @Override
	// public void notifyDynamicSymbols(Map<String, ATSymbol> ignore1, Map<String, Set<String>> ignore2) {
	// 	TimerTask task = new TimerTask() {

	// 		@Override
	// 		public void run() {
	// 			if (_listeners == null)
	// 				return;
	// 			ATDynamicSymbolPack pack = _mongodb.getDynamicSymbols();
	// 			Map<String, ATSymbol> dynamicSmbolMap = pack != null ? pack.dynamicSymbolMap : null;
	// 			Map<String, Set<String>> userSymbolMap = pack != null ? pack.userSymbolMap : null;
	// 			for (IATSymbolListener listener : _listeners) {
	// 				try {
	// 					listener.notifyDynamicSymbols(dynamicSmbolMap, userSymbolMap);
	// 				} catch (Exception ex) {
	// 					log.error(String.format("notifyDynamicSymbols Error: %s", listener), ex);
	// 				}
	// 			}

	// 		}
	// 	};
	// 	_dynTimer.cancel();
	// 	_dynTimer = new Timer();
	// 	_dynTimer.schedule(task, 1000L);
	// }

	public void shutdown() {
		log.debug(String.format("Shutting down"));
		if (_listeners != null)
			_listeners.clear();
		if (_mongodb != null)
			_mongodb.shutdown();
	}

	public void update(Collection<ATSymbol> symbols_, String type_) {
		_mongodb.update(symbols_, type_);
	}

	// @Deprecated
	// public void updateOptions(Map<String, Set<IATOptionDescription>> chains_) {
	// if (chains_ == null || chains_.isEmpty()) {
	// log.warn(String.format("Empty option chains"));
	// return;
	// }
	// for (Entry<String, Set<IATOptionDescription>> chain : chains_.entrySet()) {
	// _mongodb.updateOptionChain(chain.getKey(), chain.getValue());
	// }
	// }

	public String convertId(String occId_, String converterId_) {
		String symbolId = occId_.length() > 10 ? occId_.substring(0, 6) : occId_;
		IATSymbol symbol = getSymbol(symbolId.trim(), false);
		if (symbol != null) {
			String convId = symbol.convert(converterId_);
			if (occId_.length() > 10) {
				convId = String.format("%-6s", convId);
				convId = occId_.replace(symbolId, convId);
			}
			return convId;
		} else {
			return null;
		}
	}

	@Override
	public Map<String, ATOptionExpiration> getOptionExpirations(Collection<String> symbols_) {
		if (symbols_ == null || symbols_.isEmpty())
			return null;
		IATWhereQuery query = symbols_.size() > 1
				? ATPersistenceRegistry.query().in(IATMongo.MONGO_ID, symbols_)
				: ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, symbols_.iterator().next());
		return _mongodb.loadOptionExpirations(query);
	}

	public boolean updateOptionExpirations(Collection<IATOptionExpiration> exps_) {
		return _mongodb.updateOptionExpirations(exps_);
	}

	public long deleteOptionExpirations(IATSymbol symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATMongo.MONGO_ID, symbol_.getId());
		return _mongodb.deleteOptionExpirations(query);
	}

	// public class ATSymbolNotifier implements Runnable {

	// 	private String _id;
	// 	private boolean _isRunning;

	// 	public ATSymbolNotifier(String id_) {
	// 		_id = id_;
	// 	}

	// 	public String getId() {
	// 		return _id;
	// 	}

	// 	public void shutdown() {
	// 		_isRunning = false;
	// 	}

	// 	@Override
	// 	public void run() {
	// 		_isRunning = true;
	// 		Thread.currentThread().setName(_id);
	// 		try {
	// 			do {
	// 				ATSymbolNotification data = _queue.poll(_queueInterval, TimeUnit.MILLISECONDS);
	// 				if (data == null) {
	// 					Configuration config = ATConfigUtil.config();
	// 					_queueInterval = config.getLong("symbols.queueInterval");
	// 					if (log.isTraceEnabled())
	// 						log.trace(String.format("Polling empty queue [%d]", _queueInterval));
	// 				} else {
	// 					for (IATSymbolListener listener : _listeners) {
	// 						try {
	// 							listener.notify(data.operation, data.symbol);
	// 						} catch (Exception ex) {
	// 							log.error(String.format("Notification error: %s -> %s", data, listener), ex);
	// 						}
	// 					}
	// 				}
	// 			} while (_isRunning);
	// 		} catch (Exception ex) {
	// 			log.error(String.format("%s terminated.", ATSymbolNotifier.class.getSimpleName()), ex);
	// 		}
	// 	}

	// }

	// public class ATSymbolNotification {
	// 	public ATSymbolNotification(EATChangeOperation operation_, ATSymbol symbol_) {
	// 		operation = operation_;
	// 		symbol = symbol_;
	// 	}

	// 	public EATChangeOperation operation;
	// 	public ATSymbol symbol;

	// }

}
