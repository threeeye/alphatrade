package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.isoplane.at.adapter.alphavantage.ATAlphaVantageAdapter;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAlphaVantageAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageAdapterTest.class);
	static private ATAlphaVantageAdapter _avAdp;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String propertiesPath = System.getProperty("test_args");
		Configuration config = ATConfigUtil.init(propertiesPath, false);
		ATConfigUtil.overrideConfiguration("alphavantage.key", "demo");

		String[] configKeys = {
				"mongo.t.foundation",
				"mongo.t.statement.blnc.alva",
				"mongo.t.statement.cash.alva",
				"mongo.t.statement.comp.alva",
				"mongo.t.statement.cons.alva",
				"mongo.t.statement.earn.alva" };
		for (String key : configKeys) {
			String testTable = String.format("TEST_%s", config.getString(key));
			ATConfigUtil.overrideConfiguration(key, testTable);
		}
		// String testArgs = System.getProperty("test_args");
		// String[] propertiesPaths = testArgs.split(",");
		// _config = new CompositeConfiguration();
		// for (String path : propertiesPaths) {
		// log.info(String.format("Reading properties [%s]", path));
		// PropertiesConfiguration config = new Configurations().properties(new File(path));
		// _config.addConfiguration(config);
		// }
		// ATConfigUtil.init(testArgs, false)
		ATExecutors.init();
		_avAdp = ATAlphaVantageAdapter.getInstance();
	}

	@AfterAll
	public static void tearDown() {
		log.info(String.format("%s done", ATAlphaVantageAdapterTest.class.getSimpleName()));
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	// @Disabled
	public void testIncomeStatement() throws ParseException {
		String symbol = "IBM";
		List<Map<String, Object>> result = _avAdp.getIncomeStatement(symbol);
		assertTrue(result != null);
	}

	@Test
	// @Disabled
	public void testBalanceSheet() throws ParseException {
		String symbol = "IBM";
		List<Map<String, Object>> result = _avAdp.getBalanceSheet(symbol);
		assertTrue(result != null);
	}

	@Test
	// @Disabled
	public void testCashFlow() throws ParseException {
		String symbol = "IBM";
		List<Map<String, Object>> result = _avAdp.getCashFlow(symbol);
		assertTrue(result != null);
	}

	@Test
	// @Disabled
	public void testCompany() throws ParseException {
		String symbol = "IBM";
		List<Map<String, Object>> result = _avAdp.getCompany(symbol);
		assertTrue(result != null);
	}

	@Test
	@Disabled
	public void testParseTrades() {
		// String json = getResourceFileAsString("alpaca_trade.json");
		// List<ATTrade> list = _alpacaAdp.parseTrade(json);
		// assertTrue(list != null && list.size() == 1);
	}

}
