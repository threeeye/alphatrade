package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.Set;

import com.isoplane.at.adapter.kibot.ATKibotAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKibotAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATKibotAdapterTest.class);
	static private TestKibotAdapter _adp;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, true);

		ATExecutors.init();
		_adp = new TestKibotAdapter();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
//	@Disabled
	public void testR3000() throws ParseException {
		Set<ATSymbol> result = _adp.getRussell3000();
		assertTrue(result != null);
		assertTrue(!result.isEmpty());
	}

	@Test
	public void testParsing() {
		String tsv = ATSysUtils.getResourceFileAsString("kibot_Russell_3000_Intraday.txt");
		Set<ATSymbol> result = _adp.parseSymbols(tsv);
		assertTrue(result != null);
		assertTrue(!result.isEmpty());
	}

	public static class TestKibotAdapter extends ATKibotAdapter {

		public Set<ATSymbol> parseSymbols(String tsv_) {
			return super.parseSymbols(tsv_);
		}
	}

}
