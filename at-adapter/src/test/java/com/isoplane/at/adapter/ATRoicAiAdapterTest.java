package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import com.isoplane.at.adapter.roicai.ATRoicAiAdapter;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATRoicAiAdapterTest {

    static final Logger log = LoggerFactory.getLogger(ATRoicAiAdapterTest.class);
    static private TestRoicAiAdapter _webApi;

    @BeforeAll
    public static void setup() throws ConfigurationException {
        String configPath = System.getProperty("config_path");
        String propertiesPath = System.getProperty("test_args");
        ATConfigUtil.init(configPath, propertiesPath, true);

        ATExecutors.init();
        _webApi = new TestRoicAiAdapter();
    }

    @AfterAll
    public static void tearDown() {
        log.info(String.format("%s done", ATRoicAiAdapterTest.class.getSimpleName()));
    }

    @BeforeEach
    public void prepare() {

    }

    @AfterEach
    public void cleanup() {
    }

    @Test
    // @Disabled()
    public void testParse() {
        List<String> symbols = Arrays.asList("ATVI");
        for (String symbol : symbols) {
            String fileName = String.format("roicai_%s.htm", symbol);
            String fileId = _webApi.getFileId(fileName);
            assertNotNull(fileId);
            _webApi.readExcel(fileId);
            //   FileInputStream fis = new FileInputStream(new File("C:\\mysheets\\employee.xls"));

        }
        // assertTrue(result != null);
        // assertEquals(symbols.size(), result.size());
        // assertTrue(result.keySet().containsAll(symbols));
    }

    public static class TestRoicAiAdapter extends ATRoicAiAdapter {

        public String getFileId(String filePath_) {
            String html = ATSysUtils.getResourceFileAsString(filePath_);
            String fileId = super.extractFileId(html);
            return fileId;
        }

        public void readExcel(String fileId_) {
       //     ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            try (InputStream is = new URL("https://roic.ai/api/getexcel/ATVI-fs-01232022-kfrrw.xlsx").openStream()) {
                // try (InputStream is = classLoader.getResourceAsStream(fileId_)) {
                if (is == null)
                    return;
                Workbook wb = WorkbookFactory.create(is);
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    log.info(wb.getSheetAt(i).getSheetName());
                    Sheet sheet = wb.getSheetAt(i);
                    if (sheet == null)
                        continue;
                    for (int j = 0; j < sheet.getLastRowNum(); j++) {
                        Row row = sheet.getRow(j);
                        if (row == null)
                            continue;
                        for (int k = 0; k < row.getLastCellNum(); k++) {
                            System.out.print("\"" + row.getCell(k) + "\";");
                        }
                        System.out.println();
                    }
                    //    echoAsCSV(wb.getSheetAt(i));
                }

                //       XSSFWorkbook wb = new XSSFWorkbook(is);
                // try (InputStreamReader isr = new InputStreamReader(is);
                //         BufferedReader reader = new BufferedReader(isr)) {
                //     return reader.lines().collect(Collectors.joining(System.lineSeparator()));
                // }
            } catch (Exception ex) {
                log.error(String.format("Couldn't read '%s'", fileId_), ex);
                return;
            }
            super.getExcel(fileId_);
        }
    }

}
