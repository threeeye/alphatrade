package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;

import com.isoplane.at.adapter.etrade.ATEtradeAdapter;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATEtradeAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATEtradeAdapterTest.class);
	static private ATEtradeAdapter _etradeAdp;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String propertiesPath = System.getProperty("test_args");
	ATConfigUtil.init(propertiesPath, true);

		ATExecutors.init();
		_etradeAdp = ATEtradeAdapter.getInstance();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	 @Disabled
	public void testGetAuthorizationUrl() throws ParseException {
		String rt = _etradeAdp.getAuthorizationUrl();
		assertTrue(rt != null);
	}

	@Test
	// @Disabled
	public void testRenewAccessToken() throws ParseException {
		_etradeAdp.renewAccessToken();
		//assertTrue(rt != null);
	}
	

}
