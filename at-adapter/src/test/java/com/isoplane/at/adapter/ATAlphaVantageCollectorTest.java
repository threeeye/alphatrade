package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import com.isoplane.at.adapter.alphavantage.ATAlphaVantageCollector;
import com.isoplane.at.adapter.alphavantage.IATAlphaVantageConstants;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAlphaVantageCollectorTest {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageCollectorTest.class);
	static private TestCollector _coll;

	static private boolean isTest = true;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String propertiesPath = System.getProperty("test_args");
		Configuration config = ATConfigUtil.init(propertiesPath, false);
		if (ATAlphaVantageCollectorTest.isTest) {
			String[] configKeys = {
					"mongo.t.foundation",
					"mongo.t.statement.blnc.alva",
					"mongo.t.statement.cash.alva",
					"mongo.t.statement.comp.alva",
					"mongo.t.statement.cons.alva",
					"mongo.t.statement.earn.alva" };
			for (String key : configKeys) {
				String testTable = String.format("%s_test", config.getString(key));
				ATConfigUtil.overrideConfiguration(key, testTable);
			}
			ATConfigUtil.overrideConfiguration("alphavantage.key", "demo");
			ATConfigUtil.overrideConfiguration("alphavantage.throttletime", 1250);
		}

		ATExecutors.init();
		ATServiceRegistry.init();
		// IATFundamentalsProvider fndSvc = new ATFundamentalsController();
		// ATServiceRegistry.setFundamentalsProvider(fndSvc, false);

		_coll = new TestCollector();
	}

	@AfterAll
	public static void tearDown() {
		log.info(String.format("%s done", ATAlphaVantageCollectorTest.class.getSimpleName()));
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	// @Disabled
	public void testProcessFundamentals() throws ParseException {
		ATSymbol symbol = new ATSymbol("IBM");
		Map<String, ATFundamental> fndMap = _coll.processFundamentals(symbol);
		assertTrue(fndMap != null && !fndMap.isEmpty());
	}

	@Test
	@Disabled
	public void testEarnings() throws ParseException {
		// String symbolStr = ATConfigUtil.getResourceFileAsString("ark_symbols.csv");
		// String[] tokens = symbolStr.split(",");
		// Collection<String> symbols = ATAlphaVantageCollectorTest.isTest
		// ? Arrays.asList(new ATS"IBM")
		// : Stream.of(tokens).map(t -> new ATSymbol(t.trim())).collect(Collectors.toCollection(TreeSet::new));
		// log.info(String.format("Loaded [%d] symbols", symbols.size()));
		ATSymbol symbol = new ATSymbol("IBM");
		_coll.collectStatements(IATAlphaVantageConstants.EARNINGS, symbol);
	}

	// @Test
	// public void testCollector() throws InterruptedException {
	// Supplier<Collection<IATSymbol>> symbolSupplier = () -> Arrays.asList(new ATSymbol("IBM"));
	// ATAlphaVantageCollector coll = new ATAlphaVantageCollector();
	// coll.run(symbolSupplier);
	// Thread.sleep(10000);
	// }


	private static class TestCollector extends ATAlphaVantageCollector {

		@Override
		public List<Map<String, Object>> collectStatements(String type_, IATSymbol symbol_) {
			return super.collectStatements(type_, symbol_);
		}
	}
}
