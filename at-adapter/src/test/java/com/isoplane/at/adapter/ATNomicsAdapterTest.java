package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.naming.ConfigurationException;

import com.isoplane.at.adapter.nomics.ATNomicsWebApiAdapter;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATNomicsAdapterTest {

    static final Logger log = LoggerFactory.getLogger(ATNomicsAdapterTest.class);
    static private TestNomicsAdapter _webApi;

    @BeforeAll
    public static void setup() throws ConfigurationException {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, true);

        ATExecutors.init();
        _webApi = new TestNomicsAdapter();// ATNomicsWebApiAdapter.getInstance();
    }

    @AfterAll
    public static void tearDown() {
        log.info(String.format("%s done", ATNomicsAdapterTest.class.getSimpleName()));
    }

    @BeforeEach
    public void prepare() {

    }

    @AfterEach
    public void cleanup() {
    }

    @Test
    // @Disabled()
    public void testPricing() {
        List<String> symbols = Arrays.asList("BTC", "ETH", "XRP");
        Map<String, ATMarketData> result = _webApi.getMarketData(symbols);
        assertTrue(result != null);
        assertEquals(symbols.size(), result.size());
        assertTrue(result.keySet().containsAll(symbols));
    }

    @Test
    public void testParsePricing() {
        String json = ATSysUtils.getResourceFileAsString("nomics_pricing.json");
        List<String> symbols = Arrays.asList("BTC", "ETH", "XRP");
        Map<String, ATMarketData> result = _webApi.parsePricing(symbols, json);
        assertTrue(result != null);
        assertEquals(symbols.size(), result.size(), String.format("Found: %s", result.keySet()));
    }


    public static class TestNomicsAdapter extends ATNomicsWebApiAdapter {

        public Map<String, ATMarketData> parsePricing(Collection<String> symbols_, String json_) {
            return super.parsePricing(symbols_, json_);
        }
    }
}
