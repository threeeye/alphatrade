package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.TreeSet;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.isoplane.at.adapter.ally.ATAllyProtoAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAllyAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATAllyAdapterTest.class);
	static private AllyTestAdapter _allyAdp;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, true);

		ATExecutors.init();
		_allyAdp = new AllyTestAdapter();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
		try {
			Thread.sleep(1000);
		} catch (Exception ex) {
			log.error(String.format("cleanup Error"), ex);
		}
	}

	@Test
	@Disabled
	public void testRequest() {
		String rt = _allyAdp.testRequest();
		assertTrue(rt != null);
	}

	@Test
	//	@Disabled
	public void testStock() {
		String[] symbols = { "AAPL", "MSFT", "FGDFX" };
		String rt = _allyAdp.testStock(symbols);
		assertTrue(rt != null);
	}

	@Test
	// @Disabled
	public void testNewsHeadlines() {
		String rt = _allyAdp.getNewsHeadlines("AAPL");// ("AAPL", "MSFT");
		assertTrue(!StringUtils.isBlank(rt));
	}

	// public void testFidelityETF() {
	// 	String rt = _allyAdp.testStock();
	// }

	@Test
	@Disabled
	public void quickTest() {
		try {
			Configuration config = ATConfigUtil.config();
			String baseUrl = config.getString("ally.url");
			String apiKey = config.getString("ally.consumer.key");
			String apiSecret = config.getString("ally.consumer.secret");
			String tokenString = config.getString("ally.oauth.token");
			String tokenSecret = config.getString("ally.oauth.token.secret");
			OAuth10aService service = new ServiceBuilder(apiKey)
					.apiSecret(apiSecret)
					.build(new DefaultApi10a() {

						@Override
						public String getRequestTokenEndpoint() {
							return String.format("%s/oauth/request_token", baseUrl);
						}

						@Override
						public String getAccessTokenEndpoint() {
							return String.format("%s/oauth/access_token", baseUrl);
						}

						@Override
						protected String getAuthorizationBaseUrl() {
							return String.format("%s/oauth/authorize", baseUrl);
						}
					});

			OAuth1AccessToken token = new OAuth1AccessToken(tokenString, tokenSecret);

			String url = String.format("%s/v1/market/news/search.json", ATConfigUtil.config().getString("ally.url"));
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.addQuerystringParameter("symbols", String.join(",", new String[] { "AAPL" }));
			service.signRequest(token, request);
			Response response = service.execute(request);
			System.out.println(response.getBody());

			// String body = super.request(Verb.GET, url, params, null, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	@Disabled
	public void testNewsDetails() {
		String rt = _allyAdp.getNewsDetails("M2379931");
		assertTrue(rt != null);
	}

	// @Test
	// public void quickTest() { OAuth10aService svc = new
	// OAuth10aService(TradeKingApi.class, "apiKey", "apiSecret", "callback",
	// "scope", "debugStream", "userAgent", "httpClientConfig", "httpClient");
	// OAuthService service = new ServiceBuilder("CONSUMER_KEY")
	// .provider(TradeKingApi.class)
	// .apiKey("CONSUMER_KEY")
	// .apiSecret("CONSUMER_SECRET")
	// .build();
	// Token accessToken = new OAuth1AccessToken("OAUTH_TOKEN",
	// "OAUTH_TOKEN_SECRET");
	// OAuthRequest request = new OAuthRequest(Verb.GET, "PROTECTED_RESOURCE_URL");
	// service.signRequest(accessToken, request);
	// }

	@Test
	@Disabled
	public void testOptions() {
		ATSymbol sym = new ATSymbol("RAD");// ("AAPL");
		TreeSet<MarketData> rt = _allyAdp.getOptionChain(sym);
		assertTrue(rt != null && rt.size() > 0);
	}

	@Test
	@Disabled
	public void testParseOptions() {
		String json = ATSysUtils.getResourceFileAsString("ally_option_chain.json");
		_allyAdp.parseOptions("TEST", json);
	}

	private static class AllyTestAdapter extends ATAllyProtoAdapter {

		private AllyTestAdapter() {
			super();
			super.init();
		}

		@Override
		public TreeSet<MarketData> parseOptions(String symbol_, String json_) {
			return super.parseOptions(symbol_, json_);
		}
	}

}
