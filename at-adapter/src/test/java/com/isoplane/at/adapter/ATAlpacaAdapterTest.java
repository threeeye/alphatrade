package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import com.isoplane.at.adapter.alpaca.ATAlpacaAdapter;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAlpacaAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATAlpacaAdapterTest.class);
	static private ATAlpacaAdapter _alpacaAdp;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String propertiesPath = System.getProperty("test_args");
		 ATConfigUtil.init(propertiesPath, true);

		// String testArgs = System.getProperty("test_args");
		// String[] propertiesPaths = testArgs.split(",");
		// _config = new CompositeConfiguration();
		// for (String path : propertiesPaths) {
		// log.info(String.format("Reading properties [%s]", path));
		// PropertiesConfiguration config = new Configurations().properties(new File(path));
		// _config.addConfiguration(config);
		// }
		// ATConfigUtil.init(testArgs, false)
		ATExecutors.init();
		_alpacaAdp = ATAlpacaAdapter.getInstance();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	// @Disabled
	public void testGetHistory() throws ParseException {
		String symbol = "AAPL";
		Date date = DateUtils.parseDate("2021-02-10 14:00", "yyyy-MM-dd HH:mm");
		ATTrade result = _alpacaAdp.getTrade(symbol, date);
		log.info(String.format("mkt: %s", result));
		assertTrue(result != null);
	}

	@Test
	public void testParseTrades() {
		String json = ATSysUtils.getResourceFileAsString("alpaca_trade.json");
		List<ATTrade> list = _alpacaAdp.parseTrade(json);
		assertTrue(list != null && list.size() == 1);
	}
	

}
