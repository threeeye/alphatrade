package com.isoplane.at.adapter;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.MapConfiguration;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ib.ATIBWrapper;

public class ATIBAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATIBAdapterTest.class);

	@Test
	@Disabled()
	public void connectionTest() throws InterruptedException {
		Map<String, Object> configMap = new HashMap<>();
		configMap.put("poolSize", 10);
		configMap.put("requestDelay", 50); //18?
		configMap.put("isActive", true);
		configMap.put("clientId", 2);
		configMap.put("ibServer", "192.168.8.66");
//		configMap.put("ibServer", "127.0.0.1");
		configMap.put("port", 4002);
		configMap.put("mktDataType", 3); // 1: Live; 2: Frozen; 3: Delayed; 4: Delayed Frozen
		configMap.put("cutoffDays", 300);

		Configuration config = new MapConfiguration(configMap);
		ATIBWrapper ibWrapper = new ATIBWrapper(config);
		ibWrapper.init();
		
		Thread.sleep(5000);
		
		ibWrapper.add("AAPL");
		ibWrapper.add("FXAIX");

		Scanner scanner = new Scanner(System.in);
		String str = scanner.nextLine();
		
		ibWrapper.disconnect();
		
		Thread.sleep(1000);

		// log.info("Opening connection...");
		// EJavaSignal readerSignal = new EJavaSignal();
		// EClientSocket clientSocket = new EClientSocket(ibWrapper, readerSignal);
		// int clientId = config.getInt("clientId");
		// String server = config.getString("ibServer"); // 192.168.8.51, 127.0.0.1
		// int port = config.getInt("port");
		// int mktDateType = config.getInt("mktDataType");
		// log.info(String.format("Connecting to [%s:%d] as client [%d]", server, port, clientId));
		// clientSocket.eConnect(server, port, clientId);
		// if (clientSocket.isConnected()) {
		// // timer.cancel();
		// log.info(String.format("Socket connected"));
		// EReader reader = new EReader(_clientSocket, _readerSignal);
		// reader.start();
		// clientSocket.reqMarketDataType(mktDateType);
		// readIB();
		// } else {
		// log.error("Not connected");
		// }

	}
}
