package com.isoplane.at.adapter;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ameritrade.ATAmeritradeWebApiAdapter;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;

public class ATAmeritradeAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATAmeritradeAdapterTest.class);
//	static private CompositeConfiguration _config;
	static private ATAmeritradeWebApiAdapter _ameriAdpt;

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String testArgs = System.getProperty("test_args");
		// String[] propertiesPaths = testArgs.split(",");
		// _config = new CompositeConfiguration();
		// for (String path : propertiesPaths) {
		// log.info(String.format("Reading properties [%s]", path));
		// PropertiesConfiguration config = new Configurations().properties(new File(path));
		// _config.addConfiguration(config);
		// }
		ATConfigUtil.init(testArgs, false);
		ATExecutors.init();
		_ameriAdpt = ATAmeritradeWebApiAdapter.getInstance();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
	}

	@Test
	@Disabled
	public void testGetFundamentals() {
		List<String> occIds = Arrays.asList("AAPL", "MSFT", "FB");
		Set<IATSymbol> symbols = occIds.stream().map(s -> new ATSymbol(s)).collect(Collectors.toSet());
		List<IATLookupMap> fnds = _ameriAdpt.getFundamentals(symbols);
		assertTrue(fnds != null);
		assertTrue(fnds.size() == occIds.size());
	}

	@Test
	// @Disabled
	public void testGetOptionChain() {
		IATSymbol symbol = new ATSymbol("AAPL");
		Object result = _ameriAdpt.getOptionChain(symbol);
		log.info(String.format("oc: %s", result));
	}

	@Test
	@Disabled
	public void testGetHistory() {
		Date minDate = DateUtils.addWeeks(new Date(), -1);
		String minDS8 = ATFormats.DATE_yyyyMMdd.get().format(minDate);
		List<String> occIds = Arrays.asList("AAPL", "MSFT", "FB");
		Map<IATSymbol, String> symbolMap = occIds.stream().collect(Collectors.toMap(s -> new ATSymbol(s), k -> minDS8));
		Object result = _ameriAdpt.getHistory(symbolMap, minDate);
		log.info(String.format("oc: %s", result));
	}

}
