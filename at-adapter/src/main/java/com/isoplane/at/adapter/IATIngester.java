package com.isoplane.at.adapter;

import java.util.Collection;
import java.util.List;

import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;

public interface IATIngester {

	List<ATTrade> readTransactions(String userId_, String path_, Collection<ATTrade> trades_, List<ATBroker> brokers_);

}