package com.isoplane.at.adapter.alphavantage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoplane.at.adapter.ATBaseHttpAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

// Rate Violation Error: {
//    "Note": "Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https://www.alphavantage.co/premium/ if you would like to target a higher API call frequency."
//}

public class ATAlphaVantageAdapter extends ATBaseHttpAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageAdapter.class);

	static public final String SOURCE_KEY = "AlVtg";
	static public final String ID = "alphavantage";

	static private ATAlphaVantageAdapter _instance;

	private boolean _isRateViolation = false;

	private ATAlphaVantageAdapter() {
		super(ID);
	}

	static public ATAlphaVantageAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATAlphaVantageAdapter();
		}
		return _instance;
	}

	public boolean isRateViolation() {
		return this._isRateViolation;
	}

	public List<Map<String, Object>> getIncomeStatement(String sym_) {
		return getStatement("INCOME_STATEMENT", sym_);
	}

	public List<Map<String, Object>> getBalanceSheet(String sym_) {
		return getStatement("BALANCE_SHEET", sym_);
	}

	public List<Map<String, Object>> getCashFlow(String sym_) {
		return getStatement("CASH_FLOW", sym_);
	}

	public List<Map<String, Object>> getCompany(String sym_) {
		final int maxcount = 50;
		int countdown = 0;
		List<Map<String, Object>> result = null;
		while (result == null && ++countdown <= maxcount) {
			String json = requestStatement(sym_, "OVERVIEW");
			Map<String, Object> parsed = parseNode(json);
			result = parsed != null ? Arrays.asList(parsed) : null;
			if (result == null) {
				log.debug(String.format("getCompany Retrying [%2d/%d - %s / %s]", countdown, maxcount, "OVERVIEW", sym_));
			}
		}
		return result;
	}

	private List<Map<String, Object>> getStatement(String type_, String sym_) {
		final int maxcount = 50;
		int countdown = 0;
		List<Map<String, Object>> parsed = null;
		while (parsed == null && ++countdown <= maxcount) {
			String json = requestStatement(sym_, type_);
			parsed = parseStatement(json);
			if (parsed == null) {
				log.debug(String.format("getStatement Retrying [%2d/%d - %s / %s]", countdown, maxcount, type_, sym_));
			}
		}
		return parsed;
	}

	private String requestStatement(String sym_, String type_) {
		Configuration config = ATConfigUtil.config();
		String urlBaseKey = String.format("%s.url", ID);
		String urlBase = config.getString(urlBaseKey);
		String apiKeyKey = String.format("%s.key", ID);
		String apiKey = config.getString(apiKeyKey);
		String url = String.format("%sfunction=%s&symbol=%s&apikey=%s", urlBase, type_, sym_, apiKey);
		String json = super.getHttp(url, null, null);
		return json;
	}

	private List<Map<String, Object>> parseStatement(String json_) {
		List<Map<String, Object>> list = new ArrayList<>();
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode symbolNode = rootNode.get("symbol");
			if (symbolNode == null || !symbolNode.isValueNode()) {
				if (checkRateViolation(json_)) {
					return null;
				}
				log.debug(String.format("parseStatement Missing"));
				return list;
			}
			String symbol = symbolNode.textValue();

			JsonNode annualNodes = rootNode.withArray("annualReports");
			List<Map<String, Object>> annualMaps = this.parseNodes(symbol, annualNodes, IATAssetConstants.PERIOD_ANNUAL);
			if (annualMaps != null) {
				list.addAll(annualMaps);
			}
			JsonNode quarterlyNodes = rootNode.withArray("quarterlyReports");
			List<Map<String, Object>> quarterlyMaps = this.parseNodes(symbol, quarterlyNodes, IATAssetConstants.PERIOD_QUARTERLY);
			if (quarterlyMaps != null) {
				list.addAll(quarterlyMaps);
			}
		} catch (Exception ex) {
			int length = json_ != null ? Math.min(json_.length(), 128) : 0;
			String detail = length > 0 ? String.format(": %s", json_.substring(0, length - 1)) : "";
			log.error(String.format("parseStatement Error%s", detail), ex);
		}
		return list;
	}

	private List<Map<String, Object>> parseNodes(String sym_, JsonNode node_, String period_) {
		List<Map<String, Object>> list = new ArrayList<>();
		Iterator<JsonNode> iNode = node_.elements();
		while (iNode.hasNext()) {
			JsonNode node = iNode.next();

			String dStr = node.get("fiscalDateEnding").textValue();
			String id = String.format("%s_%s_%s", sym_, dStr, period_);
			Map<String, Object> nodeMap = new HashMap<>();
			nodeMap.put(IATLookupMap.AT_ID, id);
			nodeMap.put(IATAssetConstants.PERIOD, period_);
			nodeMap.put(IATAssetConstants.SYMBOL, sym_);
			nodeMap.put(IATAssetConstants.SOURCE, SOURCE_KEY);

			Iterator<Entry<String, JsonNode>> fields = node.fields();
			while (fields.hasNext()) {
				Entry<String, JsonNode> field = fields.next();
				Object value = super.getNodeValue(field.getValue());
				if (value != null) {
					nodeMap.put(field.getKey(), value);
				}
			}
			list.add(nodeMap);
		}
		return list;
	}

	private Map<String, Object> parseNode(String json_) {
		Map<String, Object> nodeMap = new HashMap<>();
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode symbolNode = rootNode.get("Symbol");
			if (symbolNode == null || !symbolNode.isValueNode()) {
				if (checkRateViolation(json_)) {
					return null;
				}
				log.debug(String.format("parseNode Missing"));
				return nodeMap;
			}
			String symbol = symbolNode.textValue();
			String dStr = rootNode.get("LatestQuarter").textValue();
			String id = String.format("%s_%s", symbol, dStr);
			nodeMap.put(IATLookupMap.AT_ID, id);
			nodeMap.put(IATAssetConstants.SYMBOL, symbol);
			nodeMap.put("fiscalDateEnding", dStr);
			nodeMap.put(IATAssetConstants.SOURCE, SOURCE_KEY);
			Iterator<Entry<String, JsonNode>> fields = rootNode.fields();
			while (fields.hasNext()) {
				Entry<String, JsonNode> field = fields.next();
				Object value = super.getNodeValue(field.getValue());
				if (value != null) {
					String key = field.getKey();
					nodeMap.put(key, value);
				}
			}
			nodeMap.remove("Symbol");
		} catch (Exception ex) {
			log.error(String.format("parseNode Error"), ex);
		}
		return nodeMap;
	}

	private boolean checkRateViolation(String json_) {
		if (json_ != null && json_.contains("Thank you for using Alpha Vantage!")) {
			this._isRateViolation = true;
			this.timeout(json_);
			this._isRateViolation = false;
			return true;
		}
		return false;
	}

	private void timeout(String json_) {
		try {
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong(_throttleTimeKey, 0);
			long timeoutTime = config.getLong(_timeoutTimeKey, 0);
			// Randomization to prevent simple pattern recognition at provider
			Double tt = (timeoutTime * 0.9) + (RandomUtils.nextLong(0, Math.max(0, timeoutTime)) * 0.2);
			Date d = new Date(System.currentTimeMillis() + tt.longValue());
			String dStr = ATFormats.TIME_HH_mm_ss.get().format(d);
			log.error(String.format("Rate Violation! {period: %d}. Retrying at '%s' (%dms): %s", throttleTime, dStr, tt.longValue(), json_));
			if (timeoutTime <= 0)
				return;
			Thread.sleep(tt.longValue());
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

}
