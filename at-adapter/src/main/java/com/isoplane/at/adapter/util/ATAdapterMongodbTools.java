package com.isoplane.at.adapter.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentBridgeMap;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;

public abstract class ATAdapterMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATAdapterMongodbTools.class);

	private ATMongoDao _dao;

	public ATAdapterMongodbTools() {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		this.init();
		this._dao = new ATMongoDao();
	}

	protected abstract void init();

	protected boolean replace(String table_, Map<String, Object> data_) {
		if (data_ == null || data_.isEmpty() || StringUtils.isBlank(table_))
			return false;
		String id = IATLookupMap.getAtId(data_);
		if (StringUtils.isBlank(id))
			throw new ATException(String.format("Missing ID"));
		IATWhereQuery delQ = ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, id);
		_dao.delete(table_, delQ);
		DataBridgeMap bridgeMap = new DataBridgeMap();
		bridgeMap.putAll(data_);
		@SuppressWarnings("unchecked")
		boolean success = _dao.update(table_, IATLookupMap.AT_ID, bridgeMap);
		return success;
	}

	protected List<Map<String, Object>> load(String table_, IATWhereQuery query_) {
		if (StringUtils.isBlank(table_))
			throw new ATException(String.format("Missing table"));
		IATWhereQuery query = query_ == null ? ATPersistenceRegistry.query().empty() : query_;
		List<DataBridgeMap> statements = this._dao.query(table_, query, DataBridgeMap.class);
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> result = (List<Map<String, Object>>) (Object) statements;
		return result;
	}

//	protected List<Map<String, Object>> load(String table_, String symbol_) {
//		if (StringUtils.isBlank(table_))
//			throw new ATException(String.format("Missing table"));
//		IATWhereQuery query = StringUtils.isBlank(symbol_)
//				? ATPersistenceRegistry.query().empty()
//				: ATPersistenceRegistry.query().eq("symbol", symbol_);
//		return load(table_, query);
//	}

	protected ArrayList<Document> loadProjection(String table_, IATWhereQuery query_, String... fields) {
		IATWhereQuery query = query_ == null ? ATPersistenceRegistry.query().empty() : query_;
		ArrayList<Document> result = this._dao.queryProjection(table_, query, fields);
		return result;
	}

	public static class DataBridgeMap extends ATPersistentBridgeMap {

		private static final long serialVersionUID = 1L;

		public DataBridgeMap() {
			super(IATLookupMap.AT_ID);
		}
	}
}
