package com.isoplane.at.adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATBaseHttpAdapter {

	static final Logger log = LoggerFactory.getLogger(ATBaseHttpAdapter.class);

	private long _lastRequestTime = 0;
	private long _requestSuccessCount = 0;
	private long _requestFailureCount = 0;
	private RequestConfig _httpReqConfig;
	private String _id;
	protected String _throttleTimeKey;
	protected String _timeoutTimeKey;

	public ATBaseHttpAdapter(String id_) {
		this._id = id_;
		this._throttleTimeKey = String.format("%s.throttletime", this._id);
		this._timeoutTimeKey = String.format("%s.timeouttime", this._id);
		init();
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		String httpTimeoutKey = String.format("%s.httpTimeout", this._id);
		int httpTimeout = config.getInt(httpTimeoutKey);
		_httpReqConfig = RequestConfig.custom()
				.setConnectTimeout(httpTimeout)
				.setConnectionRequestTimeout(httpTimeout)
				.setSocketTimeout(httpTimeout)
				.build();
		log.debug(String.format("HTTP timeout [%d]", httpTimeout));
	}

	protected Object getNodeValue(JsonNode node_) {
		if (node_ == null)
			return null;
		try {
			JsonNodeType type = node_.getNodeType();
			switch (type) {
			case ARRAY:
				return node_;
			case BINARY:
				return node_.binaryValue();
			case BOOLEAN:
				return node_.booleanValue();
			case MISSING:
			case NULL:
				return null;
			case NUMBER:
				return node_.doubleValue();
			case OBJECT:
			case POJO:
			case STRING:
				String str = node_.textValue();
				if ("None".equals(str)) {
					return null;
				} else if (NumberUtils.isCreatable(str)) {
					if (str.indexOf('.') > 0) {
						return Double.parseDouble(str);
					} else {
						return Long.parseLong(str);
					}
				} else {
					return str;
				}
			default:
				return node_;
			}
		} catch (Exception ex) {
			log.error(String.format("getNodeValue Error: %s", node_), ex);
			return null;
		}
	}

	protected String getHttp(String url_, Map<String, String> headers_, Map<String, String> params_) {
		CloseableHttpResponse response = null;
		try {
			throttle();
			// Configuration config = ATConfigUtil.config();
			// long throttleTime = config.getLong(_throttleTimeKey);
			// long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			// if (deltaTime < throttleTime) {
			// Thread.sleep(throttleTime);
			// // Thread.sleep(deltaTime + 100);
			// }
			// _lastRequestTime = System.currentTimeMillis();

			URIBuilder uri = new URIBuilder(url_);
			if (params_ != null) {
				for (Entry<String, String> param : params_.entrySet()) {
					uri.addParameter(param.getKey(), param.getValue());
				}
			}

			CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(_httpReqConfig).build();
			HttpGet httpGet = new HttpGet(uri.build());
			httpGet.setHeader("Accept", "application/json");
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpGet.setHeader(header.getKey(), header.getValue());
				}
			}

			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			_requestSuccessCount++;
			return content;
		} catch (SocketTimeoutException toex) {
			_requestFailureCount++;
			log.error(String.format("ERROR: Request timed out [%s - %d/%d]: %s", url_, _requestFailureCount, _requestSuccessCount,
					url_));
			return null;
		} catch (Exception ex) {
			_requestFailureCount++;
			log.error(String.format("Error requesting: %s: %s", url_, params_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

	private void throttle() {
		try {
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong(_throttleTimeKey, 0);
			if (throttleTime > 0) {
				long deltaTime = System.currentTimeMillis() - _lastRequestTime;
				if (deltaTime < throttleTime) {
					Double tt = (throttleTime * 0.9) + (RandomUtils.nextLong(0, Math.max(0, throttleTime)) * 0.2);
					Thread.sleep(tt.longValue());
				}
			}
		} catch (Exception ex) {
			log.error(String.format("throttle Interrupted"), ex);
			// throw new ATException(ex);
		}
		_lastRequestTime = System.currentTimeMillis();
	}

	public long getThrottleTime() {
		Configuration config = ATConfigUtil.config();
		long throttleTime = config.getLong(_throttleTimeKey, 0);
		return throttleTime;
	}
}
