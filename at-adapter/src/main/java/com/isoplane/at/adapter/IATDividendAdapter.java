package com.isoplane.at.adapter;

import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATSymbol;

public interface IATDividendAdapter {

	Map<String, IATDividend> getDividends(Set<IATSymbol> symbols);
}
