package com.isoplane.at.adapter.ftserussell;

import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.IATCompanyAdapter;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATRussellAdapter implements IATCompanyAdapter {

	static final Logger log = LoggerFactory.getLogger(ATRussellAdapter.class);

	static final String SOURCE_KEY = "rssll";

	public ATRussellAdapter() {
	}

	public static void main(String[] args_) {
		String url = "https://content.ftserussell.com/sites/default/files/support_document/RU3000_MembershipList_20190701.pdf";
		// Configuration config = new BaseConfiguration();
		// config.addProperty("url.r3000", url);
		ATConfigUtil.init(null, false);
		ATConfigUtil.overrideConfiguration("url.r3000", url);
		ATRussellAdapter instance = new ATRussellAdapter();
		Map<String, String> companies = instance.getCompanies();
		int i = 0;
		for (Entry<String, String> entry : companies.entrySet()) {
			log.info(String.format("%d - %-5s: %s", i++, entry.getKey(), entry.getValue()));
		}
	}

	@Override
	public Map<String, String> getCompanies() {
		Map<String, String> companies = new TreeMap<>();
		String url = ATConfigUtil.config().getString("url.r3000");
		try (PDDocument document = PDDocument.load(new URL(url).openStream())) {
			if (!document.isEncrypted()) {
				PDFTextStripperByArea stripper = new PDFTextStripperByArea();
				stripper.setSortByPosition(true);
				PDFTextStripper tStripper = new PDFTextStripper();
				String pdfFileInText = tStripper.getText(document);

				// split by line end
				String lines[] = pdfFileInText.split("\\r?\\n");
				for (String line : lines) {
					int ll = line.length();
					if (ll != 32)
						continue;
					String name = line.substring(0, 25).trim();
					String ticker = line.substring(25).trim();
					if (ticker.length() > 6) {
						// log.info(String.format("FAILED: %-5s - %s", ticker, name));
						continue;
					}
					boolean failed = false;
					for (int j = 0; j < ticker.length(); j++) {
						char c = ticker.charAt(j);
						if ('.' != c && !Character.isUpperCase(c)) {
							failed = true;
							// log.info(String.format("FAILED: %-5s - %s", ticker, name));
							break;
						}
					}
					if (failed)
						continue;
					companies.put(ticker, name);
				}
			}
		} catch (Exception ex) {
			log.error(String.format("getPdf", ex));
		}
		return companies;
	}
}
