package com.isoplane.at.adapter.alphavantage;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.isoplane.at.commons.model.ATFundamental;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATEquityFoundation;
import com.isoplane.at.commons.model.IATFundamental;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATFundamentalsSource;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATAlphaVantageCollector implements IATFundamentalsSource, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageCollector.class);

	private ATAlphaVantageDao _mongodb;
	private ATAlphaVantageAdapter _avAdp;
	private long _processedCount = 0;
	private LoadingCache<String, Set<String>> _blacklistCache;

	public ATAlphaVantageCollector() {
		this._blacklistCache = CacheBuilder.newBuilder()
				.maximumSize(10000)
				.expireAfterWrite(24, TimeUnit.HOURS)
				.build(
						new CacheLoader<String, Set<String>>() {
							public Set<String> load(String key) {
								return new HashSet<>();
							}
						});
		this._avAdp = ATAlphaVantageAdapter.getInstance();
		this._mongodb = new ATAlphaVantageDao();

		ATSystemStatusManager.register(this);
	}

	public void shutdown() {
	}

	protected void fixStatements(String type_) {
		List<Map<String, Object>> statements = this._mongodb.loadStatements(type_, null);
		for (Map<String, Object> statement : statements) {
			log.info(String.format("Processing [%s / %s]", type_, IATLookupMap.getAtId(statement)));
			Iterator<String> iKey = statement.keySet().iterator();
			while (iKey.hasNext()) {
				String key = iKey.next();
				if ("_id".equals(key)) {
					iKey.remove();
					continue;
				}
				Object value = statement.get(key);
				if (value instanceof String) {
					String str = (String) value;
					if ("None".equals(str)) {
						iKey.remove();
					} else if (NumberUtils.isCreatable(str)) {
						if (str.indexOf('.') > 0) {
							value = Double.parseDouble(str);
						} else {
							value = Long.parseLong(str);
						}
						statement.put(key, value);
					}
				}
			}
			String atId = IATLookupMap.getAtId(statement);
			if (atId.endsWith("Y")) {
				int idx = atId.lastIndexOf('Y');
				char[] chars = atId.toCharArray();
				chars[idx] = 'A';
				atId = String.valueOf(chars);
				IATLookupMap.setAtId(statement, atId);
			}
			this._mongodb.saveStatement(type_, statement);
		}
	}

	public Map<String, ATFundamental> getFundamentals() {
		List<Map<String, Object>> data = this._mongodb.loadStatements(IATAlphaVantageConstants.CONSOLIDATED, null);
		if (data == null || data.isEmpty())
			return null;

		Map<String, ATFundamental> fndMap = new TreeMap<>();
		for (Map<String, Object> map : data) {
			ATFundamental fnd = consolidatedToFundamental(map);
			fndMap.put(fnd.getOccId(), fnd);
		}
		return fndMap;
	}

	@Override
	public Map<String, ATFundamental> processFundamentals(IATSymbol... symbols_) {
		if (symbols_ == null || symbols_.length == 0)
			return null;
		Set<String> fixedBlacklist = this.getBlacklist();
		Map<String, ATFundamental> fndMap = new TreeMap<>();
		for (IATSymbol iatSymbol : symbols_) {
			String avSym = iatSymbol.convert(ATAlphaVantageAdapter.SOURCE_KEY);
			if (avSym.startsWith("$") || fixedBlacklist.contains(avSym)) {
				log.debug(String.format("processFundamentals Skipping fixed blacklist [%s]", avSym));
				continue;
			}
			Set<String> missingTypes = this._blacklistCache.getIfPresent(avSym);
			if (missingTypes != null && missingTypes.containsAll(Arrays.asList(IATAlphaVantageConstants.STATEMENTS))) {
				log.debug(String.format("processFundamentals Skipping dynamic blacklist [%s]", avSym));
				continue;
			}
			Long updatePeriod = ATConfigUtil.config().getLong("alphavantage.updatePeriod", 604800000);
			long lastUpdate = _mongodb.getLastUpdate(IATAlphaVantageConstants.CONSOLIDATED, avSym);
			if (System.currentTimeMillis() < lastUpdate + updatePeriod) {
				log.debug(String.format("processFundamentals Skipping early update [%s]", avSym));
				continue;
			}

			Map<String, List<Map<String, Object>>> statementMap = new HashMap<>();
			for (String type : IATAlphaVantageConstants.STATEMENTS) {
				List<Map<String, Object>> list = this.collectStatements(type, iatSymbol);
				if (list == null) {
					break; // Abort symbol when hitting rate limit
				} else if (!list.isEmpty()) {
					statementMap.put(type, list);
				}
			}
			if (statementMap.isEmpty())
				continue;

			Map<String, Object> conMap = this.aggregateStatements(iatSymbol,
					statementMap.get(IATAlphaVantageConstants.COMPANY),
					statementMap.get(IATAlphaVantageConstants.EARNINGS));
			ATFundamental fnd = consolidatedToFundamental(conMap);
			if (fnd != null) {
				this._mongodb.saveStatement(IATAlphaVantageConstants.CONSOLIDATED, conMap);
				Map<String, ATFundamental> symFndMap = Collections.singletonMap(iatSymbol.getId(), fnd);
				fndMap.putAll(symFndMap);
			}
		}
		return fndMap;
	}

	private Map<String, Object> aggregateStatements(IATSymbol symbol_, List<Map<String, Object>> compStatements_,
			List<Map<String, Object>> earnStatements_) {
		if (symbol_ == null || ((compStatements_ == null || compStatements_.isEmpty()) && (earnStatements_ == null || earnStatements_.isEmpty()))) {
			return null;
		}
		String symbol = symbol_.getId();
		String avSymbol = symbol_.convert(ATAlphaVantageAdapter.SOURCE_KEY);
		String symStr = symbol.equals(avSymbol) ? symbol : String.format("%s / %s", symbol, avSymbol);
		log.debug(String.format("aggregateStatements Processing [%s]", symStr));
		Map<String, Object> conMap = new HashMap<>();
		conMap.put(IATLookupMap.AT_ID, symbol);
		conMap.put(IATAssetConstants.SOURCE, ATAlphaVantageAdapter.SOURCE_KEY);
		if (earnStatements_ != null) {
			Map<String, List<Map<String, Object>>> earnGroupedByPeriod = earnStatements_.stream()
					.collect(Collectors.groupingBy(m -> (String) m.get(IATAssetConstants.PERIOD)));
			for (String earnPeriod : earnGroupedByPeriod.keySet()) {
				List<Map<String, Object>> earnPeriodStatements = earnGroupedByPeriod.get(earnPeriod);
				Collections.sort(earnPeriodStatements, (a, b) -> {
					return IATLookupMap.getAtId(a).compareTo(IATLookupMap.getAtId(b));
				});
				boolean isAnnual = IATAssetConstants.PERIOD_ANNUAL.equals(earnPeriod);
				int revWindowSize = isAnnual ? 3 : 4;
				String revStepKey = isAnnual ? IATAssetConstants.REV_GROWTH_1Y : IATAssetConstants.REV_GROWTH_1Q;
				DescriptiveStatistics revStatsAbs = new DescriptiveStatistics();
				DescriptiveStatistics revStatsRel = new DescriptiveStatistics();
				revStatsAbs.setWindowSize(revWindowSize);
				for (Map<String, Object> earnStatement : earnPeriodStatements) {
					String dStr = (String) earnStatement.get("fiscalDateEnding");
					Object revO = earnStatement.get("totalRevenue");
					if (revO == null) {
						revO = 0.0;
					}
					double revCurrent = revO instanceof Double ? (Double) revO : new Double((Long) revO);
					revStatsAbs.addValue(revCurrent);
					Double revStepChange = null;
					Double revDeltaChange = null;
					double[] revValues = revStatsAbs.getValues();
					outer: if (revValues.length >= 2) {
						double revPrevious = revValues[revValues.length - 2];
						if (revCurrent == 0) {
							revStatsRel.addValue(revPrevious < 0 ? +1 : -1);
							break outer;
						}
						revStepChange = (revCurrent - revPrevious) / revCurrent;
						earnStatement.put(revStepKey, revStepChange);
						revStatsRel.addValue(revStepChange);
						if (revValues.length >= revWindowSize) {
							if (isAnnual) {
								revStatsAbs.replaceMostRecentValue(revStepChange);
								double revAvg = revStatsRel.getMean();
								earnStatement.put(IATAssetConstants.REV_GROWTH_AVG_3Y, revAvg);
							} else {
								double revFirst = revValues[0];
								revDeltaChange = (revCurrent - revFirst) / revCurrent;
								earnStatement.put(IATAssetConstants.REV_GROWTH_YoY, revDeltaChange);
							}
						}
					}
					if (log.isDebugEnabled()) {
						log.debug(String.format("symbols: %s / %s / %s / %.2f / %.2f / %.2f", symbol, earnPeriod, dStr, revCurrent, revStepChange,
								revDeltaChange));
					}
				}

				// Populate consolidated
				Map<String, Object> latest = earnPeriodStatements.get(earnPeriodStatements.size() - 1);
				if (isAnnual) {
					String dStr = (String) latest.get("fiscalDateEnding");
					conMap.put("date_a", dStr);
					Object rev = latest.get("totalRevenue");
					if (rev instanceof Long) {
						double revD = ((Long) rev).doubleValue();
						IATFundamental.setRevenue1Y(conMap, revD);
					}
					Object rg1 = latest.get(IATAssetConstants.REV_GROWTH_1Y);
					if (rg1 instanceof Double) {
						double rg1D = (Double) rg1;
						IATFundamental.setRevenueGrowth1Y(conMap, rg1D);
					}
					Object rg3 = latest.get(IATAssetConstants.REV_GROWTH_AVG_3Y);
					if (rg3 instanceof Double) {
						double rg3D = (Double) rg3;
						IATFundamental.setRevenueGrowthAvg3Y(conMap, rg3D);
					}
				} else {
					String dStr = (String) latest.get("fiscalDateEnding");
					conMap.put("date_q", dStr);
					Object rev = latest.get("totalRevenue");
					if (rev instanceof Long) {
						double revD = ((Long) rev).doubleValue();
						IATFundamental.setRevenue1Q(conMap, revD);
					}
					Object rg1 = latest.get(IATAssetConstants.REV_GROWTH_1Q);
					if (rg1 instanceof Double) {
						double rg1D = (Double) rg1;
						IATFundamental.setRevenueGrowth1Q(conMap, rg1D);
					}
					Object rgyy = latest.get(IATAssetConstants.REV_GROWTH_YoY);
					if (rgyy instanceof Double) {
						double rgyyD = (Double) rgyy;
						IATFundamental.setRevenueGrowthYoY(conMap, rgyyD);
					}
				}
			}
		}
		if (compStatements_ != null && !compStatements_.isEmpty()) {
			Collections.sort(compStatements_, (a, b) -> {
				return IATLookupMap.getAtId(a).compareTo(IATLookupMap.getAtId(b));
			});
			Map<String, Object> latest = compStatements_.get(compStatements_.size() - 1);
			String dStr = ((String) latest.get("fiscalDateEnding"));// .replaceAll("[^0-9]", "");
			conMap.put("date_c", dStr);
			Object ev2r = latest.get("EVToRevenue");
			if (ev2r instanceof Double) {
				double ev2rD = (Double) ev2r;
				IATFundamental.setRevenueMultiple(conMap, ev2rD);
			}
			Object peg = latest.get("PEGRatio");
			if (peg instanceof Double) {
				double pegD = (Double) peg;
				IATFundamental.setPEGrowthRatio(conMap, pegD / 100);
			}
			Object tpe = latest.get("TrailingPE");
			if (tpe instanceof Double) {
				double tpeD = (Double) tpe;
				IATFundamental.setTrailingPE(conMap, tpeD / 100);
			}
			String name = (String) latest.get("Name");
			if (name instanceof String) {
				IATEquityFoundation.setDescription(conMap, name);
			}
			String sector = (String) latest.get("Sector");
			if (sector instanceof String) {
				IATEquityFoundation.setSector(conMap, sector);
			}
			String industry = (String) latest.get("Industry");
			if (industry instanceof String) {
				IATEquityFoundation.setIndustry(conMap, industry);
			}
		}
		String dA = (String) conMap.get("date_a");
		String dC = (String) conMap.get("date_c");
		String dQ = (String) conMap.get("date_q");
		String d = (StringUtils.isBlank(dA) ? "0" : dA);
		if ((StringUtils.isBlank(dC) ? "0" : dC).compareTo(d) > 0) {
			d = dC;
		}
		if ((StringUtils.isBlank(dQ) ? "0" : dQ).compareTo(d) > 0) {
			d = dQ;
		}
		if (d.equals("0"))
			return null;
		conMap.put(IATAssetConstants.LATEST_DATE_STR, d.replaceAll("[^0-9]", ""));
		return conMap;
	}

	private ATFundamental consolidatedToFundamental(Map<String, Object> conMap_) {
		String id = conMap_ != null ? IATLookupMap.getAtId(conMap_) : null;
		if (StringUtils.isBlank(id))
			return null;
		ATFundamental fnd = new ATFundamental();
		fnd.putAll(conMap_);
		fnd.setOccId(id);
		fnd.remove(IATLookupMap.AT_ID);
		fnd.put(IATAssetConstants.SOURCE, ATAlphaVantageAdapter.SOURCE_KEY);
		return fnd;
	}

	protected List<Map<String, Object>> collectStatements(String type_, IATSymbol symbol_) {
		String avSym = symbol_.convert(ATAlphaVantageAdapter.SOURCE_KEY);
		try {
			log.debug(String.format("collectStatements Processing [%s / %s]", type_, avSym));
			List<Map<String, Object>> list;
			switch (type_) {
			case IATAlphaVantageConstants.BALANCE_SHEET:
				list = _avAdp.getBalanceSheet(avSym);
				break;
			case IATAlphaVantageConstants.CASH_FLOW:
				list = _avAdp.getCashFlow(avSym);
				break;
			case IATAlphaVantageConstants.COMPANY:
				list = _avAdp.getCompany(avSym);
				break;
			case IATAlphaVantageConstants.EARNINGS:
				list = _avAdp.getIncomeStatement(avSym);
				break;
			default:
				log.warn(String.format("collectStatements Unsupported type [%s]", type_));
				return null;
			}
			if (list != null) {
				if (!list.isEmpty()) {
					for (Map<String, Object> map : list) {
						Document d = new Document(map);
						this._mongodb.saveStatement(type_, d);
					}
				} else {
					log.debug(String.format("collectStatements Missing [%s / %s]", type_, avSym));
					Set<String> missingTypes = this._blacklistCache.getIfPresent(avSym);
					if (missingTypes == null) {
						missingTypes = new HashSet<>();
						this._blacklistCache.put(avSym, missingTypes);
					}
					missingTypes.add(type_);
					if (missingTypes.containsAll(Arrays.asList(IATAlphaVantageConstants.STATEMENTS))) {
						log.info(String.format("collectStatements Blacklisting [%s]", avSym));
					}
				}
			}
			return list;
		} catch (Exception ex) {
			log.error(String.format("collectStatements Error [%s / %s]", type_, avSym), ex);
			return null;
		}
	}

	private Set<String> getBlacklist() {
		String blStr = ATConfigUtil.config().getString("alphavantage.blacklist", "");
		List<String> tokens = Arrays.asList(blStr.split(","));
		Set<String> tokenSet = tokens.stream().map(t -> t.trim()).collect(Collectors.toSet());
		return tokenSet;
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("rateViolation", String.format("%b", this._avAdp.isRateViolation()));
		status.put("size.processed", String.format("%d", _processedCount));
		status.put("size.blacklist", String.format("%d", _blacklistCache.size()));
		status.put("size.blacklist.symbols", String.format("%s", new TreeSet<>(_blacklistCache.asMap().keySet())));
		return status;
	}

}
