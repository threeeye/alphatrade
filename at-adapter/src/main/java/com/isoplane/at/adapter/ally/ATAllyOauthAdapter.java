package com.isoplane.at.adapter.ally;

import java.util.Map;
import java.util.Map.Entry;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATConfigUtil;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAllyOauthAdapter {

    static final Logger log = LoggerFactory.getLogger(ATAllyOauthAdapter.class);

    private long _lastRequestTime = 0;
    private OAuth10aService _oauthService;
    private OAuth1AccessToken _oauthToken;

    public void init() {
        Configuration config = ATConfigUtil.config();
        String baseUrl = config.getString("ally.url");
        String apiKey = config.getString("ally.consumer.key");
        String apiSecret = config.getString("ally.consumer.secret");
        String tokenString = config.getString("ally.oauth.token");
        String tokenSecret = config.getString("ally.oauth.token.secret");
        _oauthService = new ServiceBuilder(apiKey)
                .apiSecret(apiSecret)
                .build(new DefaultApi10a() {

                    @Override
                    public String getRequestTokenEndpoint() {
                        return String.format("%s/oauth/request_token", baseUrl);
                    }

                    @Override
                    public String getAccessTokenEndpoint() {
                        return String.format("%s/oauth/access_token", baseUrl);
                    }

                    @Override
                    protected String getAuthorizationBaseUrl() {
                        return String.format("%s/oauth/authorize", baseUrl);
                    }
                });

        _oauthToken = new OAuth1AccessToken(tokenString, tokenSecret);
        log.info(String.format("Ally OAuth initialized"));
    }

    public String request(Verb verb_, String url_, Map<String, String> params_, Map<String, String> headers_,
            Map<String, String> body_) {
        try {
            throttle();
            OAuthRequest request = new OAuthRequest(verb_, url_);
            if (params_ != null) {
                for (Entry<String, String> entry : params_.entrySet()) {
                    request.addQuerystringParameter(entry.getKey(), entry.getValue());
                }
            }
            if (headers_ != null) {
                for (Entry<String, String> entry : headers_.entrySet()) {
                    request.addHeader(entry.getKey(), entry.getValue());
                }
            }
            if (body_ != null) {
                for (Entry<String, String> entry : body_.entrySet()) {
                    request.addBodyParameter(entry.getKey(), entry.getValue());
                }
            }
            _oauthService.signRequest(_oauthToken, request);
            Response response = _oauthService.execute(request);
            String body = response.getBody();
            return body;
        } catch (Exception ex_) {
            throw new ATException(String.format("request Error [%s]", url_), ex_);
        }
    }

    private void throttle() {
		try {
            String throttleTimeKey = "ally.throttletime";
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong(throttleTimeKey, 0);
			if (throttleTime > 0) {
				long deltaTime = System.currentTimeMillis() - _lastRequestTime;
				if (deltaTime < throttleTime) {
					Double tt = (throttleTime * 0.9) + (RandomUtils.nextLong(0, Math.max(0, throttleTime)) * 0.2);
					Thread.sleep(tt.longValue());
				}
			}
		} catch (Exception ex) {
			log.error(String.format("throttle Interrupted"), ex);
		}
		_lastRequestTime = System.currentTimeMillis();
	}
}
