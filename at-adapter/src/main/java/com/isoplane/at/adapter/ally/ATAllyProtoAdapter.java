package com.isoplane.at.adapter.ally;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.model.Verb;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.EATChangeOperation;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAllyProtoAdapter extends ATAllyOauthAdapter {// ATBaseOAuth10aAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAllyProtoAdapter.class);

	static private ATAllyProtoAdapter _instance;

	static public final String SOURCE_KEY = "ally";
	static public final String ID = SOURCE_KEY;

	private Map<String, String> _converterMap;

	protected ATAllyProtoAdapter() {
		//super(ID, new ATAllyApi());
	}

	static public ATAllyProtoAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATAllyProtoAdapter();
			_instance.init();
		}
		return _instance;
	}

	public void init() {
		super.init();
	}

	public String testRequest() {
		String url = String.format("%s/v1/member/profile.json", ATConfigUtil.config().getString("ally.url"));
		String body = super.request(Verb.GET, url, null, null, null);
		return body;
	}

	public String testStock(String ...symbols_) {
		String url = String.format("%s/v1/market/ext/quotes.json", ATConfigUtil.config().getString("ally.url"));
		String qStr = String.join(",", symbols_);
		Map<String, String> params = Collections.singletonMap("symbols", qStr); // "AAPL,MSFT"
		String body = super.request(Verb.GET, url, params, null, null);
		log.info(String.format("testStock: %s", body));
		return body;
	}

	public String getNewsHeadlines(String... symbols_) {
		if (symbols_ == null || symbols_.length == 0)
			return null;
		String url = String.format("%s/v1/market/news/search.json", ATConfigUtil.config().getString("ally.url"));
		Map<String, String> params = Collections.singletonMap("symbols", String.join(",", symbols_));
		String body = super.request(Verb.GET, url, params, null, null);
		log.info(String.format("getNewsHeadlines: %s", body));
		return body;
	}

	public String getNewsDetails(String id_) {
		if (StringUtils.isBlank(id_))
			return null;
		String url = String.format("%s/v1/market/news/%s.json", ATConfigUtil.config().getString("ally.url"), id_);
		// Map<String, String> params = Collections.singletonMap("id", id_);
		String body = super.request(Verb.GET, url, null, null, null);
		log.info(String.format("getNewsDetails: %s", body));
		return body;
	}

	public TreeSet<MarketData> getOptionChain(IATSymbol symbol_) {
		// String atSymbol = symbol_.getId();
		// String localSymbol = getConversion(atSymbol);// symbol_.convert(SOURCE_KEY);
		// if (localSymbol != null) {
		// log.debug(String.format("Conversion [%s -> %s]", atSymbol, localSymbol));
		// }

		String atSymbol = symbol_.getId();
		String allySymbol = symbol_.convert(SOURCE_KEY);
		String url = String.format("%s/v1/market/options/search.json", ATConfigUtil.config().getString("ally.url"));
		String dstr = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		String ystr = dstr.substring(0, 4);
		String mstr = dstr.substring(4, 6);
		Map<String, String> params = new TreeMap<>();
		params.put("symbol", allySymbol);
		params.put("query", String.format("xyear-gte:%s AND xmonth-gte:%s", ystr, mstr));
		params.put("fids", "symbol,ask,ask_time,asksz,bid,bid_time,bidsz,last,datetime,incr_vl");
		// params.put("query", "xyear-eq:2021 AND xmonth-eq:09");
		// params.put("query", "xdate-eq:20210416");
		String json = super.request(Verb.GET, url, params, null, null);
		TreeSet<MarketData> mkts = this.parseOptions(atSymbol, json);
		log.debug(String.format("getOptions [%s/%d]", allySymbol, mkts != null ? mkts.size() : 0));
		return mkts;
	}

	protected TreeSet<MarketData> parseOptions(String symbol_, String json_) {
		if (json_ == null)
			return null;
		TreeSet<MarketData> result = new TreeSet<>(new Comparator<MarketData>() {
			@Override
			public int compare(MarketData a_, MarketData b_) {
				return a_.getId().compareTo(b_.getId());
			}
		});
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode responseNode = rootNode.get("response");
			JsonNode quotesNode = responseNode.get("quotes");
			JsonNode quoteNode = quotesNode.get("quote");
			if (quoteNode == null || quoteNode.isNull() || !quoteNode.isArray()) {
				log.info(String.format("parseOptions Missing [%s]", symbol_));
				return null;
			}
			SimpleDateFormat lastDf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
			SimpleDateFormat df = ATFormats.DATE_TIME_mid.get();
			String dBaseStr = df.format(new Date()).substring(0, 10);
			for (JsonNode node : quoteNode) {
				JsonNode jSym = node.get("symbol");
				try {
					String allySym = jSym.textValue();
					String occIdPost = allySym.substring(allySym.length() - 15);
					String occId = String.format("%s%s", StringUtils.rightPad(symbol_, 6), occIdPost);
					String right = Character.toString(occId.charAt(12));
					MarketData.Builder builder = MarketData.newBuilder();
					builder.setId(occId);
					builder.setIdUl(symbol_);
					builder.setSrc(SOURCE_KEY);
					builder.setRight(right);
					builder.setActionCode(EATChangeOperation.UPDATE.name());
					// builder.set
					ATFormats.normalizeOccId(symbol_);
					JsonNode jAsk = node.get("ask");
					if (jAsk.isTextual()) {
						double px = Double.parseDouble(jAsk.textValue());
						builder.setPxAsk(px);
					}
					JsonNode jAskTs = node.get("ask_time");
					if (jAskTs.isTextual()) {
						String dstr = String.format("%s %s:00", dBaseStr, jAskTs.asText());
						Date d = df.parse(dstr);
						builder.setTsAsk(d.getTime());
					}
					JsonNode jAskSz = node.get("asksz");
					if (jAskSz.isTextual()) {
						int sz = Integer.parseInt(jAskSz.textValue());
						builder.setSzAsk(sz);
					}
					JsonNode jBid = node.get("bid");
					if (jBid.isTextual()) {
						double px = Double.parseDouble(jBid.textValue());
						builder.setPxBid(px);
					}
					JsonNode jBidTs = node.get("bid_time");
					if (jBidTs.isTextual()) {
						String dstr = String.format("%s %s:00", dBaseStr, jBidTs.asText());
						Date d = df.parse(dstr);
						builder.setTsBid(d.getTime());
					}
					JsonNode jBidSz = node.get("bidsz");
					if (jBidSz.isTextual()) {
						int sz = Integer.parseInt(jBidSz.textValue());
						builder.setSzBid(sz);
					}
					JsonNode jLastSz = node.get("incr_vl");
					if (jLastSz.isTextual()) {
						String szStr = jLastSz.textValue();
						if (!StringUtils.isBlank(szStr)) {
							int sz = Integer.parseInt(szStr);
							builder.setSzLast(sz);

							JsonNode jLast = node.get("last");
							if (jLast.isTextual()) {
								double px = Double.parseDouble(jLast.textValue());
								builder.setPxLast(px);
							}
							JsonNode jLastTs = node.get("datetime");
							if (jLastTs.isTextual()) {
								Date d = lastDf.parse(jLastTs.asText());
								builder.setTsLast(d.getTime());
							}
						} else if (log.isDebugEnabled()) {
							log.debug(String.format("parseOptions Missing ts_lst [%s]", occId));
						}
					}
					MarketData mkt = builder.build();
					result.add(mkt);
				} catch (Exception ex) {
					log.error(String.format("parseOptions Error: %s", jSym), ex);
				}
			}
		} catch (Exception ex) {
			int length = json_ != null ? Math.min(json_.length(), 128) : 0;
			String detail = length > 0 ? String.format("[%s]: %s", symbol_, json_.substring(0, length - 1)) : String.format("[%s]", symbol_);
			log.error(String.format("parseOptions Error%s", detail), ex);
		}
		return result;
	}

	private String getConversion(String atId_) {
		String conv = _converterMap != null ? _converterMap.get(atId_) : null;
		return conv;
	}

	public void setConverter(Map<String, String> map_) {
		this._converterMap = map_;
	}

	// public static class ATAllyApi extends DefaultApi10a {

	// 	private String _accessTokenEndpoint;
	// 	private String _requestTokenEndpoint;
	// 	private String _authorizationUrl;

	// 	public ATAllyApi() {
	// 	}

	// 	private void updateUrls() {
	// 		// Configuration config = ATConfigUtil.config();
	// 		String baseUrl = ATConfigUtil.config().getString("ally.url");
	// 		this._accessTokenEndpoint = String.format("%s/oauth/access_token", baseUrl);
	// 		this._requestTokenEndpoint = String.format("%s/oauth/request_token", baseUrl);
	// 		this._authorizationUrl = String.format("%s/oauth/authorize?oauth_token=%%s", baseUrl);
	// 	}

	// 	@Override
	// 	public String getAccessTokenEndpoint() {
	// 		this.updateUrls();
	// 		return this._accessTokenEndpoint;
	// 	}

	// 	@Override
	// 	public String getRequestTokenEndpoint() {
	// 		this.updateUrls();
	// 		return this._requestTokenEndpoint;
	// 	}

	// 	@Override
	// 	protected String getAuthorizationBaseUrl() {
	// 		this.updateUrls();
	// 		return this._authorizationUrl;
	// 	}

	// }

}
