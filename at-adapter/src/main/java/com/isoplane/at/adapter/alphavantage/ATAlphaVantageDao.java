package com.isoplane.at.adapter.alphavantage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.util.ATAdapterMongodbTools;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATAlphaVantageDao extends ATAdapterMongodbTools {

	static final Logger log = LoggerFactory.getLogger(ATAlphaVantageDao.class);

	private String _balanceTable;
	private String _cashflowTable;
	private String _companyTable;
	private String _consolidatedTable;
	private String _earningsTable;

	@Override
	protected void init() {
		Configuration config = ATConfigUtil.config();
		this._balanceTable = config.getString("mongo.t.statement.blnc.alva");
		this._cashflowTable = config.getString("mongo.t.statement.cash.alva");
		this._companyTable = config.getString("mongo.t.statement.comp.alva");
		this._consolidatedTable = config.getString("mongo.t.statement.cons.alva");
		this._earningsTable = config.getString("mongo.t.statement.earn.alva");
	}

	public boolean saveStatement(String type_, Map<String, Object> statement_) {
		String table = getTable(type_);
		boolean result = super.replace(table, statement_);
		return result;
	}

	public List<Map<String, Object>> loadStatements(String type_, String symbol_) {
		String table = getTable(type_);
		IATWhereQuery query = StringUtils.isBlank(symbol_)
				? ATPersistenceRegistry.query().empty()
				: ATPersistenceRegistry.query().eq("symbol", symbol_);
		List<Map<String, Object>> result = super.load(table, query);
		return result;
	}

	public Long getLastUpdate(String type_, String symbol_) {
		String table = getTable(type_);
		//log.info(String.format("getLastUpdate : %s - %s", type_, table));
		IATWhereQuery query = StringUtils.isBlank(symbol_)
				? ATPersistenceRegistry.query().empty()
				: ATPersistenceRegistry.query().eq(IATLookupMap.AT_ID, symbol_);
		ArrayList<Document> result = super.loadProjection(table, query, IATLookupMap.UPDATE_TIME);
		Long lastUpdate = result != null && !result.isEmpty() ? result.get(0).getLong(IATLookupMap.UPDATE_TIME) : null;
		return lastUpdate != null ? lastUpdate : 0L;
	}

	private String getTable(String type_) {
		String table = null;
		switch (type_) {
		case IATAlphaVantageConstants.BALANCE_SHEET:
			table = this._balanceTable;
			break;
		case IATAlphaVantageConstants.CASH_FLOW:
			table = this._cashflowTable;
			break;
		case IATAlphaVantageConstants.COMPANY:
			table = this._companyTable;
			break;
		case IATAlphaVantageConstants.CONSOLIDATED:
			table = this._consolidatedTable;
			break;
		case IATAlphaVantageConstants.EARNINGS:
			table = this._earningsTable;
			break;
		default:
			log.warn(String.format("Unsupported type [%s]", type_));
			return null;
		}
		return table;
	}

}
