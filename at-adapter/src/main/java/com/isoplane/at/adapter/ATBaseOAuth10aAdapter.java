package com.isoplane.at.adapter;

import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.httpclient.okhttp.OkHttpHttpClient;
import com.isoplane.at.commons.util.ATConfigUtil;

import okhttp3.OkHttpClient;

public class ATBaseOAuth10aAdapter {

	static final Logger log = LoggerFactory.getLogger(ATBaseOAuth10aAdapter.class);

	private long _lastRequestTime = 0;
	private long _requestSuccessCount = 0;
	private long _requestFailureCount = 0;
	private String _id;
	protected String _throttleTimeKey;
	protected String _timeoutTimeKey;

	private OAuth10aService _oauthService;
	private OAuth1AccessToken _accessToken;
	private DefaultApi10a _api;

	public ATBaseOAuth10aAdapter(String id_, DefaultApi10a api_) {
		this._id = id_;
		this._api = api_;
		this._throttleTimeKey = String.format("%s.throttletime", this._id);
		this._timeoutTimeKey = String.format("%s.timeouttime", this._id);
		init();
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		String apiKeyKey = String.format("%s.consumer.key", _id);
		String apiSecretKey = String.format("%s.consumer.secret", _id);
		String httpTimeoutKey = String.format("%s.httpTimeout", this._id);
		String apiKey = config.getString(apiKeyKey);
		String apiSecret = config.getString(apiSecretKey);
		int httpTimeout = config.getInt(httpTimeoutKey);

		OkHttpClient httpClient = new OkHttpClient.Builder()
				.connectTimeout(httpTimeout, TimeUnit.MILLISECONDS)
				.readTimeout(httpTimeout, TimeUnit.MILLISECONDS)
				.build();

		ServiceBuilder builder = new ServiceBuilder(apiKey)
				// .apiKey(apiKey)
				.apiSecret(apiSecret)
				.httpClient(new OkHttpHttpClient(httpClient));

		this._oauthService = builder.build((DefaultApi10a) this._api);
		// this._accessToken = new OAuth1AccessToken(oauthToken, oauthTokenSecret);
	}

	protected String getAuthorizationUrl() {
		try {
			OAuth1RequestToken requestToken = this._oauthService.getRequestToken();
			String tokenStr = requestToken.getToken();
			String tokenSecret = requestToken.getTokenSecret();
			requestToken = new OAuth1RequestToken(tokenStr, tokenSecret);
			String url = requestToken != null ? this._oauthService.getAuthorizationUrl(requestToken) : null;
			log.info(String.format("getAuthorizationUrl [%s / %s]: %s", tokenStr, tokenSecret, url));
			return url;
		} catch (Exception ex) {
			log.error(String.format("getAuthorizationUrl"), ex);
			return null;
		}
	}

	protected OAuth1AccessToken requestAccessToken() {
		try {
			Configuration config = ATConfigUtil.config();
			String appCodeKey = String.format("%s.app.code", _id);
			String appCode = config.getString(appCodeKey);
			// OAuth1RequestToken requestToken = this._oauthService.getRequestToken();
			// OAuth1RequestToken requestToken = new OAuth1RequestToken("b9qdfEKiZbOOtustyXRPtovtocXaev50Al7MeODDDYs3",
			// 		"PlrPBv9rc93Xl6MtosnLTZaPzqSkxIlPnWNU1OaxvUA6");
			OAuth1RequestToken requestToken = new OAuth1RequestToken("PpsfmQaUj12jQHKiDqLciQ8pRhqAtcYuZVBk/Y97R9U=",
					"Fouisfx5SpSSD40ZKSdjkiqWdhqeM6i1viVNBifmx3Q=");
			OAuth1AccessToken accessToken = this._oauthService.getAccessToken(requestToken, appCode);
			String tokenStr = accessToken.getToken();
			String tokenSecret = accessToken.getTokenSecret();
			accessToken = new OAuth1AccessToken(tokenStr, tokenSecret);
			return accessToken;
		} catch (Exception ex) {
			log.error(String.format("requestAccessToken"), ex);
			return null;
		}
	}

	protected OAuth1AccessToken renewAccessToken() {
		try {
			if (this._accessToken == null) {
				this._accessToken = requestAccessToken();
			} else {
				String baseUrl = ATConfigUtil.config().getString("ally.url");
				String url = String.format("%s/oauth/access_token", baseUrl);
//				String url = String.format("%s/oauth/renew_access_token", baseUrl);
				String response = this.request(Verb.GET, url, null, null, null);
				log.debug(String.format("renewAccessToken: %s", response));
			}
			return this._accessToken;
		} catch (Exception ex) {
			log.error(String.format("renewAccessToken"), ex);
			return null;
		}
	}

	private OAuth1AccessToken getAccessToken() {
		if (this._accessToken == null) {
			this._accessToken = requestAccessToken();
			// Configuration config = ATConfigUtil.config();
			// String oauthTokenKey = String.format("%s.oauth.token", _id);
			// String oauthTokenSecretKey = String.format("%s.oauth.token.secret", _id);
			// String oauthToken = config.getString(oauthTokenKey);
			// String oauthTokenSecret = config.getString(oauthTokenSecretKey);
			// this._accessToken = new OAuth1AccessToken(oauthToken, oauthTokenSecret);
		}
		return this._accessToken;
	}

	protected String request(Verb verb_, String url_, Map<String, String> params_, Map<String, String> headers_, Map<String, String> body_) {
		OAuthRequest request = new OAuthRequest(verb_, url_);
		try {
			throttle();
			if (params_ != null) {
				for (Entry<String, String> entry : params_.entrySet()) {
					request.addQuerystringParameter(entry.getKey(), entry.getValue());
				}
			}
			if (headers_ != null) {
				for (Entry<String, String> entry : headers_.entrySet()) {
					request.addHeader(entry.getKey(), entry.getValue());
				}
			}
			if (body_ != null) {
				for (Entry<String, String> entry : body_.entrySet()) {
					request.addBodyParameter(entry.getKey(), entry.getValue());
				}
			}
			OAuth1AccessToken accessToken = getAccessToken();
			this._oauthService.signRequest(accessToken, request);
			Response response = this._oauthService.execute(request);
			String body = response.getBody();
			return body;

		} catch (Exception ex) {
			_requestFailureCount++;
			log.error(String.format("request Error [%d/%d]: %s: %s", _requestFailureCount, _requestSuccessCount, url_, params_), ex);
			return null;
		} finally {
		}
	}

	private void throttle() {
		try {
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong(_throttleTimeKey, 0);
			if (throttleTime > 0) {
				long deltaTime = System.currentTimeMillis() - _lastRequestTime;
				if (deltaTime < throttleTime) {
					Double tt = (throttleTime * 0.9) + (RandomUtils.nextLong(0, Math.max(0, throttleTime)) * 0.2);
					Thread.sleep(tt.longValue());
				}
			}
		} catch (Exception ex) {
			log.error(String.format("throttle Interrupted"), ex);
		}
		_lastRequestTime = System.currentTimeMillis();
	}
}
