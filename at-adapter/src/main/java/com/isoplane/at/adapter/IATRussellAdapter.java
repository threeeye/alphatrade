package com.isoplane.at.adapter;

import java.util.Set;

import com.isoplane.at.commons.model.ATSymbol;

public interface IATRussellAdapter {

	Set<ATSymbol> getRussell3000();
}
