package com.isoplane.at.adapter.roicai;

import org.apache.commons.lang3.StringUtils;

public class ATRoicAiAdapter {

    protected void getExcel(String symbol_) {

    }

    protected String extractFileId(String html_) {
        final String key = "fslink\":\"";
        int startIdx = StringUtils.isBlank(html_) ? -1 : html_.indexOf(key);
        if (startIdx < 0 || startIdx + key.length() > html_.length())
            return null;
        startIdx += key.length();
        int endIdx = html_.indexOf("\"", startIdx);
        if (endIdx < 0)
            return null;
        String fileId = html_.substring(startIdx, endIdx);
        return fileId;
    }
}
