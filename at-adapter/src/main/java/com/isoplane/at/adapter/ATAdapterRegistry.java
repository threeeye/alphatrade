package com.isoplane.at.adapter;

import com.isoplane.at.adapter.ftserussell.ATRussellAdapter;
import com.isoplane.at.adapter.tradier.ATTradierDividendAdapter;
import com.isoplane.at.adapter.tradier.ATTradierOptionExpirationAdapter;
import com.isoplane.at.commons.ATException;

public class ATAdapterRegistry {

	static private boolean _isInitialized = false;
	static private IATDividendAdapter _dividendAdpt;
	static private IATOptionExpirationAdapter _optionAdpt;
	static private IATCompanyAdapter _r3000Adpt;

	static public void init() {
		if (_isInitialized)
			return;
		_isInitialized = true;

		_dividendAdpt = new ATTradierDividendAdapter();
		_optionAdpt = new ATTradierOptionExpirationAdapter();
		_r3000Adpt = new ATRussellAdapter();
	}

	static public IATDividendAdapter getDividendAdapter() {
		if (_dividendAdpt == null) {
			throw new ATException(String.format("Adapter not initialized: %s", IATDividendAdapter.class));
		}
		return _dividendAdpt;
	}

	static public IATOptionExpirationAdapter getOptionExpirationAdapter() {
		if (_optionAdpt == null) {
			throw new ATException(String.format("Adapter not initialized: %s", IATOptionExpirationAdapter.class));
		}
		return _optionAdpt;
	}

	static public IATCompanyAdapter getRussell3000Adapter() {
		if (_r3000Adpt == null) {
			throw new ATException(String.format("Adapter not initialized: %s (Russell 3000)", IATCompanyAdapter.class));
		}
		return _r3000Adpt;
	}

}
