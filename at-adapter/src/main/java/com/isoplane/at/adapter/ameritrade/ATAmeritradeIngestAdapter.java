package com.isoplane.at.adapter.ameritrade;

import java.io.FileReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import com.isoplane.at.adapter.ATBaseIngester;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.IATTrade;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAmeritradeIngestAdapter extends ATBaseIngester {

	static final Logger log = LoggerFactory.getLogger(ATAmeritradeIngestAdapter.class);

	// static final SimpleDateFormat DF = new SimpleDateFormat("MM/dd/yyyy");

	public static void main(String[] args_) {
		String path = "C:\\data\\tx\\tda_transactions_2020.csv";

		// new ATAllyIngestAdapter().readTransactions(path);
	}

	// @Override
	// public List<ATTrade> readTransactions(String userId_, String path_, Collection<ATTrade> trades_, List<ATBroker> brokers_) {
	// log.info(String.format("readTransactions Only supports Dividends"));
	// TreeMap<String, ATTrade> oldTradeMap = trades_ == null
	// ? new TreeMap<>()
	// : trades_.stream().filter(t_ -> userId_.equals(t_.getTradeUserId())).collect(
	// Collectors.toMap(t_ -> {
	// String dStr = DF.format(t_.getTsTrade());
	// String dbAction = convertDbAction(t_.getTradeType());
	// String szStr = String.format("%.4f", t_.getSzTrade());
	// String pxStr = String.format("%.4f", t_.getPxTrade());
	// String pxTotalStr = String.format("%.4f", t_.getPxTradeTotal());
	// String hash = String.format("%s, %s, %s, %s, %s, %s", dStr, dbAction, szStr, t_.getOccId(), pxStr, pxTotalStr);
	// Object refExt = t_.get(IATTrade.REFERENCE_ID);
	// if (refExt != null) {
	// hash = String.format("%s, %s", hash, refExt);
	// }
	// return hash;
	// }, Function.identity(), (a, b) -> a, TreeMap::new));
	// List<Entry<String, ATTrade>> list = new ArrayList<>(oldTradeMap.entrySet());
	// list.sort((a, b) -> a.getValue().getTsTrade2().compareTo(b.getValue().getTsTrade2()));
	// for (Entry<String, ATTrade> entry : list) {
	// log.info(String.format("DB : %s", entry.getKey()));
	// }
	//
	// TreeMap<ATTrade, Pair<String, String>> newTradeMap = readFile(userId_, path_);
	//
	// List<ATTrade> result = new ArrayList<>();
	// for (Entry<ATTrade, Pair<String, String>> entry : newTradeMap.entrySet()) {
	// String hashOld = entry.getValue().getLeft();
	// String hashNew = entry.getValue().getRight();
	// if (oldTradeMap.containsKey(hashNew) || oldTradeMap.containsKey(hashOld)) {
	// log.info(String.format("HIT : %s", hashNew));
	// } else {
	// log.info(String.format("MISS: %s", hashNew));
	// result.add(entry.getKey());
	// }
	// }
	// log.info("readTransactions Done");
	// return result;
	// }

	@Override
	protected TreeMap<String, ATTrade> readFile(String userId_, String path_, List<ATBroker> brokers_) {
		SimpleDateFormat df = getDateFormat();
		TreeMap<String, ATTrade> tradeMap = new TreeMap<>();
		try (Reader reader = new FileReader(path_);
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim())) {
			for (CSVRecord record : csvParser) {
				var first = record.get(0);
				if (!StringUtils.isBlank(first) && first.trim().startsWith("#"))
					continue;
				String date = record.get("DATE");
				if (record.size() < 12 || "***END OF FILE***".equals(date))
					break;

				String dscr = record.get("DESCRIPTION");
				String csvAction = convertCsvAction(dscr);
				if (csvAction == null)
					continue;
				String txId = record.get("TRANSACTION ID");
				String countStr = record.get("QUANTITY");
				Double count = super.parseDouble(countStr, true);
				String ticker = record.get("SYMBOL");
				String pxStr = record.get("PRICE");
				Double px = super.parseDouble(pxStr, true);

				String cmms = record.get("COMMISSION");
				String amtStr = record.get("AMOUNT");
				Double pxTotal = super.parseDouble(amtStr, true);
				String feeReg = record.get("REG FEE");
				String feeSTRdm = record.get("SHORT-TERM RDM FEE");
				String feeFndRdm = record.get("FUND REDEMPTION FEE");
				String feeDfrSale = record.get("DEFERRED SALES CHARGE");

				Date tsTrade = df.parse(date);
				// String szStr = StringUtils.isBlank(count) ? null : String.format("%.4f", Double.parseDouble(count));
				// String pxStr2 = StringUtils.isBlank(pxStr) ? null : String.format("%.4f", Double.parseDouble(pxStr));
				// Double pxTrdTotal = Double.parseDouble(amt);
				// String pxTotalStr = StringUtils.isBlank(amt) ? null : String.format("%.4f", pxTrdTotal);
				// String hashOld = String.format("%s, %s, %s, %s, %s, %s", date, csvAction, szStr, ticker, pxStr2, pxTotalStr);
				// String hashNew = String.format("%s, %s", hashOld, txId);

				ATTrade trade = new ATTrade();
				trade.setOccId(ticker);
				trade.setPxTrade(px);
				trade.setPxTradeTotal(pxTotal);
				trade.setSzTrade(count);
				trade.setTradeAccount(IATSymbol.AMERITRADE);
				trade.setTradeUserId(userId_);
				trade.setTradeType(IATTrade.DIVIDEND_IN);
				trade.setTsTrade(tsTrade);
				trade.put(IATAsset.SOURCE, String.format("ingest: %s", txId));

				String hash = super.tradeHash(df, trade);
				log.debug(String.format("FILE: %s", hash));
				tradeMap.put(hash, trade);
			}
			return tradeMap;
		} catch (Exception ex) {
			throw new ATException(String.format("Error reading '%s'", path_), ex);
		}
	}

	/**
	 * Only supports dividends for now.
	 * 
	 * @param dscr_
	 * @return
	 */
	private String convertCsvAction(String dscr_) {
		if (dscr_ == null)
			return null;
		String dscr = dscr_.toLowerCase();
		// if (dscr.startsWith("bought"))
		// return "buy";
		if (dscr.startsWith("ordinary dividend") || dscr.startsWith("qualified dividend"))
			return "div_in";
		log.debug(String.format("convertCsvAction Unsupported [%s]", dscr_));
		return null;
		// String msg = String.format("Unsupported [%s]", dscr_);
		// throw new ATException(msg);
	}

	// private String convertDbAction(int type_) {
	// switch (type_) {
	// case IATTrade.CALL_BTC:
	// case IATTrade.CALL_BTO:
	// case IATTrade.MF_IN:
	// case IATTrade.PUT_BTC:
	// case IATTrade.PUT_BTO:
	// case IATTrade.REINVEST:
	// case IATTrade.SECURITY_BTC:
	// case IATTrade.SECURITY_BTC_ASG:
	// case IATTrade.SECURITY_BTC_EXEC:
	// case IATTrade.SECURITY_BTO:
	// case IATTrade.SECURITY_BTO_ASG:
	// case IATTrade.SECURITY_BTO_EXEC:
	// return "buy";
	// case IATTrade.CALL_STC:
	// case IATTrade.CALL_STO:
	// case IATTrade.MF_OUT:
	// case IATTrade.PUT_STC:
	// case IATTrade.PUT_STO:
	// case IATTrade.SECURITY_STC:
	// case IATTrade.SECURITY_STC_ASG:
	// case IATTrade.SECURITY_STC_EXEC:
	// case IATTrade.SECURITY_STO:
	// case IATTrade.SECURITY_STO_ASG:
	// case IATTrade.SECURITY_STO_EXEC:
	// return "sell";
	// case IATTrade.DIVIDEND_IN:
	// return "div_in";
	// default:
	// String msg = String.format("Unsupported [%d]", type_);
	// throw new ATException(msg);
	// }
	// }

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("MM/dd/yyyy");
	}

}
