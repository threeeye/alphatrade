package com.isoplane.at.adapter.fidelity;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.file.Paths;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.isoplane.at.adapter.ATBaseIngester;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATTrade;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATFidelityIngestAdapter extends ATBaseIngester {

	static final Logger log = LoggerFactory.getLogger(ATFidelityIngestAdapter.class);

	// static final SimpleDateFormat DF = new SimpleDateFormat("MM/dd/yyyy");

	public static void main(String[] args_) {
		String path = "C:\\data\\tx\\TdW8PcdGC9NvNl17kebhqdf5oNX2\\FID\\fid_Accounts_History.csv";

		// new ATAllyIngestAdapter().readFile("test", path);
	}

	@Override
	protected TreeMap<String, ATTrade> readFile(String userId_, String path_, List<ATBroker> brokers_) {
		SimpleDateFormat df = getDateFormat();
		// Map<String, ATTrade> interestMap = new HashMap<>();
		CSVFormat format = CSVFormat.Builder.create()
				.setIgnoreEmptyLines(true)
				.setHeader()//"Run Date", "Account", "Action", "Symbol", "Security Description", "Security Type", "Exchange Quantity", "Exchange Currency")
				.setSkipHeaderRecord(true)
				.setIgnoreHeaderCase(true)
				.setCommentMarker('#')
				.setTrim(true)
				.build();
		TreeMap<String, ATTrade> tradeMap = new TreeMap<>();
		try (Reader reader = new FidelityReader(path_);
				CSVParser csvParser = new CSVParser(reader, format)){
			// try (Reader reader = new FidelityReader(path_);
			// CSVParser csvParser = new CSVParser(reader,
			// 		CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim().withIgnoreEmptyLines())) {

			for (CSVRecord record : csvParser) {
				String date = record.get("Run Date");
				if (record.size() < 17 || (date != null && date.startsWith("\"The data and information")))
					break;

				String act = record.get("Action");
				Integer csvAction = convertCsvAction(act);
				if (csvAction == null)
					continue;
				String acct = record.get("Account");
				String acctStr = convertCsvAccount(brokers_, acct);
				if (acctStr == null) {
					log.warn(String.format("readFile Unsupported account [%s]", acct));
					continue;
				}
				String ticker = csvAction != IATTrade.INTEREST_IN ? record.get("Symbol") : "@CASH";
				String countStr = record.get("Quantity");
				Double count = super.parseDouble(countStr, true);
				String pxStr = record.get("Price");
				Double px = super.parseDouble(pxStr, true);

				String cmms = record.get("Commission");
				String fees = record.get("Fees");
				String intr = record.get("Accrued Interest");
				String amtStr = record.get("Amount");
				Double pxTotal = super.parseDouble(amtStr, true);
				String dateSettle = record.get("Settlement Date");

				Date tsTrade = df.parse(date);

				ATTrade trade = new ATTrade();
				trade.setOccId(ticker);
				trade.setPxTrade(px);
				trade.setPxTradeTotal(pxTotal);
				trade.setSzTrade(count);
				trade.setTradeAccount(acctStr);
				trade.setTradeUserId(userId_);
				trade.setTradeType(csvAction);
				trade.setTsTrade(tsTrade);
				trade.put(IATAsset.SOURCE, String.format("ingest: %s", Paths.get(path_).getFileName()));

				this.collectTrade(df, trade, tradeMap);
				// String hash = super.tradeHash(df, trade, tradeMap.keySet());
				// log.debug(String.format("FILE: %s", hash));
				// if (IATTrade.INTEREST_IN == csvAction) {
				// ATTrade iTrade = interestMap.get(hash);
				// if (iTrade != null) {
				// trade.setPxTradeTotal(trade.getPxTradeTotal() + iTrade.getPxTradeTotal());
				// }
				// interestMap.put(hash, trade);
				// }
				// tradeMap.put(hash, trade);
			}
			return tradeMap;
		} catch (Exception ex) {
			throw new ATException(String.format("Error reading '%s'", path_), ex);
		}
	}

	@Override
	protected String formatHash(String dStr_, int type_, String szStr_, String occId_, String pxStr_,
			String pxTotalStr_, String acct_,
			Object refExt_) {
		if (type_ == IATTrade.INTEREST_IN) {
			pxTotalStr_ = null;
		}
		return super.formatHash(dStr_, type_, szStr_, occId_, pxStr_, pxTotalStr_, acct_, refExt_);
	}

	private void collectTrade(SimpleDateFormat df_, ATTrade trade_, Map<String, ATTrade> tradeMap_) {
		String hash = tradeHash(df_, trade_);
		int type = trade_.getTradeType();
		if (type != IATTrade.INTEREST_IN) {
			tradeMap_.put(hash, trade_);
			return;
		}
		ATTrade oldTrade = tradeMap_.get(hash);
		if (oldTrade == null) {
			tradeMap_.put(hash, trade_);
			return;
		}
		double pxTradeTotal = Precision.round(trade_.getPxTradeTotal() + oldTrade.getPxTradeTotal(), 4);
		trade_.setPxTradeTotal(pxTradeTotal);
		tradeMap_.remove(hash);
		collectTrade(df_, trade_, tradeMap_);
	}

	private String convertCsvAccount(List<ATBroker> brokers_, String acct_) {
		int idx = acct_.lastIndexOf(' ');
		String accountId = idx >= 0 ? acct_.substring(idx).trim() : null;
		ATBroker broker = accountId != null
				? brokers_.stream().filter(b_ -> accountId.equals(b_.getReferenceId())).findAny().orElse(null)
				: null;
		if (broker == null) {
			log.warn(String.format("Missing broker [%s]", acct_));
			return null;
		}
		return broker.getId();
	}

	/**
	 * Only supports dividends for now.
	 * 
	 * @param dscr_
	 * @return
	 */
	private Integer convertCsvAction(String dscr_) {
		if (dscr_ == null)
			return null;
		String dscr = dscr_.toUpperCase();
		if (dscr.startsWith("DIVIDEND RECEIVED"))
			return IATTrade.DIVIDEND_IN;
		if (dscr.startsWith("FEE CHARGED") && dscr.contains("ADR"))
			return IATTrade.ADR_FEE;
		if (dscr.startsWith("FOREIGN TAX PAID"))
			return IATTrade.TAX_FOREIGN;
		if (dscr.startsWith("INTEREST EARNED"))
			return IATTrade.INTEREST_IN;
		if (dscr.startsWith("REINVESTMENT"))
			return IATTrade.REINVEST;
		if (dscr.startsWith("SHORT-TERM CAP GAIN"))
			return IATTrade.CAP_GAIN_ST;

		log.debug(String.format("convertCsvAction Unsupported [%s]", dscr_));
		return null;
	}

	// NOTE: Is this necessary?
	// private int convertDbAction(int type_) {
	// switch (type_) {
	// case IATTrade.CALL_BTC:
	// case IATTrade.CALL_BTO:
	// case IATTrade.MF_IN:
	// case IATTrade.PUT_BTC:
	// case IATTrade.PUT_BTO:
	// case IATTrade.REINVEST:
	// case IATTrade.SECURITY_BTC:
	// case IATTrade.SECURITY_BTC_ASG:
	// case IATTrade.SECURITY_BTC_EXEC:
	// case IATTrade.SECURITY_BTO:
	// case IATTrade.SECURITY_BTO_ASG:
	// case IATTrade.SECURITY_BTO_EXEC:
	// return 100;
	// case IATTrade.CALL_STC:
	// case IATTrade.CALL_STO:
	// case IATTrade.MF_OUT:
	// case IATTrade.PUT_STC:
	// case IATTrade.PUT_STO:
	// case IATTrade.SECURITY_STC:
	// case IATTrade.SECURITY_STC_ASG:
	// case IATTrade.SECURITY_STC_EXEC:
	// case IATTrade.SECURITY_STO:
	// case IATTrade.SECURITY_STO_ASG:
	// case IATTrade.SECURITY_STO_EXEC:
	// return 200;
	// case IATTrade.ADR_FEE:
	// case IATTrade.CALL_ASG:
	// case IATTrade.CALL_EXEC:
	// case IATTrade.CALL_EXP:
	// case IATTrade.CAP_GAIN_LT:
	// case IATTrade.CAP_GAIN_ST:
	// case IATTrade.DIVIDEND_IN:
	// case IATTrade.IN_LEU_OF:
	// case IATTrade.INTEREST_IN:
	// case IATTrade.PUT_ASG:
	// case IATTrade.PUT_EXEC:
	// case IATTrade.PUT_EXP:
	// case IATTrade.TAX_FOREIGN:
	// return type_;
	// default:
	// String msg = String.format("Unsupported [%d]", type_);
	// throw new ATException(msg);
	// }
	// }

	public static class FidelityReader extends FileReader {

		boolean firstPass = true;

		public FidelityReader(String path_) throws FileNotFoundException {
			super(path_);
		}

		@Override
		public int read() throws IOException {
			return super.read();
		}

		@Override
		public int read(char[] cbuf) throws IOException {
			return super.read(cbuf);
		}

		@Override
		public int read(char[] cbuf, int offset, int length) throws IOException {
			if (firstPass) {
				firstPass = false;
				char[] tmpBuf = new char[cbuf.length];
				int count = super.read(tmpBuf, offset, length);
				String tmpStr = new String(tmpBuf);
				int idx = tmpStr.indexOf("Run Date");
				tmpStr = tmpStr.substring(idx);
				tmpBuf = tmpStr.toCharArray();
				System.arraycopy(tmpBuf, 0, cbuf, offset, tmpBuf.length);
				return tmpBuf.length;
			}
			return super.read(cbuf, offset, length);
		}

		@Override
		public int read(CharBuffer target) throws IOException {
			return super.read(target);
		}

	}

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("MM/dd/yyyy");
	}

}
