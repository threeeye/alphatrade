package com.isoplane.at.adapter.ally;

import java.io.FileReader;
import java.io.Reader;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ATBaseIngester;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATTrade;

/**
 * Ingest copy-paste from Ally activity web page.
 * 
 * @author miron
 */
public class ATAllyIngestAdapter extends ATBaseIngester {

	static final Logger log = LoggerFactory.getLogger(ATAllyIngestAdapter.class);

	static final SimpleDateFormat DF = new SimpleDateFormat("MM/dd/yyyy");

	public static void main(String[] args_) {
		String path = "C:\\data\\tx\\TdW8PcdGC9NvNl17kebhqdf5oNX2\\FID\\fid_Accounts_History.csv";

		// new ATAllyIngestAdapter().readFile("test", path);
	}

	// @Override
	// public List<ATTrade> readTransactions(String userId_, String path_, Collection<ATTrade> trades_, List<ATBroker> brokers_) {
	// log.info(String.format("readTransactions Only supports [ARD Fees, Dividends, Foreign Tax]"));
	// TreeMap<String, ATTrade> oldTradeMap = trades_ == null
	// ? new TreeMap<>()
	// : trades_.stream().filter(t_ -> userId_.equals(t_.getTradeUserId())).collect(
	// Collectors.toMap(t_ -> {
	// String dStr = DF.format(t_.getTsTrade());
	// int dbAction = convertDbAction(t_.getTradeType());
	// String szStr = String.format("%.4f", t_.getSzTrade());
	// String pxStr = String.format("%.4f", t_.getPxTrade());
	// String pxTotalStr = String.format("%.4f", t_.getPxTradeTotal());
	// String acct = t_.getTradeAccount();
	// String hash = String.format("%s, %d, %s, %s, %s, %s, %s", dStr, dbAction, szStr, t_.getOccId(), pxStr, pxTotalStr, acct);
	// Object refExt = t_.get(IATTrade.REFERENCE_ID);
	// if (refExt != null) {
	// hash = String.format("%s, %s", hash, refExt);
	// }
	// return hash;
	// }, Function.identity(), (a, b) -> a, TreeMap::new));
	// List<Entry<String, ATTrade>> list = new ArrayList<>(oldTradeMap.entrySet());
	// list.sort((a, b) -> a.getValue().getTsTrade2().compareTo(b.getValue().getTsTrade2()));
	// for (Entry<String, ATTrade> entry : list) {
	// log.info(String.format("DB : %s", entry.getKey()));
	// }
	//
	// TreeMap<String, ATTrade> newTradeMap = readFile(userId_, path_, brokers_);
	// List<ATTrade> result = new ArrayList<>();
	// for (Entry<String, ATTrade> entry : newTradeMap.entrySet()) {
	// String hash = entry.getKey();
	// if (oldTradeMap.containsKey(hash)) {
	// log.info(String.format("HIT : %s", hash));
	// } else {
	// log.info(String.format("MISS: %s", hash));
	// result.add(entry.getValue());
	// }
	// }
	// log.info("readTransactions Done");
	// return result;
	// }

	@Override
	protected TreeMap<String, ATTrade> readFile(String userId_, String path_, List<ATBroker> brokers_) {
		if(true)
			throw new ATException("ALLY NOT IMPLEMENTED");
		TreeMap<String, ATTrade> tradeMap = new TreeMap<>();
		try (Reader reader = new FileReader(path_);
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withDelimiter('\t').withTrim().withIgnoreEmptyLines())) {
			String acctStr = brokers_.get(0).getId();
			for (CSVRecord record : csvParser) {
				String date = record.get(0);// ("DATE");
				if (record.size() < 9)
					continue;

				String act = record.get(1);// ("ACTIVITY");
				Integer csvAction = convertCsvAction(act);
				if (csvAction == null)
					continue;
				String count = record.get(2).replace("$", "");// ("QTY");
				String ticker = record.get(3);// ("SYM");
				String dscr = record.get(4);// ("DESCRIPTION");
				String px = record.get(5).replace("$", "");// ("PRICE");
				String cmms = record.get(6).replace("$", "");// ("COMMISSION");
				String fees = record.get(7).replace("$", "");// ("FEES");
				String amt = record.get(8).replace("$", "");// ("AMOUNT");

				Date tsTrade = DF.parse(date);
				String szStr = StringUtils.isBlank(count) ? null : String.format("%.4f", Double.parseDouble(count));
				String pxStr = StringUtils.isBlank(px) ? null : String.format("%.4f", Double.parseDouble(px));
				Double pxTrdTotal = Double.parseDouble(amt);
				String pxTotalStr = StringUtils.isBlank(amt) ? null : String.format("%.4f", pxTrdTotal);
				String hash = String.format("%s, %d, %s, %s, %s, %s, %s", date, csvAction, szStr, ticker, pxStr, pxTotalStr, acctStr);

				ATTrade trade = new ATTrade();
				trade.setOccId(ticker);
				trade.setPxTradeTotal(pxTrdTotal);
				trade.setTradeType(csvAction);
				trade.setTsTrade(tsTrade);
				trade.setTradeAccount(acctStr);
				trade.setTradeUserId(userId_);
				trade.put(IATAsset.SOURCE, String.format("ingest: %s", Paths.get(path_).getFileName()));

				log.info(String.format("FILE: %s", hash));
				tradeMap.put(hash, trade);
			}
			return tradeMap;
		} catch (Exception ex) {
			throw new ATException(String.format("Error reading '%s'", path_), ex);
		}
	}

	// private String convertCsvAccount(List<ATBroker> brokers_, String acct_) {
	// int idx = acct_.lastIndexOf(' ');
	// String accountId = idx >= 0 ? acct_.substring(idx).trim() : null;
	// ATBroker broker = accountId != null ? brokers_.stream().filter(b_ -> accountId.equals(b_.getReferenceId())).findAny().orElse(null) : null;
	// if (broker == null) {
	// log.warn(String.format("Missing broker [%s]", acct_));
	// return null;
	// }
	// return broker.getId();
	// }

	/**
	 * Only supports dividends for now.
	 * 
	 * @param dscr_
	 * @return
	 */
	private Integer convertCsvAction(String dscr_) {
		if (dscr_ == null)
			return null;
		String dscr = dscr_.toUpperCase();
		// if (dscr.startsWith("bought"))
		// return "buy";
		if (dscr.startsWith("DIVIDEND"))
			return IATTrade.DIVIDEND_IN;
		// if (dscr.startsWith("FEE CHARGED") && dscr.contains("ADR"))
		// return IATTrade.ADR_FEE;
		// if (dscr.startsWith("FOREIGN TAX PAID"))
		// return IATTrade.TAX_FOREIGN;
		log.debug(String.format("convertCsvAction Unsupported [%s]", dscr_));
		return null;
		// String msg = String.format("Unsupported [%s]", dscr_);
		// throw new ATException(msg);
	}

//	// NOTE: Is this necessary?
//	private int convertDbAction(int type_) {
//		switch (type_) {
//		case IATTrade.CALL_BTC:
//		case IATTrade.CALL_BTO:
//		case IATTrade.MF_IN:
//		case IATTrade.PUT_BTC:
//		case IATTrade.PUT_BTO:
//		case IATTrade.REINVEST:
//		case IATTrade.SECURITY_BTC:
//		case IATTrade.SECURITY_BTC_ASG:
//		case IATTrade.SECURITY_BTC_EXEC:
//		case IATTrade.SECURITY_BTO:
//		case IATTrade.SECURITY_BTO_ASG:
//		case IATTrade.SECURITY_BTO_EXEC:
//			return 100;
//		case IATTrade.CALL_STC:
//		case IATTrade.CALL_STO:
//		case IATTrade.MF_OUT:
//		case IATTrade.PUT_STC:
//		case IATTrade.PUT_STO:
//		case IATTrade.SECURITY_STC:
//		case IATTrade.SECURITY_STC_ASG:
//		case IATTrade.SECURITY_STC_EXEC:
//		case IATTrade.SECURITY_STO:
//		case IATTrade.SECURITY_STO_ASG:
//		case IATTrade.SECURITY_STO_EXEC:
//			return 200;
//		case IATTrade.ADR_FEE:
//		case IATTrade.CALL_ASG:
//		case IATTrade.PUT_ASG:
//		case IATTrade.CALL_EXEC:
//		case IATTrade.PUT_EXEC:
//		case IATTrade.CALL_EXP:
//		case IATTrade.PUT_EXP:
//		case IATTrade.CAP_GAIN_LT:
//		case IATTrade.CAP_GAIN_ST:
//		case IATTrade.DIVIDEND_IN:
//		case IATTrade.IN_LEU_OF:
//		case IATTrade.INTEREST_IN:
//		case IATTrade.TAX_FOREIGN:
//			return type_;
//		default:
//			String msg = String.format("Unsupported [%d]", type_);
//			throw new ATException(msg);
//		}
//	}

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("MM/dd/yyyy");
	}


}
