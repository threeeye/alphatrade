package com.isoplane.at.adapter.ib;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ib.client.Bar;
import com.ib.client.CommissionReport;
import com.ib.client.Contract;
import com.ib.client.ContractDescription;
import com.ib.client.ContractDetails;
import com.ib.client.DeltaNeutralContract;
import com.ib.client.DepthMktDataDescription;
import com.ib.client.EClientSocket;
import com.ib.client.EJavaSignal;
import com.ib.client.EReader;
import com.ib.client.EReaderSignal;
import com.ib.client.EWrapper;
import com.ib.client.Execution;
import com.ib.client.FamilyCode;
import com.ib.client.HistogramEntry;
import com.ib.client.HistoricalTick;
import com.ib.client.HistoricalTickBidAsk;
import com.ib.client.HistoricalTickLast;
import com.ib.client.MarketDataType;
import com.ib.client.NewsProvider;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.PriceIncrement;
import com.ib.client.ScannerSubscription;
import com.ib.client.SoftDollarTier;
//import com.ib.client.TickAttr;
import com.ib.client.TickAttrib;
import com.ib.client.TickAttribBidAsk;
import com.ib.client.TickAttribLast;
import com.ib.client.Types.Right;
import com.ib.contracts.OptContract;
import com.ib.contracts.StkContract;
import com.ib.controller.Instrument;
import com.ib.controller.ScanCode;
import com.isoplane.at.commons.config.IATConfigurationListener;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSecurity;
import com.isoplane.at.commons.store.ATPersistentSecurity;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATDelayedRequestRunner;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATGenericDelayedRequest;
import com.isoplane.at.commons.util.ATRecyclingThread;
import com.isoplane.at.commons.util.ATRecyclingThreadWatcher;
import com.isoplane.at.commons.util.IATIdentifyableRunnable;

/**
 * Error codes: https://www.interactivebrokers.com/en/software/api/apiguide/tables/api_message_codes.htm
 * https://interactivebrokers.github.io/tws-api/message_codes.html#gsc.tab=0
 * 
 * @author miron
 */
public class ATIBWrapper implements EWrapper, IATConfigurationListener {

	static final Logger log = LoggerFactory.getLogger(ATIBWrapper.class);

	static final SimpleDateFormat dfHHmmss = new SimpleDateFormat("HHmmss");
	static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	static final String IS_USER_ADDED = "isUserAdded";

	private boolean _isRunning;
	private Configuration _config;
	private Timer _timer;
	private EReaderSignal _readerSignal;
	private int _reqIdCounter = -1;
	private EClientSocket _clientSocket;
	private EReader _reader;
	private Map<Integer, String> _idMap;
	private Map<String, ATIBSecurity> _stockMap;
	private Map<String, ATIBSecurity> _optionMap;
	private boolean _isActive = false;
	private ATDelayedRequestRunner _reqRunner;
	private boolean _isOptionMarketOpen;
	private boolean _isStockMarketOpen;
	private long _lastRun;
	private boolean _isConnectionStartup;
	private LinkedBlockingQueue<IATLookupMap> _outQueue;

	private ATRecyclingThreadWatcher _recycler;

	public ATIBWrapper(Configuration config_) {
		_isOptionMarketOpen = true;
		_isStockMarketOpen = true;
		_isRunning = true;
		_stockMap = Collections.synchronizedMap(new LinkedHashMap<>());
		_optionMap = Collections.synchronizedMap(new HashMap<>());

		_recycler = new ATRecyclingThreadWatcher();

		int mapSize = 100000;
		_idMap = new LinkedHashMap<Integer, String>(mapSize) {
			private static final long serialVersionUID = 1L;

			@Override
			protected boolean removeEldestEntry(Entry<Integer, String> entry) {
				return size() > mapSize;
			}
		};

		_config = config_;
		// _config.registerListener(this);

		int outQueueSize = _config.getInt("outQueueSize", 500);
		_outQueue = new LinkedBlockingQueue<>(outQueueSize);

		int poolSize = _config.getInt("poolSize");
		long reqDelay = _config.getLong("requestDelay");
		_reqRunner = new ATDelayedRequestRunner(poolSize, reqDelay, "IB", true);
		// _reqRunner.run();
	}

	public void init() {
		try {
			if (!_config.getBoolean("isActive")) {
				log.warn("Interactive Brokers connection disabled");
				return;
			}
			log.info("Initializeing Interactive Brokers connection...");
			initializeConnection();
			// String securityTableId = _config.getString("securityTableId");
			// IATWhereQuery query = ATPersistenceRegistry.query().eq(ATPersistentSecurity.TYPE, 1);
			// List<ATIBSecurity> stocks = _dao.query(securityTableId, query, ATIBSecurity.class);
			// if (stocks != null) {
			// for (ATIBSecurity stock : stocks) {
			// _stockMap.put(stock.getOccId(), stock);
			// }
			// }
			setupProcessingThreads();
		} catch (Exception ex) {
			log.error("Error initializing Interactive Brokers connection", ex);
		}
	}

	public void shutdown() {
		_isRunning = false;
		if (_timer != null) {
			_timer.cancel();
		}
		if (_recycler != null) {
			_recycler.shutdown();
		}
		disconnect();
	}

	public void disconnect() {
		if (!_config.getBoolean("isActive")) {
			// log.warn("Interactive Brokers adapter disabled");
			return;
		}
		_isActive = false;
		if (_clientSocket != null) {
			_clientSocket.eDisconnect();
		}
	}

	private void initializeConnection() {
		try {
			if (_isActive)
				return;
			_isActive = true;
			_isConnectionStartup = false;
			tryConnect();
			for (int i = 5; i > 0; i--) {
				Thread.sleep(1000);
				log.info(String.format("Starting in %d...", i));
			}
		} catch (Exception ex) {
			log.error("Error on connect", ex);
		}
	}

	private void tryConnect() {
		Timer timer = new Timer("IBTryConnect");
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				if (_isConnectionStartup) {
					timer.cancel();
					return;
				}
				try {
					_isConnectionStartup = true;
					if (_clientSocket != null && _clientSocket.isConnected()) {
						log.info("Already connected");
						timer.cancel();
						return;
					}
					log.info("Opening connection...");
					_readerSignal = new EJavaSignal();
					_clientSocket = new EClientSocket(ATIBWrapper.this, _readerSignal);
					int clientId = _config.getInt("clientId");
					String server = _config.getString("ibServer"); // 192.168.8.51, 127.0.0.1
					int port = _config.getInt("port");
					int mktDateType = _config.getInt("mktDataType");
					log.info(String.format("Connecting to [%s:%d] as client [%d]", server, port, clientId));
					_clientSocket.eConnect(server, port, clientId);
					if (_clientSocket.isConnected()) {
						timer.cancel();
						log.info(String.format("Socket connected"));
						_reader = new EReader(_clientSocket, _readerSignal);
						_reader.start();
						_clientSocket.reqMarketDataType(mktDateType);
						readIB();
					} else {
						log.error("Not connected");
					}
				} finally {
					_isConnectionStartup = false;
				}
			}
		};
		timer.schedule(task, 0, 5000);
	}

	private void readIB() {
		new Thread("IB Reader") {
			@Override
			public void run() {
				while (_isActive && _clientSocket.isConnected()) {
					_readerSignal.waitForSignal();
					try {
						_reader.processMsgs();
					} catch (Exception ex) {
						// _isActive = false;
						error("Connection broken", ex);
					}
				}
				log.warn("IB Reader disconnected");
			}
		}.start();
	}

	private void setupProcessingThreads() {
		ATRecyclingThread pt = new ATRecyclingThread("IBProcess", new Supplier<Long>() {

			@Override
			public Long get() {
				long recyclerPeriod = _config.getLong("recyclerPeriod", 5000);
				if (_config.getBoolean("isActive")) {
					try {
						int count = runProcess();
						long reqDelay = _config.getLong("requestDelay", 50) * count + recyclerPeriod;
						return reqDelay;
					} catch (Exception ex) {
						log.error("Error cycling", ex);
						return recyclerPeriod;
					}
				} else {
					log.info("Exiting IB deactivated");
					return -1L;
				}
			}
		}, 30000);
		_recycler.startThread(pt);

		ATRecyclingThread ht = new ATRecyclingThread("IBHousekeeping", new Supplier<Long>() {

			@Override
			public Long get() {
				runHousekeeping();
				return 60000L;
			}
		}, 30000);
		_recycler.startThread(ht);

		// _daoThread = new Thread(new Runnable() {
		//
		// @Override
		// public void run() {
		// while (_isRunning) {
		// try {
		// IATLookupMap data = _outQueue.take();
		// String securityTableId = _config.getString("securityTableId");
		// _dao.update(securityTableId, data);
		// } catch (Exception ex) {
		// log.error(String.format("Error processing output queue"), ex);
		// }
		// }
		// }
		// }, "IBDao");
		// _daoThread.start();
	}

	private int runProcess() {
		List<ATIBSecurity> secs = new ArrayList<>();
		try {
			boolean isIgnoreMarketHours = _config.getBoolean("isIgnoreMarketHours", false);
			// if (_isStockMarketOpen || isIgnoreMarketHours) {
			ATIBSecurity stock;
			String occId;
			synchronized (_stockMap) {
				Iterator<Entry<String, ATIBSecurity>> i = _stockMap.entrySet().iterator();
				if (!i.hasNext())
					return 0;
				do {
					Entry<String, ATIBSecurity> entry = i.next();
					occId = entry.getKey();
					stock = entry.getValue();
				} while (stock.getUnderlyingOccId() != null);
				i.remove();
				_stockMap.put(occId, stock);
			}
			if (!stock.isInitialized() || _isStockMarketOpen || isIgnoreMarketHours) {
				secs.add(stock);
			}
			if (stock.isInitialized()) {
				if (System.currentTimeMillis() - stock.getOptionUpdate() > 24 * 60 * 60 * 1000) {
					requestOptionParameters(stock);
				}
				// if (_isOptionMarketOpen || isIgnoreMarketHours) {
				Collection<ATIBSecurity> options = getOptionChain(occId);
				Double sprice = stock.getPrice();
				if (sprice != null) {
					Date cutoff = DateUtils.addDays(new Date(), _config.getInt("cutoffDays"));
					for (ATIBSecurity o : options) {
						// Note: We want to initialize despite market closed.
						if (!o.isInitialized() || _isOptionMarketOpen || isIgnoreMarketHours) {
							if ((cutoff.before(o.getExpirationDate()))
									|| (IATAssetConstants.CALL.equals(o.getOptionRight()) && o.getStrike() <= sprice)
									|| (IATAssetConstants.PUT.equals(o.getOptionRight()) && o.getStrike() >= sprice))
								continue;
							secs.add(o);
						}
					}
				}
			}
			// KEEP log.info(String.format("requestMarketData %s", secs.stream().map(e -> String.format("%-5s",
			// e.getOccId())).collect(Collectors.toList())));
			log.info(String.format("[%-5s] requestMarketData [%3d]", stock.getOccId(), secs.size()));
			for (ATIBSecurity sec : secs) {
				requestMarketData(sec);
			}
			if (!_isOptionMarketOpen && isIgnoreMarketHours && (System.currentTimeMillis() - _lastRun > 60000)) {
				log.info(String.format("Ignoring market hours [%b]", isIgnoreMarketHours));
			}
			_lastRun = System.currentTimeMillis();
		} catch (Exception ex) {
			log.error("Error", ex);
		}
		return secs.size();
	}

	private void runHousekeeping() {
		try {
			log.debug(String.format("Housekeeping"));
			if (!_clientSocket.isConnected()) {
				tryConnect();
			}
			Date now = new Date();
			String dayStr = dfDay.format(now).toLowerCase();
			// log.info("Day: " + dayStr);
			if ("sat".equals(dayStr) || "sun".equals(dayStr)) {
				_isStockMarketOpen = false;
				_isOptionMarketOpen = false;

			} else {
				boolean isIgnoreMarketHours = _config.getBoolean("isIgnoreMarketHours", false);
				String postFix = isIgnoreMarketHours ? String.format("[ignored]") : "[bypassing updates]";
				String timeStr = dfHHmmss.format(now);
				if (("210000".compareTo(timeStr) < 0 && "235959".compareTo(timeStr) > 0)
						|| ("000000".compareTo(timeStr) < 0 && "070000".compareTo(timeStr) > 0)) {
					if (_isStockMarketOpen) {
						_isStockMarketOpen = false;
						log.info(String.format("Stock  market now closed %s", postFix));
					}
				} else {
					if (!_isStockMarketOpen) {
						_isStockMarketOpen = true;
						log.info("Stock market now open");
					}
				}
				if (("163000".compareTo(timeStr) < 0 && "235959".compareTo(timeStr) > 0)
						|| ("000000".compareTo(timeStr) < 0 && "084500".compareTo(timeStr) > 0)) {
					if (_isOptionMarketOpen) {
						_isOptionMarketOpen = false;
						log.info(String.format("Option market now closed %s", postFix));
					}
				} else {
					if (!_isOptionMarketOpen) {
						_isOptionMarketOpen = true;
						log.info("Option market now open");
					}
				}
			}
		} catch (Exception ex) {
			log.error("Error running maintenance", ex);
		}
	}

	@Override
	public void configurationAlert(Set<String> changedKeys_) {
		if (changedKeys_.contains("requestDelay")) {
			long newDelay = _config.getLong("requestDelay");
			_reqRunner.setDelay(newDelay);
		}
		if (changedKeys_.contains("mktDataType")) {
			int mktDateType = _config.getInt("mktDataType", 3);
			_clientSocket.reqMarketDataType(mktDateType);
		}
	}

	private int getRequestId() {
		if (!_clientSocket.isConnected()) {
			tryConnect();
			return -1;
		}
		if (_reqIdCounter < 0) {
			_clientSocket.reqIds(-1);
			while (_reqIdCounter < 0) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		int id = _reqIdCounter++;
		return id;
	}

	public TreeSet<String> getStockIds() {
		TreeSet<String> stockIds = new TreeSet<>();
		if (!_config.getBoolean("isActive")) {
			// log.warn("Interactive Brokers adapter disabled");
			return stockIds;
		}
		synchronized (_stockMap) {
			Set<String> result = _stockMap.entrySet().stream().filter(e -> e.getValue().isInitialized()).map(e -> e.getKey())
					.collect(Collectors.toSet());
			return result == null ? new TreeSet<>() : new TreeSet<>(result);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends IATSecurity> T get(String key_) {
		if (!_config.getBoolean("isActive") || StringUtils.isBlank(key_))
			return null;
		T sec = ATPersistentSecurity.isOption(key_) ? (T) _optionMap.get(key_) : (T) _stockMap.get(key_);
		return sec;
	}

	private TreeSet<ATIBSecurity> getOptionChain(String key_) {
		if (!_config.getBoolean("isActive") || key_ == null)
			return null;
		synchronized (_optionMap) {
			Set<ATIBSecurity> result = _optionMap.entrySet().stream().filter(e -> key_.equals(e.getValue().getUnderlyingOccId()))
					.map(e -> e.getValue()).collect(Collectors.toSet()); // e.getValue().isInitialized() &&
			return result == null ? new TreeSet<>() : new TreeSet<>(result);
		}
	}

	public TreeSet<String> add(String... sym_) {
		TreeSet<String> result = new TreeSet<>();
		if (!_config.getBoolean("isActive") || sym_ == null || sym_.length == 0)
			return result;
		for (String sym : sym_) {
			sym = sym.replaceAll("[^a-zA-Z]", "");
			if (add(sym, true)) {
				result.add(sym);
			}
		}
		return result;
	}

	private boolean add(String sym_, boolean isUserAdded_) {
		if (ATPersistentSecurity.isOption(sym_)) {
			return false;
		}
		ATPersistentSecurity stock = _stockMap.get(sym_);
		if (stock == null) {
			ATIBSecurity base = new ATIBSecurity(sym_);
			base.put(IS_USER_ADDED, isUserAdded_);
			// base.initializeAsStock();
			_stockMap.put(sym_, base);
			boolean result = requestSymbolContract(sym_);
			return result;
		} else {
			stock.setAddedDate(new Date());
			// stock.setUserAdded(isUserAdded_);
			return false;
		}
	}

	// TODO: implement
	public boolean remove(String sym_) {
		if (!_config.getBoolean("isActive") || StringUtils.isBlank(sym_)) {
			return false;
		}
		return false;
	}

	public void startScanner() {
		if (!_config.getBoolean("isActive")) {
			log.warn("Interactive Brokers adapter disabled");
			return;
		}
		// _clientSocket.reqScannerParameters();
		ScannerSubscription scanner1 = new ScannerSubscription();
		scanner1.instrument(Instrument.STK.name());
		// ssub.locationCode(LocationCode.STK_US_MAJOR.name());
		scanner1.abovePrice(65);
		scanner1.scanCode(ScanCode.HIGH_OPT_IMP_VOLAT.name());
		_clientSocket.reqScannerSubscription(getRequestId(), scanner1, null, null);

		ScannerSubscription scanner2 = new ScannerSubscription();
		scanner2.instrument(Instrument.STK.name());
		// ssub.locationCode(LocationCode.STK_US_MAJOR.name());
		scanner2.abovePrice(65);
		scanner2.scanCode(ScanCode.TOP_OPT_IMP_VOLAT_GAIN.name());
		_clientSocket.reqScannerSubscription(getRequestId(), scanner2, null, null);

		log.info("Scanner started");
	}

	public void getFundamentals(String sym_) {
		ATIBSecurity stock = _stockMap.get(sym_);
		int reqId = getRequestId();
		_idMap.put(reqId, createRequestValue(sym_, "fun"));
		Contract ctr = stock.getContract();
		if (ctr != null) {
			log.debug(String.format("[%-5s] reqFundamentalData", sym_));
			_clientSocket.reqFundamentalData(reqId, ctr, "CalendarReport", null);
		}
	}

	/**
	 * Callback: {@link #symbolSamples(int, ContractDescription[])}
	 * 
	 * @param sym_
	 *            - Ticker to request
	 */
	private boolean requestSymbolContract(String sym_) {
		int reqId = getRequestId();
		if (reqId < 0) {
			return false;
		}
		_idMap.put(reqId, createRequestValue(sym_, "ctr"));

		HashMap<String, Object> reqMap = new HashMap<>();
		reqMap.put("id", reqId);
		reqMap.put("sym", sym_);
		String id = String.format("ib_%s_ctr", sym_);
		ATIBRequestObject run = new ATIBRequestObject(reqId, reqMap, id) {

			@Override
			public void run() {
				String value = (String) _reqMap.get("sym");
				log.debug(String.format("[%-5s] requestSymbolContract", value));
				_clientSocket.reqMatchingSymbols(_reqId, value);
			}
		};

		ATGenericDelayedRequest req = new ATGenericDelayedRequest(run);
		boolean result = _reqRunner.add(req);
		return result;
	}

	/**
	 * Callbacks {@link #securityDefinitionOptionalParameter(int, String, int, String, String, Set, Set)}
	 * {@link #securityDefinitionOptionalParameterEnd(int)}
	 * 
	 * @param stock_
	 */
	private void requestOptionParameters(ATIBSecurity stock_) {
		int reqId = getRequestId();
		String sym = stock_.getContract().symbol();
		log.debug(String.format("[%-5s] requestOptionParameters", sym));
		_idMap.put(reqId, createRequestValue(sym, "opt"));

		HashMap<String, Object> reqMap = new HashMap<>();
		reqMap.put("id", reqId);
		reqMap.put("sym", sym);
		reqMap.put("ctrid", stock_.getContract().conid());
		String id = String.format("ib_%s_opt", sym);
		ATIBRequestObject run = new ATIBRequestObject(reqId, reqMap, id) {

			@Override
			public void run() {
				String symbol = (String) _reqMap.get("sym");
				int ctrid = (int) _reqMap.get("ctrid");
				_clientSocket.reqSecDefOptParams(_reqId, symbol, "", "STK", ctrid);
			}
		};
		ATGenericDelayedRequest req = new ATGenericDelayedRequest(run);
		_reqRunner.add(req);
	}

	/**
	 * Callbacks:
	 * {@link #tickGeneric(int, int, double)},
	 * {@link #tickPrice(int, int, double, TickAttr)},
	 * {@link #tickSize(int, int, int)},
	 * {@link #tickString(int, int, String)},
	 * {@link #tickSnapshotEnd(int)},
	 * {@link #tickOptionComputation(int, int, double, double, double, double, double, double, double, double)}
	 * 
	 * @param sec_
	 * @return
	 */
	private boolean requestMarketData(ATIBSecurity sec_) {
		String occId = sec_ != null ? sec_.getOccId() : null;
		if (occId == null) {
			return false;
		}
		Contract ctr = sec_.getContract();
		if (ctr == null) {
			if (!sec_.isOption()) {
				requestSymbolContract(occId);
			}
			return false;
		}
		int reqId = getRequestId();
		_idMap.put(reqId, createRequestValue(occId, "mkt"));
		if (ctr.exchange() == null) {
			ctr.exchange("SMART");
		}
		HashMap<String, Object> reqMap = new HashMap<>();
		reqMap.put("id", reqId);
		reqMap.put("ctr", ctr);

		if (!sec_.isOption()) {
			sec_.getGenericTicks().add(23);
			sec_.getGenericTicks().add(24);
			int reqId2 = getRequestId();
			_idMap.put(reqId2, createRequestValue(occId, "gen"));
			reqMap.put("id2", reqId2);
			reqMap.put("ticks", "104,106");
		}

		String id = String.format("ib_%s_mkt", occId);
		ATIBRequestObject run = new ATIBRequestObject(reqId, reqMap, id) {

			@Override
			public void run() {
				int mktDateType = _config.getInt("mktDataType");
				_clientSocket.reqMarketDataType(mktDateType);
				Contract ctr = (Contract) _reqMap.get("ctr");
				_clientSocket.reqMktData(reqId, ctr, "", true, false, null);
				Integer reqId2 = (Integer) _reqMap.get("id2");
				if (reqId2 != null) {
					String ticks = (String) _reqMap.get("ticks");
					_clientSocket.reqMktData(reqId2, ctr, ticks, false, false, null);
				}
				// log.info("stock: " + ctr.symbol());
			}
		};
		ATGenericDelayedRequest req = new ATGenericDelayedRequest(run);
		_reqRunner.add(req);
		return true;
	}

	@Override
	public void tickOptionComputation(int reqId_, int field_, double impliedVol_, double delta_, double optPrice_,
			double pvDividend_, double gamma_, double vega_, double theta_, double undPrice_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATPersistentSecurity sec = occId != null ? _optionMap.get(occId) : null;
		if (sec == null) {
			log.debug(String.format("tickOptionComputation missing [%d, %s, %d, %f, %f, %f, %f, %f, %f, %f, %f]", reqId_, req, field_, impliedVol_,
					delta_, optPrice_, pvDividend_, gamma_, vega_, theta_, undPrice_));
			_clientSocket.cancelMktData(reqId_);
			return;
		}
		sec.setUpdateTime(System.currentTimeMillis());
		String key = "ib_" + field_;
		updateOptionComputation(sec, key + "iv", impliedVol_);
		updateOptionComputation(sec, key + "d", delta_);
		updateOptionComputation(sec, key + "px", optPrice_);
		updateOptionComputation(sec, key + "div", pvDividend_);
		updateOptionComputation(sec, key + "g", gamma_);
		updateOptionComputation(sec, key + "v", vega_);
		updateOptionComputation(sec, key + "t", theta_);
		updateOptionComputation(sec, key + "upx", undPrice_);
	}

	private void updateOptionComputation(ATPersistentSecurity sec_, String key_, Object newValue_) {
		Object currentValue = sec_.get(key_);
		if (currentValue == null || !currentValue.equals(newValue_)) {
			sec_.put(key_, newValue_);
			sec_.setChanged(true);
		}
	}

	/**
	 * Callback: {@link #requestMarketData(ATPersistentSecurity)}
	 */
	@Override
	public void tickString(int reqId_, int field_, String value_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATPersistentSecurity sec = ATPersistentSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			log.debug(String.format("tickString missing [%d, %s, %d, %s]", reqId_, req, field_, value_));
			return;
		}
		sec.setUpdateTime(System.currentTimeMillis());
		// String key = "ib_" + field_;
		String key;
		// Note: Consider "delayed" fields
		switch (field_) {
		case 32:
			key = IATAssetConstants.EXCH_BID;
			break;
		case 33:
			key = IATAssetConstants.EXCH_ASK;
			break;
		default:
			key = "ib_" + field_;
			break;
		}

		Object value = sec.get(key);
		if (isChanged(value, value_)) {
			sec.put(key, value_);
			sec.setChanged(true);
		}
	}

	@Override
	public void tickEFP(int reqId_, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture,
			int holdDays, String futureLastTradeDate, double dividendImpact, double dividendsToLastTradeDate) {
		log.debug("tickEFP");
	}

	@Override
	public void tickNews(int reqId_, long timeStamp_, String providerCode_, String articleId_, String headline_, String extra_) {
		log.debug("tickNews");
	}

	/**
	 * Callback: {@link #requestMarketData(ATPersistentSecurity)}
	 */
	@Override
	public void tickGeneric(int reqId_, int field_, double value_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATIBSecurity sec = ATIBSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			log.debug(String.format("tickGeneric missing [%d, %s, %d, %f]", reqId_, req, field_, value_));
			return;
		}
		sec.setUpdateTime(System.currentTimeMillis());
		String key;
		switch (field_) {
		case 23:
			key = IATAssetConstants.OPTION_HV;
			break;
		case 24:
			key = IATAssetConstants.OPTION_IV;
			break;
		default:
			key = "ib_" + field_;
			break;
		}
		Object value = sec.get(key);
		if (0 != value_ && isChanged(value, value_)) {
			sec.put(key, value_);
			sec.setChanged(true);
		}
		// log.info("generic: " + field_);
		if (field_ == 23 || field_ == 24) {
			Set<Integer> ticks = sec.getGenericTicks();
			ticks.remove(field_);
			if (ticks.isEmpty()) {
				_clientSocket.cancelMktData(reqId_);
			}
		}
	}

	/**
	 * Callback: {@link #requestMarketData(ATPersistentSecurity)}
	 */
	@Override
	public void tickPrice(int reqId_, int field_, double value_, TickAttrib attrs_) {
		if (value_ == -1)
			return;
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATPersistentSecurity sec = ATPersistentSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			log.debug(String.format("tickPrice missing [%d, %s, %d, %f, %s]", reqId_, req, field_, value_, attrs_));
			return;
		}
		sec.setUpdateTime(System.currentTimeMillis());
		String key;
		// Note: Consider "delayed" fields
		switch (field_) {
		case 1:
		case 66: // Delayed bid
			key = IATAssetConstants.PX_BID;
			break;
		case 2:
		case 67: // Delayed ask
			key = IATAssetConstants.PX_ASK;
			break;
		case 4:
		case 68: // Delayed last
			key = IATAssetConstants.PX_LAST;
			break;
		case 6:
		case 72: // Delayed high
			key = IATAssetConstants.PX_HIGH;
			break;
		case 7:
		case 73: // Delayed low
			key = IATAssetConstants.PX_LOW;
			break;
		case 9:
		case 75: // Delayed close
			key = IATAssetConstants.PX_CLOSE;
			break;
		case 14:
		case 76: // Delayed open
			key = IATAssetConstants.PX_OPEN;
			break;
		default:
			key = "ib_" + field_;
			break;
		}
		Object value = sec.get(key);
		if (isChanged(value, value_)) {
			sec.put(key, value_);
			sec.setChanged(true);
			log.info(String.format("tickPrice[%d/%d]: %.2f -> %.2f", reqId_, field_, value, value_));
		}
	}

	/**
	 * Callback: {@link #requestMarketData(ATPersistentSecurity)}
	 */
	@Override
	public void tickSize(int reqId_, int field_, int value_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATPersistentSecurity sec = ATPersistentSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			log.debug(String.format("tickSize missing [%d, %s, %d, %d]", reqId_, req, field_, value_));
			return;
		}
		sec.setUpdateTime(System.currentTimeMillis());
		String key;
		// Note: Consider "delayed" fields
		switch (field_) {
		case 0:
		case 69: // Delayed bid size
			key = IATAssetConstants.SIZE_BID;
			break;
		case 3:
		case 70: // Delayed ask size
			key = IATAssetConstants.SIZE_ASK;
			break;
		case 5:
		case 71: // Delayed last size
			key = IATAssetConstants.SIZE_LAST;
			break;
		case 8:
		case 74: // Delayed volume
			key = IATAssetConstants.VOLUME;
			break;
		default:
			key = "ib_" + field_;
			break;
		}
		Object value = sec.get(key);
		if (isChanged(value, value_)) {
			sec.put(key, value_);
			sec.setChanged(true);
		}
	}

	@Override
	public void tickReqParams(int reqId_, double minTick_, String bboExch_, int ssPerm_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATIBSecurity sec = ATIBSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			log.info(String.format("[%s] Request not found [%d, %s, %f, %s, %d]", "tickReqParams", reqId_, req, minTick_, bboExch_, ssPerm_));
			return;
		}
		sec.setMinTick(minTick_);
	}

	/**
	 * Callback: {@link #requestMarketData(ATPersistentSecurity)}
	 */
	@Override
	public void tickSnapshotEnd(int reqId_) {
		_clientSocket.cancelMktData(reqId_);
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATPersistentSecurity sec = ATPersistentSecurity.isOption(occId) ? _optionMap.get(occId) : _stockMap.get(occId);
		if (sec == null) {
			String sym = _idMap.get(reqId_);
			log.debug(String.format("tickSnapshotEnd missing [%s, %d]", sym, reqId_));
			return;
		}
		if (!sec.isOption()) {
			String volStr = (sec.getHV() != null || sec.getIV() != null) ? String.format(", volatility [%f, %f]", sec.getHV(), sec.getIV()) : "";
			log.debug(String.format("[%-5s] tickSnapshotEnd%s", occId, volStr));
		}

		if (sec.getPriceAsk() == null) {
			sec.setPriceAsk(0.0);
			sec.setChanged(true);
		}
		if (sec.getPriceBid() == null) {
			sec.setPriceBid(0.0);
			sec.setChanged(true);
		}

		sec.setInitialized(true);
		sec.setUpdateTime(System.currentTimeMillis());
		if (sec.isChanged()) {
			sec.setChanged(false);
			try {
				long outQueueTimeout = _config.getLong("outQueueTimeout", 10000);
				if (!_outQueue.offer(sec, outQueueTimeout, TimeUnit.MILLISECONDS)) {
					log.warn(String.format("Timeout scheduling update [%s]", sec));
				}
			} catch (Exception ex) {
				log.error(String.format("Error scheduling update [%s]", sec));
			}
		}
	}

	private boolean isChanged(Object o1_, Object o2_) {
		if (o1_ == null) {
			return true;
		} else if (o2_ == null) {
			return false;
		} else {
			return !o1_.equals(o2_);
		}
	}

	@Override
	public void nextValidId(int orderId_) {
		log.debug("nextValidId: " + orderId_);
		_reqIdCounter = orderId_;
	}

	@Override
	public void contractDetails(int reqId_, ContractDetails contractDetails_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		if (occId == null) {
			log.info(String.format("[%s] Request not found [%d]", "contractDetails", reqId_));
			return;
		}
		log.debug(String.format("[%-5s] contractDetails", occId));
	}

	@Override
	public void bondContractDetails(int reqId_, ContractDetails contractDetails) {
		log.debug("bondContractDetails");
	}

	@Override
	public void contractDetailsEnd(int reqId_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		if (occId == null) {
			log.info(String.format("[%s] Request not found [%d]", "contractDetailsEnd", reqId_));
			return;
		}
		log.debug(String.format("[%-5s] contractDetailsEnd", occId));
	}

	@Override
	public void scannerParameters(String xml_) {
		log.debug(String.format("scannerParameters: %s", xml_));
	}

	@Override
	public void scannerData(int reqId_, int rank_, ContractDetails contractDetails_, String distance_, String benchmark_,
			String projection_, String legsStr_) {
		String sym = contractDetails_.contract().symbol();
		log.debug(String.format("scannerData [%d, %d, %s]", reqId_, rank_, sym));
		add(sym, true);
	}

	@Override
	public void scannerDataEnd(int reqId_) {
		log.debug(String.format("scannerDataEnd [%d]", reqId_));
	}

	@Override
	public void marketDataType(int reqId_, int marketDataType_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req); // TODO: This is probably nonsense
		if (occId == null) {
			log.info(String.format("[%s] Request not found [%d]", "marketDataType", reqId_));
			return;
		}
		String type = MarketDataType.getField(marketDataType_);
		log.trace(String.format("marketDataType [%d, %-5s, %s]", reqId_, occId, type));
	}

	/**
	 * Callback: {@link #requestOptionParameters(ATPersistentSecurity)}
	 */
	@Override
	public void securityDefinitionOptionalParameter(int reqId_, String exchange_, int underlyingConId,
			String tradingClass_, String multiplier_, Set<String> expirations_, Set<Double> strikes_) {
		if (!"100".equals(multiplier_))
			return;
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATIBSecurity stock = occId != null ? _stockMap.get(occId) : null;
		if (stock == null) {
			log.info(String.format("[%s] Request not found [%d]", "securityDefinitionOptionalParameter", reqId_));
			return;
		}
		stock.getStrikes().addAll(strikes_);
		stock.getExpirations().addAll(expirations_);
		stock.getExchanges().add(exchange_);
	}

	/**
	 * Callback: {@link #requestOptionParameters(ATPersistentSecurity)}
	 */
	@Override
	public void securityDefinitionOptionalParameterEnd(int reqId_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		ATIBSecurity stock = occId != null ? _stockMap.get(occId) : null;
		if (stock == null) {
			log.info(String.format("[%s] Request not found [%d]", "securityDefinitionOptionalParameterEnd", reqId_));
		} else {
			stock.setOptionUpdate(System.currentTimeMillis());
			constructOptions(occId);
		}
	}

	private void constructOptions(String occId_) {
		ATIBSecurity stock = occId_ != null ? _stockMap.get(occId_) : null;
		if (stock == null) {
			log.info(String.format("[%s] Stock not found [%s]", "constructOptions", occId_));
			return;
		}
		log.debug(String.format("[%-5s] constructOptions %s, %S, %s", occId_, stock.getExchanges(),
				stock.getExpirations(), stock.getStrikes()));

		Date lastDate = DateUtils.addDays(new Date(), _config.getInt("cutoffDays"));
		for (String dateStr : stock.getExpirations()) {
			try {
				Date date = ATFormats.DATE_yyyyMMdd.get().parse(dateStr);
				if (date.after(lastDate))
					continue;
				for (Double strike : stock.getStrikes()) {
					for (Right right : new Right[] { Right.Call, Right.Put }) {
						String optionOccId = ATFormats.toOccId(occId_, date, right.getApiString(), strike);
						// String.format("%-6s%s%s%08.0f", occId_, dateStr.substring(2), right.getApiString(), strike * 1000);
						if (_optionMap.containsKey(optionOccId))
							continue;

						Contract contract = new OptContract(occId_, dateStr, strike, right.getApiString());
						contract.multiplier("100");

						ATIBSecurity opt = new ATIBSecurity(optionOccId);
						opt.setUnderlyingOccId(occId_);
						opt.setOptionRight(right.getApiString());
						opt.setExpirationDate(date);
						opt.setExpirationDateStr(dateStr);
						opt.setStrike(strike);
						opt.setContract(contract);
						opt.setUpdateTime(System.currentTimeMillis());
						_optionMap.put(optionOccId, opt);

						requestMarketData(opt);
					}
				}
			} catch (ParseException ex) {
				log.debug(String.format("Invalid date [%s]", dateStr));
			}
		}
	}

	private void error(String msg_, Exception ex_) {
		log.error(msg_, ex_);
	}

	@Override
	public void error(Exception ex_) {
		if (!_clientSocket.isConnected()) {
			log.error("Lost connection");
		}
		log.error("Error", ex_);
	}

	@Override
	public void error(String str_) {
		log.error(String.format("Error: ", str_));
	}

	// https://interactivebrokers.github.io/tws-api/message_codes.html#gsc.tab=0
	@Override
	public void error(int reqId_, int errorCode_, String errorMsg_) {
		// Error[200] 15223 / No security definition has been found for the request
		// Error[200] 5530 / No security definition has been found for the request
		// Error[322] Server error when processing an API client request (such as duplicate ticker id)
		// Error[2108] -1 / Market data farm connection is inactive but should be available upon demand.usopt
		// Error[507] -1 / Bad Message Length null
		// Error[300] 9 / Can't find EId with tickerId:9
		// Error[100] 2 / Max rate of messages per second has been exceeded:max=50 rec=8366 (3)

		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		if (occId == null) {
			// log.info(String.format("Error [%d] Request not found [%d] - %s", "error", errorCode_, reqId_, errorMsg_));
			// return;
		}
		switch (errorCode_) {
		case 0:
			log.error(String.format("Warning [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		case 101: // Max number of tickers has been reached
			_clientSocket.cancelMktData(reqId_);
			return;
		case 200: // No security definition found (for example unavailable options)
			if (occId != null) {
				if (ATPersistentSecurity.isOption(occId)) {
					_optionMap.remove(occId);
				} else {
					_stockMap.remove(occId);
					log.warn(String.format("Removing unknown security [%s] - %s", occId, errorMsg_));
				}
			}
			return;
		case 300:
			log.trace(String.format("Unknown mkt data subscription cancel [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		case 321: // Validation error
			log.error(String.format("Server error [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			break;
		case 322:
			// log.error(String.format("Server error[%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			break;
		case 430:
			log.error(String.format("Fundamentals error [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		case 501: // Already connected
			log.trace(String.format("Error[%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			break;
		case 502:
			log.error(String.format("Couldn't connect [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			break;
		// case 504: // Not connected
		// case 507: // Bad message length
		// log.error(String.format("Error[%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
		// tryConnect();
		// break;
		case 532:
			log.error(String.format("Fundamentals error [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		// | Error[532] 2/AAPL--fun - Request Fundamental Data Sending Error - java.lang.NullPointerException
		case 100:
		case 1100: // Connectivity
		case 2107:
		case 2108:
			log.debug(String.format("Warn[%d] %s", errorCode_, errorMsg_));
			return;
		// case 200:
		// abortMessage = "No security definition found";
		case 2104:
		case 2106:
		case 2119:
			log.info(String.format("Info[%d] %d, %s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		case 10090:
			log.error(String.format("Subscription error [%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			return;
		default:
			log.error(String.format("Error[%d] %3d/%-4s - %s", errorCode_, reqId_, req, errorMsg_));
		}
		if (!_clientSocket.isConnected()) {
			tryConnect();
		} else {
			log.info("Connection alive");
		}
	}

	@Override
	public void connectionClosed() {
		log.info("connectionClosed");
	}

	@Override
	public void connectAck() {
		if (_clientSocket.isAsyncEConnect()) {
			log.info("Asynchronous connection establisched");
			_clientSocket.startAPI();
		} else {
			log.info("Synchronous connection established");
		}
	}

	/**
	 * Callback from {@link #requestSymbolContract(String)}
	 */
	@Override
	public void symbolSamples(int reqId_, ContractDescription[] cdscs_) {
		String req = _idMap.get(reqId_);
		String occId = getRequestSymbol(req);
		if (occId == null) {
			log.info(String.format("[%s] Request not found [%d]", "symbolSamples", reqId_));
			return;
		}
		for (ContractDescription cdsc : cdscs_) {
			Contract ctr = cdsc.contract();
			List<String> drv = Arrays.asList(cdsc.derivativeSecTypes());
			if (ctr == null || !occId.equals(ctr.symbol()) || !"USD".equals(cdsc.contract().currency()) || !"STK".equals(ctr.getSecType())
					|| !drv.contains("OPT"))
				// || !exch.contains(ctr.primaryExch()))
				continue;

			Contract newCtr = new StkContract(ctr.symbol());
			newCtr.conid(ctr.conid());
			// newCtr.symbol(ctr.symbol());
			// newCtr.exchange("SMART");

			ATIBSecurity prs = _stockMap.get(occId);
			if (prs == null) {
				prs = new ATIBSecurity(occId);
				_stockMap.put(occId, prs);
			}
			prs.setContract(newCtr);
			prs.setUpdateTime(System.currentTimeMillis());
			log.debug(String.format("[%-5s] symbolSamples", occId));
		}
	}

	private String createRequestValue(String sym_, String type_) {
		String str = String.format("%s--%s", sym_, type_);
		return str;
	}

	private String getRequestSymbol(String value_) {
		if (StringUtils.isBlank(value_))
			return null;
		int idx = value_.indexOf("--");
		String str = value_.substring(0, idx);
		return str;
	}

	@Override
	public void tickByTickAllLast(int arg0, int arg1, long arg2, double arg3, int arg4, TickAttribLast arg5, String arg6, String arg7) {
		// _clientSocket.reqTickByTickData(arg0, arg1, arg2);
		log.debug(String.format("tickByTickAllLast"));
	}

	@Override
	public void tickByTickBidAsk(int arg0, long arg1, double arg2, double arg3, int arg4, int arg5, TickAttribBidAsk arg6) {
		log.debug(String.format("tickByTickBidAsk"));
	}

	@Override
	public void tickByTickMidPoint(int arg0, long arg1, double arg2) {
		log.debug(String.format("tickByTickMidPoint"));
	}

	@Override
	public void familyCodes(FamilyCode[] arg0) {
		log.debug("familyCodes");
	}

	@Override
	public void headTimestamp(int arg0, String arg1) {
		log.debug("headTimestamp");
	}

	@Override
	public void histogramData(int arg0, List<HistogramEntry> arg1) {
		log.debug("histogramData");
	}

	@Override
	public void historicalData(int arg0, Bar arg1) {
		log.debug("historicalData");
	}

	@Override
	public void historicalDataEnd(int arg0, String arg1, String arg2) {
		log.debug("historicalDataEnd");
	}

	@Override
	public void historicalDataUpdate(int arg0, Bar arg1) {
		log.debug("historicalDataUpdate");
	}

	@Override
	public void historicalNews(int arg0, String arg1, String arg2, String arg3, String arg4) {
		log.debug("historicalNews");
	}

	@Override
	public void historicalNewsEnd(int arg0, boolean arg1) {
		log.debug("historicalNewsEnd");
	}

	@Override
	public void historicalTicks(int arg0, List<HistoricalTick> arg1, boolean arg2) {
		log.debug("historicalTicks");
	}

	@Override
	public void historicalTicksBidAsk(int arg0, List<HistoricalTickBidAsk> arg1, boolean arg2) {
		log.debug("historicalTicksBidAsk");
	}

	@Override
	public void historicalTicksLast(int arg0, List<HistoricalTickLast> arg1, boolean arg2) {
		log.debug("historicalTicksLast");
	}

	@Override
	public void marketRule(int arg0, PriceIncrement[] arg1) {
		log.debug("marketRule");
	}

	@Override
	public void mktDepthExchanges(DepthMktDataDescription[] arg0) {
		log.debug("mktDepthExchanges");
	}

	@Override
	public void newsArticle(int arg0, int arg1, String arg2) {
		log.debug("newsArticle");
	}

	@Override
	public void newsProviders(NewsProvider[] arg0) {
		log.debug("newsProviders");
	}

	@Override
	public void orderStatus(int arg0, String arg1, double arg2, double arg3, double arg4, int arg5, int arg6, double arg7, int arg8, String arg9,
			double arg10) {
		log.debug("orderStatus");
	}

	@Override
	public void pnl(int arg0, double arg1, double arg2, double arg3) {
		log.debug("pnl");
	}

	@Override
	public void pnlSingle(int arg0, int arg1, double arg2, double arg3, double arg4, double arg5) {
		log.debug("pnlSingle");
	}

	@Override
	public void rerouteMktDataReq(int arg0, int arg1, String arg2) {
		log.debug(String.format("rerouteMktDataReq %d %d %s", arg0, arg1, arg2));
	}

	@Override
	public void rerouteMktDepthReq(int arg0, int arg1, String arg2) {
		log.debug("rerouteMktDepthReq");
	}

	@Override
	public void smartComponents(int arg0, Map<Integer, Entry<String, Character>> arg1) {
		log.debug("smartComponents");
	}

	@Override
	public void commissionReport(CommissionReport commissionReport) {
		log.debug(String.format("commissionReport"));
	}

	@Override
	public void position(String account_, Contract contract, double pos, double avgCost) {
		log.debug(String.format("position [%d]", account_));
	}

	@Override
	public void positionEnd() {
		log.debug(String.format("positionEnd"));
	}

	@Override
	public void accountSummary(int reqId_, String account, String tag, String value, String currency) {
		log.debug("accountSummary");
	}

	@Override
	public void accountSummaryEnd(int reqId_) {
		log.debug("accountSummaryEnd");
	}

	@Override
	public void verifyMessageAPI(String apiData) {
		log.debug("verifyMessageAPI");
	}

	@Override
	public void verifyCompleted(boolean isSuccessful, String errorText) {
		log.debug("verifyCompleted");
	}

	@Override
	public void verifyAndAuthMessageAPI(String apiData, String xyzChallange) {
		log.debug("verifyAndAuthMessageAPI");
	}

	@Override
	public void verifyAndAuthCompleted(boolean isSuccessful, String errorText) {
		log.debug(String.format("verifyAndAuthCompleted[%b] %s", isSuccessful, errorText));
	}

	@Override
	public void displayGroupList(int reqId_, String groups) {
		log.debug("displayGroupList");
	}

	@Override
	public void displayGroupUpdated(int reqId_, String contractInfo) {
		log.debug("displayGroupUpdated");
	}

	@Override
	public void positionMulti(int reqId_, String account, String modelCode, Contract contract, double pos,
			double avgCost) {
		log.debug("positionMulti");
	}

	@Override
	public void positionMultiEnd(int reqId_) {
		log.debug("positionMultiEnd");
	}

	@Override
	public void accountUpdateMulti(int reqId_, String account, String modelCode, String key, String value,
			String currency) {
		log.debug("accountUpdateMulti");
	}

	@Override
	public void accountUpdateMultiEnd(int reqId_) {
		log.debug("accountUpdateMultiEnd");
	}

	@Override
	public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
		log.debug("openOrder");
	}

	@Override
	public void openOrderEnd() {
		log.debug("openOrderEnd");
	}

	@Override
	public void updateAccountValue(String key, String value, String currency, String accountName) {
		log.debug("updateAccountValue");
	}

	@Override
	public void updatePortfolio(Contract contract, double position, double marketPrice, double marketValue,
			double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {
		log.debug("updatePortfolio");
	}

	@Override
	public void updateAccountTime(String timeStamp) {
		log.debug("updateAccountTime");
	}

	@Override
	public void accountDownloadEnd(String accountName) {
		log.debug("accountDownloadEnd");
	}

	@Override
	public void realtimeBar(int reqId_, long time, double open, double high, double low, double close, long volume,
			double wap, int count) {
		log.debug(String.format("realtimeBar [%d]", reqId_));
	}

	@Override
	public void currentTime(long time_) {
		log.debug(String.format("currentTime [%d]", time_));
	}

	@Override
	public void fundamentalData(int reqId_, String data_) {
		log.debug(String.format("fundamentalData [%d]: %s", reqId_, data_));
		_clientSocket.cancelFundamentalData(reqId_);
	}

	@Override
	public void deltaNeutralValidation(int reqId_, DeltaNeutralContract underComp) {
		log.debug(String.format("deltaNeutralValidation [%d]", reqId_));
	}

	@Override
	public void softDollarTiers(int reqId_, SoftDollarTier[] tiers) {
		log.debug("softDollarTiers");
	}

	@Override
	public void execDetails(int reqId_, Contract contract, Execution execution) {
		log.debug("execDetails");
	}

	@Override
	public void execDetailsEnd(int reqId_) {
		log.debug("execDetailsEnd");
	}

	@Override
	public void updateMktDepth(int reqId_, int position, int operation, int side, double price, int size) {
		log.debug(String.format("updateMktDepth [%d]", reqId_));
	}

	@Override
	public void updateMktDepthL2(int reqId_, int position, String marketMaker, int operation, int side, double price, int size, boolean bool_) {
		log.debug(String.format("updateMktDepthL2 [%d]", reqId_));
	}

	@Override
	public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
		log.info(String.format("updateNewsBulletin [%d/%d/%s] - %s", msgId, msgType, origExchange, message));
	}

	@Override
	public void managedAccounts(String accountsList_) {
		log.debug("managedAccounts: " + accountsList_);
	}

	@Override
	public void receiveFA(int faDataType, String xml) {
		log.debug("receiveFA");
	}

	private static abstract class ATIBRequestObject implements IATIdentifyableRunnable {

		protected Map<String, Object> _reqMap;
		protected int _reqId;
		private String _id;

		public ATIBRequestObject(int reqId_, Map<String, Object> reqMap_, String id_) {
			_reqId = reqId_;
			_reqMap = reqMap_;
			_id = id_;
		}

		public Object getIdentification() {
			return _id;
		}

		@Override
		public void purge() {
			_reqMap.clear();
			_reqMap = null;
		}

	}

	@Override
	public void completedOrder(Contract arg0, Order arg1, OrderState arg2) {
		// TODO Auto-generated method stub
		log.debug("completedOrder");
	}

	@Override
	public void completedOrdersEnd() {
		// TODO Auto-generated method stub
		log.debug("completedOrdersEnd");
	}

	@Override
	public void orderBound(long arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		log.debug("orderBound");
	}

	// @Override
	// public void tickByTickAllLast(int arg0, int arg1, long arg2, double arg3, int arg4, TickAttribLast arg5, String arg6, String arg7) {
	// // TODO Auto-generated method stub
	//
	// }

	// @Override
	// public void tickByTickBidAsk(int arg0, long arg1, double arg2, double arg3, int arg4, int arg5, TickAttribBidAsk arg6) {
	// // TODO Auto-generated method stub
	//
	// }

	// @Override
	// public void tickPrice(int arg0, int arg1, double arg2, TickAttrib arg3) {
	// // TODO Auto-generated method stub
	//
	// }

}
