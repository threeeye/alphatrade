package com.isoplane.at.adapter.kibot;

import java.io.StringReader;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.isoplane.at.adapter.ATBaseHttpAdapter;
import com.isoplane.at.adapter.IATRussellAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSymbol;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// http://www.kibot.com/Files/2/Russell_3000_Intraday.txt
public class ATKibotAdapter extends ATBaseHttpAdapter implements IATRussellAdapter {

	static final Logger log = LoggerFactory.getLogger(ATKibotAdapter.class);

	static public final String ID = "kibot";

	static private ATKibotAdapter _instance;

	static public ATKibotAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATKibotAdapter();
			_instance.init();
		}
		return _instance;
	}

	protected ATKibotAdapter() {
		super(ID);
	}

	private void init() {

	}

	@Override
	public Set<ATSymbol> getRussell3000() {
		try {
			String url = "http://www.kibot.com/Files/2/Russell_3000_Intraday.txt";
			Map<String, String> headers = Map.of("Accept", "text/plain");
			String tsv = super.getHttp(url, headers, null);
			Set<ATSymbol> symbols = parseSymbols(tsv);
			log.info(String.format("Loaded    [Russell 3000(%,d)]", symbols.size()));
			return symbols;
		} catch (Exception ex) {
			log.error("Error", ex);
			return null;
		}
	}

	protected Set<ATSymbol> parseSymbols(String tsv_) {
		var idx = tsv_.indexOf('#');
		var tsv = tsv_.substring(idx);
		StringReader r = new StringReader(tsv);
		CSVParser parser = null;
		try {
			var format = CSVFormat.Builder.create(CSVFormat.TDF.withFirstRecordAsHeader()).build();
			parser = format.parse(r);
			var count = 0;
			Set<ATSymbol> symbols = new TreeSet<>();
			for (final CSVRecord rec : parser) {
				if (rec.size() < 2)
					break;
				if (!rec.isMapped("Symbol"))
					continue;
				var id = rec.get("Symbol");
				var symbol = new KibotSymbol(id);
				symbols.add(symbol);
				var dsc = rec.get("Description");
				symbol.setDescription(dsc);
				var name = dsc.replace("Common Stock", "");
				var nIdx = name.indexOf("Ordinary Shares");
				if (nIdx > 0) {
					name = name.substring(0, nIdx);
				}
				symbol.setName(name.trim());
				var ind = rec.get("Industry");
				symbol.setIndustry(ind);
				var sct = rec.get("Sector");
				symbol.setSector(sct);
				symbol.setR3000(true);
				count++;
			}
			log.info(String.format("Parsed [%d]", count));
			return symbols;
		} catch (Exception ex) {
			log.error(String.format("Parsing error"), ex);
		} finally {
			if (parser != null) {
				try {
					parser.close();
				} catch (Exception exx) {
					throw new ATException("Parsing error", exx);
				}
			}
		}
		return null;
	}

	public static class KibotSymbol extends ATSymbol {

		public KibotSymbol(String id_) {
			super(id_);
			super.source = ATKibotAdapter.ID;
		}

	}

}
