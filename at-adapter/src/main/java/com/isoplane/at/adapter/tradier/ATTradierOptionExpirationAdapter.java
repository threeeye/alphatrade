package com.isoplane.at.adapter.tradier;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.adapter.IATOptionExpirationAdapter;
import com.isoplane.at.commons.model.ATOptionExpiration;
import com.isoplane.at.commons.model.ATOptionExpiration.ATOptionExpirationDate;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATTradierOptionExpirationAdapter extends ATTradierBaseAdapter implements IATOptionExpirationAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierOptionExpirationAdapter.class);

	static final SimpleDateFormat yyyy_mm_ddFormat = ATFormats.DATE_yyyy_MM_dd.get();
	static final SimpleDateFormat ds8Format = ATFormats.DATE_yyyyMMdd.get();
	static private Gson _gson = new Gson();
	static private long _lastRuntime;

	public static void main(String[] args) throws Exception {
		// PropertiesConfiguration prop = new PropertiesConfiguration();
		// prop.read(new FileReader("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties"));

		ATConfigUtil.init("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties", false);

		String[] symbols = new String[] { "AAPL", "FMN", "TRIP", "SPY" };

		Set<IATSymbol> testSet = new TreeSet<>();
		for (String sym : symbols) {
			IATSymbol isym = new ATSymbol(sym);
			testSet.add(isym);
		}

		ATTradierOptionExpirationAdapter adapter = new ATTradierOptionExpirationAdapter();
		Map<String, IATOptionExpiration> expirations = adapter.getOptionExpirations(testSet);
		log.info(String.format("getOptionExpirations [%d]: %s", expirations.size(), expirations));
	}

	public ATTradierOptionExpirationAdapter() {
		super();
		_lastRuntime = System.currentTimeMillis();
	}

	public Map<String, IATOptionExpiration> getOptionExpirations(Set<IATSymbol> symbols_) {
		Map<String, IATOptionExpiration> result = new TreeMap<>();
		int count = 1;
		for (IATSymbol symbol : symbols_) {
			try {
				log.debug(String.format("[%3d/%3d] getOptionExpirations %s", count++, symbols_.size(), symbol));
				IATOptionExpiration symResult = getExpirations(symbol);
				if (symResult != null) {
					result.put(symbol.getId(), symResult);
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing entry [%s]: %s", symbol, ex.getMessage()));
			}
		}
		return result;
	}

	private IATOptionExpiration getExpirations(IATSymbol symbol_) {
		if (symbol_ == null)
			return null;
		try {
			long delta = System.currentTimeMillis() - _lastRuntime;
			if (delta < 500) {
				Thread.sleep(delta + 100);
			}
			_lastRuntime = System.currentTimeMillis();
			log.trace(String.format("getExpirations [%s]", symbol_));
			String url = String.format("/v1/markets/options/expirations?symbol=%s&strikes=true", symbol_.convert(PROVIDER_KEY));
			String content = getHttp(url);
			// log.info("content: " + content);
			IATOptionExpiration result = parseExpirations(symbol_, content);
			return result;
		} catch (Exception ex) {
			log.error(String.format("getExpirations [%s]", symbol_), ex);
			return null;
		}
	}

	private IATOptionExpiration parseExpirations(IATSymbol symbol_, String json_) {
		List<ATOptionExpirationDate> expList = new ArrayList<>();
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		try {
			if (root.isJsonNull() || !root.isJsonObject()) {
				return null;
			}
			JsonObject jroot = root.getAsJsonObject();
			JsonElement jExpWrapper = jroot.get("expirations");
			if (jExpWrapper == null || jExpWrapper.isJsonNull() || !jExpWrapper.isJsonObject()) {
				return null;
			}
			JsonElement jExpirations = jExpWrapper.getAsJsonObject().get("expiration");
			if (jExpirations == null || jExpirations.isJsonNull() || !jExpirations.isJsonArray()) {
				return null;
			}
			for (JsonElement expItem : jExpirations.getAsJsonArray()) {
				if (expItem.isJsonNull() || !expItem.isJsonObject()) {
					continue;
				}
				ATOptionExpirationDate ed = new ATOptionExpirationDate();
				JsonObject jExp = expItem.getAsJsonObject();
				String dStr = jExp.get("date").getAsString();
				JsonElement jStrikeWrapper = jExp.get("strikes");
				if (jStrikeWrapper == null || jStrikeWrapper.isJsonNull() || !jStrikeWrapper.isJsonObject()) {
					continue;
				}
				JsonElement jStrikes = jStrikeWrapper.getAsJsonObject().get("strike");
				if (jStrikes == null || jStrikes.isJsonNull() || !jStrikes.isJsonArray()) {
					continue;
				}
				List<Double> strikes = new ArrayList<>();
				for (JsonElement strkItem : jStrikes.getAsJsonArray()) {
					if (strkItem.isJsonNull() || !strkItem.isJsonPrimitive()) {
						continue;
					}
					Double strike = strkItem.getAsDouble();
					strikes.add(strike);

				}
				ed.setExpDS6(dStr.replace("-", "").substring(2));
				ed.setStrikes(strikes);
				expList.add(ed);
			}
			if (!expList.isEmpty()) {
				ATOptionExpiration oe = new ATOptionExpiration();
				oe.setOccId(symbol_.getId());
				oe.setExpirations(expList);
				return oe;
			}
		} catch (Exception ex) {
			log.error(String.format("Error parseExpirations [%s]: %s", symbol_, ex.getMessage()));
		}
		return null;
	}

	public static class ATTradierTempDiv {
		String exDateStr;
		String payDateStr;
		String recDateStr;
		Double div;
		Double freq;

		@Override
		public String toString() {
			return String.format("[%s, %.4f, %.2f]", exDateStr, div, freq);
		}

		@Override
		public int hashCode() {
			return exDateStr == null ? -1 : exDateStr.hashCode();
		}
	}

}
