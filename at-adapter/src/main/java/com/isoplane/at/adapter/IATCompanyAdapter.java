package com.isoplane.at.adapter;

import java.util.Map;

public interface IATCompanyAdapter {

	Map<String, String> getCompanies();
}
