package com.isoplane.at.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAsset;

public abstract class ATBaseIngester implements IATIngester {

	static final Logger log = LoggerFactory.getLogger(ATBaseIngester.class);

	public List<ATTrade> readTransactions(String userId_, String path_, Collection<ATTrade> trades_, List<ATBroker> brokers_) {
		SimpleDateFormat df = getDateFormat();
		log.info(String.format("readTransactions Only supports [ARD Fees, Dividends, Foreign Tax]"));
		TreeMap<String, ATTrade> oldTradeMap = new TreeMap<>();
		if (trades_ != null) {
			for (ATTrade trade : trades_) {
				String hash = tradeHash(df, trade);
				oldTradeMap.put(hash, trade);
			}
		}
		// TreeMap<String, ATTrade> oldTradeMap = trades_ == null
		// ? new TreeMap<>()
		// : trades_.stream().filter(t_ -> userId_.equals(t_.getTradeUserId())).collect(
		// Collectors.toMap(t_ -> tradeHash(df, t_), Function.identity(), (a, b) -> a, TreeMap::new));
		List<Entry<String, ATTrade>> list = new ArrayList<>(oldTradeMap.entrySet());
		list.sort((a, b) -> a.getValue().getTsTrade2().compareTo(b.getValue().getTsTrade2()));
		for (Entry<String, ATTrade> entry : list) {
			log.debug(String.format("DB  : %s", entry.getKey()));
		}

		TreeMap<String, ATTrade> newTradeMap = readFile(userId_, path_, brokers_);
		List<ATTrade> result = new ArrayList<>();
		for (Entry<String, ATTrade> entry : newTradeMap.entrySet()) {
			String hash = entry.getKey();
			if (oldTradeMap.containsKey(hash)) {
				log.info(String.format("HIT : %s", hash));
			} else {
				log.info(String.format("MISS: %s", hash));
				result.add(entry.getValue());
			}
		}
		log.info("readTransactions Done");
		return result;
	}

	abstract protected SimpleDateFormat getDateFormat();

	abstract protected TreeMap<String, ATTrade> readFile(String userId_, String path_, List<ATBroker> brokers_);

	protected Double parseDouble(String value_, boolean isNullZero_) {
		if (StringUtils.isBlank(value_))
			return null;
		String value = value_.replace("$", "");
		Double d = Double.parseDouble(value);
		if (d != null && isNullZero_ && d == 0) {
			d = null;
		}
		return d;
	}

	protected String tradeHash(SimpleDateFormat df_, ATTrade t_) { // , Collection<String> ids_
		String occId = t_.getOccId();
		String dStr = df_.format(t_.getTsTrade());
		// int dbAction = convertDbAction(t_.getTradeType());
		int type = t_.getTradeType();
		Double sz = t_.getSzTrade();
		String szStr = sz == null || sz == 0 ? null : String.format("%.4f", sz);
		Double px = t_.getPxTrade();
		String pxStr = px == null || px == 0 ? null : String.format("%.4f", px);
		Double amt = t_.getPxTradeTotal();
		String pxTotalStr = amt == null || amt == 0 ? null : String.format("%.4f", amt);
		String acct = t_.getTradeAccount();
		Object refExt = t_.get(IATAsset.REFERENCE_ID);
		// String hash = String.format("%s, %d, %s, %s, %s, %s, %s:%s", dStr, type, szStr, occId, pxStr, pxTotalStr, acct, refExt);
		String hash = formatHash(dStr, type, szStr, occId, pxStr, pxTotalStr, acct, refExt);
		// int i = 0;
		// String hash = null;
		// do {
		// hash = String.format("%s::%d", preHash, i++);
		// } while (ids_.contains(hash));
		return hash;
	}

	protected String formatHash(String dStr_, int type_, String szStr_, String occId_, String pxStr_, String pxTotalStr_, String acct_,
			Object refExt_) {
		String hash = String.format("%s, %d, %s, %s, %s, %s, %s:%s", dStr_, type_, szStr_, occId_, pxStr_, pxTotalStr_, acct_, refExt_);
		return hash;
	}

}
