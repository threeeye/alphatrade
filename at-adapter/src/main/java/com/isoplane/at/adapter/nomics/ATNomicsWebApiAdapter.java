package com.isoplane.at.adapter.nomics;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoplane.at.adapter.ATBaseHttpAdapter;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.EATChangeOperation;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATNomicsWebApiAdapter extends ATBaseHttpAdapter {

    static final public String SOURCE_KEY = "nomi";
    static public final String ID = "nomics";

    static final Logger log = LoggerFactory.getLogger(ATNomicsWebApiAdapter.class);
    static final SimpleDateFormat DF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    static private ATNomicsWebApiAdapter _instance;
    public long pricingCount = 0;

    static public ATNomicsWebApiAdapter getInstance() {
        if (_instance == null) {
            _instance = new ATNomicsWebApiAdapter();
            _instance.init();
        }
        return _instance;
    }

    // public static void main(String[] args) throws Exception {
    // List<String> configPaths =
    // Arrays.asList("C:/Dev/workspace/alphatrade/local.properties",
    // "C:/Dev/workspace/alphatrade/NOCHECKIN.properties");
    // CompositeConfiguration configs = new CompositeConfiguration();
    // for (String path : configPaths) {
    // PropertiesConfiguration config = new PropertiesConfiguration();
    // config.read(new FileReader(path));
    // configs.addConfiguration(config);
    // }

    // ATNomicsWebApiAdapter adapter = new ATNomicsWebApiAdapter();

    // List<String> symbols = Arrays.asList("BTC");
    // adapter.getMarketData(symbols);
    // }

    protected ATNomicsWebApiAdapter() {
        super(ID);
    }

    private void init() {

    }

    public Map<String, ATMarketData> getMarketData(Collection<String> symbols_) {
        log.debug(String.format("getMarketData[%d]", symbols_.size()));
        String json = requestPrices(symbols_);
        Map<String, ATMarketData> result = parsePricing(symbols_, json);
        return result;
    }

    public Map<String, MarketData> getProtoMarketData(Collection<String> symbols_) {
        log.debug(String.format("getProtoMarketData[%d]", symbols_.size()));
        String json = requestPrices(symbols_);
        Map<String, MarketData> result = parseProtoPricing(symbols_, json);
        return result;
    }

    private String requestPrices(Collection<String> symbols_) {
        log.debug(String.format("requestPrices[%d]", symbols_.size()));
        Configuration config = ATConfigUtil.config();
        String baseUrl = config.getString("nomics.url");
        String secret = config.getString("nomics.key");
        String symbolStr = String.join(",", symbols_);
        final String url = String.format("%s/currencies/ticker?key=%s&ids=%s&interval=1d&convert=USD&per-page=100", baseUrl, secret,
                symbolStr);
        String json = getHttp(url, null, null);
        if (log.isDebugEnabled()) {
            log.debug(String.format("requestPrices fetched: %s", json));
        }
        this.pricingCount++;
        return json;
    }

    // {
    // 	"id": "ETH",
    // 	"currency": "ETH",
    // 	"symbol": "ETH",
    // 	"name": "Ethereum",
    // 	"logo_url": "https://s3.us-east-2.amazonaws.com/nomics-api/static/images/currencies/eth.svg",
    // 	"status": "active",
    // 	"platform_currency": "ETH",
    // 	"price": "4322.64448999",
    // 	"price_date": "2021-12-08T00:00:00Z",
    // 	"price_timestamp": "2021-12-08T14:11:00Z",
    // 	"circulating_supply": "118655149",
    // 	"market_cap": "512904024678",
    // 	"market_cap_dominance": "0.2119",
    // 	"num_exchanges": "438",
    // 	"num_pairs": "71493",
    // 	"num_pairs_unmapped": "55047",
    // 	"first_candle": "2015-08-07T00:00:00Z",
    // 	"first_trade": "2015-08-07T00:00:00Z",
    // 	"first_order_book": "2018-08-29T00:00:00Z",
    // 	"rank": "2",
    // 	"rank_delta": "0",
    // 	"high": "4811.31348800",
    // 	"high_timestamp": "2021-11-08T00:00:00Z",
    // 	"1d": {
    // 		"volume": "28442320298.00",
    // 		"price_change": "-88.24957876",
    // 		"price_change_pct": "-0.0200",
    // 		"volume_change": "-8617464548.63",
    // 		"volume_change_pct": "-0.2325",
    // 		"market_cap_change": "-10415433791.65",
    // 		"market_cap_change_pct": "-0.0199"
    // 	}
    // }

    /**
     * Parses json returned from service.
     * 
     * @param symbols_ List of symbols to collect; NULL for all symbols in json.
     * @param json_    Json string to parse
     * @return Map of symbols and market data
     */
    protected Map<String, ATMarketData> parsePricing(Collection<String> symbols_, String json_) {
        Configuration config = ATConfigUtil.config();
        String prefix = config.getString("nomics.prefix", "");
        Map<String, ATMarketData> mktMap = new HashMap<>();
        List<String> symbols = symbols_ != null ? new ArrayList<>(symbols_) : null;
        try {
            JsonNode rootNode = new ObjectMapper().readTree(json_);
            if (rootNode == null || rootNode.isNull() || !rootNode.isArray()) {
                log.warn(String.format("parsePricing Missing [%s]", symbols_));
                return null;
            }
            for (JsonNode idNode : rootNode) {
                try {
                    String sym = idNode.get("symbol").asText();
                    if (symbols != null) {
                        if (!symbols.contains(sym)) {
                            continue;
                        } else {
                            symbols.remove(sym);
                        }
                    }
                    double px_lst = idNode.get("price").asDouble();
                    String ts_lst_str = idNode.get("price_timestamp").asText();
                    long ts_lst = DF.parse(ts_lst_str).getTime();
                    double px_hi = idNode.get("high").asDouble(Double.NaN);
                    // String ts_hi_str = idNode.get("price_timestamp").asText();
                    // long ts_hi = DF.parse(ts_hi_str).getTime();
                    ATMarketData mkt = new ATMarketData();
                    String adjustedSymbol = String.format("%s%s", prefix, sym);
                    mkt.setOccId(adjustedSymbol);
                    mkt.setPxLast(px_lst);
                    mkt.setTsLast(ts_lst);
                    mkt.setPxHigh(px_hi);
                    JsonNode d1Node = idNode.get("1d");
                    if (d1Node != null && !d1Node.isNull()) {
                        Double volume = d1Node.get("volume").asDouble(Double.NaN);
                        if (Double.NaN != volume) {
                            mkt.setDailyVol(volume.longValue());
                        }
                        double px_chng = d1Node.get("price_change_pct").asDouble(Double.NaN);
                        if (Double.NaN != px_chng) {
                            mkt.setPxChange(px_chng);
                            double px_cls = px_lst + (1 + px_chng);
                            mkt.setPxClose(px_cls);
                        }
                    }
                    // mkt.set
                    mktMap.put(adjustedSymbol, mkt);
                } catch (Exception ex) {
                    log.error(String.format("parsePricing Error: %s", idNode), ex);
                }
            }
            if (symbols != null && !symbols.isEmpty()) {
                log.warn(String.format("parsePricing Parsed [%s] Missing [%s]", mktMap.keySet(), symbols));
            }
        } catch (Exception ex) {
            log.error(String.format("parsePricing Error"), ex);
        }
        return mktMap;
    }

    protected Map<String, MarketData> parseProtoPricing(Collection<String> symbols_, String json_) {
        Map<String, MarketData> mktMap = new HashMap<>();
        List<String> symbols = symbols_ != null ? new ArrayList<>(symbols_) : null;
        try {
            JsonNode rootNode = new ObjectMapper().readTree(json_);
            if (rootNode == null || rootNode.isNull() || !rootNode.isArray()) {
                log.warn(String.format("parseProtoPricing Missing [%s]", symbols_));
                return null;
            }
            for (JsonNode idNode : rootNode) {
                try {
                    String sym = idNode.get("symbol").asText();
                    if (symbols != null) {
                        if (!symbols.contains(sym)) {
                            continue;
                        } else {
                            symbols.remove(sym);
                        }
                    }
                    double px_lst = idNode.get("price").asDouble();
                    String ts_lst_str = idNode.get("price_timestamp").asText();
                    long ts_lst = DF.parse(ts_lst_str).getTime();
                    MarketData.Builder builder = MarketData.newBuilder();
                    builder.setId(sym);
                    builder.setIdUl(sym);
                    builder.setActionCode(EATChangeOperation.UPDATE.name());
                    builder.setSrc(SOURCE_KEY);
                    builder.setPxLast(px_lst);
                    builder.setTsLast(ts_lst);
                    MarketData mkt = builder.build();
                    mktMap.put(sym, mkt);
                } catch (Exception ex) {
                    log.error(String.format("parseProtoPricing Error: %s", idNode), ex);
                }
            }
            if (symbols != null && !symbols.isEmpty()) {
                log.warn(String.format("parseProtoPricing Parsed [%s] Missing [%s]", mktMap.keySet(), symbols));
            }
        } catch (Exception ex) {
            log.error(String.format("parseProtoPricing Error"), ex);
        }
        return mktMap;
    }

}
