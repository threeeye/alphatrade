package com.isoplane.at.adapter.ameritrade;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.configuration2.CompositeConfiguration;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATAmeritradeWebApiAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAmeritradeWebApiAdapter.class);

	static final String SOURCE_KEY = "Ameri";

	// private Configuration _config;
	private Gson _gson = new Gson();
	private String _clientId;
	private String _rToken;
	private String _aToken;
	private long _aTokenTS;
	private long _lastRequestTime = 0;
	private long _requestSuccessCount = 0;
	private long _requestFailureCount = 0;
	private RequestConfig _httpReqConfig;
	private FileBasedConfigurationBuilder<XMLConfiguration> _configBuilder;

	static private SimpleDateFormat _dfDS8 = ATFormats.DATE_yyyyMMdd.get();
	static private ATAmeritradeWebApiAdapter _instance;

	static public ATAmeritradeWebApiAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATAmeritradeWebApiAdapter();
			_instance.init();
		}
		return _instance;
	}

	public static void main(String[] args) throws Exception {
		List<String> configPaths = Arrays.asList("C:/Dev/workspace/alphatrade/local.properties", "C:/Dev/workspace/alphatrade/NOCHECKIN.properties");
		CompositeConfiguration configs = new CompositeConfiguration();
		for (String path : configPaths) {
			PropertiesConfiguration config = new PropertiesConfiguration();
			config.read(new FileReader(path));
			configs.addConfiguration(config);
		}

		ATAmeritradeWebApiAdapter adapter = new ATAmeritradeWebApiAdapter();

		List<IATSymbol> symbols = Arrays.asList(new ATSymbol("AAPL"));
		adapter.getFundamentals(symbols);
	}

	private ATAmeritradeWebApiAdapter() {
		// _config = config_;
		// _clientId = _config.getString("ameritrade.client_id");
		// _rToken = _config.getString("ameritrade.rtoken");
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		String path = config.getString("tda.path");
		try {
			_configBuilder = new Configurations().xmlBuilder(path);
			_configBuilder.setAutoSave(true);
			XMLConfiguration xconfig = _configBuilder.getConfiguration();
			// config.setProperty("test", "TEST");
			_clientId = xconfig.getString("clientId");
			_rToken = xconfig.getString("refreshToken");
			_aToken = xconfig.getString("accessToken");
			_aTokenTS = xconfig.getLong("accessTokenTimeOut");
			log.debug(String.format("Loaded client id [%b]", !StringUtils.isBlank(_clientId)));
			log.debug(String.format("Loaded refresh token [%b]", !StringUtils.isBlank(_rToken)));
			log.debug(String.format("Loaded access token [%b]", !StringUtils.isBlank(_aToken)));
			log.debug(String.format("Loaded access token TS [%b]", _aTokenTS != 0));

			int httpTimeout = config.getInt("tda.httpTimeout");
			_httpReqConfig = RequestConfig.custom()
					.setConnectTimeout(httpTimeout)
					.setConnectionRequestTimeout(httpTimeout)
					.setSocketTimeout(httpTimeout)
					.build();
			log.debug(String.format("HTTP timeout [%d]", httpTimeout));
		} catch (Exception ex) {
			String msg = String.format("init Error loading configuration: %s", path);
			throw new ATException(msg, ex);
		}
	}

	private boolean checkAccessToken() {
		if (_aToken == null || System.currentTimeMillis() > _aTokenTS) {
			refreshAccessToken();
		}
		if (_aToken != null) {
			// _aTokenTS = System.currentTimeMillis();
			return true;
		} else {
			log.error(String.format("checkAccessToken Failure"));
			return false;
		}
	}

	private void refreshAccessToken() {
		final String url = "https://api.tdameritrade.com/v1/oauth2/token";
		Map<String, String> form = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("grant_type", "refresh_token");
				put("refresh_token", _rToken);
				put("client_id", _clientId + "@AMER.OAUTHAP");
			}
		};
		String json = postHttp(url, null, null, form);
		parseAccessToken(json);
	}

	public TreeSet<ATPriceHistory> getHistory(Map<IATSymbol, String> symMap_, Date minDate_) {
		log.debug(String.format("getHistory[%d]", symMap_.size()));
		final String url = "https://api.tdameritrade.com/v1/marketdata/%s/pricehistory";
		TreeSet<ATPriceHistory> result = new TreeSet<>();
		long startDate = minDate_.getTime();
		Map<String, String> params = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("periodType", "month");
				put("frequencyType", "daily");
				put("frequency", "1");
				put("startDate", startDate + "");
			}
		};
		int count = 0;
		for (Entry<IATSymbol, String> entry : symMap_.entrySet()) {
			count++;
			String sym = entry.getKey().convert(SOURCE_KEY);
			try {
				String sUrl = String.format(url, sym);
				String ds8 = entry.getValue();
				if (ds8 != null) {
					Date d = _dfDS8.parse(ds8);
					params.put("startDate", d.getTime() + "");
				}
				String json = getHttp(sUrl, null, params);
				if (!StringUtils.isBlank(json)) {
					Collection<ATPriceHistory> history = parseHistory(entry.getKey(), json);
					if (history != null) {
						result.addAll(history);
					}
				} else {
					log.info(String.format("getHistory No history", entry.getKey()));
				}
			} catch (Exception ex) {
				log.error(String.format("getHistory Error[%s]", entry), ex);
			}
			log.debug(String.format("%3d/%d getHistory [%-6s]", count, symMap_.size(), sym));
		}
		return result;
	}

	public List<IATLookupMap> getFundamentals(Collection<IATSymbol> symbols_) {
		log.debug(String.format("getFundamentals[%d]", symbols_.size()));
		final String url = "https://api.tdameritrade.com/v1/instruments";
		String today_ds8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		List<IATLookupMap> result = new ArrayList<>();
		int count = 0;
		for (IATSymbol sym : symbols_) {
			count++;
			String symbol = sym.convert(SOURCE_KEY);
			Map<String, String> params = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					put("projection", "fundamental");
					put("symbol", symbol);
				}
			};
			String json = getHttp(url, null, params);
			IATLookupMap dataMap = parseFundamentals(symbol, json);
			if (dataMap != null && !dataMap.isEmpty()) {
				dataMap.put(IATAssetConstants.TS, today_ds8);
				result.add(dataMap);
			}
			log.debug(String.format("%3d/%d Fundamentals [%-6s]", count, symbols_.size(), symbol));
		}
		return result;
	}

	public IATLookupMap getOptionChain(IATSymbol symbol_) {
		try {
			// if (true || !checkAccessToken())
			// return null;
			log.debug(String.format("getOptionChain: %s", symbol_));

			String symbol = symbol_.convert(SOURCE_KEY);
			final String url = "https://api.tdameritrade.com/v1/marketdata/chains";
			Map<String, String> headers = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					put("Authorization", String.format("Bearer %s", _aToken));
				}
			};
			headers.clear();
			Map<String, String> params = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					// put("apikey", _clientId);
					put("symbol", symbol);
					put("includeQuotes", "TRUE");
					put("strategy", "SINGLE");
					put("range", "ALL");
					put("optionType", "ALL");
				}
			};
			String json = getHttp(url, headers, params);
			log.info(json);
			IATLookupMap map = parseOptionChain(symbol, json);
			return map;
		} catch (Throwable ex_) {
			log.error(String.format("getOptionChain"), ex_);
			return null;
		}
	}

	private String parseAccessToken(String json_) {
		try {
			JsonElement root = _gson.fromJson(json_, JsonElement.class);
			JsonElement jat = root.getAsJsonObject().get("access_token");
			String aToken = jat.getAsString();
			JsonElement jexp = root.getAsJsonObject().get("expires_in");
			long exp = jexp.getAsLong();
			long timeout = System.currentTimeMillis() + (exp * 1000) - 5000;

			_aToken = aToken;
			_aTokenTS = timeout;
			_configBuilder.getConfiguration().setProperty("accessToken", aToken);
			_configBuilder.getConfiguration().setProperty("accessTokenTimeOut", timeout);

			return aToken;
		} catch (Exception ex) {
			log.error(String.format("parseAccessToken Error: %s", json_), ex);
			return null;
		}
	}

	private IATLookupMap parseFundamentals(String symbol_, String json_) {
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonElement jsym = root.getAsJsonObject().get(symbol_);
		JsonElement jfun = jsym == null || jsym.isJsonNull() || !jsym.isJsonObject() ? null : jsym.getAsJsonObject().get("fundamental");
		if (jfun == null || jfun.isJsonNull() || !jfun.isJsonObject()) {
			log.warn(String.format("parseFundamentals No data [%s]", symbol_));
			return null;
		}
		JsonObject jofun = jfun.getAsJsonObject();
		IATLookupMap result = new ATPersistentLookupBase();
		result.setAtId(symbol_);
		for (String key : jofun.keySet()) {
			if ("symbol".equals(key))
				continue;
			JsonElement jvalue = jofun.get(key);
			if (jvalue.isJsonNull() || !jvalue.isJsonPrimitive())
				continue;
			JsonPrimitive jp = jvalue.getAsJsonPrimitive();
			if (jp.isString()) {
				result.put(key, jp.getAsString());
			} else if (jp.isNumber()) {
				Double jn = jp.getAsDouble();
				result.put(key, jn);
			}
		}
		return result;
	}

	private Collection<ATPriceHistory> parseHistory(IATSymbol symbol_, String json_) {
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonElement jeCandles = root.getAsJsonObject().get("candles");
		if (jeCandles == null || !jeCandles.isJsonArray()) {
			log.warn(String.format("parseHistory No data [%s]", symbol_));
			return null;
		}
		List<ATPriceHistory> result = new ArrayList<>();
		JsonArray joCandles = jeCandles.getAsJsonArray();
		for (JsonElement je : joCandles) {
			if (je == null || !je.isJsonObject())
				continue;
			JsonObject jo = je.getAsJsonObject();

			long dt = jo.get("datetime").getAsLong();
			Double open = jo.get("open").getAsDouble();
			Double high = jo.get("high").getAsDouble();
			Double low = jo.get("low").getAsDouble();
			Double close = jo.get("close").getAsDouble();
			Long vol = jo.get("volume").getAsLong();

			String ds8 = _dfDS8.format(new Date(dt));
			String atId = String.format("%-5s%s", symbol_.getId(), ds8);

			ATPriceHistory pxh = new ATPriceHistory();
			pxh.setSource(SOURCE_KEY);
			pxh.setAtId(atId);
			pxh.setOccId(symbol_.getId());
			pxh.setDateStr(ds8);
			pxh.setOpen(open);
			pxh.setHigh(high);
			pxh.setLow(low);
			pxh.setClose(close);
			pxh.setVolume(vol);
			result.add(pxh);
		}
		return result;
	}

	private IATLookupMap parseOptionChain(String symbol_, String json_) {
		if (StringUtils.isBlank(json_))
			return null;
		try {
			JsonObject root = _gson.fromJson(json_, JsonElement.class).getAsJsonObject();
			JsonElement jStatus = root.get("status");
			if (jStatus == null || jStatus.isJsonNull() || !jStatus.isJsonPrimitive() || !"SUCCESS".equals(jStatus.getAsString()))
				return null;
			IATLookupMap result = new ATPersistentLookupBase();
			result.setAtId(symbol_);
			result.put(IATAssetConstants.OCCID, symbol_);
			JsonElement jInt = root.get("interestRate");
			if (jInt != null && jInt.isJsonPrimitive()) {
				result.put("interestRate", jInt.getAsDouble());
			}
			JsonElement jVol = root.get("volatility");
			if (jVol != null && jVol.isJsonPrimitive()) {
				result.put("volatility", jVol.getAsDouble());
			}
			JsonElement jStrat = root.get("strategy");
			if (jStrat != null && jStrat.isJsonPrimitive()) {
				result.put("strategy", jStrat.getAsString());
			}
			JsonElement jUl = root.get("underlying");
			@SuppressWarnings("unchecked")
			Map<String, Object> ulMap = _gson.fromJson(jUl, HashMap.class);
			result.put("ul", ulMap);
			SimpleDateFormat df_YYYY_MM_dd = ATFormats.DATE_yyyy_MM_dd.get();
			SimpleDateFormat df_YYMMdd = ATFormats.DATE_yyMMdd.get();
			JsonElement jePutExpDateMap = root.get("putExpDateMap");
			if (jePutExpDateMap != null && !jePutExpDateMap.isJsonNull() && jePutExpDateMap.isJsonObject()) {
				Map<String, Object> putMap = new TreeMap<>();
				JsonObject joPutExpDateMap = jePutExpDateMap.getAsJsonObject();
				for (String keyExpDate : joPutExpDateMap.keySet()) {
					JsonObject jeExpDate = joPutExpDateMap.get(keyExpDate).getAsJsonObject();
					if (jeExpDate == null || jeExpDate.isJsonNull())
						continue;
					Date dExp = df_YYYY_MM_dd.parse(keyExpDate.substring(0, 10));
					for (String keyStrike : jeExpDate.keySet()) {
						JsonElement jeStrike = jeExpDate.get(keyStrike);
						if (jeStrike == null || jeStrike.isJsonNull() || !jeStrike.isJsonArray())
							continue;
						Double strike = Double.parseDouble(keyStrike);
						JsonElement jaStrike = jeStrike.getAsJsonArray().get(0);
						@SuppressWarnings("unchecked")
						Map<String, Object> strikeMap = _gson.fromJson(jaStrike, HashMap.class);
						String occId = ATFormats.toOccId(symbol_, dExp, "P", strike);
						strikeMap.put(IATAssetConstants.OCCID, occId);
						strikeMap.put(IATAssetConstants.EXPIRATION_DATE_STR, df_YYMMdd.format(dExp));
						putMap.put(occId, strikeMap);
					}
				}
				result.put("puts", putMap);
			}
			JsonElement jeCallExpDateMap = root.get("callExpDateMap");
			if (jeCallExpDateMap != null && !jeCallExpDateMap.isJsonNull() && jeCallExpDateMap.isJsonObject()) {
				Map<String, Object> callMap = new TreeMap<>();
				JsonObject joCallExpDateMap = jeCallExpDateMap.getAsJsonObject();
				for (String keyExpDate : joCallExpDateMap.keySet()) {
					JsonObject jeExpDate = joCallExpDateMap.get(keyExpDate).getAsJsonObject();
					if (jeExpDate == null || jeExpDate.isJsonNull())
						continue;
					Date dExp = df_YYYY_MM_dd.parse(keyExpDate.substring(0, 10));
					for (String keyStrike : jeExpDate.keySet()) {
						JsonElement jeStrike = jeExpDate.get(keyStrike);
						if (jeStrike == null || jeStrike.isJsonNull() || !jeStrike.isJsonArray())
							continue;
						Double strike = Double.parseDouble(keyStrike);
						JsonElement jaStrike = jeStrike.getAsJsonArray().get(0);
						@SuppressWarnings("unchecked")
						Map<String, Object> strikeMap = _gson.fromJson(jaStrike, HashMap.class);
						String occId = ATFormats.toOccId(symbol_, dExp, "C", strike);
						strikeMap.put(IATAssetConstants.OCCID, occId);
						strikeMap.put(IATAssetConstants.EXPIRATION_DATE_STR, df_YYMMdd.format(dExp));
						callMap.put(occId, strikeMap);
					}
				}
				result.put("calls", callMap);
			}
			return result;
		} catch (Exception ex) {
			log.error(String.format("parseOptionChain Error [%s]", symbol_), ex);
			return null;
		}
	}

	// Adds API Key
	protected String getHttp(String url_, Map<String, String> headers_, Map<String, String> params_) {
		CloseableHttpResponse response = null;
		try {
			long throttleTime = ATConfigUtil.config().getLong("tda.throttletime");
			long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			if (deltaTime < throttleTime) {
				Thread.sleep(deltaTime + 100);
			}
			_lastRequestTime = System.currentTimeMillis();

			Map<String, String> params = params_ != null ? params_ : new HashMap<>();
			params.put("apikey", _clientId);
			URIBuilder uri = new URIBuilder(url_);
			for (Entry<String, String> param : params.entrySet()) {
				uri.addParameter(param.getKey(), param.getValue());
			}

			CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(_httpReqConfig).build();
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(uri.build());
			httpGet.setHeader("Accept", "application/json");
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpGet.setHeader(header.getKey(), header.getValue());
				}
			}

			response = httpclient.execute(httpGet);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			_requestSuccessCount++;
			return content;
		} catch (SocketTimeoutException toex) {
			_requestFailureCount++;
			log.error(String.format("ERROR: Request timed out [%s - %d/%d]: %s", params_.get("symbol"), _requestFailureCount, _requestSuccessCount,
					url_));
			return null;
		} catch (Exception ex) {
			_requestFailureCount++;
			log.error(String.format("Error requesting: %s: %s", url_, params_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

	protected String postHttp(String url_, Map<String, String> headers_, Map<String, String> params_, Map<String, String> form_) {
		CloseableHttpResponse response = null;
		try {
			long throttleTime = ATConfigUtil.config().getLong("tda.throttletime");
			long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			if (deltaTime < throttleTime) {
				Thread.sleep(deltaTime + 100);
			}
			_lastRequestTime = System.currentTimeMillis();

			// Map<String, String> params = params_ != null ? params_ : new HashMap<>();
			// params.put("apikey", _clientId);
			URIBuilder uri = new URIBuilder(url_);
			if (params_ != null) {
				for (Entry<String, String> param : params_.entrySet()) {
					uri.addParameter(param.getKey(), param.getValue());
				}
			}

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(uri.build());
			// httpPost.setHeader("Accept", "application/json");
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpPost.setHeader(header.getKey(), header.getValue());
				}
			}
			if (form_ != null) {
				List<NameValuePair> formData = new ArrayList<>();
				for (Entry<String, String> form : form_.entrySet()) {
					formData.add(new BasicNameValuePair(form.getKey(), form.getValue()));
				}
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formData, Consts.UTF_8);
				httpPost.setEntity(formEntity);
			}

			response = httpclient.execute(httpPost);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (Exception ex) {
			log.error(String.format("Error requesting: %s", url_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

}
