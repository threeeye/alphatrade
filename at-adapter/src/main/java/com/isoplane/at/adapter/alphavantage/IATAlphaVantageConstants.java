package com.isoplane.at.adapter.alphavantage;

public interface IATAlphaVantageConstants {

	public static final String BALANCE_SHEET = "balance";
	public static final String CASH_FLOW = "cashflow";
	public static final String COMPANY = "company";
	public static final String CONSOLIDATED = "consolidated";
	public static final String EARNINGS = "earnings";

	public static final String[] STATEMENTS = { COMPANY, EARNINGS, BALANCE_SHEET, CASH_FLOW };

}
