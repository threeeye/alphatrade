package com.isoplane.at.adapter.alpaca;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATAlpacaAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAlpacaAdapter.class);

	static final String SOURCE_KEY = "Alpc";
	static final SimpleDateFormat DF_1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	static final SimpleDateFormat DF_2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	private long _lastRequestTime = 0;
	private long _requestSuccessCount = 0;
	private long _requestFailureCount = 0;
	private RequestConfig _httpReqConfig;

	static private ATAlpacaAdapter _instance;

	static public ATAlpacaAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATAlpacaAdapter();
			_instance.init();
		}
		return _instance;
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		int httpTimeout = config.getInt("alpaca.httpTimeout");
		_httpReqConfig = RequestConfig.custom()
				.setConnectTimeout(httpTimeout)
				.setConnectionRequestTimeout(httpTimeout)
				.setSocketTimeout(httpTimeout)
				.build();
		log.debug(String.format("HTTP timeout [%d]", httpTimeout));
	}

	public ATTrade getTrade(String symbol_, Date date_) {
		Configuration config = ATConfigUtil.config();
		String urlBase = config.getString("alpaca.url");
		String url = String.format("%s/stocks/%s/trades", urlBase, symbol_);
		// stocks/AAPL/trades?limit=1&start=2021-02-03T13:35:08.000Z&end=2021-02-03T14:35:08.000Z
		String startStr = DF_1.format(date_);
		String endStr = DF_1.format(DateUtils.addHours(date_, 1));
		Map<String, String> params = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("limit", "1");
				put("start", startStr);
				put("end", endStr);
			}
		};
		String json = getHttp(url, null, params);
		List<ATTrade> trdList = parseTrade(json);
		ATTrade trd = trdList != null && !trdList.isEmpty() ? trdList.get(0) : null;
		return trd;
	}

	public List<ATTrade> parseTrade(String json_) {
		List<ATTrade> list = new ArrayList<>();
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode tradesNode = rootNode.withArray("trades");
			Iterator<JsonNode> iTrade = tradesNode.elements();
			while (iTrade.hasNext()) {
				JsonNode tradeNode = iTrade.next();
				String tsStr = tradeNode.get("t").textValue();
				Date d = DF_2.parse(tsStr);
				Double px = tradeNode.get("p").doubleValue();
				Double sz = tradeNode.get("s").doubleValue();
				ATTrade trd = new ATTrade();
				trd.setTsTrade(d);
				trd.setPxTrade(px);
				trd.setSzTrade(sz);
				list.add(trd);
			}
		} catch (Exception ex) {
			log.error(String.format("parseTrade Error"), ex);
		}
		return list;
	}

	protected String getHttp(String url_, Map<String, String> headers_, Map<String, String> params_) {
		CloseableHttpResponse response = null;
		try {
			Configuration config = ATConfigUtil.config();

			long throttleTime = config.getLong("alpaca.throttletime");
			long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			if (deltaTime < throttleTime) {
				Thread.sleep(deltaTime + 100);
			}
			_lastRequestTime = System.currentTimeMillis();

			String apiKey = config.getString("alpaca.key");
			String apiSecret = config.getString("alpaca.secret");
			Map<String, String> headers = headers_ != null ? headers_ : new HashMap<>();
			headers.put("APCA-API-KEY-ID", apiKey);
			headers.put("APCA-API-SECRET-KEY", apiSecret);

			Map<String, String> params = params_ != null ? params_ : new HashMap<>();
			URIBuilder uri = new URIBuilder(url_);
			for (Entry<String, String> param : params.entrySet()) {
				uri.addParameter(param.getKey(), param.getValue());
			}

			CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(_httpReqConfig).build();
			HttpGet httpGet = new HttpGet(uri.build());
			httpGet.setHeader("Accept", "application/json");
			if (headers != null) {
				for (Entry<String, String> header : headers.entrySet()) {
					httpGet.setHeader(header.getKey(), header.getValue());
				}
			}

			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			_requestSuccessCount++;
			return content;
		} catch (SocketTimeoutException toex) {
			_requestFailureCount++;
			log.error(String.format("ERROR: Request timed out [%s - %d/%d]: %s", url_, _requestFailureCount, _requestSuccessCount,
					url_));
			return null;
		} catch (Exception ex) {
			_requestFailureCount++;
			log.error(String.format("Error requesting: %s: %s", url_, params_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}
}
