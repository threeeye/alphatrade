package com.isoplane.at.adapter.etrade;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.CharBuffer;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.ATBaseIngester;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATTrade;

public class ATEtradeIngestAdapter extends ATBaseIngester {

	static final Logger log = LoggerFactory.getLogger(ATEtradeIngestAdapter.class);

	// static final SimpleDateFormat DF = new SimpleDateFormat("MM/dd/yy");

	public static void main(String[] args_) {
		String path = "C:\\data\\tx\\TdW8PcdGC9NvNl17kebhqdf5oNX2\\FID\\fid_Accounts_History.csv";

		// new ATAllyIngestAdapter().readFile("test", path);
	}

	@Override
	protected TreeMap<String, ATTrade> readFile(String userId_, String path_, List<ATBroker> brokers_) {
		SimpleDateFormat df = getDateFormat();
		TreeMap<String, ATTrade> tradeMap = new TreeMap<>();
		try (Reader reader = new EtradeReader(path_);
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim().withIgnoreEmptyLines())) {
			String acctStr = brokers_.get(0).getId();
			for (CSVRecord record : csvParser) {
				String dateStr = record.get("TransactionDate");
				if (record.size() < 9)
					break;

				String act = record.get("TransactionType");
				String amtStr = record.get("Amount");
				Double pxTotal = super.parseDouble(amtStr, true);
				Integer csvAction = convertCsvAction(act, pxTotal);
				if (csvAction == null)
					continue;
				String secType = record.get("SecurityType");
				String ticker = record.get("Symbol");
				String countStr = record.get("Quantity");
				Double count = super.parseDouble(countStr, true);
				String pxStr = record.get("Price");
				Double px = super.parseDouble(pxStr, true);

				String cmms = record.get("Commission");
				String dscr = record.get("Description");
				if (count != null && count != 0 && pxTotal != null && pxTotal != 0 && (px == null || px == 0)) {
					px = Math.abs(Precision.round(pxTotal / count, 4));
				}
				Date tsTrade = df.parse(dateStr);

				ATTrade trade = new ATTrade();
				trade.setOccId(ticker);
				trade.setPxTrade(px);
				trade.setPxTradeTotal(pxTotal);
				trade.setSzTrade(count);
				trade.setTradeAccount(acctStr);
				trade.setTradeUserId(userId_);
				trade.setTradeType(csvAction);
				trade.setTsTrade(tsTrade);
				trade.put(IATAsset.SOURCE, String.format("ingest: %s", Paths.get(path_).getFileName()));

				String hash = super.tradeHash(df, trade);
				log.debug(String.format("FILE: %s", hash));
				tradeMap.put(hash, trade);
			}
			return tradeMap;
		} catch (Exception ex) {
			throw new ATException(String.format("Error reading '%s'", path_), ex);
		}
	}

	/**
	 * Only supports dividends for now.
	 * 
	 * @param dscr_
	 * @param sec_
	 * @return
	 */
	private Integer convertCsvAction(String dscr_, Double pxTotal_) {
		if (dscr_ == null)
			return null;
		String dscr = dscr_.toUpperCase();
		if (dscr.equals("DIVIDEND")) {
			return pxTotal_ != null && pxTotal_ < 0 ? IATTrade.REINVEST : IATTrade.DIVIDEND_IN;
		}
		// if (dscr.startsWith("FEE CHARGED") && dscr.contains("ADR"))
		// return IATTrade.ADR_FEE;
		// if (dscr.startsWith("FOREIGN TAX PAID"))
		// return IATTrade.TAX_FOREIGN;
		log.debug(String.format("convertCsvAction Unsupported [%s]", dscr_));
		return null;
	}

	// NOTE: Is this necessary?
	// private int convertDbAction(int type_) {
	// switch (type_) {
	// case IATTrade.CALL_BTC:
	// case IATTrade.CALL_BTO:
	// case IATTrade.MF_IN:
	// case IATTrade.PUT_BTC:
	// case IATTrade.PUT_BTO:
	// case IATTrade.SECURITY_BTC:
	// case IATTrade.SECURITY_BTC_ASG:
	// case IATTrade.SECURITY_BTC_EXEC:
	// case IATTrade.SECURITY_BTO:
	// case IATTrade.SECURITY_BTO_ASG:
	// case IATTrade.SECURITY_BTO_EXEC:
	// return 100;
	// case IATTrade.CALL_STC:
	// case IATTrade.CALL_STO:
	// case IATTrade.MF_OUT:
	// case IATTrade.PUT_STC:
	// case IATTrade.PUT_STO:
	// case IATTrade.SECURITY_STC:
	// case IATTrade.SECURITY_STC_ASG:
	// case IATTrade.SECURITY_STC_EXEC:
	// case IATTrade.SECURITY_STO:
	// case IATTrade.SECURITY_STO_ASG:
	// case IATTrade.SECURITY_STO_EXEC:
	// return 200;
	// case IATTrade.ADR_FEE:
	// case IATTrade.CALL_ASG:
	// case IATTrade.CALL_EXEC:
	// case IATTrade.CALL_EXP:
	// case IATTrade.CAP_GAIN_LT:
	// case IATTrade.CAP_GAIN_ST:
	// case IATTrade.DIVIDEND_IN:
	// case IATTrade.IN_LEU_OF:
	// case IATTrade.INTEREST_IN:
	// case IATTrade.PUT_ASG:
	// case IATTrade.PUT_EXEC:
	// case IATTrade.PUT_EXP:
	// case IATTrade.REINVEST:
	// case IATTrade.TAX_FOREIGN:
	// return type_;
	// default:
	// String msg = String.format("Unsupported [%d]", type_);
	// throw new ATException(msg);
	// }
	// }

	public static class EtradeReader extends FileReader {

		boolean firstPass = true;

		public EtradeReader(String path_) throws FileNotFoundException {
			super(path_);
		}

		@Override
		public int read() throws IOException {
			return super.read();
		}

		@Override
		public int read(char[] cbuf) throws IOException {
			return super.read(cbuf);
		}

		@Override
		public int read(char[] cbuf, int offset, int length) throws IOException {
			if (firstPass) {
				firstPass = false;
				char[] tmpBuf = new char[cbuf.length];
				int count = super.read(tmpBuf, offset, length);
				String tmpStr = new String(tmpBuf);
				int idx = tmpStr.indexOf("TransactionDate");
				tmpStr = tmpStr.substring(idx);
				tmpBuf = tmpStr.toCharArray();
				System.arraycopy(tmpBuf, 0, cbuf, offset, tmpBuf.length);
				return tmpBuf.length;
			}
			return super.read(cbuf, offset, length);
		}

		@Override
		public int read(CharBuffer target) throws IOException {
			return super.read(target);
		}

	}

	@Override
	protected SimpleDateFormat getDateFormat() {
		return new SimpleDateFormat("MM/dd/yy");
	}

}
