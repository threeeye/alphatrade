package com.isoplane.at.adapter.ib;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import com.ib.client.Contract;
import com.isoplane.at.commons.store.ATPersistentSecurity;

//See https://interactivebrokers.github.io/tws-api/tick_types.html#halted&gsc.tab=0
public class ATIBSecurity extends ATPersistentSecurity { // ATMarketData {// 

	private static final long serialVersionUID = 1L;

	public static final String MIN_TICK = "mintk";
	transient private long _optionUpdate; // Used for stock type to schedule nightly option chain refresh
	transient private Contract _contract;
	transient private Set<Double> strikes;
	transient private Set<String> expirations;
	transient private Set<String> exchanges;
	transient private Set<Integer> _genericTicks;
	transient boolean _isOption;

	public ATIBSecurity() {
	}

	public ATIBSecurity(String occId_) {
		super.setOccId(occId_);
		this._isOption = occId_ != null && occId_.length() > 10;
		// super(occId_);
		// if (isOption()) {
		if (_isOption) {
			setOptionUpdate(0);
		}
	}

	public long getOptionUpdate() {
		return _optionUpdate;
	}

	public void setOptionUpdate(long value_) {
		this._optionUpdate = value_;
	}

	public Contract getContract() {
		return _contract;
	}

	public void setContract(Contract contract_) {
		this._contract = contract_;
	}

	public Set<Double> getStrikes() {
		if (strikes == null) {
			strikes = new TreeSet<>();
		}
		return strikes;
	}

	public void setStrikes(Set<Double> strikes) {
		this.strikes = strikes;
	}

	public Set<String> getExpirations() {
		if (expirations == null) {
			expirations = new TreeSet<>();
		}
		return expirations;
	}

	public void setExpirations(Set<String> expirations) {
		this.expirations = expirations;
	}

	public Set<String> getExchanges() {
		if (exchanges == null) {
			exchanges = new TreeSet<>();
		}
		return exchanges;
	}

	public void setExchanges(Set<String> exchanges) {
		this.exchanges = exchanges;
	}

	public Double getMinTick() {
		return (Double) get(MIN_TICK);
	}

	public void setMinTick(Double minTick) {
		put(MIN_TICK, minTick);
	}

	public Set<Integer> getGenericTicks() {
		if (_genericTicks == null) {
			_genericTicks = new HashSet<>();
		}
		return _genericTicks;
	}

	public void setGenericTicks(Set<Integer> genericTicks) {
		this._genericTicks = genericTicks;
	}
	
	public boolean isOption() {
		return this._isOption;
	}

}
