package com.isoplane.at.adapter.etrade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.isoplane.at.adapter.ATBaseOAuth10aAdapter;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATEtradeAdapter extends ATBaseOAuth10aAdapter {

	static final String SOURCE_KEY = "Etrd";
	static public final String ID = "etrade";

	static final Logger log = LoggerFactory.getLogger(ATEtradeAdapter.class);

	static private ATEtradeAdapter _instance;

	// private long _lastRequestTime = 0;
	// private long _requestSuccessCount = 0;
	// private long _requestFailureCount = 0;
	// private RequestConfig _httpReqConfig;

	protected ATEtradeAdapter() {
		super(ID, new ATEtradeApi());
	}

	static public ATEtradeAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATEtradeAdapter();
			_instance.init();
		}
		return _instance;
	}

	private void init() {
		// Configuration config = ATConfigUtil.config();
		// int httpTimeout = config.getInt("etrade.httpTimeout");
		// _httpReqConfig = RequestConfig.custom()
		// .setConnectTimeout(httpTimeout)
		// .setConnectionRequestTimeout(httpTimeout)
		// .setSocketTimeout(httpTimeout)
		// .build();
		// log.debug(String.format("HTTP timeout [%d]", httpTimeout));
	}

	public String getAuthorizationUrl() {
		String url = super.getAuthorizationUrl();
		log.info(String.format("getAuthorizationUrl: %s", url));
		return url;
	}

	public OAuth1AccessToken renewAccessToken() {
		OAuth1AccessToken o = super.renewAccessToken();
		return o;
	}

	public static class ATEtradeApi extends DefaultApi10a {

		private String _accessTokenEndpoint;
		private String _requestTokenEndpoint;
		private String _authorizationUrl;

		public ATEtradeApi() {
		}

		private void updateUrls() {
			// Configuration config = ATConfigUtil.config();
			String baseUrl = ATConfigUtil.config().getString("etrade.url");
			this._accessTokenEndpoint = String.format("%s/oauth/access_token", baseUrl);
			this._requestTokenEndpoint = String.format("%s/oauth/request_token", baseUrl);
			this._authorizationUrl = String.format("%s/e/t/etws/authorize?token=%%s", baseUrl);
		}

		@Override
		public String getAccessTokenEndpoint() {
			this.updateUrls();
			return this._accessTokenEndpoint;
		}

		@Override
		public String getRequestTokenEndpoint() {
			this.updateUrls();
			return this._requestTokenEndpoint;
		}

		// @Override
		// public String getAuthorizationUrl(Token requestToken) {
		// this.updateUrls();
		// return String.format(this._authorizationUrl, requestToken.getToken());
		// }

		@Override
		protected String getAuthorizationBaseUrl() {
			this.updateUrls();
			return this._authorizationUrl;
			// return String.format(this._authorizationUrl, requestToken.getToken());
		}

	}

}
