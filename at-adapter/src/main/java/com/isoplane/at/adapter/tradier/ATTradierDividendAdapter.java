package com.isoplane.at.adapter.tradier;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.isoplane.at.adapter.IATDividendAdapter;
import com.isoplane.at.commons.model.ATDividend;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATDividend;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;

public class ATTradierDividendAdapter extends ATTradierBaseAdapter implements IATDividendAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierDividendAdapter.class);

	// static final SimpleDateFormat dfDay = new SimpleDateFormat("EEE");
	// static final SimpleDateFormat trdDF = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	// static final SimpleDateFormat atDF = new SimpleDateFormat("yyyyMMddHHmm");
	static final SimpleDateFormat yyyy_mm_ddFormat = ATFormats.DATE_yyyy_MM_dd.get();
	static final SimpleDateFormat ds8Format = ATFormats.DATE_yyyyMMdd.get();
	static private Gson _gson = new Gson();
	static private long _lastRuntime;

	public static void main(String[] args) throws Exception {
		// PropertiesConfiguration prop = new PropertiesConfiguration();
		// prop.read(new FileReader("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties"));

		ATConfigUtil.init("C://Dev/workspace/alphatrade/NOCHECKIN-local.properties", false);

		String[] symbols = new String[] { "AAPL", "FMN", "TRIP", "SPY" };

		Set<IATSymbol> testSet = new TreeSet<>();
		for (String sym : symbols) {
			IATSymbol isym = new ATSymbol(sym);
			testSet.add(isym);
		}

		ATTradierDividendAdapter adapter = new ATTradierDividendAdapter();
		Map<String, IATDividend> history = adapter.getDividends(testSet);
		log.info(String.format("Dividend yield [%d]: %s", history.size(), history));
	}

	public ATTradierDividendAdapter() {
		super();
		_lastRuntime = System.currentTimeMillis();
	}

	@Override
	public Map<String, IATDividend> getDividends(Set<IATSymbol> symbols_) {
		Map<String, IATDividend> result = new TreeMap<>();
		int count = 1;
		List<List<IATSymbol>> symChunks = Lists.partition(new ArrayList<>(symbols_), 10);
		for (List<IATSymbol> symbols : symChunks) {
			try {
				log.debug(String.format("[%3d/%3d] getDividends %s", count++, symChunks.size(), symbols));
				Map<String, IATDividend> symResult = getDividendHistory(symbols);
				if (symResult != null) {
					result.putAll(symResult);
				}
			} catch (Exception ex) {
				log.error(String.format("getDividends Error [%s]: %s", symbols, ex.getMessage()));
			}
		}
		return result;
	}

	private Map<String, IATDividend> getDividendHistory(List<IATSymbol> symbols_) {
		if (symbols_ == null || symbols_.isEmpty())
			return null;
		try {
			long delta = System.currentTimeMillis() - _lastRuntime;
			if (delta < 500) {
				Thread.sleep(delta + 100);
			}
			String trdSyms = symbols_.stream().map(s -> s.convert(PROVIDER_KEY)).collect(Collectors.joining(","));
			log.trace(String.format("getDividendHistory [%s]", trdSyms));
			String url = String.format("/beta/markets/fundamentals/dividends?symbols=%s", trdSyms);
			String content = getHttp(url);
			// log.info("content: " + content);
			Map<String, IATDividend> result = parseDvidendHistory(symbols_, content);
			// Thread.sleep(500);
			return result;
		} catch (Exception ex) {
			log.error(String.format("getDividendHistory [%s]", symbols_), ex);
			return null;
		}
	}

	private Map<String, IATDividend> parseDvidendHistory(List<IATSymbol> symbols_, String json_) {
		Map<String, String> convMap = symbols_.stream().collect(Collectors.toMap(s -> s.convert(PROVIDER_KEY), s -> s.getId()));
		Map<String, IATDividend> resultMap = new TreeMap<>();
		Date now = new Date();
		String nowStr = yyyy_mm_ddFormat.format(now);
		Date days400 = DateUtils.addDays(now, -400);
		String days400Str = yyyy_mm_ddFormat.format(days400);
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonArray rootArray;
		if (root.isJsonArray()) {
			rootArray = root.getAsJsonArray();
		} else {
			rootArray = new JsonArray();
			rootArray.add(root);
		}
		for (JsonElement segment : rootArray) {
			try {
				if (segment.isJsonNull() || !segment.isJsonObject()) {
					continue;
				}
				JsonObject so = segment.getAsJsonObject();
				String trdSymbol = so.get("request").getAsString();
				String symbol = convMap.get(trdSymbol);
				JsonElement results = so.get("results");
				JsonArray resultsArray;
				if (results == null || results.isJsonNull()) {
					continue;
				} else if (results.isJsonArray()) {
					resultsArray = results.getAsJsonArray();
				} else {
					resultsArray = new JsonArray();
					resultsArray.add(results);
				}
				TreeSet<ATTradierTempDiv> divList = new TreeSet<>((a, b) -> a.exDateStr.compareTo(b.exDateStr));
				for (JsonElement result : resultsArray) {
					if (result.isJsonNull() || !result.isJsonObject()) {
						continue;
					}
					JsonObject ro = result.getAsJsonObject();
					if (!"Stock".equals(ro.get("type").getAsString()))
						continue;
					JsonObject to = ro.getAsJsonObject("tables");
					JsonElement cashDividends = to != null && !to.isJsonNull() ? to.get("cash_dividends") : null;
					if (cashDividends == null || !cashDividends.isJsonArray())
						continue;
					JsonArray divArray = cashDividends.getAsJsonArray();
					for (JsonElement div : divArray) {
						if (div.isJsonNull() || !div.isJsonObject()) {
							continue;
						}
						JsonObject divo = div.getAsJsonObject();
						String exDateStr = divo.get("ex_date").getAsString();
						if (StringUtils.isBlank(exDateStr) || exDateStr.compareTo(days400Str) < 0)
							continue;
						ATTradierTempDiv temp = new ATTradierTempDiv();
						temp.exDateStr = exDateStr;
						temp.div = divo.get("cash_amount").getAsDouble();
						temp.freq = divo.get("frequency").getAsDouble();
						temp.payDateStr = divo.get("pay_date").getAsString();
						temp.recDateStr = divo.get("record_date").getAsString();
						if (temp.div != null && temp.freq != null) {
							divList.add(temp);
						}
					}
				}
				if (divList.isEmpty())
					continue;

				ATTradierTempDiv last = divList.last();

				ATDividend div = new ATDividend();
				IATDividend.setSource(div, PROVIDER_KEY);
				div.setOccId(symbol); // NOTE: Reverted during parsing
				// div.setChanged(true);
				// div.setSource(PROVIDER_KEY);
				div.setDividend(last.div);
				div.setDividendFrequency(last.freq.intValue());

				if (last.exDateStr.compareTo(nowStr) > 0) {
					// String atid = String.format("%s %s", symbol, last.exDateStr);
					// div.setAtId(atid);
					div.setDividendExDateStr(last.exDateStr.replace("-", ""));
					div.setDividendPayDateStr(last.payDateStr.replace("-", ""));
					div.setDividendRecordDateStr(last.recDateStr.replace("-", ""));
					resultMap.put(symbol, div);
				} else {
					Date estDate = null;
					for (ATTradierTempDiv ldiv : divList) {
						String minExDateStr = ldiv.exDateStr;
						Date minExDate = yyyy_mm_ddFormat.parse(minExDateStr);
						Date nextExDate = DateUtils.addYears(minExDate, 1);
						if (nextExDate.compareTo(now) > 0) {
							estDate = nextExDate;
							break;
						}
					}
					if (estDate != null) {
						// String atid = String.format("%s %s", symbol, yyyy_mm_ddFormat.format(estDate));
						// div.setAtId(atid);
						div.setDividendEstimatedDS8(ds8Format.format(estDate));
						// log.debug(String.format("Symbol [%-6s]: %s, %.4f, %d", symbol, div.getDividendEstimatedDate(), div.getDividend(),
						// div.getDividendFrequency()));
						resultMap.put(symbol, div);
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error parseDvidendHistory [%s]: %s", segment, ex.getMessage()));
			}
		}
		return resultMap;
	}

	public static class ATTradierTempDiv {
		String exDateStr;
		String payDateStr;
		String recDateStr;
		Double div;
		Double freq;

		@Override
		public String toString() {
			return String.format("[%s, %.4f, %.2f]", exDateStr, div, freq);
		}

		@Override
		public int hashCode() {
			return exDateStr == null ? -1 : exDateStr.hashCode();
		}
	}

}
