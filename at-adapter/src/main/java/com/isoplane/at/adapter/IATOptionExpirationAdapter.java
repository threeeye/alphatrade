package com.isoplane.at.adapter;

import java.util.Map;
import java.util.Set;

import com.isoplane.at.commons.model.IATOptionExpiration;
import com.isoplane.at.commons.model.IATSymbol;

public interface IATOptionExpirationAdapter {
	
	Map<String, IATOptionExpiration> getOptionExpirations(Set<IATSymbol> symbols);
}
