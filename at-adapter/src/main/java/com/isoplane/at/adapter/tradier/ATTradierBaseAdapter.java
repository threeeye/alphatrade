package com.isoplane.at.adapter.tradier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.util.ATConfigUtil;

public abstract class ATTradierBaseAdapter {

	static final Logger log = LoggerFactory.getLogger(ATTradierBaseAdapter.class);
	static public final String SOURCE_KEY = "trd";
	static public final String PROVIDER_KEY = "tradier";

	// static Map<String, String> _converterMap;
	// static {
	// _converterMap = new HashMap<>();
	// _converterMap.put("BF.B", "BF/B");
	// _converterMap.put("BRK.B", "BRK/B");SBB87677
	// _converterMap.put("BRK.A", "BRK/A");
	// _converterMap.put("BRK-A", "BRK/A");
	// _converterMap.put("$NASDAQ", "COMP");
	// }
	// static String mapSymbol(String sym_) {
	// String symbol = _converterMap.get(sym_);
	// return symbol != null ? symbol : sym_;
	// }

	private String _urlBase = "https://api.tradier.com";
	// private int _maxSize = 100;
	private String _tradierToken;

	public ATTradierBaseAdapter() {
		_tradierToken = ATConfigUtil.config().getString("tradier.token");
		// _converterMap = cnvMap_;
	}

	protected String getHttp(String url_) {
		CloseableHttpResponse response = null;
		try {
			// long gap = System.currentTimeMillis() - _lastProcessTime;
			// if (gap < _processPeriod) {
			// Thread.sleep(_processPeriod - gap);
			// }

			String url = _urlBase + url_;
			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(url);
			httpGet.setHeader("Accept", "application/json");
			httpGet.setHeader("Authorization", _tradierToken);
			httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0");

			response = httpclient.execute(httpGet);
			try {
				// @SuppressWarnings("unused")
				Header h1 = response.getFirstHeader("X-Ratelimit-Allowed");
				@SuppressWarnings("unused")
				Header h2 = response.getFirstHeader("X-Ratelimit-Used");
				Header h3 = response.getFirstHeader("X-Ratelimit-Available");
				@SuppressWarnings("unused")
				Header h4 = response.getFirstHeader("X-Ratelimit-Expiry");
				if (h3 != null) {
					String h1v = h1.getValue();
					String h3v = h3.getValue();
					int available = Integer.parseInt(h3v);
					if (available < 10) {
						long timeout = 250L;
						log.warn(String.format("Rate limit approaching [%d/%s]. Throttling [%dms]...", available, h1v, timeout));
						Thread.sleep(timeout);
					}
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing headers"), ex);
			}

			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (Exception ex) {
			log.error(String.format("Error requesting: %s", url_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

}
