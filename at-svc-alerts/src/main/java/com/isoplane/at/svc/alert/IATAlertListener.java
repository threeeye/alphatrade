package com.isoplane.at.svc.alert;

import com.isoplane.at.rules.notification.IATNotificationBuilder;

public interface IATAlertListener {

	void notify(IATNotificationBuilder builder);
}
