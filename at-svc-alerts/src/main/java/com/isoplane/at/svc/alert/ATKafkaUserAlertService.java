package com.isoplane.at.svc.alert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections4.map.LRUMap;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATAlertRule.ATUserAlertRulePack;
import com.isoplane.at.commons.model.protobuf.ATProtoAlertRuleWrapper;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATPortfolioService;
import com.isoplane.at.commons.service.IATProtoAlertRuleSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.alertrule.ATKafkaAlertRuleConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer.DefaultStatusRunner;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.rules.ATRuleBuilder;
import com.isoplane.at.rules.model.ATSecurityRuleGroup;
import com.isoplane.at.rules.notification.IATNotificationBuilder;
import com.isoplane.at.users.ATUserManager;

public class ATKafkaUserAlertService implements IATSystemStatusProvider, IATAlertListener, IATProtoAlertRuleSubscriber {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUserAlertService.class);
	static private String ID;

	private ConcurrentHashMap<String, IATNotificationBuilder> _pushMap;
	private ATKafkaSystemStatusProducer _statusProducer;
	private ATKafkaPushNotificationProducer _pushProducer;
	private ATUserAlertRuleProcessor _alertPrc;
	private boolean _isRunning;
	private long _msgCount = 0;
	private long _pushDelay;
	private final CountDownLatch _serviceLatch;
	private DelayQueue<DelayedPushNotification> _delayedPushQueue;
	private ATRuleBuilder _ruleBuilder;
	private IATPortfolioService _ptfoSvc;
	private IATUserProvider _userSvc;
	private Map<String, String> _duplicateMap;

	public static void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATKafkaRegistry.register("user_mkt_svc",
					ATKafkaAlertRuleConsumer.class,
					ATKafkaMarketDataConsumer.class,
					ATKafkaMarketDataPackConsumer.class,
					ATKafkaPushNotificationProducer.class,
					ATKafkaSystemStatusProducer.class);

			String ipStr = ATSysUtils.getIpAddress("n/a");
			String portStr = config.getString("web.port");
			ID = StringUtils.isBlank(portStr) ? String.format("%s:%s", ATKafkaUserAlertService.class.getSimpleName(), ipStr)
					: String.format("%s:%s:%s", ATKafkaUserAlertService.class.getSimpleName(), ipStr, portStr);

			ATKafkaUserAlertService instance = new ATKafkaUserAlertService();
			instance.init();
			instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

			instance.shutdown();

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaUserAlertService.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaUserAlertService() {
		this._serviceLatch = new CountDownLatch(1);

		this._duplicateMap = new LRUMap<>(ATConfigUtil.config().getInt("usr_alert.duplicate.map.size", 100));
		this._delayedPushQueue = new DelayQueue<>();
		this._pushMap = new ConcurrentHashMap<>();

		this._ptfoSvc = new ATPortfolioManager(false);
		this._ptfoSvc.initService();

		this._userSvc = new ATUserManager(null, false);
		this._userSvc.initService();

		this._alertPrc = new ATUserAlertRuleProcessor();
		this._alertPrc.subscribe(this);
	}

	public void init() {
		Configuration config = ATConfigUtil.config();
		_pushDelay = config.getLong("push.delay", 1000);

		ATKafkaAlertRuleConsumer ruleConsumer = ATKafkaRegistry.get(ATKafkaAlertRuleConsumer.class);
		ruleConsumer.subscribe(this);

		ATKafkaMarketDataConsumer marketDataConsumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
		marketDataConsumer.subscribe(this._alertPrc);
		ATKafkaMarketDataPackConsumer marketDataPackConsumer = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
		marketDataPackConsumer.subscribe(this._alertPrc);
		_pushProducer = ATKafkaRegistry.get(ATKafkaPushNotificationProducer.class);
		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

		ATSystemStatusManager.register(this);

		List<ATUserAlertRulePack> alertPacks = this._userSvc.getUserAlerts(null, null);// this._mongoDb.queryUserAlerts(null);
		Map<String, Set<String>> userPositionMap = new HashMap<>();
		for (String userId : alertPacks.stream().map(p -> p.usrId).collect(Collectors.toSet())) {
			Set<String> userSymbols = this._ptfoSvc.getActiveOccIds(userId, true).stream().map(p -> ATModelUtil.getSymbol(p))
					.collect(Collectors.toSet());
			userPositionMap.put(userId, userSymbols);
		}
		this._ruleBuilder = new ATRuleBuilder();
		List<ATSecurityRuleGroup> alertRules = new ArrayList<>();
		for (ATUserAlertRulePack pack : alertPacks) {
			String userId = pack.usrId;
			String symbol = pack.occId;
			Set<String> activeSymbols = userPositionMap.get(userId);
			boolean isActive = activeSymbols != null && activeSymbols.contains(symbol);
			ATSecurityRuleGroup rule = this._ruleBuilder.build(pack, isActive);
			alertRules.add(rule);
		}
		this._alertPrc.updateRules(alertRules);
	}

	public void start() {
		this._isRunning = true;
		ATKafkaRegistry.start();
		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, DefaultStatusRunner.class.getSimpleName()),
				new DefaultStatusRunner(ATKafkaUserAlertService.ID, this, this._statusProducer));
		ATExecutors.submit(String.format("%s.%s", pre, DelayedPushRunner.class.getSimpleName()), new DelayedPushRunner());
	}

	public void shutdown() {
		this._isRunning = false;
		ATKafkaRegistry.stop();
	}

	@Override
	public void notify(IATNotificationBuilder builder_) {
		String id = builder_.getId();
		this._pushMap.put(id, builder_);

		DelayedPushNotification dpn = new DelayedPushNotification(id, this._pushDelay);
		if (!this._delayedPushQueue.contains(dpn)) {
			this._delayedPushQueue.add(dpn);
		}
	}

	@Override
	public void notifyAlertRule(ATProtoAlertRuleWrapper wrapper_) {
		if (wrapper_ == null || wrapper_.proto() == null)
			return;
		String symbol = wrapper_.proto().getTargetId();
		String userId = wrapper_.proto().getUserId();
		String ruleJson = wrapper_.proto().getData();
		if (StringUtils.isBlank(ruleJson)) {
			this._alertPrc.deleteRule(symbol, userId);
		} else {
			Set<String> userSymbols = this._ptfoSvc.getActiveOccIds(userId, true).stream().map(p -> ATModelUtil.getSymbol(p))
					.collect(Collectors.toSet());
			boolean isActive = userSymbols != null && userSymbols.contains(symbol);
			ATSecurityRuleGroup rule = this._ruleBuilder.build(wrapper_.value(), isActive);
			this._alertPrc.updateRules(Arrays.asList(rule));
		}
	}

	public void sendPushNotification(DelayedPushNotification pn_) {
		if (pn_ == null)
			return;
		IATNotificationBuilder pushBuilder = _pushMap.remove(pn_.id);
		if (pushBuilder == null)
			return;
		PushNotification push = pushBuilder.build();
		String tag = String.format("%s:%s", push.getTag(), push.getUserIdsList());
		String currentTitle = push.getTitle();
		String previousTitle = this._duplicateMap.get(tag);
		if (previousTitle != null && previousTitle.equals(currentTitle))
			return;
		this._duplicateMap.put(tag, currentTitle);
		this._pushProducer.send(push);
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.queue", String.format("%d", this._delayedPushQueue.size()));
		status.put("count.messages", String.format("%d", this._msgCount));
		status.put("isRunning", String.format("%b", this._isRunning));
		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	private class DelayedPushRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (ATKafkaUserAlertService.this._isRunning) {
					Configuration config = ATConfigUtil.config();
					long queueInterval = config.getLong("usr_alert.period");
					DelayedPushNotification pn = ATKafkaUserAlertService.this._delayedPushQueue.poll(queueInterval, TimeUnit.MILLISECONDS);
					if (pn == null)
						continue;
					ATKafkaUserAlertService.this.sendPushNotification(pn);
					ATKafkaUserAlertService.this._msgCount++;
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (ATKafkaUserAlertService.this._isRunning) {
					run();
				}
			}
			ATKafkaUserAlertService.this._serviceLatch.countDown();
		}
	}

	private static class DelayedPushNotification implements Delayed {

		public String id;
		public long timeout;

		public DelayedPushNotification(String id_, long delay_) {
			this.id = id_;
			timeout = System.currentTimeMillis() + delay_;
		}

		@Override
		public int compareTo(Delayed other_) {
			long otherTimeout = other_.getDelay(TimeUnit.MILLISECONDS);
			int result = Long.compare(timeout, otherTimeout);
			return result;
		}

		@Override
		public long getDelay(TimeUnit unit_) {
			long remainingMs = timeout - System.currentTimeMillis();
			return unit_.convert(remainingMs, TimeUnit.MILLISECONDS);
		}

		@Override
		public boolean equals(Object other_) {
			boolean result = other_ instanceof DelayedPushNotification && this.id.equals(((DelayedPushNotification) other_).id);
			return result;
		}

		@Override
		public int hashCode() {
			return this.id.hashCode();
		}

	}

}
