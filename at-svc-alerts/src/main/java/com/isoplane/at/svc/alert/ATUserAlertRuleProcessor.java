package com.isoplane.at.svc.alert;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.rules.model.ATSecurityRuleGroup;
import com.isoplane.at.rules.notification.IATNotificationBuilder;

public class ATUserAlertRuleProcessor implements IATProtoMarketSubscriber, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATUserAlertRuleProcessor.class);

	// private IATPortfolioService pftoSvc;
	private Set<IATAlertListener> subscribers;
	private Map<String, Map<String, ATSecurityRuleGroup>> ruleMap;
	// private Map<String, Set<String>> userPositionMap;
	private Map<String, MarketData> mktMap;
	private Set<String> alertOccIds;

	public ATUserAlertRuleProcessor() {
		this.alertOccIds = new HashSet<>();
		this.mktMap = new ConcurrentHashMap<>();
		// this.pftoSvc = ptfoSvc_;
		this.ruleMap = new ConcurrentHashMap<>();
		// this.rulesEngine = new ATRuleBuilder();
		this.subscribers = new HashSet<>();
		// this.userPositionMap = new ConcurrentHashMap<>();

		ATSystemStatusManager.register(this);
	}

	public int updateRules(List<ATSecurityRuleGroup> rules_) {
		if (rules_ == null)
			return 0;
		for (ATSecurityRuleGroup rule : rules_) {
			this.addRule(rule);
		}
		this.updateOccIds();
		return rules_.size();
	}

	public void subscribe(IATAlertListener listener_) {
		this.subscribers.add(listener_);
	}

	private void publish(IATNotificationBuilder builder_) {
		if (builder_ == null)
			return;
		for (IATAlertListener listener : subscribers) {
			try {
				listener.notify(builder_);
			} catch (Exception ex) {
				log.error(String.format("notify Error [%s]", builder_.getId()), ex);
			}
		}
	}

	private void process(MarketData mkt_) {
		String occId = mkt_.getId();
		if (!this.alertOccIds.contains(occId))
			return;
		this.mktMap.put(occId, mkt_);
		String symbol = mkt_.getIdUl();
		if (StringUtils.isBlank(symbol)) {
			symbol = occId;
		}
		Map<String, ATSecurityRuleGroup> userMap = this.ruleMap.get(symbol);
		if (userMap == null)
			return;
		outer: for (ATSecurityRuleGroup rule : userMap.values()) {
			Set<String> activeOccIds = rule.getActiveOccIds();
			Map<String, MarketData> ruleMktMap = new HashMap<>();
			for (String activeOccId : activeOccIds) {
				MarketData mkt = activeOccId.equals(occId) ? mkt_ : this.mktMap.get(activeOccId);
				if (mkt == null)
					continue outer;
				ruleMktMap.put(activeOccId, mkt);
			}
			if (!rule.trigger(ruleMktMap))
				continue;
			IATNotificationBuilder alertBuilder = rule.getAlertBuilder();
			this.publish(alertBuilder);
		}
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
		for (ATProtoMarketDataWrapper wrapper : wrappers_) {
			this.process(wrapper.proto());
		}
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper wrapper_) {
		String symbol = wrapper_.proto().getSymbols(0);
		if (!this.alertOccIds.contains(symbol))
			return;
		List<MarketData> mktList = wrapper_.proto().getMarketDataList();
		for (MarketData mkt : mktList) {
			this.process(mkt);
		}
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper mktStatusWrapper_) {
		// NOTE: Ignored for now
	}

	private void addRule(ATSecurityRuleGroup rule_) {
		if (rule_ == null)
			return;
		String symbol = rule_.getSymbol();
		Map<String, ATSecurityRuleGroup> userMap = this.ruleMap.get(symbol);
		if (userMap == null) {
			userMap = new ConcurrentHashMap<>();
			this.ruleMap.put(symbol, userMap);
		}
		String userId = rule_.userId;
		userMap.put(userId, rule_);
	}

	public void deleteRule(String symbol_, String userId_) {
		Map<String, ATSecurityRuleGroup> userMap = this.ruleMap.get(symbol_);
		if (userMap != null) {
			userMap.remove(userId_);
		}
		this.updateOccIds();
	}

	private int updateOccIds() {
		Set<String> activeOccIds = this.ruleMap.values().stream()
				.map(v -> v.values()).flatMap(Collection::stream).map(r -> r.getActiveOccIds())
				.flatMap(Set::stream).collect(Collectors.toSet());
		Set<String> activeSymbols = activeOccIds.stream().map(o -> ATModelUtil.getUnderlyingOccId(o)).collect(Collectors.toSet());
		@SuppressWarnings("serial")
		Set<String> alertOccIds = new HashSet<String>() {
			{
				addAll(activeSymbols);
				addAll(activeOccIds);
			}
		};
		this.alertOccIds = alertOccIds;

		// Collect active user positions (on symbol level)
		// Map<String, Set<String>> userPositionMap = new ConcurrentHashMap<>();
		// Set<String> userIds = this.ruleMap.values().stream().map(r -> r.keySet()).flatMap(Set::stream)
		// .map(u -> ATModelUtil.getSymbol(u)).collect(Collectors.toSet());
		// for (String userId : userIds) {
		// Set<String> positions = this.pftoSvc.getActiveOccIds(userId, true);
		// if (positions != null && !positions.isEmpty()) {
		// userPositionMap.put(userId, positions);
		// }
		// }
		// this.userPositionMap = userPositionMap;
		return alertOccIds.size();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("size.marketMap", String.format("%d", this.mktMap.size()));
		status.put("size.occIds", String.format("%d", this.alertOccIds.size()));
		int ruleCount = this.ruleMap.values().stream().map(v -> v.values().size()).reduce(0, Integer::sum);
		status.put("size.rules", String.format("%d", ruleCount));
		long userCount = this.ruleMap.values().stream().map(r -> r.keySet()).flatMap(Set::stream).count();
		status.put("size.users", String.format("%d", userCount));
		return status;
	}

}
