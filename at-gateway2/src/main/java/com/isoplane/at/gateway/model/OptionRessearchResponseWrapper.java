package com.isoplane.at.gateway.model;

public class OptionRessearchResponseWrapper {

	public OptionRessearchResponse data;
	
	public static class OptionRessearchResponse {

		public String ulSize;
		public OptionIdeaEquityGroup[] strategies;
	}

	
}
