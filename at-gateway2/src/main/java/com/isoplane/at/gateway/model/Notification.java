package com.isoplane.at.gateway.model;

public class Notification {
	
	public String title;
	public String body;
	public String icon;
	public String click_action;
	public String tag;
	public String mode;
	
}
