package com.isoplane.at.gateway.util;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.gateway.model.TdnRequestContext;

public class HttpTools {

	static final Logger log = LoggerFactory.getLogger(HttpTools.class);

	static String _serverA;// = "127.0.0.1:8082";
	static String _serverB;

	private CloseableHttpClient _httpClient;

	public HttpTools init(Configuration config_) {

		_serverA = String.format("%s:%d", config_.getString("gateway.source.ip"), config_.getInt("gateway.source.port"));
		_serverB = String.format("%s:%d", config_.getString("gateway.pyserver.ip"), config_.getInt("gateway.pyserver.port"));

		_httpClient = HttpClients.createDefault();

		new Timer().scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				try {
					// getOpportunities();
				} catch (Exception ex) {
					log.error("Error", ex);
				}
			}
		}, 10000, 10000);
		return this;
	}

	public String getPricing(String symbol_, boolean includeOptions_) {
		log.info("getPricing: " + symbol_);
		// TdnToken token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/pricing/%s", _serverA, symbol_);
		HttpGet httpGet = new HttpGet(url);
		// httpGet.setHeader("uid", token.getUserId());

		// CloseableHttpResponse response = _httpClient.execute(httpGet);
		try (CloseableHttpResponse response = _httpClient.execute(httpGet)) {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
			// } finally {
			// response.close();
		}

	}

	public String getOptionResearch(String queryString_) throws IOException {
		log.info("getOptionResearch: " + queryString_);
		IATSession token = TdnRequestContext.getToken();
		String params = StringUtils.isBlank(queryString_) ? "" : String.format("?%s", queryString_);
		String url = String.format("http://%s/api/v2/spreads%s", _serverA, params);
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("User", "mironroth@gmail.com");
		httpGet.setHeader("uid", token.getUserId());
		// httpGet.
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String userPrep(String userId_, boolean isActive_) throws IOException {
		String url = String.format("http://%s/api/v2/user-prep", _serverA);
		// TdnToken token = TdnRequestContext.getToken();
		String entityStr = String.format("{\"uid\":\"%s\",\"isActive\":%b}", userId_, isActive_);
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("uid", userId_);
		httpPost.setEntity(new StringEntity(entityStr));
		CloseableHttpResponse response = _httpClient.execute(httpPost);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String getEquityResearch(String queryString_) throws IOException {
		log.info("getEquityResearch: " + queryString_);
		IATSession token = TdnRequestContext.getToken();
		String params = StringUtils.isBlank(queryString_) ? "" : String.format("?%s", queryString_);
		String url = String.format("http://%s/api/v2/equity%s", _serverA, params);
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("User", "mironroth@gmail.com");
		httpGet.setHeader("uid", token.getUserId());
		// httpGet.
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String getPortfolio(String queryString_) throws IOException {
		log.info("getPortfolio: " + queryString_);
		IATSession token = TdnRequestContext.getToken();
		String params = StringUtils.isBlank(queryString_) ? "" : String.format("?%s", queryString_);
		String url = String.format("http://%s/api/v2/portfolio%s", _serverA, params);
		HttpGet httpGet = new HttpGet(url);
		// httpGet.setHeader("User", "mironroth@gmail.com");
		httpGet.setHeader("uid", token.getUserId());
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String forwardGet(String path_, String queryString_) throws IOException {
		log.info(String.format("forwardGet: %s - %s", path_, queryString_));
		IATSession token = TdnRequestContext.getToken();
		String params = StringUtils.isBlank(queryString_) ? "" : String.format("?%s", queryString_);
		String url = String.format("http://%s/api/v2%s%s", _serverA, path_, params);
		HttpGet httpGet = new HttpGet(url);
		// httpGet.setHeader("User", "mironroth@gmail.com");
		httpGet.setHeader("uid", token.getUserId());
		httpGet.setHeader(HTTP.CONTENT_ENCODING, "gzip");
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String forwardPost(String path_, String body_) throws IOException {
		log.info("forwardPost: " + path_);
		IATSession token = TdnRequestContext.getToken();
		int pathIdx = path_.indexOf("/v2/");
		String path = pathIdx < 0 ? path_ : path_.substring(pathIdx + 4);
		if (!path.startsWith("/")) {
			path = "/" + path;
		}
		String url = String.format("http://%s/api/v2%s", _serverA, path);
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("uid", token.getUserId());
		if (StringUtils.isNotBlank(body_)) {
			httpPost.setEntity(new StringEntity(body_));
		}
		CloseableHttpResponse response = _httpClient.execute(httpPost);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String postCommand(String command_, String body_, Map<String, String> headers_, Map<String, List<String>> params_) throws IOException {
		try {
			log.info("postCommand: " + command_);
			IATSession token = TdnRequestContext.getToken();
			if (token == null) {
				log.error(String.format("postCommand Error: Null Token"));
				return null;
			}

			String url = String.format("http://%s/api/v2/command/%s", _serverA, command_);
			URIBuilder uriBuilder = new URIBuilder(url);
			if (params_ != null) {
				params_.entrySet().forEach(p -> p.getValue().forEach(v -> uriBuilder.addParameter(p.getKey(), v)));
				// for (Entry<String, List<String>> entry : params_.entrySet()) {
				// for (String value : entry.getValue()) {
				// uriBuilder.addParameter(entry.getKey(), value);
				// }
				// }
			}
			HttpPost httpPost = new HttpPost(uriBuilder.build());
			httpPost.setHeader("uid", token.getUserId());
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpPost.setHeader(header.getKey(), header.getValue());
				}
			}
			if (StringUtils.isNotBlank(body_)) {
				httpPost.setEntity(new StringEntity(body_));
			}
			CloseableHttpResponse response = _httpClient.execute(httpPost);
			try {
				HttpEntity entity = response.getEntity();
				String responseStr = entity != null ? EntityUtils.toString(entity) : null;
				// log.info("Response: " + responseStr);
				return responseStr;
			} catch (Exception ex) {
				log.error("HTTP Error", ex);
				return null;
			} finally {
				response.close();
			}
		} catch (Exception ex) {
			throw new ATException(String.format("postCommand Error"), ex);
		}
	}

	public String deleteCommand(String command_, Map<String, String> headers_, Map<String, List<String>> params_) throws IOException {
		try {
			log.info("deleteCommand: " + command_);
			IATSession token = TdnRequestContext.getToken();
			if (token == null) {
				log.error(String.format("deleteCommand Error: Null Token"));
				return null;
			}

			String url = String.format("http://%s/api/v2/command/%s", _serverA, command_);
			URIBuilder uriBuilder = new URIBuilder(url);
			if (params_ != null) {
				params_.entrySet().forEach(p -> p.getValue().forEach(v -> uriBuilder.addParameter(p.getKey(), v)));
			}
			HttpDelete httpDelete = new HttpDelete(uriBuilder.build());
			httpDelete.setHeader("uid", token.getUserId());
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpDelete.setHeader(header.getKey(), header.getValue());
				}
			}
			CloseableHttpResponse response = _httpClient.execute(httpDelete);
			try {
				HttpEntity entity = response.getEntity();
				String responseStr = entity != null ? EntityUtils.toString(entity) : null;
				return responseStr;
			} catch (Exception ex) {
				log.error("deleteCommand HTTP Error", ex);
				return null;
			} finally {
				response.close();
			}
		} catch (Exception ex) {
			throw new ATException(String.format("deleteCommand Error"), ex);
		}
	}



	public String getCallback(Map<String, List<String>> params_, Map<String, String> headers_) throws Exception {
		log.info("callback");
		String url = String.format("http://%s/api/v2/callback", _serverA);
		URIBuilder builder = new URIBuilder(url);
		if (params_ != null) {
			for (Entry<String, List<String>> entry : params_.entrySet()) {
				String key = entry.getKey();
				List<String> values = entry.getValue();
				for (String value : values) {
					builder.addParameter(key, value);
				}
			}
		}
		URI uri = builder.build();
		HttpGet httpGet = new HttpGet(uri);
		if (headers_ != null) {
			for (Entry<String, String> header : headers_.entrySet()) {
				httpGet.setHeader(header.getKey(), header.getValue());
			}
		}
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		String responseStr = entity != null ? EntityUtils.toString(entity) : null;
		return responseStr;
	}

	public String getCommandA(String command_, Map<String, List<String>> params_, Map<String, String> headers_) throws Exception {
		log.info("getCommandA: " + command_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/command/%s", _serverA, command_);
		// try {
		URIBuilder builder = new URIBuilder(url);
		if (params_ != null) {
			for (Entry<String, List<String>> entry : params_.entrySet()) {
				String key = entry.getKey();
				List<String> values = entry.getValue();
				for (String value : values) {
					builder.addParameter(key, value);
				}
			}
		}
		URI uri = builder.build();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("uid", token.getUserId());
		if (headers_ != null) {
			for (Entry<String, String> header : headers_.entrySet()) {
				httpGet.setHeader(header.getKey(), header.getValue());
			}
		}
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		String responseStr = entity != null ? EntityUtils.toString(entity) : null;
		return responseStr;
	}

	public String getApiV2(String path_, Map<String, List<String>> params_, Map<String, String> headers_) throws Exception {
		log.info("getApiV2: " + path_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/%s", _serverA, path_);
		URIBuilder builder = new URIBuilder(url);
		if (params_ != null) {
			for (Entry<String, List<String>> entry : params_.entrySet()) {
				String key = entry.getKey();
				List<String> values = entry.getValue();
				for (String value : values) {
					builder.addParameter(key, value);
				}
			}
		}
		URI uri = builder.build();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("uid", token.getUserId());
		if (headers_ != null) {
			for (Entry<String, String> header : headers_.entrySet()) {
				httpGet.setHeader(header.getKey(), header.getValue());
			}
		}
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		HttpEntity entity = response.getEntity();
		String responseStr = entity != null ? EntityUtils.toString(entity) : null;
		return responseStr;
	}

	public String getCommandB(String command_, Map<String, List<String>> params_) {
		try {
			log.info("getCommandB: " + command_);
			// IATSession token = TdnRequestContext.getToken();
			String url = String.format("http://%s/api/v1/command/%s", _serverB, command_);
			// try {
			URIBuilder builder = new URIBuilder(url);
			if (params_ != null) {
				for (Entry<String, List<String>> entry : params_.entrySet()) {
					String key = entry.getKey();
					List<String> values = entry.getValue();
					for (String value : values) {
						builder.addParameter(key, value);
					}
				}
			}
			URI uri = builder.build();
			HttpGet httpGet = new HttpGet(uri);
			// httpGet.setHeader("uid", token.getUserId());
			CloseableHttpResponse response = _httpClient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
			// } catch (Exception ex) {
			// log.error("HTTP Error", ex);
			// return null;
			// } finally {
			// response.close();
			// }
		} catch (Exception ex) {
			String msg = String.format("getCommandB[%s]", command_);
			log.error(msg, ex);
			throw new ATException();
		}
	}

	public String postNote(String body_) throws IOException {
		log.info("postNote: " + body_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/note", _serverA);
		HttpPut httpPut = new HttpPut(url);
		httpPut.setHeader("uid", token.getUserId());
		httpPut.setEntity(new StringEntity(body_));
		CloseableHttpResponse response = _httpClient.execute(httpPut);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String getSecurityNew(String security_, Map<String, List<String>> queryMap) throws Exception {
		log.info("getSecurity: " + security_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/security/%s", _serverA, security_);
		URIBuilder builder = new URIBuilder(url);
		if (queryMap != null) {
			for (Entry<String, List<String>> entry : queryMap.entrySet()) {
				String key = entry.getKey();
				List<String> values = entry.getValue();
				for (String value : values) {
					builder.addParameter(key, value);
				}
			}
		}
		URI uri = builder.build();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("uid", token.getUserId());

		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String getSecurity(String security_, Map<String, String[]> queryMap) throws Exception {
		log.info("getSecurity: " + security_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/security/%s", _serverA, security_);
		URIBuilder builder = new URIBuilder(url);
		if (queryMap != null) {
			for (Entry<String, String[]> entry : queryMap.entrySet()) {
				String key = entry.getKey();
				String[] values = entry.getValue();
				for (String value : values) {
					builder.addParameter(key, value);
				}
			}
		}
		URI uri = builder.build();
		HttpGet httpGet = new HttpGet(uri);
		httpGet.setHeader("uid", token.getUserId());

		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}

	public String lookupSymbol(String symbol_) throws Exception {
		log.info("lookupSymbol: " + symbol_);
		IATSession token = TdnRequestContext.getToken();
		String url = String.format("http://%s/api/v2/lookup/%s", _serverA, symbol_);
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("uid", token.getUserId());
		CloseableHttpResponse response = _httpClient.execute(httpGet);
		try {
			HttpEntity entity = response.getEntity();
			String responseStr = entity != null ? EntityUtils.toString(entity) : null;
			// log.info("Response: " + responseStr);
			return responseStr;
		} catch (Exception ex) {
			log.error("HTTP Error", ex);
			return null;
		} finally {
			response.close();
		}
	}
}
