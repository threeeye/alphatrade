package com.isoplane.at.gateway.model;

import com.isoplane.at.commons.model.IATSession;

public class TdnRequestContext {

	static private ThreadLocal<IATSession> _token = new ThreadLocal<>();

	public static void setToken(IATSession token_) {
		_token.set(token_);
	}

	public static IATSession getToken() {
		return _token.get();
	}

	public static void clear() {
		_token.remove();
	}
}
