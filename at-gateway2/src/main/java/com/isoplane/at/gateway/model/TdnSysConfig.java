package com.isoplane.at.gateway.model;

import java.util.List;

public class TdnSysConfig {

	public String mode;
	public String serverUrl;
	public List<String> extraSymbols;

}
