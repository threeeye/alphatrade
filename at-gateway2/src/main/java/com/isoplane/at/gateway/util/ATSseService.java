package com.isoplane.at.gateway.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.WSMessageWrapper;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;

import io.javalin.http.sse.SseClient;

public class ATSseService implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATSseService.class);

	private Map<String, Set<SseClientWrapper>> _sseClientMap;
	private Gson _gson;

	public ATSseService(Configuration config_) throws Exception {
		_gson = new GsonBuilder().create();
		_sseClientMap = new HashMap<>();

		ATExecutors.scheduleAtFixedRate("SSE ping", () -> {
			int userSize = _sseClientMap.size();
			long sessionSize = _sseClientMap.values().stream().mapToInt(Set::size).sum();
			log.debug(String.format("SSE client count [%d/%d]", userSize, sessionSize));
			for (Set<SseClientWrapper> sseClientWrappers : _sseClientMap.values()) {
				for (SseClientWrapper sseClientWrapper : sseClientWrappers) {
					try {
						sseClientWrapper.client.sendEvent("ping", System.currentTimeMillis() + "");
					} catch (Exception ex) {
						log.error(String.format("SSE heartbeat error"));
					}
				}
			}
		}, 15000, 15000);

		init();
	}

	private void init() {
		ATSystemStatusManager.register(this);
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("clients", String.format("%d", this._sseClientMap.size()));
	//	status.put("clients.list", String.format("%s", this._sseClientMap.keySet()));
		for (Entry<String, Set<SseClientWrapper>> entry : this._sseClientMap.entrySet()) {
			status.put(String.format("clients.%s", entry.getKey()), String.format("%d", entry.getValue().size()));
		}
		return status;
	}
	
	@Override
	public boolean isRunning() {
		return true;
	}

	public void registerClient(SseClient client_, String sessionToken_) {
		try {
			String path = client_.ctx.path();
			String token = path.substring(path.lastIndexOf('/') + 1);
			String userId = ATServiceRegistry.getUserService().getUserId(token);
			if (StringUtils.isBlank(userId)) {
				throw new ATException(String.format("SSE missing user ID [%s]", token));
			}
			client_.sendEvent("connected", "OK");
			Set<SseClientWrapper> sseClientWrappers = _sseClientMap.get(userId);
			if (sseClientWrappers == null) {
				sseClientWrappers = new HashSet<>();
				_sseClientMap.put(userId, sseClientWrappers);
			}
			SseClientWrapper sseClientWrapper = new SseClientWrapper();
			sseClientWrapper.client = client_;
			sseClientWrapper.userId = userId;
			sseClientWrapper.timestamp = new Date();
			sseClientWrapper.webSessionToken = sessionToken_;
			sseClientWrappers.add(sseClientWrapper);

			client_.onClose(() -> {
				Iterator<Entry<String, Set<SseClientWrapper>>> iOuter = _sseClientMap.entrySet().iterator();
				outerLoop: while (iOuter.hasNext()) {
					Entry<String, Set<SseClientWrapper>> entry = iOuter.next();
					Iterator<SseClientWrapper> iInner = entry.getValue().iterator();
					while (iInner.hasNext()) {
						SseClientWrapper wrapper = iInner.next();
						if (wrapper.client.equals(client_)) {
							iInner.remove();
							if (entry.getValue().isEmpty()) {
								iOuter.remove();
								IATUserProvider userSvc = ATServiceRegistry.getUserService();
								// userSvc.logoutUser(wrapper.userId);
								userSvc.removeSession(wrapper.webSessionToken);
							}
							break outerLoop;
						}
					}
				}
			});
			log.debug(String.format("SSE client connected [%s]", token));
		} catch (Exception ex) {
			log.error(String.format("SSE connect error"), ex);
		}
	}

	public void send(WSMessageWrapper wrapper_) {
		try {
			String userId = wrapper_.getTarget();
			// if (("mov".equals(wrapper_.getType()))) {
			// log.debug("mov");
			// }
			Set<SseClientWrapper> sseClientWrappers = userId != null ? _sseClientMap.get(userId) : null;
			if (sseClientWrappers == null && WSMessageWrapper.TARGET_ALL.equals(userId)) {
				sseClientWrappers = _sseClientMap.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
			}
			if (sseClientWrappers != null && !sseClientWrappers.isEmpty()) {
				List<SseClientWrapper> clientWrappers = new ArrayList<>(sseClientWrappers);
				String json = _gson.toJson(wrapper_.getData());
				for (SseClientWrapper wrapper : clientWrappers) {
					wrapper.client.sendEvent(wrapper_.getType(), json);
				}
			} else {
				if (Arrays.asList(IATUser.USER_ALL, IATUser.USER_MOVERS, IATUser.USER_TRENDS, IATUser.USER_JUPYTER).contains(userId))
					return;
				ATKafkaUserSessionProducer kafka = ATKafkaRegistry.get(ATKafkaUserSessionProducer.class);
				ATSession nullSession = new ATSession();
				nullSession.setUserId(userId);
				nullSession.setData(Collections.singletonMap("source", wrapper_.getSource()));
				nullSession.setToken("expired");
				kafka.send(EATChangeOperation.DELETE, nullSession);
			}
		} catch (Exception ex) {
			log.error(String.format("SSE send error"), ex);
		}
	}

	private static class SseClientWrapper {

		public SseClient client;
		public String userId;
		public Date timestamp;
		public String webSessionToken;
	}

}
