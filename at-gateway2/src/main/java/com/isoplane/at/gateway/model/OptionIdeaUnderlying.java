package com.isoplane.at.gateway.model;

public class OptionIdeaUnderlying {

	public String id;
	public String name;
	public Long ts;
	
	public Double px;
	public Double pxChange;
	public Double pxAsk;
	public Double pxBid;
	public Double pxLoD;
	public Double pxHiD;
	public Double pxAvg;
	public Double pxSd;
	public Integer pxStatSpan;
}
