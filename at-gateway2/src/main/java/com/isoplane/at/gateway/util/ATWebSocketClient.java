package com.isoplane.at.gateway.util;

import java.io.IOException;
import java.net.URI;
import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketFrame;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.api.extensions.Frame;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.WSMessageWrapper;

public class ATWebSocketClient {

	static final Logger log = LoggerFactory.getLogger(ATWebSocketClient.class);

	private static Configuration _config;
	private ExecutorService _executor;
	private ScheduledExecutorService _scheduledExecutor;
	private ATWebSocket _socket;
	private String _clientId;
	private IATWebSocketListener _listener;
	// private boolean __isRunning;
	private boolean _isRunning;

	public ATWebSocketClient(Configuration config_, IATWebSocketListener listener_) {
		_config = config_;
		_listener = listener_;
		_executor = Executors.newSingleThreadExecutor();
		_scheduledExecutor = Executors.newScheduledThreadPool(1);
	}

	public void init() {
		log.info(String.format("Initializing web socket"));
		_isRunning = true;
		WebSocketClient client = new WebSocketClient();
		_socket = new ATWebSocket();
		_executor.submit(new Runnable() {

			@Override
			public void run() {
				do {
					try {
						// if(true) {
						// log.info("WebSocket turned off!");
						// return;
						// }
						String serverIp = _config.getString("gateway.source.ip");
						log.info(String.format("Connecting WebSocket client [->%s]", serverIp));
						String server = String.format("ws://%s:%d/socket/v1", serverIp, _config.getInt("gateway.source.port"));
						URI uri = new URI(server);
						client.start();
						Future<Session> fut = client.connect(_socket, uri);
						fut.get();
						_clientId = String.format("proxy[%s]", _socket.getSessionId());
						log.info(String.format("Web socket client initialized [%s] -> %s", _clientId, server));
						WSMessageWrapper registration = new WSMessageWrapper(_clientId, "server", "reg", "register", null);
						_socket.send(registration);
						_scheduledExecutor.scheduleAtFixedRate(new Runnable() {

							@Override
							public void run() {
								log.debug(String.format("Sending ping"));
								_socket.ping("Gateway");
							}
						}, 0, _config.getLong("gateway.ws.pingfreq"), TimeUnit.MILLISECONDS);
						// __isRunning = true;
						_socket.awaitClose(24, TimeUnit.HOURS);
						// __isRunning = false;

					} catch (Exception ex) {
						log.error("Failed initializing WebSocket client", ex);
					}
					if (_isRunning) {
						_socket.reset();
						log.info(String.format("Resetting WebSocket client..."));
						try {
							Thread.sleep(5000);
						} catch (InterruptedException exx) {
							log.error(String.format("Interrupted"), exx);
						}
					}
				} while (_isRunning);
			}
		});
	}

	public void process() throws InterruptedException {
		_socket.awaitClose(1000 * 60 * 60 * 24, TimeUnit.MILLISECONDS);
	}

	public void shutdown() {
		_isRunning = false;
		_socket.shutdown();
	}

	@WebSocket(maxTextMessageSize = 64 * 1024)
	public class ATWebSocket {

		private Gson _gson = new Gson();

		private CountDownLatch _closeLatch;
		private Session _session;

		public ATWebSocket() {
			reset();
		}

		public boolean send(WSMessageWrapper msg_) {
			try {
				String json = _gson.toJson(msg_);
				_session.getRemote().sendString(json);
				return true;
			} catch (IOException ex) {
				return false;
			}
		}

		public void ping(String key_) {
			try {
				_session.getRemote().sendPing(ByteBuffer.wrap(key_.getBytes()));
			} catch (IOException ex) {
				log.error(String.format("Ping [%s]", key_), ex);
			}
		}

		public String getSessionId() {
			return _session != null ? _session.getLocalAddress().toString() : null;
		}

		public boolean awaitClose(int duration, TimeUnit unit) throws InterruptedException {
			return this._closeLatch.await(duration, unit);
		}

		@OnWebSocketClose
		public void onClose(int statusCode, String reason) {
			log.info(String.format("Connection closed: %d - %s", statusCode, reason));
			this._session = null;
			this._closeLatch.countDown(); // trigger latch
		}

		@OnWebSocketConnect
		public void onConnect(Session session_) {

			log.info(String.format("Got connect: %s", session_));
			_session = session_;
			_session.setIdleTimeout(_config.getLong("gateway.ws.pingfreq") + 5000);

			log.info(String.format("Connected: %s", _session));
			try {
				// Future<Void> fut;
				// fut = session.getRemote().sendStringByFuture("Hello");
				// fut.get(2, TimeUnit.SECONDS); // wait for send to complete.
				//
				// fut = session.getRemote().sendStringByFuture("Thanks for the conversation.");
				// fut.get(2, TimeUnit.SECONDS); // wait for send to complete.
				//
				// session.close(StatusCode.NORMAL, "I'm done");
			} catch (Throwable t) {
				t.printStackTrace();
			}
		}

		@OnWebSocketFrame
		public void onFrame(Session session_, Frame frame_) {
			if (log.isDebugEnabled())
				log.debug(String.format("Received frame: %s", frame_));
		}

		@OnWebSocketMessage
		public void onMessage(String msg_) {
			try {
				if (log.isDebugEnabled())
					log.debug(String.format("Got msg: %s", msg_));
				if (ATWebSocketClient.this._listener != null) {
					WSMessageWrapper msg = _gson.fromJson(msg_, WSMessageWrapper.class);
					ATWebSocketClient.this._listener.notify(msg, true);
				}
			} catch (Exception ex) {
				log.error(String.format("Error processing message: %s", msg_), ex);
			}
		}

		@OnWebSocketError
		public void onError(Throwable cause_) {
			log.error(String.format("WebSocket error. Resetting..."), cause_);
			this.reset();
		}

		public void reset() {
			this._closeLatch = new CountDownLatch(1);
		}

		public void shutdown() {
			_session.close(0, "shutdown");
		}

	}
}
