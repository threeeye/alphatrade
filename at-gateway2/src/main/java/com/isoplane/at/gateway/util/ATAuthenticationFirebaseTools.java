package com.isoplane.at.gateway.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.ExportedUserRecord;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.ListUsersPage;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.service.IATSessionTokenProcessor;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATAuthenticationFirebaseTools implements IATSessionTokenProcessor {

	static final Logger log = LoggerFactory.getLogger(ATAuthenticationFirebaseTools.class);

	/// static final String _firebaseDbURL = "https://tradonado.firebaseio.com/";
	// static final int _firebaseIdTokenTimeout = 300000;

	// private Configuration _config;
	private FirebaseAuth _firebaseAuth;

	// private Map<String, Set<String>> _tokenMap = new HashMap<>();
	// private int counter = 0;
	public ATAuthenticationFirebaseTools() {
		// _config = config_;
		init();
	}

	private ATAuthenticationFirebaseTools init() {
		initFirebase();

		// new Timer().scheduleAtFixedRate(new TimerTask() {
		//
		// @Override
		// public void run() {
		// for (Set<String> set : _tokenMap.values()) {
		// for (String token : set) {
		// try {
		// counter++;
		// // sendMessage(token);
		// } catch (Exception ex) {
		// log.error("Error pushing", ex);
		// }
		// }
		// }
		// }
		// }, 10000, 60000);
		return this;
	}

	@Override
	public Map<String, String> listUsers() {
		Map<String, String> users = new HashMap<>();
		try {
			ListUsersPage page = _firebaseAuth.listUsers(null);
			for (ExportedUserRecord user : page.iterateAll()) {
				String userId = user.getUid();
				String email = user.getEmail();
				users.put(userId, email);
			}
		} catch (Exception ex) {
			log.error(String.format("Error listing users"), ex);
		}
		return users;
	}

	private void initFirebase() {
		try {
			Configuration config = ATConfigUtil.config();
			String path = config.getString("firebase.trd.cred");
			// String dbUrl = _config.getString("users.firebase.dbUrl");
			InputStream serviceAccount = new FileInputStream(path);
			GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(credentials)
					// .setDatabaseUrl(dbUrl)
					.build();
			FirebaseApp firebase = FirebaseApp.initializeApp(options, this.getClass().getSimpleName());
			_firebaseAuth = FirebaseAuth.getInstance(firebase);
		} catch (Exception ex) {
			log.error("Error initializing Firebase", ex);
		}
	}

	@Override
	public ATSession verifySessionToken(IATSession session_) {
		try {
			Configuration config = ATConfigUtil.config();
			long tokenTimeout = config.getLong("users.firebase.tokenTimeout");
			String refreshToken = session_.getToken();
			FirebaseToken decodedToken = _firebaseAuth.verifyIdToken(refreshToken, true /* revoked? */);
			long authTimeMillis = TimeUnit.SECONDS.toMillis((long) decodedToken.getClaims().get("auth_time"));
			if (System.currentTimeMillis() - authTimeMillis < tokenTimeout) {
				String sessionTokenStr = _firebaseAuth.createCustomToken(decodedToken.getUid());
				ATSession createdSession = new ATSession();
				// createdSession.setAtId(sessionTokenStr);
				createdSession.setToken(sessionTokenStr);
				createdSession.setRefreshToken(refreshToken);
				createdSession.setUpdateTime(System.currentTimeMillis());

				createdSession.setUserId(decodedToken.getUid() != null ? decodedToken.getUid() : session_.getUserId());
				createdSession.setName(decodedToken.getName() != null ? decodedToken.getName() : session_.getName());
				createdSession.setEmail(decodedToken.getEmail() != null ? decodedToken.getEmail() : session_.getEmail());
				createdSession.setClientIp(session_.getClientIp());
				return createdSession;
			} else {
				String msg = "Firebase auth token timeout";
				// log.error(msg);
				throw new ATException(msg);
			}
		} catch (Exception ex) {
			throw new ATException("Firebase authentication error", ex);
		}
	}

	@Override
	public void revokeSessionToken(String token_) {
		try {
			_firebaseAuth.revokeRefreshTokens(token_);
		} catch (Exception ex) {
			// User logged out client side. This is just to make sure...
			log.debug(String.format("Can be ignored: %s", ex.getMessage()));
		}
	}

	// public void saveSystemConfig(TdnSysConfig config_) {
	// Firestore db = FirestoreClient.getFirestore();
	// // Gson gson = new Gson();
	// // CollectionReference colRef = db.collection("configuration");
	// DocumentReference doc = db.document(String.format("configuration/%s", config_.mode));
	// Map<String, Object> data = new HashMap<>();
	// data.put("extraSymbols", config_.extraSymbols);
	// doc.set(data, SetOptions.merge());
	// }

}
