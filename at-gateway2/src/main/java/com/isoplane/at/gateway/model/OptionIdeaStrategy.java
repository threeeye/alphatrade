package com.isoplane.at.gateway.model;

public class OptionIdeaStrategy {

	public String type;
	public Double yldAbs;
	public Double yldPctA;
	public Double score;
	public Double prb;
	public Double mrg;
	public Double strkWidth;
	public Integer credits;
	
	public OptionIdeaOption[] options;

}
