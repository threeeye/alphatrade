package com.isoplane.at.gateway.model;

public class TdnPortfolioRecord {

	public String aid;
	public String label;
	public Double count;
	public String acct;
	public String dateTrd;
	
	
	public Double pxTrade;
	public Double pxClose;
	public Double pxLast;
	public Double pxUL;
	public Double pxTotal;
}
