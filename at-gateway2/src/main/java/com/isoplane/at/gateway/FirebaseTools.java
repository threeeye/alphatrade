package com.isoplane.at.gateway;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.SetOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.cloud.FirestoreClient;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.gateway.model.TdnSysConfig;

public class FirebaseTools {

	static final Logger log = LoggerFactory.getLogger(FirebaseTools.class);

	static  String _firebaseCredentialPath = "C:/Dev/data/tradonado-45651ea44151.json";
	static final String _firebaseDbURL = "https://tradonado.firebaseio.com/";
	static final int _firebaseIdTokenTimeout = 300000;

	// private Map<String, Set<String>> _tokenMap = new HashMap<>();
	// private int counter = 0;
	// private FirebaseApp _fb;
	private FirebaseAuth _firebaseAuth;

	public FirebaseTools init() {
		
		Configuration config = ATConfigUtil.config();
		_firebaseCredentialPath = config.getString("firebase.cred.2");
		initFirebase();
		// initDB();

		// new Timer().scheduleAtFixedRate(new TimerTask() {
		//
		// @Override
		// public void run() {
		// for (Set<String> set : _tokenMap.values()) {
		// for (String token : set) {
		// try {
		// counter++;
		// // sendMessage(token);
		// } catch (Exception ex) {
		// log.error("Error pushing", ex);
		// }
		// }
		// }
		// }
		// }, 10000, 60000);
		return this;
	}

	private void initFirebase() {
		try {
			InputStream serviceAccount = new FileInputStream(_firebaseCredentialPath);
			GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);
			// FirestoreOptions storeOptions = FirestoreOptions.newBuilder().setTimestampsInSnapshotsEnabled(true).build();
			FirebaseOptions baseOptions = new FirebaseOptions.Builder()
					.setCredentials(credentials)
					// .setDatabaseUrl(_firebaseDbURL)
					// .setFirestoreOptions(storeOptions)
					.build();
			FirebaseApp fb = FirebaseApp.initializeApp(baseOptions, this.getClass().getSimpleName());
			_firebaseAuth = FirebaseAuth.getInstance(fb);
		} catch (Exception ex) {
			log.error("Error initializing Firebase", ex);
		}
	}

	// private void initDB() {
	// // DocumentReference configRef = db.collection("system").document("source_server");
	// // configRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
	// //
	// // @Override
	// // public void onEvent(DocumentSnapshot value, FirestoreException error) {
	// // // TODO Auto-generated method stub
	// //
	// // }
	// // });
	//
	// if (true)
	// return;
	// Gson gson = new Gson();
	// Firestore db = FirestoreClient.getFirestore();
	// CollectionReference colRef = db.collection("user");
	// colRef.addSnapshotListener(new EventListener<QuerySnapshot>() {
	//
	// @Override
	// public void onEvent(QuerySnapshot snapshot, FirestoreException error) {
	// if (error != null) {
	// log.error("Listen failed: " + error);
	// return;
	// }
	// // snapshot.getDocumentChanges().forEach(d -> {
	// // switch (d.getType()) {
	// // case ADDED:
	// // log.info("New : " + d.getDocument().getData());
	// // break;
	// // case MODIFIED:
	// // log.info("Mod : " + d.getDocument().getData());
	// // break;
	// // case REMOVED:
	// // log.info("Del : " + d.getDocument().getData());
	// // break;
	// // }
	// // });
	// // _tokenSet.clear();
	// _tokenMap.clear();
	// for (DocumentSnapshot doc : snapshot) {
	// log.info("snapshot : " + doc.getId() + " - " + gson.toJson(doc.getData()));
	// // Object data = doc.get("push_token");
	// // ArrayList<?> list = data != null && data instanceof ArrayList<?> ? (ArrayList<?>) data : null;
	// // if (list == null || list.isEmpty()) {
	// // continue;
	// // }
	// // if (!(list.get(0) instanceof String)) {
	// // continue;
	// // }
	// // try {
	// // String uid = doc.getId();
	// // UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
	// // @SuppressWarnings("unchecked")
	// // ArrayList<String> tokens = (ArrayList<String>) list;
	// // Set<String> tokenSet = new HashSet<>();
	// // for (String token : tokens) {
	// // log.info(String.format("Token: %s - %s", userRecord.getEmail(), token));
	// // String tokenStr = token.substring(4);
	// // tokenSet.add(tokenStr);
	// // }
	// // _tokenMap.put(userRecord.getEmail(), tokenSet);
	// // } catch (FirebaseAuthException authex_) {
	// // log.error(String.format("Snapshot error uid [%s]", doc.getId()), authex_);
	// // }
	// }
	// }
	// });
	// }

	public ATSession verifySessionToken(ATSession session_) {
		try {
			FirebaseToken decodedToken = _firebaseAuth.verifyIdToken(session_.getToken(), true /* revoked? */);
			long authTimeMillis = TimeUnit.SECONDS.toMillis((long) decodedToken.getClaims().get("auth_time"));
			if (System.currentTimeMillis() - authTimeMillis < _firebaseIdTokenTimeout) {
				String sessionTokenStr = _firebaseAuth.createCustomToken(decodedToken.getUid());
				// TdnToken sessionToken = new TdnToken();
				session_.setUserId(decodedToken.getUid());
				session_.setName(decodedToken.getName());
				session_.setEmail(decodedToken.getEmail());
				session_.setToken(sessionTokenStr);
				return session_;
			} else {
				log.error("Firebase id token timeout");
				return null;
			}
		} catch (Exception ex) {
			log.error("Firebase authentication error", ex);
			return null;
		}
	}

	public void revokeSessionToken(String token_) {
		try {
			_firebaseAuth.revokeRefreshTokens(token_);
		} catch (Exception ex) {
			// User logged out client side. This is just to make sure...
			log.debug(String.format("Can be ignored: %s", ex.getMessage()));
		}
	}

	// public boolean subscribeNotifications(String uid_, TdnNotificationSubscription sub_) {
	// if (StringUtils.isBlank(uid_) || !sub_.verify()) {
	// log.error(String.format("Missing notification subscription data [%s, %s]", uid_, sub_));
	// return false;
	// }
	// try {
	// FirebaseMessaging fbm = FirebaseMessaging.getInstance(_fb);
	// List<String> tokens = Arrays.asList(sub_.token);
	// String generalTopic = "general";
	// int count = 0;
	// switch (sub_.mode) {
	// case "none":
	// count += fbm.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, "min")).getFailureCount();
	// count += fbm.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, "all")).getFailureCount();
	// count += fbm.unsubscribeFromTopic(tokens, generalTopic).getFailureCount();
	// break;
	// case "min":
	// count += fbm.subscribeToTopic(tokens, String.format("%s_%s", uid_, "min")).getFailureCount();
	// count += fbm.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, "all")).getFailureCount();
	// count += fbm.unsubscribeFromTopic(tokens, generalTopic).getFailureCount();
	// break;
	// case "all":
	// count += fbm.subscribeToTopic(tokens, String.format("%s_%s", uid_, "min")).getFailureCount();
	// count += fbm.subscribeToTopic(tokens, String.format("%s_%s", uid_, "all")).getFailureCount();
	// count += fbm.subscribeToTopic(tokens, generalTopic).getFailureCount();
	// break;
	// }
	// log.debug(String.format("Notification subscription [%s: %s_%s]: %s", count == 0 ? "Success" : "Failure", uid_, sub_.mode, tokens));
	// return count == 0;
	// } catch (Exception ex) {
	// log.error(String.format("Error subscribint [%s, %s]", uid_, sub_), ex);
	// return false;
	// }
	// }

	public void saveSystemConfig(TdnSysConfig config_) {
		Firestore db = FirestoreClient.getFirestore();
		// Gson gson = new Gson();
		// CollectionReference colRef = db.collection("configuration");
		DocumentReference doc = db.document(String.format("configuration/%s", config_.mode));
		Map<String, Object> data = new HashMap<>();
		data.put("extraSymbols", config_.extraSymbols);
		doc.set(data, SetOptions.merge());
	}

	// https://firebase.google.com/docs/cloud-messaging/admin/send-messages#webpush-specific_fields
	// https://tools.ietf.org/html/rfc8030#section-9.1
	// https://firebase.google.com/docs/cloud-messaging/http-server-ref
	// https://firebase.google.com/docs/reference/admin/java/reference/com/google/firebase/messaging/WebpushNotification.Builder
	// https://developers.google.com/web/fundamentals/push-notifications/display-a-notification
	// public void sendNotification(Notification notification_, String mode_, Collection<String> messageIds_) {
	// // for (String pushIdx : messageIds_) {
	// String topic = null;
	// // if ("mironroth@gmail.com".equals(pushId) || "gu3QsLjaszUXS0ixUGthOMNh3Cf1".equals(pushId)) {
	// // // userId = "enalposi@yahoo.com";
	// // topic = String.format("%s_%s", "gu3QsLjaszUXS0ixUGthOMNh3Cf1", mode_);
	// // }
	// WebpushConfig wpConfig = buildPushConfig(notification_);
	// try {
	// if (StringUtils.isNotBlank(topic)) {
	// Message message = Message.builder()
	// .setWebpushConfig(wpConfig)
	// .putData("pid", "" + counter)
	// .setTopic(topic)
	// .build();
	// String response = FirebaseMessaging.getInstance().send(message);
	// log.info(String.format("Successful proxy push [%s]: %s", "topic", response));
	// } else {
	// Set<String> tokens = new HashSet<>(messageIds_);// _tokenMap.get(pushId);
	// if (tokens == null || tokens.isEmpty()) {
	// log.error(String.format("Invalid notification [%s]", messageIds_));
	// return;
	// }
	// for (String token : tokens) {
	// Message message = Message.builder()
	// .setWebpushConfig(wpConfig)
	// .putData("pid", "" + counter)
	// .setToken(token)
	// .build();
	// String response = FirebaseMessaging.getInstance(_fb).send(message);
	// log.info(String.format("Successful proxy push [%s]: %s", "token", response));
	// }
	// }
	//
	// } catch (FirebaseMessagingException ex_) {
	// log.error(String.format("Error proxying push [%s]", notification_), ex_);
	// }
	// // }
	// }

	// private WebpushConfig buildPushConfig(Notification notification_) {
	// WebpushConfig wpConfig = WebpushConfig.builder()
	// .setNotification(
	// WebpushNotification.builder()
	// .setTitle(notification_.title)
	// .setBody(notification_.body)
	// .setIcon(String.format("https://tradonado.com/assets/icons/%s", notification_.icon))
	// .putCustomData("click_action",
	// notification_.click_action == null ? "https://tradonado.com" : notification_.click_action)
	// .setSilent(false)
	// .setRenotify(true)
	// .setTimestampMillis(System.currentTimeMillis())
	// .setTag(notification_.tag)
	// .setBadge("https://tradonado.com/assets/icons/icon-16x16.png")
	// .build())
	// .build();
	// return wpConfig;
	// }

	// private void sendMessage(String token_) {
	// // https://firebase.google.com/docs/cloud-messaging/admin/send-messages#webpush-specific_fields
	// // https://tools.ietf.org/html/rfc8030#section-9.1
	// // https://firebase.google.com/docs/cloud-messaging/http-server-ref
	// // https://firebase.google.com/docs/reference/admin/java/reference/com/google/firebase/messaging/WebpushNotification.Builder
	// // https://developers.google.com/web/fundamentals/push-notifications/display-a-notification
	// LocalDateTime now = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
	// Message message = Message.builder()
	// .setWebpushConfig(WebpushConfig.builder()
	// .setNotification(
	// WebpushNotification.builder()
	// .setTitle("test #7")
	// .setBody(String.format("Count: %d\nSent: %s %s", counter,
	// DateTimeFormatter.ISO_LOCAL_DATE.format(now),
	// DateTimeFormatter.ISO_LOCAL_TIME.format(now)))
	// .setIcon("https://tradonado.com/assets/icons/globe_star.ico")
	// .putCustomData("click_action", "https://tradonado.com")
	// .setSilent(false)
	// .setRenotify(false)
	// // .addAction(new Action("https://tradonado.com", "https://tradonado.com"))
	// .setTimestampMillis(System.currentTimeMillis())
	// .setTag("notificationTest")
	// .setBadge("https://tradonado.com/assets/icons/icon-16x16.png")
	// .build())
	// .build())
	// // .putData("score", "850")
	// // .putData("time", "2:45")
	// // .putData("click_action", "https://tradonado.com")
	// .setToken(token_)
	// // .setTopic("test")
	// .build();
	//
	// try {
	// String response = FirebaseMessaging.getInstance().send(message);
	// log.info(String.format("Successfully sent push: %s", response));
	// } catch (FirebaseMessagingException ex_) {
	// log.error(String.format(String.format("Error sending push: %s", token_), ex_));
	// }
	// }

}
