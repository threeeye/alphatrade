package com.isoplane.at.gateway.model;

public class TdnEquityResearchResponseWrapper {

	public TdnEquityResearchResponse data;

	public class TdnEquityResearchResponse {

		public String avgMode;
		public String hvMode;
		public TdnEquityResearch[] equities;

		public class TdnEquityResearch {

			public String id;
			public String name;
			public Long ts;

			public Double px;
			public Double pxChange;
			public Double pxAsk;
			public Double pxBid;
			public Double pxLoD;
			public Double pxHiD;
			public Double pxAvg;
			public String pxAvgMode;
			public Double pxHV;
			public String pxHVMode;
			public String note;
			public Boolean owned;
			public String dateEarn;
			public String dateDiv;
			public Double divAmt;
		}
	}
}
