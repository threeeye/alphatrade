package com.isoplane.at.gateway.util;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.WSMessageWrapper;

import io.javalin.websocket.WsConfig;

@WebSocket
public class ATWebSocketServiceJavalin {

	static final Logger log = LoggerFactory.getLogger(ATWebSocketServiceJavalin.class);

	private static final Set<IATWebSocketListener> _listeners = new HashSet<>();
	private static final Map<String, WSSessionToken> _clientMap = new ConcurrentHashMap<>();
	private static final Map<Session, String> _sessionMap = new ConcurrentHashMap<>();

	// Store sessions if you want to, for example, broadcast a message to all users
	// private static final Queue<Session> sessions = new ConcurrentLinkedQueue<>();

	// TODO: This temporary design sort of sucks
	// private static HttpTools _tools;
	// public static void setHttpTools(HttpTools tools_) {
	// _tools = tools_;
	// }
	static private Configuration _config;
	static private ATWebSocketServiceJavalin _instance;

	public static ATWebSocketServiceJavalin instance() {
		if (_instance == null) {
			try {
				// _config = config_;
				_instance = new ATWebSocketServiceJavalin();
			} catch (UnknownHostException ex) {
				log.error(String.format("WebSocket init failed"), ex);
			}
		}
		return _instance;
	}

	private String _serverName;
	private int counter = 0;
	private Gson _gson = new Gson();
	private ScheduledExecutorService _executor;

	private ATWebSocketServiceJavalin() throws UnknownHostException {
		if (_config == null) {
			throw new NullPointerException("Missing configuration");
		}
		_instance = this;
		String ipStr = InetAddress.getLocalHost().getHostAddress();
		_serverName = String.format("Server[%s]", ipStr);
		_executor = Executors.newSingleThreadScheduledExecutor();
		// init();
	}

	public void init(WsConfig ws_) {

		_executor.scheduleAtFixedRate(new Runnable() {

			@Override
			public void run() {
				try {
					for (String sessionId : _clientMap.keySet()) {
						WSMessageWrapper wrapper = new WSMessageWrapper();
						wrapper.setSource(_serverName);
						wrapper.setTarget(sessionId);
						wrapper.setFunction("ping");
						wrapper.setType("ping");
						Map<String, Object> messageMap = new HashMap<>();
						messageMap.put("Message", counter++);
						wrapper.setData(messageMap);
						send(wrapper);
					}
				} catch (Exception ex) {
					log.error(String.format("Error sending ping"), ex);
				}
			}
		}, 10000, 10000, TimeUnit.MILLISECONDS);
		log.info("Initialized");

		ws_.onConnect(ctx_ -> {
			Session session = ctx_.session;
			long keepAliveFreq = _config.getLong("gateway.ws.pingfreq");
			session.setIdleTimeout(keepAliveFreq + 5000);
			log.info(String.format("connected: %s", session.getRemoteAddress()));
		});

		ws_.onClose(ctx_ -> {
			Session session = ctx_.session;
			String clientId = _sessionMap.remove(session);
			if (clientId != null) {
				_clientMap.remove(clientId);
			}
			if (_sessionMap.isEmpty())
				counter = 0;
			log.info(String.format("Closed [%s/%s]", clientId, session.getRemoteAddress()));
			if (log.isDebugEnabled())
				log.debug(String.format(" - closed: %s, %d, %s", session.getRemoteAddress(), ctx_.status(), ctx_.reason()));
		});

		ws_.onMessage(ctx_ -> {
			Session session = ctx_.session;
			String message = ctx_.message();
			if (log.isDebugEnabled())
				log.debug(String.format("message: %s, %s", session.getRemoteAddress(), message));
			WSMessageWrapper wrapper = _gson.fromJson(message, WSMessageWrapper.class);
			wrapper.raw = message;
			String clientId = wrapper.getSource();
			int idx = clientId.lastIndexOf("@");
			if (idx > 0) {
				clientId = clientId.substring(0, idx);
			}
			String function = wrapper.getFunction();
			if (!"server".equals(wrapper.getTarget()) || function == null || clientId == null) {
				log.warn(String.format("Invalid message [%s]", wrapper));
				return;
			}
			switch (function) {
			case "register":
				if (!_sessionMap.containsKey(session)) {
					_sessionMap.put(session, clientId);
				}
				WSSessionToken token = _clientMap.get(clientId);
				if (token == null) {
					token = new WSSessionToken();
					token.session = session;
					_clientMap.put(clientId, token);
				}
				token.timeStamp = System.currentTimeMillis();
				log.debug(String.format("%s [%s]", function, clientId));
				break;
			default:
				notify(wrapper);
				break;
			}
		});

		ws_.onError(ctx_ -> {
			log.error(String.format("WebSocket error"), ctx_.error());
		});

	}

	private void notify(WSMessageWrapper data_) {
		for (IATWebSocketListener listener : _listeners) {
			try {
				listener.notify(data_, false);
			} catch (Exception ex) {
				log.error(String.format("Error notifying listener [%s]", listener), ex);
			}
		}
	}

	static public void configure(Configuration config_) {
		_config = config_;
	}

	static public void register(IATWebSocketListener listener_) {
		_listeners.add(listener_);
	}

	static public void unregister(IATWebSocketListener listener_) {
		_listeners.remove(listener_);
	}

	synchronized public void send(WSMessageWrapper data_) {
		try {
			String target = data_.getTarget();
			if (!StringUtils.isBlank(target)) {
				String[] clientIds = target.split(",");
				for (String clientId : clientIds) {
					WSSessionToken token = _clientMap.get(clientId);
					if (token != null) {
						String json = _gson.toJson(data_);
						log.debug(String.format("Sending: %s -> %s(%s:%s)", data_.getSource(), target, data_.getType(), data_.getFunction()));
						token.session.getRemote().sendString(json);
					}
				}
			} else {
				log.warn(String.format("WebSocket broadcast: %s", data_));
				Set<Session> sessions = _sessionMap.keySet();
				if (sessions == null || sessions.isEmpty()) {
					log.warn(String.format("Missing sessions [%s]", target));
					return;
				}
				String json = _gson.toJson(data_);
				log.debug(String.format("Sending: %s -> %s(%s)", data_.getSource(), target, data_.getFunction()));
				for (Session session : sessions) {
					session.getRemote().sendString(json);
				}
			}
		} catch (IOException ex) {
			log.error(String.format("send: %s", data_), ex);
		}
	}

	public static class WSSessionToken {
		public Session session;
		public long timeStamp;

	}
}
