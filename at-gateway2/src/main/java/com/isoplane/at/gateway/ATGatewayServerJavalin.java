package com.isoplane.at.gateway;

import static io.javalin.apibuilder.ApiBuilder.get;
import static io.javalin.apibuilder.ApiBuilder.path;
import static io.javalin.apibuilder.ApiBuilder.post;
import static io.javalin.apibuilder.ApiBuilder.delete;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import com.google.api.client.http.HttpMethods;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.ProtocolStringList;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATGenericNotification;
import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.ATResponsePack.ATSecurityPack;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.IATSession;
import com.isoplane.at.commons.model.WSMessageWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoPushNotificationWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoSecurityPackWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataStatus;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.model.protobuf.SecurityPackProtos.SecurityPack;
import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoPushNotificationSubscriber;
import com.isoplane.at.commons.service.IATProtoSseSubscriber;
import com.isoplane.at.commons.service.IATProtoSystemSubscriber;
import com.isoplane.at.commons.service.IATSessionListener;
import com.isoplane.at.commons.service.IATSessionTokenProcessor;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFileAppender;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.commons.util.IATJsonSerializationExclude;
import com.isoplane.at.gateway.model.TdnJsonWrapper;
import com.isoplane.at.gateway.model.TdnRequestContext;
import com.isoplane.at.gateway.model.TdnSysConfig;
import com.isoplane.at.gateway.util.ATAuthenticationFirebaseTools;
import com.isoplane.at.gateway.util.ATSseService;
import com.isoplane.at.gateway.util.ATWebSocketClient;
import com.isoplane.at.gateway.util.ATWebSocketServiceJavalin;
import com.isoplane.at.gateway.util.HttpTools;
import com.isoplane.at.gateway.util.IATWebSocketListener;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationConsumer;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageConsumer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusConsumer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.users.ATUserManager;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.Javalin;
import io.javalin.http.Context;

public class ATGatewayServerJavalin
		implements IATWebSocketListener, IATSessionListener, IATProtoSseSubscriber, IATProtoSystemSubscriber,
		IATProtoPushNotificationSubscriber {

	static {
		String logPath = System.getProperty("logPath");
		ATFileAppender.overrideFile(logPath);
	}

	static final Logger log = LoggerFactory.getLogger(ATGatewayServerJavalin.class);
	Gson _gson;
	static private String ID;

	static ATGatewayServerJavalin _instance;
	static FirebaseTools _firebaseTools;
	static HttpTools _httpTools;

	static final String apiV2Path = "/api/v2";

	private Javalin _server;
	private ATSseService _sseSvc;
	private ATWebSocketClient _wsClient;
	private ATKafkaUserSessionProducer _kafkaUserSessionProducer;
	private Map<String, Map<String, String>> _systemStatusMap;
	private boolean _isRunning;
	private LoadingCache<String, Long> _ipCache;
	private LoadingCache<String, Long> _extCache;

	int _webPoolSizeMin = 2;
	int _webPoolSizeMax = 16;
	int _webPoolIdleTimeout = 20000;
	int _webFormSizeMax = 500000;
	static int _port = 8888;
	private ATKafkaSystemStatusProducer _statusProducer;

	public static void main(String[] args_) {
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			_port = config.getInt("gateway.web.port", 8888);

			ATExecutors.init();
			ATMongoTableRegistry.init();
			ATWebSocketServiceJavalin.configure(config);

			ATKafkaRegistry.register("gateway",
					ATKafkaUserSessionProducer.class,
					ATKafkaSseMessageConsumer.class,
					ATKafkaPushNotificationConsumer.class,
					ATKafkaSystemStatusConsumer.class,
					ATKafkaSystemStatusProducer.class,
					ATKafkaUserSessionConsumer.class);

			IATSessionTokenProcessor authSvc = new ATAuthenticationFirebaseTools();
			IATUserProvider userSvc = new ATUserManager(authSvc, true);
			ATServiceRegistry.setUserService(userSvc, true);

			_instance = new ATGatewayServerJavalin();
			_instance.init();
			userSvc.register(_instance);
			// _instance.setupWebServer();
			_instance.serve();
		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATGatewayServerJavalin.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private void init() {
		try {
			_systemStatusMap = new TreeMap<>();
			Configuration config = ATConfigUtil.config();
			// _config = config_;
			// _sessionTimeout = _config.getLong("gateway.timeout");
			_gson = new GsonBuilder()
					.addSerializationExclusionStrategy(new ExclusionStrategy() {
						@Override
						public boolean shouldSkipField(FieldAttributes f) {
							return f.getAnnotation(IATJsonSerializationExclude.class) != null;
						}

						@Override
						public boolean shouldSkipClass(Class<?> clazz) {
							return false;
						}
					})
					.create();
			_firebaseTools = new FirebaseTools().init();
			_httpTools = new HttpTools().init(config);

			_wsClient = new ATWebSocketClient(config, this);

			// ATStreamingController wsListener = new ATStreamingController(config_, _httpTools);
			_sseSvc = new ATSseService(config);

			_isRunning = true;

			ATKafkaSseMessageConsumer sseConsumer = ATKafkaRegistry.get(ATKafkaSseMessageConsumer.class);
			sseConsumer.subscribe(this);
			ATKafkaSystemStatusConsumer systemStatusConsumer = ATKafkaRegistry.get(ATKafkaSystemStatusConsumer.class);
			systemStatusConsumer.subscribe(this);
			// _pushWorker = new PushWorker();
			ATKafkaPushNotificationConsumer pushNotificationConsumer = ATKafkaRegistry
					.get(ATKafkaPushNotificationConsumer.class);
			pushNotificationConsumer.subscribe(this);

			ATKafkaRegistry.start();
			_kafkaUserSessionProducer = ATKafkaRegistry.get(ATKafkaUserSessionProducer.class);
			_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

			// ATWebSocketServiceJavalin.configure(config_);
			// ATWebSocketServiceJavalin.register(_sseSvc);

			_ipCache = CacheBuilder.newBuilder()
					.maximumSize(10000)
					.expireAfterWrite(30, TimeUnit.MINUTES)
					.build(
							new CacheLoader<String, Long>() {
								public Long load(String key) {
									return 0L;
								}
							});
			_extCache = CacheBuilder.newBuilder()
					.maximumSize(10000)
					.expireAfterWrite(config.getLong("gateway.ext.throttle"), TimeUnit.MILLISECONDS)
					.build(
							new CacheLoader<String, Long>() {
								public Long load(String key) {
									return 0L;
								}
							});

			final String ipStr = ATSysUtils.getIpAddress("n/a");
			ID = String.format("%s:%s:%s", ATGatewayServerJavalin.class.getSimpleName(), ipStr, _port);

			long cleanInterval = config.getLong("gateway.cleanInterval");
			String threadId = String.format("%s.cleaner", getClass().getSimpleName());
			ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {

				@Override
				public void run() {
					runCleaner();
				}
			}, cleanInterval, cleanInterval);
			_wsClient.init();

			String pre = this.getClass().getSimpleName();
			ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

			log.info(String.format("%s[%s:%d]", this.getClass().getSimpleName(), ipStr, _port));
		} catch (Exception ex) {
			log.error("Error initializing", ex);
		}
	}

	private void runCleaner() {
		try {
			// Iterator<Entry<String, TdnToken>> i = _sessionMap.entrySet().iterator();
			// _sessionTimeout = _config.getLong("gateway.timeout");
			// while (i.hasNext()) {
			// Entry<String, TdnToken> entry = i.next();
			// if (entry.getValue().getExpire() < System.currentTimeMillis()) {
			// i.remove();
			// log.info(String.format("Timeout [%s]", entry.getValue().getName()));
			// }
			// }
		} catch (Exception ex) {
			log.error(String.format("Error cleaning sessions"), ex);
		}
	}

	private void serve() {
		Configuration config = ATConfigUtil.config();

		//	Gson gson = new GsonBuilder().create();
		//	JavalinJson.setFromJsonMapper(gson::fromJson);
		//	JavalinJson.setToJsonMapper(gson::toJson);

		_server = Javalin.create(config_ -> {
			config_.enableCorsForAllOrigins();
			boolean isWebDebugLog = config.getBoolean("web.debug.logging", false);
			if (isWebDebugLog) {
				config_.enableDevLogging();
			}
		}).start(_port);

		// _server.ws("/tradonado/socket/v1", ATWebSocketService.class);
		_server.ws("/tradonado/socket/v1", ws_ -> {
			ATWebSocketServiceJavalin.instance().init(ws_);
		});

		// Set<SseClient> sseClients = new HashSet<>();
		//
		// ATExecutors.scheduleAtFixedRate("SSE beat", () -> {
		// log.debug(String.format("SSE client count [%d]", sseClients.size()));
		// for (SseClient sseClient : sseClients) {
		// try {
		// sseClient.sendEvent("beat", "" + System.currentTimeMillis());
		// } catch (Exception ex) {
		// log.error(String.format("Error sending SSE heartbeat"));
		// }
		// }
		// }, 15000, 15000);

		_server.sse("/tradonado/sse/*", client_ -> {
			// NOTE: Authorized in 'before'
			IATSession session = TdnRequestContext.getToken();
			String sessionToken = session != null ? session.getToken() : null;
			_sseSvc.registerClient(client_, sessionToken);
			// client_.ctx.header(header)
		});

		_server.before(ctx_ -> {
			String method = ctx_.method();
			String ctxPath = ctx_.path();
			String ipStr = ctx_.ip(); // TODO: This is the proxy IP. Fix!
			try {
				ipStr = ctx_.req.getRemoteAddr();
			} catch (Exception ex) {
				log.error("Error getting remote address", ex);
			}
			if (log.isDebugEnabled() && !ID.contains(ipStr)) {
				log.debug(String.format("External request: %s", ipStr));
			}

			IATUserProvider userSvc = ATServiceRegistry.getUserService();
			if (!HttpMethods.OPTIONS.equals(method)
					&& (StringUtils.isBlank(ctxPath) || !ctxPath.startsWith("/tradonado/api/v2/auth"))) {
				String tokenId = ctx_.header("Authorization");
				String origin = ctx_.header("Origin");
				String activityTsStr = ctx_.header("ActivityTS");

				if ("internal".equals(tokenId) && ctxPath.equals("/internal/temp/notification")) {
					log.info("internal!");
				} else if ("test".equals(tokenId)) {
					log.warn(String.format("processing TEST request: %s", ctxPath));
					String body = ctx_.body();
					verifyTest(ctxPath, body);
					// String pathEnd = ctxPath.substring(ctxPath.lastIndexOf('/') + 1);
					Map<String, Object> stmpl = Collections.singletonMap(IATSession.TOKEN, tokenId);
					IATSession ssn = new ATSession(stmpl);
					TdnRequestContext.setToken(ssn);
				} else if (tokenId != null && tokenId.startsWith("external")) {
					String extKey = tokenId.replaceAll(" ", "").substring(8);
					verifyExternal(ipStr, ctxPath, extKey);
					Map<String, Object> stmpl = Collections.singletonMap(IATSession.TOKEN, tokenId);
					IATSession ssn = new ATSession(stmpl);
					TdnRequestContext.setToken(ssn);
				} else {
					try {
						Long activityTs = ctxPath.startsWith("/tradonado/sse") ? System.currentTimeMillis()
								: Long.parseLong(activityTsStr);
						if (ctxPath.startsWith("/tradonado/sse")) {
							activityTs = System.currentTimeMillis();
							tokenId = ctxPath.substring(ctxPath.lastIndexOf('/') + 1);
						}
						IATSession session = userSvc.verifySession(tokenId, ipStr, activityTs);
						if (session == null) {
							log.error(String.format("Authentication timeout [%s -> %s]", ipStr, ctxPath));
							ctx_.header("Access-Control-Allow-Origin", origin);
							ctx_.status(401);
						}
						TdnRequestContext.setToken(session);
						long timeout = userSvc.getSessionTimeout();
						ctx_.header("TokenExpiration", "" + (session.getUpdateTime() + timeout));
						String ipStr2 = ctx_.header("X-Forwarded-For");
						if (ipStr2 == null) {
							ipStr2 = ipStr;
						}
						String uid = session.getUserId();
						userSvc.logUserActivity(ipStr2, uid, ctxPath, null);
						this.validateIp(ipStr2, uid);
					} catch (Exception ex) {
						log.error(String.format("Error registering session [%s]: %s", ipStr, ex.getMessage()));
						log.error(String.format("Authentication timeout [%s -> %s]", ipStr, ctxPath));
						if (!StringUtils.isBlank(origin)) {
							ctx_.header("Access-Control-Allow-Origin", origin);
						}
						ctx_.status(401);
					}
				}
			}
		});

		_server.after(ctx_ -> {
			String uri = ctx_.url();
			TdnRequestContext.clear();
			if (!uri.contains("/socket/") && !uri.contains("/sse")) {
				String origin = ctx_.header("Origin");
				ctx_.contentType("application/json");
				if (!StringUtils.isBlank(origin)) {
					ctx_.header("Access-Control-Allow-Origin", origin);
				}
				ctx_.header("Access-Control-Allow-Credentials", "true");
				ctx_.header("Access-Control-Expose-Headers", "TokenExpiration");
				// } else if(uri.contains("/sse")) {
				// log.info("sse");
			}
		});

		// _server.wsBefore(ws_ -> {
		// ws_.onMessage(ctx_ ->{
		// String tokenId = ctx_.header("Authorization");
		// ctx_.session.set
		// ctx_.h.header("Sec-WebSocket-Protocol", tokenId);
		// });//.....header("Authorization");
		// // ctx_.header("Sec-WebSocket-Protocol", tokenId);
		// });

		// _server.before("/tradonado/*", ctx_->{
		// String url = ctx_.url();
		// });

		_server.routes(() -> {
			path("/tradonado", () -> {

				// before((request, response) -> {
				// // String method = request.requestMethod();
				// String ctxPath = request.uri();
				// if (StringUtils.isNotBlank(ctxPath) && ctxPath.startsWith("/api/v2/")) { // ctxPath.contains("login")
				// // log.debug(String.format("V2 request: %s (%s)", request.uri(), method));
				// // Object h = request.headers();
				// // log.debug(String.format("%s, %s", h, method));
				// // CORS handler
				// String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
				// if (accessControlRequestHeaders != null) {
				// response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
				// }
				// String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
				// if (accessControlRequestMethod != null) {
				// response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
				// }
				// }
				// });
				//
				// after((request, response) -> {
				// response.header("Content-Encoding", "gzip");
				// // response.type("application/json");
				// });
				//
				// options("/*", (request, response) -> {
				// try {
				// log.info(String.format("Pre-flight from [%s]", request.ip()));
				// response.header("Access-Control-Allow-Origin", "*"); // request.ip()
				// response.header("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS");
				// response.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
				// response.header("Access-Control-Max-Age", "86400");
				// response.status(200);
				// return "OK";
				// } finally {
				// // ATRequestContext.clear();
				// }
				// });

				path("/api/v2/auth", () -> {
					post("/login", ctx_ -> {
						try {
							String ipStr = ctx_.ip();
							String bodyStr = ctx_.body();
							ATSession requestSession = _gson.fromJson(bodyStr, ATSession.class);
							requestSession.setClientIp(ipStr);
							IATSession session = ATServiceRegistry.getUserService().registerSession(requestSession);
							if (StringUtils.isNotBlank(ipStr) && session != null) {
								String responseStr = _gson.toJson(session);
								// ctx_.cookie("atcookie1", "api-is-cookie");
								// ctx_.cookieStore("atcookie2", "api-is-cookiestore");
								ctx_.status(200);
								ctx_.result(responseStr);
								log.debug(String.format("Logged in [%s - %s]", session.getEmail(), ipStr));
							} else {
								log.error(String.format("Unauthorized login [%s / %s]", requestSession.getEmail(),
										ipStr));
								ctx_.status(401);
								ctx_.result(String.format("{\"error\":%s}", "Unauthorized"));
							}
						} catch (Exception ex) {
							log.error(String.format("Error logging in"), ex);
							ctx_.status(401);
							ctx_.result(String.format("{\"error\":%s}", "Unauthorized"));
						}
					});

					post("/logout", ctx_ -> {
						String sessionTokenStr = ctx_.header("Authorization");
						int status = ATServiceRegistry.getUserService().removeSession(sessionTokenStr) ? 1 : -1;
						ctx_.result(String.format("{\"Status\":\"%d\"}", status));
					});
					post("/alive/{timestamp}", ctx_ -> {
						String sessionTokenStr = ctx_.header("Authorization");
						String activityTsStr = ctx_.header("ActivityTS");
						Long activityTs = Long.parseLong(activityTsStr);
						IATUserProvider userSvc = ATServiceRegistry.getUserService();
						String userId = userSvc.getUserId(sessionTokenStr);
						if (userId != null) {
							Long timeout = userSvc.getSessionTimeout();
							boolean activityTimedOut = activityTs > 0
									&& activityTs + timeout < System.currentTimeMillis();
							ctx_.result(String.format("{\"Status\":\"%d\"}", activityTimedOut ? -1 : 0));
						} else {
							throw new ATException(String.format("Invalid session"));
						}
					});
					get("/verify", ctx_ -> {
						String ip = ctx_.ip();
						String sessionTokenStr = ctx_.header("Authorization");
						IATSession session = ATServiceRegistry.getUserService().verifySession(sessionTokenStr, ip, 0L);
						String userId = session != null ? session.getUserId() : null;
						ctx_.result(String.format("{\"Status\":\"%b\"}", userId != null));
					});

				});

				get("/api/v2/callback", ctx_ -> {
					if (log.isDebugEnabled()) {
						String query = _gson.toJson(ctx_.queryParamMap());
						Map<String, String> headers = ctx_.headerMap();
						String body = ctx_.body();
						log.debug(String.format("Callback query  : %s", query));
						log.debug(String.format("Callback headers: %s", headers));
						log.debug(String.format("Callback body   : %s", body));
					}
					Map<String, String> headerMap = new HashMap<>();
					headerMap.put("AT-host", ctx_.host());
					headerMap.put("AT-url", ctx_.fullUrl());
					if (ctx_.headerMap() != null) {
						headerMap.putAll(ctx_.headerMap());
					}
					_httpTools.getCallback(ctx_.queryParamMap(), headerMap);
					ctx_.redirect("http://tradonado.com");
				});

				path("/api/v2/command", () -> {
					post("/*", ctx_ -> {
						Map<String, List<String>> queryParams = ctx_.queryParamMap();
						String path = ctx_.path();
						String cmd = path.substring(path.lastIndexOf("command/") + "command/".length());
						String body = ctx_.body();
						String result = _httpTools.postCommand(cmd, body, null, queryParams);
						ctx_.result(result);
					});
					get("/*", ctx_ -> {
						String path = ctx_.path();
						String cmd = path.substring(path.lastIndexOf("command/") + "command/".length());
						Map<String, List<String>> params = ctx_.queryParamMap();
						String result = _httpTools.getCommandA(cmd, params, null);
						ctx_.result(result);
					});
					delete("/*", ctx_ -> {
						Map<String, List<String>> queryParams = ctx_.queryParamMap();
						String path = ctx_.path();
						String cmd = path.substring(path.lastIndexOf("command/") + "command/".length());
						String result = _httpTools.deleteCommand(cmd, null, queryParams);
						ctx_.result(result);
					});
				});
				path("/api/v2/security-pack", () -> {
					final var sec_pack = "security-pack/";
					get("/*", ctx_ -> {
						String path = ctx_.path();
						String type = path.substring(path.lastIndexOf(sec_pack) + sec_pack.length());
						Map<String, List<String>> params = ctx_.queryParamMap();
						var spPath = String.format("%s/%s", sec_pack, type);
						String result = _httpTools.getApiV2(spPath, params, null);
						ctx_.result(result);
					});
				});

				path("api2/v1/command", () -> {
					get("/*", ctx_ -> {
						String path = ctx_.path();
						String cmd = path.substring(path.lastIndexOf("command/") + "command/".length());
						Map<String, List<String>> params = ctx_.queryParamMap();
						String result = _httpTools.getCommandB(cmd, params);
						ctx_.result(result);
					});
				});

				path("/api/v2", () -> {
					get("/systemstatus", ctx_ -> {
						String sessionTokenStr = ctx_.header("Authorization");
						IATUserProvider userSvc = ATServiceRegistry.getUserService();
						String userId = userSvc.getUserId(sessionTokenStr);
						ATUser user = userSvc.getUser(userId);

						ATWebResponseWrapper response = new ATWebResponseWrapper();

						if (user == null || !user.getRoles().contains("admin")) {
							log.error(String.format("Unauthorized access [%s/%s]", "/systemstatu", userId));
							response.error = "unauthorized";
						} else {
							long tsNow = System.currentTimeMillis();
							Iterator<Entry<String, Map<String, String>>> is = _systemStatusMap.entrySet().iterator();
							while (is.hasNext()) {
								Entry<String, Map<String, String>> entry = is.next();
								//								if (entry.getKey().startsWith("ATKafkaAmeritrade")) {
								//									log.debug("HELLO");
								//								}
								Map<String, String> statusMap = entry.getValue();
								String ts = statusMap == null ? null : statusMap.get(" timestamp");
								Date d = ts == null ? null : ATFormats.DATE_TIME_mid.get().parse(ts);
								long diffInMillies = d == null ? Integer.MAX_VALUE : Math.abs(tsNow - d.getTime());
								long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);
								if (diff > 15) {
									is.remove();
								}
							}
							response.data = _systemStatusMap;
						}
						String jsonResponse = _gson.toJson(response);
						ctx_.result(jsonResponse);
					});
					get("/system_status", ctx_ -> {
						String statusJson = _gson.toJson(_systemStatusMap);
						ctx_.result(statusJson);
					});
					get("/subscribe", ctx_ -> {
						String response = null;
						String queryString = ctx_.queryString();
						String pathInfo = ctx_.contextPath();
						String command = pathInfo.substring(pathInfo.indexOf(apiV2Path) + apiV2Path.length());
						try {
							response = _httpTools.forwardGet(command, queryString);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", pathInfo, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					get("/performance", ctx_ -> {
						String response = null;
						String queryString = ctx_.queryString();
						String path = ctx_.path();
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
						try {
							response = _httpTools.forwardGet(command, queryString);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					get("/history/*", ctx_ -> {
						String response = null;
						String queryString = ctx_.queryString();
						String path = ctx_.path();
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
						try {
							response = _httpTools.forwardGet(command, queryString);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					get("/assets", ctx_ -> {
						String response = null;
						String queryString = ctx_.queryString();
						String path = ctx_.path();
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
						try {
							response = _httpTools.forwardGet(command, queryString);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					get("/trades/*", ctx_ -> {
						String path = ctx_.path();
						String response = null;
						String queryString = ctx_.queryString();
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
						try {
							response = _httpTools.forwardGet(command, queryString);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					post("/trades/*", ctx_ -> {
						String response = null;
						// String queryString = ctx_.queryString();
						String path = ctx_.path();
						// String pathInfo = ctx_.contextPath();// .pathInfo();
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
						String bodyStr = ctx_.body();
						try {
							response = _httpTools.forwardPost(command, bodyStr);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					post("/settings/*", ctx_ -> {
						String response = null;
						// String pathInfo = ctx_.contextPath();// .pathInfo();
						String path = ctx_.path();// .pathInfo();
						// String cmd = path.substring(path.lastIndexOf("settings/") + "settings/".length());
						String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());

						// Map<String, String> paramMap = ctx_.pathParamMap();
						// String cmd = String.join("/", paramMap.keySet());// .splat());
						String bodyStr = ctx_.body();
						try {
							// response = _httpTools.forwardPost(String.format("settings/%s", cmd), bodyStr);
							response = _httpTools.forwardPost(command, bodyStr);
						} catch (Exception ex) {
							log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
							response = "error";
						}
						ctx_.result(response);
					});
					get("/user_alerts/*", ctx_ -> {
						String response = getRequest(ctx_);
						ctx_.result(response);
					});
					post("/user_alerts", ctx_ -> {
						String response = postRequest(ctx_);
						ctx_.result(response);
					});
					get("/user_config/*", ctx_ -> {
						String response = getRequest(ctx_);
						ctx_.result(response);
					});
					post("/user_config", ctx_ -> {
						String response = postRequest(ctx_);
						ctx_.result(response);
					});
					get("/user_notes/*", ctx_ -> {
						String response = getRequest(ctx_);
						ctx_.result(response);
					});
					post("/user_notes", ctx_ -> {
						String response = postRequest(ctx_);
						ctx_.result(response);
					});
					// post("/research", ctx_ -> {
					// String response = postRequest(ctx_);
					// ctx_.result(response);
					// });
					get("/ext_data", ctx_ -> {
						String response = getRequest(ctx_);
						ctx_.result(response);
					});
				});

				path("/api/v2/portfolio", () -> {
					get("/exposure", ctx_ -> {
						File file = new File(getClass().getClassLoader().getResource("test-exposure.json").getFile());
						String content = new String(Files.readAllBytes(file.toPath()));
						// log.info("json: " + content);
						ctx_.result(content);
					});
					get("/positions", ctx_ -> {
						String queryString = ctx_.queryString();
						String content = _httpTools.getPortfolio(queryString);
						try {
							// @SuppressWarnings("unused")
							// TdnPortfolioResponse data = _gson.fromJson(content, TdnPortfolioResponse.class);
						} catch (Exception ex) {
							log.error("Error in payload: " + content, ex);
						}
						ctx_.result(content);
					});
					post("/positions", ctx_ -> {
						String content = _httpTools.getPortfolio(null);
						try {
							// @SuppressWarnings("unused")
							// TdnPortfolioResponse data = _gson.fromJson(content, TdnPortfolioResponse.class);
						} catch (Exception ex) {
							log.error("Error in payload: " + content, ex);
						}
						ctx_.result(content);
					});
				});

				path("/api/v2/system", () -> {
					post("/config", ctx_ -> {
						String sessionTokenStr = ctx_.header("Authorization");
						// TODO: Verify user has admin privileges
						String userId = ATServiceRegistry.getUserService().getUserId(sessionTokenStr);
						// TdnToken token = _sessionMap.get(sessionTokenStr);
						String body = ctx_.body();
						TdnJsonWrapper wrapper = new TdnJsonWrapper();
						if (userId == null || config == null) {
							log.error(String.format("Sys config missing token/data [%s]", ctx_.ip()));
							ctx_.status(404);
							wrapper.error = "Missing token/data";
						} else {
							try {
								TdnSysConfig sysConfig = _gson.fromJson(body, TdnSysConfig.class);
								_firebaseTools.saveSystemConfig(sysConfig);
								wrapper.data = "OK";
								log.info(String.format("Saved system config [%s]: %s", userId, body));
							} catch (Exception ex) {
								wrapper.error = String.format("Failed to save system config");
							}
						}
						ctx_.result(_gson.toJson(wrapper));
						// return result ? String.format("{\"data\":%b}", true) : String.format("{\"error\":%s}", "Failed");
					});
				});

				// path("/api/v2/research", () -> {
				// get("/options", ctx_ -> {
				// String queryString = ctx_.queryString();
				// String content = _httpTools.getOptionResearch(queryString);
				// try {
				// @SuppressWarnings("unused")
				// OptionRessearchResponseWrapper data = _gson.fromJson(content, OptionRessearchResponseWrapper.class);
				// } catch (Exception ex) {
				// log.error("Error in payload: " + content);
				// }
				// ctx_.result(content);
				// });
				// get("/equity", ctx_ -> {
				// String queryString = ctx_.queryString();
				// String content = _httpTools.getEquityResearch(queryString);
				// try {
				// @SuppressWarnings("unused")
				// TdnEquityResearchResponseWrapper data = _gson.fromJson(content, TdnEquityResearchResponseWrapper.class);
				// } catch (Exception ex) {
				// log.error("Error in payload: " + content);
				// }
				// ctx_.result(content);
				// });
				// });

				// path("/api/v2/user", () -> {
				// post("/note", ctx_ -> {
				// String body = ctx_.body();
				// String content = _httpTools.postNote(body);
				// ctx_.result(content);
				// });
				// });

				get("/api/v2/security/{security}", ctx_ -> {
					String security = ctx_.pathParam("security");// .formParam(":security");
					Map<String, List<String>> queryMap = ctx_.queryParamMap();// .queryMap().toMap();
					String content = _httpTools.getSecurityNew(security, queryMap);
					ctx_.result(content);
				});

				get("/api/v2/lookup/{symbol}", ctx_ -> {
					String symbol = ctx_.pathParam("symbol");// .params(":symbol");
					String content = _httpTools.lookupSymbol(symbol);
					ctx_.result(content);
				});

			});
		});
	}

	private void validateIp(String ipStr_, String uid_) {
		if (StringUtils.isBlank(ipStr_) || StringUtils.isBlank(uid_))
			return;
		// || ipStr_.startsWith("173.63.145") || ipStr_.startsWith("192.168.8")
		if (false && ipStr_.startsWith("124.123.105")) {
			String cacheId = String.format("%s:%s", ipStr_, uid_);
			long accessCount = this._ipCache.getUnchecked(cacheId) + 1;
			if (accessCount > 15) {
				throw new ATException(String.format("Access violation [%s]", cacheId));
			}
			this._ipCache.put(cacheId, accessCount);
			log.info(String.format("Suspicious access [%s / %d]", cacheId, accessCount));
			Map<String, String> msgMap = new HashMap<>();
			msgMap.put("icon", "warning_amber");
			msgMap.put("title", "Suspicious Activity Detected");
			msgMap.put("body", "Please contact Tradonado administration immediately to prevent lockout!");
			WSMessageWrapper sseMsg = new WSMessageWrapper();
			sseMsg.setData(msgMap);
			sseMsg.setFunction("user");
			sseMsg.setType("user");
			sseMsg.setSource("server");
			sseMsg.setTarget(uid_);
			_sseSvc.send(sseMsg);
		}
	}

	private String getRequest(Context ctx_) {
		String response = null;
		String queryString = ctx_.queryString();
		String path = ctx_.path();// ..pathInfo();
		String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
		try {
			response = _httpTools.forwardGet(command, queryString);
		} catch (Exception ex) {
			log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
			response = "error";
		}
		return response;
	}

	private String postRequest(Context ctx_) {
		String response = null;
		// String pathInfo = ctx_.contextPath();// .pathInfo();
		String path = ctx_.path();// .pathInfo();
		String command = path.substring(path.indexOf(apiV2Path) + apiV2Path.length());
		String bodyStr = ctx_.body();
		try {
			response = _httpTools.forwardPost(command, bodyStr);
		} catch (Exception ex) {
			log.error(String.format("Error forwarding [%s]: %s", path, response), ex);
			response = "error";
		}
		return response;
	}

	@Override
	public void notify(WSMessageWrapper msg_, boolean fromServer_) {
		_sseSvc.send(msg_);
	}

	@Override
	public void notify(EATChangeOperation action_, ATSession session_) {
		_kafkaUserSessionProducer.send(action_, session_);
	}

	@Override
	public void notifyPushNotification(PushNotification msg_) {
		Collection<String> userIds = msg_.getUserIdsList();
		ATProtoPushNotificationWrapper pushWrapper = new ATProtoPushNotificationWrapper(msg_);
		ATGenericNotification notification = pushWrapper.value();
		this.pushToSse(userIds, notification);
	}

	private void pushToSse(Collection<String> userIds_, ATGenericNotification notification_) {
		// NOTE: Consider testing if UID active to save rsrc
		if (notification_ == null || userIds_ == null || userIds_.isEmpty())
			return;
		String type = notification_.getType();
		WSMessageWrapper sseMsg = new WSMessageWrapper();
		sseMsg.setData(notification_);
		sseMsg.setFunction(type);
		sseMsg.setType(type);
		sseMsg.setSource("server");
		// if ("px".equals(type) && !notification_.getLabel().startsWith("###")) {
		// log.info("hello");
		// }
		Iterator<String> it = userIds_.iterator();
		while (it.hasNext()) {
			String target = it.next();
			sseMsg.setTarget(target);
			if (log.isTraceEnabled()) {
				log.trace(String.format("SSE [%s/%s]", type, target));
			}
			_sseSvc.send(sseMsg);
		}
	}

	@Override
	public void notifySseMessage(SseMessage msg_) {
		try {
			WSMessageWrapper sseMsg = null;
			ProtocolStringList targets = msg_.getTargetsList();
			if (targets.isEmpty())
				return;
			String type = msg_.getType();
			switch (type) {
				case ATProtoMarketDataWrapper.TYPE_MARKET_DATA:
					MarketData protoMkt = msg_.getData().unpack(MarketData.class);
					ATProtoMarketDataWrapper mktWrapper = new ATProtoMarketDataWrapper(protoMkt);
					ATMarketData mkt = mktWrapper.value();

					sseMsg = new WSMessageWrapper();
					sseMsg.setData(Arrays.asList(mkt));
					sseMsg.setFunction(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
					sseMsg.setType(type);
					sseMsg.setSource("server");
					break;
				case ATProtoMarketDataWrapper.TYPE_MARKET_DATA_PACK:
					MarketDataPack protoMktPack = msg_.getData().unpack(MarketDataPack.class);
					List<MarketData> protoMkts = protoMktPack.getMarketDataList();
					if (protoMkts == null || protoMkts.isEmpty())
						return;
					String mktPackType = protoMktPack.getType();

					Map<String, ATMarketData> mktMap = new TreeMap<>();
					for (MarketData proto : protoMkts) {
						mktMap.put(proto.getId(), ATProtoMarketDataWrapper.toMarketData(proto));
					}
					ATSecurityPack pack = new ATSecurityPack(null, null, mktMap);
					sseMsg = new WSMessageWrapper();
					sseMsg.setSource("server");
					sseMsg.setType(mktPackType);
					sseMsg.setFunction(mktPackType);
					sseMsg.setData(pack);
					break;
				case ATProtoMarketDataWrapper.TYPE_MARKET_AVAILABLE:
					MarketDataStatus protoMkStatus = msg_.getData().unpack(MarketDataStatus.class);
					String mktAvailType = protoMkStatus.getType();
					String data = protoMkStatus.getData();
					sseMsg = new WSMessageWrapper();
					sseMsg.setSource("server");
					sseMsg.setType(mktAvailType);
					sseMsg.setFunction(mktAvailType);
					sseMsg.setData(Arrays.asList(data));
					break;
				case ATProtoSecurityPackWrapper.TYPE_MOVER_PACK:
				case ATProtoSecurityPackWrapper.TYPE_TREND_PACK:
					SecurityPack protoSecPack = msg_.getData().unpack(SecurityPack.class);
					ATSecurityPack secPack = ATProtoSecurityPackWrapper.toSecurityPack(protoSecPack);
					sseMsg = new WSMessageWrapper();
					sseMsg.setSource("server");
					sseMsg.setType(type);
					sseMsg.setFunction(type);
					sseMsg.setData(secPack);
					break;
				default:
					log.warn(String.format("notifySseMessage: Unsupported message type [%s]", type));
					break;
			}
			if (sseMsg != null) {
				Iterator<String> it = targets.iterator();
				while (it.hasNext()) {
					String target = it.next();
					sseMsg.setTarget(target);
					if (log.isTraceEnabled()) {
						log.trace(String.format("notifySseMessage SSE [%s/%s]", type, target));
					}
					_sseSvc.send(sseMsg);
				}
			}
		} catch (Exception ex) {
			log.error(String.format("notifySseMessage Error [%s]", msg_.getType()), ex);
		}
	}

	private void verifyTest(String path_, String body_) {
		final List<String> permittedPath = Arrays.asList("dynamic_symbols", "opportunities", "security",
				"user_setting");
		String pathEnd = path_.substring(path_.lastIndexOf('/') + 1);
		if (!permittedPath.contains(pathEnd)) {
			throw new ATException(String.format("Unauthorized access [%s]", path_));
		}
	}

	private void verifyExternal(String ip_, String path_, String key_) {
		Configuration config = ATConfigUtil.config();
		Long ipCount = this._extCache.getIfPresent(ip_);
		long maxCount = config.getLong("gateway.ext.count");
		if (ipCount != null) {
			if (ipCount++ > maxCount) {
				String msg = String.format("Request allowance exceeded [%s:%d/%d]", ip_, ipCount, maxCount);
				log.error(msg);
				throw new ATException(msg);
			}
		} else {
			ipCount = 1L;
		}
		this._extCache.put(ip_, ipCount);

		final List<String> permittedPath = Arrays.asList("ext_data");
		String pathEnd = path_.substring(path_.lastIndexOf('/') + 1);
		String key = ATConfigUtil.config().getString("gateway.external.key");
		boolean pass = key.equals(key_) && permittedPath.contains(pathEnd);
		if (!pass) {
			throw new ATException(String.format("Unauthorized access [%s]", path_));
		}
	}

	@Override
	public void notifySystemStatus(String source_, Map<String, String> status_) {
		if (StringUtils.isBlank(source_) || status_ == null || status_.isEmpty())
			return;
		_systemStatusMap.put(source_, status_);
	}

	public static class ATWebResponseWrapper {
		public String type;
		public Object data;
		public Object error;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (ATGatewayServerJavalin.this._isRunning) {
					long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
					Map<String, String> status = ATSystemStatusManager.getStatus();
					ATGatewayServerJavalin.this._statusProducer.send(ATGatewayServerJavalin.ID, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (ATGatewayServerJavalin.this._isRunning) {
					run();
				}
			}
		}
	}

}
