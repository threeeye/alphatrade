package com.isoplane.at.gateway.model;

public class OptionIdeaOption {
	
	public String exp;
	public Double strike;
	public Double count;
	
	public Double px;
	public Double pxAsk;
	public Double pxBid;
	public Double pxLoD;
	public Double pxHiD;
	
}
