package com.isoplane.at.gateway.model;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class TdnNotificationSubscription implements Serializable {

	private static final long serialVersionUID = 1L;
	public String token;
	public String mode;
//	public transient String uid;

	public boolean verify() {
		return StringUtils.isNotBlank(token) && StringUtils.isNotBlank(mode);
	}
}
