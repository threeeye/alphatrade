package com.isoplane.at.gateway.util;

import com.isoplane.at.commons.model.WSMessageWrapper;

public interface IATWebSocketListener {

	void notify(WSMessageWrapper message, boolean fromServer_);

}
