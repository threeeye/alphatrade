package com.isoplane.at.gateway.model;

import java.io.Serializable;

public class TdnJsonWrapper implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Object data;
	public String error;
	public Integer errorCode;
}
