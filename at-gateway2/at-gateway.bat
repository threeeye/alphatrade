title AT - Gateway Server
REM -Dorg.eclipse.jetty.server.Request.maxFormContentSize=6000000
SET dir=C:/Dev/workspace/alphatrade
C:/Dev/Java/jdk-10.0.2/bin/java -Xmx2048m -XX:MaxDirectMemorySize=1500m -Dorg.eclipse.jetty.server.Request.maxFormContentSize=5000000 -Djava.library.path="C:\Dev\workspace\alphatrade\at-ext\lib\;${env_var:PATH}" -Dlogback.configurationFile=//%dir%/at-gateway2/at-gateway-logback.xml -jar %dir%/jar/at-gateway-server.jar %dir%/NOCHECKIN.properties,%dir%/at-gateway2/at-gateway.properties,%dir%/local.properties
