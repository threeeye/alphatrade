package com.isoplane.at.svc.push;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.SendResponse;
import com.google.firebase.messaging.WebpushConfig;
import com.google.firebase.messaging.WebpushNotification;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.util.ATConfigUtil;

//http://csbruce.com/software/utf-8.html
public class ATFirebaseProtoAdapter {

	static final Logger log = LoggerFactory.getLogger(ATFirebaseProtoAdapter.class);

	static final String FB_ID = "tradonado";

	static private ATFirebaseProtoAdapter _instance;

	private Map<String, PushNotification> _notificationMap = new HashMap<>();
	private FirebaseApp _firebaseNew;
	private FirebaseMessaging _firebaseMessaging;
	private Map<String, ATUser> _users = new HashMap<>();

	static public ATFirebaseProtoAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATFirebaseProtoAdapter();
			_instance.init();
		}
		return _instance;
	}

	private ATFirebaseProtoAdapter() {
	}

	public void init() {
		try {
			Configuration config = ATConfigUtil.config();

			String fbCredPath = config.getString("firebase.trd.cred");
			// String newFirebaseDbURL = _oldConfig.getString("firebase.trd.cred");
			InputStream newServiceAccount = new FileInputStream(fbCredPath);
			GoogleCredentials newCredentials = GoogleCredentials.fromStream(newServiceAccount);
			FirestoreOptions fsOptions = FirestoreOptions.newBuilder().build();
			FirebaseOptions newOptions = new FirebaseOptions.Builder()
					.setCredentials(newCredentials)
					// .setDatabaseUrl(newFirebaseDbURL)
					.setFirestoreOptions(fsOptions)
					.build();
			_firebaseNew = FirebaseApp.initializeApp(newOptions, FB_ID);
			_firebaseMessaging = FirebaseMessaging.getInstance(_firebaseNew);

			log.info(String.format("Firebase initialized [%s]", FirebaseApp.getInstance(FB_ID).getName()));
		} catch (Exception ex) {
			log.error("Error initializing Firebase", ex);
		}
	}

	public boolean subscribeNotifications(String uid_, String token_, String mode_) {
		if (StringUtils.isBlank(uid_) || StringUtils.isBlank(token_) || StringUtils.isBlank(mode_)) {
			log.error(String.format("Missing notification subscription data [%s, %s, %s]", uid_, token_, mode_));
			return false;
		}
		try {
			List<String> tokens = Arrays.asList(token_);
			String generalTopic = "general";
			int count = 0;
			switch (mode_) {
			case IATUser.MESSAGE_NONE:
				count += _firebaseMessaging.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_MIN)).getFailureCount();
				count += _firebaseMessaging.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_ALL)).getFailureCount();
				count += _firebaseMessaging.unsubscribeFromTopic(tokens, generalTopic).getFailureCount();
				break;
			case IATUser.MESSAGE_MIN:
				count += _firebaseMessaging.subscribeToTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_MIN)).getFailureCount();
				count += _firebaseMessaging.unsubscribeFromTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_ALL)).getFailureCount();
				count += _firebaseMessaging.unsubscribeFromTopic(tokens, generalTopic).getFailureCount();
				break;
			case IATUser.MESSAGE_ALL:
				count += _firebaseMessaging.subscribeToTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_MIN)).getFailureCount();
				count += _firebaseMessaging.subscribeToTopic(tokens, String.format("%s_%s", uid_, IATUser.MESSAGE_ALL)).getFailureCount();
				count += _firebaseMessaging.subscribeToTopic(tokens, generalTopic).getFailureCount();
				break;
			default:
				log.error(String.format("Invalid mode [%s]", mode_));
				return false;
			}
			log.debug(String.format("Notification subscription [%s: %s_%s]: %s", count == 0 ? "Success" : "Failure", uid_, mode_, token_));
			return count == 0;
		} catch (Exception ex) {
			log.error(String.format("Error subscribing [%s, %s, %s]", uid_, token_, mode_), ex);
			return false;
		}
	}

	public Set<String> pushNotification(PushNotification notification_, List<String> tokens_) {
		try {
			// Configuration config = ATConfigUtil.config();
			// ATNotificationContent content = notification_.getNotificationContent(_notificationMap,
			// config.getLong("notification.timeout", 60000));
			if (tokens_ == null || tokens_.isEmpty())
				return null;
			String meta = notification_.getMeta();
			Map<String, Object> data = new HashMap<>();
			data.put("symbol", notification_.getSymbol());
			if (!StringUtils.isBlank(meta)) {
				data.put("meta", meta);
			}

			WebpushConfig wpConfig = WebpushConfig.builder()
					.setNotification(
							WebpushNotification.builder()
									.setTitle(notification_.getTitle())
									.setBody(notification_.getBody())
									.setIcon(String.format("https://tradonado.com/assets/icons/%s", notification_.getIcon()))
									.putCustomData("click_action",
											notification_.getClickAction() == null ? "https://tradonado.com" : notification_.getClickAction())
									.setSilent(notification_.getIsSilent() || !notification_.getIsActive())
									.setRenotify(false)
									.setTimestampMillis(System.currentTimeMillis())
									.setTag(notification_.getTag())
									.setBadge("https://tradonado.com/assets/icons/icon-16x16.png")
									.setData(data)
									.build())
					// .setFcmOptions(WebpushFcmOptions.withAnalyticsLabel("tradonado"))
					.build();
			MulticastMessage message = MulticastMessage.builder()
					.setWebpushConfig(wpConfig)
					// .putData("score", "850")
					// .putData("time", "2:45")
					.addAllTokens(tokens_)
					.build();
			BatchResponse response = _firebaseMessaging.sendMulticast(message);
			if (response.getFailureCount() > 0) {
				List<SendResponse> responses = response.getResponses();
				Set<String> failedTokens = new HashSet<>();
				for (int i = 0; i < responses.size(); i++) {
					if (!responses.get(i).isSuccessful()) {
						// The order of responses corresponds to the order of the registration tokens.
						failedTokens.add(tokens_.get(i));
					}
				}
				log.error(String.format("Failed messaging: %s", failedTokens));
				return failedTokens;
			}
			return null;
		} catch (Exception ex) {
			log.error(String.format("Failed to send notification"), ex);
			return null;
		}
	}

	public void reset() {
		log.info("Reseting notification state");
		_notificationMap.clear();
	}

	public ATUser getUser(String email_) {
		if (StringUtils.isBlank(email_))
			return null;
		String key = email_.replace(".", "%20");
		ATUser user = _users.get(key);
		return user;
	}

}
