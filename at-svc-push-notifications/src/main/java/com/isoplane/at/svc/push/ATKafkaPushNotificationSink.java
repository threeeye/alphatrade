package com.isoplane.at.svc.push;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoPushNotificationSubscriber;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
//import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationConsumer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.users.ATUserManager;

public class ATKafkaPushNotificationSink implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaPushNotificationSink.class);

	static private String ID;
	static private ATKafkaPushNotificationSink _instance;

	private final CountDownLatch _serviceLatch;
	private boolean _isRunning;

	private ATKafkaSystemStatusProducer _statusProducer;

	private PushWorker _pushWorker;

	private SessionWorker _sessionWorker;

	static public void main(String[] args_) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);

			final String ipStr = ATSysUtils.getIpAddress("n/a");
			// ATSysLogger.setLoggerClass(String.format("TradierAgent.[%s]", ipStr));

			ID = String.format("%s:%s", ATKafkaPushNotificationSink.class.getSimpleName(), ipStr);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					ATExecutors.shutdown();
					_instance.shutdown();
				}
			}));

			ATExecutors.init();
			ATKafkaRegistry.register("push_sink",
					ATKafkaPushNotificationConsumer.class,
					ATKafkaSystemStatusProducer.class,
					ATKafkaUserSessionConsumer.class);

			IATUserProvider userSvc = new ATUserManager(null, true);
			ATServiceRegistry.setUserService(userSvc, true);

			_instance = new ATKafkaPushNotificationSink();
			_instance.init();

			String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
			log.info(msg);
			// ATSysLogger.info(String.format("%s [%s] started", _instance.getClass().getSimpleName(), ipStr));

			_instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaSystemStatusProducer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaPushNotificationSink() {
		_serviceLatch = new CountDownLatch(2);
	}

	private void init() {
		ATSystemStatusManager.register(this);

		_pushWorker = new PushWorker();
		ATKafkaPushNotificationConsumer pushNotificationConsumer = ATKafkaRegistry.get(ATKafkaPushNotificationConsumer.class);
		pushNotificationConsumer.subscribe(_pushWorker);

		_sessionWorker = new SessionWorker();
		ATKafkaUserSessionConsumer userSessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		userSessionConsumer.subscribe(_sessionWorker);
	}

	private void start() {
		this._isRunning = true;
		ATKafkaRegistry.start();

		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());
	}

	protected void shutdown() {

	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.push", String.format("%d", this._pushWorker._pushCount));
		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			// NOTE: Dual nested to (a) reduce try/cath overhead (b) avoid nested iteration
			while (_isRunning) {
				try {
					while (_isRunning) {
						long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
						Map<String, String> status = ATSystemStatusManager.getStatus();
						_statusProducer.send(ID, status);
						Thread.sleep(statusPeriod);
					}
				} catch (Exception ex) {
					log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				}
			}
			log.info(String.format("%s Stopped", this.getClass().getSimpleName()));
		}
	}

	private class SessionWorker implements IATProtoUserSubscriber {

		private ATFirebaseProtoAdapter _firebaseAdapter;
		private IATUserProvider _userSvc;

		public SessionWorker() {
			this._firebaseAdapter = ATFirebaseProtoAdapter.getInstance();
			this._userSvc = ATServiceRegistry.getUserService();
		}

		@Override
		public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
			try {
				if (!EATChangeOperation.UPDATE.equals(action_) || session_ == null)
					return;
				String userId = session_.getUserId();
				Map<String, String> data = session_.getData();
				String mode = data != null ? data.get("mode") : null;
				String token = data != null ? data.get("token") : null;
				boolean result = this._firebaseAdapter.subscribeNotifications(userId, token, mode);

				if (result) {
					ATUser user = _userSvc.getUser(userId);
					if (user != null) {
						if (user.updateMessageToken(mode, token)) {
							result = _userSvc.updateUser(user);
						}
					}
				}
				log.info(String.format("notifyUserSession Updated {user: %s, mode: %s, token: %s}", userId, mode, token));
			} catch (Exception ex) {
				log.error(String.format("notifyPushNotification Error"), ex);
			}
		}

	}

	private class PushWorker implements IATProtoPushNotificationSubscriber {

		private ATFirebaseProtoAdapter _firebaseAdapter;
		private IATUserProvider _userSvc;

		private long _lastTimestamp;
		private long _pushCount;
		private Set<String> _tokenBlacklist = new HashSet<>();

		public PushWorker() {
			this._firebaseAdapter = ATFirebaseProtoAdapter.getInstance();
			this._userSvc = ATServiceRegistry.getUserService();
		}

		@Override
		public void notifyPushNotification(PushNotification msg_) {
			try {
				Collection<String> userIds = msg_.getUserIdsList();
				if (System.currentTimeMillis() - this._lastTimestamp > 300000) {
					this._lastTimestamp = System.currentTimeMillis();
					log.info(String.format("Push count [%d]", this._pushCount));
				}
				if (userIds == null || userIds.isEmpty())
					return;
				Configuration config = ATConfigUtil.config();
				long timeout = config.getLong("push.timeout", 300000);
				if (System.currentTimeMillis() - msg_.getTimestamp() > timeout) {
					return;
				}
				boolean isTest = msg_.hasIsTest();
				if (!isTest) {
					String startStr = config.getString("push.starttime", "08:30");
					LocalTime startTime = LocalTime.parse(startStr.length() < 5 ? "0" + startStr : startStr);
					String endStr = config.getString("push.endtime", "16:30");
					LocalTime endTime = LocalTime.parse(endStr.length() < 5 ? "0" + endStr : endStr);
					LocalTime now = LocalTime.now();
					if (startTime.isAfter(now) || endTime.isBefore(now)) {
						log.debug(String.format("Outside notification hours [%s - %s]", startTime, endTime));
						return;
					}
				}
				String mode = msg_.getMode();
				Map<Set<String>, ATUser> tokenUserMap = new HashMap<>();
				List<String> messageTokens = new ArrayList<>();
				for (String userId : userIds) {
					ATUser user = this._userSvc.getUser(userId);
					Set<String> userMessageTokens = user != null ? user.getMessageTokens(mode) : null;
					if (userMessageTokens != null) {
						messageTokens.addAll(userMessageTokens);
						tokenUserMap.put(userMessageTokens, user);
					}
				}
				// messageTokens.removeAll(this._tokenBlacklist);
				if (messageTokens.isEmpty())
					return;
				_pushCount++;
				Set<String> failedTokens = this._firebaseAdapter.pushNotification(msg_, messageTokens);
				if (failedTokens != null && !failedTokens.isEmpty()) {
					this._tokenBlacklist.addAll(failedTokens);
					log.info(String.format("blacklist %s", this._tokenBlacklist));
					// this._userSvc.removeMessageTokens(failedTokens);
				}
			} catch (Exception ex) {
				log.error(String.format("notifyPushNotification Error"), ex);
			}
		}

	}

}
