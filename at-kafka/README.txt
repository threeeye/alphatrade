Insstall and config confluent/kafka
https://docs.confluent.io/current/installation/installing_cp/deb-ubuntu.html

Create prptobuf classes:
C:\Dev\util\protoc-3.13.0-win64\bin\protoc.exe -I="C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf\" --java_out="C:\Dev\workspace\alphatrade\at-commons\target\generated-sources\"  --experimental_allow_proto3_optional C:\Dev\workspace\alphatrade\at-commons\src\main\protobuf\market_data.proto

Get topic info:
kafka-configs --bootstrap-server 192.168.8.99:9092 --entity-type topics --describe --entity-name user_session

Update cleanup policy (example compact, delete>6h):
kafka-configs --bootstrap-server 192.168.8.99:9092 --topic user_session --alter --add-config cleanup.policy=compact,delete.retention.ms=21600000