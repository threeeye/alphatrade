package com.isoplane.at.kafka.user;

import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.protobuf.ATProtoSessionWrapper;
import com.isoplane.at.commons.model.protobuf.UserSessionProtos.UserSession;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaUserSessionProducer extends ATKafkaBaseProducer<UserSession> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUserSessionProducer.class);

	public static final String TOPIC = "user_session";

	public ATKafkaUserSessionProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, UserSession.class);
	}

	public void send(EATChangeOperation action_, ATSession session_) {
		this.send(__topic, action_, session_);
	}

	public void send(String topic_, EATChangeOperation action_, ATSession session_) {
		if (session_ == null || action_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}
		String topic = StringUtils.isBlank(topic_) ? __topic : topic_;
		String id = session_.getUserId();
		UserSession ussn = ATProtoSessionWrapper.toUserSession(session_, action_, __source, __isTestProducer);

		__producer.send(new ProducerRecord<String, UserSession>(topic, id, ussn));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent: %s", id));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}

}
