package com.isoplane.at.kafka.sse;

import java.util.Map;
import java.util.TreeMap;

import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKafkaSseMessageProducer extends ATKafkaBaseProducer<SseMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSseMessageProducer.class);

	public static final String TOPIC = "sse";

	private long _recordCount;

	public ATKafkaSseMessageProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SseMessage.class);

	//	ATSystemStatusManager.register(this);
	}

	public void send(SseMessage msg_) {
		if (msg_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}

		String id = msg_.getType();
		__producer.send(new ProducerRecord<String, SseMessage>(__topic, id, msg_));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent: %s", id));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}
}
