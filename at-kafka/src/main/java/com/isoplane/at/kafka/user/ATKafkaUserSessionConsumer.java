package com.isoplane.at.kafka.user;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.protobuf.ATProtoSessionWrapper;
import com.isoplane.at.commons.model.protobuf.UserSessionProtos.UserSession;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaUserSessionConsumer extends ATKafkaBaseConsumer<UserSession> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUserSessionConsumer.class);

	public static final String TOPIC = "user_session";

	private Set<IATProtoUserSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaUserSessionConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, UserSession.class);
		this._subscribers = new HashSet<>();

	//	ATSystemStatusManager.register(this);
	}

	@Override
	protected void start() {
		__isRunning = true;
		try {
			while (__isRunning) {
				ConsumerRecords<String, UserSession> records = super.poll();
				if (records != null && records.count() > 0 && !_subscribers.isEmpty()) {
					for (ConsumerRecord<String, UserSession> record : records) {
						if (this.__groupId.contains("user_session_ameritrade")) {
							log.debug(this.__groupId);
						}
						UserSession proto = record.value();
						ATSession session = ATProtoSessionWrapper.toUserSession(proto);
						EATChangeOperation action = EATChangeOperation.valueOf(proto.getActionCode());
						for (IATProtoUserSubscriber subscriber : this._subscribers) {
							subscriber.notifyUserSession(action, session);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("UserSession [%s]: %s", action, session);
							log.debug(msg);
						}
					}
					_recordCount += records.count();
				}
				super.commit(false);
			}
		} catch (Throwable ex) {
			log.error(String.format("poll Error"));
			// log.error(String.format("poll Error"), ex);
			// super.commit(true);
			if (__isRunning) {
				start();
			}
		}
		super.close();
	}

	public void subscribe(IATProtoUserSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}
}
