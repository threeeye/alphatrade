package com.isoplane.at.kafka.eventbus;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.EventubusMessageProtos.EventbusMessage;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaEventbusProducer extends ATKafkaBaseProducer<EventbusMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaEventbusProducer.class);

	public static final String TOPIC = "at_eventbus";

	private long _recordCount;

	public ATKafkaEventbusProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, EventbusMessage.class);
	}

	public void send(EventbusMessage msg_) {
		if (msg_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}
		String type = msg_.getType();
		__producer.send(new ProducerRecord<String, EventbusMessage>(__topic, type, msg_));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%s]", type));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
