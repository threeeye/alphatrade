package com.isoplane.at.kafka.symbol;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoSymbolPackWrapper;
import com.isoplane.at.commons.model.protobuf.SymbolPackProtos.SymbolPack;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaSymbolPackProducer extends ATKafkaBaseProducer<SymbolPack> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSymbolPackProducer.class);

	public static final String TOPIC = "symbols";

	private long _recordCount;

	public ATKafkaSymbolPackProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SymbolPack.class);
	}

	public void send(ATProtoSymbolPackWrapper wrapper_) {
		String key = wrapper_.getKey();
		if (key == null) {
			key = wrapper_.getTarget();
		}
		__producer.send(new ProducerRecord<String, SymbolPack>(__topic, key, wrapper_.proto()));
		this._recordCount++;
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent: %s", key));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
