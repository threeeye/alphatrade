package com.isoplane.at.kafka.symbol;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoSymbolPackWrapper;
import com.isoplane.at.commons.model.protobuf.SymbolPackProtos.SymbolPack;
import com.isoplane.at.commons.service.IATProtoSymbolSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaSymbolPackConsumer extends ATKafkaBaseConsumer<SymbolPack> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSymbolPackConsumer.class);

	public static final String TOPIC = "symbols";

	private Set<IATProtoSymbolSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaSymbolPackConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SymbolPack.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, SymbolPack> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, SymbolPack> record : records) {
						ATProtoSymbolPackWrapper wrapper = new ATProtoSymbolPackWrapper(record.value());
						for (IATProtoSymbolSubscriber subscriber : this._subscribers) {
							subscriber.notifySymbolPack(wrapper);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("Symbol [%s]", wrapper.getSource());
							log.debug(msg);
						}
					}
					this._recordCount += records.count();
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
			}
		}
		super.close();
	}

	public void subscribe(IATProtoSymbolSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
