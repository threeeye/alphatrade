package com.isoplane.at.kafka;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ForkJoinTask;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKafkaRegistry {

	static final Logger log = LoggerFactory.getLogger(ATKafkaRegistry.class);

	static private Map<Class<?>, Object> _kafkaMap = new HashMap<>();
	static private Map<IATKafkaRunnable, Boolean> _runnables = new HashMap<>();

	static private boolean _isRunning;
	static private String _source;

	static public void register(String source_, Class<?>... classes_) {
		ATExecutors.init();
		try {
			_source = source_;
			// String postId = getPostId(config_);
			for (Class<?> clazz : classes_) {
				if (_kafkaMap.containsKey(clazz))
					continue;
				registerClass(clazz);
			}
		} catch (Exception ex) {
			log.error("Initialization error", ex);
			throw new ATException(ex);
		}
	}

	static private void registerClass(Class<?> clazz_) throws Exception {
		if (_kafkaMap.containsKey(clazz_)) {
			log.warn(String.format("Component [%s] already registered.", clazz_.getSimpleName()));
			log.warn(String.format("Components should be registered in the main/initializing application."));
			return;
		}
		String id = String.format("%s:%s", clazz_.getSimpleName(), UUID.randomUUID());
		Constructor<?> ctr = clazz_.getConstructor(String.class, Configuration.class);
		Configuration config = ATConfigUtil.config();
		Object kafka = ctr.newInstance(id, config);
		if (kafka != null) {
			_kafkaMap.put(clazz_, kafka);
			if (kafka instanceof IATKafkaRunnable) {
				_runnables.put((IATKafkaRunnable) kafka, false);
			}
			if (kafka instanceof ATKafkaBaseProducer) {
				((ATKafkaBaseProducer<?>) kafka).setSource(_source);
			}
		}
		log.info(String.format("registerClass [%s]", id));
	}

	// static private String getPostId(Configuration config_) throws URISyntaxException, UnknownHostException {
	// String jarPath = ATKafkaRegistry.class
	// .getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
	// String jarName = StringUtils.isBlank(jarPath) ? "" : jarPath.substring(jarPath.lastIndexOf("/") + 1);
	// jarName = StringUtils.isBlank(jarName) ? "FNU" : jarName.substring(0, jarName.lastIndexOf('.'));
	//
	// String portStr = config_.getString("web.port");
	// InetAddress ip = InetAddress.getLocalHost();
	// String ipStr = ip.getHostAddress();
	// String id = String.format("%s:%s:%s", jarName, ipStr, portStr);
	// return id;
	// }

	static public void start() {
		if (_isRunning) {
			log.warn(String.format("Multiple calls to %s.start().", ATKafkaRegistry.class.getSimpleName()));
			log.warn(String.format("Components should be registered in the main/initializing application."));
			log.warn(String.format("%s.start() sould be called from the main/initializing application..",
					ATKafkaRegistry.class.getSimpleName()));
		}
		_isRunning = true;
		for (IATKafkaRunnable runnable : _runnables.keySet()) {
			if (_runnables.get(runnable))
				continue;
			ATExecutors.submit(runnable.getId(), runnable);
			_runnables.put(runnable, true);
			log.info(String.format("Starting [%s]", runnable.getId()));
		}
	}

	static public void stop() {
		try {
			//	Thread.currentThread().join();
			for (IATKafkaRunnable runnable : _runnables.keySet()) {
				runnable.stop();
				_runnables.put(runnable, false);
			}
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static public <T> T get(Class<T> class_) {
		@SuppressWarnings("unchecked")
		T kafka = (T) _kafkaMap.get(class_);
		if (kafka == null) {
			throw new ATException(String.format("Component [%s] not registered.", class_.getSimpleName()));
		}
		return kafka;
	}

}
