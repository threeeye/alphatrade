package com.isoplane.at.kafka;

public interface IATKafkaRunnable extends Runnable {

	// public void start();

	public void stop();

	public String getId();
}
