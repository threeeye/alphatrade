package com.isoplane.at.kafka.market;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaMarketDataProducer extends ATKafkaBaseProducer<MarketData> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataProducer.class);

	public static final String TOPIC = "market_data";

	private long _recordCount;

	public ATKafkaMarketDataProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketData.class);
	}

	public void send(IATMarketData mkt_) {
		MarketData mkt = ATProtoMarketDataWrapper.toMarketData(mkt_);
		String occId = mkt.getId();
		String key = ATModelUtil.getSymbol(occId);
		__producer.send(new ProducerRecord<String, MarketData>(__topic, key, mkt));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent: %s", occId));
		}
	}

	public void send(Collection<ATProtoMarketDataWrapper> wrappers_) {
		if (wrappers_ == null)
			return;
		for (ATProtoMarketDataWrapper wrapper : wrappers_) {
			MarketData mkt = wrapper.proto();
			String occId = mkt.getId();
			String key = ATModelUtil.getSymbol(occId);
			__producer.send(new ProducerRecord<String, MarketData>(__topic, key, mkt));
			_recordCount++;
			if (log.isTraceEnabled()) {
				log.debug(String.format("Sent: %s", occId));
			}
		}
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%d]", wrappers_.size()));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
