package com.isoplane.at.kafka.market;

import java.util.Date;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketStatusProtos.MarketStatus;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaMarketStatusProducer extends ATKafkaBaseProducer<MarketStatus> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketStatusProducer.class);

	public static final String TOPIC = "market_status";

	public ATKafkaMarketStatusProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketStatus.class);
	}

	public void send(EATMarketStatus mkt_) {
		MarketStatus status = ATProtoMarketStatusWrapper.toMarketStatus(mkt_, new Date(), __isTestProducer);
		ATProtoMarketStatusWrapper wrapper = new ATProtoMarketStatusWrapper(status);
		send(wrapper);
	}

	public void send(ATProtoMarketStatusWrapper wrapper_) {
		if (wrapper_ == null)
			return;
		MarketStatus status = wrapper_.proto();
		__producer.send(new ProducerRecord<String, MarketStatus>(__topic, __topic, status));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%s/%s]", __topic, status.getStatus()));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}

}
