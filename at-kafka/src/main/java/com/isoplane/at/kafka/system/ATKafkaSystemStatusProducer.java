package com.isoplane.at.kafka.system;

import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.SystemStatusProtos.SystemStatus;
import com.isoplane.at.commons.model.protobuf.SystemStatusProtos.SystemStatus.Builder;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaSystemStatusProducer extends ATKafkaBaseProducer<SystemStatus> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSystemStatusProducer.class);

	public static final String TOPIC = "system_status";

	public ATKafkaSystemStatusProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SystemStatus.class);
	}

	public void send(String source_, Map<String, String> data_) {
		Builder builder = SystemStatus.newBuilder();
		builder.setSource(source_);
		builder.putAllData(data_);
		SystemStatus status = builder.build();

		String id = source_;
		__producer.send(new ProducerRecord<String, SystemStatus>(__topic, id, status));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent: %s", id));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}

	public static class DefaultStatusRunner implements Runnable {

		private ATKafkaSystemStatusProducer statusProducer;
		private IATSystemStatusProvider statusProvider;
		private String id;

		public DefaultStatusRunner(String id_, IATSystemStatusProvider statusProvider_, ATKafkaSystemStatusProducer statusProducer_) {
			this.id = id_;
			this.statusProvider = statusProvider_;
			this.statusProducer = statusProducer_;
		}

		@Override
		public void run() {
			try {
				while (this.statusProvider.isRunning()) {
					long statusPeriod = ATConfigUtil.isConfigured()
							? ATConfigUtil.config().getLong("status.period", 30000)
							: 30000L;
					Map<String, String> status = ATSystemStatusManager.getStatus();// ATKafkaUserMarketService.this.getStatus();
					this.statusProducer.send(this.id, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (this.statusProvider.isRunning()) {
					run();
				}
			}
		}
	}

}
