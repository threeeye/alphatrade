package com.isoplane.at.kafka.alertrule;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.AlertRuleProtos.AlertRule;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaAlertRuleProducer extends ATKafkaBaseProducer<AlertRule> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaAlertRuleProducer.class);

	public static final String TOPIC = "alert_rule";

	private long _recordCount;

	public ATKafkaAlertRuleProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, AlertRule.class);
	}

	public void send(AlertRule msg_) {
		if (msg_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}
		String type = msg_.getType();
		__producer.send(new ProducerRecord<String, AlertRule>(__topic, type, msg_));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%s]", type));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}
}
