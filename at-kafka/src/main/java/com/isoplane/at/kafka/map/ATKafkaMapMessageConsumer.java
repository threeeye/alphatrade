package com.isoplane.at.kafka.map;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.service.IATProtoMapMessageSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public abstract class ATKafkaMapMessageConsumer extends ATKafkaBaseConsumer<MapMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMapMessageConsumer.class);

	// public static final String TOPIC = "map_msg";

	private Set<IATProtoMapMessageSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaMapMessageConsumer(String id_, Configuration config_, String topic_) {
		super(id_, config_, topic_, MapMessage.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, MapMessage> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, MapMessage> record : records) {
						MapMessage proto = record.value();
						for (IATProtoMapMessageSubscriber subscriber : this._subscribers) {
							subscriber.notifyMapMessage(proto);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("MapMsg [%s]", proto.getType());
							log.debug(msg);
						}
					}
					_recordCount += records.count();
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (__isRunning) {
					start();
				}
			}
		}
		super.close();
	}

	public void subscribe(IATProtoMapMessageSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
