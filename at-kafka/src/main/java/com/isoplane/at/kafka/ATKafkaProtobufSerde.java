package com.isoplane.at.kafka;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;

import com.google.protobuf.Message;

import io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig;
import io.confluent.kafka.streams.serdes.protobuf.KafkaProtobufSerde;

public class ATKafkaProtobufSerde<T extends Message> extends KafkaProtobufSerde<T> {

	public ATKafkaProtobufSerde(Configuration config_, Class<T> class_) {
		super(class_);
		String schemaUrl = String.format("http://%s", config_.getString("schema.registry"));
		Map<String, Object> serdeConfig = new HashMap<>();
		serdeConfig.put(AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
		super.configure(serdeConfig, false);
	}

}