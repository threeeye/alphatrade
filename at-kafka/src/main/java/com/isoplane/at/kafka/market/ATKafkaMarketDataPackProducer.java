package com.isoplane.at.kafka.market;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Iterables;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaMarketDataPackProducer extends ATKafkaBaseProducer<MarketDataPack> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataPackProducer.class);

	public static final String TOPIC = "market_data_pack";

	private long _recordCount;

	public ATKafkaMarketDataPackProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketDataPack.class);
	}

	private void send(String type_, String subtype_, String source_, IATMarketData... data_) {
		MarketDataPack pack = ATProtoMarketDataPackWrapper.toMarketDataPack(type_, subtype_, source_, data_);
		ProducerRecord<String, MarketDataPack> rec = new ProducerRecord<>(__topic, type_, pack);
		this.send(rec);
	}

	public void send(String type_, String subtype_, String source_, Iterable<IATMarketData> data_) {
		IATMarketData[] array = Iterables.toArray(data_, IATMarketData.class);
		send(type_, subtype_, source_, array);
	}

	public void send(ATProtoMarketDataPackWrapper wrapper_) {
		if (wrapper_ == null)
			return;
		String type = wrapper_.getType();
		MarketDataPack pack = wrapper_.proto();
		ProducerRecord<String, MarketDataPack> rec = new ProducerRecord<>(__topic, type, pack);
		this.send(rec);
	}

	private void send(ProducerRecord<String, MarketDataPack> rec_) {
		__producer.send(rec_);
		_recordCount++;
		if (log.isDebugEnabled()) {
			MarketDataPack pack = rec_.value();
			String type = rec_.key();
			String subType = pack.hasSubType() ? pack.getSubType() : "";
			int count = pack.getMarketDataCount();
			log.debug(String.format("Sent [%s/%s/%d/%s]", type, subType, count, pack.getSymbolsList()));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
