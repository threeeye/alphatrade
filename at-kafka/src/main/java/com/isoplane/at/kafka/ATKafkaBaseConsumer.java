package com.isoplane.at.kafka;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;

import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializerConfig;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializerConfig;

public abstract class ATKafkaBaseConsumer<T> implements IATKafkaRunnable, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaBaseConsumer.class);

	protected String __id;
	protected KafkaConsumer<String, T> __consumer;
	protected boolean __isRunning = false;
	protected String __topic;
	protected String __groupId;
	protected boolean __isAutoCommit = true;
	private Duration __pollTimeout;
	protected Class<?> __keyDeserializerClass = StringDeserializer.class;
	protected Class<?> __valueDeserializerClass = KafkaProtobufDeserializer.class;

	protected ATKafkaBaseConsumer(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_,
			Class<?> keyDeserializerClass_,
			Class<?> valueDeserializerClass_) {
		__keyDeserializerClass = keyDeserializerClass_;
		__valueDeserializerClass = valueDeserializerClass_;
		this.__id = id_;
		this.init(id_, config_, topic_, payloadClass_);
	}

	public ATKafkaBaseConsumer(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_) {
		// this.__id = id_;
		this(id_, config_, topic_, payloadClass_, StringDeserializer.class, KafkaProtobufDeserializer.class);
	}

	private void init(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_) {
		if ("user_session".equals(topic_)) {
			log.debug(topic_);
		}
		String bootsrap_servers = config_.getString("kafka.servers");
		String schema_registry = config_.getString("schema.registry");
		if (StringUtils.isBlank(bootsrap_servers) || StringUtils.isBlank(schema_registry)) {
			throw new ATException(String.format("Kafka Servers or Schema Registry missing"));
		}
		String topicKey = String.format("kafka.%s.topic", topic_);
		String topicKeyIn = String.format("kafka.%s.topic.in", topic_);
		String topicKeyOther = String.format("kafka.%s.topic.other", topic_);

		try {
			__pollTimeout = Duration.ofMillis(config_.getLong(getPollTimeoutKey(topic_), 500));
			__topic = config_.getString(topicKeyIn, config_.getString(topicKey, topic_));
			String otherTopics = config_.getString(topicKeyOther, null);
			Collection<String> topicsA = Arrays.asList(__topic);
			Collection<String> topicsB = StringUtils.isBlank(otherTopics)
					? new HashSet<>()
					: Arrays.asList(otherTopics.trim().split("\\s*,\\s*"));
			@SuppressWarnings("unchecked")
			Collection<String> topics = CollectionUtils.union(topicsA, topicsB);
			__groupId = config_.getString(getGroupKey(__topic), __topic);
			if (config_.getBoolean(getGroupUniqueKey(__topic), false)) {
				__groupId = String.format("%s_%s", __groupId, UUID.randomUUID());
			}
			String offsetResetKey = String.format("kafka.%s.offset.reset", __topic);
			String offsetReset = config_.getString(offsetResetKey, "latest");
			String autoCommitKey = String.format("kafka.%s.auto.commit", __topic);
			__isAutoCommit = config_.getBoolean(autoCommitKey, true);
			String seekToBeginningKey = String.format("kafka.%s.seek.to.beginning", __topic);
			boolean seekToBeginning = config_.getBoolean(seekToBeginningKey, false);

			Properties props = new Properties();
			props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootsrap_servers);
			props.put(ConsumerConfig.CLIENT_ID_CONFIG, id_);
			props.put(ConsumerConfig.GROUP_ID_CONFIG, String.format("protobuf-consumer-group-%s", __groupId));
			props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, __isAutoCommit);
			props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offsetReset);
			props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, __keyDeserializerClass);
			props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, __valueDeserializerClass);
			props.put(KafkaProtobufSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, String.format("http://%s", schema_registry));
			props.put(KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE, payloadClass_);

			__consumer = new KafkaConsumer<>(props);
			__consumer.subscribe(topics, new ConsumerRebalanceListener() {

				@Override
				public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
					log.info(String.format("onPartitionsRevoked [%s", __id));
				}

				// @Override
				// public void onPartitionsLost(Collection<TopicPartition> partitions) {
				// 	// TODO Auto-generated method stub
				// 	ConsumerRebalanceListener.super.onPartitionsLost(partitions);
				// }

				@Override
				public void onPartitionsAssigned(Collection<TopicPartition> partitions_) {
					log.info(String.format("onPartitionsAssigned [%s", __id));
					if (seekToBeginning) {
						__consumer.seekToBeginning(partitions_);
					}
				}

			});

			String statusId = String.format("core.%s", this.getClass().getSimpleName());
			ATSystemStatusManager.register(this, statusId);

			log.info(String.format("init %s[topic: %s, group: %s, id: %s]", this.getClass().getSimpleName(), __topic, __groupId, id_));
		} catch (Exception ex) {
			throw new ATException("init Error", ex);
		}
	}

	abstract protected void start();

	@Override
	public void stop() {
		__consumer.commitSync();
		__isRunning = false;
	}

	@Override
	public void run() {
		this.start();
	}

	@Override
	public String getId() {
		return this.__id;
	}

	protected void close() {
		__consumer.close();
	}

	protected ConsumerRecords<String, T> poll() {
		try {
			ConsumerRecords<String, T> records = __consumer.poll(__pollTimeout);
			return records;
		} catch (Throwable ex) {
			log.error(String.format("poll Error: %s", ex.getMessage()), ex);
			if (__topic.equals("user_session")) {
				log.info(__topic);
			}
			return null;
		}
	}

	protected void commit(boolean isSync_) {
		if (!__isAutoCommit) {
			if (isSync_) {
				__consumer.commitSync();
			} else {
				__consumer.commitAsync();
			}
		}
	}

	abstract protected Map<String, String> getSupplementalSystemStatus();

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("topic", __topic);
		status.put("group", __groupId);
		Map<String, String> supplementalStatus = getSupplementalSystemStatus();
		if (supplementalStatus != null) {
			status.putAll(supplementalStatus);
		}
		return status;
	}

	@Override
	public boolean isRunning() {
		return this.__isRunning;
	}

	static private String getGroupKey(String topic_) {
		String key = String.format("kafka.%s.group", topic_);
		return key;
	}

	static private String getGroupUniqueKey(String topic_) {
		String key = String.format("kafka.%s.group.unique", topic_);
		return key;
	}

	static private String getPollTimeoutKey(String topic_) {
		String key = String.format("kafka.%s.poll.timeout", topic_);
		return key;
	}

}
