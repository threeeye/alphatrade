package com.isoplane.at.kafka.test;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage;
import com.isoplane.at.kafka.IATKafkaRunnable;

import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializerConfig;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializerConfig;

public class ATKafkaSimpleMessageConsumer implements IATKafkaRunnable {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSimpleMessageConsumer.class);

	public static final String TOPIC = "test";

	private KafkaConsumer<String, SimpleMessage> _consumer;
	private String _id;
	private boolean _isRunning = false;
	private int _count = 0;

	public ATKafkaSimpleMessageConsumer(String id_, Configuration config_) {
		this._id = id_;
		this.init(id_, config_);
	}

	private void init(String id_, Configuration config_) {
		String bootsrap_servers = config_.getString("kafka.servers");
		String schema_registry = config_.getString("schema.registry");
		if (StringUtils.isBlank(bootsrap_servers) || StringUtils.isBlank(schema_registry)) {
			throw new ATException(String.format("Kafka Servers or Schema Registry missing"));
		}

		try {
			Properties props = new Properties();
			props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootsrap_servers);
			props.put(ConsumerConfig.CLIENT_ID_CONFIG, id_);
			props.put(ConsumerConfig.GROUP_ID_CONFIG, String.format("protobuf-consumer-group-%s", TOPIC));
			props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
			// props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
			props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaProtobufDeserializer.class);
			props.put(KafkaProtobufSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, String.format("http://%s", schema_registry));
			props.put(KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE, SimpleMessage.class);

			_consumer = new KafkaConsumer<>(props);
			_consumer.subscribe(Collections.singleton(TOPIC));

		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	public void start() {
		_isRunning = true;
		while (_isRunning) {
			ConsumerRecords<String, SimpleMessage> records = _consumer.poll(Duration.ofMillis(100));
			for (ConsumerRecord<String, SimpleMessage> record : records) {
				SimpleMessage sm = record.value();
				String msg = String.format("Received: %s - %s", sm.getDateTime(), sm.getContent());
				log.info(msg);
				_count++;
			}
			_consumer.commitAsync();
		}
		_consumer.close();// .close(Duration.ofMillis(1000));
	}

	public void stop() {
		_isRunning = false;
	}

	public int getCount() {
		return _count;
	}

	@Override
	public void run() {
		this.start();
	}

	@Override
	public String getId() {
		return this._id;
	}
}
