package com.isoplane.at.kafka.map;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public abstract class ATKafkaMapMessageProducer extends ATKafkaBaseProducer<MapMessage> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMapMessageProducer.class);

	private long _recordCount;

	public ATKafkaMapMessageProducer(String id_, Configuration config_, String topic_) {
		super(id_, config_, topic_, MapMessage.class);
	}

	public void send(MapMessage msg_) {
		if (msg_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}
		String type = msg_.getType();
		__producer.send(new ProducerRecord<String, MapMessage>(__topic, type, msg_));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%s]", type));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}
}
