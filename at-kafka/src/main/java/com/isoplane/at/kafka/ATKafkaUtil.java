package com.isoplane.at.kafka;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.CreateTopicsResult;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.errors.TopicExistsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Throwables;
import com.isoplane.at.commons.ATException;

public class ATKafkaUtil {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUtil.class);

	static public boolean createTopic(Configuration config_, String topic_, int partitionCount_, int replicationFactor_, boolean isCompact_) {
		String bootsrap_servers = config_.getString("kafka.servers");
		if (StringUtils.isBlank(bootsrap_servers)) {
			throw new ATException(String.format("Kafka Servers missing"));
		}

		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootsrap_servers);
		Map<String, String> topicConfig = new HashMap<>();
		if (isCompact_) {
			topicConfig.put("cleanup.policy", "compact");
		}
		NewTopic topic = new NewTopic(topic_, partitionCount_, (short) replicationFactor_).configs(topicConfig);
		AdminClient ac = AdminClient.create(props);
		CreateTopicsResult ctResult = ac.createTopics(Collections.singleton(topic));

		boolean result = false;
		for (Map.Entry<String, KafkaFuture<Void>> entry : ctResult.values().entrySet()) {
			try {
				entry.getValue().get();
				log.info("topic {} created", entry.getKey());
				result = true;
			} catch (InterruptedException | ExecutionException e) {
				if (Throwables.getRootCause(e) instanceof TopicExistsException) {
					log.info("topic {} exists", entry.getKey());
				}
				result = false;
			}
		}

		return result;
	}
}
