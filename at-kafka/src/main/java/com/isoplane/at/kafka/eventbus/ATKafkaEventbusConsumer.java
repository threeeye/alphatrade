package com.isoplane.at.kafka.eventbus;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.EventubusMessageProtos.EventbusMessage;
import com.isoplane.at.commons.service.IATProtoEventbusSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaEventbusConsumer extends ATKafkaBaseConsumer<EventbusMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaEventbusConsumer.class);

	public static final String TOPIC = "at_eventbus";

	private Set<IATProtoEventbusSubscriber> _subscribers;

	public ATKafkaEventbusConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, EventbusMessage.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, EventbusMessage> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, EventbusMessage> record : records) {
						EventbusMessage proto = record.value();
						String source = proto.getSource();
						String target = proto.getTarget();
						String type = proto.getType();
						Long ts = proto.getTimestamp();
						Map<String, String> data = proto.getDataMap();
						for (IATProtoEventbusSubscriber subscriber : this._subscribers) {
							subscriber.notifyEventbus(source, target, type, ts, data);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("System [%s]", proto.getSource());
							log.debug(msg);
						}
					}
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
			}
		}
		super.close();
	}

	public void subscribe(IATProtoEventbusSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}
}
