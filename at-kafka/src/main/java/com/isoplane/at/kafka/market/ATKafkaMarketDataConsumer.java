package com.isoplane.at.kafka.market;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaMarketDataConsumer extends ATKafkaBaseConsumer<MarketData> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataConsumer.class);

	public static final String TOPIC = "market_data";

	private Set<IATProtoMarketSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaMarketDataConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketData.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		try {
			while (__isRunning) {
				ConsumerRecords<String, MarketData> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					List<ATProtoMarketDataWrapper> protos = new ArrayList<>();
					for (ConsumerRecord<String, MarketData> record : records) {
						ATProtoMarketDataWrapper proto = new ATProtoMarketDataWrapper(record.value());
						protos.add(proto);
						if (log.isTraceEnabled()) {
							String msg = String.format("MarketData [%s]", proto.proto().getId());
							log.debug(msg);
						}
					}
					for (IATProtoMarketSubscriber subscriber : this._subscribers) {
						subscriber.notifyMarketData(protos);
					}
					_recordCount += records.count();
				}
				super.commit(false);
			}
		} catch (Exception ex) {
			log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
			if (__isRunning) {
				start();
			}
		}
		super.close();
	}

	public void subscribe(IATProtoMarketSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
