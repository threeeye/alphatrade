package com.isoplane.at.kafka;

import java.time.Duration;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializerConfig;

abstract public class ATKafkaBaseProducer<T> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaBaseProducer.class);

	protected KafkaProducer<String, T> __producer;
	protected String __topic;
	protected boolean __isTestProducer;
	protected String __source;
	protected Class<?> __keySerializerClass = StringSerializer.class;
	protected Class<?> __valueSerializerClass = KafkaProtobufSerializer.class;

	private boolean _isRunning;

	protected ATKafkaBaseProducer(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_,
			Class<?> keySerializerClass_,
			Class<?> valueSerializerClass_) {
		__keySerializerClass = keySerializerClass_;
		__valueSerializerClass = valueSerializerClass_;
		this.init(id_, config_, topic_, payloadClass_);
	}

	public ATKafkaBaseProducer(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_) {
		this(id_, config_, topic_, payloadClass_, StringSerializer.class, KafkaProtobufSerializer.class);
		// this.init(id_, config_, topic_, payloadClass_);
	}

	private void init(
			String id_,
			Configuration config_,
			String topic_,
			Class<T> payloadClass_) {

		String topicKey = String.format("kafka.%s.topic", topic_);
		String topicKeyOut = String.format("kafka.%s.topic.out", topic_);
		__isTestProducer = config_.getBoolean(getMarkTestKey(topic_), false);

		String bootsrap_servers = config_.getString("kafka.servers");
		String schema_registry = config_.getString("schema.registry");
		if (StringUtils.isBlank(bootsrap_servers) || StringUtils.isBlank(schema_registry)) {
			throw new ATException(String.format("Kafka Servers or Schema Registry missing"));
		}

		try {
			__topic = config_.getString(topicKeyOut, config_.getString(topicKey, topic_));
			// if("sse".equals(__topic)) {
			// 	__topic = "sse_test_x";
			// }

			ATKafkaUtil.createTopic(config_, __topic, 1, 1, true);

			Properties props = new Properties();
			props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootsrap_servers);
			props.put(ProducerConfig.CLIENT_ID_CONFIG, id_);
			props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, __keySerializerClass);
			props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, __valueSerializerClass);
			props.put(KafkaProtobufSerializerConfig.SCHEMA_REGISTRY_URL_CONFIG, String.format("http://%s", schema_registry));
		//	props.put(KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE, payloadClass_);

			__producer = new KafkaProducer<>(props);

			String statusId = String.format("core.%s", this.getClass().getSimpleName());
			ATSystemStatusManager.register(this, statusId);

			this._isRunning = true;
			log.info(String.format("init %s[id: %s, topic: %s]", this.getClass().getSimpleName(), id_, __topic));
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	public void setSource(String value_) {
		this.__source = value_;
	}

	public void stop() {
		this._isRunning = false;
		__producer.flush();
		__producer.close(Duration.ofMillis(1000));
	}

	abstract protected Map<String, String> getSupplementalSystemStatus();

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("topic", __topic);
		Map<String, String> supplementalStatus = getSupplementalSystemStatus();
		if (supplementalStatus != null) {
			status.putAll(supplementalStatus);
		}
		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	static private String getMarkTestKey(String topic_) {
		String key = String.format("kafka.%s.test.producer", topic_);
		return key;
	}

}
