package com.isoplane.at.kafka.test;

import java.time.Instant;
import java.util.Map;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage;
import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage.Builder;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaSimpleMessageProducer extends ATKafkaBaseProducer<SimpleMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSimpleMessageProducer.class);

	public static final String TOPIC = "test";

	public ATKafkaSimpleMessageProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SimpleMessage.class);
	}

	public void send(String txt_) {
		Builder mbld = SimpleMessage.newBuilder();
		mbld.setContent(txt_);
		mbld.setDateTime(String.format("%s", Instant.now().toString()));
		SimpleMessage sm = mbld.build();
		String topic = TOPIC;
		__producer.send(new ProducerRecord<String, SimpleMessage>(topic, txt_, sm));
		String msg = String.format("Sent    : %s - %s", sm.getDateTime(), sm.getContent());
		log.info(msg);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}

}
