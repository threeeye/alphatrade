package com.isoplane.at.kafka.fundamentals;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.kafka.map.ATKafkaMapMessageConsumer;

public class ATKafkaFundamentalsConsumer extends ATKafkaMapMessageConsumer {

	static final Logger log = LoggerFactory.getLogger(ATKafkaFundamentalsConsumer.class);

	public static final String TOPIC = "fundamentals";

	public ATKafkaFundamentalsConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC);
	}

}
