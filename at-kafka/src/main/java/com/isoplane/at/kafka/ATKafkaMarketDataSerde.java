package com.isoplane.at.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;

public class ATKafkaMarketDataSerde implements Serde<MarketData> {

	private Serializer<MarketData> serializer = new ProtoSerializer();
	private Deserializer<MarketData> deserializer = new ProtoDeserializer();

	@Override
	public Serializer<MarketData> serializer() {
		return this.serializer;
	}

	@Override
	public Deserializer<MarketData> deserializer() {
		return this.deserializer;
	}

	private class ProtoSerializer implements Serializer<MarketData> {
		@Override
		public byte[] serialize(String topic_, MarketData mkt_) {
			byte[] result = mkt_ == null ? null : mkt_.toByteArray();
			return result;
		}
	}

	private class ProtoDeserializer implements Deserializer<MarketData> {

		@Override
		public MarketData deserialize(String topic, byte[] data_) {
			try {
				MarketData result = data_ == null ? null : MarketData.parseFrom(data_);
				return result;
			} catch (Exception ex) {
				throw new ATException(ex);
			}
		}

	}

}