package com.isoplane.at.kafka.push;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.kafka.ATKafkaBaseProducer;

public class ATKafkaPushNotificationProducer extends ATKafkaBaseProducer<PushNotification> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaPushNotificationProducer.class);

	public static final String TOPIC = "push";

	private long _recordCount;

	public ATKafkaPushNotificationProducer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, PushNotification.class);

		//ATSystemStatusManager.register(this);
	}

	public void send(PushNotification msg_) {
		if (msg_ == null) {
			log.error(String.format("Invalid parameters"));
			return;
		}

		String id = msg_.getTag();
		__producer.send(new ProducerRecord<String, PushNotification>(__topic, id, msg_));
		if (log.isDebugEnabled()) {
			log.debug(String.format("Sent [%s]", id));
		}
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}
}
