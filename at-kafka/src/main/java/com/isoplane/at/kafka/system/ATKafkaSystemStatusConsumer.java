package com.isoplane.at.kafka.system;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.SystemStatusProtos.SystemStatus;
import com.isoplane.at.commons.service.IATProtoSystemSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaSystemStatusConsumer extends ATKafkaBaseConsumer<SystemStatus> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSystemStatusConsumer.class);

	public static final String TOPIC = "system_status";

	private Set<IATProtoSystemSubscriber> _subscribers;

	public ATKafkaSystemStatusConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SystemStatus.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, SystemStatus> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, SystemStatus> record : records) {
						SystemStatus proto = record.value();
						String source = proto.getSource();
						Map<String, String> status = proto.getDataMap();
						for (IATProtoSystemSubscriber subscriber : this._subscribers) {
							subscriber.notifySystemStatus(source, status);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("System [%s]", proto.getSource());
							log.debug(msg);
						}
					}
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
			}
		}
		super.close();
	}

	public void subscribe(IATProtoSystemSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}
}
