package com.isoplane.at.kafka;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

import com.google.protobuf.Message;
import com.google.protobuf.Parser;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;

// NOTE: Doesn't seem to work
public class ATKafkaGenericProtobufSerde<T extends Message> implements Serde<T> {

	private Serializer<T> serializer;
	private Deserializer<T> deserializer;

	private Class<? extends Message> clazz;

	public ATKafkaGenericProtobufSerde(Class<T> class_) {
		this.clazz = class_;
		this.serializer = new ProtoSerializer();
		this.deserializer = new ProtoDeserializer();
	}

	@Override
	public Serializer<T> serializer() {
		return this.serializer;
	}

	@Override
	public Deserializer<T> deserializer() {
		return this.deserializer;
	}

	private class ProtoSerializer implements Serializer<T> {
		@Override
		public byte[] serialize(String topic_, T proto_) {
			byte[] result = proto_ == null ? null : proto_.toByteArray();
			return result;
		}
	}

	private class ProtoDeserializer implements Deserializer<T> {

		private Parser<T> parser;

		@SuppressWarnings("unchecked")
		private ProtoDeserializer() {
			try {
				// Parser<MarketData> p = MarketData.parser();
				this.parser = (Parser<T>) clazz.getMethod("parser").invoke(null);
			} catch (Exception ex) {
				throw new ATException(ex);
			}
		}

		@Override
		public T deserialize(String topic, byte[] data_) {
			try {
				PushNotification.newBuilder().mergeFrom(data_).build();
				T result = this.parser.parseFrom(data_);
				return result;
			} catch (Exception ex) {
				throw new ATException(ex);
			}
		}

	}

}