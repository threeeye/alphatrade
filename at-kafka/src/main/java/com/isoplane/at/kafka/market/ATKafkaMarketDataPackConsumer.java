package com.isoplane.at.kafka.market;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaMarketDataPackConsumer extends ATKafkaBaseConsumer<MarketDataPack> implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataPackConsumer.class);

	public static final String TOPIC = "market_data_pack";

	private Set<IATProtoMarketSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaMarketDataPackConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketDataPack.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		try {
			while (__isRunning) {
				ConsumerRecords<String, MarketDataPack> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, MarketDataPack> record : records) {
						ATProtoMarketDataPackWrapper proto = new ATProtoMarketDataPackWrapper(record.value());
						for (IATProtoMarketSubscriber subscriber : this._subscribers) {
							subscriber.notifyMarketDataPack(proto);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("MarketData [%s]", proto.proto().getType());
							log.debug(msg);
						}
					}
					_recordCount += records.count();
				}
				super.commit(false);
			}
		} catch (Exception ex) {
			log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
			if (__isRunning) {
				start();
			}
		}
		super.close();
	}

	public void subscribe(IATProtoMarketSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
