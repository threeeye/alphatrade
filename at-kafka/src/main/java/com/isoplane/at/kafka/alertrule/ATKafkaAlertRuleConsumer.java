package com.isoplane.at.kafka.alertrule;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoAlertRuleWrapper;
import com.isoplane.at.commons.model.protobuf.AlertRuleProtos.AlertRule;
import com.isoplane.at.commons.service.IATProtoAlertRuleSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaAlertRuleConsumer extends ATKafkaBaseConsumer<AlertRule> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaAlertRuleConsumer.class);

	public static final String TOPIC = "alert_rule";

	private Set<IATProtoAlertRuleSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaAlertRuleConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, AlertRule.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, AlertRule> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, AlertRule> record : records) {
						AlertRule proto = record.value();
						ATProtoAlertRuleWrapper wrapper = new ATProtoAlertRuleWrapper(proto);
						for (IATProtoAlertRuleSubscriber subscriber : this._subscribers) {
							subscriber.notifyAlertRule(wrapper);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("AlertRule [%s]", proto.getType());
							log.debug(msg);
						}
					}
					_recordCount += records.count();
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (__isRunning) {
					start();
				}
			}
		}
		super.close();
	}

	public void subscribe(IATProtoAlertRuleSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
