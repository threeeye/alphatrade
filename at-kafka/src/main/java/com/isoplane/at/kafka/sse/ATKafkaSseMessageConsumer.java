package com.isoplane.at.kafka.sse;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.commons.service.IATProtoSseSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaSseMessageConsumer extends ATKafkaBaseConsumer<SseMessage> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSseMessageConsumer.class);

	public static final String TOPIC = "sse";

	private Set<IATProtoSseSubscriber> _subscribers;
	private long _recordCount;

	public ATKafkaSseMessageConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, SseMessage.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		__isRunning = true;
		while (__isRunning) {
			try {
				ConsumerRecords<String, SseMessage> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, SseMessage> record : records) {
						SseMessage proto = record.value();
						for (IATProtoSseSubscriber subscriber : this._subscribers) {
							subscriber.notifySseMessage(proto);
						}
						if (log.isTraceEnabled()) {
							String msg = String.format("SSE [%s]", proto.getType());
							log.debug(msg);
						}
					}
					_recordCount += records.count();
				}
				super.commit(false);
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (__isRunning) {
					start();
				}
			}
		}
		super.close();
	}

	public void subscribe(IATProtoSseSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		Map<String, String> status = new TreeMap<>();
		status.put("count.records", String.format("%d", _recordCount));
		return status;
	}

}
