package com.isoplane.at.kafka.market;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration2.Configuration;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketStatusProtos.MarketStatus;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.kafka.ATKafkaBaseConsumer;

public class ATKafkaMarketStatusConsumer extends ATKafkaBaseConsumer<MarketStatus> {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketStatusConsumer.class);

	public static final String TOPIC = "market_status";

	private Set<IATProtoMarketSubscriber> _subscribers;

	public ATKafkaMarketStatusConsumer(String id_, Configuration config_) {
		super(id_, config_, TOPIC, MarketStatus.class);
		this._subscribers = new HashSet<>();
	}

	@Override
	protected void start() {
		try {
			__isRunning = true;
			while (__isRunning) {
				ConsumerRecords<String, MarketStatus> records = super.poll();
				if (records != null && records.count() > 0 && _subscribers.size() > 0) {
					for (ConsumerRecord<String, MarketStatus> record : records) {
						ATProtoMarketStatusWrapper proto = new ATProtoMarketStatusWrapper(record.value());
						if (proto.proto().hasIsTest()) {
							log.info(String.format("MarketStatus Test [%s]", proto.proto().getStatus()));
						} else {
							for (IATProtoMarketSubscriber subscriber : this._subscribers) {
								subscriber.notifyMarketStatus(proto);
							}
							if (log.isTraceEnabled()) {
								String msg = String.format("MarketStatus [%s]", proto.proto().getStatus());
								log.debug(msg);
							}
						}
					}
				}
				super.commit(false);
			}
			super.close();
		} catch (Exception ex) {
			log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
		}
	}

	public void subscribe(IATProtoMarketSubscriber sub_) {
		this._subscribers.add(sub_);
	}

	@Override
	protected Map<String, String> getSupplementalSystemStatus() {
		return null;
	}
}
