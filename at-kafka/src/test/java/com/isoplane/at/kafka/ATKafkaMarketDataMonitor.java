package com.isoplane.at.kafka;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.TextFormat;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusConsumer;

public class ATKafkaMarketDataMonitor {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMarketDataMonitor.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("market_monitor",
					ATKafkaMarketDataConsumer.class,
					ATKafkaMarketDataPackConsumer.class,
					ATKafkaMarketStatusConsumer.class);
			ATKafkaMarketDataConsumer marketDataConsumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
			ATKafkaMarketDataPackConsumer marketDataPackConsumer = ATKafkaRegistry.get(ATKafkaMarketDataPackConsumer.class);
			ATKafkaMarketStatusConsumer marketStatusConsumer = ATKafkaRegistry.get(ATKafkaMarketStatusConsumer.class);
			ATKafkaMarketDataTestSubscriber monitor = new ATKafkaMarketDataTestSubscriber();
			marketDataConsumer.subscribe(monitor);
			marketDataPackConsumer.subscribe(monitor);
			marketStatusConsumer.subscribe(monitor);
			ATKafkaRegistry.start();

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaMarketDataMonitor.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	static class ATKafkaMarketDataTestSubscriber implements IATProtoMarketSubscriber {

		@Override
		public void notifyMarketData(Collection<ATProtoMarketDataWrapper> data_) {
			if (!log.isTraceEnabled())
				return;
			int count = data_ == null ? 0 : data_.size();
			if (count == 1) {
				ATProtoMarketDataWrapper wrapper = data_.iterator().next();
				MarketData proto = wrapper.proto();
				String dataStr = TextFormat.shortDebugString(proto);
				log.info(String.format("Monitor: notifyMarketData[%s/%d]: %s", proto.getSrc(), count, dataStr));
			} else {
				log.info(String.format("Monitor: notifyMarketData[%d]", count));
			}
		}

		@Override
		public void notifyMarketDataPack(ATProtoMarketDataPackWrapper data_) {
			if (!log.isTraceEnabled())
				return;
			MarketDataPack proto = data_.proto();
			// if(proto.getSymbolsList().contains("SPY")) {
			// Collection<String> ids =proto.getMarketDataList().stream().map(p->p.getId()).collect(Collectors.toCollection(TreeSet::new));
			// log.info("SPY");
			// }
			String subType = proto.hasSubType() ? proto.getSubType() : " ";
			log.info(String.format("Monitor: notifyMarketDataPack[%s/%s/%s/%d/%d/%s/%d]",
					proto.getSource(),
					proto.getType(),
					subType,
					proto.hasSeq() ? proto.getSeq() : 0,
					proto.hasSeqLength() ? proto.getSeqLength() : 0,
					proto.getSymbolsList(),
					proto.getMarketDataCount()));
		}

		@Override
		public void notifyMarketStatus(ATProtoMarketStatusWrapper data_) {
			String status = data_ != null && data_.proto() != null ? data_.proto().getStatus() : null;
			log.info(String.format("Monitor: notifyMarketStatus[%s]", status));
		}

	}

}
