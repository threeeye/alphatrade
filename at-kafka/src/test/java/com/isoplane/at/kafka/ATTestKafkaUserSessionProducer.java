package com.isoplane.at.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.user.ATKafkaUserSessionProducer;

public class ATTestKafkaUserSessionProducer {

	static final Logger log = LoggerFactory.getLogger(ATTestKafkaUserSessionProducer.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("session_test", ATKafkaUserSessionProducer.class);
			ATKafkaUserSessionProducer producer = ATKafkaRegistry.get(ATKafkaUserSessionProducer.class);

			ATKafkaUserSessionProducerTestDriver driver = new ATKafkaUserSessionProducerTestDriver(producer);
			ATExecutors.scheduleAtFixedRate(ATTestKafkaUserSessionProducer.class.getSimpleName(), driver, 1000, 1000);

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATTestKafkaUserSessionProducer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private static class ATKafkaUserSessionProducerTestDriver implements Runnable {

		ATKafkaUserSessionProducer _producer;

		public ATKafkaUserSessionProducerTestDriver(ATKafkaUserSessionProducer producer_) {
			_producer = producer_;
		}

		@Override
		public void run() {
			ATSession session = new ATSession();
			// session.setExpiration(-1);
			session.setToken(String.format("test_session_%d", System.currentTimeMillis()));
			session.setUserId("test_user");
			_producer.send(EATChangeOperation.CREATE, session);
		}

	}

}
