package com.isoplane.at.kafka;

import java.util.Date;

import com.google.protobuf.Any;
import com.google.protobuf.ProtocolStringList;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataStatus;
import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage;
import com.isoplane.at.commons.model.protobuf.SimpleMessageProtos.SimpleMessage.Builder;
import com.isoplane.at.commons.model.protobuf.SseMessageProtos.SseMessage;
import com.isoplane.at.commons.service.IATProtoSseSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageConsumer;
import com.isoplane.at.kafka.sse.ATKafkaSseMessageProducer;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKafkaSseMessageMonitor {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSseMessageMonitor.class);

	public static void main(String[] args_) {
		try {
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);

			ATExecutors.init();

			ATKafkaRegistry.register("sse_monitor", ATKafkaSseMessageConsumer.class);
			ATKafkaSseMessageConsumer consumer = ATKafkaRegistry.get(ATKafkaSseMessageConsumer.class);
			ATKafkaSseMessageTestSubscriber monitor = new ATKafkaSseMessageTestSubscriber();
			consumer.subscribe(monitor);
			ATKafkaRegistry.start();

			var isBouncing = config.getBoolean("monitor.sse.bounce", false);
			if (isBouncing) {
				ATKafkaRegistry.register("sse_test", ATKafkaSseMessageProducer.class);
				ATKafkaSseMessageProducer producer = ATKafkaRegistry.get(ATKafkaSseMessageProducer.class);
				String producerTopic = String.format("%s%s", consumer.__topic,
						config.getString("monitor.sse.topic.postfix"));
				producer.__topic = producerTopic;
				ATKafkaSseProducerTestDriver driver = new ATKafkaSseProducerTestDriver(producer);
				consumer.subscribe(driver);
			}

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaSseMessageMonitor.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	static class ATKafkaSseMessageTestSubscriber implements IATProtoSseSubscriber {

		@Override
		public void notifySseMessage(SseMessage msg_) {
			try {
				String type = msg_.getType();

				ProtocolStringList targets = msg_.getTargetsList();
				if (targets.isEmpty()) {
					log.info(String.format("Monitor: notifySseMessage Empty target [%s]", type));
					return;
				}

				switch (type) {
					case "mkt":
						MarketData protoMkt = msg_.getData().unpack(MarketData.class);
						log.info(String.format("Monitor: notifySseMessage[%s]: %s", type, protoMkt.getId()));
						break;
					case "mktPack":
						MarketDataPack protoMktPack = msg_.getData().unpack(MarketDataPack.class);
						log.info(String.format("Monitor: notifySseMessage[%s]: %s", type,
								protoMktPack.getSymbolsList()));
						break;
					case ATProtoMarketDataWrapper.TYPE_MARKET_AVAILABLE:
						MarketDataStatus protoMktStatus = msg_.getData().unpack(MarketDataStatus.class);
						log.info(String.format("Monitor: notifySseMessage[%s]: %s", type, protoMktStatus.getType()));
						break;
					default:
						log.error(String.format("notifySseMessage Unknown type [%s]", type));
						break;
				}
			} catch (Exception ex) {
				log.error(String.format("notifySseMessage Error"), ex);
			}
		}

	}

	static class ATKafkaSseProducerTestDriver implements IATProtoSseSubscriber {

		ATKafkaSseMessageProducer _producer;

		public ATKafkaSseProducerTestDriver(ATKafkaSseMessageProducer producer_) {
			_producer = producer_;
		}

		@Override
		public void notifySseMessage(SseMessage msg_) {
			try {
				Builder msgBuilder = SimpleMessage.newBuilder();
				msgBuilder.setContent("test");
				msgBuilder.setDateTime(String.format("%s", new Date()));
				msgBuilder.setIsTest(true);
				SimpleMessage msgData = msgBuilder.build();

				Any data = /*Any.pack(msgData);/*/ msg_.getData();
				var type = msg_.getType();
				var targets = msg_.getTargetsList();
				SseMessage.Builder builder = SseMessage.newBuilder();
				builder.setData(data);
				builder.setType(type);
				for (String target : targets) {
					builder.addTargets(target);
				}
				SseMessage msg = builder.build();
				_producer.send(msg);
				log.info(String.format("Bounce: %s", msg_.getType()));
			} catch (Exception ex) {
				log.error("Error", ex);
			}
		}

	}

}
