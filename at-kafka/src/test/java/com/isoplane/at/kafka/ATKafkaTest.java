package com.isoplane.at.kafka;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collection;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.kafka.market.ATKafkaMarketDataConsumer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.kafka.test.ATKafkaSimpleMessageConsumer;
import com.isoplane.at.kafka.test.ATKafkaSimpleMessageProducer;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATKafkaTest {

	static final Logger log = LoggerFactory.getLogger(ATKafkaTest.class);

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, false);
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() throws Exception {
	}

	@AfterEach
	public void cleanup() throws Exception {
	}

	// @Disabled
	@Test
	public void testSimpleMessage() throws InterruptedException {
		Configuration config = ATConfigUtil.config();
		ATKafkaSimpleMessageProducer producer = new ATKafkaSimpleMessageProducer("test_producer", config);
		ATKafkaSimpleMessageConsumer consumer = new ATKafkaSimpleMessageConsumer("test_consumer", config);
		new Thread(consumer).start();

		final int COUNT = 30;
		for (int i = 0; i < COUNT; i++) {
			producer.send(String.format("Test %d", i));
			Thread.sleep(10);
		}

		Thread.sleep(1000);
		ATKafkaRegistry.stop();
		// producer.stop();
		// consumer.stop();
		int count = consumer.getCount();
		assertEquals(COUNT, count);
	}

	//	@Disabled
	@Test
	public void testMarketData() throws InterruptedException {

		var topicKey = String.format("kafka.%s.topic", ATKafkaMarketDataProducer.TOPIC);
		String topicKeyOut = String.format("kafka.%s.topic.out", ATKafkaMarketDataProducer.TOPIC);
		ATConfigUtil.config().setProperty(topicKey, "test_topic_mkt");
		ATConfigUtil.config().setProperty(topicKeyOut, "test_topic_mkt");

		ATKafkaRegistry.register("test", ATKafkaMarketDataProducer.class, ATKafkaMarketDataConsumer.class);
		ATKafkaRegistry.start();
		ATKafkaMarketDataProducer producer = ATKafkaRegistry.get(ATKafkaMarketDataProducer.class);
		ATKafkaMarketDataConsumer consumer = ATKafkaRegistry.get(ATKafkaMarketDataConsumer.class);
		consumer.subscribe(new IATProtoMarketSubscriber() {

			int count = 0;

			@Override
			public void notifyMarketData(Collection<ATProtoMarketDataWrapper> data_) {
				count++;
				var data = data_.iterator().next().value();
				log.info(String.format("notifyMarketData [%d/%s]", count, data));
			}

			@Override
			public void notifyMarketDataPack(ATProtoMarketDataPackWrapper data) {
				log.info(String.format("notifyMarketDataPack [%s]", data));
			}

			@Override
			public void notifyMarketStatus(ATProtoMarketStatusWrapper data) {
				log.info(String.format("notifyMarketStatus [%s]", data));
			}

		});
		final int COUNT = 30;
		for (int i = 0; i < COUNT; i++) {
			ATMarketData mkt = new ATMarketData();
			mkt.setOccId(String.format("MKT_%d", i));
			producer.send(mkt);
			Thread.sleep(10);
		}
		Thread.sleep(1000);
		//	ATKafkaRegistry.stop();
		producer.stop();
		consumer.stop();
		Thread.sleep(1000);
	}

	public static class ATKafkaMarketDataTestProducer extends ATKafkaMarketDataProducer {

		public ATKafkaMarketDataTestProducer(String id_, Configuration config_) {
			//			super(id_, config_, TOPIC, MarketData.class);
			super(id_, config_);
		}

	}

}
