package com.isoplane.at.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;

public class ATKafkaUserSessionMonitor {

	static final Logger log = LoggerFactory.getLogger(ATKafkaUserSessionMonitor.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("session_monitor", ATKafkaUserSessionConsumer.class);
			ATKafkaUserSessionConsumer consumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
			ATKafkaUserSessionTestSubscriber monitor = new ATKafkaUserSessionTestSubscriber();
			consumer.subscribe(monitor);
			ATKafkaRegistry.start();

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaUserSessionMonitor.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	static class ATKafkaUserSessionTestSubscriber implements IATProtoUserSubscriber {

		private Gson _gson = new Gson();

		@Override
		public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
			log.info(String.format("Monitor: notifyUserSession[%s/%s]: %s", action_, session_.getUserId(), _gson.toJson(session_)));
		}

	}

}
