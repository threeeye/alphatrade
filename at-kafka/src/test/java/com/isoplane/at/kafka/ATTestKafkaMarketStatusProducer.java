package com.isoplane.at.kafka;

import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketStatusProtos.MarketStatus;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusProducer;

public class ATTestKafkaMarketStatusProducer {

	static final Logger log = LoggerFactory.getLogger(ATTestKafkaMarketStatusProducer.class);

	public static final String TOPIC = "user_session";

	static public void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATKafkaRegistry.register("mkt_status_test", ATKafkaMarketStatusProducer.class);
			ATKafkaMarketStatusProducer mktStatusProducer = ATKafkaRegistry.get(ATKafkaMarketStatusProducer.class);

			ATTestKafkaMarketStatusDriver mktStatusDriver = new ATTestKafkaMarketStatusDriver(mktStatusProducer);
			ATExecutors.scheduleAtFixedRate(ATTestKafkaMarketStatusProducer.class.getSimpleName(), mktStatusDriver, 1000, 1000);

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATTestKafkaMarketStatusProducer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private static class ATTestKafkaMarketStatusDriver implements Runnable {

		private ATKafkaMarketStatusProducer _producer;
		private Random _rnd;

		public ATTestKafkaMarketStatusDriver(ATKafkaMarketStatusProducer producer_) {
			_producer = producer_;
			_rnd = new Random(System.currentTimeMillis());
		}

		@Override
		public void run() {
			EATMarketStatus[] stati = EATMarketStatus.values();
			int idx = _rnd.nextInt(stati.length);
			EATMarketStatus status = stati[idx];
			MarketStatus proto = ATProtoMarketStatusWrapper.toMarketStatus(status, new Date(), true);
			ATProtoMarketStatusWrapper wrapper = new ATProtoMarketStatusWrapper(proto);
			_producer.send(wrapper);
		}
	}
}
