package com.isoplane.at.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.protobuf.TextFormat;
import com.isoplane.at.commons.model.protobuf.PushNotificationProtos.PushNotification;
import com.isoplane.at.commons.service.IATProtoPushNotificationSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.push.ATKafkaPushNotificationConsumer;

public class ATKafkaPushNotificationMonitor {

	static final Logger log = LoggerFactory.getLogger(ATKafkaPushNotificationMonitor.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("push_monitor", ATKafkaPushNotificationConsumer.class);
			ATKafkaPushNotificationConsumer consumer = ATKafkaRegistry.get(ATKafkaPushNotificationConsumer.class);
			ATKafkaPushNotificationTestSubscriber monitor = new ATKafkaPushNotificationTestSubscriber();
			consumer.subscribe(monitor);
			ATKafkaRegistry.start();

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaPushNotificationMonitor.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	static class ATKafkaPushNotificationTestSubscriber implements IATProtoPushNotificationSubscriber {

		@Override
		public void notifyPushNotification(PushNotification msg_) {
			try {
				String dataStr = TextFormat.shortDebugString(msg_);
				log.info(String.format("Monitor: notifyPushNotification[%s/%s]: %s", msg_.getType(), msg_.getTag(), dataStr));
			} catch (Exception ex) {
				log.error(String.format("notifyPushNotification Error"), ex);
			}
		}

	}

}
