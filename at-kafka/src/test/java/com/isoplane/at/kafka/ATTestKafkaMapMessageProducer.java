package com.isoplane.at.kafka;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMapMessageWrapper;
import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.map.ATKafkaMapMessageProducer;

public class ATTestKafkaMapMessageProducer {

	static final Logger log = LoggerFactory.getLogger(ATTestKafkaMapMessageProducer.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("map_msg_test", ATKafkaMapMessageProducer.class);
			ATKafkaMapMessageProducer producer = ATKafkaRegistry.get(ATKafkaMapMessageProducer.class);

			ATKafkMapMessageProducerTestDriver driver = new ATKafkMapMessageProducerTestDriver(producer);
			ATExecutors.scheduleAtFixedRate(ATTestKafkaMapMessageProducer.class.getSimpleName(), driver, 1000, 1000);

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATTestKafkaMapMessageProducer.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private static class ATKafkMapMessageProducerTestDriver implements Runnable {

		ATKafkaMapMessageProducer _producer;

		private int count = 0;

		public ATKafkMapMessageProducerTestDriver(ATKafkaMapMessageProducer producer_) {
			_producer = producer_;
		}

		@Override
		public void run() {
			Map<String, Object> map = new HashMap<>();
			count++;
			map.put("double_key", 1.1 * count);
			map.put("int_key", 10 * count);
			map.put("long_key", 100L * count);
			map.put("string_key", String.format("hello world! %d", count));

			MapMessage mapMsg = ATProtoMapMessageWrapper.toMapMessage("test_type", String.format("test_id_%d", count), "test_source", "test_target",
					System.currentTimeMillis(), map);
			_producer.send(mapMsg);
		}

	}

}
