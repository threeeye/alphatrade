package com.isoplane.at.kafka;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.Future;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.util.ATConfigUtil;

public class ATKafkaSimpleTest {

	static final Logger log = LoggerFactory.getLogger(ATKafkaSimpleTest.class);

	@BeforeAll
	public static void setup() throws ConfigurationException {
		String configPath = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		ATConfigUtil.init(configPath, propertiesPath, false);
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() throws Exception {
	}

	@AfterEach
	public void cleanup() throws Exception {
	}

	// @Disabled
	@Test
	public void testSimpleMessage() throws InterruptedException {
		Configuration config = ATConfigUtil.config();
		final String topic = "AT_test_topic";
		ATKafkaStringDataProducer producer = new ATKafkaStringDataProducer("test_producer", config, topic);
		ATKafkaStringDataConsumer consumer = new ATKafkaStringDataConsumer("test_consumer", config, topic);
		new Thread(consumer).start();

		final int COUNT = 30;
		for (int i = 0; i < COUNT; i++) {
			producer.send(String.format("Test %d", i));
			Thread.sleep(10);
		}

		Thread.sleep(1000);
		producer.stop();
		consumer.stop();
		int count = consumer.getCount();
		assertEquals(COUNT, count);
	}

	public static class ATKafkaStringDataProducer extends ATKafkaBaseProducer<String> {

		public ATKafkaStringDataProducer(String id_, Configuration config_, String topic_) {
			super(id_, config_, topic_, String.class, StringSerializer.class, StringSerializer.class);
		}

		public void send(String txt_) {
			Future<RecordMetadata> sf = __producer.send(new ProducerRecord<String, String>(super.__topic, txt_));
			RecordMetadata sd;
			try {
				sd = sf.get();
				long ts = sd.hasTimestamp() ? sd.timestamp() : -1;
				String msg = String.format("Sent    : %d - %d", ts, sd.hashCode());
				log.info(msg);
			} catch (Exception ex) {
				log.error(String.format("Send error"), ex);
			}
		}

		@Override
		protected Map<String, String> getSupplementalSystemStatus() {
			return null;
		}

	}

	public static class ATKafkaStringDataConsumer extends ATKafkaBaseConsumer<String> {

		private int _count = 0;

		public ATKafkaStringDataConsumer(String id_, Configuration config_, String topic_) {
			super(id_, config_, topic_, String.class, StringDeserializer.class, StringDeserializer.class);
		}

		public void start() {
			__isRunning = true;
			while (__isRunning) {
				ConsumerRecords<String, String> records = __consumer.poll(Duration.ofMillis(100));
				for (ConsumerRecord<String, String> record : records) {
					String sm = record.value();
					String msg = String.format("Received: %d - %s", record.timestamp(), sm);
					log.info(msg);
					_count++;
				}
				__consumer.commitAsync();
				// __consumer.close();
			}
		}

		@Override
		public void stop() {
			__isRunning = false;
		}

		public int getCount() {
			return _count;
		}

		@Override
		protected Map<String, String> getSupplementalSystemStatus() {
			return null;
		}
	}

}
