package com.isoplane.at.kafka;

import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.protobuf.ATProtoMapMessageWrapper;
import com.isoplane.at.commons.model.protobuf.MapMessageProtos.MapMessage;
import com.isoplane.at.commons.service.IATProtoMapMessageSubscriber;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.map.ATKafkaMapMessageConsumer;

public class ATKafkaMapMessageMonitor {

	static final Logger log = LoggerFactory.getLogger(ATKafkaMapMessageMonitor.class);

	public static void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();

			ATKafkaRegistry.register("map_msg_monitor", ATKafkaMapMessageConsumer.class);
			ATKafkaMapMessageConsumer consumer = ATKafkaRegistry.get(ATKafkaMapMessageConsumer.class);
			ATKafkaMapMessageTestSubscriber monitor = new ATKafkaMapMessageTestSubscriber();
			consumer.subscribe(monitor);
			ATKafkaRegistry.start();

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaMapMessageMonitor.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	static class ATKafkaMapMessageTestSubscriber implements IATProtoMapMessageSubscriber {

		@Override
		public void notifyMapMessage(MapMessage msg_) {
			try {
				String type = msg_.getType();
				String target = msg_.getTarget();
				String id = msg_.getId();
				String source = msg_.getSource();
				Long ts = msg_.getTimestamp();
				Date d = new Date(ts);
				String dstr = ATFormats.DATE_TIME_mid.get().format(d);
				// Map<String, Double> doubleMap = msg_.getDoubleMapMap();
				// Map<String, Integer> intMap = msg_.getIntMapMap();
				// Map<String, Long> longMap = msg_.getLongMapMap();
				// Map<String, String> stringMap = msg_.getStringMapMap();
				// log.info(String.format("Monitor: notifyMapMessage[%s: %s/%s]: %s -> %s\ndoubleMap: %s\nintMap : %s\nlongMap : %s\nstringMap: %s",
				// dstr, type, id, source, target, doubleMap, intMap, longMap, stringMap));
				Map<String, Object> map = ATProtoMapMessageWrapper.toMap(msg_, new TreeMap<>());
				log.info(String.format("Monitor: notifyMapMessage[%s: %s/%s]: %s -> %s\nmap: %s",
						dstr, type, id, source, target, map));
			} catch (Exception ex) {
				log.error(String.format("notifyMapMessage Error"), ex);
			}
		}

	}

}
