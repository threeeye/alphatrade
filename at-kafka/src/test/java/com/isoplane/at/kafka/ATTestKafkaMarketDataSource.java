package com.isoplane.at.kafka;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATMarketData;
import com.isoplane.at.commons.model.IATMarketData;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusProducer;

public class ATTestKafkaMarketDataSource {

	static final Logger log = LoggerFactory.getLogger(ATTestKafkaMarketDataSource.class);

	public static final String TOPIC = "user_session";

	static public void main(String[] args_) {
		try {
			String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			ATConfigUtil.init(configPath, true);

			ATExecutors.init();
			ATKafkaRegistry.register("mkt_data_test",
					ATKafkaMarketDataProducer.class,
					ATKafkaMarketDataPackProducer.class,
					ATKafkaMarketStatusProducer.class);
			ATKafkaMarketDataProducer mktDataProducer = ATKafkaRegistry.get(ATKafkaMarketDataProducer.class);
			ATKafkaMarketDataPackProducer mktDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);

			ATKafkaMarketDataProducerTestDriver mktDataDriver = new ATKafkaMarketDataProducerTestDriver(mktDataProducer, mktDataPackProducer);
			ATExecutors.scheduleAtFixedRate(ATTestKafkaUserSessionProducer.class.getSimpleName(), mktDataDriver, 3000, 3000);

			ATSysUtils.waitForEnter();
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATTestKafkaMarketDataSource.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	private static class ATKafkaMarketDataProducerTestDriver implements Runnable {

		private ATKafkaMarketDataProducer _mktDataProducer;
		private ATKafkaMarketDataPackProducer _mktDataPackProducer;
		private Random _rnd;
		private boolean _flip;

		private String[] _occIds;
		private double[] _pxClose;
		private double[] _pxHi;
		private double[] _pxLast;
		private double[] _pxLo;

		public ATKafkaMarketDataProducerTestDriver(ATKafkaMarketDataProducer mktDataProducer_, ATKafkaMarketDataPackProducer mktDataPackProducer_) {
			_mktDataProducer = mktDataProducer_;
			_mktDataPackProducer = mktDataPackProducer_;

			_rnd = new Random(System.currentTimeMillis());

			_occIds = new String[] { "_TEST", "_TEST 210319C00100000" };
			_pxClose = new double[] { _rnd.nextDouble() * 100 + 10, _rnd.nextDouble() * 10 + 1 };
			_pxLast = new double[] { _pxClose[0], _pxClose[1] };
			_pxHi = new double[] { _pxClose[0], _pxClose[1] };
			_pxLo = new double[] { _pxClose[0], _pxClose[1] };
		}

		@Override
		public void run() {
			_flip = !_flip;

			List<IATMarketData> mktList = new ArrayList<>();
			for (int i = 0; i < _occIds.length; i++) {
				ATMarketData mkt = new ATMarketData();
				mkt.setOccId(_occIds[i]);
				mkt.setDailyVol(_rnd.nextLong());
				mkt.setOpenInterest(_rnd.nextLong());

				double pxDelta = (_rnd.nextBoolean() ? 1 : -1) * _rnd.nextGaussian() * _pxLast[i] / 10;
				_pxLast[i] += pxDelta;
				if (_pxLast[i] < 1 || _pxLast[i] > 1000) {
					_pxLast[i] = _rnd.nextDouble() * 100 + 10;
				}
				if (_pxLast[i] > _pxHi[i]) {
					_pxHi[i] = _pxLast[i];
				}
				if (_pxLast[i] < _pxLo[i]) {
					_pxLo[i] = _pxLast[i];
				}
				mkt.setPxAsk(_pxLast[i] * 1.02);
				mkt.setPxBid(_pxLast[i] + 0.98);
				mkt.setPxChange(100 * _pxLast[i] / _pxClose[i]);
				mkt.setPxClose(_pxClose[i]);
				mkt.setPxClosePrev(_pxClose[i]);
				mkt.setPxHigh(_pxHi[i]);
				mkt.setPxLast(_pxLast[i]);
				mkt.setPxLow(_pxLo[i]);
				mkt.setSzAsk(_rnd.nextInt(101));
				mkt.setSzBid(_rnd.nextInt(101));
				mkt.setSzLast(_rnd.nextInt(100) + 1);
				mkt.setTsAsk(System.currentTimeMillis());
				mkt.setTsBid(System.currentTimeMillis());
				mkt.setTsLast(System.currentTimeMillis());

				mktList.add(mkt);

				if (_flip) {
					log.info(String.format("Sending [%s]", mkt.getOccId()));
					_mktDataProducer.send(mkt);
				}
			}
			if (!_flip) {
				log.info(String.format("Sending pack: %s", mktList.stream().map(d -> d.getAtId()).collect(Collectors.toList())));
				_mktDataPackProducer.send(ATProtoMarketDataWrapper.TYPE_MARKET_DATA_PACK, ATProtoMarketDataWrapper.TYPE_MARKET_DATA, "TEST", mktList);
			}
		}
	}
}
