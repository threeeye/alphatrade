package com.isoplane.at.ibproxy.model;

import com.isoplane.at.commons.model.ATSecurity;

public interface IATSecurityChangeListener {

	void changed(ATSecurity sec);
}
