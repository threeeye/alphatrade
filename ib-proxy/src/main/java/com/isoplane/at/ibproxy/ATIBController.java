package com.isoplane.at.ibproxy;

import org.apache.commons.lang3.StringUtils;

import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.ibproxy.adapter.ATIBWrapper;
import com.isoplane.at.ibproxy.model.IATSecurityChangeListener;

public class ATIBController implements IATSecurityChangeListener {

	private ATIBWrapper _wrapper;
	private ATStore _store;
	// _secMap = new ConcurrentHashMap<>();

	public ATIBController(ATStore store_, ATIBWrapper wrapper_) {
		_wrapper = wrapper_;
		_store = store_;
		_wrapper.registerListener(this);
	}

	public boolean add(String occId_) {
		if (StringUtils.isBlank(occId_))
			return false;
		ATSecurity sec = _store.get(occId_);
		if (sec == null) {
			sec = ATModelUtil.toSecurity(occId_);
			_store.put(sec);
		}
		return _wrapper.subscribe(occId_);
	}

	public void remove(String occId_) {
		if (StringUtils.isBlank(occId_))
			return;
		_wrapper.unsubscribe(occId_);
	}

	public String test(String[] data_) {
		// String symbol = "AAPL";
		if (data_ != null && data_.length > 0) {
			String[] tokens = data_[0].split(",");
			for (String token : tokens) {
				String symbol = token.trim().toUpperCase();
				add(symbol);
			}
			return String.format("{\"occId\": \"%s\"}", data_.toString());// "ok";
		} else {
			add("AAPL");
			return String.format("{\"occId\": \"Default-AAPL\"}");// "ok";
		}
	}

	@Override
	public void changed(ATSecurity sec_) {
		// TODO Auto-generated method stub

	}
}
