package com.isoplane.at.ibproxy.adapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashBiMap;
import com.ib.client.CommissionReport;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.DeltaNeutralContract;
import com.ib.client.EClientSocket;
import com.ib.client.EJavaSignal;
import com.ib.client.EReader;
import com.ib.client.EReaderSignal;
import com.ib.client.EWrapper;
import com.ib.client.Execution;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.SoftDollarTier;
import com.ib.client.TagValue;
import com.ib.client.Types.MktDataType;
import com.ib.contracts.StkContract;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.ibproxy.ATStore;
import com.isoplane.at.ibproxy.model.ATConfiguration;
import com.isoplane.at.ibproxy.model.IATSecurityChangeListener;
import com.isoplane.at.ibproxy.model.MCIbOptionRecord;
import com.isoplane.at.ibproxy.model.MCSemaphore;

/**
 * Error codes: https://www.interactivebrokers.com/en/software/api/apiguide/tables/api_message_codes.htm
 * https://interactivebrokers.github.io/tws-api/message_codes.html#gsc.tab=0
 * 
 * @author miron
 */
public class ATIBWrapper implements EWrapper {

	static final Logger log = LoggerFactory.getLogger(ATIBWrapper.class);

	// @Value("${ib.server}")
	// private String _ibServer;
	// @Value("${ib.port}")
	// private int _ibPort;
	// @Value("${ib.detail.timeout}")
	// private long _ibDetailTimeout;

	private ATConfiguration _config;
	private ATStore _store;

	private Semaphore _semaphore;
	private EReaderSignal _readerSignal;
	private int _reqId = 1;
	private EClientSocket _clientSocket;
	private EReader _reader;
	private Map<Integer, Semaphore> _reqSemMap;
	private Map<Integer, IATIBResponseHandler> _responseMap;
	private boolean _isActive = false;

	private Map<Integer, String> _id2OccMap;
	private Map<String, Integer> _occ2IdMap;
	private Set<IATSecurityChangeListener> _securityListeners;

	static public int CLIENT_ID = 1;

	public ATIBWrapper(ATConfiguration config_, ATStore store_) {
		_config = config_;
		_store = store_;

		_semaphore = new Semaphore(0);
		_reqSemMap = new HashMap<>();
		_responseMap = new HashMap<>();
		_securityListeners = new HashSet<>();

		HashBiMap<Integer, String> map = HashBiMap.create();
		_id2OccMap = map;
		_occ2IdMap = map.inverse();
	}

	public void init() {
		connect();
	}

	public void shutdown() {
		disconnect();
	}

	private void connect() {
		try {
			if (_isActive)
				return;
			_isActive = true;
			log.info(String.format("Connecting to [%s:%d] as client [%d]", _config.getIBServer(), _config.getIBPort(),
					CLIENT_ID));
			_readerSignal = new EJavaSignal();
			_clientSocket = new EClientSocket(this, _readerSignal);
			// tryConnect();
			_clientSocket.eConnect(_config.getIBServer(), _config.getIBPort(), CLIENT_ID);
			if (!_clientSocket.isConnected()) {
				log.error("Not connected");
				return;
			}
			_reader = new EReader(_clientSocket, _readerSignal);
			_reader.start();
			_clientSocket.reqMarketDataType(MktDataType.Delayed.ordinal());

			new Thread() {
				@Override
				public void run() {
					while (_isActive && _clientSocket.isConnected()) {
						if (_semaphore.hasQueuedThreads()) {
							_semaphore.release();
						}
						_readerSignal.waitForSignal();
						try {
							_reader.processMsgs();
						} catch (IOException ex) {
							_isActive = false;
							error("Connection broken", ex);
						}
					}
					log.info("##################################");
					log.info("#                                #");
					log.info("#    Terminating IB Connector    #");
					log.info("#                                #");
					log.info("##################################");
					_semaphore.release();
				}
			}.start();

			_semaphore.acquire();

		} catch (InterruptedException ex) {
			log.error("connect", ex);
		}
	}

	public void registerListener(IATSecurityChangeListener listener_) {
		_securityListeners.add(listener_);
	}

	public void unregisterListener(IATSecurityChangeListener listener_) {
		_securityListeners.remove(listener_);
	}

	private void tryConnect() {
		Timer timer = new Timer();
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				_clientSocket.eConnect(_config.getIBServer(), _config.getIBPort(), CLIENT_ID);
				if (_clientSocket.isConnected()) {
					log.info("Socket connected");
					timer.cancel();
				}
			}
		};
		timer.schedule(task, 0, 5000);
	}

	public void disconnect() {
		_isActive = false;
		_clientSocket.eDisconnect();
	}

	private synchronized int getRequestId() {
		int id = _reqId++;
		return id;
	}

	public boolean subscribe(String occId_) {
		if (StringUtils.isBlank(occId_))
			return false;
		Integer reqId = _occ2IdMap.get(occId_);
		if (reqId == null) {
			reqId = getRequestId();
			_occ2IdMap.put(occId_, reqId);
		}
		Contract contract = new StkContract(occId_);
		_clientSocket.reqMktData(reqId, contract, "", false, null);
		return true;
	}

	public void unsubscribe(String occId_) {
		if (StringUtils.isBlank(occId_))
			return;
		Integer reqId = _occ2IdMap.remove(occId_);
		if (reqId != null) {
			_clientSocket.cancelMktData(reqId);
		}
	}

	public List<ContractDetails> requestOptionDetails(String sym_, Contract ctc_) {
		try {
			int reqId = getRequestId();
			ATIBOptionDetailsHandler result = new ATIBOptionDetailsHandler(reqId, sym_, _clientSocket);
			_responseMap.put(reqId, result);
			Semaphore s = new Semaphore(0);
			_reqSemMap.put(reqId, s);
			_clientSocket.reqContractDetails(reqId, ctc_);
			boolean isAcquired = s.tryAcquire(_config.getIBDetailTimeout(), TimeUnit.MILLISECONDS);
			_responseMap.remove(reqId);
			if (!isAcquired) {
				String msg = String.format("TIME OUT requestOptionDetails [%s]", sym_);
				log.warn(msg);
				new Thread(new Runnable() {

					@Override
					public void run() {
						contractDetailsEnd(reqId);
					}
				}).start();
				return null;
				// throw new MCException(msg, MCException.ERROR_TIMEOUT);
			}
			if (result.isAborted()) {
				log.warn("Aborted");
				return null;
				// throw new MCException(result.getAbortMessage(), result.getAbortId());
			}
			return result.getContracts();
		} catch (InterruptedException ex) {
			log.warn("Interrupted");
			return null;
			// throw new MCException(ex.getMessage(), ex);
		}
	}

	public List<MCIbOptionRecord> getOptionChain(String symbol_, List<ContractDetails> details_) {
		if (details_ != null && !details_.isEmpty()) {
			List<MCIbOptionRecord> result = new ArrayList<>();
			_clientSocket.reqMarketDataType(2);

			int factor = details_.size() / 100;
			int remainder = details_.size() % 100;
			log.info("size: " + details_.size());
			int startIdx = 0;
			for (int i = 0; i < factor; i++) {
				int endIdx = startIdx + 100;
				List<ContractDetails> subdetails = details_.subList(startIdx, endIdx);
				List<MCIbOptionRecord> chain = requestOptionChain(symbol_, subdetails);
				if (chain != null) {
					result.addAll(chain);
				}
				log.info("i: " + i + ", start: " + startIdx + ", end: " + endIdx + ", size: " + subdetails.size());
				startIdx = endIdx;
			}
			if (remainder > 0) {
				int endIdx = startIdx + remainder;
				List<ContractDetails> subdetails = details_.subList(startIdx, endIdx);
				List<MCIbOptionRecord> chain = requestOptionChain(symbol_, subdetails);
				if (chain != null) {
					result.addAll(chain);
				}
				log.info("remainder - start: " + startIdx + ", end: " + endIdx + ", size: " + subdetails.size());
			}

			// int startIdx = 0;
			// int batchSize = Math.min(100, details_.size());
			// List<ContractDetails> subdetails = details_.subList(i, i + batchSize);
			//
			// List<MCIbOptionRecord> chain = requestOptionChain(symbol_, details_);
			// if (chain != null)
			// result.addAll(chain);

			return result;
		} else {
			return null;
		}
	}

	private List<MCIbOptionRecord> requestOptionChain(String symbol_, List<ContractDetails> details_) {
		long timeout = 15000;
		List<MCIbOptionRecord> result = Collections.synchronizedList(new ArrayList<>());
		MCSemaphore s = new MCSemaphore(details_.size());
		for (ContractDetails detail : details_) {
			try {
				int id = getRequestId();
				ATIBOptionsHandler handler = new ATIBOptionsHandler(id, symbol_, result, detail.contract(),
						_clientSocket, s, timeout);
				_responseMap.put(id, handler);
				_clientSocket.reqMktData(id, detail.contract(), "", false, Collections.<TagValue> emptyList());

				Thread.sleep(100); // Throttle for IB max 50 requests/sec
			} catch (InterruptedException ex) {
				log.error("Request interrupted", ex);
			}
		}
		s.acquire();
		return s.isAborted() ? null : result;
	}

	// http://interactivebrokers.github.io/tws-api/tick_types.html#gsc.tab=0
	@Override
	public void tickPrice(int reqId_, int field_, double price_, int canAutoExecute_) {
		String occId = _id2OccMap.get(reqId_);
		ATSecurity sec = StringUtils.isNotBlank(occId) ? _store.get(occId) : null;
		if (sec == null) {
			log.warn(String.format("Unregistered security [%s]", occId));
			return;
		}
		if (price_ < 0) {
			log.warn(String.format("Negative price [%s - %d - %.2f]", occId, field_, price_));
			return;
		}
		log.info(String.format("Price [%-4s - %3d - %6.2f]", sec.getOccId(), field_, price_));
		boolean isChanged = false;
		switch (field_) {
		case 1: // Bid price
		case 66: // Bid price delayed
			sec.setPriceBid(price_);
			isChanged = true;
			break;
		case 2: // Ask price
		case 67: // Ask price delayed
			sec.setPriceAsk(price_);
			isChanged = true;
			break;
		case 4: // Last price
		case 68: // Last price delayed
			sec.setPriceLast(price_);
			isChanged = true;
			break;
		case 6: // Day high
		case 72: // Day high delayed
			sec.setPriceHigh(price_);
			isChanged = true;
			break;
		case 7: // Day low
		case 73: // Day low delayed
			sec.setPriceLow(price_);
			isChanged = true;
			break;
		case 9: // Previous day close
		case 75: // Previous day close delayed
			sec.setPriceClose(price_);
			isChanged = true;
			break;
		case 14: // Day open
		case 76: // Day open delayed
			sec.setPriceOpen(price_);
			isChanged = true;
			break;
		default:
			log.warn(String.format("Unrecognized price field [%d]", field_));
		}
		if (isChanged) {
			sec.setChanged(true);
			sec.setLastUpdate(new Date());
			for (IATSecurityChangeListener listener : _securityListeners) {
				listener.changed(sec);
			}
		}
	}

	@Override
	public void tickSize(int reqId_, int field_, int size_) {
		String occId = _id2OccMap.get(reqId_);
		ATSecurity sec = StringUtils.isNotBlank(occId) ? _store.get(occId) : null;
		if (sec == null) {
			log.warn(String.format("Unregistered security [%s]", occId));
			return;
		}
		log.info(String.format("Size  [%-4s - %3d - %6d]", sec.getOccId(), field_, size_));
		boolean isChanged = false;
		switch (field_) {
		case 0: // Bid size
		case 69: // Bid size delayed
			sec.setSizeBid(size_);
			isChanged = true;
			break;
		case 3: // Ask size
		case 70: // Ask size delayed
			sec.setSizeAsk(size_);
			isChanged = true;
			break;
		case 5: // Last size
		case 71: // Last size delayed
			sec.setSizeLast(size_);
			isChanged = true;
			break;
		case 8: // Day volume (US: mult 100)
		case 74: // Day volume delayed
			sec.setVolume(size_);
			isChanged = true;
			break;
		default:
			log.warn(String.format("Unrecognized size field  [%d]", field_));
		}
		if (isChanged) {
			sec.setChanged(true);
			sec.setLastUpdate(new Date());
			for (IATSecurityChangeListener listener : _securityListeners) {
				listener.changed(sec);
			}
		}
	}

	@Override
	public void tickOptionComputation(int reqId_, int field_, double impliedVol_, double delta_, double optPrice_,
			double pvDividend_, double gamma_, double vega_, double theta_, double undPrice_) {
		IATIBResponseHandler handler = _responseMap.get(reqId_);
		if (handler instanceof ATIBOptionsHandler) {
			((ATIBOptionsHandler) handler).tickOptionComputation(field_, impliedVol_, delta_, optPrice_, pvDividend_,
					gamma_, vega_, theta_, undPrice_);
		}
		// log.debug("tickOptionComputation");
	}

	@Override
	public void tickGeneric(int reqId_, int tickType, double value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickString(int reqId_, int tickType_, String value_) {
		String occId = _id2OccMap.get(reqId_);
		log.debug(String.format("tickString [%s - %d - %s]", occId, tickType_, value_));
	}

	@Override
	public void tickEFP(int reqId_, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture,
			int holdDays, String futureLastTradeDate, double dividendImpact, double dividendsToLastTradeDate) {
		log.debug("tickEFP");
	}

	@Override
	public void orderStatus(int orderId, String status, double filled, double remaining, double avgFillPrice,
			int permId, int parentId, double lastFillPrice, int clientId, String whyHeld) {
		log.debug("orderStatus");
	}

	@Override
	public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
		log.debug("openOrder");
	}

	@Override
	public void openOrderEnd() {
		log.debug("openOrderEnd");
	}

	@Override
	public void updateAccountValue(String key, String value, String currency, String accountName) {
		log.debug("updateAccountValue");
	}

	@Override
	public void updatePortfolio(Contract contract, double position, double marketPrice, double marketValue,
			double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {
		log.debug("updatePortfolio");
	}

	@Override
	public void updateAccountTime(String timeStamp) {
		log.debug("updateAccountTime");
	}

	@Override
	public void accountDownloadEnd(String accountName) {
		log.debug("accountDownloadEnd");
	}

	@Override
	public void nextValidId(int orderId_) {
		log.debug("nextValidId: " + orderId_);
	}

	@Override
	public void contractDetails(int reqId_, ContractDetails contractDetails_) {
		IATIBResponseHandler result = _responseMap.get(reqId_);
		if (result != null) {
			result.handle(contractDetails_);
		}
	}

	@Override
	public void bondContractDetails(int reqId_, ContractDetails contractDetails) {
		log.debug("bondContractDetails");
	}

	@Override
	public void contractDetailsEnd(int reqId_) {
		// log.info("contractDetailsEnd: " + reqId_);
		Semaphore s = _reqSemMap.remove(reqId_);
		if (s != null) {
			s.release();
		}
	}

	@Override
	public void execDetails(int reqId_, Contract contract, Execution execution) {
		log.debug("execDetails");
	}

	@Override
	public void execDetailsEnd(int reqId_) {
		log.debug("execDetailsEnd");
	}

	@Override
	public void updateMktDepth(int reqId_, int position, int operation, int side, double price, int size) {
		log.debug("updateMktDepth");
	}

	@Override
	public void updateMktDepthL2(int reqId_, int position, String marketMaker, int operation, int side, double price,
			int size) {
		log.debug("updateMktDepthL2");
	}

	@Override
	public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
		log.debug("updateNewsBulletin");
	}

	@Override
	public void managedAccounts(String accountsList_) {
		log.debug("managedAccounts: " + accountsList_);
	}

	@Override
	public void receiveFA(int faDataType, String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void historicalData(int reqId_, String date, double open, double high, double low, double close, int volume,
			int count, double WAP, boolean hasGaps) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerParameters(String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerData(int reqId_, int rank, ContractDetails contractDetails, String distance, String benchmark,
			String projection, String legsStr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerDataEnd(int reqId_) {
		// TODO Auto-generated method stub

	}

	@Override
	public void realtimeBar(int reqId_, long time, double open, double high, double low, double close, long volume,
			double wap, int count) {
		// TODO Auto-generated method stub

	}

	@Override
	public void currentTime(long time_) {
		log.debug(String.format("currentTime [%d]", time_));
	}

	@Override
	public void fundamentalData(int reqId_, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deltaNeutralValidation(int reqId_, DeltaNeutralContract underComp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickSnapshotEnd(int reqId_) {
		log.debug(String.format("tickSnapshotEnd [%d]", reqId_));
	}

	@Override
	public void marketDataType(int reqId_, int marketDataType_) {
		// log.debug("marketDataType: " + MarketDataType.getField(marketDataType_));
	}

	@Override
	public void commissionReport(CommissionReport commissionReport) {
		// TODO Auto-generated method stub

	}

	@Override
	public void position(String account, Contract contract, double pos, double avgCost) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummary(int reqId_, String account, String tag, String value, String currency) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummaryEnd(int reqId_) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyMessageAPI(String apiData) {
		log.debug("verifyMessageAPI");
	}

	@Override
	public void verifyCompleted(boolean isSuccessful, String errorText) {
		log.debug("verifyCompleted");
	}

	@Override
	public void verifyAndAuthMessageAPI(String apiData, String xyzChallange) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyAndAuthCompleted(boolean isSuccessful, String errorText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayGroupList(int reqId_, String groups) {
		// TODO Auto-generated method stub

	}

	@Override
	public void displayGroupUpdated(int reqId_, String contractInfo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionMulti(int reqId_, String account, String modelCode, Contract contract, double pos,
			double avgCost) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionMultiEnd(int reqId_) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountUpdateMulti(int reqId_, String account, String modelCode, String key, String value,
			String currency) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountUpdateMultiEnd(int reqId_) {
		// TODO Auto-generated method stub

	}

	@Override
	public void securityDefinitionOptionalParameter(int reqId_, String exchange, int underlyingConId,
			String tradingClass, String multiplier, Set<String> expirations, Set<Double> strikes) {
		// TODO Auto-generated method stub

	}

	@Override
	public void securityDefinitionOptionalParameterEnd(int reqId_) {
		// TODO Auto-generated method stub

	}

	@Override
	public void softDollarTiers(int reqId_, SoftDollarTier[] tiers) {
		// TODO Auto-generated method stub

	}

	/// UTILITY

	private void error(String msg_, Exception ex_) {
		log.error(msg_, ex_);
	}

	@Override
	public void error(Exception ex_) {
		if (!_clientSocket.isConnected()) {
			log.error("Lost connection");
		}
		log.error("Error", ex_);
	}

	@Override
	public void error(String str_) {
		log.error(String.format("Error: ", str_));
	}

	@Override
	public void error(int reqId_, int errorCode_, String errorMsg_) {
		// Error[200] 15223 / No security definition has been found for the request
		// Error[200] 5530 / No security definition has been found for the request
		// Error[322] Server error when processing an API client request (such as duplicate ticker id)
		// Error[2108] -1 / Market data farm connection is inactive but should be available upon demand.usopt
		// Error[507] -1 / Bad Message Length null
		// Error[300] 9 / Can't find EId with tickerId:9
		// Error[100] 2 / Max rate of messages per second has been exceeded:max=50 rec=8366 (3)

		String occId = _id2OccMap.get(reqId_);
		String abortMessage = String.format("unknown");
		switch (errorCode_) {
		case 322:
			log.error(String.format("Server error[%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			break;
		case 2104:
		case 2106:
			log.trace(String.format("Warn [%d] %d / %s", errorCode_, reqId_, errorMsg_));
			break;
		case 200:
			abortMessage = "No security definition found";
		default:
			log.error(String.format("Error[%d] %3d/%-4s - %s", errorCode_, reqId_, occId, errorMsg_));
			IATIBResponseHandler handler = _responseMap.get(reqId_);
			if (handler != null) {
				handler.abort(errorCode_, abortMessage);
			}
			Semaphore s = _reqSemMap.remove(reqId_);
			if (s != null) {
				s.release();
			}
			break;
		}
	}

	@Override
	public void connectionClosed() {
		log.info("connectionClosed");
	}

	@Override
	public void connectAck() {
		if (_clientSocket.isAsyncEConnect()) {
			log.info("Acknowledging asynchronous connection");
			_clientSocket.startAPI();
		} else {
			log.info("Acknowledging synchronous connection");
		}
	}

}
