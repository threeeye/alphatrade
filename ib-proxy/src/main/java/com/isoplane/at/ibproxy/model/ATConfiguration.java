package com.isoplane.at.ibproxy.model;

import org.apache.commons.configuration2.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATConfiguration {

	static final Logger log = LoggerFactory.getLogger(ATConfiguration.class);

	// private double priceStatAmplitude;
	// private double priceStatSlope;
	// private long processPeriod;
	// private long pricingPeriod;
	// private int maxOptionDays; // Only for json decoding
	// private double baseSafetyMargin; // Only for json decoding
	// private double boundaryMargin; // Property only for json decoding. Stored in underlying map
	// private int boundaryRange; // Property only for json decoding. Stored in underlying map
	// private double minApy; // Only for json decoding
	// private int saveBulkSize;
	// private String[] extraSymbols;
	// private String[] ideaTriggers;
	// private String[] blacklistSymbols;
	// private String[] alertTriggers;
	private Configuration _config;
	// private int _status;

	public ATConfiguration(Configuration config_) {
		_config = config_;
	}

	public ATConfigurationChange update(ATConfiguration config_) {
		ATConfigurationChange result = new ATConfigurationChange();
		// boolean result = false;
		if (config_ == null)
			return result;
		// if (getPriceStatAmplitude() != config_.priceStatAmplitude) {
		// setPriceStatAmplitude(config_.priceStatAmplitude);
		// log.info(String.format("-> Price Stat Amp [%.2f]", getPriceStatAmplitude()));
		// result.isChanged = true;
		// }
		// if (getPriceStatSlope() != config_.priceStatSlope) {
		// setPriceStatSlope(config_.priceStatSlope);
		// log.info(String.format("-> Price Stat Slope [%.2f]", getPriceStatSlope()));
		// result.isChanged = true;
		// }
		// if (getProcessPeriod() != config_.processPeriod) {
		// setProcessPeriod(config_.processPeriod);
		// log.info(String.format("-> Processing period [%d]", getProcessPeriod()));
		// result.isChanged = true;
		// }
		// if (getPricingPeriod() != config_.pricingPeriod) {
		// setPricingPeriod(config_.pricingPeriod);
		// log.info(String.format("-> Pricing period [%d]", getPricingPeriod()));
		// result.isChanged = true;
		// }
		// if (getMaxOptionDays() != config_.maxOptionDays) {
		// setMaxOptionDays(config_.maxOptionDays);
		// log.info(String.format("-> Max Option Days [%d]", getMaxOptionDays()));
		// result.isChanged = true;
		// }
		// if (getBaseSafetyMargin() != config_.baseSafetyMargin) {
		// setBaseSafetyMargin(config_.baseSafetyMargin);
		// log.info(String.format("-> Safety Margin [%.2f]", getBaseSafetyMargin()));
		// result.isChanged = true;
		// }
		// if (getBoundaryMargin() != config_.boundaryMargin) {
		// setBoundaryMargin(config_.boundaryMargin);
		// log.info(String.format("-> Boundary Margin [%.2f]", getBoundaryMargin()));
		// result.isChanged = true;
		// result.isBoundaryMargin = true;
		// }
		// if (getBoundaryRange() != config_.boundaryRange) {
		// setBoundaryRange(config_.boundaryRange);
		// log.info(String.format("-> Boundary Range [%d]", getBoundaryRange()));
		// result.isChanged = true;
		// result.isBoundaryRange = true;
		// }
		// if (getMinApy() != config_.minApy) {
		// setMinApy(config_.minApy);
		// log.info(String.format("-> Minimum APY [%.2f]", getMinApy()));
		// result.isChanged = true;
		// }
		// if (getSaveBulkSize() != config_.saveBulkSize) {
		// setSaveBulkSize(config_.saveBulkSize);
		// log.info(String.format("-> Save bulk size [%d]", getSaveBulkSize()));
		// result.isChanged = true;
		// }
		// if (!Arrays.equals(getExtraSymbols(), config_.getExtraSymbols())) {
		// setExtraSymbols(config_.getExtraSymbols());
		// log.info(String.format("-> Extra symbols [%s]", String.join(", ", getExtraSymbols())));
		// result.isChanged = true;
		// }
		// if (!Arrays.equals(getIdeaTriggers(), config_.getIdeaTriggers())) {
		// setIdeaTriggers(config_.getIdeaTriggers());
		// log.info(String.format("-> Idea triggers [%s]", String.join(", ", getIdeaTriggers())));
		// result.isChanged = true;
		// result.isIdeaTriggers = true;
		// }
		// if (!Arrays.equals(getBlacklistSymbols(), config_.getBlacklistSymbols())) {
		// setBlacklistSymbols(config_.getBlacklistSymbols());
		// log.info(String.format("-> Blacklist [%s]", String.join(", ", getBlacklistSymbols())));
		// result.isChanged = true;
		// }
		// if (!Arrays.equals(getAlertTriggers(), config_.getAlertTriggers())) {
		// setAlertTriggers(config_.getAlertTriggers());
		// log.info(String.format("-> Alert triggers [%s]", String.join(", ", getAlertTriggers())));
		// result.isChanged = true;
		// result.isAlertTriggers = true;
		// }
		return result;
	}

	public int getWebPort() {
		return _config.getInt("web.port");
	}

	public int getWebPoolSizeMin() {
		return _config.getInt("web.pool.min");
	}

	public int getWebPoolSizeMax() {
		return _config.getInt("web.pool.max");
	}

	public int getWebPoolIdldeTimeout() {
		return _config.getInt("web.pool.idleto");
	}

	public String getIBServer() {
		return _config.getString("ib.server");
	}

	public int getIBPort() {
		return _config.getInt("ib.port");
	}

	public long getIBDetailTimeout() {
		return _config.getLong("ib.detail.timeout");
	}

	// public double getBaseSafetyMargin() {
	// return _config.getDouble("safety.margin");
	// }
	//
	// public void setBaseSafetyMargin(double baseSafetyMargin) {
	// _config.setProperty("safety.margin", baseSafetyMargin);
	// }
	//
	// public double getMinApy() {
	// return _config.getDouble("min.apy");
	// }
	//
	// public void setMinApy(double minApy) {
	// _config.setProperty("min.apy", minApy);
	// }
	//
	// public long getProcessPeriod() {
	// return _config.getLong("period.process");
	// }
	//
	// public void setProcessPeriod(long processPeriod) {
	// _config.setProperty("period.process", processPeriod);
	// }
	//
	// public long getPricingPeriod() {
	// return _config.getLong("period.pricing");
	// }
	//
	// public void setPricingPeriod(long pricingPeriod) {
	// _config.setProperty("period.pricing", pricingPeriod);
	// }
	//
	// public int getMaxOptionDays() {
	// return _config.getInt("max.option.days");
	// }
	//
	// public void setMaxOptionDays(int maxOptionDays) {
	// _config.setProperty("max.option.days", maxOptionDays);
	// }

	// public String[] getExtraSymbols() {
	// return extraSymbols;
	// }
	//
	// public void setExtraSymbols(String[] extraSymbols) {
	// this.extraSymbols = extraSymbols;
	// }

	public String getString(String key_) {
		return _config.getString(key_);
	}

	// public void setMarketStatus(int status_) {
	// _status = status_;
	// }
	//
	// public int getMarketStatus() {
	// return _status;
	// }

	// public double getPriceStatSlope() {
	// return _config.getDouble("stat.price.slope");
	// }
	//
	// public void setPriceStatSlope(double priceStatSlope) {
	// _config.setProperty("stat.price.slope", priceStatSlope);
	// }
	//
	// public double getPriceStatAmplitude() {
	// return _config.getDouble("stat.price.amp");
	// }
	//
	// public void setPriceStatAmplitude(double priceStatAmplitude) {
	// _config.setProperty("stat.price.amp", priceStatAmplitude);
	// }

	// public String[] getIdeaTriggers() {
	// return ideaTriggers;
	// }
	//
	// public void setIdeaTriggers(String[] fixedOpportunities) {
	// this.ideaTriggers = fixedOpportunities;
	// }
	//
	// public String[] getAlertTriggers() {
	// return alertTriggers;
	// }
	//
	// public void setAlertTriggers(String[] priceTriggers) {
	// this.alertTriggers = priceTriggers;
	// }
	//
	// public String[] getBlacklistSymbols() {
	// return blacklistSymbols;
	// }
	//
	// public void setBlacklistSymbols(String[] blacklistSymbols) {
	// this.blacklistSymbols = blacklistSymbols;
	// }

	// public int getSaveBulkSize() {
	// return _config.getInt("save.bulk.size");
	// }
	//
	// public void setSaveBulkSize(int saveBulkSize) {
	// _config.setProperty("save.bulk.size", saveBulkSize);
	// }
	//
	// public double getBoundaryMargin() {
	// return _config.getDouble("boundary.margin");
	// }
	//
	// public void setBoundaryMargin(double boundaryMargin) {
	// _config.setProperty("boundary.margin", boundaryMargin);
	// }
	//
	// public int getBoundaryRange() {
	// return _config.getInt("boundary.range");
	//
	// }
	//
	// public void setBoundaryRange(int boundaryRange) {
	// _config.setProperty("boundary.range", boundaryRange);
	// }

	public static class ATConfigurationChange {

		private boolean isChanged;
		// private boolean isBoundaryMargin;
		// private boolean isBoundaryRange;
		// private boolean isIdeaTriggers;
		// private boolean isMaxOptionDays;
		// private boolean isPriceStatAmplitude;
		// private boolean isPriceStatSlope;
		// private boolean isExtraSymbols;
		// private boolean isBlacklist;
		// private boolean isAlertTriggers;

		// public boolean isMaxOptionDays() {
		// return isMaxOptionDays;
		// }
		//
		// public boolean isPriceStatAmplitude() {
		// return isPriceStatAmplitude;
		// }
		//
		// public boolean isPriceStatSlope() {
		// return isPriceStatSlope;
		// }
		//
		// public boolean isExtraSymbols() {
		// return isExtraSymbols;
		// }
		//
		// public boolean isBlacklist() {
		// return isBlacklist;
		// }
		//
		// public boolean isIdeaTriggers() {
		// return isIdeaTriggers;
		// }
		//
		// public boolean isAlertTriggers() {
		// return isAlertTriggers;
		// }

		public boolean isChanged() {
			return isChanged;
		}

		// public boolean isBoundaryMargin() {
		// return isBoundaryMargin;
		// }
		//
		// public boolean isBoundaryRange() {
		// return isBoundaryRange;
		// }

	}

}
