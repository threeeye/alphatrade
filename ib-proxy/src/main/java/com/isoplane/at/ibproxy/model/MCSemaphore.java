package com.isoplane.at.ibproxy.model;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class MCSemaphore {

	private int _size;
	private Long _timeout;
	private Semaphore _semaphore;
	private boolean _isAborted;

	public MCSemaphore(int size_) {
		_isAborted = false;
		_size = size_;
		_timeout = null;
		_semaphore = new Semaphore(0);
	}

	public MCSemaphore(int size_, long timeout_) {
		this(size_);
		_timeout = timeout_;
	}

	public void release() {
		_size--;
		if (_size <= 0) {
			_semaphore.release();
		}
	}

	public void releaseAll() {
		_size = 0;
		_semaphore.drainPermits();
	}

	public void abort() {
		_isAborted = true;
		releaseAll();
	}

	public boolean isAborted() {
		return _isAborted;
	}

	public void acquire() {
		if (_isAborted)
			return;
		try {
			if (_timeout == null) {
				_semaphore.acquire();
			} else {
				_semaphore.tryAcquire(_timeout, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
