package com.isoplane.at.ibproxy.adapter;

public interface IATIBResponseHandler {

	void handle(Object o);

	void abort(int id, String message);

	boolean isAborted();

	String getSymbol();

	String getAbortMessage();

	int getAbortId();
}
