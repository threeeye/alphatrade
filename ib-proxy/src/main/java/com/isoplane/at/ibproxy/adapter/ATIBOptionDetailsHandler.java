package com.isoplane.at.ibproxy.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.EClientSocket;
import com.isoplane.at.ibproxy.model.MCIbOptionRecord;

public class ATIBOptionDetailsHandler implements IATIBResponseHandler {

	static final Logger log = LoggerFactory.getLogger(ATIBOptionDetailsHandler.class);

	static final SimpleDateFormat _df = new SimpleDateFormat("yyyyMMdd");

	private List<ContractDetails> _result = new ArrayList<>();
	@SuppressWarnings("unused")
	private int _reqId;
	@SuppressWarnings("unused")
	private EClientSocket _clientSocket;
	private boolean _isAborted;
	private String _sym;
	private String _abortMessage;
	private int _abortId;

	public ATIBOptionDetailsHandler(int reqId_, String sym_, EClientSocket clientSocket_) {
		_reqId = reqId_;
		_clientSocket = clientSocket_;
		_sym = sym_;
		_isAborted = false;
	}

	@Override
	public void handle(Object o_) {
		if (_isAborted)
			return;
		if (o_ instanceof ContractDetails) {
			_result.add((ContractDetails) o_);
		}
	}

	public List<ContractDetails> getContracts() {
		return _result;
	}

	@Override
	public void abort(int id_, String msg_) {
		_isAborted = true;
		_abortId = id_;
		_abortMessage = msg_;
	}

	@Override
	public int getAbortId() {
		return _abortId;
	}

	@Override
	public String getAbortMessage() {
		return _abortMessage;
	}

	@Override
	public boolean isAborted() {
		return _isAborted;
	}

	@Override
	public String getSymbol() {
		return _sym;
	}

	static public MCIbOptionRecord toOptionRecord(ContractDetails contract_) {
		if (contract_ != null && contract_.contract() != null) {
			Contract ctr = contract_.contract();
			try {
				MCIbOptionRecord result = new MCIbOptionRecord();
				result.setSymbol(ctr.symbol());
				result.setExpiration(_df.parse(ctr.lastTradeDateOrContractMonth()));
				return result;
			} catch (ParseException ex) {
				log.error(String.format("Error parsing [%s]", ctr.lastTradeDateOrContractMonth()), ex);
			}
		}
		return null;
	}

}
