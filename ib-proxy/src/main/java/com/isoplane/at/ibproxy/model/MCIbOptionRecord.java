package com.isoplane.at.ibproxy.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MCIbOptionRecord extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;

	public static final String SYMBOL = "mcSymbol";
	public static final String DATE = "mcDate";

	public static final String SIZE_ASK = "mcSizeAsk";
	public static final String SIZE_BID = "mcSizeBid";
	public static final String SIZE_LAST = "mcSizeLast";
	public static final String STRIKE = "mcStrike";
	public static final String DATE_EXP = "mcDateExp";
	public static final String PRICE_ASK = "mcPxAsk";
	public static final String PRICE_BID = "mcPxBid";
	public static final String PRICE_HIGH = "mcPxHigh";
	public static final String PRICE_LOW = "mcPxLow";
	public static final String VOLUME = "mcVolume";
	public static final String PRICE_LAST = "mcPxLast";
	public static final String PRICE_UNDERLYING = "mcPxUl";
	public static final String DIV_UNDERLYING = "mcDivUl"; // The present value of dividends expected on the underlying
	public static final String IV = "mcIV";
	public static final String DELTA = "mcDelta";
	public static final String GAMMA = "mcGamma";
	public static final String VEGA = "mcVega";
	public static final String THETA = "mcTheta";
	public static final String PRICE_CLOSE = "mcPxClose";
	public static final String PRICE_OPTION = "mcPxOption";
	public static final String OPTION_RIGHT = "mcOptRight";
	public static final String OPTION_TYPE = "mcOptType";

	public MCIbOptionRecord() {
	}

	public MCIbOptionRecord(Map<String, Object> doc_) {
		super(doc_);
	}

	public MCIbOptionRecord(String symbol_, double strike_, Date expDate_, String right_) {
		// setKey(key_);
		setSymbol(symbol_);
		setStrike(strike_);
		setExpiration(expDate_);
		setOptionRight(right_);
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> all) {
		super.putAll(all);
	}

	// @Override
	// public String getKey() {
	// return getString(SYM_KEY);
	// }
	//
	// public void setKey(String symKey) {
	// put(SYM_KEY, symKey);
	// }

	public String getSymbol() {
		return (String) super.get(SYMBOL);
	}

	public void setSymbol(String symbol) {
		this.put(SYMBOL, symbol);
	}

	public Date getDate() {
		return (Date) get(DATE);
	}

	public void setDate(Date date) {
		put(DATE, date);
	}

	public void setSymbol(Date date) {
		this.put(DATE, date);
	}

	public Integer getSizeAsk() {
		return (Integer) get(SIZE_ASK);
	}

	public void setSizeAsk(Integer sizeAsk) {
		this.put(SIZE_ASK, sizeAsk);
	}

	public Integer getSizeBid() {
		return (Integer) get(SIZE_ASK);
	}

	public void setSizeBid(Integer sizeBid) {
		this.put(SIZE_BID, sizeBid);
	}

	public Integer getSizeLast() {
		return (Integer) get(SIZE_LAST);
	}

	public void setSizeLast(Integer sizeLast) {
		this.put(SIZE_LAST, sizeLast);
	}

	public Double getStrike() {
		return (Double) get(STRIKE);
	}

	public void setStrike(Double strike) {
		this.put(STRIKE, strike);
	}

	public Date getExpiration() {
		return (Date) get(DATE_EXP);
	}

	public void setExpiration(Date expiration) {
		this.put(DATE_EXP, expiration);
	}

	public Double getPriceAsk() {
		return (Double) get(PRICE_ASK);
	}

	public void setPriceAsk(Double priceAsk) {
		this.put(PRICE_ASK, priceAsk);
	}

	public Double getPriceBid() {
		return (Double) get(PRICE_BID);
	}

	public void setPriceBid(Double priceBid) {
		this.put(PRICE_BID, priceBid);
	}

	public Double getPriceHigh() {
		return (Double) get(PRICE_HIGH);
	}

	public void setPriceHigh(Double priceHigh) {
		this.put(PRICE_HIGH, priceHigh);
	}

	public Double getPriceLow() {
		return (Double) get(PRICE_LOW);
	}

	public void setPriceLow(Double priceLow) {
		this.put(PRICE_LOW, priceLow);
	}

	public Double getPriceLast() {
		return (Double) get(PRICE_LAST);
	}

	public void setPriceLast(Double priceLast) {
		this.put(PRICE_LAST, priceLast);
	}

	public Double getPticeClose() {
		return (Double) get(PRICE_CLOSE);
	}

	public void setPriceClose(Double close) {
		this.put(PRICE_CLOSE, close);
	}

	public Double getPriceOption() {
		return (Double) get(PRICE_OPTION);
	}

	public void setPriceOption(Double priceOption) {
		this.put(PRICE_OPTION, priceOption);
	}

	public Double getPriceUnderlying() {
		return (Double) get(PRICE_UNDERLYING);
	}

	public void setPriceUnderlying(Double priceUnderlying) {
		this.put(PRICE_UNDERLYING, priceUnderlying);
	}

	public Integer getVolume() {
		return (Integer) get(VOLUME);
	}

	public void setVolume(Integer volume) {
		this.put(VOLUME, volume);
	}

	public Double getDivUnderlying() {
		return (Double) get(DIV_UNDERLYING);
	}

	public void setDivUnderlying(Double divUnderlying) {
		this.put(DIV_UNDERLYING, divUnderlying);
	}

	public Double getIv() {
		return (Double) get(IV);
	}

	public void setIv(Double iv) {
		this.put(IV, iv);
	}

	public Double getDelta() {
		return (Double) get(DELTA);
	}

	public void setDelta(Double delta) {
		this.put(DELTA, delta);
	}

	public Double getGamma() {
		return (Double) get(GAMMA);
	}

	public void setGamma(Double gamma) {
		this.put(GAMMA, gamma);
	}

	public Double getVega() {
		return (Double) get(VEGA);
	}

	public void setVega(Double vega) {
		this.put(VEGA, vega);
	}

	public Double getTheta() {
		return (Double) get(THETA);
	}

	public void setTheta(Double theta) {
		this.put(THETA, theta);
	}

	public String getOptionRight() {
		return (String) get(OPTION_RIGHT);
	}

	public void setOptionRight(String optionRight) {
		this.put(OPTION_RIGHT, optionRight);
	}

	public String getOptionType() {
		return (String) get(OPTION_TYPE);
	}

	public void setOptionType(String optionType) {
		this.put(OPTION_TYPE, optionType);
	}
}
