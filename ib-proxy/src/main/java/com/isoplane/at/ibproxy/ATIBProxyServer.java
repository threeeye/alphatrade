package com.isoplane.at.ibproxy;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.threadPool;

import java.io.File;
import java.net.InetAddress;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.isoplane.at.ibproxy.adapter.ATIBWrapper;
import com.isoplane.at.ibproxy.model.ATConfiguration;

public class ATIBProxyServer {

	static final Logger log = LoggerFactory.getLogger(ATIBProxyServer.class);

	static private ATIBProxyServer _instance;

	private Gson _gson;
	private ATIBController _svc;
	// private ATQueueBoss _queue;
	private ATConfiguration _config;
	// private Configuration _config2;

	public static void main(String[] args) {
		// log.info("lib: " + System.getProperty("java.library.path"));
		// System.loadLibrary("QuantLib");

		_instance = new ATIBProxyServer();
		_instance.init();
		_instance.serve();
	}

	private void init() {
		try {
			Configuration config = new Configurations().properties(new File("./ib-proxy.properties"));
			_config = new ATConfiguration(config);

			_gson = new Gson();
			// new GsonBuilder().registerTypeAdapter(ATSecurity.class, new ATSecurityDeserializer())
			// .registerTypeAdapter(ATSecurity.class, new ATSecuritySerializer())
			// .registerTypeAdapter(Date.class, new ATDateSerializer()).create();
			
			ATStore store = new ATStore(_config);

			ATIBWrapper ibwrapper = new ATIBWrapper(_config, store);
			ibwrapper.init();

			_svc = new ATIBController(store, ibwrapper);

			Runtime.getRuntime().addShutdownHook(new Thread() {
				public void run() {
					try {
						log.info("Shutting down");
						ibwrapper.shutdown();
						log.info("Terminated");
					} catch (Exception ex) {
						log.error("Error shutting down", ex);
					}
				}
			});

			String ipStr = "localhost";
			InetAddress ip = InetAddress.getLocalHost();
			ipStr = ip.getHostAddress();
			log.info(String.format("%s:%d", ipStr, _config.getWebPort()));
		} catch (Exception ex) {
			log.error("Error initializing", ex);
		}
	}

	private void serve() {
		port(_config.getWebPort());
		threadPool(_config.getWebPoolSizeMax(), _config.getWebPoolSizeMin(), _config.getWebPoolIdldeTimeout());
		after((request, response) -> {
			response.header("Content-Encoding", "gzip");
			response.type("application/json");
		});

		get("/api/status", (request, response) -> {
			// String json = _gson.toJson(ATSystemStatus.Current());
			return "status";
		});

		get("/api/test", (request, response) -> {
			String str = _svc.test(null);
			return str;
		});

		get("/api/test/*", (request, response) -> {
			String[] data = request.splat();
			String str = _svc.test(data);
			return str;
		});

	}

}
