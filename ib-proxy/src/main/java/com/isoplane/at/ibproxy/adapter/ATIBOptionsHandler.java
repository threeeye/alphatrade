package com.isoplane.at.ibproxy.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.ib.client.TickType;
import com.isoplane.at.ibproxy.model.MCIbOptionRecord;
import com.isoplane.at.ibproxy.model.MCSemaphore;

public class ATIBOptionsHandler implements IATIBResponseHandler {

	static final Logger log = LoggerFactory.getLogger(ATIBOptionsHandler.class);

	static final List<String> REQUEST_TYPES = Arrays.asList("tickPrice-ask", "tickPrice-bid", "tickPrice-close",
			/* "tickPrice-last", "tickPrice-high", "tickPrice-low", */ "tickSize-ask",
			"tickSize-bid", /* "tickSize-last", "tickSize-volume", */
			"tickOptionComputation");

	private List<String> _requestTypes = new ArrayList<String>(REQUEST_TYPES);
	private int _reqId;
	private MCIbOptionRecord _option;
	private EClientSocket _socket;
	private MCSemaphore _mcs;
	private boolean _isRunning;
	private boolean _isBestGreeks;
	private boolean _isAborted;
	private String _sym;
	private String _abortMessage;
	private int _abortId;

	private SimpleDateFormat _df = new SimpleDateFormat("yyyyMMdd");

	public ATIBOptionsHandler(int reqId_, String sym_, List<MCIbOptionRecord> result_, Contract ctc_,
			EClientSocket socket_, MCSemaphore mcs_, long timeout_) {
		_reqId = reqId_;
		_socket = socket_;
		_mcs = mcs_;
		_isAborted = false;
		_sym = sym_;

		try {
			Date d = _df.parse(ctc_.lastTradeDateOrContractMonth());
			_option = new MCIbOptionRecord(ctc_.symbol(), ctc_.strike(), d, ctc_.right().name());
			result_.add(_option);
			_isRunning = true;
			_isBestGreeks = false;

			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					if (terminate()) {
						log.trace(_reqId + " timed out");
					}
				}
			}, timeout_);

			// log.debug(_reqId + " - new option");// + _option);
		} catch (ParseException ex) {
			log.error(String.format("Invalid expiration date [%s]", sym_, ctc_.lastTradeDateOrContractMonth()), ex);
		}
	}

	@Override
	public void handle(Object o_) {
	}

	public void tickPrice(int tickType_, double price_, int canAutoExecute_) {
		// log.debug(_reqId + " - tickPrice");
		if (_requestTypes.isEmpty())
			return;
		TickType tickType = TickType.get(tickType_);
		switch (tickType) {
		case ASK:
			_option.setPriceAsk(price_);
			removeRequestType("tickPrice-ask");
			break;
		case BID:
			_option.setPriceBid(price_);
			removeRequestType("tickPrice-bid");
			break;
		case CLOSE:
			_option.setPriceClose(price_);
			removeRequestType("tickPrice-close");
			break;
		case HIGH:
			_option.setPriceHigh(price_);
			removeRequestType("tickPrice-high");
			break;
		case LOW:
			_option.setPriceLow(price_);
			removeRequestType("tickPrice-low");
			break;
		case LAST:
			_option.setPriceLast(price_);
			removeRequestType("tickPrice-last");
			break;
		default:
			// log.debug("tickPrice: " + tickType);
			break;
		}
	}

	public void tickSize(int tickType_, int size_) {
		// log.debug(_reqId + " - tickSize");
		if (_requestTypes.isEmpty())
			return;
		TickType tickType = TickType.get(tickType_);
		switch (tickType) {
		case ASK_SIZE:
			_option.setSizeAsk(size_);
			removeRequestType("tickSize-ask");
			break;
		case BID_SIZE:
			_option.setSizeBid(size_);
			removeRequestType("tickSize-bid");
			break;
		case LAST_SIZE:
			_option.setSizeLast(size_);
			removeRequestType("tickSize-last");
			break;
		case VOLUME:
			_option.setVolume(size_);
			removeRequestType("tickSize-volume");
			break;
		default:
			log.debug("tickSize: " + tickType);
			break;
		}
	}

	public void tickOptionComputation(int tickType_, double impliedVol_, double delta_, double optPrice_,
			double pvDividend_, double gamma_, double vega_, double theta_, double undPrice_) {
		// log.debug(_reqId + " - tickOptionComputation");
		if (_isBestGreeks || _requestTypes.isEmpty())
			return;

		String tickType;
		switch (TickType.get(tickType_)) {
		case ASK_OPTION:
			tickType = "Ask";
			break;
		case BID_OPTION:
			tickType = "Bid";
			break;
		case LAST_OPTION:
			tickType = "Last";
			break;
		case MODEL_OPTION:
			tickType = "Model";
			_isBestGreeks = true;
			break;
		default:
			tickType = null;
			break;
		}
		_option.setOptionType(tickType);

		_option.setIv(impliedVol_);
		_option.setDelta(delta_);
		_option.setTheta(theta_);
		_option.setGamma(gamma_);
		_option.setVega(vega_);

		_option.setPriceUnderlying(undPrice_);
		_option.setDivUnderlying(pvDividend_);
		_option.setPriceOption(optPrice_);

		removeRequestType("tickOptionComputation");

		// _isBestGreeks = TickType.MODEL_OPTION.index() == tickType_;
		// TickType.get(tickType_);
		// switch (tickType) {
		// case MODEL_OPTION:
		// break;
		// default:
		// log.debug("tickOptionComputation: " + tickType);
		// break;
		// }
	}

	private synchronized boolean terminate() {
		if (!_isRunning)
			return false;
		_isRunning = false;
		_option.setDate(new Date());
		if (_socket != null) {
			_socket.cancelMktData(_reqId);
		}
		if (_mcs != null) {
			_mcs.release();
		}
		return true;
	}

	private void removeRequestType(String type_) {
		_requestTypes.remove(type_);
		if (_requestTypes.isEmpty()) {
			if (terminate()) {
				// log.debug(_reqId + " - finalized: ");//
			}
		} else if (_requestTypes.size() == 1) {
			// log.info("1");
		}
	}

	@Override
	public void abort(int id_, String msg_) {
		if (_isAborted)
			return;
		_isAborted = true;
		_abortId = id_;
		_abortMessage = msg_;
		_socket.cancelMktData(_reqId);
		if (_mcs != null) {
			_mcs.abort();
		}
	}

	@Override
	public String getAbortMessage() {
		return _abortMessage;
	}

	@Override
	public int getAbortId() {
		return _abortId;
	}

	@Override
	public boolean isAborted() {
		return _isAborted;
	}

	@Override
	public String getSymbol() {
		return _sym;
	}

}
