package com.isoplane.at.ibproxy;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATOptionSecurity;
import com.isoplane.at.commons.model.ATSecurity;
import com.isoplane.at.commons.model.ATStock;
import com.isoplane.at.ibproxy.model.ATConfiguration;

public class ATStore {

	static final Logger log = LoggerFactory.getLogger(ATStore.class);

	@SuppressWarnings("unused")
	private ATConfiguration _config;
	// private IATSecuritiesDb _secDb;
	// private IATSecEventsDb _eventDb;
	private ConcurrentSkipListMap<String, ATStock> _stockMap;
	private ConcurrentSkipListMap<String, ATOptionSecurity> _optionMap;
	// private ConcurrentSkipListMap<String, TreeSet<ATLegacyDividend>> _dividendMap;
	// private ConcurrentSkipListMap<String, TreeSet<ATEarning>> _earningsMap;
	// private ConcurrentSkipListMap<String, Set<ATBaseCriteria>> _alertCriteriaMap;
	// private ConcurrentSkipListMap<String, Set<ATBaseCriteria>> _opportunityCriteriaMap;
	private Set<ATSecurity> _secStorageSet;
	// private Set<ATSecEvent> _eventStorageSet;
	// private int _maxStoreQueueSize;

	public ATStore(ATConfiguration config_) {
		_config = config_;
		// _secDb = secDb_;
		// _eventDb = sevDb_;
		_optionMap = new ConcurrentSkipListMap<>();
		_stockMap = new ConcurrentSkipListMap<>();
		// _dividendMap = new ConcurrentSkipListMap<>();
		// _earningsMap = new ConcurrentSkipListMap<>();
		// _alertCriteriaMap = new ConcurrentSkipListMap<>();
		// _opportunityCriteriaMap = new ConcurrentSkipListMap<>();
		_secStorageSet = new HashSet<>();
		// _eventStorageSet = new HashSet<>();
	}

	public void init() {
		log.info(String.format("Initializing..."));
		// List<ATSecurity> securities = _secDb.load(null, true);
		// int optCount = 0;
		// int stockCount = 0;
		// if (securities != null) {
		// for (ATSecurity sec : securities) {
		// if (sec instanceof ATStock) {
		// _stockMap.put(sec.getOccId(), (ATStock) sec);
		// stockCount++;
		// } else if (sec instanceof ATOptionSecurity) {
		// _optionMap.put(sec.getOccId(), (ATOptionSecurity) sec);
		// optCount++;
		// }
		// }
		// }
		// List<ATSecEvent> events = _eventDb.load(null, true);
		// int divCount = 0;
		// int ernCount = 0;
		// if (events != null) {
		// for (ATSecEvent event : events) {
		// String occId = event.getOccId();
		// if (event instanceof ATLegacyDividend) {
		// TreeSet<ATLegacyDividend> dividends = _dividendMap.get(occId);
		// if (dividends == null) {
		// dividends = new TreeSet<>();
		// _dividendMap.put(occId, dividends);
		// }
		// dividends.add((ATLegacyDividend) event);
		// divCount++;
		// } else if (event instanceof ATEarning) {
		// TreeSet<ATEarning> earnings = _earningsMap.get(occId);
		// if (earnings == null) {
		// earnings = new TreeSet<>();
		// _earningsMap.put(occId, earnings);
		// }
		// earnings.add((ATEarning) event);
		// ernCount++;
		// }
		// }
		// }
		// log.info(String.format("Loaded [%,7d] stocks", stockCount));
		// log.info(String.format("Loaded [%,7d] options", optCount));
		// log.info(String.format("Loaded [%,7d] dividends", divCount));
		// log.info(String.format("Loaded [%,7d] earnings", ernCount));
	}

	public void flush() {
		log.info("Flushing...");
		Set<ATSecurity> secStore = _secStorageSet;
		_secStorageSet = new HashSet<>();
		// _secDb.store(secStore);

		// Set<ATSecEvent> eventStore = _eventStorageSet;
		// _eventStorageSet = new HashSet<>();
		// _eventDb.store(eventStore);
	}

	public Set<String> getStockIds() {
		Set<String> result = new HashSet<>(_stockMap.keySet());
		return result;
	}

	@SuppressWarnings("unchecked")
	public <T extends ATSecurity> T get(String key_) {
		if (key_ == null)
			return null;
		T sec;
		if (key_.length() <= 13) {
			sec = (T) _stockMap.get(key_);
		} else {
			sec = (T) _optionMap.get(key_);
		}
		return sec;
	}

	// @SuppressWarnings("unchecked")
	// public <T extends ATSecEvent> TreeSet<T> getEvent(String key_, Class<T> clazz_) {
	// if (key_ == null)
	// return null;
	// TreeSet<T> sec;
	// if (ATLegacyDividend.class.equals(clazz_)) {
	// sec = (TreeSet<T>) _dividendMap.get(key_);
	// } else if (ATEarning.class.equals(clazz_)) {
	// sec = (TreeSet<T>) _earningsMap.get(key_);
	// } else {
	// return null;
	// }
	// return sec;
	// }

	public Collection<ATOptionSecurity> getOptionChain(String key_) {
		if (key_ == null)
			return null;
		String fromKey = key_ + "000000";
		String toKey = key_ + "999999";
		ConcurrentNavigableMap<String, ATOptionSecurity> subMap = _optionMap.subMap(fromKey, toKey);
		return subMap == null ? new TreeSet<>() : subMap.values();
	}

	public void put(ATSecurity sec_) {
		if (sec_ == null || sec_.getOccId() == null || !sec_.isChanged())
			return;
		sec_.setChanged(false);
		if (sec_ instanceof ATStock) {
			_stockMap.put(sec_.getOccId(), (ATStock) sec_);
		} else if (sec_ instanceof ATOptionSecurity) {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			ATOptionSecurity option = (ATOptionSecurity) sec_;
			if (today.compareTo(option.getExpirationDate()) <= 0) {
				_optionMap.put(option.getOccId(), option);
			}
		} else {
			return;
		}
		// queueForPersistence(sec_);
	}

	// public void put(ATSecEvent event_) {
	// if (event_ == null || event_.getOccId() == null || !event_.isChanged())
	// return;
	// event_.setChanged(false);
	// Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
	// String occId = event_.getOccId();
	// if (event_ instanceof ATLegacyDividend) {
	// ATLegacyDividend div = (ATLegacyDividend) event_;
	// if (today.compareTo(div.getExDate()) <= 0) {
	// TreeSet<ATLegacyDividend> dividends = _dividendMap.get(occId);
	// if (dividends == null) {
	// dividends = new TreeSet<>();
	// _dividendMap.put(occId, dividends);
	// }
	// dividends.add(div);
	// }
	// } else if (event_ instanceof ATEarning) {
	// ATEarning ern = (ATEarning) event_;
	// if (today.compareTo(ern.getDate()) <= 0) {
	// TreeSet<ATEarning> earnings = _earningsMap.get(occId);
	// if (earnings == null) {
	// earnings = new TreeSet<>();
	// _earningsMap.put(occId, earnings);
	// }
	// earnings.add(ern);
	// }
	// } else {
	// return;
	// }
	// queueForPersistence(event_);
	// }

	public void deactivate() {
		for (ATSecurity atSecurity : _stockMap.values()) {
			Set<String> userIds = atSecurity.getUserIds();
			if (userIds != null) {
				userIds.clear();
			}
		}
		for (ATSecurity atSecurity : _optionMap.values()) {
			Set<String> userIds = atSecurity.getUserIds();
			if (userIds != null) {
				userIds.clear();
			}
		}
	}

	// public void updateAlertCriteria(Set<ATBaseCriteria> crits_) {
	// Set<String> crits = updateCriteria(crits_, _alertCriteriaMap);
	// if (crits != null) {
	// log.debug(String.format("Alerts %s", String.join(",", crits)));
	// }
	// }

	// public void updateOpportunitiesCriteria(Set<ATBaseCriteria> crits_) {
	// Set<String> crits = updateCriteria(crits_, _opportunityCriteriaMap);
	// if (crits != null) {
	// log.debug(String.format("Ideas %s", String.join(",", crits)));
	// }
	// }

	// public Set<ATBaseCriteria> getAlertCriteria(String occId_) {
	// if (StringUtils.isBlank(occId_))
	// return null;
	// return _alertCriteriaMap.get(occId_);
	// }

	// public Set<ATBaseCriteria> getOpportunityCriteria(String occId_) {
	// if (StringUtils.isBlank(occId_))
	// return null;
	// return _opportunityCriteriaMap.get(occId_);
	// }

	// synchronized private Set<String> updateCriteria(Set<ATBaseCriteria> crits_,
	// ConcurrentSkipListMap<String, Set<ATBaseCriteria>> map_) {
	// if (crits_ == null || crits_.isEmpty()) {
	// Set<String> result = map_.isEmpty() ? null : new HashSet<>();
	// map_.clear();
	// return result;
	// }
	//
	// int oldHash = map_.values().stream().map(c -> c.toString().hashCode()).mapToInt(Integer::intValue).sum();
	// map_.clear();
	// Set<String> result = new TreeSet<>();
	// for (ATBaseCriteria crit : crits_) {
	//
	// if (crit == null)
	// continue;
	// Set<ATBaseCriteria> crits = map_.get(crit.getOccId());
	// if (crits == null) {
	// crits = new HashSet<>();
	// map_.put(crit.getOccId(), crits);
	// }
	// if (crits.add(crit)) {
	// result.add(crit.toString());
	// }
	// }
	// int newHash = map_.values().stream().map(c -> c.toString().hashCode()).mapToInt(Integer::intValue).sum();
	// return oldHash != newHash ? result : null;
	// }

	// private void queueForPersistence(ATSecurity sec_) {
	// try {
	// _secStorageSet.add(sec_);
	// if (_secStorageSet.size() > _config.getSaveBulkSize()) {
	// Set<ATSecurity> toStore = _secStorageSet;
	// _secStorageSet = new HashSet<>();
	// log.debug(String.format("Storing [%s(%d)]", sec_.getClass().getSimpleName(), toStore.size()));
	// (new Thread() {
	// public void run() {
	// _secDb.store(toStore);
	// toStore.clear();
	// }
	// }).start();
	// }
	// } catch (Exception ex) {
	// log.error(String.format("Unable to queue [%s]", sec_.getOccId()), ex);
	// }
	// }

	// private void queueForPersistence(ATSecEvent event_) {
	// try {
	// _eventStorageSet.add(event_);
	// if (_eventStorageSet.size() > _config.getSaveBulkSize()) {
	// Set<ATSecEvent> toStore = _eventStorageSet;
	// _eventStorageSet = new HashSet<>();
	// log.debug(String.format("Storing [%s(%d)]", event_.getClass().getSimpleName(), toStore.size()));
	// (new Thread() {
	// public void run() {
	// _eventDb.store(toStore);
	// toStore.clear();
	// }
	// }).start();
	// }
	// } catch (Exception ex) {
	// log.error(String.format("Unable to queue [%s %s]", event_.getOccId()), ex);
	// }
	// }

}
