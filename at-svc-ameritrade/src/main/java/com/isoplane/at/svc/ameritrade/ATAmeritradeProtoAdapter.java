package com.isoplane.at.svc.ameritrade;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.store.ATPersistentLookupBase;
import com.isoplane.at.commons.store.ATPriceHistory;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.EATChangeOperation;

public class ATAmeritradeProtoAdapter {

	static final Logger log = LoggerFactory.getLogger(ATAmeritradeProtoAdapter.class);

	static public final String SOURCE_KEY = "TDA";

	static public final String KEY_UNDERLYING = "ul";
	static public final String KEY_STRATEGY = "strategy";
	static public final String KEY_VOLATILITY = "volatility";
	static public final String KEY_RATE = "interestRate";
	static public final String KEY_PUTS = "puts";
	static public final String KEY_CALLS = "calls";

	// private Configuration _config;
	private Gson _gson = new Gson();
	private String _clientId;
	private String _rToken;
	private String _aToken;
	private long _aTokenTS;
	private long _lastRequestTime = 0;
	private long _requestSuccessCount = 0;
	private long _requestFailureCount = 0;
	private RequestConfig _httpReqConfig;
	private FileBasedConfigurationBuilder<XMLConfiguration> _configBuilder;
	private Map<String, String> _converterMap;

	static private SimpleDateFormat DF_YYYY_MM_dd = ATFormats.DATE_yyyy_MM_dd.get();
	// static private SimpleDateFormat DF_YYMMdd = ATFormats.DATE_yyMMdd.get();
	static private SimpleDateFormat _dfDS8 = ATFormats.DATE_yyyyMMdd.get();
	static private ATAmeritradeProtoAdapter _instance;

	static public ATAmeritradeProtoAdapter getInstance() {
		if (_instance == null) {
			_instance = new ATAmeritradeProtoAdapter();
			_instance.init();
		}
		return _instance;
	}

	private ATAmeritradeProtoAdapter() {
		// _config = config_;
		// _clientId = _config.getString("ameritrade.client_id");
		// _rToken = _config.getString("ameritrade.rtoken");
	}

	private void init() {
		Configuration config = ATConfigUtil.config();
		String path = config.getString("tda.path");
		try {
			_configBuilder = new Configurations().xmlBuilder(path);
			_configBuilder.setAutoSave(true);
			XMLConfiguration xmlConfig = _configBuilder.getConfiguration();
			// config.setProperty("test", "TEST");
			_clientId = xmlConfig.getString("clientId");
			_rToken = xmlConfig.getString("refreshToken");
			_aToken = xmlConfig.getString("accessToken");
			_aTokenTS = xmlConfig.getLong("accessTokenTimeOut");
			log.debug(String.format("Loaded client id [%b]", !StringUtils.isBlank(_clientId)));
			log.debug(String.format("Loaded refresh token [%b]", !StringUtils.isBlank(_rToken)));
			log.debug(String.format("Loaded access token [%b]", !StringUtils.isBlank(_aToken)));
			log.debug(String.format("Loaded access token TS [%b]", _aTokenTS != 0));

			int httpTimeout = config.getInt("tda.httpTimeout");
			_httpReqConfig = RequestConfig.custom()
					.setConnectTimeout(httpTimeout)
					.setConnectionRequestTimeout(httpTimeout)
					.setSocketTimeout(httpTimeout)
					.build();
			log.debug(String.format("HTTP timeout [%d]", httpTimeout));
		} catch (Exception ex) {
			String msg = String.format("init Error loading tda.path configuration: %s", path);
			throw new ATException(msg, ex);
		}
	}

	private boolean checkAccessToken() {
		// if(true) return true;
		if (_aToken == null || System.currentTimeMillis() > _aTokenTS) {
			refreshAccessToken();
		}
		if (_aToken != null) {
			// _aTokenTS = System.currentTimeMillis();
			return true;
		} else {
			log.error(String.format("checkAccessToken Failure"));
			return false;
		}
	}

	private void refreshAccessToken() {
		final String url = "https://api.tdameritrade.com/v1/oauth2/token";
		Map<String, String> form = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("grant_type", "refresh_token");
				put("refresh_token", _rToken);
				put("client_id", _clientId + "@AMER.OAUTHAP");
			}
		};
		String json = postHttp(url, null, null, form);
		parseAccessToken(json);
	}

	public void setConverter(Map<String, String> map_) {
		this._converterMap = map_;
	}

	private String getConversion(String atId_) {
		String conv = _converterMap != null ? _converterMap.get(atId_) : null;
		// return conv != null ? conv : atId_;
		return conv;
	}

	public TreeSet<ATPriceHistory> getHistory(Map<IATSymbol, String> symMap_, Date minDate_) {
		log.debug(String.format("getHistory[%d]", symMap_.size()));
		final String url = "https://api.tdameritrade.com/v1/marketdata/%s/pricehistory";
		TreeSet<ATPriceHistory> result = new TreeSet<>();
		long startDate = minDate_.getTime();
		Map<String, String> params = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("periodType", "month");
				put("frequencyType", "daily");
				put("frequency", "1");
				put("startDate", startDate + "");
			}
		};
		int count = 0;
		for (Entry<IATSymbol, String> entry : symMap_.entrySet()) {
			count++;
			String sym = entry.getKey().convert(SOURCE_KEY);
			try {
				String sUrl = String.format(url, sym);
				String ds8 = entry.getValue();
				if (ds8 != null) {
					Date d = _dfDS8.parse(ds8);
					params.put("startDate", d.getTime() + "");
				}
				String json = getHttp(sUrl, null, params);
				if (!StringUtils.isBlank(json)) {
					Collection<ATPriceHistory> history = parseHistory(entry.getKey(), json);
					if (history != null) {
						result.addAll(history);
					}
				} else {
					log.info(String.format("getHistory No history", entry.getKey()));
				}
			} catch (Exception ex) {
				log.error(String.format("getHistory Error[%s]", entry), ex);
			}
			log.debug(String.format("%3d/%d getHistory [%-6s]", count, symMap_.size(), sym));
		}
		return result;
	}

	public List<IATLookupMap> getFundamentals(Collection<IATSymbol> symbols_) {
		log.debug(String.format("getFundamentals[%d]", symbols_.size()));
		final String url = "https://api.tdameritrade.com/v1/instruments";
		String today_ds8 = ATFormats.DATE_yyyyMMdd.get().format(new Date());
		List<IATLookupMap> result = new ArrayList<>();
		int count = 0;
		for (IATSymbol sym : symbols_) {
			count++;
			String symbol = sym.convert(SOURCE_KEY);
			Map<String, String> params = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					put("projection", "fundamental");
					put("symbol", symbol);
				}
			};
			String json = getHttp(url, null, params);
			IATLookupMap dataMap = parseFundamentals(symbol, json);
			if (dataMap != null && !dataMap.isEmpty()) {
				dataMap.put(IATAssetConstants.TS, today_ds8);
				result.add(dataMap);
			}
			log.debug(String.format("%3d/%d Fundamentals [%-6s]", count, symbols_.size(), symbol));
		}
		return result;
	}

	public Map<String, MarketData> getMarketData(Collection<IATSymbol> symbols_) {
		try {
			log.debug(String.format("getMarketData[%d]", symbols_.size()));
			final String url = "https://api.tdameritrade.com/v1/marketdata/quotes";
			List<String> symList = symbols_.stream().map(s_ -> s_.convert(SOURCE_KEY)).collect(Collectors.toList());
			Map<String, String> params = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					put("symbol", String.join(",", symList));
				}
			};
			String json = getHttp(url, null, params);
			JsonNode rootNode = new ObjectMapper().readTree(json);
			if (rootNode == null || rootNode.isNull())
				return null;

			Map<String, MarketData> mktMap = new TreeMap<>();
			for (String sym : symList) {
				JsonNode symNode = rootNode.get(sym);
				if (symNode != null && !symNode.isNull()) {
					MarketData mkt = parseMarketData(sym, null, null, null, symNode);
					mktMap.put(sym, mkt);
				}
			}
			return mktMap;
		} catch (Exception ex) {
			log.error(String.format("getMarketData Error [%s]", symbols_), ex);
			return null;
		}
	}

	public IATLookupMap getOptionChain(IATSymbol symbol_, boolean isLarge_) {
		try {
			boolean isBypassAuth = ATConfigUtil.config().getBoolean("tda.bypass.tokens", false);
			if (!isBypassAuth && !checkAccessToken())
				return null;
			log.debug(String.format("getOptionChain: %s", symbol_));

			String atSymbol = symbol_.getId();
			String localSymbol = getConversion(atSymbol);// symbol_.convert(SOURCE_KEY);
			if (localSymbol != null) {
				log.debug(String.format("Conversion [%s -> %s]", atSymbol, localSymbol));
			}
			final String url = "https://api.tdameritrade.com/v1/marketdata/chains";
			Map<String, String> headers = isBypassAuth
					? new HashMap<>()
					: new HashMap<String, String>() {
						private static final long serialVersionUID = 1L;
						{
							put("Authorization", String.format("Bearer %s", _aToken));
						}
					};
			Map<String, String> params = new HashMap<String, String>() {
				private static final long serialVersionUID = 1L;
				{
					//		put("apikey", _clientId);
					put("symbol", localSymbol != null ? localSymbol : atSymbol);
					put("includeQuotes", "TRUE");
					put("strategy", "SINGLE");
					put("range", "ALL");
					put("optionType", "ALL");
				}
			};
			IATLookupMap map = null;
			if (!isLarge_) {
				String json = getHttp(url, headers, params);
				map = parseOptionChain(atSymbol, localSymbol, json);
			} else {
				Configuration config = ATConfigUtil.config();
				int largeMonths = config.getInt("tda.large.months", 6);
				int largeCycles = config.getInt("tda.large.cycles", 10);
				DateTimeFormatter dFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
				LocalDate dateStart = LocalDate.now();
				for (int i = 0; i < largeCycles; i++) {
					LocalDate dateEnd = dateStart.plusMonths(largeMonths);
					String dateStartStr = dFmt.format(dateStart);
					String dateEndStr = dFmt.format(dateEnd);
					params.put("fromDate", dateStartStr);
					params.put("toDate", dateEndStr);
					String json = getHttp(url, headers, params);
					IATLookupMap tempMap = parseOptionChain(atSymbol, localSymbol, json);
					if (i == 0 && tempMap != null) {
						map = tempMap;
					} else if (tempMap == null || tempMap.isEmpty()) {
						log.warn(String.format("Missing [%s %s-%s]", atSymbol, dateStartStr, dateEndStr));
						if (log.isDebugEnabled()) {
							int length = json != null ? Math.min(json.length(), 100) : 0;
							log.debug(String.format(" -> [%s] json: %s...", atSymbol,
									length > 0 ? json.substring(0, length) : "null"));
						}
						break;
					} else {
						if (log.isDebugEnabled()) {
							log.debug(String.format("Success [%s %s-%s]", atSymbol, dateStartStr, dateEndStr));
						}
						@SuppressWarnings("unchecked")
						Map<String, MarketData> oldCallMap = (Map<String, MarketData>) map.get(KEY_CALLS);
						@SuppressWarnings("unchecked")
						Map<String, MarketData> newCallMap = (Map<String, MarketData>) tempMap.get(KEY_CALLS);
						if (oldCallMap == null) {
							map.put(KEY_CALLS, newCallMap);
						} else if (newCallMap != null) {
							oldCallMap.putAll(newCallMap);
						}
						@SuppressWarnings("unchecked")
						Map<String, MarketData> oldPutMap = (Map<String, MarketData>) map.get(KEY_PUTS);
						@SuppressWarnings("unchecked")
						Map<String, MarketData> newPutMap = (Map<String, MarketData>) tempMap.get(KEY_PUTS);
						if (oldPutMap == null) {
							map.put(KEY_PUTS, newPutMap);
						} else if (newPutMap != null) {
							oldPutMap.putAll(newPutMap);
						}
					}
					dateStart = dateEnd;
				}
			}
			return map;
		} catch (Throwable ex_) {
			log.error(String.format("getOptionChain Error"), ex_);
			return null;
		}
	}

	private String parseAccessToken(String json_) {
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode jAccessToken = rootNode.get("access_token");
			JsonNode jExpiry = rootNode.get("expires_in");
			if (jAccessToken == null || jAccessToken.isNull() || !jAccessToken.isTextual() || jExpiry == null
					|| jExpiry.isNull()
					|| !jExpiry.isInt()) {
				try {
					JsonNode jError = rootNode.get("error");
					String msg = jError != null && !jError.isNull() && jError.isTextual() ? jError.asText()
							: jError + "";
					boolean isRateExceed = msg.contains("transactions per seconds restriction reached");
					log.error(String.format("parseAccessToken Error%s: %s", isRateExceed ? "[Rate exceeded]" : "",
							jError));
					Thread.sleep(1000);
					return null;
				} catch (Exception ex) {
				}
			}
			long timeout = System.currentTimeMillis() + (jExpiry.asInt() * 1000) - 5000;
			_aToken = jAccessToken.textValue();
			_aTokenTS = timeout;
			_configBuilder.getConfiguration().setProperty("accessToken", _aToken);
			_configBuilder.getConfiguration().setProperty("accessTokenTimeOut", timeout);

			return _aToken;
		} catch (Exception ex) {
			log.error(String.format("parseAccessToken Error: %s", json_), ex);
			return null;
		}
	}

	private IATLookupMap parseFundamentals(String symbol_, String json_) {
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonElement jsym = root.getAsJsonObject().get(symbol_);
		JsonElement jfun = jsym == null || jsym.isJsonNull() || !jsym.isJsonObject() ? null
				: jsym.getAsJsonObject().get("fundamental");
		if (jfun == null || jfun.isJsonNull() || !jfun.isJsonObject()) {
			log.warn(String.format("parseFundamentals No data [%s]", symbol_));
			return null;
		}
		JsonObject jofun = jfun.getAsJsonObject();
		IATLookupMap result = new ATPersistentLookupBase();
		result.setAtId(symbol_);
		for (String key : jofun.keySet()) {
			if ("symbol".equals(key))
				continue;
			JsonElement jvalue = jofun.get(key);
			if (jvalue.isJsonNull() || !jvalue.isJsonPrimitive())
				continue;
			JsonPrimitive jp = jvalue.getAsJsonPrimitive();
			if (jp.isString()) {
				result.put(key, jp.getAsString());
			} else if (jp.isNumber()) {
				Double jn = jp.getAsDouble();
				result.put(key, jn);
			}
		}
		return result;
	}

	private Collection<ATPriceHistory> parseHistory(IATSymbol symbol_, String json_) {
		JsonElement root = _gson.fromJson(json_, JsonElement.class);
		JsonElement jeCandles = root.getAsJsonObject().get("candles");
		if (jeCandles == null || !jeCandles.isJsonArray()) {
			log.warn(String.format("parseHistory No data [%s]", symbol_));
			return null;
		}
		List<ATPriceHistory> result = new ArrayList<>();
		JsonArray joCandles = jeCandles.getAsJsonArray();
		for (JsonElement je : joCandles) {
			if (je == null || !je.isJsonObject())
				continue;
			JsonObject jo = je.getAsJsonObject();

			long dt = jo.get("datetime").getAsLong();
			Double open = jo.get("open").getAsDouble();
			Double high = jo.get("high").getAsDouble();
			Double low = jo.get("low").getAsDouble();
			Double close = jo.get("close").getAsDouble();
			Long vol = jo.get("volume").getAsLong();

			String ds8 = _dfDS8.format(new Date(dt));
			String atId = String.format("%-5s%s", symbol_.getId(), ds8);

			ATPriceHistory pxh = new ATPriceHistory();
			pxh.setSource(SOURCE_KEY);
			pxh.setAtId(atId);
			pxh.setOccId(symbol_.getId());
			pxh.setDateStr(ds8);
			pxh.setOpen(open);
			pxh.setHigh(high);
			pxh.setLow(low);
			pxh.setClose(close);
			pxh.setVolume(vol);
			result.add(pxh);
		}
		return result;
	}

	private IATLookupMap parseOptionChain(String atSymbol_, String localSymbol_, String json_) {
		if (StringUtils.isBlank(json_)) {
			log.warn(String.format("parseOptionChain Blank [%s]", atSymbol_));
			return null;
		}
		try {
			JsonNode rootNode = new ObjectMapper().readTree(json_);
			JsonNode jStatus = rootNode.get("status");
			String status = jStatus != null && jStatus.isTextual() ? rootNode.get("status").asText() : null;
			if (!"SUCCESS".equals(status)) {
				log.warn(String.format("parseOptionChain Failed [%s]", atSymbol_));
				return null;
			}

			IATLookupMap result = new ATPersistentLookupBase();
			result.setAtId(atSymbol_);
			result.put(IATAssetConstants.OCCID, atSymbol_);
			JsonNode jInt = rootNode.get("interestRate");
			if (jInt != null && jInt.isDouble()) {
				result.put(KEY_RATE, jInt.asDouble());
			}
			JsonNode jVol = rootNode.get("volatility");
			if (jVol != null && jVol.isDouble()) {
				result.put(KEY_VOLATILITY, jVol.asDouble());
			}
			JsonNode jStrat = rootNode.get("strategy");
			if (jStrat != null && jStrat.isTextual()) {
				result.put(KEY_STRATEGY, jStrat.asText());
			}
			JsonNode jUl = rootNode.get("underlying");
			MarketData mktUl = parseMarketData(atSymbol_, localSymbol_, null, null, jUl);
			if (mktUl != null) {
				result.put(KEY_UNDERLYING, mktUl);
			}
			JsonNode jePutExpDateMap = rootNode.get("putExpDateMap");
			Map<String, MarketData> putMap = parseOptionMap(atSymbol_, localSymbol_, jePutExpDateMap);
			if (putMap != null) {
				result.put(KEY_PUTS, putMap);
			}
			JsonNode jeCallExpDateMap = rootNode.get("callExpDateMap");
			Map<String, MarketData> callMap = parseOptionMap(atSymbol_, localSymbol_, jeCallExpDateMap);
			if (callMap != null) {
				result.put(KEY_CALLS, callMap);
			}
			return result;
		} catch (Exception ex) {
			log.error(String.format("parseOptionChain Error [%s]", atSymbol_), ex);
			return null;
		}
	}

	private Map<String, MarketData> parseOptionMap(String atSymbol_, String localSymbol_, JsonNode jOptionExpMap_)
			throws ParseException {
		if (jOptionExpMap_ == null || jOptionExpMap_.isNull() || !jOptionExpMap_.isObject())
			return null;
		Map<String, MarketData> resultMap = new TreeMap<>();
		Iterator<String> expDateIterator = jOptionExpMap_.fieldNames();
		while (expDateIterator.hasNext()) {
			String expDateStr = expDateIterator.next();
			JsonNode jStrikes = jOptionExpMap_.get(expDateStr);
			if (jStrikes == null || jStrikes.isNull())
				continue;
			Date dExp = DF_YYYY_MM_dd.parse(expDateStr.substring(0, 10));
			Iterator<String> strikeIterator = jStrikes.fieldNames();
			while (strikeIterator.hasNext()) {
				String strikeStr = strikeIterator.next();
				JsonNode jStrikeArray = jStrikes.get(strikeStr);
				if (jStrikeArray == null || jStrikeArray.isNull() || !jStrikeArray.isArray())
					continue;
				Double strike = Double.parseDouble(strikeStr);
				JsonNode jStrikeValue = jStrikeArray.get(0);
				MarketData option = parseMarketData(atSymbol_, localSymbol_, dExp, strike, jStrikeValue);
				if (option != null) {
					resultMap.put(option.getId(), option);
				}
			}
		}
		return resultMap;
	}

	// NOTE:
	// Option: {"putCall":"PUT","symbol":"AAL_100220P5","description":"AAL Oct 2 2020 5 Put
	// (Weekly)","exchangeName":"OPR","bid":0.0,"ask":0.01,"last":0.01,"mark":0.01,"bidSize":0,"askSize":8,"bidAskSize":"0X8","lastSize":0,"highPrice":0.0,"lowPrice":0.0,"openPrice":0.0,"closePrice":0.0,"totalVolume":0,"tradeDate":null,"tradeTimeInLong":1601405392021,"quoteTimeInLong":1601490200404,"netChange":0.01,"volatility":394.281,"delta":-0.003,"gamma":0.002,"theta":-0.007,"vega":0.0,"rho":0.0,"openInterest":13,"timeValue":0.01,"theoreticalOptionValue":0.005,"theoreticalVolatility":29.0,"optionDeliverablesList":null,"strikePrice":5.0,"expirationDate":1601668800000,"daysToExpiration":2,"expirationType":"S","lastTradingDay":1601683200000,"multiplier":100.0,"settlementType":"
	// ","deliverableNote":"","isIndexOption":null,"percentChange":9900.0,"markChange":0.0,"markPercentChange":4900.0,"mini":false,"inTheMoney":false,"nonStandard":false}
	// Stock: {"symbol":"AAL","description":"American Airlines Group, Inc. - Common
	// Stock","change":0.04,"percentChange":0.3265,"close":12.25,"quoteTime":1601497521901,"tradeTime":1601497528827,"bid":12.3,"ask":12.31,"last":12.29,"mark":12.3,"markChange":0.05,"markPercentChange":0.4082,"bidSize":3900,"askSize":1000,"highPrice":12.915,"lowPrice":12.22,"openPrice":12.32,"totalVolume":64658040,"exchangeName":"NAS","fiftyTwoWeekHigh":31.67,"fiftyTwoWeekLow":8.25,"delayed":false}

	private MarketData parseMarketData(String atSymbol_, String localSymbol_x, Date dExp_, Double strike_,
			JsonNode jNode_) {
		if (jNode_ == null || jNode_.isNull() || !jNode_.isObject())
			return null;

		MarketData.Builder builder = MarketData.newBuilder();
		builder.setIdUl(atSymbol_);
		builder.setActionCode(EATChangeOperation.UPDATE.name());
		builder.setSrc(SOURCE_KEY);
		String occId = atSymbol_;

		String assetType = jNode_.get("assetType").asText();
		if ("MUTUAL_FUND".equals(assetType)) {
			builder.setId(occId);
			JsonNode jClose = jNode_.get("closePrice");
			if (jClose != null && jClose.isDouble()) {
				double close = jClose.doubleValue();
				if (close != 0) {
					builder.setPxLast(close);
//					builder.setPxClose(close);
				}
			}
		} else {
			if (dExp_ != null) {
				builder.setPxStrk(strike_);
				JsonNode jPutCall = jNode_.get("putCall");
				if (jPutCall != null && jPutCall.isTextual()) {
					String right = "PUT".equals(jPutCall.textValue()) ? "P" : "C";
					occId = ATFormats.toOccId(atSymbol_, dExp_, right, strike_);
					builder.setRight(right);
				}
				JsonNode jOpenInt = jNode_.get("openInterest");
				if (jOpenInt != null && jOpenInt.isLong()) {
					long value = jOpenInt.longValue();
					builder.setOpenInt(value);
				}
				JsonNode jPxTheo = jNode_.get("theoreticalOptionValue");
				if (jPxTheo != null && jPxTheo.isDouble()) {
					double value = jPxTheo.doubleValue();
					builder.setPxTheo(value);
				}
				JsonNode jVolatility = jNode_.get("volatility");
				if (jVolatility != null && jVolatility.isDouble()) {
					double value = jVolatility.doubleValue();
					if (value != 0) {
						builder.setIv(value);
					}
				}
				JsonNode jDelta = jNode_.get("delta");
				if (jDelta != null && jDelta.isDouble()) {
					double value = jDelta.doubleValue();
					builder.setDelta(value);
				}
				JsonNode jGamma = jNode_.get("gamma");
				if (jGamma != null && jGamma.isDouble()) {
					double value = jGamma.doubleValue();
					builder.setGamma(value);
				}
				JsonNode jRho = jNode_.get("rho");
				if (jRho != null && jRho.isDouble()) {
					double value = jRho.doubleValue();
					builder.setRho(value);
				}
				JsonNode jTheta = jNode_.get("theta");
				if (jTheta != null && jTheta.isDouble()) {
					double value = jTheta.doubleValue();
					builder.setTheta(value);
				}
				JsonNode jVega = jNode_.get("vega");
				if (jVega != null && jVega.isDouble()) {
					double value = jVega.doubleValue();
					builder.setVega(value);
				}
			}
			builder.setId(occId);

			JsonNode jLowPrice = jNode_.get("lowPrice");
			if (jLowPrice != null && jLowPrice.isDouble()) {
				double value = jLowPrice.doubleValue();
				if (value != 0) {
					builder.setPxDayLo(value);
				}
			}

			JsonNode jHighPrice = jNode_.get("highPrice");
			if (jHighPrice != null && jHighPrice.isDouble()) {
				double value = jHighPrice.doubleValue();
				if (value != 0) {
					builder.setPxDayHi(value);
				}
			}

			JsonNode jPercentChange = jNode_.get("percentChange");
			if (jPercentChange != null && jPercentChange.isDouble()) {
				double value = jPercentChange.doubleValue();
				if (value != 0) {
					builder.setPxDayCh(value);
				}
			}

			Long quoteTime = null;
			JsonNode jQuoteTime = dExp_ == null ? jNode_.get("quoteTime") : jNode_.get("quoteTimeInLong");
			if (jQuoteTime != null && jQuoteTime.isLong()) {
				quoteTime = jQuoteTime.longValue();
			}

			JsonNode jLast = jNode_.get("last");
			if (jLast != null && jLast.isDouble()) {
				double value = jLast.doubleValue();
				if (value != 0) {
					builder.setPxLast(value);
					JsonNode jLastTime = dExp_ == null ? jNode_.get("tradeTime") : jNode_.get("tradeTimeInLong");
					if (jLastTime != null && jLastTime.isLong()) {
						builder.setTsLast(jLastTime.longValue());
					}
					JsonNode jLastSize = jNode_.get("lastSize");
					if (jLastSize != null && jLastSize.isInt()) {
						int value2 = jLastSize.intValue();
						if (value2 != 0) {
							builder.setSzLast(value2);
						}
					}
				}
			}

			JsonNode jAsk = jNode_.get("ask");
			if (jAsk != null && jAsk.isDouble()) {
				double value = jAsk.doubleValue();
				if (value != 0) {
					builder.setPxAsk(value);
					if (quoteTime != null) {
						builder.setTsAsk(quoteTime);
					}
					JsonNode jAskSize = jNode_.get("askSize");
					if (jAskSize != null && jAskSize.isInt()) {
						int value2 = jAskSize.intValue();
						if (value2 != 0) {
							builder.setSzAsk(value2);
						}
					}

				}
			}

			JsonNode jBid = jNode_.get("bid");
			if (jBid != null && jBid.isDouble()) {
				double value = jBid.doubleValue();
				if (value != 0) {
					builder.setPxBid(value);
					if (quoteTime != null) {
						builder.setTsBid(quoteTime);
					}
					JsonNode jBidSize = jNode_.get("bidSize");
					if (jBidSize != null && jBidSize.isInt()) {
						int value2 = jBidSize.intValue();
						if (value2 != 0) {
							builder.setSzBid(value2);
						}
					}
				}
			}

			JsonNode jOpen = jNode_.get("openPrice");
			if (jOpen != null && jOpen.isDouble()) {
				double value = jOpen.doubleValue();
				if (value != 0) {
					builder.setPxOpen(value);
				}
			}

			JsonNode jClose = dExp_ == null ? jNode_.get("close") : jNode_.get("closePrice");
			if (jClose != null && jClose.isDouble()) {
				double close = jClose.doubleValue();
				if (close != 0) {
					builder.setPxClose(close);
				}
			}

			JsonNode jDailyVol = jNode_.get("totalVolume");
			if (jDailyVol != null && jDailyVol.isInt()) {
				int value = jDailyVol.intValue();
				if (value != 0) {
					builder.setVolDay(value);
				}
			}
		}
		MarketData mkt = builder.build();
		return mkt;
	}

	// Adds API Key
	protected String getHttp(String url_, Map<String, String> headers_, Map<String, String> params_) {
		CloseableHttpResponse response = null;
		try {
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong("tda.throttletime");
			long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			if (deltaTime < throttleTime) {
				Thread.sleep(deltaTime + 100);
			}
			_lastRequestTime = System.currentTimeMillis();

			Map<String, String> params = params_ != null ? params_ : new HashMap<>();
			params.put("apikey", _clientId);
			URIBuilder uri = new URIBuilder(url_);
			for (Entry<String, String> param : params.entrySet()) {
				uri.addParameter(param.getKey(), param.getValue());
			}

			CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(_httpReqConfig).build();
			// CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpGet httpGet = new HttpGet(uri.build());
			httpGet.setHeader("Accept", "application/json");
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpGet.setHeader(header.getKey(), header.getValue());
				}
			}

			response = httpclient.execute(httpGet);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			_requestSuccessCount++;
			return content;
		} catch (SocketTimeoutException toex) {
			_requestFailureCount++;
			log.error(String.format("getHttp Error: timeout [%s - %d/%d]: %s", params_.get("symbol"),
					_requestFailureCount, _requestSuccessCount,
					url_));
			return null;
		} catch (UnknownHostException ex) {
			log.error(String.format("getHttp Error [Unknown Host]: %s: %s", url_, params_));
			try {
				Thread.sleep(1000);
			} catch (Exception exx) {
			}
			return null;
		} catch (Exception ex) {
			_requestFailureCount++;
			log.error(String.format("getHttp Error: %s: %s", url_, params_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

	protected String postHttp(String url_, Map<String, String> headers_, Map<String, String> params_,
			Map<String, String> form_) {
		CloseableHttpResponse response = null;
		try {
			Configuration config = ATConfigUtil.config();
			long throttleTime = config.getLong("tda.throttletime");
			long deltaTime = System.currentTimeMillis() - _lastRequestTime;
			if (deltaTime < throttleTime) {
				Thread.sleep(deltaTime + 100);
			}
			_lastRequestTime = System.currentTimeMillis();

			// Map<String, String> params = params_ != null ? params_ : new HashMap<>();
			// params.put("apikey", _clientId);
			URIBuilder uri = new URIBuilder(url_);
			if (params_ != null) {
				for (Entry<String, String> param : params_.entrySet()) {
					uri.addParameter(param.getKey(), param.getValue());
				}
			}

			CloseableHttpClient httpclient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(uri.build());
			// httpPost.setHeader("Accept", "application/json");
			if (headers_ != null) {
				for (Entry<String, String> header : headers_.entrySet()) {
					httpPost.setHeader(header.getKey(), header.getValue());
				}
			}
			if (form_ != null) {
				List<NameValuePair> formData = new ArrayList<>();
				for (Entry<String, String> form : form_.entrySet()) {
					formData.add(new BasicNameValuePair(form.getKey(), form.getValue()));
				}
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(formData, Consts.UTF_8);
				httpPost.setEntity(formEntity);
			}

			response = httpclient.execute(httpPost);
			// log.trace(String.format("Response [%1$d] - 'GET' %2$s ", response.getStatusLine().getStatusCode(), url_));
			HttpEntity entity = response.getEntity();

			StringBuffer sb = new StringBuffer();
			BufferedReader rd = new BufferedReader(new InputStreamReader(entity.getContent()));
			String line;
			while ((line = rd.readLine()) != null) {
				sb.append(line).append(System.lineSeparator());
			}
			String content = sb.toString();
			EntityUtils.consume(entity);

			// _lastProcessTime = System.currentTimeMillis();
			return content;
		} catch (Exception ex) {
			log.error(String.format("postHttp Error: %s", url_), ex);
			return null;
		} finally {
			try {
				if (response != null) {
					response.close();
				}
			} catch (IOException e) {
			}
		}
	}

}
