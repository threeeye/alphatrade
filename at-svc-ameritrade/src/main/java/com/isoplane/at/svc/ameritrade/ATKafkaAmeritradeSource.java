package com.isoplane.at.svc.ameritrade;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATSession;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.IATUser;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketDataPack;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoUserSubscriber;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.commons.util.ATSessionDelayQueue;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusProducer;
import com.isoplane.at.kafka.symbol.ATKafkaSymbolPackConsumer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.kafka.user.ATKafkaUserSessionConsumer;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.symbols.ATSymbolManager;

public class ATKafkaAmeritradeSource implements IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaAmeritradeSource.class);

	static private String ID;
	static private ATKafkaAmeritradeSource _instance;

	static final private String PERIOD_CYCLE = "cycle";
	static final private String PERIOD_POLL = "poll";

	private final CountDownLatch _serviceLatch;

	private boolean _isRunning;
	private UserSessionManager _userSessionManager;
	private PriorityBlockingQueue<SymbolItem> _symbolQueue;
	private ATKafkaSystemStatusProducer _statusProducer;
	private ATAmeritradeProtoAdapter _ameritradeAdapter;
	private ATSymbolManager _symbolSvc;
	private ATKafkaMarketDataPackProducer _marketDataPackProducer;
	private MarketWorker _marketWorker;
	private ATSessionDelayQueue _sessionQueue;
	private Map<String, Long> _errorMap = new TreeMap<>();

	static public void main(String[] args_) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// Configuration config = ATConfigUtil.init(configPath, true);

			final String ipStr = ATSysUtils.getIpAddress("n/a");
			// ATSysLogger.setLoggerClass(String.format("TradierAgent.[%s]", ipStr));

			ID = String.format("%s:%s", ATKafkaAmeritradeSource.class.getSimpleName(), ipStr);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					ATExecutors.shutdown();
					if (_instance != null) {
						_instance.shutdown();
					}
				}
			}));

			ATExecutors.init();
			ATKafkaRegistry.register("tda_src",
					ATKafkaMarketDataProducer.class,
					ATKafkaMarketDataPackProducer.class,
					ATKafkaMarketStatusProducer.class,
					ATKafkaSymbolPackConsumer.class,
					ATKafkaSystemStatusProducer.class,
					ATKafkaUserSessionConsumer.class);

			_instance = new ATKafkaAmeritradeSource();
			_instance.init();

			String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
			log.info(msg);
			// ATSysLogger.info(String.format("%s [%s] started", _instance.getClass().getSimpleName(), ipStr));

			_instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}
			System.exit(0);

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaAmeritradeSource.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaAmeritradeSource() {
		_serviceLatch = new CountDownLatch(2);
	}

	private void init() {
		this._symbolQueue = new PriorityBlockingQueue<>(50, new Comparator<SymbolItem>() {

			@Override
			public int compare(SymbolItem a_, SymbolItem b_) {
				int prio = Integer.compare(a_.priority, b_.priority);
				return prio;
			}

		});

		this._symbolSvc = new ATSymbolManager(true);
		this._symbolSvc.initService();
		this._ameritradeAdapter = ATAmeritradeProtoAdapter.getInstance();

		ATSystemStatusManager.register(this);

		this._userSessionManager = new UserSessionManager();
		this._sessionQueue = new ATSessionDelayQueue();
		this._sessionQueue.subscribe(this._userSessionManager);
		ATKafkaUserSessionConsumer userSessionConsumer = ATKafkaRegistry.get(ATKafkaUserSessionConsumer.class);
		userSessionConsumer.subscribe(this._sessionQueue);

		// userSessionConsumer.subscribe(_userSessionManager);

	}

	private void start() {
		this._isRunning = true;

		_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);
		_marketDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);

		_marketWorker = new MarketWorker();
		ATExecutors.submit(MarketWorker.class.getSimpleName(), _marketWorker);

		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

		this._sessionQueue.start();

		ATKafkaRegistry.start();
	}

	protected void shutdown() {
		if (this._sessionQueue != null) {
			this._sessionQueue.stop();
		}
	}

	private void sendMarketData(String symbol_, List<MarketData> data_) {
		if (data_ == null || data_.isEmpty())
			return;
		// if ("SPY".equals(symbol_)) {
		// log.info(String.format("sendMarketData -> %s", symbol_));
		// }
		List<List<MarketData>> partitions = ListUtils.partition(data_, 100);
		for (int i = 0; i < partitions.size(); i++) {
			List<MarketData> partition = partitions.get(i);
			MarketDataPack.Builder builder = MarketDataPack.newBuilder();
			builder.addAllMarketData(partition);
			builder.setType(ATProtoMarketDataWrapper.TYPE_MARKET_DATA);
			builder.setSubType(ATProtoMarketDataWrapper.SUB_TYPE_OPTION_CHAIN);
			builder.setSource(ATAmeritradeProtoAdapter.SOURCE_KEY);
			builder.addSymbols(symbol_);
			builder.setSeqLength(partitions.size());
			builder.setSeq(i);
			MarketDataPack pack = builder.build();
			ATProtoMarketDataPackWrapper wrapper = new ATProtoMarketDataPackWrapper(pack);
			if (log.isDebugEnabled()) {
				log.debug(String.format("sendMarketData [%s, %02d/%d - %d]", symbol_, i + 1, partitions.size(), partition.size()));
			}
			this._marketDataPackProducer.send(wrapper);
		}
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Configuration config = ATConfigUtil.config();
		Map<String, String> status = new TreeMap<>();

		status.put("config.portfolio", String.format("%b", !config.getBoolean("tda.exclude.portfolio", false)));
		status.put("config.system", String.format("%b", !config.getBoolean("tda.exclude.system", false)));
		status.put("cycle.count", String.format("%d", this._marketWorker._cycleCount));
		status.put("cycle.duration", String.format("%s", this._marketWorker._cycleDuration));
		Set<String> userIds = this._userSessionManager.getUserIds();
		status.put("size.users", String.format("%d", userIds.size()));
		status.put("size.users.ids", String.format("%s", userIds));
		status.put("size.symbols",
				String.format("%d", this._userSessionManager._portfolioSet.size() + this._userSessionManager._temporarySet.size()
						+ this._userSessionManager._whitelistSet.size()));
		Set<String> portSymbols = this._userSessionManager.getPortSymbols();
		status.put("size.symbols.portfolio", String.format("%d", portSymbols.size()));
		status.put("size.symbols.portfolio.ids", String.format("%s", portSymbols));
		Set<String> sysSymbols = this._userSessionManager.getSystemDynamicSymbols();
		status.put("size.symbols.system", String.format("%d", sysSymbols.size()));
		status.put("size.symbols.system.ids", String.format("%s", sysSymbols));
		Set<String> tempSymbols = this._userSessionManager.getTempSymbols();
		status.put("size.symbols.temporary", String.format("%d", tempSymbols.size()));
		status.put("size.symbols.temporary.ids", String.format("%s", tempSymbols));
		Set<String> blSymbols = _userSessionManager.getBlacklistSymbols();
		status.put("size.symbols.whitelist", String.format("%d", blSymbols.size()));
		status.put("size.symbols.whitelist.ids", String.format("%s", blSymbols));
		Set<String> wlSymbols = _userSessionManager.getWhitelistSymbols();
		status.put("size.symbols.whitelist", String.format("%d", wlSymbols.size()));
		status.put("size.symbols.whitelist.ids", String.format("%s", wlSymbols));
		Set<String> lgSymbols = _userSessionManager.getLargelistSymbols();
		status.put("size.symbols.large", String.format("%d", lgSymbols.size()));
		status.put("size.symbols.large.ids", String.format("%s", lgSymbols));

		for (Entry<String, Long> entry : _errorMap.entrySet()) {
			status.put(String.format("errors.%s", entry.getKey()), String.format("%d", entry.getValue()));
			// status.put("count.errors.ids", String.format("%s", _errorMap.keySet()));
		}

		return status;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	private long getPeriod(String type_) {
		Configuration config = ATConfigUtil.config();
		boolean isIgnoreHours = config.getBoolean("tda.ignore.markethours", false);
		boolean isOpen = true;
		if (!isIgnoreHours) {
			DayOfWeek day = DayOfWeek.of(LocalDate.now().get(ChronoField.DAY_OF_WEEK));
			isOpen = !(DayOfWeek.SATURDAY.equals(day) || DayOfWeek.SUNDAY.equals(day));
			if (isOpen) {
				String startTimeStr = config.getString("tda.market.start", "09:00:00");
				String endTimeStr = config.getString("tda.market.end", "18:00:00");
				LocalTime startTime = LocalTime.parse(startTimeStr, DateTimeFormatter.ofPattern("HH:mm:ss"));
				LocalTime endTime = LocalTime.parse(endTimeStr, DateTimeFormatter.ofPattern("HH:mm:ss"));
				LocalTime now = LocalTime.now();
				isOpen = now.isAfter(startTime) && now.isBefore(endTime);
			}
		}
		if (!isOpen) {
			log.debug(String.format("Off-hour mode"));
		}
		long period = 10000;
		switch (type_) {
		case PERIOD_CYCLE:
			String cycleKey = isOpen ? "tda.queue.cycle.period" : "tda.queue.cycle.period.closed";
			period = config.getLong(cycleKey, 30000);
			break;
		case PERIOD_POLL:
			String pollKey = isOpen ? "tda.queue.poll.period" : "tda.queue.poll.period.closed";
			period = config.getLong(pollKey, 5000);
			break;
		}
		return period;
	}

	public class MarketWorker implements Runnable {

		private long _cycleCount = 0;
		private Duration _cycleDuration = Duration.ZERO;

		@Override
		public void run() {

			while (_isRunning) {
				try {
					// Configuration config = ATConfigUtil.config();
					long start = System.currentTimeMillis();
					while (!_symbolQueue.isEmpty()) {
						long pollPeriod = getPeriod(PERIOD_POLL);
						// long pollPeriod = config.getLong("tda.queue.poll.period", 5000);
						SymbolItem symItem = ATKafkaAmeritradeSource.this._symbolQueue.poll(pollPeriod, TimeUnit.MILLISECONDS);
						if (symItem == null)
							continue;
						log.debug(String.format("Processing [%s/%d]", symItem.symbol, symItem.priority));
						// if ("BRKB".equals(symItem.symbol)) {
						// log.info(symItem.symbol);
						// }
						IATSymbol symbol = ATKafkaAmeritradeSource.this._symbolSvc.getSymbol(symItem.symbol, true);
						if (symbol == null)
							continue;
						boolean isLarge = ATKafkaAmeritradeSource.this._userSessionManager.getLargelistSymbols().contains(symbol.getId());
						IATLookupMap optionChain = _ameritradeAdapter.getOptionChain(symbol, isLarge);
						log.debug(String.format("Request [%s]: %b", symbol, optionChain != null));
						if (optionChain != null && !optionChain.isEmpty()) {
							// Set<MarketData> data = new HashSet<>();
							List<MarketData> data = new ArrayList<>();
							MarketData underlying = (MarketData) optionChain.get(ATAmeritradeProtoAdapter.KEY_UNDERLYING);
							if (underlying != null) {
								data.add(underlying);
							}
							@SuppressWarnings("unchecked")
							Map<String, MarketData> calls = (Map<String, MarketData>) optionChain.get(ATAmeritradeProtoAdapter.KEY_CALLS);
							if (calls != null) {
								data.addAll(calls.values());
							}
							@SuppressWarnings("unchecked")
							Map<String, MarketData> puts = (Map<String, MarketData>) optionChain.get(ATAmeritradeProtoAdapter.KEY_PUTS);
							if (puts != null) {
								data.addAll(puts.values());
							}
							ATKafkaAmeritradeSource.this.sendMarketData(symItem.symbol, data);
						} else {
							String id = symbol.getId();
							Long count = _errorMap.get(id);
							Long newCount = count != null ? count + 1 : 1;
							_errorMap.put(id, newCount);
							log.warn(String.format("Failed [%s/%d]", id, newCount));
						}
					}
					long durationMs = System.currentTimeMillis() - start;
					_cycleDuration = Duration.ofMillis(durationMs);
					Collection<SymbolItem> symbols = ATKafkaAmeritradeSource.this._userSessionManager.getSymbols();
					_symbolQueue.addAll(symbols);
					if (symbols != null && !symbols.isEmpty()) {
						log.info(String.format("Cycle time [%s]", _cycleDuration));
					}
					_cycleCount++;
					// long cyclePeriod = config.getLong("tda.queue.cycle.period", 30000);
					long cyclePeriod = getPeriod(PERIOD_CYCLE);
					long delta = cyclePeriod - durationMs;
					if (delta > 0) {
						Thread.sleep(delta);
					}
				} catch (Exception ex) {
					log.error(String.format("%s Error"), this.getClass().getSimpleName(), ex);
				}
			}
		}

	}

	public class UserSessionManager implements IATProtoUserSubscriber {

		private Map<String, Collection<String>> _userSymbolPortMap;
		private Map<String, Collection<String>> _userSymbolTempMap;
		private Map<String, Collection<String>> _systemDynamicTempMap;
		private Set<SymbolItem> _portfolioSet;
		private Set<SymbolItem> _systemDynamicSet;
		private Set<SymbolItem> _temporarySet;
		private Set<String> _blacklistSet;
		private Set<String> _whitelistSet;
		private Set<String> _largeSet;

		public UserSessionManager() {
			this._userSymbolPortMap = new TreeMap<>();
			this._userSymbolTempMap = new TreeMap<>();
			this._systemDynamicTempMap = new TreeMap<>();
			this._systemDynamicSet = new TreeSet<>();
			this._portfolioSet = new HashSet<>();
			this._temporarySet = new HashSet<>();
			this._blacklistSet = new HashSet<>();
			this._whitelistSet = new HashSet<>();
			this._largeSet = new HashSet<>();
		}

		@Override
		public void notifyUserSession(EATChangeOperation action_, ATSession session_) {
			String userId = session_.getUserId();
			Map<String, String> data = session_.getData();
			switch (action_) {
			case CREATE:
			case UPDATE:
				String occIdsPortStr = data == null ? null : data.get(ATSession.DATA_SYMBOLS_PORT);
				if (occIdsPortStr != null) {
					Collection<String> occIdsPort = Arrays.asList(occIdsPortStr.trim().split("\\s*,\\s*"))
							.stream().map(s -> ATModelUtil.getSymbol(s)).collect(Collectors.toSet());
					this._userSymbolPortMap.put(userId, occIdsPort);
				}
				String symbolsTempStr = data == null ? null : data.get(ATSession.DATA_SYMBOLS_TEMP);
				if (symbolsTempStr != null) {
					Collection<String> symbolsTemp = Arrays.asList(symbolsTempStr.trim().split("\\s*,\\s*"))
							.stream().map(s -> ATModelUtil.getSymbol(s)).collect(Collectors.toSet());
					if (IATUser.STATIC_USERS.contains(userId)) {
						this._systemDynamicTempMap.put(userId, symbolsTemp);
					} else {
						this._userSymbolTempMap.put(userId, symbolsTemp);
					}
				}
				break;
			case DELETE:
				this._userSymbolPortMap.remove(userId);
				this._userSymbolTempMap.remove(userId);
				break;
			default:
				log.error(String.format("notifyUserSession Unsupported action [%s]", action_));
				break;
			}
			_portfolioSet = this._userSymbolPortMap.values().stream()
					.flatMap(m -> m.stream().map(s -> new SymbolItem(s, 3))).collect(Collectors.toCollection(TreeSet::new));
			_temporarySet = this._userSymbolTempMap.values().stream()
					.flatMap(m -> m.stream().map(s -> new SymbolItem(s, 1))).collect(Collectors.toCollection(TreeSet::new));
			_systemDynamicSet = this._systemDynamicTempMap.values().stream()
					.flatMap(m -> m.stream().map(s -> new SymbolItem(s, 1))).collect(Collectors.toCollection(TreeSet::new));

			Collection<SymbolItem> symbolItems = getSymbols();
			Set<String> symbols = symbolItems.stream().map(s -> s.symbol).collect(Collectors.toCollection(TreeSet::new));
			log.info(String.format("Symbols[%d]: %s", symbols.size(), symbols));
		}

		@SuppressWarnings("unchecked")
		public Collection<SymbolItem> getSymbols() {
			Map<String, String> convs = _symbolSvc.getSymbolConversions(ATAmeritradeProtoAdapter.SOURCE_KEY);
			_ameritradeAdapter.setConverter(convs);

			Configuration config = ATConfigUtil.config();
			Collection<SymbolItem> symbols = _temporarySet;

			String whitelistStr = config.getString("tda.whitelist");
			if (!StringUtils.isBlank(whitelistStr)) {
				_whitelistSet = new TreeSet<>(Arrays.asList(whitelistStr.trim().split("\\s*,\\s*")));
				Set<SymbolItem> whitelistSymbols = _whitelistSet.stream().map(s -> new SymbolItem(s.trim(), 2)).collect(Collectors.toSet());
				symbols = CollectionUtils.union(symbols, whitelistSymbols);
			} else {
				_whitelistSet = new HashSet<>();
			}

			boolean isExcludePortfolio = config.getBoolean("tda.exclude.portfolio", false);
			if (!isExcludePortfolio && !_portfolioSet.isEmpty()) {
				symbols = CollectionUtils.union(symbols, _portfolioSet);
			}
			boolean isExcludeSystem = config.getBoolean("tda.exclude.system", false);
			if (!isExcludeSystem && !_systemDynamicSet.isEmpty()) {
				symbols = CollectionUtils.union(symbols, _systemDynamicSet);
			}

			String blacklistStr = config.getString("tda.blacklist");
			if (!StringUtils.isBlank(blacklistStr)) {
				_blacklistSet = new TreeSet<>(Arrays.asList(blacklistStr.trim().split("\\s*,\\s*")));
				if (symbols != null) {
					symbols.removeIf(s -> _blacklistSet.contains(s.symbol));
				}
			} else {
				_blacklistSet = new HashSet<>();
			}

			String largelistStr = config.getString("tda.large.list");
			if (largelistStr != null && !largelistStr.isEmpty()) {
				this._largeSet = Arrays.asList(largelistStr.trim().split("\\s*,\\s*"))
						.stream().map(s -> ATModelUtil.getSymbol(s)).collect(Collectors.toCollection(TreeSet::new));
			} else {
				this._largeSet = new HashSet<>();
			}

			return symbols;
		}

		public Set<String> getUserIds() {
			Set<String> userIds = new HashSet<>(_userSymbolPortMap.keySet());
			userIds.addAll(_userSymbolTempMap.keySet());
			return userIds;
		}

		public Set<String> getPortSymbols() {
			Set<String> symbols = _portfolioSet.stream().map(s -> s.symbol).collect(Collectors.toCollection(TreeSet::new));
			return symbols;
		}

		public Set<String> getTempSymbols() {
			Set<String> symbols = _temporarySet.stream().map(s -> s.symbol).collect(Collectors.toCollection(TreeSet::new));
			return symbols;
		}

		public Set<String> getSystemDynamicSymbols() {
			Set<String> symbols = _systemDynamicSet.stream().map(s -> s.symbol).collect(Collectors.toCollection(TreeSet::new));
			return symbols;
		}

		public Set<String> getBlacklistSymbols() {
			return this._blacklistSet;
		}

		public Set<String> getWhitelistSymbols() {
			return this._whitelistSet;
		}

		public Set<String> getLargelistSymbols() {
			return this._largeSet;
		}
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			// NOTE: Dual nested to (a) reduce try/cath overhead (b) avoid nested iteration
			while (_isRunning) {
				try {
					while (_isRunning) {
						long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
						Map<String, String> status = ATSystemStatusManager.getStatus();
						_statusProducer.send(ID, status);
						Thread.sleep(statusPeriod);
					}
				} catch (Exception ex) {
					log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				}
			}
			log.info(String.format("%s Stopped", this.getClass().getSimpleName()));
		}
	}

	public static class SymbolItem implements Comparable<SymbolItem> {

		public SymbolItem(String symbol_, int priority_) {
			symbol = symbol_;
			priority = priority_;
		}

		public String symbol;
		public int priority;

		@Override
		public String toString() {
			return this.symbol;
		}

		@Override
		public int compareTo(SymbolItem other_) {
			if (other_ == null) {
				return -1;
			}
			return other_.symbol.compareTo(this.symbol);
		}
	}

}
