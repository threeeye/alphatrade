package com.isoplane.at.svc.ameritrade;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.store.IATLookupMap;
import com.isoplane.at.commons.util.ATConfigUtil;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATAmeritradeProtoAdapterTest {

	static final Logger log = LoggerFactory.getLogger(ATAmeritradeProtoAdapterTest.class);
	static private ATAmeritradeProtoAdapter _adapter;

	@BeforeAll
	public static void setup() {
		String root = System.getProperty("config_path");
		String propertiesPath = System.getProperty("test_args");
		propertiesPath = propertiesPath + ",at-svc-ameritrade/at-svc-ameritrade.properties";
		ATConfigUtil.init(root, propertiesPath, false);

		//	ATExecutors.init();
		_adapter = ATAmeritradeProtoAdapter.getInstance();
	}

	@AfterAll
	public static void tearDown() {
	}

	@BeforeEach
	public void prepare() {

	}

	@AfterEach
	public void cleanup() {
		try {
			Thread.sleep(1000);
		} catch (Exception ex) {
			log.error(String.format("cleanup Error"), ex);
		}
	}

	@Test
	//	@Disabled
	public void testMarketData() {
		List<String> symbols = Arrays.asList( "FGDFX"); //"AAPL", "MSFT",
		Collection<IATSymbol> syms = symbols.stream().map(s_ -> new ATSymbol(s_)).collect(Collectors.toList());
		Map<String, MarketData> rt = _adapter.getMarketData(syms);
		log.info(String.format("testMarketData: %s", rt));
		assertNotNull(rt);	}

	@Test
	@Disabled
	public void testFundamentals() {
		List<IATSymbol> symbols = Arrays.asList(new ATSymbol("AAPL"));
		List<IATLookupMap> rt = _adapter.getFundamentals(symbols);
		log.info(String.format("testFundamentals: %s", rt));
		assertNotNull(rt);
	}

	// public static void main(String[] args_) throws Exception {
	// 	//		List<String> configPaths = Arrays.asList("C:/Dev/workspace/alphatrade/local.properties", "C:/Dev/workspace/alphatrade/NOCHECKIN.properties");
	// 	//		CompositeConfiguration configs = new CompositeConfiguration();
	// 	//		for (String path : configPaths) {
	// 	//			PropertiesConfiguration config = new PropertiesConfiguration();
	// 	//			config.read(new FileReader(path));
	// 	//			configs.addConfiguration(config);
	// 	//		}

	// 	String configPath = args_ == null || args_.length < 1 ? null : args_[0];
	// 	ATConfigUtil.init(configPath, false);

	// 	ATAmeritradeProtoAdapter adapter = ATAmeritradeProtoAdapter.getInstance();

	// 	List<IATSymbol> symbols = Arrays.asList(new ATSymbol("AAPL"));
	// 	adapter.getFundamentals(symbols);
	// }
}
