package com.isoplane.at.ext.quantlib;

public class ATOptionAnalytics {

	private Double price;
	private Double delta; // Δ Delta – Sensitivity to Underlying's Price
	private Double gamma; // Γ Gamma – Sensitivity to Delta
	private Double vega; // ν Vega – Sensitivity to Underlying's Volatility
	private Double theta; // Θ Theta – Sensitivity to Time Decay

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDelta() {
		return delta;
	}

	public void setDelta(Double delta) {
		this.delta = delta;
	}

	public Double getGamma() {
		return gamma;
	}

	public void setGamma(Double gamma) {
		this.gamma = gamma;
	}

	public Double getVega() {
		return vega;
	}

	public void setVega(Double vega) {
		this.vega = vega;
	}

	public Double getTheta() {
		return theta;
	}

	public void setTheta(Double theta) {
		this.theta = theta;
	}

	@Override
	public String toString() {
		String str = String.format("%6.3f %4.2f %4.2f %4.2f %4.2f", getPrice(), getDelta(), getTheta(), getGamma(), getVega());
		return str;
	}

}
