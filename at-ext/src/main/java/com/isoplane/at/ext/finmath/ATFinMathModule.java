package com.isoplane.at.ext.finmath;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.finmath.functions.AnalyticFormulas;

public class ATFinMathModule {

	static final Logger log = LoggerFactory.getLogger(ATFinMathModule.class);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String dataStr = "79.95, 70.00, 13.04, 0.36, 0.02, CALL";
		String[] dataArray = dataStr.split(",");
		double underlying = Double.parseDouble(dataArray[0]);
		double strike = Double.parseDouble(dataArray[1]);
		double duration = Double.parseDouble(dataArray[2]);
		double volatility = Double.parseDouble(dataArray[3]);
		double interest = Double.parseDouble(dataArray[4]);
		boolean isPut = dataArray[5].startsWith("PUT");

		// ATFinMathModule instance = new ATFinMathModule();
		blackscholesOptionValue(underlying, strike, duration, volatility, interest, isPut);
//		blackscholesOptionValue(underlying, strike, duration, volatility, interest, true);
	}

	/**
	 * Simple Black-Scholes call value calculation
	 * 
	 * @param underlying_
	 *            - Unterlying price
	 * @param strike_
	 *            - Strike price
	 * @param duration_
	 *            - Years to expiration
	 * @param volatility_
	 *            - IV as fraction
	 * @param interest_
	 *            - Risk-free interest as fraction
	 * @param isPut_
	 *            - Put or Call
	 */
	static public double blackscholesOptionValue(double underlying_, double strike_, double duration_, double volatility_, double interest_,
			boolean isPut_) {
		double value = AnalyticFormulas.blackScholesOptionValue(underlying_, interest_, volatility_, duration_, strike_);
		if (isPut_) {
			value = strike_ * Math.exp(-interest_ * duration_) - underlying_ + value;
		}
		// if (log.isDebugEnabled()) {
		log.info(String.format("UL/Strike/Time/Vola/IRate: %.2f,%.2f,%.2f,%.2f,%.2f,%s: %.2f", underlying_, strike_, duration_,
				volatility_, interest_, isPut_ ? "PUT" : "CALL", value));
		// }
		return value;
	}

}
