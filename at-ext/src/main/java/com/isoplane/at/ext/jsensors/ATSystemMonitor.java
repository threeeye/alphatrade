package com.isoplane.at.ext.jsensors;

import java.util.HashMap;
import java.util.Map;

import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.profesorfalken.jsensors.JSensors;
import com.profesorfalken.jsensors.model.components.Components;
import com.profesorfalken.jsensors.model.components.Cpu;
import com.profesorfalken.jsensors.model.components.Disk;
import com.profesorfalken.jsensors.model.components.Gpu;
import com.profesorfalken.jsensors.model.components.Mobo;
import com.profesorfalken.jsensors.model.sensors.Fan;
import com.profesorfalken.jsensors.model.sensors.Load;
import com.profesorfalken.jsensors.model.sensors.Temperature;

public class ATSystemMonitor implements IATSystemStatusProvider {

	static private ATSystemMonitor _instance;

	private Components _components;

	static public void init() {
		if (_instance != null)
			return;
		_instance = new ATSystemMonitor();
		_instance._components = JSensors.get.components();
		String statusId = String.format("core.%s", ATSystemMonitor.class.getSimpleName());
		ATSystemStatusManager.register(_instance, statusId);
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Map<String, String> status = new HashMap<>();
		for (Cpu cpu : _components.cpus) {
			String name = cpu.name;
			for (Fan fan : cpu.sensors.fans) {
				status.put(String.format("cpu[%s].fan[%s]", name, fan.name), String.format("%.2f", fan.value));
			}
			for (Load load : cpu.sensors.loads) {
				status.put(String.format("cpu[%s].load[%s]", name, load.name), String.format("%.2f", load.value));
			}
			for (Temperature temp : cpu.sensors.temperatures) {
				status.put(String.format("cpu[%s].temp[%s]", name, temp.name), String.format("%.2f", temp.value));
			}
		}
		for (Gpu gpu : _components.gpus) {
			String name = gpu.name;
			for (Fan fan : gpu.sensors.fans) {
				status.put(String.format("gpu[%s].fan[%s]", name, fan.name), String.format("%.2f", fan.value));
			}
			for (Load load : gpu.sensors.loads) {
				status.put(String.format("gpu[%s].load[%s]", name, load.name), String.format("%.2f", load.value));
			}
			for (Temperature temp : gpu.sensors.temperatures) {
				status.put(String.format("gpu[%s].temp[%s]", name, temp.name), String.format("%.2f", temp.value));
			}
		}
		for (Disk disk : _components.disks) {
			String name = disk.name;
			for (Fan fan : disk.sensors.fans) {
				status.put(String.format("disk[%s].fan[%s]", name, fan.name), String.format("%.2f", fan.value));
			}
			for (Load load : disk.sensors.loads) {
				status.put(String.format("disk[%s].load[%s]", name, load.name), String.format("%.2f", load.value));
			}
			for (Temperature temp : disk.sensors.temperatures) {
				status.put(String.format("disk[%s].temp[%s]", name, temp.name), String.format("%.2f", temp.value));
			}
		}
		for (Mobo mobo : _components.mobos) {
			String name = mobo.name;
			for (Fan fan : mobo.sensors.fans) {
				status.put(String.format("mobo[%s].fan[%s]", name, fan.name), String.format("%.2f", fan.value));
			}
			for (Load load : mobo.sensors.loads) {
				status.put(String.format("mobo[%s].load[%s]", name, load.name), String.format("%.2f", load.value));
			}
			for (Temperature temp : mobo.sensors.temperatures) {
				status.put(String.format("mobo[%s].temp[%s]", name, temp.name), String.format("%.2f", temp.value));
			}
		}

		return status;
	}
	
	@Override
	public boolean isRunning() {
		return true;
	}

}
