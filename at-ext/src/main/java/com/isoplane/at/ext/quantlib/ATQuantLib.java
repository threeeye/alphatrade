package com.isoplane.at.ext.quantlib;

import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.lang3.time.DateUtils;
import org.quantlib.Actual365Fixed;
import org.quantlib.AmericanExercise;
import org.quantlib.BaroneAdesiWhaleyApproximationEngine;
import org.quantlib.BinomialCRRVanillaEngine;
import org.quantlib.BinomialEQPVanillaEngine;
import org.quantlib.BinomialJ4VanillaEngine;
import org.quantlib.BinomialJRVanillaEngine;
import org.quantlib.BinomialLRVanillaEngine;
import org.quantlib.BinomialTianVanillaEngine;
import org.quantlib.BinomialTrigeorgisVanillaEngine;
import org.quantlib.BjerksundStenslandApproximationEngine;
import org.quantlib.BlackConstantVol;
import org.quantlib.BlackScholesMertonProcess;
import org.quantlib.BlackVolTermStructure;
import org.quantlib.BlackVolTermStructureHandle;
import org.quantlib.Calendar;
import org.quantlib.Date;
import org.quantlib.DayCounter;
import org.quantlib.Exercise;
import org.quantlib.FdBlackScholesVanillaEngine;
import org.quantlib.FlatForward;
import org.quantlib.Month;
import org.quantlib.Option;
import org.quantlib.PlainVanillaPayoff;
import org.quantlib.PricingEngine;
import org.quantlib.QuoteHandle;
import org.quantlib.Settings;
import org.quantlib.SimpleQuote;
import org.quantlib.StrikedTypePayoff;
import org.quantlib.UnitedStates;
import org.quantlib.VanillaOption;
import org.quantlib.YieldTermStructure;
import org.quantlib.YieldTermStructureHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATOptionDescription;
import com.isoplane.at.commons.model.IATOptionDescription;
import com.isoplane.at.commons.model.IATOptionDescription.Right;
import com.isoplane.at.commons.util.ATConfigUtil;

public class ATQuantLib {

	static final Logger log = LoggerFactory.getLogger(ATQuantLib.class);

	final static private DayCounter _dayCounter = new Actual365Fixed();
	final static private Calendar _calendar = new UnitedStates();

	static private ATQuantLib _instance;

	// private Configuration _config;
	private EATPricingEngine _engine = EATPricingEngine.BinomialCoxRossRubinsteinEngine;
	private Date _today;

	// https://leanpub.com/quantlibpythoncookbook/read
	public static void main(String[] args_) throws ConfigurationException {

		String propertiesPath = args_[0];
		log.info(String.format("Reading properties: %s", propertiesPath));
		// PropertiesConfiguration config = new Configurations().properties(new File(propertiesPath));

		// our option
		double underlying = 261.09;
		double strike = 245.0;
		double riskFreeRate = 0.0244;
		double dividendYield = 0.0;
		double volatility = 0.643;
		// Option.Type type = Option.Type.Put;

		ATQuantLib instance = new ATQuantLib();
		instance.init();

		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.set(java.util.Calendar.YEAR, 2019);
		cal.set(java.util.Calendar.MONTH, 4 - 1);
		cal.set(java.util.Calendar.DAY_OF_MONTH, 18);

		ATOptionDescription option = new ATOptionDescription("TEST", cal.getTime(), strike, Right.Put);
		Map<String, ATOptionAnalytics> result = instance.priceAmericanEquityOption(underlying, volatility, dividendYield, riskFreeRate, option);

		log.info(String.format("Option analytics: %s", result));

		// Date nowDate = Date.todaysDate();
		// DayCounter dayCounter = new Actual365Fixed();
		// Settings.instance().setEvaluationDate(nowDate);
		// Settings.instance().includeReferenceDateEvents(true); // Don't set option value to 0 on expiration

		// Date tomorrowDate = nowDate.add(1);
	}

	static public ATQuantLib instance() {
		if (_instance == null) {
			_instance = new ATQuantLib();
			_instance.init();
		}
		return _instance;
	}

	private ATQuantLib() {
		reset();
	}

	public void init() {
		reset();
	}

	public void reset() {
		_today = Date.todaysDate();
		Settings.instance().setEvaluationDate(_today);
	}

	public void setEngine(EATPricingEngine engine_) {
		_engine = engine_;
	}

	public Map<String, ATOptionAnalytics> priceAmericanEquityOption(Double pxUL_, Double volatility_, Double dividendYield_, Double riskFreeRate_,
			IATOptionDescription... options_) {
		try {
			if (options_ == null || pxUL_ == null || volatility_ == null || dividendYield_ == null || riskFreeRate_ == null) {
				log.warn(String.format("Invalid option pricing params"));
				return null;
			}

			// define the underlying asset and the yield/dividend/volatility curves
			QuoteHandle underlyingH = new QuoteHandle(new SimpleQuote(pxUL_));
			YieldTermStructure dividendYieldStructure = new FlatForward(0, _calendar, dividendYield_, _dayCounter);
			YieldTermStructure riskFreeYieldStructure = new FlatForward(0, _calendar, riskFreeRate_, _dayCounter);
			BlackVolTermStructure volatilityTermStructure = new BlackConstantVol(0, _calendar, volatility_, _dayCounter);
			YieldTermStructureHandle dividendYield = new YieldTermStructureHandle(dividendYieldStructure);
			YieldTermStructureHandle interestYield = new YieldTermStructureHandle(riskFreeYieldStructure);
			BlackVolTermStructureHandle volatilityTerms = new BlackVolTermStructureHandle(volatilityTermStructure);

			BlackScholesMertonProcess stochasticProcess = new BlackScholesMertonProcess(underlyingH, dividendYield, interestYield, volatilityTerms);

			int steps = ATConfigUtil.config().getInt("quantlib.steps");
			PricingEngine engine;
			switch (_engine) {
			case BaroneAdesiWhaleyEngine:
				engine = new BaroneAdesiWhaleyApproximationEngine(stochasticProcess);
				break;
			case BjerksundStenslandEngine:
				engine = new BjerksundStenslandApproximationEngine(stochasticProcess);
				break;
			case FDAmericanEngine:
				engine = new FdBlackScholesVanillaEngine(stochasticProcess, steps, steps - 1);
				break;
			case BinomialJarrowRuddEngine:
				engine = new BinomialJRVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialCoxRossRubinsteinEngine:
				engine = new BinomialCRRVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialAdditiveEQPBinomialTreeEngine:
				engine = new BinomialEQPVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialTrigeorgisEngine:
				engine = new BinomialTrigeorgisVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialTianEngine:
				engine = new BinomialTianVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialLeisenReimerEngine:
				engine = new BinomialLRVanillaEngine(stochasticProcess, steps);
				break;
			case BinomialJoshi4aEngine:
				engine = new BinomialJ4VanillaEngine(stochasticProcess, steps);
				break;
			default:
				log.warn(String.format("Unsupported engine [%s]", _engine));
				return null;
			}

			boolean vegaSupport = vegaSupport(_engine);
			if (log.isDebugEnabled())
				log.debug(String.format("%-35s %-36s %-10s", "Method", "American", "Time"));
			Map<String, ATOptionAnalytics> result = new TreeMap<>();
			Date earliestDate = _today;
			for (IATOptionDescription option : options_) {
				try {
					long start = System.currentTimeMillis();
					Date expirationDate = date2Date(option.getExpiration());
					Option.Type type = right2Type(option.getRight());
					Exercise americanExercise = new AmericanExercise(earliestDate, expirationDate);
					StrikedTypePayoff payoff = new PlainVanillaPayoff(type, option.getStrike());
					// StrikedTypePayoff stPayoff = new org.quantlib.
					VanillaOption americanOption = new VanillaOption(payoff, americanExercise);
					ATOptionAnalytics analytics = analyze(americanOption, engine, vegaSupport);
					long duration = System.currentTimeMillis() - start;
					if (log.isDebugEnabled())
						log.debug(String.format("%-35s [%33s] %5d", engine, analytics, duration));
					result.put(option.getOccId(), analytics);
				} catch (Exception exx) {
					final String errToken = "earliest > latest exercise date";
					String msg = exx.getMessage();
					if (msg.contains(errToken)) {
						log.error(String.format("Error pricing [%s]: %s", option.getOccId(), msg));
					} else {
						log.error(String.format("Error pricing [%s]", option.getOccId()), exx);
					}
				}
			}

			return result;

		} catch (Exception ex) {
			log.error(String.format("Error calculating option analytics [%s]", _engine), ex);
			return null;
		}
	}

	private ATOptionAnalytics analyze(VanillaOption option_, PricingEngine engine_, boolean calcVega_) {
		option_.setPricingEngine(engine_);
		ATOptionAnalytics result = new ATOptionAnalytics();
		result.setPrice(option_.NPV());
		result.setDelta(option_.delta());
		result.setGamma(option_.gamma());
		result.setTheta(option_.theta());
		if (calcVega_) {
			result.setVega(option_.vega());
		}
		return result;

	}

	private static Date date2Date(java.util.Date date_) {
		java.util.Calendar cal = DateUtils.toCalendar(date_);
		@SuppressWarnings("static-access")
		Date result = new org.quantlib.Date(cal.get(cal.DAY_OF_MONTH), Month.swigToEnum(cal.get(cal.MONTH) + 1), cal.get(cal.YEAR));
		return result;
	}

	private static Option.Type right2Type(IATOptionDescription.Right right_) {
		switch (right_) {
		case Call:
			return Option.Type.Call;
		case Put:
			return Option.Type.Put;
		default:
			throw new ATException(String.format("Invalid option right [%s]", right_));
		}
	}

	private static boolean vegaSupport(EATPricingEngine engine_) {
		switch (engine_) {
		case BaroneAdesiWhaleyEngine:
			return true;
		case BjerksundStenslandEngine:
			return true;
		case FDAmericanEngine:
			return true;
		case BinomialJarrowRuddEngine:
			return true;
		case BinomialCoxRossRubinsteinEngine:
			return false;
		case BinomialAdditiveEQPBinomialTreeEngine:
			return true;
		case BinomialTrigeorgisEngine:
			return true;
		case BinomialTianEngine:
			return true;
		case BinomialLeisenReimerEngine:
			return true;
		case BinomialJoshi4aEngine:
			return true;
		default:
			log.warn(String.format("Unsupported engine [%s]", engine_));
			return false;
		}

	}

	public static enum EATPricingEngine {
		BaroneAdesiWhaleyEngine,
		BjerksundStenslandEngine,
		FDAmericanEngine,
		BinomialJarrowRuddEngine,
		BinomialCoxRossRubinsteinEngine,
		BinomialAdditiveEQPBinomialTreeEngine,
		BinomialTrigeorgisEngine,
		BinomialTianEngine,
		BinomialLeisenReimerEngine,
		BinomialJoshi4aEngine;
	}
}
