package com.isoplane.at.svc.tradier;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.symbols.ATSymbolManager;

public class ATTradierUtil {

	static final Logger log = LoggerFactory.getLogger(ATTradierUtil.class);

	static private Map<String, String> _at2TradierMap = new HashMap<>();
	static private Map<String, String> _tradier2AtMap = new HashMap<>();

	static public String tradier2OccId(String tradierId_, Map<String, String> reverseSymbolMap_) {
		if (StringUtils.isBlank(tradierId_))
			return tradierId_;
		if (tradierId_.length() < 9) {
			String tradierSymbol = tradierId_.trim();
			String symbol = reverseSymbolMap_.get(tradierSymbol);
			if (symbol == null)
				symbol = tradierSymbol;
			return symbol;
		}
		int i = 0;
		while (i < tradierId_.length() && !Character.isDigit(tradierId_.charAt(i)))
			i++;
		String tradierSymbol = tradierId_.substring(0, i);
		String symbol = reverseSymbolMap_.get(tradierSymbol);
		if (symbol == null)
			symbol = tradierSymbol;
		String rest = tradierId_.substring(i);
		String occId = String.format("%-6s%s", symbol, rest);
		return occId;
	}

	static public String toTradierOptionSymbol(String tradierSymbol_, Date expiration_, String right_, Double strike_) {
		String symbol = tradierSymbol_.replace("/", ""); // NOTE: Discrepancy between underlying and option symbol
		String expStr = ATFormats.DATE_yyMMdd.get().format(expiration_);
		Long strikeL = (long) (strike_ * 1000);
		// NOTE: Assumes symbol is converted
		String tradierId = String.format("%s%s%s%08d", symbol, expStr, right_, strikeL);
		return tradierId;
	}

	static public String toTradierSymbol(String occId_, ATSymbolManager symSvc_) {
		try {
			if (occId_.startsWith("TE$T"))
				return null;
			if (occId_.length() < 10) {
				String tradierSym = symSvc_.convertId(occId_, IATSymbol.TRADIER);
				return tradierSym;
			}
			String symbol = occId_.substring(0, 6).trim();
			String tradierSymbol = symSvc_.convertId(symbol, IATSymbol.TRADIER);
			if (tradierSymbol == null)
				tradierSymbol = symbol;
			String expStr = occId_.substring(6, 12);
			String right = occId_.substring(12, 13);
			String strikeStr = occId_.substring(13);
			Long strikeL = Long.parseLong(strikeStr);

			String tradierId = String.format("%s%s%s%08d", tradierSymbol, expStr, right, strikeL);
			return tradierId;
		} catch (Exception ex) {
			log.error(String.format("Error converting [%s]", occId_), ex);
			return null;
		}
	}

	static public void registerSymbol(String atSymbol_, String tradierSymbol_) {
		if (StringUtils.isBlank(atSymbol_) || StringUtils.isBlank(tradierSymbol_) || atSymbol_.equals(tradierSymbol_))
			return;
		_at2TradierMap.put(atSymbol_, tradierSymbol_);
		_tradier2AtMap.put(tradierSymbol_, atSymbol_);
	}

	static String toAtSymbol(String tradierSymbol_) {
		String atSymbol = tradierSymbol_ != null ? _tradier2AtMap.get(tradierSymbol_) : null;
		return atSymbol != null ? atSymbol : tradierSymbol_;
	}

	static String toTradierSymbol(String atSymbol_) {
		String tradierSymbol = atSymbol_ != null ? _at2TradierMap.get(atSymbol_) : null;
		return tradierSymbol != null ? tradierSymbol : atSymbol_;
	}

}
