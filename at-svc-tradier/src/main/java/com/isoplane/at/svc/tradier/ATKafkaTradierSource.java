package com.isoplane.at.svc.tradier;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATSymbol;
import com.isoplane.at.commons.model.EATMarketStatus;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataPackWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketDataWrapper;
import com.isoplane.at.commons.model.protobuf.ATProtoMarketStatusWrapper;
import com.isoplane.at.commons.model.protobuf.MarketDataProtos.MarketData;
import com.isoplane.at.commons.service.ATSystemStatusManager;
import com.isoplane.at.commons.service.IATProtoMarketSubscriber;
import com.isoplane.at.commons.service.IATSymbolListener;
import com.isoplane.at.commons.service.IATSystemStatusProvider;
import com.isoplane.at.commons.service.IATTotalPositionListener;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.commons.util.ATFormats;
import com.isoplane.at.commons.util.ATPriorityQueue;
import com.isoplane.at.commons.util.ATPriorityQueue.IATQueueable;
//import com.isoplane.at.commons.util.ATSysLogger;
import com.isoplane.at.commons.util.ATSysUtils;
import com.isoplane.at.commons.util.EATChangeOperation;
import com.isoplane.at.kafka.ATKafkaRegistry;
import com.isoplane.at.kafka.market.ATKafkaMarketDataPackProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketDataProducer;
import com.isoplane.at.kafka.market.ATKafkaMarketStatusProducer;
import com.isoplane.at.kafka.system.ATKafkaSystemStatusProducer;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.symbols.ATSymbolManager;

public class ATKafkaTradierSource
		implements IATSymbolListener, IATTotalPositionListener, IATProtoMarketSubscriber, IATSystemStatusProvider {

	static final Logger log = LoggerFactory.getLogger(ATKafkaTradierSource.class);

	private static ATKafkaTradierSource _instance;
	private static String ID;

	private ATKafkaMarketDataProducer _kafkaMarketDataProducer;
	private ATKafkaMarketDataPackProducer _kafkaMarketDataPackProducer;
	private ATKafkaMarketStatusProducer _kafkaMarketStatusProducer;

	private ATTradierProtoConnector _connector;
	private boolean _isRunning;
	private Set<String> _tradierSymbols;
	private ATPriorityQueue<SymbolRequestToken> _standardSymbolQueue;
	private ATSymbolManager _symbolsManager;
	private final ReentrantLock _symbolLock = new ReentrantLock();
	private final CountDownLatch _serviceLatch;// = new CountDownLatch(2);
	private long _periodicCycle = 0;
	private long _periodicReportTimestamp = System.currentTimeMillis();
	private long _periodicMarketDataCount = 0;
	private long _periodicMarketDataRate = 0;
	private long _monitorReportTimestamp = System.currentTimeMillis();
	private long _monitorMarketDataCount = 0;
	private EATMarketStatus _marketStatus;// = EATMarketStatus.OPEN;
	private Date _marketStatusRequestTs;
	private Date _marketStatusChangeTs;
	// private long _throughputLogInterval;
	private ATSymbolRefresher _symbolRefresher;

	private ATKafkaSystemStatusProducer _statusProducer;
	Map<String, Set<SymbolRequestToken>> _symbolOptionTokenMap;

	static public void main(String[] args_) {
		ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
		try {
			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			Configuration config = ATConfigUtil.init(root, args_[0], true);

			final String ipStr = ATSysUtils.getIpAddress("n/a");
			// ATSysLogger.setLoggerClass(String.format("TradierAgent.[%s]", ipStr));

			ID = String.format("%s:%s", ATKafkaTradierSource.class.getSimpleName(), ipStr);

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
				@Override
				public void run() {
					log.info(String.format("Executing ShutdownHook"));
					_instance.shutdown();
					if (_instance != null) {
						ATExecutors.shutdown();
					}
				}
			}));

			ATExecutors.init();
			ATMongoTableRegistry.init();
			ATKafkaRegistry.register("trd_src",
					ATKafkaMarketDataProducer.class,
					ATKafkaMarketDataPackProducer.class,
					ATKafkaMarketStatusProducer.class,
					ATKafkaSystemStatusProducer.class);

			_instance = new ATKafkaTradierSource();
			_instance.init();

			String msg = String.format("Initialized - %s", ATSysUtils.getMemoryStatus());
			log.info(msg);

			_instance.start();

			if (config.getBoolean("runAsService", true)) {
				log.info(String.format("Running as service"));
				_instance._serviceLatch.await();
			} else {
				ATSysUtils.waitForEnter();
			}

		} catch (Exception ex) {
			log.error(String.format("Failed to start %s", ATKafkaTradierSource.class.getSimpleName()), ex);
			System.exit(1);
		}
	}

	public ATKafkaTradierSource() {
		_serviceLatch = new CountDownLatch(1);
		_tradierSymbols = Collections.synchronizedSet(new TreeSet<>());

		_symbolOptionTokenMap = new TreeMap<>();
		_standardSymbolQueue = new ATPriorityQueue<>();
		_symbolRefresher = new ATSymbolRefresher();
		_symbolsManager = new ATSymbolManager(true);
		_symbolsManager.register(this);
		ATSystemStatusManager.register(this);
	}

	private void init() {
		try {
			Configuration config = ATConfigUtil.config();
			_connector = new ATTradierProtoConnector();
			_connector.initAsyncClient();
			_connector.subscribe(this);
			_marketStatus = EATMarketStatus.CLOSED;
			_symbolsManager.initService();

			ATKafkaRegistry.start();
			_kafkaMarketDataProducer = ATKafkaRegistry.get(ATKafkaMarketDataProducer.class);
			_kafkaMarketDataPackProducer = ATKafkaRegistry.get(ATKafkaMarketDataPackProducer.class);
			_kafkaMarketStatusProducer = ATKafkaRegistry.get(ATKafkaMarketStatusProducer.class);
			_statusProducer = ATKafkaRegistry.get(ATKafkaSystemStatusProducer.class);

			boolean isDeleteCache = config.getBoolean("tradier.option.token.reset");
			if (isDeleteCache) {
				log.info(String.format("Delete option cache file [%b]", isDeleteCache));
				deleteOptionCacheFile();
			}
			reloadSymbols();

			LocalTime refreshTime = LocalTime.parse(config.getString("tradier.refreshTime"),
					DateTimeFormatter.ofPattern("HH:mm:ss"));
			long dur = ChronoUnit.MILLIS.between(LocalTime.now(), refreshTime);
			if (Math.signum(dur) < 0) {
				dur += (1000 * 60 * 60 * 24);
			}
			ATExecutors.scheduleAtFixedRate("symbol_reload", () -> {
				deleteOptionCacheFile();
				_symbolOptionTokenMap = new HashMap<>();
				reloadSymbols();
			}, dur, 1000 * 60 * 60 * 24);

			// _portfolioManager.initService();
			// _portfolioManager.getActiveOccIds(null, true).forEach(id_ -> {
			// notify(EATChangeOperation.ADD, id_);
			// String underlyingId = ATModelUtil.getUnderlyingOccId(id_);
			// if (underlyingId != null) {
			// notify(EATChangeOperation.ADD, underlyingId);
			// }
			// });
			_symbolsManager.getSymbols().forEach((id_, s_) -> {
				notify(EATChangeOperation.ADD, id_);
			});
			log.info(String.format("Loaded [%d] symbols: %s", _tradierSymbols.size(), _tradierSymbols));

			_periodicCycle = config.getLong("tradier.stream.interval.report");
			ATExecutors.scheduleAtFixedRate("throughput", new Runnable() {

				@Override
				public void run() {
					long deltaTs = System.currentTimeMillis() - _periodicReportTimestamp;
					_periodicMarketDataRate = (long) Math.ceil(_periodicMarketDataCount * 1000.0 / deltaTs);
					_periodicReportTimestamp = System.currentTimeMillis();
					_periodicMarketDataCount = 0;
					log.info(String.format("Effective throughput (1min/%s) [%d/s]", _marketStatus,
							_periodicMarketDataRate));
				}
			}, _periodicCycle, _periodicCycle);
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	private void start() {
		_isRunning = true;
		Configuration config = ATConfigUtil.config();

		String pre = this.getClass().getSimpleName();
		ATExecutors.submit(String.format("%s.%s", pre, StatusRunner.class.getSimpleName()), new StatusRunner());

		String threadId = String.format("%s.marketStatus", getClass().getSimpleName());
		ATExecutors.scheduleAtFixedRate(threadId, new Runnable() {
			@Override
			public void run() {
				Thread.currentThread().setName(threadId);
				updateMarketStatus();
			}
		}, 1000, 60000);

		boolean isStreamSymbols = config.getBoolean("tradier.stream.symbol", false);
		log.info(String.format("Symbol streaming [%b]", isStreamSymbols));
		String streamThreadId = String.format("%s", StreamRunner.class.getSimpleName());
		ATExecutors.submit(streamThreadId, new StreamRunner(streamThreadId));
		// streamSymbols();

		boolean isRequestSymbol = config.getBoolean("tradier.request.symbol", false);
		log.info(String.format("Market scanning  [%b]", isRequestSymbol));
		String requestThreadId = String.format("%s", StreamRunner.class.getSimpleName());
		ATExecutors.submit(requestThreadId, new RequestRunner(requestThreadId));
		// requestSymbols();
	}

	private void reloadSymbols() {
		log.info(String.format("reloadSymbols"));
		Map<String, ATSymbol> symbolMap = _symbolsManager.getSymbols();
		List<SymbolRequestToken> symbolList = new ArrayList<>();
		List<SymbolRequestToken> additionalSymbolList = loadSymbolMap(symbolMap, true);
		if (additionalSymbolList != null) {
			symbolList.addAll(additionalSymbolList);
		}

		_symbolLock.lock();
		log.info(String.format("reloadSymbols Locking symbols..."));
		Map<Integer, List<SymbolRequestToken>> tokenMap = symbolList.stream()
				.collect(Collectors.groupingBy(s -> s.priority));
		_standardSymbolQueue.clear();
		for (Entry<Integer, List<SymbolRequestToken>> entry : tokenMap.entrySet()) {
			_standardSymbolQueue.addAll(entry.getValue(), entry.getKey());
		}
		log.info(String.format("Loaded [%d] tickers: %s", symbolList.size(), _standardSymbolQueue.getStatus()));
		if (_symbolLock.isHeldByCurrentThread()) {
			_symbolLock.unlock();
		}
		log.info(String.format("reloadSymbols Symbols unlocked"));
	}

	// Pre-market trading is 4:00-9:30. After-hours trading is 16:00-20:00.
	private void updateMarketStatus() {
		try {
			EATMarketStatus status = _connector.getMarketStatus();

			_marketStatusRequestTs = new Date();
			if (status == null || (_marketStatus != null && _marketStatus.equals(status)))
				return;
			_marketStatusChangeTs = new Date();
			EATMarketStatus oldStatus = _marketStatus;
			_marketStatus = status;
			this._kafkaMarketStatusProducer.send(status);
			// if (log.isDebugEnabled())
			log.info(String.format("Market status changed [%s -> %s]", oldStatus, _marketStatus));
		} catch (Exception ex) {
			log.error(String.format("updateMarketStatus Error"), ex);
		}
	}

	@Override
	public void notify(EATChangeOperation operation_, ATSymbol symbol_) {
		// log.info(String.format("Reloading [%s]", symbol_));
		_symbolRefresher.schedule();
		// reloadSymbols(); // Note: Expensive but should rarely occur (actually occurs
		// nightly...)
	}

	@Override
	public void notifyDynamicSymbols(Map<String, ATSymbol> symbolMap_, Map<String, Set<String>> ignore) {
		// if (symbolMap_ == null || symbolMap_.isEmpty())
		// return;
		// // Map<String, String> reverseSymbMap = new HashMap<>();
		// List<SymbolRequestToken> symbolList = new ArrayList<>();
		// List<SymbolRequestToken> additionalSymbolList = loadSymbolMap(symbolMap_,
		// false);
		// if (additionalSymbolList != null) {
		// symbolList.addAll(additionalSymbolList);
		// }
		// log.info(String.format("notifyDynamicSymbols [%d]", additionalSymbolList !=
		// null ? additionalSymbolList.size() : 0));

		// _symbolLock.lock();
		// log.info(String.format("Locking for dynamic symbols..."));
		// _symbolLock.unlock();
		// log.info(String.format("Dynamic symbols unlocked"));
	}

	private void deleteOptionCacheFile() {
		Configuration config = ATConfigUtil.config();
		String pathStr = config.getString("tradier.option.token.path");
		Path path = Paths.get(pathStr);
		try {
			Files.deleteIfExists(path);
		} catch (IOException ex) {
			log.error(String.format("deleteOptionCacheFile Error deleting option token file [%s]", pathStr), ex);
		}
	}

	private List<SymbolRequestToken> loadSymbolMap(Map<String, ATSymbol> symbolMap_, boolean isSave_) {
		List<SymbolRequestToken> symbolList = new ArrayList<>();
		Configuration config = ATConfigUtil.config();
		String pathStr = config.getString("tradier.option.token.path");
		File file = new File(pathStr);
		int maxDays = config.getInt("tradier.stream.maxdays");
		Date maxDate = DateUtils.addDays(new Date(), maxDays);
		OptionTokens optionTokens = new OptionTokens();
		OptionTokens oldOptionTokens = null;

		Map<String, Set<String>> optionTickerMap = null;
		boolean isLogVerbose = config.getBoolean("tradier.log.verbose", false);

		for (IATSymbol symbol : symbolMap_.values()) {
			String occId = symbol.getId();
			String msg = String.format("Loading and preparing [%s]", occId);
			log(msg, isLogVerbose);
			String tradierId = symbol.convert(IATSymbol.TRADIER);
			if (!occId.equals(tradierId)) {
				ATTradierUtil.registerSymbol(occId, tradierId);
			}
			SymbolRequestToken underlyingToken = new SymbolRequestToken();
			underlyingToken.tradierId = tradierId;
			underlyingToken.underlyingOccId = occId;
			underlyingToken.priority = 2;
			symbolList.add(underlyingToken);

			Set<SymbolRequestToken> optionTokenSet = _symbolOptionTokenMap.get(occId);
			if (optionTokenSet == null) {
				OptionTokens.Symbol optionTokenSymbol = new OptionTokens.Symbol();
				optionTokenSymbol.rootSymbol = occId;
				optionTokens.symbols.add(optionTokenSymbol);

				optionTokenSet = new HashSet<>();
				_symbolOptionTokenMap.put(occId, optionTokenSet);
				Set<String> optionTickers = null;
				if (optionTickerMap == null && file.exists()) {
					try {
						if (oldOptionTokens == null) {
							ObjectMapper mapper = new ObjectMapper();
							oldOptionTokens = mapper.readValue(file, OptionTokens.class);
						}
						if (oldOptionTokens != null) {
							optionTickers = oldOptionTokens.symbols.stream().filter(s -> occId.equals(s.rootSymbol))
									.flatMap(s -> s.options.stream())
									.collect(Collectors.toSet());
						}
					} catch (IOException ex) {
						log.error(String.format("loadSymbolMap Error reading option ticker file [%s]", pathStr), ex);
					}
				}
				if (optionTickers == null) {
					optionTickers = _connector.requestOptionTickers(tradierId);
				}
				if (optionTickers == null) {
					continue;
				}
				int dateStart = tradierId.length();
				int dateEnd = dateStart + 6;
				MarketData marketData = _connector.getCachedMarketData(occId);
				try {
					for (String ticker : optionTickers) {
						optionTokenSymbol.options.add(ticker);
						String expDateStr = ticker.substring(dateStart, dateEnd);
						Date expDate = ATFormats.DATE_yyMMdd.get().parse(expDateStr);
						if (maxDate.before(expDate))
							continue;
						char right = ticker.charAt(dateEnd);
						String strikeStr = ticker.substring(dateEnd + 1);
						double strike = Double.parseDouble(strikeStr) / 1000;
						int days2Exp = (int) TimeUnit.DAYS.convert(expDate.getTime() - System.currentTimeMillis(),
								TimeUnit.MILLISECONDS) + 1;

						SymbolRequestToken optionToken = new SymbolRequestToken();
						optionToken.days2Exp = days2Exp;
						optionToken.strike = strike;
						optionToken.tradierId = ticker;
						optionToken.underlyingOccId = occId;
						optionToken.isPut = 'P' == right;
						optionToken.priority = optionToken.getPriority(marketData);
						optionTokenSet.add(optionToken);

						symbolList.add(optionToken);
					}
				} catch (Exception ex) {
					log.error(String.format("reloadSymbols Error processing [%s]", occId), ex);
					continue;
				}
			}
		}
		if (isSave_) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.writeValue(new File(pathStr), optionTokens);
			} catch (IOException ex) {
				log.error(String.format("reloadSymbols Error saving"), ex);
			}
		}
		return symbolList;
	}

	@Override
	public void notify(EATChangeOperation operation_, String occId_) {
		String tradierId = ATTradierUtil.toTradierSymbol(occId_, _symbolsManager);
		if (tradierId == null) {
			log.warn(String.format("Unavailable symbol [%s/%s]", occId_, IATSymbol.TRADIER));
			return;
		}
		ATTradierUtil.registerSymbol(occId_, tradierId);
		switch (operation_) {
			case ADD:
				_tradierSymbols.add(tradierId);
				break;
			case DELETE:
				if (occId_.length() > 6) { // We only remove options
					_tradierSymbols.remove(tradierId);
				}
				break;
			default:
				break;
		}
	}

	@Override
	public void notifyMarketData(Collection<ATProtoMarketDataWrapper> wrappers_) {
		if (wrappers_ == null)
			return;
		_periodicMarketDataCount += wrappers_.size();
		_kafkaMarketDataProducer.send(wrappers_);
	}

	@Override
	public void notifyMarketDataPack(ATProtoMarketDataPackWrapper wrapper_) {
		int count = wrapper_ != null ? wrapper_.proto().getMarketDataCount() : -1;
		if (count < 1)
			return;
		_periodicMarketDataCount += count;
		_kafkaMarketDataPackProducer.send(wrapper_);
	}

	@Override
	public void notifyMarketStatus(ATProtoMarketStatusWrapper wrapper_) {
		// NOTE: Ignored here. Component checks periodically and sends to Kafka

		// if (wrapper_ != null) {
		// this._kafkaMarketStatusProducer.send(wrapper_);
		// }
	}

	public void shutdown() {
		log.debug(String.format("Shutting down"));
		if (_symbolsManager != null)
			_symbolsManager.shutdown();
		if (_connector != null)
			_connector.shutdown();
		ATKafkaRegistry.stop();
	}

	public class ATSymbolRefresher {

		private ScheduledFuture<?> _existingFuture;

		public void schedule() {
			if (_existingFuture != null) {
				_existingFuture.cancel(false);
				_existingFuture = null;
			}
			String threadId = "refresh_symbols(update)";
			ScheduledFuture<?> newFuture = ATExecutors.schedule(threadId, new Runnable() {

				@Override
				public void run() {
					Thread.currentThread().setName(threadId);
					log.info(String.format("ATSymbolRefresher.run()"));
					reloadSymbols();
					_existingFuture = null;
				}
			}, 2000);
			_existingFuture = newFuture;
		}

	}

	// @Override
	public void resetThroughput() {
		_monitorReportTimestamp = System.currentTimeMillis();
		_monitorMarketDataCount = 0;
	}

	// @Override
	public Long getThoughputRate() {
		long deltaTs = System.currentTimeMillis() - _monitorReportTimestamp;
		long ratio = (long) Math.ceil(_monitorMarketDataCount * 1000.0 / deltaTs);
		return ratio;
	}

	// @Override
	public String getThroughputDuration() {
		Duration duration = Duration.ofMillis(System.currentTimeMillis() - _monitorReportTimestamp);
		return duration.toString();
	}

	// @Override
	public String getThoughputStart() {
		Date date = new Date(_monitorReportTimestamp);
		String str = String.format("%s %s", ATFormats.DATE_yyyy_MM_dd.get().format(date),
				ATFormats.TIME_HH_mm_ss.get().format(date));
		return str;
	}

	@Override
	public Map<String, String> getSystemStatus() {
		Configuration config = ATConfigUtil.config();
		Map<String, String> statusMap = new HashMap<>();
		statusMap.put("timestamp", ATFormats.DATE_TIME_mid.get().format(new Date()));

		statusMap.put("throughput.duration", getThroughputDuration());
		statusMap.put("throughput.start", getThoughputStart());
		statusMap.put("throughput.rate", String.format("%d", getThoughputRate()));
		statusMap.put("throughput.count", String.format("%d", _monitorMarketDataCount));
		statusMap.put(String.format("throughput.rate(%ds)", (long) (_periodicCycle / 1000)),
				_periodicMarketDataRate + "");

		statusMap.put("marketStatus", _marketStatus.name());
		statusMap.put("marketStatus.change",
				_marketStatusChangeTs != null ? ATFormats.DATE_TIME_mid.get().format(_marketStatusChangeTs) : "n/a");
		statusMap.put("marketStatus.last",
				_marketStatusRequestTs != null ? ATFormats.DATE_TIME_mid.get().format(_marketStatusRequestTs) : "n/a");

		statusMap.put("config.request.symbols",
				String.format("%b", config.getBoolean("tradier.request.symbol", false)));
		statusMap.put("config.request.bulkSize", config.getInt("tradier.request.bulksize") + "");
		statusMap.put("config.request.period", config.getInt("tradier.request.period") + "");
		// statusMap.put("stream.symbols", _isStreamSymbols + "");
		statusMap.put("config.stream.positions",
				String.format("%b", config.getBoolean("tradier.stream.symbol", false)));

		statusMap.put("size.positions", String.format("%d", _tradierSymbols.size()));
		statusMap.put("size.positions.ids", String.format("%s", _tradierSymbols));

		for (Entry<String, Object> entry : _standardSymbolQueue.getStatus().entrySet()) {
			statusMap.put(String.format("queue.std.%s", entry.getKey()), entry.getValue() + "");
		}
		// for (Entry<String, Object> entry :
		// _dynamicSymbolQueue.getStatus().entrySet()) {
		// statusMap.put(String.format("queue.dyn.%s", entry.getKey()), entry.getValue()
		// + "");
		// }
		return statusMap;
	}

	@Override
	public boolean isRunning() {
		return this._isRunning;
	}

	private class StatusRunner implements Runnable {

		@Override
		public void run() {
			try {
				while (_isRunning) {
					long statusPeriod = ATConfigUtil.config().getLong("status.period", 30000);
					Map<String, String> status = ATSystemStatusManager.getStatus();
					_statusProducer.send(ID, status);
					Thread.sleep(statusPeriod);
				}
			} catch (Exception ex) {
				log.error(String.format("%s Error", this.getClass().getSimpleName()), ex);
				if (_isRunning) {
					run();
				}
			}
		}
	}

	public static class SymbolRequestToken implements IATQueueable {

		public String tradierId;
		public String underlyingOccId;
		public Double strike;
		public Integer days2Exp;
		public Integer priority;
		public Boolean isPut;
		public Long lastProcessedTime = -1L;

		@Override
		public String toString() {
			return tradierId;
		}

		@Override
		public int hashCode() {
			return tradierId == null ? 0 : tradierId.hashCode();
		}

		@Override
		public boolean equals(Object other_) {
			if (other_ instanceof SymbolRequestToken) {
				return this.hashCode() == other_.hashCode();
			}
			return false;
		}

		@Override
		public Long getLastProcessedTime() {
			return lastProcessedTime;
		}

		public int getPriority(MarketData market_) {
			int priceFactor = 1;
			if (market_ != null && strike != null && market_.hasPxLast()) {
				Double ulPxLast = market_.getPxLast();
				if (ulPxLast != null) {
					boolean isOutside = (strike < 0.5 * ulPxLast || strike > 1.5 * ulPxLast);
					if (isPut) {
						priceFactor = isOutside ? -1 : strike > ulPxLast ? 2 : 4;
					} else {
						priceFactor = isOutside ? -1 : 3;// strike < pxUl_ ? 2 : 4;
					}
				}
			}
			int timeFactor = days2Exp == null ? 5
					: days2Exp > 180 ? 1 : days2Exp > 90 ? 2 : days2Exp > 30 ? 3 : days2Exp > 14 ? 4 : 5;
			// (-> bulksize=200)
			// 1,6,9 -> 575/390/200
			// 1,7,12 -> 750/430/180 ???
			// 1,7,11 -> 610/350/120
			// (-> bulksize=250)
			// 1,7,10 -> 485/280/100
			// (-> bulksize=300)
			// 1,6,9 -> 360/240/80
			// 1,7,9 -> 380/220/90
			// 1,6,8 -> 340/230/85
			// 1,6,7 -> 310/210/95
			int weightedFactor = (3 * timeFactor + 2 * priceFactor) / 5;
			if (weightedFactor >= 4)
				return 7;
			else if (weightedFactor > 2)
				return 6;
			else
				return 1;
		}

	}

	private class StreamRunner implements Runnable {

		private String _threadId;

		public StreamRunner(String threadId_) {
			_threadId = threadId_;
		}

		@Override
		public void run() {
			Thread.currentThread().setName(_threadId);
			do {
				try {
					Configuration config = ATConfigUtil.config();
					boolean ignoreMarketHours = config.getBoolean("tradier.ignore.hours", false);
					boolean isStreamSymbols = config.getBoolean("tradier.stream.symbol", false);
					if ((!ignoreMarketHours && EATMarketStatus.CLOSED == _marketStatus) || !isStreamSymbols) {
						long snooze = config.getLong("tradier.snooze", 60000);
						log.info(String.format("streamSymbols  [%b] Market [%s]. Snoozing [%ds]...", isStreamSymbols,
								_marketStatus,
								snooze / 1000));
						Thread.sleep(snooze);
						continue;
					}
					Set<String> symbols = new TreeSet<>(_tradierSymbols);
					String positionDataTypes = config.getString("tradier.stream.symbol.data");
					_connector.streamSymbols(_threadId, symbols, positionDataTypes);
					long pause = config.getInt("tradier.stream.break.open");
					Thread.sleep(pause);
				} catch (Exception ex) {
					log.error(String.format("Error processing stream [%s]", _threadId), ex);
				}
			} while (_isRunning);
			_serviceLatch.countDown();
		}
	}

	private class RequestRunner implements Runnable {

		private String _threadId;

		public RequestRunner(String threadId_) {
			_threadId = threadId_;
		}

		@Override
		public void run() {
			Thread.currentThread().setName(_threadId);
			do {
				try {
					Configuration config = ATConfigUtil.config();
					boolean isRequestSymbol = config.getBoolean("tradier.request.symbol", false);
					boolean ignoreMarketHours = config.getBoolean("tradier.ignore.hours", false);
					if ((!ignoreMarketHours && EATMarketStatus.OPEN != ATKafkaTradierSource.this._marketStatus)
							|| !isRequestSymbol) {
						long snooze = config.getLong("tradier.snooze", 60000);
						log.info(String.format("requestSymbols [%b] Market [%s]. Snoozing [%ds]...", isRequestSymbol,
								ATKafkaTradierSource.this._marketStatus,
								snooze / 1000));
						Thread.sleep(snooze);
						continue;
					}
					ATKafkaTradierSource.this._symbolLock.lock();
					Set<String> requestedSymbols = new HashSet<>();
					ATPriorityQueue<SymbolRequestToken> queue = ATKafkaTradierSource.this._standardSymbolQueue;
					long reqPeriod = config.getLong("tradier.request.period");
					long end = System.currentTimeMillis() + reqPeriod;
					int bulkSize = config.getInt("tradier.request.bulksize");
					Collection<SymbolRequestToken> tokens = queue.poll(bulkSize);
					if (tokens == null || tokens.isEmpty()) {
						log.warn(String.format("requestSymbols No symbols. Throttling..."));
						// _symbolLock.unlock();
						Thread.sleep(5000);
						continue;
					}
					// log.debug(String.format("Symbols: %s", tokens.size()));
					Set<String> tradierIds = new HashSet<>();
					Set<String> underlyingIds = new TreeSet<>();
					tokens.forEach(t_ -> {
						tradierIds.add(t_.tradierId);
						underlyingIds.add(t_.underlyingOccId);
					});
					if (log.isDebugEnabled()) {
						requestedSymbols.addAll(tradierIds);
						log.debug(String.format("requestSymbols Requesting [%d/%d] %s", underlyingIds.size(),
								requestedSymbols.size(),
								log.isTraceEnabled() ? underlyingIds : ""));
					}
					Collection<String> unmatched = ATKafkaTradierSource.this._connector.requestQuotes(tradierIds);
					if (unmatched != null && !unmatched.isEmpty()) {
						log.info(String.format("requestSymbols Removing unmatched: %s", unmatched));
					}
					tokens.forEach(t_ -> {
						if (unmatched != null && unmatched.contains(t_.tradierId)) {
							requestedSymbols.remove(t_.tradierId);
							return;
						}
						ATKafkaTradierSource.this._monitorMarketDataCount++;
						t_.lastProcessedTime = System.currentTimeMillis();
						MarketData mkt = ATKafkaTradierSource.this._connector.getCachedMarketData(t_.underlyingOccId);
						t_.priority = t_.getPriority(mkt);
						queue.add(t_, t_.priority);
					});
					long remaining = end - System.currentTimeMillis();
					if (remaining > 0) {
						Thread.sleep(remaining);
					}
				} catch (Exception ex) {
					log.error(String.format("requestSymbols Error"), ex);
				} finally {
					if (ATKafkaTradierSource.this._symbolLock.isHeldByCurrentThread()) {
						ATKafkaTradierSource.this._symbolLock.unlock();
					}
				}
			} while (ATKafkaTradierSource.this._isRunning);
			ATKafkaTradierSource.this._serviceLatch.countDown();
		}
	}

	public static class OptionTokens {

		public List<Symbol> symbols;

		public OptionTokens() {
			symbols = new ArrayList<>();
		}

		public static class Symbol {

			public String rootSymbol;
			public List<String> options;

			public Symbol() {
				options = new ArrayList<>();
			}
		}

	}

	public static void log(String msg_, boolean isVerbose_) {
		if (isVerbose_) {
			log.info(msg_);
		} else {
			log.debug(msg_);
		}
	}

}
