package com.isoplane.at.tools.fixer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.ATPersistentBridgeMap;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.IATMongo;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;
import com.isoplane.at.tools.ATToolUtil;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.BulkWriteOptions;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOneModel;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATFixer {
    static final Logger log = LoggerFactory.getLogger(ATFixer.class);

    static final String MODE_FIX_SZ_ACT = "Fix 'sz_act'";
    static final String[] MODES = { MODE_FIX_SZ_ACT };

    private ATPortfolioMogodbTools _tradeDb;
    private FixerDao _dao;

    public static void main(String[] args_) {
        try {
            if (args_ == null || args_.length < 1) {
                throw new Exception(
                        String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
            }
            String root = System.getProperty("alphatradePath");
            ATConfigUtil.init(root, args_[0], true);

            ATExecutors.init();
            ATMongoTableRegistry.init();

            var instance = new ATFixer();
            instance.init();
            var mode = ATToolUtil.getSelection("Select mode", MODES, false)[0];
            switch (mode) {
                case MODE_FIX_SZ_ACT:
                    instance.fixSzAct();
                    break;
            }
        } catch (Exception ex_) {
            log.error(String.format("Error: %s", ex_.getMessage()), ex_);
        } finally {
            ATExecutors.shutdown();
            log.info("Exit");
        }
    }

    private void init() {
        _tradeDb = new ATPortfolioMogodbTools(false, null);
        _tradeDb.init();

        _dao = new FixerDao();
    }

    private void fixSzAct() {
        var tradeTable = ATMongoTableRegistry.getTradeTable();
        var query = ATPersistenceRegistry.query();
        var field = IATAssetConstants.OCCID;
        var occIds = _dao.distinct(tradeTable, query, field, String.class);

        var tradeDocComparator = new Comparator<TradeDoc>() {
            @Override
            public int compare(TradeDoc a_, TradeDoc b_) {
                return a_.compareTo(b_);
            }
        };
        int count = 0;
        for (String occId : occIds) {
            log.info(String.format("%s: %s  %d / %d", MODE_FIX_SZ_ACT, occId, ++count, occIds.size()));
            query = ATPersistenceRegistry.query().eq(field, occId);
            var trades = _dao.query(tradeTable, query, TradeDoc.class);
            trades.sort(tradeDocComparator);

            List<UpdateOneModel<Document>> bulks = new ArrayList<>();
            var sizeAct = 0.0;
            for (TradeDoc trade : trades) {
                var szAct = trade.get(IATAssetConstants.SIZE_ACTIVE);
                if (szAct != null)
                    continue;
                var szTrdNum = trade.get(IATAssetConstants.SIZE_TRADE);
                if (szTrdNum == null)
                    continue;
                double szTrd = szTrdNum instanceof Double ? (Double) szTrdNum : Double.valueOf((Integer) szTrdNum);
                sizeAct += szTrd;
                log.info(String.format("%d %s: %8.2f -> %8.2f", trade.get(IATAssetConstants.TS_TRADE), occId, szTrd,
                        sizeAct));

                var id = trade.get(IATMongo.MONGO_ID);
                var filter = Filters.eq(IATMongo.MONGO_ID, id);
                var doc = new Document(IATAssetConstants.SIZE_ACTIVE, sizeAct);
                if (szTrdNum instanceof Double) {
                    doc.append(IATAssetConstants.SIZE_TRADE, szTrd);
                }
                var update = new Document("$set", doc);
                bulks.add(new UpdateOneModel<>(filter, update));
            }
            if (bulks.isEmpty()) {
                log.info(String.format("%s Nothing to update: %s", MODE_FIX_SZ_ACT, occId));
            } else {
                var bulkResult = _dao.getCollection(tradeTable).bulkWrite(bulks, new BulkWriteOptions().ordered(false));
                log.info(String.format("%s Bulk update: %s", MODE_FIX_SZ_ACT, bulkResult.toString()));
            }
            // break;
        }
    }

    public static class FixerDao extends ATMongoDao {
        public MongoCollection<Document> getCollection(String id_) {
            var coll = super._db.getCollection(id_);
            return coll;
        }
    }

    public static class TradeDoc extends ATPersistentBridgeMap implements Comparable<TradeDoc> {

        public TradeDoc() {
            super(IATMongo.MONGO_ID);
        }

        @Override
        public int compareTo(TradeDoc other_) {
            var tsThis = (Long) this.get(IATAssetConstants.TS_TRADE);
            var tsOther = (Long) other_.get(IATAssetConstants.TS_TRADE);
            return tsThis.compareTo(tsOther);
        }

    }

}
