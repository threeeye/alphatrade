package com.isoplane.at.tools;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.adapter.IATIngester;
import com.isoplane.at.adapter.ally.ATAllyIngestAdapter;
import com.isoplane.at.adapter.ameritrade.ATAmeritradeIngestAdapter;
import com.isoplane.at.adapter.etrade.ATEtradeIngestAdapter;
import com.isoplane.at.adapter.fidelity.ATFidelityIngestAdapter;
import com.isoplane.at.adapter.schwab.ATSchwabIngestAdapter;
import com.isoplane.at.commons.ATException;
import com.isoplane.at.commons.model.ATBroker;
import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATTradeDigest;
import com.isoplane.at.commons.model.ATTradeGroup;
import com.isoplane.at.commons.model.IATAsset;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATSymbol;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.marketdata.ATMarketDataManager;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;

public class ATTradeToolBelt {

	static final Logger log = LoggerFactory.getLogger(ATTradeToolBelt.class);

	// static final String BROKER_FIDELITY = "Fidelity";
	// static final String BROKER_TDA = "TDA";
	static final String[] BROKERS = { IATSymbol.ALLY, IATSymbol.AMERITRADE, IATSymbol.ETRADE, IATSymbol.FIDELITY, IATSymbol.SCHWAB };

	private ATPortfolioMogodbTools _tradeDb;

	public ATTradeToolBelt() {
		this.init();
	}

	private void init() {
		_tradeDb = new ATPortfolioMogodbTools(false, null);
		_tradeDb.init();
	}

	public void ledgerizeTrades(String userId_, String broker_, String symbol_) {
		ATPortfolioManager pfSvc = new ATPortfolioManager(false);
		ATServiceRegistry.setPortfolioService(pfSvc, false);

		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);// .and().eq(IATTrade.UNDERLYING, "AAPL");
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		if (!StringUtils.isBlank(broker_)) {
			IATUserProvider userSvc = ATServiceRegistry.getUserService();
			List<ATBroker> brokers = userSvc.getUserBrokers(userId_);
			brokers.removeIf(b -> !broker_.equals(b.getBrokerId()));
			if (brokers == null || brokers.isEmpty()) {
				log.warn(String.format("getTrades No brokers [%s]", userId_));
				return;
			}
			Set<String> accountIds = brokers.stream().map(ATBroker::getId).collect(Collectors.toSet());
			query = query.and().in(IATTrade.TRADE_ACCOUNT, accountIds);
		}

		@SuppressWarnings("unchecked")
		List<IATTrade> dbTrades = (List<IATTrade>) (Object) _tradeDb.getTrades(query);
		if (dbTrades == null || dbTrades.isEmpty()) {
			log.info(String.format("ledgerizeTrades NO TRADES [%s]", userId_));
			return;
		}
		Map<String, Set<String>> symAccountMap = dbTrades.stream()
				.map(t_ -> new ATTrade(t_))
				.collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_.getOccId()),
						Collectors.mapping(ATTrade::getTradeAccount, Collectors.toSet())));
		log.info(String.format("ledgerizeTrades Symbol/Trade count[%s]: %d / %d", userId_, symAccountMap.size(), dbTrades.size()));

		// symAccountMap.keySet().removeIf(k_ -> !k_.equals("CCL"));

		Map<String, Map<String, Collection<ATTradeDigest>>> symbolGroupMap = new TreeMap<>();
		for (Entry<String, Set<String>> entry : symAccountMap.entrySet()) {
			String symbol = entry.getKey();
			Map<String, Collection<ATTradeDigest>> accountMap = new HashMap<>();
			for (String account : entry.getValue()) {
				Map<String, ATTradeDigest> accountSymbolMap = pfSvc.digestTrades(userId_, account, symbol);
				if (accountSymbolMap != null && !accountSymbolMap.isEmpty()) {
					accountMap.put(account, accountSymbolMap.values());
					ArrayList<ATTradeGroup> groups = pfSvc.groupTrades(userId_, account, symbol, accountSymbolMap.values());
					if (log.isDebugEnabled()) {
						log.debug(String.format("Saved [%s / %s / %s]: %d", userId_, account, symbol, groups != null ? groups.size() : 0));
					}
				}
			}
			if (!accountMap.isEmpty()) {
				symbolGroupMap.put(symbol, accountMap);
			}
		}
	}

	public void loadTradeGroups(String userId_) {
		ATPortfolioManager pfSvc = new ATPortfolioManager(false);
		ATServiceRegistry.setPortfolioService(pfSvc, false);

		List<ATTradeGroup> groups = pfSvc.getTradeGroups(userId_, null);
		log.info(String.format("Groups: %s", groups));
	}

	public String[] ingestTrades(String userId_, String broker_, String[] selection_) {
		IATIngester ingAdp = getIngester(broker_);
		String rootPath = ATConfigUtil.config().getString("dirRoot");
		Path tdaPath = Paths.get(rootPath, "tx", userId_, broker_);
		Set<String> files = ATToolUtil.listDirectory(tdaPath.toString());
		if (files == null || files.isEmpty()) {
			log.error(String.format("ingestTrades No statements found [%s]", tdaPath));
			return null;
		}
		String[] selection = selection_ != null ? selection_ : ATToolUtil.getSelection("statement", files.toArray(new String[0]), true);

		IATUserProvider userSvc = ATServiceRegistry.getUserService();
		List<ATBroker> brokers = userSvc.getUserBrokers(userId_);
		if (broker_ != null) {
			brokers.removeIf(b -> !broker_.equals(b.getBrokerId()));
		}
		if (brokers == null || brokers.isEmpty()) {
			log.warn(String.format("ingestTrades No brokers [%s]", userId_));
			return null;
		}

		Set<String> accountIds = brokers.stream().map(ATBroker::getId).collect(Collectors.toSet());
		IATWhereQuery tradeQuery = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_).and().in(IATTrade.TRADE_ACCOUNT, accountIds);
		List<ATTrade> dbTrades = _tradeDb.getTradeLedger(tradeQuery);
		Set<String> symbols = dbTrades.stream().map(t_ -> IATAsset.getString(t_, IATTrade.UNDERLYING)).collect(Collectors.toSet());

		ATPortfolioManager pfSvc = new ATPortfolioManager(false);
		ATServiceRegistry.setPortfolioService(pfSvc, false);
		if (!ATToolBelt.TEST_MODE) {
			ATMarketDataManager mktSvc = new ATMarketDataManager(false);
			ATServiceRegistry.setMarketDataService(mktSvc, false);
		}

		// Map<String,String> symSubMap = new HashMap<>();
		int count = 0;
		for (String fileName : selection) {
			Path filePath = Paths.get(tdaPath.toString(), fileName);
			List<ATTrade> newTrades = ingAdp.readTransactions(userId_, filePath.toString(), dbTrades, brokers);
			if (newTrades != null && !newTrades.isEmpty()) {
				for (ATTrade trade : newTrades) {
					if (!IATTrade.TYPES_IMMEDIATE.contains(trade.getTradeType()))
						continue;
					if (!symbols.contains(trade.getOccId())) {
						log.warn(String.format("Missing position [%s]: %s", trade.getOccId(), trade));
					}
				}
				Map<Object, List<ATTrade>> tradeMap = newTrades.stream()
						.collect(Collectors.groupingBy(t_ -> Triple.of(t_.getOccId(), t_.getTradeAccount(), t_.getTsTrade2())));
				if (ATToolBelt.TEST_MODE) {
					log.info(String.format("Test Mode [%b]", ATToolBelt.TEST_MODE));
				}
				for (List<ATTrade> tradeGroup : tradeMap.values()) {
					if (ATToolBelt.TEST_MODE) {
						log.info(String.format("[TEST] Saving [%d]: %s", tradeGroup.size(), tradeGroup));
						for (ATTrade trade : tradeGroup) {
							try {
								IATTrade.verify(trade, userId_);
							} catch (ATException ex_) {
								if (ATException.ERROR_NUMERIC_SYMBOL == ex_.getErrorId()) {
									this.fixNumericSymbol(trade, userId_);
								} else {
									throw ex_;
								}
							}
						}
					} else {
						count += pfSvc.saveTrades(userId_, tradeGroup.toArray(new ATTrade[0])) ? tradeGroup.size() : 0;
					}
				}
			}
		}
		log.info(String.format("ingestTrades Saved [%s / %d]", broker_, count));
		return selection;
	}

	private void fixNumericSymbol(ATTrade trade_, String userId_) {
		String symbol = ATToolUtil.getInput(String.format("Replace (%s)", trade_.getOccId()), false);
		trade_.setOccId(symbol);
		IATTrade.verify(trade_, userId_);
	}

	public void backfillTrades(String userId_, String broker_, String symbol_) {
		IATUserProvider userSvc = ATServiceRegistry.getUserService();
		List<ATBroker> brokers = userSvc.getUserBrokers(userId_);
		if (broker_ != null) {
			brokers.removeIf(b -> !broker_.equals(b.getBrokerId()));
		}
		if (brokers == null || brokers.isEmpty()) {
			log.warn(String.format("backfillTrades No brokers [%s]", userId_));
			return;
		}
		Set<String> accountIds = brokers.stream().map(ATBroker::getId).collect(Collectors.toSet());

		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_).and().in(IATTrade.TRADE_ACCOUNT, accountIds);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}

		List<IATTrade> dbTrades = _tradeDb.getTrades(query);
		if (dbTrades == null || dbTrades.isEmpty()) {
			return;
		}

		Map<String, Map<String, List<ATTrade>>> symAccountMap = dbTrades.stream()
				.map(t_ -> new ATTrade(t_))
				.sorted((a_, b_) -> {
					int sort = a_.getTsTrade2().compareTo(b_.getTsTrade2());
					if (sort == 0) {
						sort = a_.getOccId().compareTo(b_.getOccId());
					}
					return sort;
				})
				.collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_.getOccId()),
						Collectors.groupingBy(ATTrade::getTradeAccount)));

		ATPortfolioManager pfSvc = new ATPortfolioManager(false);
		ATServiceRegistry.setPortfolioService(pfSvc, false);
		if (!ATToolBelt.TEST_MODE) {
			ATMarketDataManager mktSvc = new ATMarketDataManager(false);
			ATServiceRegistry.setMarketDataService(mktSvc, false);
		}
		int count = 0;
		for (Entry<String, Map<String, List<ATTrade>>> symbolEntry : symAccountMap.entrySet()) {
			String symbol = symbolEntry.getKey();
			for (Entry<String, List<ATTrade>> accountEntry : symbolEntry.getValue().entrySet()) {
				String account = accountEntry.getKey();
				log.info(String.format("backfillTrades: %s / %6s / %s", userId_, symbol, account));
				for (ATTrade trade : accountEntry.getValue()) {
					IATTrade.cleanLedger(trade);
					trade.remove(IATTrade.PX_NET);
					if (ATToolBelt.TEST_MODE) {
						count++;
						log.info(
								String.format("[TEST] backfillTrades Saving[%2d]: %-21s / %d / %s / %s", count, trade.getOccId(), trade.getTsTrade2(),
										userId_, broker_));
						IATTrade.verify(trade, userId_);
					} else {
						count += pfSvc.saveTrades(userId_, new ATTrade[] { trade }) ? 1 : 0;
					}
				}
			}
		}
		log.info(String.format("backfillTrades Saved [%s / %d]", broker_, count));
	}

	public void cumulateTrades(String userId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		List<ATTrade> dbTrades = _tradeDb.getTradeLedger(query);
		Map<Object, List<ATTrade>> tradeMap = dbTrades.stream()
				.sorted((a_, b_) -> {
					int sort = a_.getTsTrade2().compareTo(b_.getTsTrade2());
					if (sort == 0) {
						sort = a_.getOccId().compareTo(b_.getOccId());
					}
					return sort;
				})
				.collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_.getOccId())));

		ATPortfolioManager pfSvc = new ATPortfolioManager(false);
		ATServiceRegistry.setPortfolioService(pfSvc, false);
		if (!ATToolBelt.TEST_MODE) {
			ATMarketDataManager mktSvc = new ATMarketDataManager(false);
			ATServiceRegistry.setMarketDataService(mktSvc, false);
			for (Entry<Object, List<ATTrade>> entry : tradeMap.entrySet()) {
				log.info(String.format("cumulateTrades: %s / %6s", userId_, entry.getKey()));
				pfSvc.cumulateTrades(userId_, entry.getValue());
			}
		} else {
			log.warn(String.format("cumulateTrades Cannot cumulate in test mode"));
			for (Entry<Object, List<ATTrade>> entry : tradeMap.entrySet()) {
				log.info(String.format("[TEST] cumulateTrades: %s / %-6s", userId_, entry.getKey()));
			}
		}
	}

	public List<ATTrade> getTrades(String userId_, String broker_, String symbol_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATTrade.TRADE_USER, userId_);
		if (!StringUtils.isBlank(symbol_)) {
			query = query.and().eq(IATTrade.UNDERLYING, symbol_);
		}
		if (!StringUtils.isBlank(broker_)) {
			IATUserProvider userSvc = ATServiceRegistry.getUserService();
			List<ATBroker> brokers = userSvc.getUserBrokers(userId_);
			brokers.removeIf(b -> !broker_.equals(b.getBrokerId()));
			if (brokers == null || brokers.isEmpty()) {
				log.warn(String.format("getTrades No brokers [%s]", userId_));
				return null;
			}
			Set<String> accountIds = brokers.stream().map(ATBroker::getId).collect(Collectors.toSet());
			query = query.and().in(IATTrade.TRADE_ACCOUNT, accountIds);
		}
		List<ATTrade> dbTrades = _tradeDb.getTradeLedger(query);
		return dbTrades;
	}

	private IATIngester getIngester(String broker_) {
		IATIngester ingAdp;
		switch (broker_) {
		case IATSymbol.ALLY:
			ingAdp = new ATAllyIngestAdapter();
			break;
		case IATSymbol.AMERITRADE:
			ingAdp = new ATAmeritradeIngestAdapter();
			break;
		case IATSymbol.ETRADE:
			ingAdp = new ATEtradeIngestAdapter();
			break;
		case IATSymbol.FIDELITY:
			ingAdp = new ATFidelityIngestAdapter();
			break;
		case IATSymbol.SCHWAB:
			ingAdp = new ATSchwabIngestAdapter();
			break;
		default:
			throw new ATException(String.format("unsupported Broker [%s]", broker_));
		}
		return ingAdp;
	}

}
