package com.isoplane.at.tools;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.ATUser;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.service.IATUserProvider;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATExecutors;
import com.isoplane.at.mongodb.util.ATMongoTableRegistry;
import com.isoplane.at.users.ATUserManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATToolBelt {

	static final Logger log = LoggerFactory.getLogger(ATToolBelt.class);

	static public boolean TEST_MODE = true;

	static final String CMD_TRADE_BACKFILLER = "trade-backfiller";
	static final String CMD_TRADE_CUMULATOR = "trade-cumulator";
	static final String CMD_TRADE_EXTRACTOR = "trade-extractor";
	static final String CMD_TRADE_GROUP_LOADER = "trade-group-loader";
	static final String CMD_TRADE_INGESTER = "trade-ingester";
	static final String CMD_TRADE_LEDGERIZER = "trade-ledgerizer";
	static final String CMD_TRADE_MIGRATOR = "trade-migrator";
	static final String[] CMDS = { CMD_TRADE_BACKFILLER, CMD_TRADE_CUMULATOR, CMD_TRADE_EXTRACTOR,
			CMD_TRADE_GROUP_LOADER, CMD_TRADE_INGESTER,
			CMD_TRADE_LEDGERIZER, CMD_TRADE_MIGRATOR };

	public static void main(String[] args_) {
		try {
			// String configPath = args_ == null || args_.length < 1 ? null : args_[0];
			// ATConfigUtil.init(configPath, true);

			if (args_ == null || args_.length < 1) {
				throw new Exception(
						String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
			}
			String root = System.getProperty("alphatradePath");
			ATConfigUtil.init(root, args_[0], true);

			ATExecutors.init();

			ATMongoTableRegistry.init();

			ATUserManager usrSvc = new ATUserManager(null, false);
			ATServiceRegistry.setUserService(usrSvc, false);

			ATToolBelt instance = new ATToolBelt();
			LinkedList<Object> commands = new LinkedList<>();
			boolean isRunning = false;
			do {
				isRunning = instance.execute(commands);
			} while (isRunning);
		} catch (Exception ex_) {
			log.error(String.format("Error: %s", ex_.getMessage()), ex_);
		} finally {
			ATExecutors.shutdown();
			log.info("Exit");
		}
	}

	private boolean execute(LinkedList<Object> commands_) {
		TEST_MODE = getMode("test mode", true);
		int result = executeCommands(commands_);
		boolean isRepeat = getMode("Repeat command sequence?", false);
		return isRepeat && !commands_.isEmpty();
	}

	private int executeCommands(LinkedList<Object> commands_) {
		try {
			boolean isScripted = !commands_.isEmpty();
			String cmd = isScripted ? (String) commands_.poll() : ATToolUtil.getSelection("command", CMDS, false)[0];

			switch (cmd) {
				case CMD_TRADE_BACKFILLER: {
					String userId = getUserIds(false).get(0);
					String[] brokers = ATToolUtil.getSelection("broker", ATTradeToolBelt.BROKERS, true);
					String broker = brokers.length > 1 ? null : brokers[0];
					String symbol = ATToolUtil.getInput("Symbol", false);
					if (symbol != null) {
						symbol = symbol.toUpperCase();
					}
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					try {
						tradeTool.backfillTrades(userId, broker, symbol);
					} catch (Exception ex) {
						log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
					}
					break;
				}
				case CMD_TRADE_CUMULATOR: {
					String userId = getUserIds(false).get(0);
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					try {
						tradeTool.cumulateTrades(userId);
					} catch (Exception ex) {
						log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
					}
					break;
				}
				case CMD_TRADE_EXTRACTOR: {
					String userId = getUserIds(false).get(0);
					String[] brokers = ATToolUtil.getSelection("broker", ATTradeToolBelt.BROKERS, true);
					String broker = brokers.length > 1 ? null : brokers[0];
					String symbol = null;
					while (StringUtils.isBlank(symbol)) {
						symbol = ATToolUtil.getInput("Symbol (or not)", false);
					}
					symbol = symbol.toUpperCase();
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					try {
						List<ATTrade> trades = tradeTool.getTrades(userId, broker, symbol.toUpperCase());
						if (trades != null) {
							trades.sort((a, b) -> {
								int sort = a.getTsTrade2().compareTo(b.getTsTrade2());
								if (sort == 0) {
									sort = a.getOccId().compareTo(b.getOccId());
								}
								return sort;
							});
							String rootPath = ATConfigUtil.config().getString("dirRoot");
							Path saveDir = Paths.get(rootPath, "tx", userId, "export");
							Path filePath = ATToolUtil.saveFile(saveDir, trades);
							log.info(String.format("Saved: %s", filePath));
						} else {
							log.info(String.format("No trades: %s / %s / %s", userId, broker, symbol));
						}
					} catch (Exception ex) {
						log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
					}
					break;
				}
				case CMD_TRADE_GROUP_LOADER: {
					List<String> userIds = getUserIds(true);
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					for (String userId : userIds) {
						try {
							tradeTool.loadTradeGroups(userId);
						} catch (Exception ex) {
							log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
						}
					}
					break;
				}
				case CMD_TRADE_INGESTER: {
					String userId = isScripted ? (String) commands_.poll() : getUserIds(false).get(0);
					String broker = isScripted ? (String) commands_.poll()
							: ATToolUtil.getSelection("broker", ATTradeToolBelt.BROKERS, false)[0];
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					String[] selection = isScripted ? (String[]) commands_.poll() : null;
					try {
						selection = tradeTool.ingestTrades(userId, broker, selection);
						if (selection != null) {
							//							refreshCommands(commands_, cmd, userId, broker, selection);
						}
					} catch (Exception ex) {
						log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
						//						refreshCommands(commands_, (Object[]) null);
					}
					refreshCommands(commands_, cmd, userId, broker, selection);
					break;
				}
				case CMD_TRADE_LEDGERIZER: {
					List<String> userIds = getUserIds(true);
					String broker = null;
					if (userIds.size() == 1) {
						String[] brokers = ATToolUtil.getSelection("broker", ATTradeToolBelt.BROKERS, true);
						broker = brokers.length > 1 ? null : brokers[0];
					}
					String symbol = ATToolUtil.getInput("Symbol", false);
					if (symbol != null) {
						symbol = symbol.toUpperCase();
					}
					ATTradeToolBelt tradeTool = new ATTradeToolBelt();
					for (String userId : userIds) {
						try {
							tradeTool.ledgerizeTrades(userId, broker, symbol);
						} catch (Exception ex) {
							log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
						}
					}
					break;
				}
				case CMD_TRADE_MIGRATOR: {
					List<String> userIds = getUserIds(true);
					ATMigrationToolBelt tool = new ATMigrationToolBelt();
					for (String userId : userIds) {
						try {
							tool.migrateTrades(userId);
						} catch (Exception ex) {
							log.error(String.format("Error %s[%s]: %s", cmd, userId, ex.getMessage()), ex);
						}
					}
					break;
				}
				default:
					commands_.remove(cmd);
					break;
			}
		} catch (Exception ex_) {
			log.error(String.format("Error: %s", ex_.getMessage()), ex_);
			return -1;
		}
		return 0;
	}

	private static boolean getMode(String label_, boolean default_) {
		String selection = null;
		try {
			String[] tf = new String[] { "true", "false" };
			selection = ATToolUtil.getSelection(label_, tf, false)[0];
			boolean testMode = Boolean.parseBoolean(selection);
			return testMode;
		} catch (Exception ex) {
			log.error(String.format("[%s] Unsupported [%s]. Defaulting to [%b].", label_, selection, default_));
			return default_;
		}
	}

	private void refreshCommands(LinkedList<Object> list_, Object... commands_) {
		if (list_ != null)
			list_.clear();
		if (commands_ == null)
			return;
		log.info(String.format("Command list: %s", Arrays.asList(commands_)));
		for (Object command : commands_) {
			list_.offer(command);
		}
	}

	private static List<String> getUserIds(boolean allowMultiple_) {
		IATUserProvider usrSvc = ATServiceRegistry.getUserService();
		List<ATUser> svcUsers = usrSvc.getUsers();
		svcUsers.sort((a, b) -> a.getEmail().compareTo(b.getEmail()));
		final List<ATUser> users = new ArrayList<>(new HashSet<>(svcUsers));
		if (allowMultiple_) {
			ATUser allUser = new ATUser();
			allUser.setAtId("ALL");
			allUser.setEmail("ALL");
			users.add(0, allUser);
		}
		Map<Integer, ATUser> map = new TreeMap<>();
		for (int i = 0; i < users.size(); i++) {
			map.put(i, users.get(i));
		}
		// Collectors.toMap(e -> e.getKey(), e -> e.getValue());
		// Map<Integer, ATUser> map = IntStream.range(0, users.size())
		// 		.boxed()
		// 		.collect(Collectors.toMap(i -> i, users::get, (o1, o2) -> o1 ));
		// Map<Integer, ATUser> map = IntStream.range(0, users.size())
		// 		.mapToObj(idx_ -> new SimpleEntry<Integer, ATUser>(idx_, users.get(idx_)))
		// 		.collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (o1, o2) -> o1, TreeMap::new));
		@SuppressWarnings("resource") // NOTE: Don't close System.in
		Scanner scanner = new Scanner(System.in);
		Integer userIdx = -1;
		while (userIdx == -1) {
			System.out.println(String.format("Select user..."));
			for (Entry<Integer, ATUser> entry : map.entrySet()) {
				System.out.println(String.format("%2d: %-28s - %s", entry.getKey(), entry.getValue().getAtId(),
						entry.getValue().getEmail()));
			}
			System.out.println(String.format(" X: Exit"));
			String str = scanner.nextLine();
			try {
				if ("X".equalsIgnoreCase(str)) {
					System.out.print(String.format("Exit"));
					System.exit(0);
				}
				userIdx = Integer.parseInt(str);
			} catch (Exception ex) {
				userIdx = -1;
			}
			if (!map.containsKey(userIdx)) {
				System.out.println(String.format("Error [%s]", str));
				userIdx = -1;
			}
		}
		System.out.println(String.format(" -> %s", map.get(userIdx).getEmail()));
		if (allowMultiple_) {
			map.remove(0);
		}
		List<String> result = (allowMultiple_ && userIdx == 0)
				? map.values().stream().map(u_ -> u_.getAtId()).collect(Collectors.toList())
				: Arrays.asList(map.get(userIdx).getAtId());
		return result;
	}

}
