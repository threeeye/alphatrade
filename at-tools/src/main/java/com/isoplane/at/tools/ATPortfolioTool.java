package com.isoplane.at.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.service.ATServiceRegistry;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATConfigUtil;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.marketdata.ATMarketDataManager;
import com.isoplane.at.mongodb.ATMongoDao;
import com.isoplane.at.mongodb.query.ATMongoWhereQuery;
import com.isoplane.at.portfolio.ATPortfolioManager;
import com.isoplane.at.portfolio.model.ATMongoTrade;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ATPortfolioTool {

    static final String[] TABLE_KEYS = new String[] { "mongo.t.trades", "mongo.t.trade.acu", "mongo.t.trade.group",
            "mongo.t.trade.ledger", "mongo.t.trade.digest", "mongo.t.portfolio" };

    static final Logger log = LoggerFactory.getLogger(ATPortfolioTool.class);

    private static ATPortfolioTool _tool;

    //    private ATPortfolioMogodbTools _mongodb;
    private ATPortfolioManager _portman;

    public static void main(String[] args_) {
        ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
        try {
            if (args_ == null || args_.length < 1) {
                throw new Exception(
                        String.format("Invalid arguments: %s", args_ != null ? Arrays.asList(args_) : null));
            }
            String root = System.getProperty("alphatradePath");
            Configuration config = ATConfigUtil.init(root, args_[0], true);
            for (String key : TABLE_KEYS) {
                String table = config.getString(key);
                ATConfigUtil.overrideConfiguration(key, String.format("TOOL_%s", table));
            }

            _tool = new ATPortfolioTool();

        } catch (Exception ex) {
            log.error(String.format("Error"), ex);
        }
        System.exit(0);
    }

    public ATPortfolioTool() {
        init();

        fixTraceIds("TdW8PcdGC9NvNl17kebhqdf5oNX2", "AAPL", null);
    }

    private void init() {
        ATPersistenceRegistry.setProviderClass(ATMongoWhereQuery.class);
        //    _mongodb = new ATPortfolioMogodbTools(false, null, null);
        //    _mongodb.init();
        ATMarketDataManager mktSvc = new ATMarketDataManager(false);
        ATServiceRegistry.setMarketDataService(mktSvc, false);

        _portman = new ATPortfolioManager(false);
        _portman.initService();
        //    _portman._mongodb
    }

    public void fixTraceIds(String userId_, String symbol_, String account_) {
        ATMongoDao db = new ATMongoDao();
        String table = ATConfigUtil.config().getString("mongo.t.trade.ledger");

        IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);
        if (!StringUtils.isBlank(symbol_)) {
            query = query.and().eq(IATTrade.UNDERLYING, symbol_);
        }
        if (!StringUtils.isBlank(account_)) {
            query = query.and().eq(IATAssetConstants.TRADE_ACCOUNT, account_);
        }
        List<ATMongoTrade> mongoTrades = db.query(table, query, ATMongoTrade.class);
        List<ATTrade> ledger = mongoTrades.stream().map(t -> new ATTrade((IATTrade) t)).toList();
        // for (ATMongoTrade trade : ledger) {
        //     trade.remove(IATAssetConstants.TRACE_ID);
        // }

        //    List<ATTrade> ledger = _portman._mongodb.getTradeLedger(query);
        // long deleteCount = db.delete(table, query);
        //  log.info(String.format("fixTraceIds {%s/%s/%s} loaded/deleted [%d/%d]", userId_, symbol_, account_,
        //          ledger.size(), deleteCount));

        Map<String, ArrayList<ATTrade>> symbolMap = new TreeMap<>();
        int count = 0;
        for (ATTrade trade : ledger) {
            String symbol = ATModelUtil.getSymbol(trade.getOccId());
            ArrayList<ATTrade> trades = symbolMap.get(symbol);
            if (trades == null) {
                trades = new ArrayList<>();
                symbolMap.put(symbol, trades);
            }
            trades.add(trade);
            count++;
        }
        log.info(String.format("Trade count: %d / %d", symbolMap.size(), count));

        for (String symbol : symbolMap.keySet()) {
            var symbolTrades = symbolMap.get(symbol);
            Map<String, List<ATTrade>> accountMap = symbolTrades.stream()
                    .collect(Collectors.groupingBy(ATTrade::getTradeAccount, TreeMap::new, Collectors.toList()));
            for (String acct : accountMap.keySet()) {
                List<ATTrade> accountTrades = accountMap.get(acct);
                Map<Long, List<ATTrade>> tradeMap = accountTrades.stream()
                        .collect(Collectors.groupingBy(ATTrade::getTsTrade2, TreeMap::new, Collectors.toList()));
                for (Long ts : tradeMap.keySet()) {
                    List<ATTrade> trades = tradeMap.get(ts);
                    trades.sort((o1, o2) -> o1.getTsTrade2().compareTo(o2.getTsTrade2()));
                    _portman.saveTrades(userId_, trades.toArray(new ATTrade[0]));
                    //      break;
                }
                //    break;
            }
            //        break;
        }
    }

}
