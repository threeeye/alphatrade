package com.isoplane.at.tools;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.isoplane.at.commons.model.ATTrade;
import com.isoplane.at.commons.model.IATAssetConstants;
import com.isoplane.at.commons.model.IATTrade;
import com.isoplane.at.commons.store.ATPersistenceRegistry;
import com.isoplane.at.commons.store.IATWhereQuery;
import com.isoplane.at.commons.util.ATModelUtil;
import com.isoplane.at.portfolio.util.ATPortfolioMogodbTools;

public class ATMigrationToolBelt {

	static final Logger log = LoggerFactory.getLogger(ATMigrationToolBelt.class);

	private ATPortfolioMogodbTools _tradeDb;

	public ATMigrationToolBelt() {
		this.init();
	}

	private void init() {
		_tradeDb = new ATPortfolioMogodbTools(false, null);
		_tradeDb.init();
	}

	public void migrateTrades(String userId_) {
		IATWhereQuery query = ATPersistenceRegistry.query().eq(IATAssetConstants.TRADE_USER, userId_);// .and().eq(IATTrade.UNDERLYING, "AAPL");
		@SuppressWarnings("unchecked")
		List<IATTrade> oldTrades = (List<IATTrade>) (Object) _tradeDb.getTrades(query);
		if (oldTrades == null || oldTrades.isEmpty()) {
			log.info(String.format("migrateTrades NO TRADES [%s]", userId_));
			return;
		}

		Function<ATTrade, String> a = (t) -> String.format("%s:%f:%f:%f:%d:%d:%s:%s", t.getOccId(), t.getSzTrade(), t.getPxTrade(),
				t.getPxTradeTotal(), t.getTradeType(), t.getTsTrade2(), t.getTradeUserId(), t.getTradeAccount());

		Map<String, Map<String, ATTrade>> oldSymbolMap = oldTrades.stream()
				// .filter(t_ -> !"CASH".equals(t_.getOccId()))
				.map(t_ -> new ATTrade(t_))
				.collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_.getOccId()),
						Collectors.toMap(t_ -> a.apply(t_), Function.identity())));
		// Collectors.groupingBy(ATTrade::getTradeAccount)));
		log.info(String.format("migrateTrades Symbol/Trade count[%s]: %d / %d", userId_, oldSymbolMap.size(), oldTrades.size()));

		// oldSymbolMap.keySet().removeIf(k_ -> !k_.equals("CL"));

		List<ATTrade> newTrades = _tradeDb.getTradeLedger(query);
		// newTrades.stream().collect(Collectors.toMap(t_ -> a.apply(t_), Function.identity()));
		Map<String, Map<String, ATTrade>> newSymbolMap = newTrades == null
				? null
				: newTrades.stream()
						.collect(Collectors.groupingBy(t_ -> ATModelUtil.getSymbol(t_.getOccId()),
								Collectors.toMap(t_ -> a.apply(t_), Function.identity())));

		for (Entry<String, Map<String, ATTrade>> oldSymbolEntry : oldSymbolMap.entrySet()) {
			String symbol = oldSymbolEntry.getKey();

			if (!"CL".equals(symbol)) {
				// log.info(String.format("migrateTrades skipping [%s]", symbol));
				continue;
			}

			Map<String, ATTrade> oldMap = oldSymbolEntry.getValue();
			Map<String, ATTrade> newMap = newSymbolMap == null ? null : newSymbolMap.get(symbol);
			if (newMap != null && !newMap.isEmpty()) {
				for (Entry<String, ATTrade> oldEntry : oldMap.entrySet()) {
					if (newMap.containsKey(oldEntry.getKey())) {
						log.info(String.format("migrateTrades HIT  [%s]", oldEntry.getKey()));
					} else {
						log.info(String.format("migrateTrades MISS [%s]", oldEntry.getKey()));
					}
				}
			} else {
				log.info(String.format("migrateTrades Missing Empty [%s]", symbol));
			}
		}
	}

}
