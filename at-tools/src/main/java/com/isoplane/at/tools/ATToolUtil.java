package com.isoplane.at.tools;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isoplane.at.commons.ATException;

public class ATToolUtil {

	static final Logger log = LoggerFactory.getLogger(ATToolUtil.class);

	static public String[] getSelection(String prompt_, String[] selection_, boolean allowAll_) {
		Map<Integer, String> map = IntStream.range(0, selection_.length)
				.mapToObj(idx_ -> new SimpleEntry<Integer, String>(idx_, selection_[idx_]))
				.collect(Collectors.toMap(SimpleEntry::getKey, SimpleEntry::getValue));
		@SuppressWarnings("resource") // NOTE: Don't close System.in
		Scanner scanner = new Scanner(System.in);
		Integer cmdIdx = -1;
		while (cmdIdx == -1) {
			System.out.println(String.format("Select %s...", prompt_));
			if (allowAll_) {
				System.out.println(String.format("%2d: %s", 0, "* All *"));
			}
			for (Entry<Integer, String> entry : map.entrySet()) {
				System.out.println(String.format("%2d: %s", entry.getKey() + 1, entry.getValue()));
			}
			System.out.println(String.format(" X: Exit"));
			System.out.print(String.format("Selection: "));
			String str = scanner.nextLine();
			try {
				if ("X".equalsIgnoreCase(str)) {
					System.out.print(String.format("Exit"));
					System.exit(0);
				}
				cmdIdx = Integer.parseInt(str);
				if (allowAll_ && cmdIdx == 0) {
					return selection_;
				}
				cmdIdx -= 1;
			} catch (Exception ex) {
				cmdIdx = -1;
			}
			if (!map.containsKey(cmdIdx)) {
				System.out.println(String.format("Error [%s]", str));
				cmdIdx = -1;
			}
		}
		System.out.println(String.format(" -> %s", selection_[cmdIdx]));
		String[] selected = { selection_[cmdIdx] };
		return selected;
	}

	static public String getInput(String prompt_, boolean isExitX_) {
		try {
			System.out.println(String.format("Enter %s...", prompt_));

			@SuppressWarnings("resource") // NOTE: Don't close System.in
			Scanner scanner = new Scanner(System.in);
			String str = scanner.nextLine();
			if (isExitX_ && "X".equalsIgnoreCase(str)) {
				System.out.print(String.format("Exit"));
				System.exit(0);
			}
			return str;
		} catch (Exception ex) {
			throw new ATException(ex);
		}
	}

	static public Set<String> listDirectory(String dir_) {
		try (Stream<Path> stream = Files.list(Paths.get(dir_))) {
			return stream
					.filter(file -> !Files.isDirectory(file))
					.map(Path::getFileName)
					.map(Path::toString)
					.collect(Collectors.toCollection(TreeSet::new));
		} catch (Exception ex) {
			log.error(String.format("listDirectory Error [%s", dir_), ex);
			return null;
		}
	}

	static public Path saveFile(Path dir_, Object content_) {
		Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
		String content = gson.toJson(content_);
		String fileName = getInput("file name (.json)", false);
		if (!fileName.endsWith(".json")) {
			fileName = String.format("%s.json", fileName);
		}
		Path path = dir_.resolve(fileName);
		try {
			BufferedWriter writer = Files.newBufferedWriter(path);
			writer.append(content);
			writer.close();
		} catch (IOException ex) {
			throw new ATException(ex);
		}

		return path;
	}
}
